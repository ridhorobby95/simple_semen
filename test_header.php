<?php 

	$dataHF = array(
			    		'id_document'	=> 4083,
			    		'judul'			=> 'testing nama board manual 2',
			    		'kode_dokumen'	=> 'BM/SMI/COM/024',
			    		'revisi'		=> 0,
			    		'tanggal_revisi'=> '21/06/2019 00.00.00,000000',
			    		'unit_kerja'	=> '00002000',
			    		'id_tipe_dokumen'=> 1,
			    		'filename'		=> 'test.pdf',
			    		'is_upload'		=> 1
	);
	$nama_file = "test";
	giveHeaderFooter($dataHF, $nama_file);


	function giveHeaderFooter($dataHF, $fileName){
		if(file_exists('local.txt')){$dir='./';}
		elseif(file_exists('/opt/lampp/htdocs/simple/prod.txt')){$dir = '/opt/lampp/htdocs/simple/';}
		elseif(file_exists('/opt/lampp/htdocs/dev/collaboration/smsi/dev.txt')){$dir = '/opt/lampp/htdocs/dev/collaboration/smsi/';}
		elseif(file_exists('/var/www/html/integra/sevima.txt')){$dir = '/var/www/html/integra/';}

		require_once $dir.'application/third_party/fpdf/pdf.php';

		if (file_exists('local.txt')){ // Local
			$db = "(DESCRIPTION =
                    (ADDRESS = (PROTOCOL = TCP)(HOST = THINKWEB)(PORT = 1521))
                    (CONNECT_DATA =
                      (SERVER = DEDICATED)
                      (SERVICE_NAME = XE)
                    )
                  )" ;
	  		$conn = OCILogon("arya", "1234", $db);
		}
		elseif(file_exists('/opt/lampp/htdocs/simple/dev.txt') && file_exists('/opt/lampp/htdocs/simple/prod.txt')){ // Prod
			$db = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.15.5.96)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SID=SGG)))'; 
			$conn = OCILogon("smsi", "semsi2018", $db);
		}
		elseif(file_exists('/opt/lampp/htdocs/dev/collaboration/smsi/dev.txt') && !file_exists('/opt/lampp/htdocs/dev/collaboration/smsi/prod.txt')){ // Dev
			$db = "(DESCRIPTION=(ADDRESS=(PORT=1521)(HOST=10.15.5.101)(PROTOCOL=TCP))(CONNECT_DATA=(SID=DEVSGG)))";
	  		$conn = OCILogon("smsi", "DmS12tiga", $db);
		}
		elseif(file_exists('/var/www/html/integra/sevima.txt')){
			$db = "(DESCRIPTION =
				    (ADDRESS = (PROTOCOL = TCP)(HOST = 36.85.91.184)(PORT = 1521))
				    (CONNECT_DATA =
				      (SERVER = DEDICATED)
				      (SID = SEVIMA)
				    )
				  )";
			$conn = OCILogon("integra", "DmS12tiga", $db);
		}

        /////get tanggal revisi
       $queryRevisi = oci_parse($conn, "SELECT to_char(add_months(VALIDITY_DATE, '-12'), 'DD MON YYYY') AS tanggal_revisi FROM documents where ID='".$dataHF['id_document']."'");
		$d = oci_execute($queryRevisi);
		$tanggalRevisi = oci_fetch_array($queryRevisi, OCI_ASSOC+OCI_RETURN_NULLS);
        // GET COMPANY NAME/////
        $query = oci_parse($conn, "SELECT COMPANY FROM UNITKERJA where ID='".$dataHF['unit_kerja']."'");
		$a = oci_execute($query);
		$unitkerja = oci_fetch_array($query, OCI_ASSOC+OCI_RETURN_NULLS);
		$dataCompany =  array(
			'2000'	=> 'PT Semen Indonesia (Persero) Tbk.',
			'3000'	=> 'PT SEMEN PADANG',
			'4000'	=> 'PT SEMEN TONASA',
			'5000'	=> 'PT SEMEN GRESIK',
			'9000'	=> 'PT SEMEN KUPANG INDONESIA'

		);
		$namaCompany = $dataCompany[$unitkerja['COMPANY']];
		// GET PHOTO LOGO COMPANY////////////////////////////
		$photo = oci_parse($conn, "SELECT PHOTO FROM UNITKERJA where ID='0000".$unitkerja['COMPANY']."'");
		$resPhoto = oci_execute($photo);
		$filePhoto = oci_fetch_array($photo, OCI_ASSOC+OCI_RETURN_NULLS);
		$logo = $filePhoto['PHOTO'];
		///////////////END OF GET PHOTO LOGO COMPANY////////////////
		// GET document type document
		$queryType = oci_parse($conn, "SELECT type FROM document_types where ID=".$dataHF['id_tipe_dokumen']);
		$c = oci_execute($queryType);
		$tipeDokumen = oci_fetch_array($queryType, OCI_ASSOC+OCI_RETURN_NULLS);

		$dataFooter =array();
		$index = 0;
		// GET related document
		$queryRelated1 = oci_parse($conn, "SELECT DOCUMENT_RELATED.DOCUMENT2, DOCUMENTS.CODE FROM DOCUMENT_RELATED INNER JOIN DOCUMENTS ON DOCUMENT_RELATED.DOCUMENT2 = DOCUMENTS.ID WHERE DOCUMENT_RELATED.DOCUMENT1 = '".$dataHF['id_document']."'");
		$exec_Related1 = oci_execute($queryRelated1);
		// $relatedDocument1 = oci_fetch_array($queryRelated1, OCI_ASSOC+OCI_RETURN_NULLS);
		while($relatedDocument1 = oci_fetch_array($queryRelated1, OCI_ASSOC+OCI_RETURN_NULLS)){
			$dataFooter[$index]['document'] = $relatedDocument1['DOCUMENT2'];
			$dataFooter[$index]['kode']		= $relatedDocument1['CODE'];
			$index++;
		}
		// print_r($relatedDocument1);

		// $queryRelated2 = oci_parse($conn, "SELECT DOCUMENT_RELATED.DOCUMENT1, DOCUMENTS.CODE FROM DOCUMENT_RELATED INNER JOIN DOCUMENTS ON DOCUMENT_RELATED.DOCUMENT1 = DOCUMENTS.ID WHERE DOCUMENT_RELATED.DOCUMENT2 = '".$dataHF['id_document']."' ");
		// $exec_Related2 = oci_execute($queryRelated2);
		// // $relatedDocument2 = oci_fetch_array($queryRelated2, OCI_ASSOC+OCI_RETURN_NULLS);
		// while($relatedDocument2 = oci_fetch_array($queryRelated2, OCI_ASSOC+OCI_RETURN_NULLS)){
		// 	$dataFooter[$index]['document'] = $relatedDocument2['DOCUMENT1'];
		// 	$dataFooter[$index]['kode']		= $relatedDocument2['CODE'];
		// 	$index++;
		// }
		// print_r($relatedDocument2);
		// print_r($dataFooter);
       
		// $pdf = new PDF();
		$data = array(
			    		'id_document'	=> $dataHF['id_document'],
			    		'judul'			=> $dataHF['judul'],
			    		'kode_dokumen'	=> $dataHF['kode_dokumen'],
			    		'revisi'		=> $dataHF['revisi'],
			    		'tanggal_revisi'=> $tanggalRevisi['TANGGAL_REVISI'],
			    		'unit_kerja'	=> $dataHF['unit_kerja'],
			    		'nama_company'	=> $namaCompany,
			    		'id_company'	=> $unitkerja['COMPANY'],
			    		'logo'			=> $logo,
			    		'tipe_dokumen'=> $tipeDokumen['TYPE'],
			    		'filename'		=> $dataHF['filename']
			    	);
		echo "<pre>";print_r($data);echo "</pre>";
		$pdf->setDataHeader($data);
		$pdf->setDataFooter($dataFooter);
		// echo "<pre>";print_r($pdf->getDataFooter());echo "</pre>";
		// echo "<pre>";print_r($pdf->getDataHeader());echo "</pre>";
		// $originalDate = $dataHF['tanggal_revisi'];
		// $newDate = date("d-m-Y", strtotime($originalDate));
		
		// $date = new DateTime($source);
		// echo $date->format('d.m.Y');
		// echo $newDate;
		$file = $fileName;
		// // echo "/assets/uploads/document/files/".$file;
		if (file_exists($dir."assets/uploads/document/files/".$file)){
		        $pagecount = $pdf->setSourceFile($dir."/assets/uploads/document/files/".$file);
		        $pdf->setTotalPage($pagecount);
		        echo "jjumlah halaman: ".$pagecount;
		} 
		else {
		    return FALSE;
		}
		// // // echo "total page =".$pdf->getTotalPage();
		// // // echo getcwd();
		// for($i = 1; $i <= $pagecount; $i++){
		//             $tpl = $pdf->importPage($i);
		//             $size = $pdf->getTemplateSize($tpl);
		//             $pageSize = "A4";
		//             if($size['h'] == 297.00008333333){ $pageSize = "A4";}
		//             elseif($size['h'] == 355.6){ $pageSize = "Legal";}
		//             elseif($size['h'] == 279.4){ $pageSize = "Letter";}
		//             elseif($size['h'] == 419.99958333333){ $pageSize = "A3";}

		//             if($i == 1 || $i == $pagecount){ // jika halaman 1
		//                 if($size['w'] <= $size['h']){ // jika portrait
		//                     $pdf->setOrientationForHeader('P');
		//                     $pdf->setOrientationForFooter('P');
		//                     $tempForFooter[$i] = 'P';
		                   
		//                     $pdf->addPage("P", $pageSize);
		//                     if($dataHF['id_tipe_dokumen'] != 5){
		//                     	$pdf->RotatedImage($dir.'assets/web/images/table-2.png', 5, 6, 200, 24, 0);
		//                 	}
		//                     $pdf->setOrientationForFooter('P');
		//                 }
		//                 else{ // jika landscape
		//                     $pdf->setOrientationForHeader('L');
		//                     $pdf->setOrientationForFooter('L');
		//                     $tempForFooter[$i] = 'L';
		//                     $pdf->addPage("L", $pageSize);
		//                     if($dataHF['id_tipe_dokumen'] != 5){
		//                     	$pdf->RotatedImage($dir.'assets/web/images/landscape-01.png', 5, 6, 287, 24, 0);
		//                     }
		//                     $pdf->setOrientationForFooter('L');
		//                 }
		//             }
		//             else{  // jika selain halaman 1
		//                 if($size['w'] <= $size['h']){  // jika portrait
		//                     $pdf->setOrientationForFooter($tempForFooter[$i-1]);
		//                     $pdf->setOrientationForHeader('P');
		//                     $tempForFooter[$i] = 'P';
		//                     $pdf->addPage("P", $pageSize);
		//                     if($dataHF['id_tipe_dokumen'] != 5){
		//                     	$pdf->RotatedImage($dir.'assets/web/images/table-2.png', 5, 6, 200, 24, 0);
		//                 	}
		//                 }
		//                 else{ // jika landscape
		//                     $pdf->setOrientationForFooter($tempForFooter[$i-1]); // ambil value footer dr halaman sebelumnya
		//                     $pdf->setOrientationForHeader('L');
		//                     $tempForFooter[$i] = 'L'; //set value footer untuk diambil di halaman berikutnya
		//                     $pdf->addPage("L", $pageSize);
		//                     if($dataHF['id_tipe_dokumen'] != 5){
		//                     	$pdf->RotatedImage($dir.'assets/web/images/landscape-01.png', 5, 6, 287, 24, 0);
		//                 	}
		//                 }
		//             }
		            
		//             $a =$pdf->useTemplate($tpl, 1000, 100, 0, 0, TRUE);
		// }
		// // // rename($dataHF['filename'],$dataHF['filename'].'.backup');
		// // // unlink('assets/uploads/document/files/'.$dataHF['filename'].'.backup');
		// // // unlink('./assets/uploads/document/files/'.$dataHF['filename']);
		// echo "output";
		// $pdf->Output($dir.''.$fileName, 'F');
	}
 ?>
function togglePlay(id){
    var player = document.getElementById(id);
    if (player.paused) 
      player.play(); 
    else 
      player.pause(); 
}

function textAreaAdjust(e){
	e.style.height = "1px";
	e.style.height = (e.scrollHeight)+"px";
}

$(document).ready(function() {
	$(".post-section").mark($('.header-search-input').val());
	if ($('#searchkm').length) {
		$(".post-section").mark($('#searchkm').val());
	}
	$('#btn_submit_modal, #btn_approve_modal, #btn_agree, #btn_add_reviewer_modal, #btn_save_form').on('click', function() {
        $('.loading').addClass('fa fa-spinner fa-spin');
        $('.loadings').show();
        $("button").prop("disabled", true);
        return true;
    });
    $('#set_attribute_form, #form_review_atribut').submit(function(){
    	$('.loading').addClass('fa fa-spinner fa-spin');
        $('.loadings').show();
        $("button").prop("disabled", true);
        return true;
    });
});
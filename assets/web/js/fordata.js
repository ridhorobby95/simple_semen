var formdata;

$(function() {
	formdata = $("#form_data");
	
    $("[data-type='list']").click(function() {
        location.href = list;
    });
    $("[data-type='add']").click(function() {
        location.href = g_page;
    });
    $("[data-type='edit']").click(function() {
        location.href = g_page + "/edit" + (key === true ? "" : "/" + key);
    });
    $("[data-type='undo']").click(function() {
        location.href = g_page + "/detail" + (key === true ? "" : "/" + key);
    });
    $("[data-type='save']").click(function() {
		if(cekRequired(formdata))
			goSubmit(formdata,"save");
    });
    $("[data-type='delete']").click(function() {
        var hapus = confirm("Apakah anda yakin akan menghapus data ini?");
        
        if(hapus)
            goSubmit(formdata,"delete");
    });
    $("[data-type='deletefile']").click(function() {
        var hapus = confirm("Apakah anda yakin akan menghapus file ini?");
        
        if(hapus) {
            formdata.find("#key").val($(this).attr("data-name"));
            goSubmit(formdata,"deletefile");
        }
    });
    $("[data-type='download']").click(function() {
        formdata.find("#key").val($(this).attr("data-name"));
        goSubmit(formdata,"download");
    });
    
    // pencarian data
    $("#form_cari").submit(function() {
        var key = $("#div_cari #key").val();
        
        // hanya jika pakai autocomplete
        if(key != "")
            location.href = g_extpage + "/" + key; // key-nya di-override
        
        return false;
    });
	
	// timepicker
	$(".timepicker").timepicker({
		showInputs: false,
		showMeridian: false,
		defaultTime: false
	});
	
	// sembunyikan header
	if(jQuery.trim($(".box-title").html()).length == 0)
		$(".box-header").html("");
});
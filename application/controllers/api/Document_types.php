<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }

//Ada tabel Role ItemRole Item

class Document_types extends PrivateApiController {

    protected $title = 'Document Type';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
    	$data = $this->model->order('urutan')->getAllJenis();
        if(!isset($data)){
            $message = 'data kosong';
            $json = null;
        }
        else{
            $message = 'Berhasil';
            $json = array();
            foreach ($data as $key => $value) {
                $json[] = array(
                    'key'   => $value['id'],
                    'row'   => $value['type'],
                    'value' => $value['id']
                );
            }
        }

        
    	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $json);
    }

    public function setting() {
        $this->load->model('Document_types_model');
        $data = $this->model->order('urutan')->getAllJenis();
        $this->data['data'] = $data;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success!!'), $this->data);
    }


    public function edit($id=NULL){
        if (isset($id))
            $data = $this->model->getDocumentType($id);
        else
            $data = array();
        $this->data['data'] = $data;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success!!'), $this->data);
    }

    public function ajaxSetUrutan(){
        $type_id = $this->input->post('id', true);
        $urutan = $this->input->post('urutan', true);
        $urutan_exist = $this->model->filter(" urutan=$urutan ")->getOne('id');
        if ($urutan_exist!=NULL){
            $urutan_now = $this->model->filter(" id=$type_id ")->getOne('urutan');
            $data = array();
            $data['urutan'] = $urutan_now;
            $this->model->updateJenis($urutan_exist, $data);
        }
        $data = array();
        $data['urutan'] = $urutan;
        $this->model->updateJenis($type_id, $data);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success!!'));
    }

}

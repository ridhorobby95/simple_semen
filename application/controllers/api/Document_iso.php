<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
class Document_iso extends PrivateApiController {

    public function __construct() {
        parent::__construct();
    }

    public function index($page=1) {
    	$allData = $this->model->getAll();
    	$data['data'] = $allData;

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function edit($id=NULL){
    	if (isset($id))
            $data = $this->model->getBy($id);
        else
            $data = array();

        $this->load->model('Document_iso_klausul_model', 'klausul');
        $data['klausul']=  Util::toList($this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"" ))->filter(" iso_id={$data['id']} ")->getAll(),'id');
        $data['var_klausul'] = Util::toMap($this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"" ))->getAll(),'id', 'klausul');

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function add(){
        $data = $this->input->post(null, true);
        $insert = $this->model->insert($data);
        if ($insert === true) {
        	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully added document iso'), $data);
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to add document iso';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;
        	$this->setResponse($this->setSystem(ResponseStatus::ERROR, $msg));
        }
    }

    public function update($id = null) {
        $data = $this->input->post(null, true);
        $update = $this->model->save($id, $data);
        if ($update === true) {
            $this->load->model('Document_iso_klausul_model', 'klausul');

            $klausul = $this->input->post('klausul_iso', true);
            $klausul_diff = array_diff(Util::toList($this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"" ))->filter(" iso_id=$id ")->getAll(),'id'),$klausul);
            $klausul_diff_str = implode(',',$klausul_diff);
            $this->klausul->filter(" iso_id=$id and id in ($klausul_diff_str) ")->delete();
            foreach ($klausul as $key => $value) {
                $is_exist = $this->klausul->filter(" iso_id=$id and id='$value' ")->getOne('1');
                if (!$is_exist){
                    $data['iso_id'] = $id;
                    $data['klausul'] = $value;
                    dbInsert('DOCUMENT_ISO_KLAUSUL', $data);
                }
            }
            
        	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully changed document iso'), $data);
        } else {
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change document iso';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        	$this->setResponse($this->setSystem(ResponseStatus::ERROR, $msg));
        }

    }

    public function delete($id) {
        $delete = $this->model->delete($id);

        if ($delete === true) {
        	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully deleted document iso'), $data);
        } else {
        	$this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to delete document iso'));
        }
    }

    public function editClause($id){
        if (isset($id))
            $data = $this->model->getBy($id);
        else
            $data = array();

        $this->load->model('Document_iso_klausul_model', 'klausul');
        $data['klausul']=  $this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"", "code" => "\"code\"" ))->filter(" iso_id={$data['id']} ")->order('code')->getAll();

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function formEditClause($id_clause){
        $this->load->model('Document_iso_klausul_model', 'klausul');

        $getData = $this->klausul->filter('id='.$id_clause)->getAll();
        $data = $getData[0];

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function updateClause(){
        $this->load->model('Document_iso_klausul_model', 'klausul');
        $clause = $this->input->post('clause');
        $code = $this->input->post('code');
        $data = array(
            'klausul'   => $clause,
            'code'      => $code
        );
        $id = $this->input->post('id_clause');
        $update = $this->klausul->update($id, $data);
        if ($update === true) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully changed Iso Clause Document'), $data);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed changed document iso clause'), $data);
        }
    }

    public function deleteClause($id_clause, $id_iso){
        $this->load->model('Document_iso_klausul_model', 'klausul');
        $delete = $this->klausul->filter('id='.$id_clause)->delete();
        if ($delete === true) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully deleted the document iso clause'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to delete document iso clause'));
        }
    }

}
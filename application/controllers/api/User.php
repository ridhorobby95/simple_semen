<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
class User extends PrivateApiController {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->model = $this->User_model;
    }

    public function index()
    {
        if ($this->input->get()){
            $limit = $this->input->get('limit', true);
            $offset = $this->input->get('offset', true);
            
            $search = strtoupper($this->input->get('search', true));
            $role = $this->input->get('role', true);
            $company = $this->input->get('company', true);

            if ($search) {
                $condition = "where (upper(\"username\") like '%$search%' or upper(\"name\") like '%$search%' or upper(\"eselon_name\") like '%$search%') ";
            }
            if ($role) {
                $condition .= " and \"role\"='$role'  ";

                if ($company && $role != 'X'){
                    $company = $this->input->get('company', true);
                    $condition .= " and \"company\"='".ltrim($company,'0')."' ";
                }
            }
            else{
                $condition .= " and \"role\"='X'  ";
            }
        }
        
        $data['data'] = $this->model->getAllUsersWithRole(TRUE, $condition, $limit, $offset);
        
        $data['role'] = array(0=>'All Role',
                                    Role::ADMINISTRATOR => "Administrator",
                                    Role::ADMIN_UNIT => "Admin Unit",
                                    Role::DOCUMENT_CONTROLLER => "Document Controller",
                                    Role::DRAFTER => "Drafter",
                                    Role::EKSTERNAL => "Eksternal"
                                    );

        $this->load->model('Unitkerja_model');
        // $where = '';
        // if($this->user['role'] == Role::EKSTERNAL){
        //     $where = implode('\',\'', SessionManagerWeb::getCompanyAccess());
        //     $where = "and company in('".$where."')";
        // }
        $company = $this->Unitkerja_model->show_sql(false)->getAll(" and parent='0' and LENGTH(ltrim(id, '0'))<5 $where ");
        foreach ($company as $comp) {
            $data['company'][ltrim($comp['id'], '0')] = $comp['name']; 
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function add()
    {
        $this->edit();
    }

    public function edit($id = null)
    {
        if ($id) {
            $data = $this->model->get($id);
            $this->load->model('Karyawan_model');
            $karyawan = $this->Karyawan_model->show_sql(true)->column(array('tgl_masuk' => 'tgl_masuk', 'tgl_pensiun' => 'tgl_pensiun'))->getBy($data['karyawan_id']);
            $data['tgl_masuk'] = $karyawan['TGL_MASUK'];
            $data['tgl_pensiun'] = $karyawan['TGL_PENSIUN'];
            $access = dbGetRow('select company, document_type from user_eksternal where user_id='.$id);
            $data['company_access'] = json_decode($access['COMPANY']);
            $data['document_type_access'] = json_decode($access['DOCUMENT_TYPE']);
            $company = ltrim($this->model->filter("where \"user_id\"='$id' ")->getOneFilter('company'), '0');
            $data['company'] = $company;
            if ($this->user['role'] == Role::ADMIN_UNIT){
                if ($this->user['company'] != $company){
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Cannot edit this user! You only can edit user in your company!"));
                }
            }
        }
        if ($data['photo']!=NULL)
            $data['photo'] = Image::getLocation($data['id'], Image::IMAGE_THUMB, $data['photo'], 'users/photos');
        $this->data['data'] = $data;
        $this->data['a_role'] = array(
            Role::DRAFTER => 'Drafter',
            Role::ADMINISTRATOR => 'Administrator',
            Role::ADMIN_UNIT => 'Admin Unit',            
            Role::DOCUMENT_CONTROLLER => 'Document Controller',
            Role::EKSTERNAL => 'Eksternal'
        );

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $this->data);
    }

	public function me()
	{
        $this->load->model('Karyawan_model');

        $data 		= $this->model->get($this->user['id']);
        $karyawan 	= $this->Karyawan_model->show_sql(true)->column(array('tgl_masuk' => 'tgl_masuk', 'tgl_pensiun' => 'tgl_pensiun'))->getBy($data['karyawan_id']);
        $data['tgl_masuk'] 		= $karyawan['TGL_MASUK'];
        $data['tgl_pensiun'] 	= $karyawan['TGL_PENSIUN'];
        $access = dbGetRow('select company, document_type from user_eksternal where user_id='.$id);
        $data['company_access'] = json_decode($access['COMPANY']);
        $data['document_type_access'] = json_decode($access['DOCUMENT_TYPE']);
        $company = ltrim($this->model->filter("where \"user_id\"='$id' ")->getOneFilter('company'), '0');
        $data['company'] = $company;
        if ($this->user['role'] == Role::ADMIN_UNIT){
            if ($this->user['company'] != $company){
                $msg = "Cannot edit this user! You only can edit user in your company!";
            	$this->setResponse($this->setSystem(ResponseStatus::FAILED, $msg), array());
            }
        }

        if ($data['photo']!=NULL)
            $data['photo'] = Image::getLocation($data['id'], Image::IMAGE_THUMB, $data['photo'], 'users/photos');

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
	}

    /**
     * Update data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
	public function update($id)
	{
        $data = $this->postData;
        $isself = (empty($id) ? true : false);

        if ($isself) {
            $id = $this->user['id'];
            $data['username'] = $this->user['username'];
            unset($data['status']);
        }
        if ($data['karyawan_id']!=NULL) {
            $data['karyawan_id'] = $data['karyawan_id'];
        }
        if ($_FILES['photo']['error'] == UPLOAD_ERR_OK)
            $data['photo'] = $_FILES['photo']['name'];
        else
            unset($_FILES['photo']);

        if($data['role'] == Role::EKSTERNAL){
            $karyawan = array(
                'tgl_masuk'     => strtoupper("'".$data['tgl_masuk']."'"),
                'tgl_pensiun'   => strtoupper("'".$data['tgl_pensiun']."'")
            );
            $user_eksternal = array(
                'company'       => json_encode($data['company_access']),
                'document_type' => json_encode($data['type_access'])
            );
            unset($data['tgl_masuk']);
            unset($data['tgl_pensiun']);
            unset($data['company_access']);
            unset($data['type_access']);
        }
        $update = $this->model->save($id, $data, TRUE);

        if ($update === true) {
            if($data['role'] == Role::EKSTERNAL){
                dbUpdate("karyawan", $karyawan, "id='".$data['karyawan_id']."'");
             	dbUpdate("user_eksternal", $user_eksternal, "user_id='".$id."'");
            }

            $msg = 'Successfully changed profile';
        	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $msg), $data);

        } else {

            $msg = 'Cannot changed profile';
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $msg), array());

        }
	}


}
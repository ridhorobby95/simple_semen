<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Document_iso_klausul extends PrivateApiController {

    protected $title = 'Iso Dokumen';

    public function ajaxGetKlausul($iso_id){
        // $data = $this->postData;

        // $iso_id = $data['iso_id'];
        // $iso_id = implode(',',$iso_id);
        $iso_id = str_replace('_', ',', $iso_id);
        $this->data = $this->model->filter(" iso_id in ($iso_id) ")->getAll();
        if($klausul){           
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
    }


    public function uploadKlausul(){
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        if (is_dir($path_source.'document/master/klausul/'.$id)) {
           $files = glob($path_source.'document/master/klausul/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."document/master/klausul/".$id."/".$metadata;

        if (!file_exists($file)){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to migrate document. Metadata not exist'));
        }
        
        $this->load->library('Libexcel');
        $data = array();
        $index = 0;
        $tmpfname = $file;
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getActiveSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $this->load->model('Document_iso_klausul_model');
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $clause = $worksheet->getCell('A'.$row)->getValue();
            $code = $worksheet->getCell('B'.$row)->getValue();
            $dataExist = $this->Document_iso_klausul_model->filter(" \"code\" = '".$code."' ")->getAll();
            if($dataExist == NULL){
                $data = array(
                        'iso_id'    => $this->input->post('id_iso'),
                        'klausul'   => $clause,
                        'code'      => $code
                );
                dbInsert('document_iso_klausul', $data);
            }
        }

        $this->_deleteFiles($path_source."document/master/klausul/".$id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully Add Iso Clause Document'), $this->data);
    }

}

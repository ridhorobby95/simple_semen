<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }

//Ada tabel Role ItemRole Item

class Buletin_iso extends PrivateApiController {

    protected $title = 'Buletin_iso';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index($unitkerja=null) {
        if($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
            $data = $this->model->show_sql(true)->getAllTree();
        }elseif($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] != '2000'){
            $data = $this->model->show_sql(true)->getAllTree($this->user['company'], 'company');
        }else{
            $data = $this->model->show_sql(true)->getAllTree($unitkerja, 'unitkerja');
        }
    	// $data = $this->model->show_sql(false)->getAllTree($unitkerja, 'unitkerja');
        if(!isset($data)){
            $message = 'data kosong';
            $this->data = null;
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $message), $this->data);
        }
        else{
            $message = 'Berhasil';
            $this->data = $data;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data);
        }
    	
    }

    public function detailSub($id, $level, $unitkerja){
        if($level == 1){
            if($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
                $data = $this->model->show_sql(false)->getDetailTree($id,$level);
            }elseif($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] != '2000'){
                $data = $this->model->show_sql(true)->getDetailTree($id,$level,$this->user['company'],'company');
            }else{
                $data = $this->model->show_sql(true)->getDetailTree($id,$level,$unitkerja,'unitkerja');
            }
            
        }
        else{
            $data = $this->model->show_sql(false)->getDetailTree($id,$level);
        }
        if(!isset($data)){
            $message = 'data kosong';
            $this->data = null;
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $message), $this->data);
        }
        else{
            $message = 'Berhasil';
            $this->data = $data;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data);
        }
        

    }

    public function postKategori($level=Buletin_iso_model::LEVEL_KATEGORI, $id=null, $codeKategori=null){ // sub kategori 2, dan 3 juga disini
        if($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
            if($level == Buletin_iso_model::LEVEL_KATEGORI){
                $postData = $this->postdata;

                // insert kategori
                $codeKategori = 'KAT'.$this->model->getLastCode();
                $dataKategori = array(
                    'nama'  => $postData['kategori'],
                    'level' => Buletin_iso_model::LEVEL_KATEGORI,
                    'code'  => $codeKategori
                );
                $insertKategori = $this->model->create($dataKategori, TRUE, TRUE);
                if($insertKategori){
                    $this->postSubKategori($insertKategori, $codeKategori, $postData['subkategori']);
                }

            }
            elseif($level == Buletin_iso_model::LEVEL_SUBKATEGORI){
                $this->postSubKategori($id, $codeKategori, $postData['subkategori']);
            }
            elseif($level == Buletin_iso_model::LEVEL_SUBKATEGORI_2){
                $this->postSubKategori($id, $codeKategori, $postData['subkategori2']);
            }
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "You can't access this feature"));
        }
        
        
    }

    public function postSubKategori($idKategori, $codeKategori, $data=null){
        if($this->user['role'] == Role::DOCUMENT_CONTROLLER){
            if($data == null){
                $data = $this->$postData;
            }
            $dataSubKategori = $data['subkategori'];
            foreach ($dataSubKategori as $key => $value) {
                $dataSubKategori2 = $value['subkategori2'];
                unset($dataSubKategori[$key]['subkategori2']);
                $unitkerjaSubKategori = implode(',', $value['unitkerja']);
                $company = array();

                //membuat variabel access_company dari list unitkerja/////
                foreach ($value['unitkerja'] as $keyUnit => $valueUnit) {
                    $getCompany = dbGetOne("select company from unitkerja where id=".$valueUnit);
                    if(!in_array($getCompany, $company)){ // jka company tidak ada didalam array, maka tambahkan
                        $company[] = $getCompany;
                    }
                }
                $companySubKategori = implode(',', $company);
                ///////////////////////////////////////////////////

                $codeSubKategori = $codeKategori.'/SUB'.$this->model->getLastCode(Buletin_iso_model::LEVEL_SUBKATEGORI);
                $dataInsertSubKategori = array(
                    'nama'  => $value['name'],
                    'level' => Buletin_iso_model::LEVEL_SUBKATEGORI,
                    'parent'=> $idKategori,
                    'code'  => $codeSubKategori,
                    'access_unitkerja'  => $unitkerjaSubKategori,
                    'access_company'    => $companySubKategori
                );
                $insertSubKategori = $this->model->create($dataInsertSubKategori, TRUE, TRUE);
                if($insertSubKategori){
                    $this->postSubKategori2($insertSubKategori, $codeSubKategori, $dataSubKategori2);
                }
            }
        }
        

    }

    public function postSubKategori2($idSubKategori, $codeSubKategori, $dataSubKategori2=null){
        if($this->user['role'] == Role::DOCUMENT_CONTROLLER){
            if($dataSubKategori2 == null){
                $data = $this->$postData;
            }
            $codeSubKategori2 = $codeSubKategori.'/SUBL'.$this->model->getLastCode(Buletin_iso_model::LEVEL_SUBKATEGORI_2);
            foreach ($dataSubKategori2 as $key2 => $value2) {
                $dataInsertSubKategori2 = array(
                    'nama'  => $value2,
                    'level' => Buletin_iso_model::LEVEL_SUBKATEGORI_2,
                    'parent'=> $idSubKategori,
                    'code'  => $codeSubKategori2
                );
                $insertSubKategori2 = $this->model->create($dataInsertSubKategori2, TRUE, TRUE);
            }
        }
        
    }

    public function postIso(){
        $data = $this->postdata;
        for ($i=0; $i <= count($data['nama']) ; $i++) {
            $dataKategori = dbGetRow("select \"LEVEL\" as \"level\", code as \"code\" from Buletin_iso where id=".$data['id_kategori'][$i]);
            $idFileOld = dbGetOne("select id from Buletin_iso_file where code='".$data['old_code'][$i]."'");
            $code = $dataKategori.'FIL'.$this->model->getLastCode();
            $name = $_FILES['file']['name'][$i];
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $dataInsert = array(
                'nama'          => $data['nama'][$i],
                'code'          => $code,
                'filename'      => str_replace('/', '_', $code).'.'.$type,
                'oriname'       => $name,
                'is_obsolete'   => 0,
                'id_kategori'   => $data['id_kategori'][$i],
                'periode'       => $data['periode'][$i],
                'retention'     => $data['retention'][$i],
                'parent'        => $idFileOld
            );
            $insert = $this->model->create($dataInsert, TRUE, TRUE);
            if($insert){
                $_FILES['userfile']['name']= $_FILES['file'][$i]['name'];
                $_FILES['userfile']['type']= $_FILES['file'][$i]['type'];
                $_FILES['userfile']['tmp_name']= $_FILES['file'][$i]['tmp_name'];
                $_FILES['userfile']['error']= $_FILES['file'][$i]['error'];
                $_FILES['userfile']['size']= $_FILES['file'][$i]['size'];
                File::uploadFile('userfile', $insert, str_replace('/', '_', $code).'.'.$type, 'iso');
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to add iso'), $id_iso);
            }else{
                this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to add iso'));
            }
        }
    }

    public function updateKategori($id, $level=Buletin_iso_model::LEVEL_KATEGORI){
        if($level == Buletin_iso_model::LEVEL_KATEGORI && $this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
            $data = $this->postData;
            $update = $this->model->save($id, $data, TRUE);
            if($update){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success update kategori'), $id_iso);
            }
            else{
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed update kategori'));
            }
        }
        elseif($level != Buletin_iso_model::LEVEL_KATEGORI && && $this->user['role'] == Role::DOCUMENT_CONTROLLER){
            $data = $this->postData;
            $update = $this->model->save($id, $data, TRUE);
            if($update){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success update kategori'), $id_iso);
            }
            else{
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed update kategori'));
            }
        }
    }

    public function isObsolete($id_iso){
        $data = array('is_obsolete' => 1);
        $update = $this->model->save($id, $data, TRUE);
        if($update){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to obsolete file'), $id_iso);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to obsolete file'));
        }
    }

    public function detailKategori($id){
        $data = $this->model->getBy($id);
        if(!isset($data)){
            $message = 'data kosong';
            $this->data = null;
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $message), $this->data);
        }
        else{
            $message = 'Berhasil';
            $this->data = $data;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data);
        }
    }

    public function detailFile($id){
        $data = $this->model->getFileBy($id);
        if(!isset($data)){
            $message = 'data kosong';
            $this->data = null;
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $message), $this->data);
        }
        else{
            $message = 'Berhasil';
            $this->data = $data;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data);
        }
    }

    public function downloadFile($filename, $oriname){
        // download baru 
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'].'iso/';
        $fsize = filesize($path.$filename);
        $fname = basename($oriname);
        header('Content-Description: File Transfer'); 
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header('Expires: 0'); 
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
        header('Pragma: public'); 
        header("Content-disposition: attachment; filename=\"" . $fname . "\""); 
        header("Content-Length: ".$fsize);
        ob_clean();
        flush();
        // echo readfile($file['link']);
        echo readfile($path.$filename);

    }

    

    



}

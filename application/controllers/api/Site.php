<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
class Site extends PublicApiController {

    // function getBearerToken() {
    //     if ($_SERVER["HTTP_AUTHORIZATION"]) {
    //         list($type, $data) = explode(" ", $_SERVER["HTTP_AUTHORIZATION"]);
    //         if ($type==  "Bearer") {
    //             return $data;
    //         } else {
    //             return null;
    //         }
    //     } else {
    //         return null;
    //     }
    // }

    

    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $device = $this->input->post('device');
        // $username = 'yuliana.wulandari';
        // $password = '1234';
        // $device = array(
        //     'secure_id'    => 'ASDSA',
        //     'name'          => 'xiaomi',
        //     'manufacture'         => 'xiaomi',
        //     'model'         => 'asus',
        //     'product'       => 'AVASD',
        //     'serial'        => '093204324hsfsd',
        //     'hardware'      => 'QUALCOMM',
        //     'version'       => '22',
        //     'reg_id'        => '9283U4324'

        // );
        $device = '{
          "brand": "HUAWEI",
          "hardware": "sc8830",
          "manufacture": "HUAWEI",
          "model": "BGO-DL09",
          "name": "hwbgo",
          "product": "BGO",
          "reg_id": "fZ0BMeTQEu4:APA91bF13iUq7L4osGW7aqgJ9X55MIrefQ-R8bqJqwFlY8Y4eiZ7H5W3Zbc2aK2Eqirc1WCttgLFm5l7OqOPkPWk3RAWR8M-yU9_ABXUWesIgiA5egTn87Hp5H-lbyeJ1G2XE1SlwuLR",
          "secure_id": "761e6ebd23ef8dff",
          "serial": "T6RFG16B16000762",
          "version": "23"
        }';
        // $devices = json_encode($device);
        // echo "<pre>";print_r($devices);die();
        // $data = AuthManager::login($username, $password, $devices);
        
        $data = AuthManager::login($username, $password, $device);
        if (is_string($data)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $data));
        } else {
            list($message, $token, $user) = $data;
            $this->load->model('User_token_model');
            // $user['userToken'] = $this->User_token_model->with('Device')->get_by(array('token' => $token));
            // $this->systemResponse->token = $token;
            $user['token'] = $token;
            // echo "<pre>";print_r($user);die();
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $user);
        }
    }


    public function resetPassword(){
        $data = $this->postData;
        $username = explode('@', $data['email']);
        if(strtoupper($username[1]) != '@SEMENINDONESIA.COM'){
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
            $randomString = ''; 
          
            for ($i = 0; $i < 15; $i++) { 
                $index = rand(0, strlen($characters) - 1); 
                $randomString .= $characters[$index]; 
            }
            $insertData = array(
                'username'    => $username[0],
                'password'    =>  $randomString,
                'is_agree'    => 0

            );
            $this->load->model('Change_password_model');
            $insert = $this->Change_password_model->create($insertData, true, true);
            if($insert){
                $user_id = dbGetOne("select id from users where email ='".strtoupper($data['email'])."'");
                $sender_id = dbGetOne("select id from users where role='".Role::ADMINISTRATOR."'");
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_CHANGEPASSWORD, $insert,$sender_id, true, $user_id);
            }
        }
        
    }

    public function ConfirmResetPassword($idReset,$user_id){
        $agree = dbGetOne("select is_agree from change_password where id=".$idReset);
        if($agree == 0){
            $agree = dbUpdate('change_password', array('is_agree' => 1), "id=$idReset");
            if($agree){
                $password = dbGetOne("select password from change_password where id=".$idReset);
                $users['password_salt'] = SecurityManager::generateAuthKey();
                $users['password'] = SecurityManager::hashPassword($password, $users['password_salt']);
                dbUpdate('users', $users, "id=$user_id");
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success change password, please login"));
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "You're Request is Expired"));
        }
        
    }
    public function dashboard() {
        $id = '61273';
        $role = 'C';
        $company = '2000';
        $this->load->model('Document_model');
        // echo "<pre>";print_r($this->user);die();
        $data = $this->Document_model->filter($filter." and \"status\"!='C' ")->getDocumentNeedAction($id, $role, $company);
        if(!isset($data)){
            $message = 'data kosong';
        }
        else{
            $message = '';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat postingan'));

        // if ($insert === TRUE) {
        //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat postingan'));
        // } elseif (is_string($insert)) {
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        // } else {
        //     $validation = $this->model->getErrorValidate();
        //     if (empty($validation)) {
        //         $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat postingan'));
        //     } else {
        //         $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
        //     }
        // }
    }

    // public function getDocProsesBisnis(){
    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
    //     $this->load->model('Document_prosesbisnis_model');
    //     $data = $this->Document_prosesbisnis_model->getAll();

    //     $dataList = array();
    //     foreach ($data as $k_jenis => $v_jenis) {
    //         $dataList[]=array('value'=>$v_jenis['id'],'label'=>$v_jenis['prosesbisnis']);
    //     }
    //     if(!isset($dataList)){
    //         $message = 'data kosong';
    //     }
    //     else{
    //         $message = '';
    //     }
    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
    // }

    // public function getCodeBussProc($id){
    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
    //     $this->load->model('Document_prosesbisnis_model');
    //     $data = $this->Document_prosesbisnis_model->getBy($id);

    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);
    // }

    // public function getCodeWorkUnit($id){
    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
        
    //     $this->load->model('Unitkerja_model');
    //     $data = $this->Unitkerja_model->getBy($id);

    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);
    // }

    // public function getDocType(){

    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
    //     $this->load->model('Document_types_model');
    //     $this->load->model('Tingkatan_document_types_model');
    //     $tingkatan = $this->User_model->getEselon($user['id']);
    //     $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
    //     $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

    //     $this->load->model("Document_model");
    //     $docType = array();
    //     foreach ($jenis as $k_jenis => $v_jenis) {
    //         if ($v_jenis['id']!=Document_model::BOARD_MANUAL){
    //             $docType[]=array('value'=>$v_jenis['id'],'label'=>$v_jenis['type']);
    //             // unset($jenis[$k_jenis]);
    //         }
    //     }
    //     if(!isset($docType)){
    //         $message = 'data kosong';
    //     }
    //     else{
    //         $message = '';
    //     }
    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $docType);
    // }

    // public function getWorkUnit(){
    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
    //     $this->load->model('User_model');
    //     $this->load->model('Unitkerja_model');

    //     $myunitkerja = $this->User_model->getBy($user['id'],'unitkerja_id');

    //     $level = array("'DEPT'", "'BIRO'", "'SECT', 'DIR', 'KOMP'");
    //     // $level = array("'DEPT'", "'BIRO'", "'SECT' ");
    //     $levels = implode(',',$level);
    //     $this->load->model('User_model');
    //     if ($myunitkerja==NULL)
    //         return NULL;
    //     $parent = $this->User_model->getBy($user['id'], 'unitkerja_parent');
    //     $unitkerja = array();
    //     $unitkerja[$myunitkerja] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($myunitkerja, 'name');
    //     $unitkerja[$parent] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'name');
    //     while ($parent!='0' and $parent!=NULL){
    //         $parent = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'parent');
    //         if ($parent=='0')
    //             continue;
    //         $unitkerja[$parent] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'name');
    //     }
    //     $dataList = array();
    //     foreach ($unitkerja as $key => $value) {
    //         if ($value==NULL)
    //             unset($unitkerja[$key]);
    //         else
    //             $dataList[] = array('value'=>$key,'label'=>$value) ;  
    //     }

        
    //     if(!isset($dataList)){
    //         $message = 'data kosong';
    //     }
    //     else{
    //         $message = '';
    //     }
    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
        
    // }


    // public function getClauseIso($arrId){

    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
    //     $iso_id = explode('_',$arrId);
    //     $iso_id = implode(',',$iso_id);
        
    //     $this->load->model('Document_iso_klausul_model');
    //     $data = $this->Document_iso_klausul_model->filter(" iso_id in ($iso_id) ")->getAll();
        

    //     $dataList = array();
    //     foreach ($data as $key) {
    //         $dataList[] = array('value'=>$key['id'],'label'=>$key['klausul']) ;  
    //     }


    //     if(!isset($dataList)){
    //         $message = 'data kosong';
    //     }
    //     else{
    //         $message = '';
    //     }
    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
    // }

    // public function getReviewer(){
    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
    //     $this->load->model('User_model');
    //     $mycompany = $this->User_model->getBy($user['id'], 'company');
    //     $reviewer = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\""))->getAll(" where \"company\"='$mycompany' ");
    //     $dataList = array();
    //     foreach ($reviewer as $key => $row) {
    //         $dataList[] = array('value'=>$row['user_id'],'label'=>$row['unitkerja_name'].' - '.$row['name']);
    //     }

    //     if(!isset($dataList)){
    //         $message = 'data kosong';
    //     }
    //     else{
    //         $message = '';
    //     }
    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);

    // }

    // public function getCreator($type,$unitkerja){
    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());
    //     $data = array();

    //     $this->load->model('User_model');
    //     $this->load->model("Unitkerja_model");
    //     $myunitkerja = $this->User_model->getBy( $user['id'],'unitkerja_id');
    //     $this->load->model('Document_types_model');
    //     $this->load->model('Tingkatan_document_types_model');
    //     $tingkatan = $this->User_model->getEselon($user['id']);
    //     $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
    //     $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

    //     $this->load->model("Document_model");
    //     foreach ($jenis as $k_jenis => $v_jenis) {
    //         if ($v_jenis['id']==Document_model::BOARD_MANUAL){
    //             unset($jenis[$k_jenis]);
    //         }
    //     }
    //     $creators = $this->Unitkerja_model->filter('')->getCreator($myunitkerja, $jenis);

    //     $cek = array();
    //     $no = 0;
    //     foreach ($creators[$unitkerja][$type] as $key => $value) {
    //         if ($key != null){
    //             $data[$no]['value'] =$key;
    //             $data[$no]['label'] = $value;
    //             $cek[$no] = $key;
    //             $no++;    
    //         }
            
    //     }


    //     // $this->load->model('Atasan_model');
    //     $atasan_nopeg = $this->Unitkerja_model->getAtasanByLevel($unitkerja, $user['id']);

    //     if (!in_array($atasan_nopeg, $cek)){
    //         $name = $this->User_model->filter("where \"user_id\"='".$atasan_nopeg."'")->getOneFilter("name");

            
    //         if ($atasan_nopeg!=NULL){
    //             $data[$no]['value'] = $atasan_nopeg;
    //             $data[$no]['label'] = $name;
    //         }
            
    //     }
        

    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);

        
    // }

    // public function getDirut($company='2000'){
    //     $sql = "select id from unitkerja where lower(name) like '%president directorate%' and company='$company' and \"LEVEL\"='DIR' and sysdate < end_date order by id desc";
    //     $unitkerja_dirut = dbGetOne($sql);
    //     $sql_user = " select \"user_id\" from users_view where \"unitkerja_id\"='$unitkerja_dirut' and \"is_chief\"='X' and \"is_active\"=1 ";
    //     $dirut_id = dbGetOne($sql_user);
    //     return $dirut_id;
    // }


    // public function getApprover($creator_id,$type_id){
    //     list($token, $user) = AuthManager::validateToken('bea61bd3c89aa70566da84aa96eb19cd67b07395cef92859853c0886060ee98e4fe00bfcf3b34ca0dc0bef5c43b19398');
    //     $this->load->model('User_model');
    //     $this->load->model('Atasan_model');
    //     $data = array();
    //     $creator_nopeg = $this->User_model->getBy($creator_id, 'karyawan_id');

        
    //     $mycompany = $this->User_model->getBy($user['id'], 'company');
        
    //     $atasan = $this->Atasan_model->filter(" where k_nopeg='$creator_nopeg' ")->getBy();
    //     $atasan1_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan1_nopeg']."' ")->getOneFilter('user_id');
    //     $atasan2_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan2_nopeg']."' ")->getOneFilter('user_id');


        
    //     $data['approver'][0]['value'] = $atasan1_id;
    //     if ($user['id']==$creator_id){
    //         $data['approver'][0]['value'] = $atasan1_id;
    //     }

    //     // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data['approver']);        
        
    //     $data['approver'][0]['label'] = $this->User_model->getBy($data['approver'][0]['value'], 'name');
    //     if ($type_id=='2'){
    //         $data['approver'][1]['value'] = $this->getDirut($mycompany);
    //         $data['approver'][1]['label'] = $this->User_model->getBy($data['approver'][1]['value'], 'name');
    //         if ($data['approver'][0]['value']==$data['approver'][1]['value']){
    //             unset($data['approver'][1]);
    //         }
    //     }
    //     // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data['approver']);     
    //     if ($data['approver'][0]['value']==null){
    //         if ($data['approver'][1]['value']==null){
    //             $data = array();
    //         } else {
    //             $data['approver'][0] = $data['approver'][1];
    //             unset($data['approver'][1]); 
    //         }
            
    //     }

    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data['approver']);        
    // }

    // public function getRelatedWorkUnit(){
    //     list($token, $user) = AuthManager::validateToken(self::getBearerToken());

    //     $this->load->model('User_model');
    //     $mycompany = $this->User_model->getBy($user['id'], 'company');
    //     $this->load->model('Unitkerja_model');
    //     $unitkerja=$this->Unitkerja_model->order('name')->getAllList(" company='$mycompany' ");
    //     $dataList = array();
    //     foreach ($unitkerja as $key => $value) {
    //         $dataList[] = array('value'=>$key,'label'=>$value);
    //     }
    //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);        
    // }

    

}

?>
<?php
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Quick_link extends PrivateApiController {

    protected $title = 'Custom Quick Link';

    public function index() {
        if ($this->user['role'] == Role::ADMIN_UNIT or $this->user['role'] == Role::ADMINISTRATOR){
            $buttons[] = array('label' => 'Add Menu', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        }
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['quicklink'] = $this->model->show_sql(false)->getAll();
        $this->data['buttons'] = $buttons;
        // echo "<pre>";print_r($this->data['quicklink']);die();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
    }

    public function add() {
        parent::add();
    }

    public function edit($id = null) {

        $this->data['title'] = $this->data['title'];

        parent::edit($id);
        
        if (isset($id)) {
            $data = $this->model->getBy($id);
        }
        else{
            $data = array();
        }

        $this->data['a_icon'] = array(
            'home' => 'home',
            'user' => 'user',
            'dashboard' => 'dashboard',
            'megaphone' => 'megaphone',
            'file' => 'file',
            'list' => 'list',
            'book' => 'book',
            'stats' => 'stats',
        );

        $this->data['data'] = $data;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        // $data = $this->input->post(null, true);
        $data['name'] = $this->input->post("name", true);
        $data['link'] = $this->input->post("link", true);
        $data['description'] = trim($this->input->post("description", true));
        $data['icon'] = $this->input->post("icon", true);
        
        // echo "<pre>";print_r($data);die();

        $insert = dbInsert('quick_links', $data);
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully add Menu'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Successfully add Menu'));
        }

    }

    public function update($id = null) {
        $data['name'] = $this->input->post("name", true);
        $data['link'] = $this->input->post("link", true);
        $data['description'] = trim($this->input->post("description", true));
        $data['icon'] = $this->input->post("icon", true);
        $data['status'] = $this->input->post("status", true);
        
        // echo "<pre>";print_r($data);die();
        $update = dbUpdate('quick_links', $data, "id='$id'");
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully add Menu'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Successfully add Menu'));
        }
    }

    public function delete($id) {
        if($id){
            $delete = dbQuery("delete from quick_links where id=".$id);
            if($delete){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully delete Menu'));

            }
            else{
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to delete Menu'));
            }
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to delete Menu'));
        }
    }

}

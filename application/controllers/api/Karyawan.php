<?php
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Karyawan extends PrivateApiController {

    protected $title = 'Employee';

    public function index() {
        // if ($_GET){
        //     $param = strtoupper($this->input->get('search', true));
        //     $condition = "where (upper(\"name\") like '%$param%' or upper(\"karyawan_id\") like '%$param%' ) ";
        //     if ($_GET['company'] and $_GET['company']!='0'){
        //         $company = $this->input->get('company', true);
        //         $condition .= " and \"company\"='".ltrim($company,'0')."' ";
        //     }
        // }

        // $this->load->model("User_model");
        // $users = $this->User_model->getAllUsersWithRole(TRUE, $condition);
        // // $data = $this->model->getAll($condition);
        // $data = array();
        // foreach ($users as $key => $value) {
        //     $data[$key] = $this->model->getBy($value['karyawan_id']);
        //     $data[$key]['company'] = $value['company'];
        //     $data[$key]['status'] = $this->model->getStatus($data[$key]['status']);
        // }

        // $condition = "where rownum<51";
        // if ($_GET){
        //     $param = strtoupper($this->input->get('search', true));
        //     $condition .= "and (upper(k.name) like '%$param%' or upper(k.id) like '%$param%' ) ";
        //     if ($_GET['company'] and $_GET['company']!='0'){
        //         $company = $this->input->get('company', true);
        //         $condition .= " and u.company='".ltrim($company,'0')."' ";
        //     }
        // }

        $data = $this->model->table("karyawan k")
                            ->with("left join jabatan j on j.id=k.jabatan_id
                                    left join unitkerja u on u.id=j.unitkerja_id")
                            ->column(array(
                                "k.id" => "\"id\"", 
                                "k.name" => "\"name\"", 
                                "k.jabatan_id" => "\"jabatan_id\"", 
                                "k.tgl_masuk" => "\"tgl_masuk\"", 
                                "k.tgl_pensiun" => "\"tgl_pensiun\"", 
                                "k.email" => "\"email\"", 
                                "k.status" => "\"status\"",
                                "k.eselon_code" => "\"eselon_code\"",
                                "u.company" => "\"company\""
                            ))
                            ->filter($condition)
                            ->order("k.id")
                            ->show_sql(false)
                            ->getAllFilter();
        if($data){
            $this->data['data']   =$data;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);

        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
        // $this->data['title'] = 'Employee List';

        // $this->load->model('Unitkerja_model');
        // $this->data['company'] = $this->Unitkerja_model->getAllList(" \"LEVEL\"='COMP' and parent=0 and id<10000 ");
        // $this->data['company'][0] = "All Company";

        // tombol
        // $buttons = array();
        // if ($this->user['role'] == Role::ADMIN_UNIT or SessionManagerWeb::isAdministrator()){
        //     $buttons[] = array('label' => ' Import Excel', 'type' => 'secondary', 'icon' => 'file-excel-o', 'click' => 'goExport()');
        //     $buttons[] = array('label' => 'Add Employee', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        // }
        // $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        // $this->data['buttons'] = $buttons;
        
        // $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Fungsi Delete user
     */
    public function delete($id) {
        $ok = false;
        $data['status'] = Status::VOID;
        $update = $this->model->update($id, $data, TRUE);
        if ($update) {
            $ok = true;
            $msg = 'Successfully deleted user';
        } else
            $msg = 'Failed to delete user';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        
        redirect($this->ctl);
    }

    /**
     * Halaman profil user
     */
    public function me() {
        // tombol
        $buttons = array();
        $buttons['save'] = array('label' => 'Save', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Edit Profile';

        $this->edit($this->user['id']);
    }

    /**
     * Halaman tambah data
     * @param int $userid
     */
    public function add($userid = null) {
        parent::add();
    }

    /**
     * Halaman edit user
     * @param int $id
     */
    public function edit($id = null) {

        $this->data['title'] = $this->data['title'];

        parent::edit($id);
        
        if (isset($id)) {
            $data = $this->model->getBy($id);
            $this->load->model("User_model");
            $data['company'] = $this->User_model->filter("where \"karyawan_id\"='$id' ")->getOneFilter('company');
            if ($this->user['role'] == Role::ADMIN_UNIT){
                if ($this->user['company']!=$data['company']){
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Cannot edit this employee! You only can edit employee in your company!'));
                }
            }
        }
        else{
            $data = array();
        }
        $this->data['data'] = $data;

        // Eselon
        $eselons = $this->model->column(array("eselon_code"=>"\"eselon_code\"", "eselon_name" =>"\"eselon_name\""))->group_by("group by eselon_code, eselon_name")->order("eselon_code nulls first")->getAllFilter();
        
        $this->data['eselons'] = Util::toMap($eselons, "eselon_code", "eselon_name");
        unset($this->data['eselons']['00']);

        $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Success get employee'), $this->data);

    }

    /**
     * Membuat data baru
     */
    public function create() {
        // $data = $this->input->post(null, true);
        $data['id'] = $this->input->post("id", true);
        $data['name'] = $this->input->post("name", true);
        $data['email'] = strtoupper($this->input->post("email", true));
        
        
        // Eselon
        $data['eselon_code'] = $this->input->post("eselon_code", true);
        $arr_eselons = $this->model->column(array("eselon_code"=>"\"eselon_code\"", "eselon_name" =>"\"eselon_name\""))->group_by("group by eselon_code, eselon_name")->order("eselon_code nulls first")->getAllFilter();
        
        $eselons = Util::toMap($arr_eselons, "eselon_code", "eselon_name");

        $data['eselon_name'] = $eselons[$data['eselon_code']];
        $data['jabatan_id'] = $this->input->post("jabatan_id", true);
        $data['status'] = Karyawan_model::AKTIF;
        $data['tgl_masuk'] = strtoupper("'".$this->input->post("tgl_masuk", true)."'");
        $data['tgl_pensiun'] = strtoupper("'".$this->input->post("tgl_pensiun", true)."'");
        if ($this->input->post("tgl_pensiun", true)=='' or $this->input->post("tgl_pensiun", true)==NULL){
            $data['tgl_pensiun'] = "'31-DES-9999'";
        }
        $data['updated_at'] = "to_date('".date("d/m/Y")."','DD-MM-YYYY')"; 

        $is_exist = $this->model->getBy($data['id']);
        if ($is_exist) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Employee Number is already in use. Please use another number.'));
        }

        $insert = dbInsert('Karyawan', $data);
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully add Employee'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to add Employee'));
        }

    }

    /**
     * Edit data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
    public function update($id = null) {
        // SessionManagerWeb::setFlashMsg(false, "Sorry, this feature is still unavailable");
        // redirect($this->ctl.'/edit/'.$id);
        // die("fitur ini sedang dimatikan");

        $data['id'] = $this->input->post("id", true);
        $data['name'] = $this->input->post("name", true);
        $data['email'] = strtoupper($this->input->post("email", true));
        
        
        // Eselon
        $data['eselon_code'] = $this->input->post("eselon_code", true);
        $arr_eselons = $this->model->column(array("eselon_code"=>"\"eselon_code\"", "eselon_name" =>"\"eselon_name\""))->group_by("group by eselon_code, eselon_name")->order("eselon_code nulls first")->getAllFilter();
        
        $eselons = Util::toMap($arr_eselons, "eselon_code", "eselon_name");

        $data['eselon_name'] = $eselons[$data['eselon_code']];
        $data['jabatan_id'] = $this->input->post("jabatan_id", true);
        $data['status'] = Karyawan_model::AKTIF;
        $data['tgl_masuk'] = strtoupper("'".$this->input->post("tgl_masuk", true)."'");
        $data['tgl_pensiun'] = strtoupper("'".$this->input->post("tgl_pensiun", true)."'");
        if ($this->input->post("tgl_pensiun", true)=='' or $this->input->post("tgl_pensiun", true)==NULL){
            $data['tgl_pensiun'] = "'31-DES-9999'";
        }
        $data['updated_at'] = "to_date('".date("d/m/Y")."','DD-MM-YYYY')"; 

        $update = dbUpdate('Karyawan', $data, "id='$id'");
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully update Employee'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to update Employee'));
        }
    }


    public function ajaxCheckEmail(){
        $email = strtolower($this->input->post("email", true));

        $status = 0;

        // ngecek depan @ sudah ada belum, kalo udh di kasi warning krn ga bisa dijadiin user
        $front_email = explode("@", $email);
        $is_front_exist = $this->model->filter("where lower(email) like '%".$front_email[0]."%'")->getOneFilter('1');
        if ($is_front_exist){
            $status=2;
        }

        // cek email sudah ada belum
        $is_exist = $this->model->filter("where lower(email)='".$email."'")->getOneFilter('1');
        if ($is_exist){
            $status=1;
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'), $status);
    }

    public function ajaxGetKaryawan(){
        $this->load->model("User_model");

        $id = $this->input->post("id", true);
        $karyawan = $this->model->getBy($id);
        if ($karyawan==false or $karyawan==NULL){
            die('0');
        }
        $var['name'] = $karyawan['name'];
        $var['email'] = $karyawan['email'];
        $arr_email = explode("@",$karyawan['email']);
        $var['username'] = $arr_email[0];

        $is_exist = $this->User_model->filter("where lower(\"username\")='".strtolower($var['username'])."'")->getOneFilter('name');
        if ($is_exist==NULL or $is_exist==false){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'), $var);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'), '1');
        }
    }

    public function ajaxGetEmployee($is_check_exist=0){
        $this->load->model("User_model");

        $id = $this->input->post("id", true);
        $user_id = $this->User_model->filter("where \"karyawan_id\"='$id' ")->getOneFilter('user_id');
        $user = $this->User_model->getByAll($user_id);
        if ($user==null or $user==false){
            $user='0';
        }

        if (!$is_check_exist){
            echo json_encode($user);
            die();
        }

        $is_exist = $this->User_model->filter("where lower(\"username\")='".strtolower($user['username'])."'")->getOneFilter('name');
        if ($is_exist==NULL or $is_exist==false){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'), $user);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'), '1');
        }
    }


    public function uploadMetadata(){
        $this->metadataform('masters/karyawan');
    }

    public function import(){

        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'/masters/karyawan/'.$id)) {
           $files = glob($path_source.'/masters/karyawan/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."/masters/karyawan/".$id."/".$metadata;

        if (!file_exists($file)){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to import data'));
        }

        $this->load->library('Libexcel');

        $tmpfname = $file;
        
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $eselons = array(10,20,30,40,50);
        $this->load->model("Jabatan_model");

        $column = array();
        $success = array();
        $failed = array();
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data = array();
            $insert_other = array();
            for ($col = 0 ; $col < $worksheetData[0]['totalColumns']; $col++){
                $colString = PHPExcel_Cell::stringFromColumnIndex($col);
                $value =  $worksheet->getCell($colString.$row)->getValue();

                switch($col){
                    case 0:
                        $data['id'] = $value;
                    break;
                    case 1:
                        $data['name'] = "{$value}";
                    break;
                    case 2:
                        $data['email'] = "{$value}";
                    break;
                    case 3:
                        $data['jabatan_id'] = $value;
                    break;
                    case 4:
                        $data['eselon_code'] = "{$value}"; 
                    break;
                    case 5:
                        $data['tgl_masuk'] = "to_date('".$value."', 'dd/mm/yyyy')";
                    break;
                    case 6:
                        $data['tgl_pensiun'] = "to_date('".$value."', 'dd/mm/yyyy')";
                        if ($value=='' or $value==NULL){
                            $data['tgl_pensiun'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
                        }
                        
                    break;
                }
            }
            if ($data['id']==NULL or $data['id']==''){
                break;
            }

            if ($this->user['role'] == Role::ADMIN_UNIT){
                $company = ltrim($this->model->filter("where \"karyawan_id\"='".$data['id']."' ")->getOneFilter('company'));
                if ($company!= $this->user['company']){
                    $failed[] = array("id"=>$data['id'], "reason"=>"Not Allowed : you cannot import the employee from $company");
                    continue;
                }
            }

            // Check Metadata
            // cek id
            if (strlen($data['id'])!=8){
                $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Id not 8 Digit");
                continue;
            } else {
                $data['id'] = "{$data['id']}";
            }

            // nama
            if ($data['name']==NULL or $data['name']==''){
                 $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Empty Name");
                 continue;
            } 

            // cek eselon
            if (!in_array($data["eselon_code"], $eselons)){
                 $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Eselon not Exist / Allowed");
                 continue;
            } else {
                $data['eselon_name'] = $this->model->filter("where eselon_code='{$data['eselon_code']}'")->getOneFilter('eselon_name');
            }

            // jabatan id
            $jabatan_id = str_pad($data['jabatan_id'], 8, '0', STR_PAD_LEFT);                
            $is_jabatan_exist = $this->Jabatan_model->filter("where id='".$jabatan_id."' ")->getOneFilter('1');
            if (!$is_jabatan_exist){
                $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Position not Exist");
                continue;
            } else {
                $data['jabatan_id'] = "{$data['jabatan_id']}";
            }

            $data['updated_at'] = "to_date('".date('d/m/Y')."','dd/mm/yyyy')";

            $is_exist = $this->model->getOne($data['id'], '1');
            $is_exist_email = $this->model->filter("where lower(email)='".strtolower($data['email'])."' ")->getOneFilter('1');
            if (!$is_exist_email){
                if ($is_exist){
                    // update
                    if (dbUpdate("karyawan", $data, "id='".$data['id']."'")){
                        $success[] = array("id" => $data['id'], "reason" => "Successfully updated");
                        $user_id = $this->User_model->filter("where \"karyawan_id\"='".$data['id']."' ")->getOneFilter('user_id');
                        $user = array();
                        $user['username'] = $data['email'];
                        $user['name'] = $data['name'];
                        $user['email'] = $data['email'];
                        dbUpdate("users", $user, "id='$user_id'");
                    } else {
                        $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata");
                    }
                } else {
                    $data['status'] = Karyawan_model::AKTIF;
                
                    if (dbInsert("Karyawan", $data)){
                        $success[] = array("id" => $data['id'], "reason" => "Successfully added");
                    } else {
                        $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata");
                    }
                }
            } else {
                $email = $this->model->getOne($data['id'], 'email');
                if ($email==$data['email']){
                    if (dbUpdate("karyawan", $data, "id='".$data['id']."'")){
                        $success[] = array("id" => $data['id'], "reason" => "Successfully updated");
                        $user_id = $this->User_model->filter("where \"karyawan_id\"='".$data['id']."' ")->getOneFilter('user_id');
                        $user = array();
                        $user['username'] = $data['email'];
                        $user['name'] = $data['name'];
                        $user['email'] = $data['email'];
                        dbUpdate("users", $user, "id='$user_id'");
                    } else {
                        $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata");
                    }
                        
                } else {
                    // Keluarkan kalo sudah ada
                    $failed[] = array("id"=>$data['id'], "reason"=>"Email Already is Use (to update, email and employee number <br>must be same from previous data)");
                }
                
            }
            
        }
        // $_SESSION[SessionManagerWeb::INDEX]['import']['jabatan'] = array(
        //     "success" => $success,
        //     "failed" => $failed
        // );
        $this->data['success'] = $success;
        $this->data['failed'] = $failed;
        $buttons['download'] = array('label' => 'Download Report', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownload()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;

        $this->_deleteFiles($path_source."masters/karyawan/".$id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Sucess to import data'));
        // $this->template->viewDefault($this->view, $this->data);
    }

    public function downloadImportReport(){
        $filename = "import_employee_report";  
        $file_ending = "xls";
        //header info for browser
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");    
        header("Content-Disposition: attachment; filename=$filename.xls");  
        header("Pragma: no-cache"); 
        header("Expires: 0");

        /*******Start of Formatting for Excel*******/   
        $sep = "\t"; 

        echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
        echo "<tr></tr>";
        echo "<tr><td colspan='2'><b>Import Jabatan</b></td></tr>";
        echo "<tr><td>Di Import Tanggal</td><td>".date("d M Y")."</td></tr>";
        echo "<tr><td>Di Import Oleh</td><td colspan=3>".$this->user['name']."</td></tr>";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header = "<tr>
                <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Employee Number</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema='';
        // foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['jabatan']['failed'] as $fail) {
        //     $schema.= "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
        //             {$fail['id']}
        //         </td>{$sep}
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
        //             {$fail['reason']}
        //         </td>{$sep}
        //     </tr>";
        // }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header ="<tr>
                <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Employee Number</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema = '';
        // foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['jabatan']['success'] as $s) {
        //     $schema .= "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
        //             {$s['id']}
        //         </td>{$sep}
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
        //             {$s['reason']}
        //         </td>{$sep}
        //     </tr>";
        // }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo '</table>';
        // unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
    }
}

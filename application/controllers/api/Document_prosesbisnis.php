<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
class Document_prosesbisnis extends PrivateApiController {

    public function __construct() {
        parent::__construct();
    }

    public function index($page=1) {
    	$allData = $this->model->getAll();
    	$data['data'] = $allData;

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function edit($id=NULL){
    	if (isset($id))
            $data = $this->model->getBy($id);
        else
            $data = array();
        
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function add(){
        $data = $this->input->post(null, true);
        $insert = $this->model->insert($data);
        if ($insert === true) {
        	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully added business process'), $data);
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to add business process';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;
        	$this->setResponse($this->setSystem(ResponseStatus::ERROR, $msg));
        }
    }

    public function update($id = null) {
        $data = $this->input->post(null, true);
        $update = $this->model->save($id, $data);
        if ($update === true) {
        	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully changed business process'), $data);
        } else {
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change business process';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        	$this->setResponse($this->setSystem(ResponseStatus::ERROR, $msg));
        }

    }

    public function delete($id) {
        $delete = $this->model->delete($id);

        if ($delete === true) {
        	$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully deleted business process'), $data);
        } else {
        	$this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to delete business process'));
        }
    }
}
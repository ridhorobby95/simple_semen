<?php
//////////////////////// CARI FUNGSI PER PAGE APLIKASI:    ////////////////////////////////
//////////////////////// KEYWORD: START OF 'NAMAPAGE' PAGE ////////////////////////////////
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
class Document extends PrivateApiController {
    const CTL_PATH = 'api/';

    /**
     * Mendapatkan nama controller untuk redirect
     * @param string $ctl
     */
    protected function getCTL($ctl = null) {
        if (!isset($ctl))
            $ctl = $this->class;

        return $this::CTL_PATH . $ctl;
    }

    /**
     * Mendapatkan menu
     * @return array
     */
    public function getMenu() {
        $this->load->model('Quick_link_model');
        $this->load->model('Document_types_model');

        if($this->user['role'] != Role::ADMIN_UNIT){
            $getMenu = dbGetRows("select * from menus where parent=0 and role_access is null");
        }
        else{
            $getMenu = dbGetRows("select * from menus where parent=0 and role_access='".Role::ADMIN_UNIT."'");
        }
        foreach ($getMenu as $key => $value) {
            if($value['NAME'] == 'DOCUMENT TYPE'){
                if($this->user['role'] == Role::EKSTERNAL){
                    $where = implode('\',\'', $user['document_type_access']);
                    $document_types = $this->Document_types_model->order('urutan')->getAllJenis("and id in('".$where."') ");
                }
                else{
                     $document_types = $this->Document_types_model->order('urutan')->getAllJenis();
                }
                foreach ($document_types as $keytypes => $types) {
                    $getMenu[$key]['CHILD'][] = array('PARENT' => $value['ID'], 'NAME' => $types['type'], 'DESCRIPTION' => $types['type'], 'LINK' => '/documentTypes/'.$types['id'], 'ICON' => '');
                }
            }
            if($value['NAME'] == 'OTHERS'){
                $others = $this->Quick_link_model->show_sql(false)->getAll('and status=1');
                foreach ($others as $oth) {
                    $getMenu[$key]['CHILD'][] = array('PARENT' => $value['ID'], 'NAME' => $oth['name'], 'DESCRIPTION' => $oth['name'], 'LINK' => $oth['link'], 'ICON' => $oth['icon']);
                }
            }
            else{
                $getMenu[$key]['CHILD'] = dbGetRows("select * from menus where parent=".$value['ID']." order by urutan");
            }
            
        }
       
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $menu);
        // echo "<pre>";print_r($getMenu);die();

        

        // $menu = array();
        // if($this->user['role'] != Role::EKSTERNAL){
        //     $menu[] = array('namamenu' => 'Home', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        //     $menu[] = array('namamenu' => 'Dashboard', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 1, 'faicon' => 'dashboard');
        //     if ($this->user['role'] == Role::DOCUMENT_CONTROLLER || $this->user['role'] == Role::ADMINISTRATOR){
        //         $menu[] = array('namamenu' => 'Statistic', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/statistik/1'), 'levelmenu' => 1, 'faicon' => 'dashboard');
        //         $menu[] = array('namamenu' => 'List All', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/list_all'), 'levelmenu' => 1, 'faicon' => 'dashboard');
        //     }
        // }
        
        // $menu[] = array('namamenu' => 'Quick Link', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        // $menu[] = array('namamenu' => 'All Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=all'), 'levelmenu' => 1, 'faicon' => 'file');
        // if($this->user['role'] != Role::EKSTERNAL){
        //     $menu[] = array('namamenu' => 'My Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=me'), 'levelmenu' => 1, 'faicon' => 'file');
        // }
        
        // $menu[] = array('namamenu' => 'Work Unit Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=unitkerja'), 'levelmenu' => 1, 'faicon' => 'file');
        // $menu[] = array('namamenu' => 'Related Work Unit', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=related'), 'levelmenu' => 1, 'faicon' => 'file');
        // if ($this->user['role'] == Role::ADMINISTRATOR or $this->user['role'] == Role::DOCUMENT_CONTROLLER){
        //     $menu[] = array('namamenu' => 'Obsolete Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=obsolete'), 'levelmenu' => 1, 'faicon' => 'file');
        // }
        // $menu[] = array('namamenu' => 'Document Type', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        // $this->load->model('Document_types_model');
        // if($this->user['role'] == Role::EKSTERNAL){
        //     $where = implode('\',\'', $user['document_type_access']);
        //     $document_types = $this->Document_types_model->order('urutan')->getAllJenis("and id in('".$where."') ");
        // }
        // else{
        //     $document_types = $this->Document_types_model->order('urutan')->getAllJenis();
        // }
        // foreach ($document_types as $key => $types) {
        //     $menu[] = array('namamenu' => $types['type'], 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=type_'.$types['id']), 'levelmenu' => 1, 'faicon' => 'dashboard');
        // }


        // $menu[] = array('namamenu' => 'Buletin ISO', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        // $menu[] = array('namamenu' => 'Kebijakan Perusahaan', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=all'), 'levelmenu' => 1, 'faicon' => 'file');
        // $menu[] = array('namamenu' => 'Standar dan Sertifikat', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=me'), 'levelmenu' => 1, 'faicon' => 'file');
        // $menu[] = array('namamenu' => 'Daftar Dokumen Eksternal', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=unitkerja'), 'levelmenu' => 1, 'faicon' => 'file');
        // $menu[] = array('namamenu' => 'Daftar IPDK', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=related'), 'levelmenu' => 1, 'faicon' => 'file');
        // if ($this->user['role'] == Role::ADMINISTRATOR or $this->user['role'] == Role::DOCUMENT_CONTROLLER){
        //     $menu[] = array('namamenu' => 'dll', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=obsolete'), 'levelmenu' => 1, 'faicon' => 'file');
        // }

        // $menu[] = array('namamenu' => 'Others', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        
        // $others = $this->Quick_link_model->show_sql(false)->getAll('and status=1');
        // foreach ($others as $oth) {
        //     $menu[] = array('namamenu' => $oth['name'], 'class' => 'blank', 'namafile' => $oth['link'], 'levelmenu' => 1, 'faicon' => $oth['icon']);
        // }
        // // $menu[] = array('namamenu' => 'Hukum Online', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 1, 'faicon' => 'book');

        // if ($this->user['role'] == Role::ADMIN_UNIT){
        //     $menu = array();
        //     $menu[] = array('namamenu' => 'Setting', 'class' => $this->getCTL('setting'), 'namafile' => $this->getCTL('setting'), 'levelmenu' => 0, 'faicon' => 'setting');
        //     $menu[] = array('namamenu' => 'User', 'class' => $this->getCTL('user'), 'namafile' => $this->getCTL('user'), 'levelmenu' => 1, 'faicon' => 'user');
        //     $menu[] = array('namamenu' => 'Employee', 'class' => $this->getCTL('karyawan'), 'namafile' => $this->getCTL('karyawan'), 'levelmenu' => 1, 'faicon' => 'user');
        //     $menu[] = array('namamenu' => 'Position', 'class' => $this->getCTL('jabatan'), 'namafile' => $this->getCTL('jabatan'), 'levelmenu' => 1, 'faicon' => 'user');
        //     $menu[] = array('namamenu' => 'Superior', 'class' => $this->getCTL('atasan'), 'namafile' => $this->getCTL('atasan'), 'levelmenu' => 1, 'faicon' => 'user');
        //     $menu[] = array('namamenu' => 'Work unit', 'class' => $this->getCTL('unitkerja'), 'namafile' => $this->getCTL('unitkerja'), 'levelmenu' => 1, 'faicon' => 'user');

        // }
        // // echo "<pre>";print_r($menu);die();
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $menu);
    }

    public function dashboard() {
        // echo "<pre>";print_r($this->user);die();
        $data = $this->model->filter($filter." and \"status\"!='C' ")->getDocumentNeedAction($this->user['id'], $this->user['role'], $this->user['company']);
        if(!isset($data)){
            $message = 'data kosong';
        }
        else{
            $message = '';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat postingan'));

        // if ($insert === TRUE) {
        //     $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat postingan'));
        // } elseif (is_string($insert)) {
        //     $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        // } else {
        //     $validation = $this->model->getErrorValidate();
        //     if (empty($validation)) {
        //         $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat postingan'));
        //     } else {
        //         $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
        //     }
        // }
    }

	public function setDocumentEvaluation($document_id){
        $post = $this->input->post(null, true);

        
        $data['document_id'] = $document_id;
        $radio  = explode(',',$post['radio']);
        $text = array();
        for ($i = 1; $i < 10; $i++) {
            $text[$i] = $radio[$i-1];
        }
        $data['text'] = json_encode($text);
        $data['evaluation_comment'] = $post['comment'];
        $drafter_evaluation = dbGetOne('select drafter_evaluation from documents where id='.$document_id);
        if($drafter_evaluation){
            $data['drafter_id'] = $drafter_evaluation;
        }
        // echo "<pre>"; print_r($data);die();
        $this->load->model('Document_evaluation_model');
        $is_exist = $this->Document_evaluation_model->filter("where document_id=$document_id")->getOne('1');
        if ($is_exist){
            $this->Document_evaluation_model->update($document_id, $data);
            $this->addHistory($document_id, "Change document evaluation form");
        } else {
            $this->Document_evaluation_model->insert($data);
            $this->addHistory($document_id, "Change document evaluation form");
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to save the evaluation form'), '');
    }													
    public function tes(){
        $data = $this->model->getDocumentFlow(5147);
        echo "<pre>";print_r($data);die();
    }


    public function getDocProsesBisnis(){
        // list($token, $user) = AuthManager::validateToken(self::getBearerToken());
        $this->load->model('Document_prosesbisnis_model');
        $data = $this->Document_prosesbisnis_model->getAll();

        $dataList = array();
        foreach ($data as $k_jenis => $v_jenis) {
            $dataList[]=array('value'=>$v_jenis['id'],'label'=>$v_jenis['prosesbisnis']);
        }
        if(!isset($dataList)){
            $message = 'data kosong';
        }
        else{
            $message = '';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
    }

    public function getCodeBussProc($id){
        $this->load->model('Document_prosesbisnis_model');
        $data = $this->Document_prosesbisnis_model->getBy($id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);
    }

    public function getCodeWorkUnit($id){
        $this->load->model('Unitkerja_model');
        $data = $this->Unitkerja_model->getBy($id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);
    }

    public function getDocType(){
        $this->load->model('Document_types_model');
        $this->load->model('Tingkatan_document_types_model');
        $tingkatan = $this->User_model->getEselon($this->user['id']);
        $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
        $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

        $this->load->model("Document_model");
        $docType = array();
        foreach ($jenis as $k_jenis => $v_jenis) {
            if ($v_jenis['id']!=Document_model::BOARD_MANUAL){
                $docType[]=array('value'=>$v_jenis['id'],'label'=>$v_jenis['type']);
                // unset($jenis[$k_jenis]);
            }
        }
        if(!isset($docType)){
            $message = 'data kosong';
        }
        else{
            $message = '';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $docType);
    }

    public function getWorkUnit(){
        $this->load->model('User_model');
        $this->load->model('Unitkerja_model');
        $user = $this->user;
        $myunitkerja = $this->User_model->getBy($user['id'],'unitkerja_id');

        $level = array("'DEPT'", "'BIRO'", "'SECT', 'DIR', 'KOMP'");
        // $level = array("'DEPT'", "'BIRO'", "'SECT' ");
        $levels = implode(',',$level);
        $this->load->model('User_model');
        if ($myunitkerja==NULL)
            return NULL;
        $parent = $this->User_model->getBy($user['id'], 'unitkerja_parent');
        $unitkerja = array();
        $unitkerja[$myunitkerja] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($myunitkerja, 'name');
        $unitkerja[$parent] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'name');
        while ($parent!='0' and $parent!=NULL){
            $parent = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'parent');
            if ($parent=='0')
                continue;
            $unitkerja[$parent] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'name');
        }
        $dataList = array();
        foreach ($unitkerja as $key => $value) {
            if ($value==NULL)
                unset($unitkerja[$key]);
            else
                $dataList[] = array('value'=>$key,'label'=>$value) ;  
        }

        
        if(!isset($dataList)){
            $message = 'data kosong';
        }
        else{
            $message = '';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
        
    }


    public function getDocIso(){
        $this->load->model('Document_iso_model');
        $data = $this->Document_iso_model->getAll();
        

        $dataList = array();
        foreach ($data as $key) {
            $dataList[] = array('value'=>$key['id'],'label'=>$key['iso']) ;  
        }


        if(!isset($dataList)){
            $message = 'data kosong';
        }
        else{
            $message = '';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
    }


    public function getClauseIso($arrId){
        $iso_id = explode('_',$arrId);
        $iso_id = implode(',',$iso_id);
        
        $this->load->model('Document_iso_klausul_model');
        $data = $this->Document_iso_klausul_model->filter(" iso_id in ($iso_id) ")->getAll();
        

        $dataList = array();
        foreach ($data as $key) {
            $dataList[] = array('value'=>$key['id'],'label'=>$key['klausul']) ;  
        }


        if(!isset($dataList)){
            $message = 'data kosong';
        }
        else{
            $message = '';
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
    }

    public function getReviewer($return=false){
        $user = $this->user;
        $this->load->model('User_model');
        $mycompany = $this->User_model->getBy($user['id'], 'company');
        $reviewer = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\""))->getAll(" where \"company\"='$mycompany' ");
        $dataList = array();
       
        if($return == true){
            foreach ($reviewer as $key => $row) {
                $dataList['reviewer'][$row['user_id']] = $row['unitkerja_name'].' - '.$row['name'];
            }
            return $dataList;
        }

        else{
            foreach ($reviewer as $key => $row) {
                $dataList[] = array('value'=>$row['user_id'],'label'=>$row['unitkerja_name'].' - '.$row['name']);
            }
            if(!isset($dataList)){
                $message = 'data kosong';
            }
            else{
                $message = '';
            }
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);
        }
        

    }

    public function getCreator($type,$unitkerja){
        $user = $this->user;
        $data = array();

        $this->load->model('User_model');
        $this->load->model("Unitkerja_model");
        $myunitkerja = $this->User_model->getBy( $user['id'],'unitkerja_id');
        $this->load->model('Document_types_model');
        $this->load->model('Tingkatan_document_types_model');
        $tingkatan = $this->User_model->getEselon($user['id']);
        $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
        $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

        $this->load->model("Document_model");
        foreach ($jenis as $k_jenis => $v_jenis) {
            if ($v_jenis['id']==Document_model::BOARD_MANUAL){
                unset($jenis[$k_jenis]);
            }
        }
        $creators = $this->Unitkerja_model->filter('')->getCreator($myunitkerja, $jenis);

        $cek = array();
        $no = 0;
        foreach ($creators[$unitkerja][$type] as $key => $value) {
            if ($key != null){
                $data[$no]['value'] =$key;
                $data[$no]['label'] = $value;
                $cek[$no] = $key;
                $no++;    
            }
            
        }


        // $this->load->model('Atasan_model');
        $atasan_nopeg = $this->Unitkerja_model->getAtasanByLevel($unitkerja, $user['id']);

        if (!in_array($atasan_nopeg, $cek)){
            $name = $this->User_model->filter("where \"user_id\"='".$atasan_nopeg."'")->getOneFilter("name");

            
            if ($atasan_nopeg!=NULL){
                $data[$no]['value'] = $atasan_nopeg;
                $data[$no]['label'] = $name;
            }
            
        }
        

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data);

        
    }

    public function getDirut($company='2000'){
        $sql = "select id from unitkerja where lower(name) like '%president directorate%' and company='$company' and \"LEVEL\"='DIR' and sysdate < end_date order by id desc";
        $unitkerja_dirut = dbGetOne($sql);
        $sql_user = " select \"user_id\" from users_view where \"unitkerja_id\"='$unitkerja_dirut' and \"is_chief\"='X' and \"is_active\"=1 ";
        $dirut_id = dbGetOne($sql_user);
        return $dirut_id;
    }


    public function getApprover($creator_id,$type_id){
        $this->load->model('User_model');
        $this->load->model('Atasan_model');
        $data = array();
        $creator_nopeg = $this->User_model->getBy($creator_id, 'karyawan_id');

        
        $mycompany = $this->User_model->getBy($user['id'], 'company');
        
        $atasan = $this->Atasan_model->filter(" where k_nopeg='$creator_nopeg' ")->getBy();
        $atasan1_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan1_nopeg']."' ")->getOneFilter('user_id');
        $atasan2_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan2_nopeg']."' ")->getOneFilter('user_id');


        
        $data['approver'][0]['value'] = $atasan1_id;
        if ($user['id']==$creator_id){
            $data['approver'][0]['value'] = $atasan1_id;
        }

        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data['approver']);        
        
        $data['approver'][0]['label'] = $this->User_model->getBy($data['approver'][0]['value'], 'name');
        if ($type_id=='2'){
            $data['approver'][1]['value'] = $this->getDirut($mycompany);
            $data['approver'][1]['label'] = $this->User_model->getBy($data['approver'][1]['value'], 'name');
            if ($data['approver'][0]['value']==$data['approver'][1]['value']){
                unset($data['approver'][1]);
            }
        }
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data['approver']);     
        if ($data['approver'][0]['value']==null){
            if ($data['approver'][1]['value']==null){
                $data = array();
            } else {
                $data['approver'][0] = $data['approver'][1];
                unset($data['approver'][1]); 
            }
            
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $data['approver']);        
    }

    public function getRelatedWorkUnit(){
        $this->load->model('User_model');
        $mycompany = $this->User_model->getBy($user['id'], 'company');
        $this->load->model('Unitkerja_model');
        $unitkerja=$this->Unitkerja_model->order('name')->getAllList(" company='$mycompany' ");
        $dataList = array();
        foreach ($unitkerja as $key => $value) {
            $dataList[] = array('value'=>$key,'label'=>$value);
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $dataList);        
    }

    public function upload() {
        list($token, $user) = AuthManager::validateToken(self::getBearerToken());
        if(!empty($_FILES['file']['name'])){
             $ciConfig = $this->config->item('utils');
             $config['upload_path'] = $ciConfig['full_upload_dir'];
             $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx';
             $config['file_name'] = $_FILES['file']['name'];
             $config['encrypt_name'] = TRUE;
             $this->load->library('upload'); 
             $this->upload->initialize($config);
             if($this->upload->do_upload('file')){
                     $uploadData = $this->upload->data();

                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $uploadData);   
                }else{
                    $error = array('error' => $this->upload->display_errors());
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->upload);   
                }
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// START OF DASHBOARD PAGE  /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //1. getNeedActions : get dokumen yang harus diproses user
    //2. getNeedProgress : get dokumen yang telah diproses atau akan dproses oleh user
    //3. getDocumentReject: get dokumen yang pengajuannya ditolak
    //4. getDownloadRequest : Mendapatkan data permintaan download user
    //5. getPublishDoc: mendapatkan dokumen yang baru saja di publish
    //6. getPopularDoc: mendapatkan dokumen dengan view terbanyak
    //7. getVisitorAmount : mendapatkan data visitor tiap bulan
    //8. getTotalPerType: mendapatkan statistik dokumen per tipe dokumen
    function getNeedActions(){
        $user = $this->user;
        // $user['id'] = 61273;
        // $user['role'] = 'E';
        // $user['company'] = '2000';
        
        $data = $this->model->filter(" and \"status\"!='C' ")->getDocumentNeedAction(0, $user['id'], $user['role'], $user['company']);
        $this->data['total_document'] = $data['total_document'];
        unset($data['total_document']);
        $this->data['total_all_document'] = $data['total_all_document'];
        unset($data['total_all_document']);
        $this->data['need_action'] = $data['need_action'];
        unset($data['need_action']);
        $this->data['in_process'] = $data['in_process'];
        unset($data['in_process']);
        $this->data['data'] = $data;
        // echo "<pre>";print_r($this->data);die();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data); 
    }

    function getInProgress(){
        $user = $this->user;
        // $id = $user['id'];
        //  $user['id'] = 61273;
        // $user['role'] = 'E';
        // $user['company'] = '2000';
        $data = $this->model->filter(" and \"status\"!='C' ")->getDocumentNeedAction(1, $user['id'], $user['role'], $user['company']);
        unset($data['total_document']);
        $this->data['total_all_document'] = $data['total_all_document'];
        unset($data['total_all_document']);
        $this->data['need_action'] = $data['need_action'];
        unset($data['need_action']);
        $this->data['in_process'] = $data['in_process'];
        unset($data['in_process']);
        $this->data['data'] = $data;
        // echo "<pre>";print_r($this->data);die();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data);
    }

    function getDocumentReject(){
        $user = $this->user;
        $rejected_documents = $this->model->filter(" and \"status\"='C' ")->getDocumentNeedAction(3,$user['id'],$user['role'],$user['company']);
        if($rejected_documents){
            $this->data['rejected_document_counter'] = $rejected_documents['total_all_document'];
            if($this->data['rejected_document_counter'] == 0){
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
            }
            else{
                unset($rejected_documents['total_document']);
                unset($rejected_documents['total_all_document']);
                unset($rejected_documents['need_action']);
                unset($rejected_documents['in_process']);
                unset($rejected_documents['next']);
                $this->data['rejected_documents'] = $rejected_documents;
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
            }
            
        }
    }

	public function searchAdvance(){
    	$this->load->model('user_model');
    	// $company = SessionManagerWeb::getCompany();
    	$company = $this->input->post('company');
		// $filter = " where \"show\"='1' and \"status\"='D' ";
		$filter = " where \"show\"='1' and \"is_published\"=1 and \"company\" = '$company'";
		
		$keywordSearch = strtoupper($this->input->post('title',true));
		if ($keywordSearch!='')
			$filter .= " and (upper(\"title\") like '%$keywordSearch%' or upper(\"code\") like '%$keywordSearch%' or upper(\"unitkerja_name\") like '%$keywordSearch%') ";
		$typeSearch = $this->input->post('type', true);
		if ($typeSearch!='')
			$filter .= " and \"type_id\"=$typeSearch ";

		// die($filter);
		$search = $this->model->filter($filter)->getDocumentOld();

		array_multisort($updated, SORT_DESC, $search);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $search);
    }
    function getDownloadRequest(){
        $this->load->model('Document_download_model');
        $user = $this->user;
        $downloads = $this->Document_download_model->getAll($filter);
        // echo '<pre>';
        // var_dump($downloads);
        // die();
        $download_counter = 0;
        if ($downloads){
            $this->load->model('Document_types_model');
            $this->load->model('User_model');
            $this->load->model('Unitkerja_model');
            foreach ($downloads as $k_download => $v_download) {
                if ($user['role'] == Role::DOCUMENT_CONTROLLER){
                    $company = $this->User_model->filter(" where \"user_id\"='".$user['id']."' ")->getOneFilter("company");
                    $unitkerja_document = $this->model->getOne($v_download['document_id'], "unitkerja_id");
                    $company_document = $this->Unitkerja_model->getOne($unitkerja_document, "company");

                    if ($company!=$company_document){
                        unset($downloads[$k_download]);
                        continue;
                    }
                }
                $download_counter++;
                $downloads[$k_download]['requestor'] = $this->User_model->getBy($v_download['user_id'], 'name');
                $downloads[$k_download]['requestor_workunit'] = $this->User_model->getBy($v_download['user_id'], 'unitkerja_name');
                $workunit = $this->model->getOne($v_download['document_id'], 'unitkerja_id');
                $downloads[$k_download]['document_workunit'] = $this->Unitkerja_model->getOneBy($workunit, 'name');
                $downloads[$k_download]['name'] = $this->model->getOne($v_download['document_id'], 'title');
                $type = $this->model->getOne($v_download['document_id'], 'type_id');
                $downloads[$k_download]['type_name'] = $this->Document_types_model->filter(" id=$type ")->getOne('type');
            }
            $this->data['downloads'] = $downloads;
            $this->data['download_counter'] = $download_counter;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
        // echo "<pre>";print_r($this->data);die();
    }

    function getPublishDoc($company = '2000'){
        $filter = " where \"show\"='1' and \"is_published\"=1 and rownum<=5 and \"company\"='".ltrim($company, '0')."' ";
        $this->data['newest_doc'] = $this->model->filter($filter)->getDocumentOld();
        if($this->data['newest_doc']){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
    }

    function getPopularDoc(){
        $this->data['document_popular'] = $this->model->getDocumentPopuler();
        if($this->data['document_popular']){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
    }

    function getVisitorAmount(){
        $this->load->model('Statistic_model');
        $this->data['dataVisitor'] = $this->Statistic_model->getDataVisitor();
        if($this->data['document_popular']){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
    }

    function getTotalPerType($company='2000'){
        $company = str_pad($company, 8, "0", STR_PAD_LEFT);
        $this->data['total_type'] = array_change_key_case($this->model->getStatistikTotalType($company),CASE_LOWER);
        if($this->data['total_type']){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// END OF DASHBOARD PAGE  /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// START OF ALL DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //1. index : get dokumen yang sudah publish pada suatu company
    //2. indexdetail : untuk mengambil detail dari dokumen yang diklik (data disamping kanan)
    //3. getfilter : mengambil data filter untuk dmasukkan ke dalam query
    public function index($dataFilter= null, $company = '2000') {
        $this->load->model('Tag_model');
        $this->load->model('Document_shares_model');
        $this->load->model('user_model');
        $this->load->model('Workflow_model');
        $this->load->model('Document_download_model');
        $this->load->model('Unitkerja_model');

        $company = ltrim($company, '0');
        $where = '';
        if($this->user['role'] == Role::EKSTERNAL){
            $access_eksternal = dbGetRow("select document_type from user_eksternal where user_id=".$this->user['id']);
            $document_type_access = json_decode($access_eksternal['DOCUMENT_TYPE'], true);
            $where = " and \"type_id\" in('";
            $where .= implode('\',\'', $document_type_access);
            $where .= "')";
        }
        $filter = " where \"is_published\"=1 and (\"company\"='$company' or nvl(\"company\",0)='0' ) $where ";
        $filter = $this->getFilter($filter, $dataFilter);
        $column = 'd.*,rownum r';
        $table = '';
        $on = '';
        $filter_obsolete = " and \"show\"='1' ";
        if ($_SESSION['selected_menu']=='obsolete'){
            // $column = 'd.*';
            // $table = 'document_evaluation de';
            // $on =" de.document_id=d.\"document_id\" ";
            $filter_obsolete = " and \"is_obsolete\"='1' ";
        }
        $filter .= $filter_obsolete;
        $limit = 10;
        
        // echo "asdsad";die();
        // echo "<pre>";print_r($_SESSION['document_search']);die();
        $doc = $this->model->column($column)->with($table, $on)->filter($filter)->limit($limit)->getDocument(1, $dataFilter, $this->user, 1, true);
    // echo "<pre>";print_r($doc);die();
        if(count($doc) != 0){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $doc);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
        
    }

    function getIndexDetail($id_doc){
        $this->load->model('Tag_model');
        $this->load->model('Document_shares_model');
        $this->load->model('user_model');
        $this->load->model('Workflow_model');
        $this->load->model('Document_download_model');
        $this->load->model('Document_activity_model');
        $this->load->model('Unitkerja_model');


        $value = $this->model->getDocumentBy($this->user['id'], $id_doc);
        $unitkerja = $this->User_model->show_sql()->getBy($this->user['id'],'unitkerja_id');
        $user = $value['user_id'];
        $hash = hash_hmac('sha256',$value['filename'], $user);
        $id = md5($user . $this->config->item('encryption_key'));
        $ext = end(explode('.',$value['filename']));
        $ukuran = 0;
        if ($ext=='docx' || $ext=='doc' || $ext=='pptx' || $ext=='ppt' || $ext=='xlsx' || $ext=='xls' || $ext=='pdf' || 1==1) {
            // $file = File::getFile($hash,$value['filename'],'document/files');
            // $path = 
            
            $ukuran = $this->calculateSize(filesize($path.$value['filename']));
            $value['file'] = $file;
            $value['ukuran'] =$ukuran;
            $published_dates = explode(' ',$doc[$key]['published_at']);
            $published_date = $published_dates[0];
            $value['published_date'] = $published_date;
            $value['have_header'] = dbGetOne('select have_header from documents where id='.$document[$key]['document_id']);
        } 
        switch($value['workflow_id']){
            case Workflow_model::EVALUASI:
                $this->load->model('Document_evaluation_model');
                $document_id = $value['document_id'];
                $hasil_evaluasi = $this->Document_evaluation_model->filter(" where document_id=$document_id ")->getOne('status');
                $value['hasil_evaluasi'] = $hasil_evaluasi;
            break;
        }
        $is_allow_download = false;
        if ($this->Document_download_model->filter("where user_id=".$this->user['id']." and document_id=".$value['document_id'])->getOne('is_allowed')==1){
            $is_allow_download = true;
        }
        $value['is_allow_download'] = $is_allow_download;
        if ($this->user['role'] == Role::DOCUMENT_CONTROLLER ||$this->user['role'] == Role::ADMINISTRATOR){
            $value['reconvert']         = 1;
        }
        else{
            $value['reconvert']         = 0;
        }

        //// CEK ACCESS EVAL /////
        $value['access_eval'] = 0;
        // 1. Cek akses tipe dokumen
        $access_type = in_array($value['type_id'], $allTypes);
        if($access_type && $value['creator_id'] != $this->user['id']){
            //2. Cek Unitkerja
            // $myunitkerja = $this->data['variables']['myunitkerja'];
            $unitkerja = $this->User_model->getBy($this->user['id'],'unitkerja_id');
            $myunitkerja =$this->Unitkerja_model->order('name')->getMyUnitkerjaList($myunitkerja);
            $access_unitkerja = array_key_exists($value['unitkerja_id'], $myunitkerja);
            if($access_unitkerja){
                $getAtasan = $this->Unitkerja_model->getAtasan($unitkerja, $this->user['id']);
                $access_eval = in_array($value['creator_id'], $getAtasan);
                if($access_eval){
                    $value['access_eval'] = 1;
                }
                
                // echo "<pre>";print_r($getAtasan);die();
            }
        }

        // Activity
        $value['activity'] = $this->Document_activity_model->getActivity($value['document_id']);

        // History
        $value['history'] = $this->Document_activity_model->getHistory($value['document_id']);
        $value['docRelated'] = $this->model->getRelatedDocument($value['document_id']);
        $value['docRelatedNonForm'] = array_search('0', array_column($value['docRelated'], 'IS_FORM'));
        $value['docRelatedForm'] = array_search('1', array_column($value['docRelated'], 'IS_FORM'));

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $value);
        // echo "<pre>";print_r($value);die();
    }

    public function ajaxEvaluateDocument(){
        $document_id = (int)$this->input->post('id', true);
        $access_eval = (int)$this->input->post('access_eval', true);

        $this->load->model('Workflow_model');
        $data = array();
        $data['workflow_id'] = Workflow_model::EVALUASI;
        
        $data['status'] = Document_model::STATUS_BARU;
        // $data['user_id'] = $this->model->getOne($document_id, 'creator_id');
        $data['user_jabatan_id'] = $this->model->getOne($document_id, 'creator_jabatan_id');
        if($access_eval == 1){
            $data['drafter_evaluation'] = $this->input->post('drafter_eval', true);
            $data['workflow_urutan'] = 0;
        }
        else{
            $data['workflow_urutan'] = 1;
        }
        if (dbUpdate('documents', $data, "id=$document_id")){
            $this->deleteEvaluateDocument($document_id);
            $this->load->model('Document_reviewer_model');
            $data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, Workflow_model::PENGAJUAN);
            if ($data_reviewer==NULL){
                $data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, Workflow_model::REVISI);
            }
            $this->Document_reviewer_model->create($document_id,Workflow_model::EVALUASI, $data_reviewer);
            $data_baru = $this->model->getDocumentFlow($document_id);
            $this->inputDocLog($document_id,$data_baru,0,$data['status']);
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $document_id);
    }


    public function reconvert(){
        $document_id = $this->input->post('id');
        $getData = $this->model->getOne($document_id, 'DOCUMENT');
        $extensionFile = pathinfo($getData, PATHINFO_EXTENSION);
        if($extensionFile == 'xlsx' || $extensionFile == 'xls'){
            $data = array(
                'is_converted'  => 1,
                'have_header'   => 0
            );
        }
        else{
            $data = array(
                'is_converted'  => 0,
                'have_header'   => 0 
            );
            $updateDoc = $this->model->updateDocument($this->user['id'],$document_id,$data);
            if($updateDoc){
                // echo "berhasil update<br>";
                $hapus = $this->model->hapusConvertChecker($document_id);
                if($hapus){
                    // echo "berhasil hapus<br>";
                    $document_name = $this->model->getOne($document_id, 'DOCUMENT');
                    $explodeFile= explode('.', $document_name);
                    $extfile    = end($explodeFile); // extensi dari file yang di cursor
                    unset($explodeFile[count($explodeFile)-1]);
                    $nama_file = implode('.', $explodeFile);
                    foreach (glob('./assets/uploads/document/files/'.$nama_file."*") as $file) {
                        if(pathinfo($file, PATHINFO_EXTENSION) == 'pdf'){
                            unlink($file);
                        }
                    }
                }
                $title_name = $this->model->getOne($document_id, 'TITLE');
            }
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to reconvert Document "'.$title_name.'"'), $document_id);
        }
        // echo "<pre>";print_r($getData);echo "</pre>"
        
    }

    public function viewdoc($id, $is_echo=1){
        $is_published = '0';
        if ($_GET['is_published']){
            $is_published = '1';
        }

        $document = $this->model->getDocumentBy($this->user['id'], $id);
        $file = $this->model->getDocumentWithLink($id);
        $res['document'] = $document;
        $res['file'] = $file;
        $res['username'] = $this->user['username'];
        $res['date'] = date("d-m-Y");
        $res['ip'] = $_SERVER['REMOTE_ADDR'];
        $have_header = $this->model->getOne($id, "have_header");

        if($have_header == 1){
            if(!file_exists($res['file']['link_watermark'])){
                $unitkerja_id = $this->model->getOne($id, "unitkerja_id");
                $this->load->model('Unitkerja_model');
                $idCompany = $this->Unitkerja_model->getOneBy($unitkerja_id, "\"COMPANY\"");
                // $idCompany =    $this->model->get
                // $idCompany   = SessionManagerWeb::getCompany();
                $dataCompany =  array(
                                    '2000'  => 'PT. Semen Indonesia (Persero) Tbk.',
                                    '3000'  => 'PT. SEMEN PADANG',
                                    '4000'  => 'PT. SEMEN TONASA',
                                    '5000'  => 'PT. SEMEN GRESIK',
                                    '9000'  => 'PT. SEMEN KUPANG INDONESIA'
                                );
                
                $username   = $this->user['username'];
                $userid     = $this->user['id'];
                $explodeFile= explode('.', $res['file']['nama_file']);
                $extfile    = end($explodeFile);
                // echo $extfile;
                unset($explodeFile[count($explodeFile)-1]);
                $nama_file = implode('.', $explodeFile).'.pdf';
                $this->load->model('Unitkerja_model');
                // echo "<pre>";print_r($nama_file);die();
                // $photoCompany = $this->Unitkerja_model->getOneBy('0000'.$idCompany, "\"PHOTO\"");
                // echo "masuk sini";die();
                $hasil = $this->PlaceWatermark($nama_file, $idCompany , $username, $userid, 'pdf' );
            }
            $watermarked_hash = array();
            $watermarked_hash['user_id'] = $this->user['id'];
            $watermarked_hash['document_name'] = $res['file']['watermark'];
            $watermarked_hash['encryption'] = hash_hmac('sha256',$watermarked_hash['document_name'].$watermarked_hash['user_id'], date("d-m-Y h:i:sa")); 
            if ($this->model->isExistWatermarked($watermarked_hash['document_name'], $watermarked_hash['user_id'])){
                dbUpdate('document_watermarked_hash', $watermarked_hash, "document_name='".$watermarked_hash['document_name']."' and user_id=".$watermarked_hash['user_id']);
            } else {
                dbInsert('document_watermarked_hash', $watermarked_hash);
            }
            
            // $res['file']['link'] = $res['file']['link_watermark'];
            $res['file']['link'] = $watermarked_hash['encryption'];
            $res['is_converted'] = 1;
            $res['have_header'] = 1;

            $addView = $this->model->updateDocumentPopuler($id); // add view;
            $this->addActivity($id, "Viewed this Document.");
            if ($is_echo){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $res);
                // echo "<pre>";print_r($res);die();
                // echo json_encode($res);
            } else {
                return $watermarked_hash['encryption'];
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $watermarked_hash);
            }
            // print_r($hasil);
        } else {
            $res['is_converted'] = 0;
            $res['have_header'] = 0;
            if ($is_echo){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $res);
                // echo json_encode($res);
            }
            
        }       
    }

    public function addActivity($document_id, $text){
        $activity = array();
        $this->load->model('Document_activity_model');
        $activity["user_id"] = $this->user['id'];
        $activity["document_id"] = $document_id;
        $activity["activity"] = $text;
        $activity["type"] = Document_activity_model::ACTIVITY;
        $activity['workflow_id'] = $this->model->getOne($document_id, 'workflow_id');
        $activity['workflow_urutan'] = $this->model->getOne($document_id, 'workflow_urutan');
        $activity = array_change_key_case($activity, CASE_UPPER);
        $this->Document_activity_model->create($activity,TRUE,TRUE);
    }

    public function PlaceWatermark($file, $idCompany, $username, $userid, $extfile) {
        $this->load->model('Unitkerja_model');
        $namaCompany = $this->Unitkerja_model->getOne("0000".$idCompany."", 'name');

        // require_once(APPPATH."/third_party/fpdf/fpdf.php");
        require_once(APPPATH."/third_party/fpdf/rotation.php");
        // require_once(APPPATH."/third_party/fpdf/pdf.php");
        $dataCompany =  array(
                                    '2000'  => 'PT. Semen Indonesia (Persero) Tbk.',
                                    '3000'  => 'PT. SEMEN PADANG',
                                    '4000'  => 'PT. SEMEN TONASA',
                                    '5000'  => 'PT. SEMEN GRESIK',
                                    '9000'  => 'PT. SEMEN KUPANG INDONESIA'
                                );
        // $pdf = new FPDI();
        $pdf = new PDF_Rotate();

        $name = uniqid();
        //font
        $font = './assets/web/fonts/arial.ttf';
        //font size
        $font_size = 15;
        //image width
        $width = 1500;
        //text margin
        $margin = 0;

        $data_text = array();
        $explodefilename = explode('.', $file);
        $filename = $explodefilename[0];
        $data_text['text1']['value']         = "Dokumen ini terbatas untuk kalangan sendiri dan milik ".$namaCompany.", Dilarang membagikan, menyalin, dan";
        $data_text['text1']['name']          = $filename.'_'.$userid.'_text1';
        $data_text['text2']['value']          = "mencetak tanpa izin. Kesalahan dan perbedaan isi diluar tanggung jawab ".$namaCompany;
        $data_text['text2']['name']            = $filename.'_'.$userid.'_text2';
        $data_text['text3']['value']     = "Diakses oleh ".$username." (".$_SERVER['REMOTE_ADDR']."), Tanggal ".date("d-m-Y");
        $data_text['text3']['name']      = $filename.'_'.$userid.'_text3';
        // $data_text['tgl_view']['value']     = date("d-m-Y");
        // $data_text['tgl_view']['name']      = uniqid();

        foreach ($data_text as $text) {
            $text_a = explode(' ', $text['value']);
            $text_new = '';
            foreach($text_a as $word){
                //Create a new text, add the word, and calculate the parameters of the text
                $box = imagettfbbox($font_size, 0, $font, $text_new.' '.$word); 
                //if the line fits to the specified width, then add the word with a space, if not then add word with new line
                // echo "<pre>";print_r($box);echo "</pre>";
                if($box[2] > $width - $margin*2){
                    $text_new .= "\n".$word;
                } else {
                    $text_new .= " ".$word;
                }
            }
            // //trip spaces
            $text_new = trim($text_new);
            //new text box parameters
            // echo "<pre>";print_r($text_new);echo "</pre>";
            $box = imagettfbbox($font_size, 0, $font, $text_new);
            //new text height
            $height = $box[1] + $font_size + $margin * 2; // set height
            ///////////////////////CREATE FILE 1///////////////////////////////
            $img = imagecreatetruecolor($width,$height); // create new img and set width height
            // Background color
            $bg = imagecolorallocate($img, 255, 255, 255);  // set color from new img
            imagefilledrectangle($img, 0, 0,$width ,$height , $bg); //make background from new img, ()
            imagecolortransparent($img, $bg);
            ///////////////////////////////END OF FILE 1/////////////////////////////////////
            ////////////////////////////////FILE 2////////////////////////////////////////////
                // //create image
            $im = imagecreatetruecolor($width, $height);
             
            //create colors
            // $tbg = imagecolorallocate($im, 255, 255, 255);
            $white = imagecolorallocate($im, 255, 255, 255);
            $red = imagecolorallocate($im, 255, 0, 0);
            //color image
            imagefilledrectangle($im, 0, 0, $width, $height, $white);
            // $color = imagecolorallocate($img, 0, 0, 0);
            //add text to image
            imagettftext($im, $font_size, 0, $margin, $font_size+$margin, $red, $font, $text_new);
             $op = 100;
                if ( ($op < 0) OR ($op >100) ){
                    $op = 100;
                }
            imagecopymerge($img, $im, 0, 0, 0, 0, $width, $height, $op); // menggabung file $blank dan $img
            $dirpng = "./assets/uploads/document/files/";
            imagepng($img,$dirpng.''.$text['name'].".png");
            ////////////////////////////////END OF FILE 2////////////////////////////////////////////
        }
        // $dirpng = "./assets/uploads/document/files/";
        /////////////////////////////////OPACITY LOGO////////////////////////////////////////////////
  //       $this->load->model('Unitkerja_model');
        // $photoCompany = $this->Unitkerja_model->getOneBy('0000'.$idCompany, "\"PHOTO\"");
        // // echo "<pre>";print_r($initialCompany);echo "</pre>";
        // if(!file_exists("./assets/uploads/company/0000".$idCompany."_opa.png")){
        //  // return $photoCompany;
        //  $image = imagecreatefrompng('./assets/uploads/company/'.$photoCompany);
        //  $opacity = 0.2;
        //  imagealphablending($image, false); // imagesavealpha can only be used by doing this for some reason
        //  imagesavealpha($image, true); // this one helps you keep the alpha. 
        //  $transparency = 1 - $opacity;
        //  imagefilter($image, IMG_FILTER_COLORIZE, 0,0,0,127*$transparency); // the fourth parameter is alpha
        //  // header('Content-type: image/png');/
        //  imagepng($image, './assets/uploads/company/0000'.$idCompany.'_opa.png' );
        // } 
        /////////////////////////////////END OF OPACITY LOGO///////////////////////////
        $explodefilename = explode('.', $file);
        $filename = $explodefilename[0];
        $getFile = $filename.'_header.pdf';
        // $extfile  = $explodefilename[1];
        $file = "./assets/uploads/document/files/".$getFile;
        // echo $file;
        if (file_exists($file)){
           $pagecount = $pdf->setSourceFile($file);
           // echo $pagecount;
            // echo "b";
        } else {
            return FALSE;
            // echo "a";
        }
        // $logo = '0000'.$idCompany.'_opa.png';
        $company = $dataCompany[$idCompany];
        if($company ==  'PT. Semen Indonesia (Persero) Tbk.'){
            $logo = 'Untitled-1-04.png';
        }
        elseif($company == 'PT. SEMEN PADANG'){
            $logo = 'Untitled-1-02.png';
        }
        elseif($company == 'PT. SEMEN TONASA'){
            $logo = 'Untitled-1-03.png';
        }
        elseif($company == 'PT. SEMEN GRESIK'){
            $logo = 'Untitled-1-01.png';
        }
        elseif($company == 'PT. SEMEN KUPANG INDONESIA'){

        }

        for($i = 1; $i <= $pagecount; $i++){
            $tpl = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($tpl);
            // echo "<pre>";print_r($size);echo "</pre>";
            $pageSize = "A4";
            if($size['h'] == 297.00008333333){ $pageSize = "A4";}
            elseif($size['h'] == 355.6){ $pageSize = "Legal";}
            elseif($size['h'] == 279.4){ $pageSize = "Letter";}
            elseif($size['h'] == 419.99958333333){ $pageSize = "A3";}
            else {$pageSize = "A4";}
              // echo $dirpng."".$data_text['text1']['name'].'.png<br>';
            if($size['w'] <= $size['h']){
                // echo "masuk if<br>";
                $pdf->addPage("P", $pageSize);
                // $pdf->Image('./assets/uploads/company/'.$logo,55,90,100,77);
                // echo $dirpng."".$data_text['text1']['name'].'.png<br>';
                // $pdf->RotatedImage('./assets/web/images/logo/'.$logo,55,90,100,100);
                $pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 207, 60, 250, 3, 270);
                // unlink($dirpng."".$data_text['text1']['name'].'.png');
                //echo $dirpng."".$data_text['text2']['name'].'.png<br>';
                $pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 202, 95, 250, 3, 270);
                // unlink($dirpng."".$data_text['text2']['name'].'.png');
                //echo $dirpng."".$data_text['text3']['name'].'.png<br>';
                $pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 197, 100, 250, 3, 270);
                // unlink($dirpng."".$data_text['text3']['name'].'.png');
            }
            else{
                // echo "masuk else<br>";
                $pdf->addPage("L", $pageSize);
                // $pdf->Image('./assets/web/images/logo/'.$logo,115,70,100,100);
                $pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 290, 45, 175, 3, 270);    
                $pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 285, 55, 175, 3, 270);
                $pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 280, 65, 250, 3, 270);
                
            }
           
                // $x -= 5;
                // $y += 10;
            // }
            $pdf->useTemplate($tpl, 0, 0, 0, 0, TRUE);
            unlink($dirpng."".$data_text['text1']['name'].'.png');
            unlink($dirpng."".$data_text['text2']['name'].'.png');
            unlink($dirpng."".$data_text['text3']['name'].'.png');
           
        }
        // // echo './assets/uploads/document/files/'.$filename.'_'.$userid.'.'.$extfile;
        $pdf->Output('./assets/uploads/document/files/'.$filename.'_'.$userid.'_watermarked.'.$extfile, 'F');
    }

    function getFilter($filter = NULL, $dataFilter = NULL){        
        $filter=$filter;
        if ($dataFilter != NULL){
            foreach ($dataFilter as $key => $value) {
                if ($value==''){
                    unset($_SESSION['document_search'][$key]);
                    continue;
                }
                $val = strtoupper($value);
                switch($key){
                    case 'keyword':
                        $filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%' or upper(\"unitkerja_name\") like '%$val%') ";
                        // $_SESSION['document_search']['title'] = $value;
                    break;

                    case 'user':
                        $filter .= " and (upper(\"username\") like '%$val%' or upper(\"name\") like '%$val%' or upper(\"creator_username\") like '%$val%' or upper(\"creator_name\") like '%$val%') ";
                    break;

                    case 'type':
                        if ($dataFilter['type']=='0'){
                            $filter_type = '';
                        } else {
                            $filter_type = " and \"type_id\" in ($val) ";
                        }
                        $filter .= $filter_type;
                    break;

                    // case 'status':
                    //     // if ($_SESSION['document_search']['arr_status'][0]=='0'){
                    //     //  $filter_status = '';
                    //     // } else {
                    //     //  echo "<pre>";print_r($_SESSION['document_search']['arr_status']);die();
                    //     //  $filter_status = " and \"workflow_id\" in ($val) ";
                    //     // }
                    //     // $filter .= $filter_status;
                    // break;
                    case 'title':
                        $filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%') ";
                    break;

                    case 'show_by':
                        switch($val){
                            case 'ME':
                                $filter .= " and \"user_id\"='".$this->user['id']."'";
                            break;
                            // case 'ALL':
                            //  unset($_SESSION['document_search']['show_by']);
                            // break;
                            case 'NEED ACTION':
                                $filter .= " and (\"user_id\"!='".$this->user['id']."' or (\"workflow_urutan\"='1' and \"user_id\"='".$this->user['id']."') )";
                            break;
                        }
                    break;

                    case 'unitkerja':
                        if ($dataFilter['unitkerja']=='0'){
                            $filter_unitkerja = '';
                        } else {
                            $filter_unitkerja = " and \"unitkerja_id\" in ($val) ";
                        }
                        $filter .= $filter_unitkerja;
                    break;

                    case 'datestart':
                        $filter .= " and (upper(\"updated_at\") >= '$val') ";
                    break;

                    case 'dateend':
                        $filter .= " and (upper(\"updated_at\") <= '$val%') ";
                    break;
                    case 'obsolete':
                        // $filter .= " and de.status='C' ";

                    break;

                    // case 'related':
                    //  $filter .= " and \"unitkerja_id\" not in ($val) ";
                    // break;
                }
            }
        }
        // echo '<prE>';
        // vaR_dump($filter);
        // die();
        return $filter;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// END OF ALL DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// START OF LIST_ALL DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function list_all($company){
        parent::list_all($company);
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// END OF ALL DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// START OF LIST_ALL DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function statistik($session=0){
        if ($session==1){
            // unset($_SESSION['dms_semen_indonesia']['statistik']);
            // redirect("web/document/statistik");
        }
        if ($this->user['role'] == Role::DOCUMENT_CONTROLLER or $this->user['role'] == Role::ADMINISTRATOR){

            $this->load->model("Document_model");
            $this->load->model("Workflow_model");
            $this->load->model("Statistic_documents_model");

            // cek filternya
            if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'])){
                $_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'] = Workflow_model::PENGAJUAN;
            }
            if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'])){
                $_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'] = 1;
            }
            if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'])){
                $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] = 'DEPT';
            }
            if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'])){
                $_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow']= Workflow_model::PENGAJUAN;
            }
            if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'])){
                $_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'] = 1;
            }
            if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'])){
                $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] = 'DEPT';
            }

            $filter = $_SESSION['dms_semen_indonesia']['statistik'];
            
            $this->data['tasks']= $this->Workflow_model->getTasks(Workflow_model::PENGAJUAN);
            $color = array(
                "red-prog",
                "purple-prog",
                "blue-prog",
                "green-prog",
                "yellow-prog",
                "orange-prog",
                "brown-prog"

            );
            $this->data['color'] = $color;
            $workflow_id = Workflow_model::PENGAJUAN;
            $stats = $this->Statistic_documents_model->filter("where type='".Statistic_documents_model::PENDING_PROCESS."' and workflow_id=$workflow_id")->order("order by jumlah_hari desc")->getAll();

            // get Type
            $this->load->model("Document_types_model");
            $all_types = $this->Document_types_model->getAllJenis();
            $types = Util::toMap($all_types, 'id', 'type');
            // $types[0] = "All";
            ksort($types);
            $this->data['type_statistics'] = $types;

            // get flow pending process
            $this->data['pending_process']['flow'] = array(
                Workflow_model::PENGAJUAN =>"Submission",
                Workflow_model::REVISI =>"Revision"
            );

            // get Flow lead time
            $this->data['lead_time']['flow'] = array(
                Workflow_model::PENGAJUAN =>"Submission",
                Workflow_model::EVALUASI =>"Evaluation",
                Workflow_model::REVISI =>"Revision"
            );

            //get UnitKerja
            $this->load->model('Unitkerja_model');
            $this->data['pending_process']['workunit'] = array(
                'DEPT' => 'Department',
                'BIRO' => 'Bureau',
                'SECT' => 'Section',
            );
            $this->data['lead_time']['workunit'] = $this->data['pending_process']['workunit'];

            $this->load->model('Statistic_model');
            $dataStatistik_p = array();
            $dataStatistik_l = array();
            // if($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'DEPT'){
            //////////////////PENDING PROCESS//////////////////////////////////////////////
            // get yang memang benar2 dari filter workunit
            $dataStatistik_p_workunit =$this->Statistic_model->getDataStatistic($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'],$_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'],'P', 'unitkerja_id', $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'],ltrim(SessionManagerWeb::getCompany(),"0")); // hanya akan keluar per 1 unitkerja_id dengan syarat ini.(tidak akan muncul 2 row dengan unitkerja_id yang sama)
            if($dataStatistik_p_workunit){
                foreach ($dataStatistik_p_workunit as $workunit) {
                    $dataUrutan = json_decode($workunit['URUTAN'], true);
                    $dataStatistik_p[$workunit['UNITKERJA_ID']]['UNITKERJA_ID'] = $workunit['UNITKERJA_ID'];
                    $dataStatistik_p[$workunit['UNITKERJA_ID']]['NAME'] = $workunit['NAME'];
                    $dataStatistik_p[$workunit['UNITKERJA_ID']]['WORKFLOW_ID'] = $workunit['WORKFLOW_ID'];
                    $dataStatistik_p[$workunit['UNITKERJA_ID']]['DOC_TYPE'] = $workunit['DOC_TYPE'];
                    $dataStatistik_p[$workunit['UNITKERJA_ID']]['DATA_URUTAN'] = $dataUrutan;
                    $dataStatistik_p[$workunit['UNITKERJA_ID']]['JUMLAH_DATA'] = 1;
                }
                
            }
            // get dari parent(biro, dept)_idnya
            if($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'DEPT' || $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'BIRO'){
                $column = $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'].'_ID';
                $dataStatistik_p_unworkunit = $this->Statistic_model->groupByStatistik($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],$_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'], 'P',ltrim(SessionManagerWeb::getCompany(),"0") );
                if($dataStatistik_p_unworkunit){
                    foreach ($dataStatistik_p_unworkunit as $unworkunit) {
                        $dataUrutan = json_decode($unworkunit['URUTAN'], true);
                        if(isset($dataStatistik_p[$unworkunit[$column]])){ // Jika data dari workunit pada array P ada
                            for ($i=0; $i <= 6 ; $i++) { // perulangan dari urutan
                                $dataStatistik_p[$unworkunit[$column]]['DATA_URUTAN'][$i] += $dataUrutan[$i];
                            }
                            $dataStatistik_p[$unworkunit[$column]]['JUMLAH_DATA'] += 1;
                        }
                        // Membuat data baru dari dept/biro yang tidak mempunyai data di statistic tapi bawahannya punya
                        else{
                            $dataStatistik_p[$unworkunit[$column]]['UNITKERJA_ID'] = $unworkunit[$column];
                            $dataStatistik_p[$unworkunit[$column]]['NAME'] = $unworkunit['NAME'];
                            $dataStatistik_p[$unworkunit[$column]]['WORKFLOW_ID'] = $unworkunit['WORKFLOW_ID'];
                            $dataStatistik_p[$unworkunit[$column]]['DOC_TYPE'] = $unworkunit['DOC_TYPE'];
                            $dataStatistik_p[$unworkunit[$column]]['DATA_URUTAN'] = $dataUrutan;
                            $dataStatistik_p[$unworkunit[$column]]['JUMLAH_DATA'] = 1;
                        }
                    }
                }
            }
            
                
            // echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
            // echo "a";
            // die();
            // }
            
            // echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
            //////////////////LEAD TIME//////////////////////////////////////////////
            // get sesuai filter workunit
            // echo "ini dari luar=".$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type']);
            $dataStatistik_l_workunit =$this->Statistic_model->getDataStatistic($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],'L', 'unitkerja_id', $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'],ltrim(SessionManagerWeb::getCompany(),"0"));
            if($dataStatistik_l_workunit){
                foreach ($dataStatistik_l_workunit as $workunit) {
                    $dataUrutan = json_decode($workunit['URUTAN'], true);
                    $dataStatistik_l[$workunit['UNITKERJA_ID']]['UNITKERJA_ID'] = $workunit['UNITKERJA_ID'];
                    $dataStatistik_l[$workunit['UNITKERJA_ID']]['NAME'] = $workunit['NAME'];
                    $dataStatistik_l[$workunit['UNITKERJA_ID']]['WORKFLOW_ID'] = $workunit['WORKFLOW_ID'];
                    $dataStatistik_l[$workunit['UNITKERJA_ID']]['DOC_TYPE'] = $workunit['DOC_TYPE'];
                    $dataStatistik_l[$workunit['UNITKERJA_ID']]['DATA_URUTAN'] = $dataUrutan;
                    $dataStatistik_l[$workunit['UNITKERJA_ID']]['JUMLAH_DATA'] = 1;
                }
            }

            // get dari parent(biro / dept)_idnya
            if($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] == 'DEPT' || $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] == 'BIRO'){
                $column = $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'].'_ID';
                $dataStatistik_l_unworkunit = $this->Statistic_model->groupByStatistik($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'], 'L', ltrim(SessionManagerWeb::getCompany(),"0"));
                if($dataStatistik_l_unworkunit){
                    foreach ($dataStatistik_l_unworkunit as $unworkunit) {
                        $dataUrutan = json_decode($unworkunit['URUTAN'], true);
                        // echo "<pre>";print_r($dataUrutan);echo "</pre>";
                        if(isset($dataStatistik_l[$unworkunit[$column]])){
                            // echo "masuk if<br>";
                            for ($i=0; $i <= 6 ; $i++) { 
                                $dataStatistik_l[$unworkunit[$column]]['DATA_URUTAN'][$i] += $dataUrutan[$i];
                            }
                            $dataStatistik_l[$unworkunit[$column]]['JUMLAH_DATA'] += 1;
                        }
                        else{
                            $dataStatistik_l[$unworkunit[$column]]['UNITKERJA_ID'] = $unworkunit[$column];
                            $dataStatistik_l[$unworkunit[$column]]['NAME'] = $unworkunit['NAME'];
                            $dataStatistik_l[$unworkunit[$column]]['WORKFLOW_ID'] = $unworkunit['WORKFLOW_ID'];
                            $dataStatistik_l[$unworkunit[$column]]['DOC_TYPE'] = $unworkunit['DOC_TYPE'];
                            $dataStatistik_l[$unworkunit[$column]]['DATA_URUTAN'] = $dataUrutan;
                            $dataStatistik_l[$unworkunit[$column]]['JUMLAH_DATA'] = 1;
                        }
                    }
                }

            }
            
            
            // echo "<pre>";print_r($dataStatistik_l);echo "</pre>";

            // $dataDept = "SELECT S.DEPT_ID, U.NAME FROM STATISTIC_DOCUMENTS S LEFT JOIN UNITKERJA U ON S.DEPT_ID = U.ID GROUP BY S.DEPT_ID, U.NAME ORDER BY S.DEPT_ID";
            // $getData = dbGetRows($dataDept);

            // // echo "<pre>";print_r($getData);echo "</pre>";
            // foreach ($getData as $data) {
            //  $dataStatistik_p[$data['DEPT_ID']]['name'] = $data['NAME'];
            //  for ($i=1; $i <=5 ; $i++) { 
            //      for ($j=1; $j<=7 ; $j++) {
            //          if($j >= 2){
            //              if($j == 2){
            //                  $getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=2 AND ACTOR='E'";
            //              }
            //              elseif($j == 3){
            //                  $getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=2 AND ACTOR='R'";
            //              }
            //              else{
            //                  $urutan = $j-1;
            //                  $getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=".$urutan;
            //              }
            //          }
            //          else{
            //              $getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=".$j;
                            
            //          }
            //          // echo $getStatistik."<br>";
            //          $getStat = dbGetOne($getStatistik);
            //          if($getStat == null || $getStat == ''){
            //              $dataStatistik_p[$data['DEPT_ID']][$i][$j] = 0;
            //          }
            //          else{
            //              $dataStatistik_p[$data['DEPT_ID']][$i][$j] = $getStat;
            //          }
                        
                        
            //      }
            //  }
            // }
            // echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
            // echo "<pre>";print_r($dataStatistik_l);echo "</pre>";
            $total_data_p = 0;
            $total_data_l = 0;
            foreach ($dataStatistik_p as $p) {
                $total_data_p++;
                for ($i=0; $i <=6 ; $i++) { 
                    $p['DATA_URUTAN'][$i] = floor($p['DATA_URUTAN'][$i] / $p['JUMLAH_DATA']);
                }
            }
            foreach ($dataStatistik_l as $l) {
                $total_data_l++;
                for ($i=0; $i <=6 ; $i++) { 
                    $l['DATA_URUTAN'][$i] = floor($l['DATA_URUTAN'][$i] / $l['JUMLAH_DATA']);
                }
            }
            // // $this->dashboard();
            $this->data['dataStatistik_p'] = $dataStatistik_p;
            $this->data['dataStatistik_l'] = $dataStatistik_l;
            $this->data['total_data_p'] = $total_data_p;
            $this->data['total_data_l'] = $total_data_l;
            // // echo "<pre>";print_r($this->data['dataStatistik_p']);echo "</pre>";
            // die();

        }else{
            // redirect("web/document/dashboard");
        }
        // echo $_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'];
    // echo "<pre>";print_r($this->data);echo "</pre>";die();
    // echo "<pre>";print_r($_SESSION['dms_semen_indonesia']);echo "</pre>";    
        $this->template->viewDefault($this->view, $this->data);
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// END OF ALL DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// START OF CHANGE WORK UNIT DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Change Work Unit
    public function changeunit($id){

        $this->load->model('User_model');
        $this->load->model('Jabatan_model');
        $this->load->model('Atasan_model');
        $this->load->model('Unitkerja_model');

        $buttons = array();
        $buttons[] = array('label' => ' Back', 'type' => 'success', 'icon' => 'chevron-left', 'click' => 'goBack()' );
        $this->data['buttons'] = $buttons;

        // Get Document
        $document = $this->model->show_sql(true)->getDocumentBy($this->user['id'],$id);
        $document['changeunit'] = $this->model->getChangeWorkunit($id);
        $document['changeunit_verification'] = $this->model->getChangeWorkunitVerification($id);
        $document['changeunit_moved'] = $this->model->changeUnitMoved($id);
        $document['creator_jabatan_name'] = $this->Jabatan_model->getOne($document['creator_jabatan_id'], "name");
        $document['approver_jabatan_name'] = $this->Jabatan_model->getOne($document['approver_jabatan_id'], "name");
        $document_company = $this->Unitkerja_model->getOne($document['unitkerja_id'], "company");
        $creator = array(
            "id" => $document['creator_id'],
            "name" => $document['creator_name'],
            "jabatan_id" => $document['creator_jabatan_id'],
            "jabatan_name" => $document['creator_jabatan_name'],
            "unitkerja_id" => $this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"),
            "unitkerja_name" => $this->Unitkerja_model->getOne($this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"), "name")
        );


        $btn = array();
        $users = array();
        $approvers = array();
        switch ($document['changeunit']) {
            case 'A':
                $document['creator_status'] = "<b style='color:green'>OK</b>";
                $document['creator_solution'] = NULL;

                $actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['approver_jabatan_id']."' ")->getOneFilter("name");
                if ($actor!=NULL) {
                    $document['approver_status'] = "<b style='color:red'> This Approver is not served at this position </b>";
                    $document['approver_solution'] = " The Approver will be chosen automatically by system. The new Approver will be <b style='color:green'>".$actor."</b>.";
                    $btn['approver_agree'] = 1;
                } else {
                    $document['approver_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
                    $document['approver_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
                    $btn['approver_agree'] = 0;
                }

                // Get Approver
                $karyawan_id = $this->User_model->filter(" where \"user_id\"=".$document['creator_id'])->getOneFilter("karyawan_id");
                $approver_nopeg = $this->Atasan_model->filter(" where k_nopeg='".$karyawan_id."' ")->getOne("ATASAN1_NOPEG");
                $approvers[0]['id'] = $this->User_model->filter(" where \"karyawan_id\"='$approver_nopeg' ")->getOneFilter("user_id");
                $approvers[0]['name'] = $this->User_model->filter(" where \"user_id\"=".$approvers[0]['id'])->getOneFilter("name");
            break;
            case 'B':
                $actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("name");
                if ($actor!=NULL) {
                    $document['creator_status'] = "<b style='color:red'> This Creator is not served at this position </b>";
                    $document['creator_solution'] = " The Creator will be chosen automatically by system. The new Creator will be <b style='color:green'>".$actor."</b>.";
                    $btn['creator_agree'] = 1;
                } else {
                    $document['creator_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
                    $document['creator_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
                    $btn['creator_agree'] = 0;
                }

                $actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['approver_jabatan_id']."' ")->getOneFilter("name");
                if ($actor!=NULL) {
                    $document['approver_status'] = "<b style='color:red'> This Approver is not served at this position </b>";
                    $document['approver_solution'] = " The Approver will be chosen automatically by system. The new Approver will be <b style='color:green'>".$actor."</b>.";
                    $btn['approver_agree'] = 1;
                } else {
                    $document['approver_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
                    $document['approver_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
                    $btn['approver_agree'] = 0;
                }

                // Users bisa choose manual
                $users = $this->User_model->getAll(" where \"is_active\"=1 and \"company\"='$document_company' ");

                // Get Approver
                $karyawan_id = $this->User_model->filter(" where \"user_id\"=".$document['creator_id'])->getOneFilter("karyawan_id");
                $approver_nopeg = $this->Atasan_model->filter(" where k_nopeg='".$karyawan_id."' ")->getOne("ATASAN1_NOPEG");
                $approvers[0]['id'] = $this->User_model->filter(" where \"karyawan_id\"='$approver_nopeg' ")->getOneFilter("user_id");
                $approvers[0]['name'] = $this->User_model->filter(" where \"user_id\"=".$approvers[0]['id'])->getOneFilter("name");
            break;
            case 'C':
                $actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("name");
                if ($actor!=NULL) {
                    $document['creator_status'] = "<b style='color:red'> This Creator is not served at this position </b>";
                    $document['creator_solution'] = " The Creator will be chosen automatically by system. The new Creator will be <b style='color:green'>".$actor."</b>.";
                    $btn['creator_agree'] = 1;
                } else {
                    $document['creator_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
                    $document['creator_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
                    $btn['creator_agree'] = 0;
                }
                
                $document['approver_status'] = "<b style='color:green'>OK</b>";
                $document['approver_solution'] =NULL;

                // Users bisa choose manual
                $users = $this->User_model->getAll(" where \"is_active\"=1 and \"company\"='$document_company' ");
            break;
            case 'X':
                $document['workunit_status'] = "<b style='color:red'> This Workunit is not serve </b>";
                $document['creator_status'] = "<b>-</b>";
                $document['creator_solution'] = NULL;
                $document['approver_status'] = "<b>-</b>";
                $document['approver_solution'] =NULL;
            case 'O':
                $document['creator_status'] = "<b style='color:green'>OK</b>";
                $document['creator_solution'] = NULL;
                $document['approver_status'] = "<b style='color:green'>OK</b>";
                $document['approver_solution'] =NULL;
            break;
        }

        $reason = $this->model->getChangeunitRejection($id);
        $document['creator_reason'] = $reason['creator_reason'];
        $document['approver_reason'] = $reason['approver_reason'];
        // $document['workunit_reason'] = $reason['workunit_reason'];

        if ($document['changeunit_verification']['creator']==1 or $document['changeunit_verification']['approver']==1){
            $new = $this->model->getChangeWorkunitUserId($id);
            $new_users = array();
            if ($new['creator_id']!=NULL) {
                $new_users['creator'] = $this->User_model->filter(" where \"user_id\"='".$new['creator_id']."' ")->getOneFilter("name");
            }
            if ($new['approver_id']!=NULL) {
                $new_users['approver'] = $this->User_model->filter(" where \"user_id\"='".$new['approver_id']."' ")->getOneFilter("name");
            }
            $this->data['new'] = $new;
            $this->data['new_users'] = $new_users;
        }

        // if (!SessionManagerWeb::isAdministrator() and !SessionManagerWeb::isDocumentController()){
        if ($this->user['role'] != Role::DOCUMENT_CONTROLLER){
            if (!isset($new)){
                $new = $this->model->getChangeWorkunitUserId($id);
                if ($new['creator_id']!=$this->user['id'] and $new['approver_id']!=$this->user['id']){
                    die('Permission Denied');
                }
            } else {
                if ($new['creator_id']!=$this->user['id'] and $new['approver_id']!=$this->user['id']){
                    die('Permission Denied');
                }
            }
        }


        $this->data['title'] = $document['type_name'].' "'.$document['title'].'"';

        $this->data['document'] = $document;
        $this->data['btn'] = $btn;
        $this->data['users'] = $users;
        $this->data['company_choose'] = dbGetRows("select \"ID\" as \"id\", \"NAME\" as \"name\" from unitkerja where \"LEVEL\"='COMP'");
        $this->data['approvers'] = $approvers;
        $this->data['creator'] = $creator;
        // echo '<pre>';
        // print_r($this->data);
        // die();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
    }

    public function listChangeUnit($show_all=0){
        // if (!SessionManagerWeb::isAdministrator() and !SessionManagerWeb::isDocumentController()){
        if ($this->user['role']!=Role::DOCUMENT_CONTROLLER && $this->user['role']!=Role::ADMINISTRATOR){
            die('You cant access this page!');
        }
        $this->load->model("User_model");
        $this->load->model("Jabatan_model");
        $this->load->model("Document_types_model");

        $this->data['title'] = "Document List Change Unit";
        $company = $this->User_model->filter(" where \"user_id\"='".$this->user['id']."' ")->getOneFilter("company");
        $filter = '';
        // if($_SESSION['filter_changeunit']['keyword']){
        //     $filter .= "and lower(d.title) like '%".strtolower($_SESSION['filter_changeunit']['keyword'])."%'";
        // }
        // if($_SESSION['filter_changeunit']['arr_type']){
        //     $filter .= " and d.type_id in (".$_SESSION['filter_changeunit']['type'].")";
        // }
        // if($_SESSION['filter_changeunit']['arr_unitkerja']){
        //     $filter .= " and d.unitkerja_id in (".$_SESSION['filter_changeunit']['unitkerja'].")";
        // }
        $data = $this->model->filter($filter)->getAllUnitChange($company);
        foreach ($data as $k_data => $v_data) {
            $data[$k_data]['old']['creator_name'] = $this->User_model->filter(" where \"user_id\"='".$v_data['creator_id']."' ")->getOneFilter("name");
            $data[$k_data]['old']['approver_name'] = $this->User_model->filter(" where \"user_id\"='".$v_data['approver_id']."' ")->getOneFilter("name");

            // Get New Creator and Approver
            $data[$k_data]['new']['creator_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$v_data['creator_jabatan_id']."' ")->getOneFilter("user_id");
            $data[$k_data]['new']['approver_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$v_data['approver_jabatan_id']."' ")->getOneFilter("user_id");
            $data[$k_data]['new']['creator_name'] = $this->User_model->filter(" where \"user_id\"='".$data[$k_data]['new']['creator_id']."' ")->getOneFilter("name");
            $data[$k_data]['new']['approver_name'] = $this->User_model->filter(" where \"user_id\"='".$data[$k_data]['new']['approver_id']."' ")->getOneFilter("name");
            $data[$k_data]['new']['unitkerja_name'] = null;
            
            if ($data[$k_data]['new']['creator_id']==null or $data[$k_data]['new']['approver_id']==NULL){
                // echo "id:".$data[$k_data]['code']."<br>";
                // echo "creator:".$data[$k_data]['new']['creator_id']." ||| approver:".$data[$k_data]['new']['approver_id']."<br>";
                if ($show_all==0){
                    unset($data[$k_data]);
                } else {
                    $data[$k_data]['is_problematic'] = 1;
                }
                
            }
            elseif($data[$k_data]['new']['creator_id']!=null && $data[$k_data]['new']['approver_id']!=NULL && $v_data['unitkerja'] == 1){
                // echo "masuk unitkerja kosong<br>";
                if ($show_all==0){
                    unset($data[$k_data]);
                } else {
                    $data[$k_data]['is_problematic'] = 1;
                }
            }

        }

        // jenis dokumen
        // $all_types= $this->Document_types_model->getAllJenis();
        // $types = Util::toMap($all_types, 'id', 'type');

        $this->data['data'] = $data;
        $this->data['show_all'] = $show_all;
        $this->data['variables'] = PrivateApiController::getVariables($this->user);
        // echo "<pre>";print_r($this->data);die();
        // $this->template->viewDefault($this->view, $this->data);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
    }

    public function bulkChangeunit(){
        $this->load->model("User_model");
        $ids = $this->input->post('approve', true);
        foreach ($ids as $id) {
            $record = array();
            $record['approver'] = 0;
            $record['creator'] = 0;
            if (dbUpdate("document_changeunit", $record, "id=$id")){
                $document_id = $this->model->getOneChangeunit($id, "document_id");
                $this->changeunitCreateNewDocument($document_id);
            }

        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to move to new workunit document.'));
    }

    public function agreeWithSolution($id, $actor){
        // echo ",masuk fungsi agree";die();
        $this->load->model('User_model');
        $this->load->model("Unitkerja_model");
        $document = $this->model->getDocumentBy($this->user['id'],$id);
        // echo "<pre>";print_r($document);die();
        $record = array();
        $changeunit = array();
        if (strtolower($actor)=='creator'){
            $changeunit['creator_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("user_id");
            // $changeunit['creator_verification'] = 1;
            $changeunit['creator'] = 0;

            $type_id = $this->model->getOne($id, 'type_id');
            $approver = $this->Unitkerja_model->getApprover($changeunit['creator_id'], $type_id);
            $changeunit['approver'] = 0;
            $changeunit['approver_id'] = $approver['approver'][0]['id'];
            if ($changeunit['approver_id']==NULL){
                unset($changeunit['approver_id']);
                unset($changeunit['approver']);
            }
        } else if (strtolower($actor)=='approver') {
            $changeunit['approver_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$document['approver_jabatan_id']."' ")->getOneFilter("user_id");
            // $changeunit['approver_verification'] = 1;
            $changeunit['approver'] = 0;
            $is_move_creator = dbGetOne($sql);
            if (!$is_move_creator){
                $changeunit['is_moved']=1;
            }
        }
        // echo "<pre>";print_r($changeunit);die();
        if (dbUpdate("document_changeunit", $changeunit, "document_id=$id")){
            $this->load->model("Notification_model");
            if (strtolower($actor)=='creator'){

                $this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_CREATOR, $id, $this->user['id']);

                $record = array();
                $record['creator_id'] = $changeunit['creator_id'];
                $record['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['creator_id']."' ")->getOneFilter("jabatan_id");

                
                $record['approver_id'] = $changeunit['approver_id'];
                $record['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['approver_id']."' ")->getOneFilter("jabatan_id");
                $text .= "This document is moved from ".$document['creator_name']." as creator";

            } else if (strtolower($actor)=='approver') {
                $record = array();
                $record['approver_id'] = $changeunit['approver_id'];
                $record['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['approver_id']."' ")->getOneFilter("jabatan_id");
                $this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, $this->user['id']);
                $text .= "This document is moved from ".$document['approver_name']." as approver";
            }
            if (dbUpdate("documents", $record, "id=$id")){
                $sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$id  ";
                dbQuery($sql);
                if (strtolower($actor)=='creator'){
                    $this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, $this->user['id']);
                    // $this->moveChangeUnit($id, true);
                }
            }
            $this->addHistory($id, $text);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to Choose new User.'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Choose new User.'));
        }
    }

    public function ajaxUpdateCreator($id){
        $document = $this->model->getDocumentBy($this->user['id'], $id);
        $doc = array_change_key_case($document, CASE_LOWER);
        // echo "<pre>";print_r($doc);die();
        $this->load->model('Jabatan_model');
        $changeunit = array();
        // $changeunit['creator_verification'] = 1;
        $changeunit['creator'] = 0;
        $changeunit['creator_id'] = $this->input->post('creator_id', true);
        $changeunit['approver'] = 0;
        $changeunit['approver_id'] = $this->input->post('approver_id', true);
        // echo "<pre>";print_r($changeunit);die();
        if (dbUpdate("document_changeunit", $changeunit, "document_id=$id")){
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_CREATOR, $id, $this->user['id']);
            $this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, $this->user['id']);
            $record = array();
            // mengganti creator dan approver beserta jabatan masing2 ke dokumen yang sekarang(lama sebelum di gantikan)
            $record['creator_id'] = $changeunit['creator_id'];
            $record['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['creator_id']."' ")->getOneFilter("jabatan_id");
            $record['approver_id'] = $changeunit['approver_id']; // meng
            $record['approver_jabatan_id'] = $this->input->post('approver_jabatan_id', true);
            $record['unitkerja_id'] = $this->Jabatan_model->getOne($record['creator_jabatan_id'], "unitkerja_id");
            $codes=explode('/',$doc['code']);
            // echo "<pre>";print_r($record);die();
            // die();
            if($doc['unitkerja_id'] != $record['unitkerja_id']){
                if($doc['type_id'] == Document_model::INSTRUKSI_KERJA || $codes[0] == 'FI'){
                    $this->moveChangeUnit($id, false, $record); // harus kirim creator,approver beserta jabatannya untuk diinsertkan di data doc yang baru
                }
                else{
                    $this->load->model('Unitkerja_model');
                    dbUpdate("documents", $record, "id=$id");
                    $unitkerja_lama = $this->Unitkerja_model->show_sql(false)->getBy($doc['unitkerja_id']);
                    $unitkerja_baru = $this->Unitkerja_model->getBy($record['unitkerja_id']);
                        // print_r($unitkerja_lama);
                    $this->addHistory($id, "This document move from workunit ".$unitkerja_lama['name']." to ".$unitkerja_baru['name']);
                    $text = "This document is moved from ".$doc['creator_name']." as creator";
                    $this->addHistory($id, $text);
                }
            }
            else{
                dbUpdate("documents", $record, "id=$id");
                $text = "This document is moved from ".$doc['creator_name']." as creator";
                $this->addHistory($id, $text);
            }
            $sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$id  ";
            dbQuery($sql);           
            // mengganti creator dan approver beserta jabatan masing2 ke dokumen yang sekarang(lama sebelum di gantikan)
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to Choose new User.'), $id);
        } 
        else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Choose new User.'));
        }
    }

    public function moveChangeUnit($id, $is_redirect=true, $newCreatorApp=null){
        if($newCreatorApp!=null){
            // echo $id;die();
            $this->load->model('User_model');
            $this->load->model('Jabatan_model');
            $this->load->model('Atasan_model');
            $this->load->model('Unitkerja_model');
            $this->load->model('Workflow_model');

            $data = array();
            $document = $this->model->getDocumentBy($this->user['id'], $id);
            $doc = array_change_key_case($document, CASE_LOWER);
            $record = array();
            $record['is_moved'] = 1;
            dbUpdate("document_changeunit", $record, "document_id=$id");

            $data['user_id'] = $newCreatorApp['creator_id'];
            $data['title'] = $doc['title'];
            $data['is_upload'] = $doc['is_upload']; 
            $data['retension'] = $doc['retension'];
            $data['hash'] = $doc['hash'];
            $data['revision'] = 0;
            $data['status'] = Document_model::STATUS_SELESAI;
            $data['workflow_id'] = Workflow_model::PENGAJUAN;
            $data['workflow_urutan'] = 6;
            $data['show'] = 1;
            $data['is_published'] = 1;

            // Setting
            $data['type_id'] = $doc['type_id'];
            $data['creator_id'] = $newCreatorApp['creator_id'];
            $data['approver_id'] = $newCreatorApp['approver_id'];                   
            $data['prosesbisnis_id'] = $doc['prosesbisnis_id'];
            $data['user_jabatan_id'] = $newCreatorApp['creator_jabatan_id'];    
            $data['creator_jabatan_id'] = $newCreatorApp['creator_jabatan_id']; 
            $data['approver_jabatan_id'] = $newCreatorApp['approver_jabatan_id'];
            $data['unitkerja_id'] = $newCreatorApp['unitkerja_id'];

            $data['is_converted'] = $this->model->getOne($id, "is_converted");
            $data['have_header'] = 0;

            $unitkerja_baru = dbGetOne("select name from unitkerja where id='".$data['unitkerja_id']."'");


            // check code
            $codes=explode('/',$doc['code']);
            if($doc['company'] == null){
                $code = $this->model->getCodeInForm($data['type_id'],$data['prosesbisnis_id'],$data['unitkerja_id'],null, $newCreatorApp['company']);
                $data['code_number'] = $this->model->getLastCode($code, $param);
                $data['code'] = $code.''.$data['code_number'];
            }
            else{
                if ($data['type_id']==Document_model::INSTRUKSI_KERJA){
                    $param = " and unitkerja_id='".$data['unitkerja_id']."' ";
                } 
                else {
                    if ($type_id==Document_model::FORM){
                        if ($codes[0]=='FI'){
                            $param = " and unitkerja_id='".$data['unitkerja_id']."' ";
                        } else {
                            $param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
                        }
                    } else {
                        $param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
                    }
                }

                $code = $codes[0].'/'.$codes[1].'/'.$codes[2];
                if (strtoupper($codes[0])=='IK' or strtoupper($codes[0])=='FI'){
                    $code  .= '/'.$data['unitkerja_id'];
                }
                $data['code_number'] = $this->model->getLastCode($code, $param);
                $data['code'] = $code.'/'.$data['code_number'];
            }

            //CHANGE LOG
            $data['description'] = "This document is moved from document no. ".$doc['code']." Rev ".$doc['revision'];
            // Setting new Document
            $file_explode = explode('.', $doc['filename']); 
            $code_explode = explode('/',$data['code']);
            $code_implode = implode('_',$code_explode);
            // echo "<pre>";print_r($code_explode);die();
            $data['document'] = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
            $new_id = $this->addNewDocument($data);
            if ($new_id){   // jika berhasil insert, dokumen lama di obsoletkan
                $record = array();
                $record['is_obsolete'] = 1;
                $record['show'] = 0;
                dbUpdate('documents', $record, "id=$id"); // update obsolete dokumen                
            // echo "berhasil buat dokumen baru";
                // update tanggal
                $sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$new_id  ";
                dbQuery($sql);


                $this->addHistory($id, "This document is moved to document no. ".$data['code']);
                $this->addHistory($new_id, "This document is moved from document no. ".$doc['code']." Rev ".$doc['revision']);

                $document_id = $id;

                //Copy lama ke baru 
                $CI = & get_instance();
                $config = $CI->config->item('utils');
                $folder = "document/files";
                $full_path = $config['full_upload_dir'] . $folder . '/';
                $explodefilename = explode('.', $doc['filename']);
                $filename = $explodefilename[0].'.pdf';
                $idCompany = $this->Unitkerja_model->getOneBy($doc['unitkerja_id'], "\"COMPANY\"");


                $from = $full_path.$doc['filename'];
                $to = $full_path.$data['document'];

                copy($from, $to);

                if ($data['is_converted']==1) {
                    $old_docs = explode('.',$doc['filename']);
                    unset($old_docs[count($old_docs)-1]);
                    $old_doc = implode('.', $old_docs);
                    $new_docs = explode('.',$data['document']);
                    unset($new_docs[count($new_docs)-1]);
                    $new_doc = implode('.', $new_docs);

                    $from = $full_path.$old_doc.'.pdf';
                    $to = $full_path.$new_doc.'.pdf';
                    copy($from, $to);
                }

                if ($data['have_header']==1) {

                    rename($full_path.$explodefilename[0].'_header.pdf', './assets/uploads/document/files/'.$explodefilename[0].'_header_old.pdf');
                    $this->ObsoleteWatermark($filename, $idCompany, 'pdf');
                    unlink($full_path.'./assets/uploads/document/files/'.$explodefilename[0].'_header_old.pdf');


                    $old_docs = explode('.',$doc['filename']);
                    unset($old_docs[count($old_docs)-1]);
                    $old_doc = implode('.', $old_docs);
                    $new_docs = explode('.',$data['document']);
                    unset($new_docs[count($new_docs)-1]);
                    $new_doc = implode('.', $new_docs);

                    $from = $full_path.$old_doc.'_header.pdf';
                    $to = $full_path.$new_doc.'_header.pdf';
                    copy($from, $to);
                }

                if ($data['is_upload']==0){
                    $old_docs = explode('.',$doc['filename']);
                    unset($old_docs[count($old_docs)-1]);
                    $old_doc = implode('.', $old_docs);
                    $new_docs = explode('.',$data['document']);
                    unset($new_docs[count($new_docs)-1]);
                    $new_doc = implode('.', $new_docs);

                    $from = $full_path.$old_doc.'.txt';
                    $to = $full_path.$new_doc.'.txt';
                    copy($from, $to);
                }

                // reviewer
                if ($type['need_reviewer']=='1'){
                    $this->load->model('Document_reviewer_model');
                    $data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, $workflow_id);
                    $data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
                    $this->Document_reviewer_model->create($new_id,Workflow_model::REVISI, $data_reviewer);
                }
                
                // ISO
                $data_iso = array();
                $data_iso['document_id'] = $new_id;
                $iso_id = $this->model->getDocumentISOS($document_id);
                foreach ($iso_id as $key => $value) {
                    $data_iso['iso_id'] = $value;
                    dbInsert("DOCUMENT_ISOS", $data_iso);
                }

                // Klausul
                if ($type['klausul']=='1'){
                    $this->load->model('Document_klausul_model');
                    $data_klausul['document_id'] = $new_id;
                    $klausul = $this->Document_klausul_model->filter(" document_id=$document_id ")->getAll();
                    foreach ($klausul as $key => $value) {
                        $data_klausul['klausul_id'] = $value['klausul_id'];
                        dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
                    }
                }
            
                // Unit Kerja
                $this->load->model('Document_unitkerja_model');
                $unitkerja_related = $this->Document_unitkerja_model->filter(" where document_id=$document_id ")->getAll();
                if ($unitkerja_related!=NULL){
                    $data_unit_kerja = array();
                    foreach ($unitkerja_related as $k_unitkerja_related => $v_unitkerja_related) {
                        $data_unit_kerja = $v_unitkerja_related;
                        $data_unit_kerja['document_id'] = $new_id;
                        dbInsert('DOCUMENT_UNITKERJA', $data_unit_kerja);
                    }   
                }
                
                // Related dari dokumen lama ke dokumen lain
                $relatedDocument = dbGetRows("SELECT DOCUMENT2 as document2 FROM DOCUMENT_RELATED WHERE DOCUMENT1=".$document_id);
                $related = array();
                for ($i=0; $i < count($relatedDocument) ; $i++) { 
                    $related[$i]['document2'] = $relatedDocument[$i]['DOCUMENT2'];
                }
                $this->model->setRelatedDocument2($new_id,$related);

                //related dari dokumen lain ke dokumen ini
                dbUpdate("document_related", array('document2' => $new_id), "document2=".$document_id);
                $relatedDocument2 = dbGetRows("SELECT r.DOCUMENT1 as \"document_id\", d.document as \"document\" FROM DOCUMENT_RELATED r LEFT JOIN DOCUMENTS d ON r.DOCUMENT1 = d.id WHERE r.DOCUMENT2=".$document_id);
                foreach ($relatedDocument2 as $related) {
                    $doc_name = explode('.', $related['document']);
                    unlink($full_path.'./assets/uploads/document/files/'.$doc_name[0].'_header.pdf');
                    dbUpdate("documents", array('have_header' => 0), "id=".$related['document_id']);
                }
                // $relatedDocument = $this->input->post('related',true);
                

                // $data = array();
       //          $data['user_id'] = $data['creator_id'];
       //          $data['user_jabatan_id'] =$data['creator_jabatan_id'];

       //          dbUpdate('documents', $data, "id=$document_id");
                $unitkerja_lama = $this->Unitkerja_model->show_sql(false)->getBy($doc['unitkerja_id']);
                // $unitkerja_baru = dbGetOne("select name from unitkerja where id='".$data['unitkerja_id']."'");
                // $unitkerja_baru = $this->Unitkerja_model->column(array('name' => 'name'))->getBy($data['unitkerja_id']);
                    // print_r($unitkerja_lama);
                $this->addHistory($new_id, "This document moved from workunit ".$unitkerja_lama['name']." to ".$unitkerja_baru);
                $this->addHistory($id, "This document moved from workunit ".$unitkerja_lama['name']." to ".$unitkerja_baru);
                $text = "This document is moved from ".$doc['creator_name']." as creator";
                $this->addHistory($new_id, $text);
        //         if ($is_redirect){
        //          SessionManagerWeb::setFlashMsg(true, 'Success to move to new workunit document.');
                    // redirect("web/document/changeunit/".$id);
        //         }
                
            }
            return $new_id;
        }
        else{
            return null;
        }
        
    }

    public function ajaxUpdateApprover($id){
        $document = $this->model->getDocumentBy($this->user['id'], $id);
        $doc = array_change_key_case($document, CASE_LOWER);
        $changeunit = array();
        // $changeunit['approver_verification'] = 1;
        $changeunit['approver'] = 0;
        $changeunit['approver_id'] = $this->input->post('approver_id', true);
        $sql = "select creator from document_changeunit where document_id=$id";
        $is_move_creator = dbGetOne($sql);
        if (!$is_move_creator){
            $changeunit['is_moved']=1;
        }
        if (dbUpdate("document_changeunit", $changeunit, "document_id=$id")){
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, $this->user['id']);
            $text .= "This document is moved from ".$doc['approver_name']." as approver";
            $this->addHistory($id, $text);
            $record = array();
            $record['approver_id'] = $changeunit['approver_id'];
            $record['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['approver_id']."' ")->getOneFilter("jabatan_id");
            dbUpdate("documents", $record, "id=$id");
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to Choose new User.'), $id);
        } else {
           $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Choose new User.'));
        }
    }

    public function ajaxUpdateCompany($id){
        $document = $this->model->getDocumentBy($this->user['id'], $id);
        $doc = array_change_key_case($document, CASE_LOWER);        
        $this->load->model('Jabatan_model');
        $changeunit = $this->input->post();
        if(dbUpdate("document_changeunit", array('unitkerja' => 0, 'unitkerja_id' => $changeunit['workunit']), "document_id=$id")){
            $record['creator_id'] = $changeunit['creator_id'];
            $record['creator_jabatan_id'] = $changeunit['creator_jabatan_id'];
            $record['approver_id'] = $changeunit['approver_id'];
            $record['approver_jabatan_id'] =  $changeunit['approver_jabatan_id'];
            $record['unitkerja_id'] = $changeunit['workunit'];
            $record['company'] = $changeunit['company'];
            $new_id = $this->moveChangeUnit($id, false, $record);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to Choose new Workunit'), $new_id);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Choose new Workunit'));
        }
        // echo "<pre>";print_r($changeunit);die();
    }

    public function setChangeUnitReason($id){
        $this->load->model("User_model");
        $is_approver = $this->input->post('is_approver', true);
        $record = array();
        $new = $this->model->getChangeWorkunitUserId($id);
        if (!$is_approver){
            $creator = $this->User_model->filter(" where \"user_id\"='".$new['creator_id']."' ")->getOneFilter("name");
            $record['creator_reason'] = "<b style='color:green'>".$creator."</b> <b style='color:red'>REJECTED</b>, because <b>".$this->input->post('reason_textarea', true)."</b>";
            $record['creator_verification'] = 0;
            $record['creator_id'] = null;
        } else {
            $approver = $this->User_model->filter(" where \"user_id\"='".$new['approver_id']."' ")->getOneFilter("name");
            $record['approver_reason'] = "<b style='color:green'>".$approver."</b> <b style='color:red'>REJECTED</b>, because <b>".$this->input->post('reason_textarea', true)."</b>";
            $record['approver_verification'] = 0;
            $record['approver_id'] = null;
        }

        if (dbUpdate("document_changeunit", $record, "document_id=".$id)){
            if (!$is_approver){
                $this->load->model("Notification_model");
                $this->Notification_model->generate(Notification_model::ACTION_REJECT_CHANGEUNIT_CREATOR, $id, $this->user['id']);
            } else {
                $this->load->model("Notification_model");
                $this->Notification_model->generate(Notification_model::ACTION_REJECT_CHANGEUNIT_APPROVER, $id, $this->user['id']);
            }
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to Reject Verification'));            
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Reject Verification'));
        }
    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// END OF CHANGE UNIT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// START OF UPLOAD DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

    function getVariables(){
        $this->data = parent::getVariables();
        if($this->data){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// END OF UPLOAD DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// START OF DETAIL DOCUMENT PAGE ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function detailProps($id){
        $this->load->model('Workflow_model');
        $this->load->model('User_model');
        $this->load->model('Unitkerja_model');
        $drafter_evaluation = dbgetOne('select drafter_evaluation from documents where id='.$id);
        $this->data['data'] = $this->model->getDocumentBy($this->user['id'], $id);
        // echo "<pre>";print_r($this->data['data']);die();
        if($this->data['data']){
            $this->data['user'] = $this->User_model->getByAll($this->data['data']['user_id']);
            $this->data['user']['company_name'] = $this->Unitkerja_model->getOne($this->data['user']['company'], 'name');

            $this->data['type'] = $this->Document_types_model->get($this->data['data']['type_id']); // mendapatkan persyaratan dari tipe dokumen
            $this->data['data']['iso'] = $this->model->getDocumentISOS($id);
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'].'document/files/';
            $ukuran = $this->calculateSize(filesize($path.$this->data['data']['document']));
            if ($ukuran!='0 Bytes') {
                $this->data['data']['ukuran'] =$ukuran;
            }
            // Related Document
            $this->data['data']['docRelated'] = $this->model->getRelatedDocument($id);
            for ($i=0; $i < count($this->data['data']['docRelated']) ; $i++) {
                // echo "select name from unitkerja where id='".$doc[$i]['UNITKERJA_ID']."'";
                $this->data['data']['docRelated'][$i]['TYPE_NAME'] = dbGetOne("select type from document_types where id=".$this->data['data']['docRelated'][$i]['TYPE_ID']);
                $this->data['data']['docRelated'][$i]['UNITKERJA'] = dbGetOne("select name from unitkerja where id='".$this->data['data']['docRelated'][$i]['UNITKERJA_ID']."'");
            }

            // echo "<pre>";print_r($this->data);die();
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);

        }
        else{
            
        }
        
    }

    public function workflow($data, $drafter_evaluation){
        $this->load->model('Workflow_model');
        $this->load->model('User_model');
        $this->load->model('Unitkerja_model');
        $tasks= $this->Workflow_model->getTasks($this->data['data']['workflow_id'], null ,$drafter_evaluation);
        $this->data['tasks']= array();
        $this->data['user'] = $this->User_model->getByAll($this->data['data']['user_id']); 
        foreach ($tasks as $key => $task) {
            // proses skip flow
            $skip = 0;
            switch($this->data['data']['workflow_id']){
                case Workflow_model::PENGAJUAN: // jika dalam tahap pengajuan
                    if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR){
                        if ($this->data['type']['need_reviewer']=='0'){ // jika tidak membutuhkan reviewer, maka flow reviewer dilewati
                            $skip=1;
                        }
                    }
                    if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT))){ // jika tidak btuh reviewer dan form nya adalah review atribut atau review content, akan di lewati juga
                        $skip=1;    
                    }
                break;
                case Workflow_model::EVALUASI: // flow evaluasi
                    $this->data['drafter_evaluation'] = $drafter_evaluation;

                    if ($drafter_evaluation == null && $task['urutan']==0){ // untuk melewati eval by drafter jika tidak ada drafter
                        $skip=1;
                    }
                    if ($drafter_evaluation != null && $task['urutan']==1 ){ // jika ada draf_eval maka urutan 1 dlewati
                        $skip=1; 
                    }
                    if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT))) // jika tidak btuh reviewer dan form nya adalah review atribut atau review content, akan di lewati juga
                        $skip=1;
                    // if ($delegated==NULL and $task['urutan']==3){
                    if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $drafter_evaluation==null and $task['urutan']==3){
                        if ($this->data['type']['need_reviewer']=='0'){ // tidak butuh reviewer, drafter=creator, tidak ada draft_eval, dan urutan 3, maka urutan 3 di skip
                            $skip=1;
                        }
                    }
                break;
                case Workflow_model::REVISI: // jika workflow revisi
                    $this->data['drafter_evaluation'] = $drafter_evaluation;
                    if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR and $drafter_evaluation == null){
                        if ($task['urutan']!=1 and $this->data['type']['need_reviewer']=='0'){
                            $skip=1; // di lewati ketika drafter=creator dan proses tidak ada draft_eval, jika urutannya !=1 dan tidak butuh reviewer
                        }
                    }
                    if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT)))
                        $skip=1; // tidak bth reviewer, dan formnya adalah rev_atr atau rev_cont
                break;
            }

            if ($skip==0){
                $tasks[$key]['actor_name'] = $this->getActorName($this->data['data'],$task);
                $tasks[$key]['comment'] = $this->model->getComment($this->data['data']['document_id'], $task['urutan']);
                $this->data['tasks'][] = $tasks[$key];
                continue;
            }

            // $this->data['tasks'][$key]['actor_name'] = $this->getActorName($this->data['data'],$task);
            // $this->data['tasks'][$key]['comment'] = $this->model->getComment($this->data['data']['document_id'], $task['urutan']);
            
        }
        unset($this->data['user']);

        // echo "<pre>";print_r($this->data['tasks']);echo "</pre>";die();
        // echo json_encode($this->data['tasks']);die();
        return $this->data['tasks'];
    }


    public function workflowProcess($document_id){
        // $this->data['variables'] = parent::getVariables();
        $this->load->model('Workflow_model');
        $this->load->model('User_model');
        $this->load->model('Document_reviews_model');
		$this->load->model('Document_revises_model');											 
        $this->data['data'] = $this->model->getDocumentBy($this->user['id'], $document_id);
        // echo "<pre>";print_r($this->data['data']);die();
        
        // Cek History
        $this->load->model('Document_activity_model');
        $this->data['data']['history'] = $this->Document_activity_model->getHistory($document_id);
		$this->data['data']['revise'] = $this->Document_revises_model->column(array(
                                                                "dr.text" => "\"text\"",
                                                                "u.name" => "\"name\"",
                                                                "dr.id" => "\"id\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.user_id")->table('document_revises dr ')
                                                            ->filter(" document_id=".$document_id)
                                                            ->order('dr.id asc')->getBy();																			  
        $this->data['type'] = $this->Document_types_model->get($this->data['data']['type_id']);
        $drafter_evaluation = dbgetOne('select drafter_evaluation from documents where id='.$document_id);
        self::workflow($this->data['data'], $drafter_evaluation);

        if ($this->data['data']['status']==Document_model::STATUS_REVISE){
            $this->load->model('Document_revises_model');
            $this->data['revise_requestor'] = $this->Document_revises_model->getLastUser($document_id);
        }
        // echo "<pre>";print_r($this->data['task']);die();
        // $tasks= $this->Workflow_model->getTasks($workflow_id, $workflow_urutan ,$drafter_evaluation);
        $workflow_id = $this->data['data']['workflow_id'];
        $workflow_urutan = $this->data['data']['workflow_urutan'];
        $this->data['access'] = 0; // menentukan apakah user dapat mengakses form atau tidak
        $this->data['urutan'] = $workflow_urutan+1; // menentukan step selanjutnya(bisa berubah-ubah tergantung hasil dari verifikasi dsb)
        switch ($workflow_id) {
            case 1: // FLOW PENGAJUAN
                switch ($workflow_urutan) {
                    case 1:
                        if($this->user['id'] == $this->data['data']['user_id']){ // user yang diizinkan akses adalah drafternya
                            $this->data['access'] = 1;
						            $this->load->model('Document_reviewer_model');
                                    $reviewer = $this->Document_reviewer_model->getAllBy($document_id, 1);
                                    foreach ($reviewer as $value) {
                                        $this->data['data']['reviewer'][$value] = $value;
                                    }
                                    $this->data['data']['iso'] = $this->model->getDocumentISOS($document_id);
                                    $this->load->model('Document_iso_klausul_model');
                                    $this->load->model('Document_klausul_model');
                                    $docrealted = $this->model->getRelatedDocument($document_id);
                                    $this->data['data']['docRelated'] = ($docrealted);
                                    $klausul = $this->Document_klausul_model->
                                            column(array(
                                                "dk.klausul_id" => "\"value\"",
                                                "dik.klausul" => "\"label\""
                                            ))
                                            ->table("document_klausul dk")
                                            ->with(array("name"=>"Document_iso_klausul", "initial"=>"dik"), "dik.id=dk.klausul_id")
                                            ->filter(" document_id=$document_id ")
                                            ->getAll();
                                    $this->data['data']['klausul'] = ($klausul);
                                    $this->load->model('Document_unitkerja_model');
                                    $this->data['data']['unitkerja'] = $this->Document_unitkerja_model->getAllBy($document_id);																				   
                            if ($this->data['type']['need_reviewer']=='0'){ // jika tidak butuh reviewer(ik, form), maka akan di skip 
                                $this->data['urutan'] = $workflow_urutan+2;
                                if ($this->data['data']['creator_id']==$this->data['data']['user_id']){
                                    $this->data['urutan'] += 1;
                                    $this->data['approved_status'] = 'P';
                                }
                            }
                        }
                    break;
                    case 2:
                        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
                        $this->load->model('Document_reviewer_model');
                        $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                        foreach ($reviewer as $value) {
                            $this->data['data']['reviewer'][$value] = $value;
                        }
                        $this->load->model('Document_delegates_model');
                        $delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();
                        if(($this->user['role'] ==Role::DOCUMENT_CONTROLLER && $is_chief=='X' && $this->user['company'] == $this->data['data']['company']) || $delegated['delegate'] == $this->user['id'] ){
                            // reviewer
                            
                            
                            $this->data['data']['review'] = $this->Document_reviews_model
                                                            ->column(array(
                                                                "dr.document_id" => "\"document_id\"",
                                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                                "dr.review" => "\"review\"",
                                                                "u.name" => "\"reviewer_name\"",
                                                                "dr.is_agree" => "\"is_agree\"",
                                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                            ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                            ->getAll();

                            // echo "<pre>";print_r($this->data['data']['review']);die();
                            // count reviewer
                            $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);
                            // $this->data['variables'] = $this->getReviewer(true);
                            // reviewed
                            $this->data['variables']['reviewed'] = array();
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                                // $this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
                                // unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

                            }


                            // delegated
                            $this->load->model('Document_delegates_model');
                            $conditions = array(
                                " document_id=$document_id ", 
                                "workflow_id=$workflow_id", 
                                "workflow_urutan=$workflow_urutan"
                            );
                            $condition = implode(' and ', $conditions);
                            $delegated = $this->Document_delegates_model->filter($condition)->getBy();
                            $this->data['delegated'] = $delegated;
                            if($is_chief){
                                $this->data['is_chief'] = true;
                            }
                            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->user['company']."' ");

                            $this->data['data']['iso'] = $this->model->getDocumentISOS($document_id);

                            $this->load->model('Document_klausul_model');
                            $this->load->model('Document_iso_klausul_model');
                            $this->load->model('Document_unitkerja_model');
                            $iso_id = implode(',',$this->data['data']['iso']);

                            // $this->data['variables']['klausul'] = Util::toMap($this->Document_iso_klausul_model->filter(" iso_id in ($iso_id) ")->getAll(),'id', 'klausul');
                            $this->data['data']['unitkerja'] = $this->Document_unitkerja_model->getAllBy($document_id);

                            $klausul = $this->Document_klausul_model->
                                            column(array(
                                                "dk.klausul_id" => "\"value\"",
                                                "dik.klausul" => "\"label\""
                                            ))
                                            ->table("document_klausul dk")
                                            ->with(array("name"=>"Document_iso_klausul", "initial"=>"dik"), "dik.id=dk.klausul_id")
                                            ->filter(" document_id=$document_id ")
                                            ->getAll();
                            $this->data['data']['klausul'] = array_unique($klausul);
                            $this->data['urutan'] = 0;
                            // if(isset($this->data['data']['review'])){
                            //     $this->data['access'] = 0;
                            // }else{
                                $this->data['access'] = 1;
                            // }
                            
                        }
                        elseif(in_array($this->user['id'], $this->data['data']['reviewer'])){
                            $reviewed = dbGetOne("select id from document_reviews where document_id=".$document_id." and document_reviewer='".$this->user['id']."' ");
                            if(!$reviewed){
                                $this->data['access'] = 1;
                            }
                        }
						$this->data['data']['docRelated'] = $this->model->getRelatedDocument($document_id);
                        $this->data['approved_status'] = 'P';
                    break;
                    case 3:
                        if($this->user['id'] == $this->data['data']['creator_id']){
                            // reviewer
                            $this->load->model('Document_reviewer_model');
                            $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                            foreach ($reviewer as $value) {
                                $this->data['data']['reviewer'][$value] = $value;
                        }
                            $this->data['data']['review'] = $this->Document_reviews_model
                                                            ->column(array(
                                                                "dr.document_id" => "\"document_id\"",
                                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                                "dr.review" => "\"review\"",
                                                                "u.name" => "\"reviewer_name\"",
                                                                "dr.is_agree" => "\"is_agree\"",
                                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                            ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                            ->getAll();
                            $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);
                            // $this->data['variables'] = $this->getReviewer(true);
                            // reviewed
                            $this->data['variables']['reviewed'] = array();
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                                // $this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
                                // unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

                            }
                            $this->data['access'] = 1;
                            $this->data['approved_status'] = 'P';
                        }
                    break;
                    case 4:
                        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
                        $this->load->model('Document_delegates_model');
                        $delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();
						$this->load->model('Document_reviewer_model');
                        $reviewer = $this->Document_reviewer_model->getAllBy($document_id, 1);
                        foreach ($reviewer as $value) {
                            $this->data['data']['reviewer'][$value] = $value;
                        }
                        $this->data['data']['iso'] = $this->model->getDocumentISOS($document_id);
                        $this->load->model('Document_iso_klausul_model');
                        $this->load->model('Document_klausul_model');
                        $docrealted = $this->model->getRelatedDocument($document_id);
                        $this->data['data']['docRelated'] = ($docrealted);
                        $klausul = $this->Document_klausul_model->
                                column(array(
                                    "dk.klausul_id" => "\"value\"",
                                    "dik.klausul" => "\"label\""
                                ))
                                ->table("document_klausul dk")
                                ->with(array("name"=>"Document_iso_klausul", "initial"=>"dik"), "dik.id=dk.klausul_id")
                                ->filter(" document_id=$document_id ")
                                ->getAll();
                        $this->data['data']['klausul'] = ($klausul);
                        $this->load->model('Document_unitkerja_model');
                        $this->data['data']['unitkerja'] = $this->Document_unitkerja_model->getAllBy($document_id);																					   
                        if(($this->user['role']==Role::DOCUMENT_CONTROLLER && $is_chief=='X' && $this->user['company'] == $this->data['data']['company']) || $delegated['delegate'] == $this->user['id']){
                            $this->data['approved_status'] = 'P';
                            $this->load->model('Document_verification_model');
                            $this->data['data']['verification'] = $this->Document_verification_model->getTypeBy($document_id);
                            $counter = 0;
                            foreach ($this->data['data']['verification'] as $k_verification => $v_verification) {
                                if ($v_verification['is_appropriate']=='0')
                                    $counter++;
                            }
                            $this->data['data']['is_verification_appropriate']=$counter;
                            $this->data['approved_status'] = 'P';
                            $this->data['is_chief'] = true;
                            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->user['company']."' ");
                            $this->data['access'] = 1;

                        }
                    break;
                        case 5:
                        if($this->user['id'] == $this->data['data']['approver_id']){
                            $this->data['access'] = 1;
                            $this->data['approved_status'] = 'D';
                        }
                    break;
                }
            break;
            case 2: // FLOW EVALUASI
                switch ($workflow_urutan) {
                    case 0:
                        if($drafter_evaluation == $this->user['id']){
                            $this->data['skip'][] = 1;
                            $this->data['access'] = 1;
                            $this->load->model('Document_evaluation_model');
                            $evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
							$this->load->model('Document_reviewer_model');
                            $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                            foreach ($reviewer as $value) {
                                $this->data['data']['reviewer'][$value] = $value;
                            }
                             $this->load->model('Document_reviews_model');
                            $this->data['data']['review'] = $this->Document_reviews_model
                                                            ->column(array(
                                                                "dr.document_id" => "\"document_id\"",
                                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                                "dr.review" => "\"review\"",
                                                                "u.name" => "\"reviewer_name\"",
                                                                "dr.is_agree" => "\"is_agree\"",
                                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                            ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                            ->getAll();
                            $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                            }											   
                            if ($evaluation!=NULL){
                                $evaluation_text_counter = 0;
                                $evaluation['text'] = json_decode($evaluation['text'], true);
                                foreach ($evaluation['text'] as $key => $value) {
                                    if ($value=='1')
                                        $evaluation_text_counter++;
                                }
                                $evaluation['result'] = $evaluation_text_counter;
                                $this->data['evaluation'] = $evaluation;
                                if($this->data['evaluation']['result']==0){ // hasilnya cabut
                                    if($this->data['data']['type_id'] ==4 || $this->data['data']['type_id'] ==5){
                                        $this->data['urutan'] = 3;
                                    }
                                    else{
                                        $this->data['urutan'] = 2;
                                    }
                                    
                                }
                                elseif($this->data['evaluation']['result']> 0 and $this->data['evaluation']['result']<= 9){ //hasilnya revisi
                                    $this->data['skip'][] = 2;
                                    $this->data['skip'][] = 4;
                                    $this->data['skip'][] = 5;
                                    $this->data['urutan'] = 3;
                                }
                                $this->data['approved_status'] = 'P';
                            }
                            else{
                                $this->data['urutan'] = 2;
                                $this->data['approved_status'] = 'P';
                            }
                            // if ($this->data['evaluation']['result']==0){

                            // }
                        }
                    break;
                    case 1:
                        if($this->user['id'] == $this->data['data']['creator_id']){
                            $this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm($document_id);
                            $this->data['access'] = 1;
                            $this->load->model('Document_evaluation_model');
							$this->load->model('Document_reviewer_model');
                            $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                            foreach ($reviewer as $value) {
                                $this->data['data']['reviewer'][$value] = $value;
                            }
                             $this->load->model('Document_reviews_model');
                            $this->data['data']['review'] = $this->Document_reviews_model
                                                            ->column(array(
                                                                "dr.document_id" => "\"document_id\"",
                                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                                "dr.review" => "\"review\"",
                                                                "u.name" => "\"reviewer_name\"",
                                                                "dr.is_agree" => "\"is_agree\"",
                                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                            ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                            ->getAll();
                            $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);

                            }
                            $evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
                            if ($evaluation!=NULL){
                                $evaluation_text_counter = 0;
                                $evaluation['text'] = json_decode($evaluation['text'], true);
                                foreach ($evaluation['text'] as $key => $value) {
                                    if ($value=='1')
                                        $evaluation_text_counter++;
                                }
                                $evaluation['result'] = $evaluation_text_counter;
                                $this->data['evaluation'] = $evaluation;
                                if($this->data['evaluation']['result']==0){ // hasilnya cabut
                                    if($this->data['data']['type_id'] ==4 || $this->data['data']['type_id'] ==5){
                                        $this->data['urutan'] = 3;
                                }
                                    else{
                                        $this->data['urutan'] = 2;
                                    }
                                    $this->data['approved_status'] = 'P';
                                    
                                }
                                elseif($this->data['evaluation']['result']> 0 and $this->data['evaluation']['result']< 9){ //hasilnya revisi
                                    $this->data['skip'][] = 2;
                                    $this->data['skip'][] = 4;
                                    $this->data['skip'][] = 5;
                                    $this->data['urutan'] = 6;
                                    $this->data['approved_status'] = 'D';
                                }
                            }
                            else{
                                $this->data['approved_status'] = 'P';
                            }
                        }
                    break;
                    case 2:
                        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
                        // reviewer
                        $this->load->model('Document_reviewer_model');
                        $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                        foreach ($reviewer as $value) {
                            $this->data['data']['reviewer'][$value] = $value;
                        }
                        $this->load->model('Document_delegates_model');
                        $delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();
                        if(($this->user['role'] ==Role::DOCUMENT_CONTROLLER && $is_chief=='X' && $this->user['company'] == $this->data['data']['company']) || $delegated['delegate'] == $this->user['id'] ){
                            $this->load->model('Document_reviews_model');
                            $this->data['data']['review'] = $this->Document_reviews_model
                                                            ->column(array(
                                                                "dr.document_id" => "\"document_id\"",
                                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                                "dr.review" => "\"review\"",
                                                                "u.name" => "\"reviewer_name\"",
                                                                "dr.is_agree" => "\"is_agree\"",
                                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                            ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                            ->getAll();
                            // $this->data['variables'] = $this->getReviewer(true);

                            // count reviewer
                            $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);

                            // reviewed
                            $this->data['variables']['reviewed'] = array();
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                                // $this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
                                // unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

                            }
                            // delegated
                            $this->load->model('Document_delegates_model');
                            $conditions = array(
                                " document_id=$document_id ", 
                                "workflow_id=$workflow_id", 
                                "workflow_urutan=$workflow_urutan"
                            );
                            $condition = implode(' and ', $conditions);
                            $delegated = $this->Document_delegates_model->filter($condition)->getBy();
                            $this->data['delegated'] = $delegated;

                            $this->data['is_chief'] = true;
                            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->user['company']."' ");

                            $this->data['urutan'] = 0;
                            $this->data['access'] = 1;
                        }
                        elseif(in_array($this->user['id'], $this->data['data']['reviewer'])){
                            $this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm($document_id);
                            $this->load->model('Document_evaluation_model');
                            $evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
                            if ($evaluation!=NULL){
                                $evaluation_text_counter = 0;
                                $evaluation['text'] = json_decode($evaluation['text'], true);
                                foreach ($evaluation['text'] as $key => $value) {
                                    if ($value=='1')
                                        $evaluation_text_counter++;
                                }
                                $evaluation['result'] = $evaluation_text_counter;
                                $this->data['evaluation'] = $evaluation;
								if($this->data['evaluation']['result']==0){
                                    $this->data['approved_status'] = 'P';
                                }
                                elseif($this->data['evaluation']['result']> 0 and $this->data['evaluation']['result']< 9){ //hasilnya revisi
                                    $this->data['approved_status'] = 'D';
                                }
                            }else{
                                 $this->data['approved_status'] = 'P';										   									  
                            }
                            $this->data['access'] = 1;
                        }
                    break;
                    case 3:
                        if($this->data['data']['creator_id']==$this->data['data']['user_id']){
                            $this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm($document_id);
                            $this->data['access'] = 1;

                            $this->load->model('Document_evaluation_model');
                            $evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
                            if ($evaluation!=NULL){
                                $evaluation_text_counter = 0;
                                $evaluation['text'] = json_decode($evaluation['text'], true);
                                foreach ($evaluation['text'] as $key => $value) {
                                    if ($value=='1')
                                        $evaluation_text_counter++;
                                }
                                $evaluation['result'] = $evaluation_text_counter;
                                $this->data['evaluation'] = $evaluation;
                                if($this->data['evaluation']['result']==0){ // hasilnya cabut
                                    $this->data['approved_status'] = 'P';
                                }
                                elseif($this->data['evaluation']['result']> 0 and $this->data['evaluation']['result']< 9){ //hasilnya revisi atau relevan
                                    $this->data['skip'][] = 2;
                                    $this->data['skip'][] = 4;
                                    $this->data['skip'][] = 5;
                                    $this->data['approved_status'] = 'D';
                                    $this->data['urutan'] = 6;

                                    // reviewer
                                    $this->load->model('Document_reviewer_model');
                                    $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                                    foreach ($reviewer as $value) {
                                        $this->data['data']['reviewer'][$value] = $value;
                                    }
                                    $this->load->model('Document_reviews_model');
                                    $this->data['data']['review'] = $this->Document_reviews_model
                                                                    ->column(array(
                                                                        "dr.document_id" => "\"document_id\"",
                                                                        "dr.document_reviewer" => "\"document_reviewer\"",
                                                                        "dr.review" => "\"review\"",
                                                                        "u.name" => "\"reviewer_name\"",
                                                                        "dr.is_agree" => "\"is_agree\"",
                                                                        "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                                    ))
                                                                    ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                                    ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                                    ->getAll();
                                    // $this->data['variables'] = $this->getReviewer(true);
                                    // count reviewer
                                    $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);

                                    // reviewed
                                    $this->data['variables']['reviewed'] = array();
                                    foreach ($this->data['data']['review'] as $key => $review) {
                                        $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                        unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                                        // $this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
                                        // unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

                                    }
                                }
                            }

                        }
                    break;
                    case 4:
                        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
                        $this->load->model('Document_delegates_model');
                        $delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();
                        if(($this->user['role'] ==Role::DOCUMENT_CONTROLLER && $is_chief=='X' && $this->user['company'] == $this->data['data']['company']) || $delegated['delegate'] == $this->user['id'] ){
                            $this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm($document_id);
                            $this->data['access'] = 1;

                            $this->load->model('Document_evaluation_model');
                            $evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
                            if ($evaluation!=NULL){
                                $evaluation_text_counter = 0;
                                $evaluation['text'] = json_decode($evaluation['text'], true);
                                foreach ($evaluation['text'] as $key => $value) {
                                    if ($value=='1')
                                        $evaluation_text_counter++;
                                }
                                $evaluation['result'] = $evaluation_text_counter;
                                $this->data['evaluation'] = $evaluation;
                                if($this->data['evaluation']['result']==0){ // hasilnya cabut
                                    $this->data['approved_status'] = 'P';
                                    $this->load->model('Document_reviews_model');
                                    $this->data['data']['review'] = $this->Document_reviews_model
                                                                    ->column(array(
                                                                        "dr.document_id" => "\"document_id\"",
                                                                        "dr.document_reviewer" => "\"document_reviewer\"",
                                                                        "dr.review" => "\"review\"",
                                                                        "u.name" => "\"reviewer_name\"",
                                                                        "dr.is_agree" => "\"is_agree\"",
                                                                        "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                                    ))
                                                                    ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                                    ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                                    ->getAll();
                                    // $this->data['variables'] = $this->getReviewer(true);
                                }

                            }
                        }
                    break;
                    case 5:
                        if($this->data['data']['approver_id']==$this->user['id']){
                            $this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm($document_id);
                            $this->data['access'] = 1;

                            $this->load->model('Document_evaluation_model');
                            $evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
                            if ($evaluation!=NULL){
                                $evaluation_text_counter = 0;
                                $evaluation['text'] = json_decode($evaluation['text'], true);
                                foreach ($evaluation['text'] as $key => $value) {
                                    if ($value=='1')
                                        $evaluation_text_counter++;
                                }
                                $evaluation['result'] = $evaluation_text_counter;
                                $this->data['evaluation'] = $evaluation;
                                if($this->data['evaluation']['result']==0){ // hasilnya cabut
                                    $this->data['approved_status'] = 'D';
                                    $this->load->model('Document_reviews_model');
                                    $this->data['data']['review'] = $this->Document_reviews_model
                                                                    ->column(array(
                                                                        "dr.document_id" => "\"document_id\"",
                                                                        "dr.document_reviewer" => "\"document_reviewer\"",
                                                                        "dr.review" => "\"review\"",
                                                                        "u.name" => "\"reviewer_name\"",
                                                                        "dr.is_agree" => "\"is_agree\"",
                                                                        "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                                    ))
                                                                    ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                                    ->filter(" dr.document_id=$id and workflow_id=".$this->data['data']['workflow_id'])
                                                                    ->getAll();
                                    // $this->data['variables'] = $this->getReviewer(true);
                                }

                            }
                        }
                    break;
                }
            break;
            case 3: // flow revisi
				$tabParent = $this->model->getDocumentBy($this->user['id'], $this->data['data']['parent']);
                $this->data['data']['filenameold'] = $tabParent['filename'];
                $this->data['prevdoc']  = $this->viewdoc($this->data['data']['parent'],0);																						   																																  
                switch ($workflow_urutan) {
                    case 1:
						$this->load->model('Document_delegates_model');									   
						$delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();																																																									 
                        if($drafter_evaluation== $this->user['id'] || $this->data['data']['creator_id'] == $this->user['id']){
                            $ciConfig = $this->config->item('utils');
                            $path = $ciConfig['full_upload_dir'] . 'document/files/' ;
                            $full_filename = $path.$this->data['data']['filename'];
                            if (file_exists($full_filename) or $this->data['data']['is_upload']==0){
                                $this->data['is_file_exist'] = true;
                            }
                            else{
                                $this->data['is_file_exist'] = false;
                            }
							$this->load->model('Document_reviewer_model');
                            $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                            foreach ($reviewer as $value) {
                                $this->data['data']['reviewer'][$value] = $value;
                            }
                            $this->data['variables']['reviewed'] = array();
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                            }
                            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->user['company']."' ");																																														   
                            $this->data['access'] = 1;
                            $this->data['approved_status'] = 'P';
                        }
                    break;
                    case 2:
                        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
                        $this->load->model('Document_reviewer_model');
                        $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                        foreach ($reviewer as $value) {
                            $this->data['data']['reviewer'][$value] = $value;
                        }
                        $this->load->model('Document_delegates_model');
						$delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();																																																								 
                        if(($this->user['role'] ==Role::DOCUMENT_CONTROLLER && $is_chief=='X' && $this->user['company'] == $this->data['data']['company']) || $delegated['delegate'] == $this->user['id'] ){
                            // reviewer
                            
                            
                            $this->data['data']['review'] = $this->Document_reviews_model
                                                            ->column(array(
                                                                "dr.document_id" => "\"document_id\"",
                                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                                "dr.review" => "\"review\"",
                                                                "u.name" => "\"reviewer_name\"",
                                                                "dr.is_agree" => "\"is_agree\"",
                                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                            ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                            ->getAll();

                            // echo "<pre>";print_r($this->data['data']['review']);die();
                            // count reviewer
                            $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);
                            // $this->data['variables'] = $this->getReviewer(true);
                            // reviewed
                            $this->data['variables']['reviewed'] = array();
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                                // $this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
                                // unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

                            }


                            // delegated
                            $this->load->model('Document_delegates_model');
                            $conditions = array(
                                " document_id=$document_id ", 
                                "workflow_id=$workflow_id", 
                                "workflow_urutan=$workflow_urutan"
                            );
                            $condition = implode(' and ', $conditions);
                            $delegated = $this->Document_delegates_model->filter($condition)->getBy();
                            $this->data['delegated'] = $delegated;
                            if($is_chief){
                                $this->data['is_chief'] = true;
                            }
                            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->user['company']."' ");

                            $this->data['data']['iso'] = $this->model->getDocumentISOS($document_id);

                            $this->load->model('Document_klausul_model');
                            $this->load->model('Document_iso_klausul_model');
                            $this->load->model('Document_unitkerja_model');
                            $iso_id = implode(',',$this->data['data']['iso']);

                            // $this->data['variables']['klausul'] = Util::toMap($this->Document_iso_klausul_model->filter(" iso_id in ($iso_id) ")->getAll(),'id', 'klausul');
                            $this->data['data']['unitkerja'] = $this->Document_unitkerja_model->getAllBy($document_id);

                            $klausul = Util::ToList($this->Document_klausul_model->
                                            column(array(
                                                "dk.klausul_id" => "\"klausul_id\"",
                                                "dik.klausul" => "\"klausul_name\""
                                            ))
                                            ->table("document_klausul dk")
                                            ->with(array("name"=>"Document_iso_klausul", "initial"=>"dik"), "dik.id=dk.klausul_id")
                                            ->filter(" document_id=$document_id ")
                                            ->getAll(), 'klausul_id');
                            $this->data['data']['klausul'] = array_unique($klausul);
                            $this->data['urutan'] = 0;
                            // if(isset($this->data['data']['review'])){
                            //     $this->data['access'] = 0;
                            // }else{
                                $this->data['access'] = 1;
                            // }
                            
                        }
                        elseif(in_array($this->user['id'], $this->data['data']['reviewer'])){
                            $this->data['access'] = 1;
                        }
                        $this->data['approved_status'] = 'P';
                    break;
                    case 3:
                        if($this->user['id'] == $this->data['data']['creator_id']){
                            $this->data['data']['review'] = $this->Document_reviews_model
                                                            ->column(array(
                                                                "dr.document_id" => "\"document_id\"",
                                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                                "dr.review" => "\"review\"",
                                                                "u.name" => "\"reviewer_name\"",
                                                                "dr.is_agree" => "\"is_agree\"",
                                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                                            ))
                                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                                            ->filter(" dr.document_id=$document_id and workflow_id=".$this->data['data']['workflow_id'])
                                                            ->getAll();

                            // echo "<pre>";print_r($this->data['data']['review']);die();
                            // count reviewer
                            $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);
                            // $this->data['variables'] = $this->getReviewer(true);
                            // reviewed
                            $this->data['variables']['reviewed'] = array();
                            foreach ($this->data['data']['review'] as $key => $review) {
                                $this->data['data']['reviewed'][] = $review['document_reviewer'];
                                unset($this->data['data']['reviewer'][$review['document_reviewer']]);
                                // $this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
                                // unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

                            }
                            $this->data['access'] = 1;
                            $this->data['approved_status'] = 'P';
                        }
                    break;
                    case 4:
                        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
                        $this->load->model('Document_reviewer_model');
                        $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
                        foreach ($reviewer as $value) {
                            $this->data['data']['reviewer'][$value] = $value;
                        }
                        $this->load->model('Document_delegates_model');
                        $delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();
                        if(($this->user['role'] ==Role::DOCUMENT_CONTROLLER && $is_chief=='X' && $this->user['company'] == $this->data['data']['company']) || $delegated['delegate'] == $this->user['id'] ){
                            $this->data['approved_status'] = 'P';
                            $this->load->model('Document_verification_model');
                            $this->data['data']['verification'] = $this->Document_verification_model->getTypeBy($document_id);
                            $counter = 0;
                            foreach ($this->data['data']['verification'] as $k_verification => $v_verification) {
                                if ($v_verification['is_appropriate']=='0')
                                    $counter++;
                            }
                            $this->data['data']['is_verification_appropriate']=$counter;
                            $this->data['approved_status'] = 'P';
                            $this->data['is_chief'] = true;
                            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->user['company']."' ");
                            $this->data['access'] = 1;
                        }
                    break;
                    case 5:
                        if($this->user['id'] == $this->data['data']['approver_id']){
                            $this->data['access'] = 1;
                            $this->data['approved_status'] = 'D';
                        }
                    break;
                    
                }
            break;

        }
        unset($this->data['type']);
        // echo "<pre>";print_r($this->data);die();
        if($this->data['access'] == 1){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
        
        
        
    }

    private function deleteDocumentISOS($document_id){
        $sql ="delete from document_isos where document_id=$document_id";
        return dbQuery($sql);
    }

    private function deleteDocumentKlausul($document_id){
        $sql ="delete from document_klausul where document_id=$document_id";
        return dbQuery($sql);
    }

    public function update($document_id){
        // echo "<pre>";print_r($this->input->post('related'));die();
        // die();
        // echo '<pre>';
        // var_dump($this->input->post(null, true));
        // die();
        // Update data dokumen
        $data = array();
        $data['title'] = $this->input->post('input_judul',true);
        $data['description'] = $this->input->post('input_deskripsi',true);
        // $data['user_id'] = SessionManagerWeb::getUserID();
        // $data['creator_id'] = $this->input->post('input_creator',true);
        $data['approver_id'] = $this->input->post('approver_id',true);
        foreach ($data as $key => $value) {
            if ($value==NULL)
                unset($data[$key]);
        }

        $data = array_change_key_case($data, CASE_UPPER);
        $this->model->save($document_id, $data, true);

        // Check apakah kode berubah
        // if (isset($data['CODE'])){
        //  $ciConfig = $this->config->item('utils');
        //  $filename = $this->model->getOne($document_id, 'document');
        //  $full_path = $ciConfig['full_upload_dir'].'document/files/';
        //  $full_filename = $full_path.$filename;
        //  $file_split = explode('.', $filename);
        //  $ext_file = $file_split[1];
        //  // $split_name = explode('/', $document_code);
     //        $revision = '_R'.$this->model->getOne($document_id, 'revision');
        //     // $file_name = $pecah_nama[0].'_'.$pecah_nama[1]. '_'.$pecah_nama[2].'_'.$pecah_nama[3].'_'.$revision.'.'.$ext_file;
        //     $code_split = explode('/', $data['CODE']);
        //     $file_name = implode('_', $code_split);
        //     $file_name .= $revision.'.'.$ext_file;
        // }

        $workflow_id = $this->model->getOne($document_id, 'workflow_id');

        // already review
        // $this->load->model('Document_reviews_model');
        // $review = $this->Document_reviews_model
        //                                 ->column(array(
        //                                     "dr.document_id" => "\"document_id\"",
        //                                     "dr.document_reviewer" => "\"document_reviewer\"",
        //                                     "dr.review" => "\"review\"",
        //                                     "u.name" => "\"reviewer_name\"",
        //                                     "dr.is_agree" => "\"is_agree\"",
        //                                     "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
        //                                 ))
        //                                 ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
        //                                 ->filter(" dr.document_id=$document_id and workflow_id=".$workflow_id)
        //                                 ->getAll();
        // // reviewed
        // $reviewed = array();
        // foreach ($review as $key => $review) {
        //     $reviewed[] = $review['document_reviewer'];
        // }

        // $counter['reviewed'] = count($this->data['data']['reviewed']);

        //Change Title
        if($this->input->post('title', true)!=NULL){
            $title = $this->input->post('title', true);
            $this->model->updateTitle($document_id,$title);
            $id = SessionManagerWeb::getUserID();
            $this->load->model('User_model');
            $name = $this->User_model->filter("WHERE ID=$id")->getOneFilter('NAME');
            $this->addHistory($document_id, "Changed title attribute of this document to \"".$title."\"");
        }

        // Add Reviewer
        $this->load->model('Document_types_model');
        $type_id = $this->model->getOne($document_id, 'type_id');
        if ($this->Document_types_model->getBy($type_id, 'need_reviewer')){ // jika dia tidak butuh reviewer
            if ($this->input->post('input_reviewer', true)!=NULL or $reviewed!=NULL){
                $this->load->model('Document_reviewer_model');
                $reviewer_string = $this->input->post('input_reviewer', true);
                $reviewer = explode(',', $reviewer_string);
                foreach ($reviewed as $rev) {
                    if (!in_array($rev, $reviewer)){
                        $reviewer[] = $rev;
                    }
                }
                $this->Document_reviewer_model->deleteWith($document_id, $workflow_id);
                if ($reviewer!=NULL){
                    $this->Document_reviewer_model->create($document_id, $workflow_id, $reviewer);

                }
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Reviewer cant be Empty'));
            }
        }
        

                    
        // // ISO
        if ($this->input->post('input_iso', true)){
            $data_iso = array();
            $data_iso['document_id'] = $document_id;
            $iso_id_string = $this->input->post('input_iso', true);
            $iso_id = explode(',', $iso_id_string);

            $this->deleteDocumentISOS($document_id);
            foreach ($iso_id as $iso_id_key => $iso_id_value) {
                $data_iso['iso_id'] = $iso_id_value;
                dbInsert("DOCUMENT_ISOS", $data_iso);
            }      
        }


        // Klausul
        if ($this->input->post('input_iso_klausul', true)){
            $this->load->model('Document_types_model');
            $type_id = $this->model->getOne($document_id, 'type_id');
            $need_klausul = $this->Document_types_model->getBy($type_id, 'klausul');
            if ($need_klausul){
                $data_klausul['document_id'] = $document_id;
                $klausul_string = $this->input->post('input_iso_klausul', true);
                $klausul = explode(',', $klausul_string);
                $this->deleteDocumentKlausul($document_id);
                foreach ($klausul as $key_klausul => $value_klausul) {
                    $data_klausul['klausul_id'] = $value_klausul;
                    dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
                }
            }
        }

        // Add unitkerja terkait
        if ($this->input->post('input_unitkerjaterkait', true)) {
            $this->load->model('Document_unitkerja_model');
            $unitkerja_string = $this->input->post('input_unitkerjaterkait', true);
            $unitkerja = explode(',', $unitkerja_string);
            $this->Document_unitkerja_model->deleteWith($document_id);
            if ($unitkerja!=NULL)
                $this->Document_unitkerja_model->create($document_id, $unitkerja);
        }
        // Related
        if ($this->input->post('related',true)){
            $relatedDocument_string = $this->input->post('related',true);
            $relatedDocument = explode(',', $relatedDocument_string);
            $this->model->updateRelatedDocumentNew($document_id,$relatedDocument);
            // $this->model->setRelatedDocument($document_id,$relatedDocument);         
        }
        else{
            dbQuery("delete from document_related where document1=".$document_id);
        }


        // // Activity
        $this->load->model('Document_activity_model');
        $activity["user_id"] = $this->user['id'];
        $activity["document_id"] = $insert;
        $activity["activity"] = "Mengganti atribut dokumen";
        $activity["type"] = Document_activity_model::HISTORY;
        $activity = array_change_key_case($activity, CASE_UPPER);
        $this->Document_activity_model->create($activity,TRUE,TRUE);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to change the document attributes'));
        // SessionManagerWeb::setFlashMsg(true, 'Success to change the document attributes');
        // header('Location: ' . site_url('web/document/detail').'/'.$document_id);
    }

    public function download($id){
            
            $this->load->model('Document_activity_model');
            $activity["user_id"] = $this->user['id'];
            $activity["document_id"] = $id;
            $activity["activity"] = "Downloaded this Document.";
            $activity["type"] = Document_activity_model::ACTIVITY;
            //$this->Document_activity_model->create($activity,TRUE,TRUE);

            $file = $this->model->getDocumentWithLink($id, $this->user['id']);

            // $fsize = filesize($file['local']);

            // download baru 
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'].'document/files/';
            $fsize = filesize($path.$file['nama_file']);
            // echo '<pre>';
            // vaR_dump($file);
            // die();
            // $fullmime = Image::getMime($file['nama_file']);
            // $mimes = explode('/', $fullmime);
        
            $fname = basename($file['nama_file']);
            header('Content-Description: File Transfer'); 
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary"); 
            header('Expires: 0'); 
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
            header('Pragma: public'); 
            header("Content-disposition: attachment; filename=\"" . $fname . "\""); 
            header("Content-Length: ".$fsize);
            ob_clean();
            flush();
            // echo readfile($file['link']);
            return readfile($file['real_link']);
        }
    }


    //tambah reviewer
    public function ajaxAddReviewer(){
        $document_id = $this->input->post('document_id');
        $this->load->model('Document_reviewer_model');
        $reviewer_string = $this->input->post('reviewer', true);
        $reviewer = explode(',', $reviewer_string);
        $workflow_id = $this->model->getOne($document_id, 'workflow_id');

        $this->load->model('Document_reviews_model');
        $review = $this->Document_reviews_model
                                        ->column(array(
                                            "dr.document_id" => "\"document_id\"",
                                            "dr.document_reviewer" => "\"document_reviewer\"",
                                            "dr.review" => "\"review\"",
                                            "u.name" => "\"reviewer_name\"",
                                            "dr.is_agree" => "\"is_agree\"",
                                            "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                        ))
                                        ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                        ->filter(" dr.document_id=$document_id and workflow_id=".$workflow_id)
                                        ->getAll();
        // reviewed
        $reviewed = array();
        foreach ($review as $key => $review) {
            $reviewed[] = $review['document_reviewer'];
        }
        // $reviewer = explode(',', $reviewer);
        $this->Document_reviewer_model->deleteWith($document_id, $workflow_id);
        if ($reviewer!=NULL or $reviewed!=NULL){
            foreach ($reviewed as $rev) {
                if (!in_array($rev, $reviewer)){
                    $reviewer[] = $rev;
                }
            }
            $this->Document_reviewer_model->create($document_id, $workflow_id, $reviewer);
            $this->addHistory($document_id, "Added Reviewer ");
        }
    }

    // pendelegasian
    public function ajaxSetDelegates(){
        $data = $this->input->post(null, true);
        $this->load->model('Document_delegates_model');
        $this->Document_delegates_model->deleteBy($data);
        if ($this->Document_delegates_model->create($data)){

            // change drafter to delegated
            if ($data['workflow_urutan']==1){
                $record = array();
                $record['user_id'] = $data['delegate'];
                $this->load->model('User_model');
                $record['user_jabatan_id'] = $this->User_model->getBy($record['user_id'], 'user_jabatan_id');
                dbUpdate('Documents', $record, "id=".$data['document_id']);                 
            }

            // add History
            $this->load->model('Workflow_model');
            $task = $this->Workflow_model->getTasks($data['workflow_id'],$data['workflow_urutan']);
            $this->load->model('User_model');
            $delegate_name = $this->User_model->filter("where \"user_id\"=".$data['delegate'])->getOneFilter( "name");
            $this->addHistory($data['document_id'], "Delegates ".$delegate_name." to ".$task['name']);

            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_DELEGATE, $data['document_id'], $this->user['id'], $this->user['name']);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to delegates'));
        }
    }

    public function ajaxSetDocumentVerification(){
        $this->load->model('Document_verification_model');
        $this->load->model('Document_activity_model');
        $document_id = $this->input->post('document_id', true);
        $input = $this->input->post('json', true);
        $this->deleteVerification($document_id, Document_verification_model::TEMPLATE.','.Document_verification_model::TUGAS. ','.Document_verification_model::PENULISAN);
        $count_not_appropriate = 0;
        $activity = array();
        $activity["user_id"] = $this->user['id'];
        $activity["document_id"] = $document_id;
        $activity["activity"] = "Fill the Verification Form. ";
        $activity["type"] = Document_activity_model::HISTORY;
		$input = json_decode($input,true);								  
        foreach ($input as $key => $value) {
            $record = array();
            $record['document_id'] = $document_id;
            $record['text'] = $value['text'];
            $record['is_appropriate'] = $value['is_appropriate'];
            switch($key){
                case 'template':
                    $record['type'] = Document_verification_model::TEMPLATE; 
                break;
                case 'tugas':
                    $record['type'] = Document_verification_model::TUGAS; 
                break;
                case 'penulisan':
                    $record['type'] = Document_verification_model::PENULISAN; 
                break;

            }
            dbInsert('DOCUMENT_VERIFICATION', $record);
            if ($value['is_appropriate']==0){
                $type = $record['type'];
                $text = $this->Document_verification_model->filter(" where document_id='$document_id' and type='$type' and is_delete=0 ")->getOne("dbms_lob.substr(text , 30000, 1 )");
                $standar = $this->Document_verification_model->getType($record['type']);
                $activity["activity"] .= "\n- ".$standar." BELUM SESUAI ";
                if ($record['text']!=''){
                    $activity["activity"] .= " dengan catatan ".$text;
                } 
                $activity["activity"] .= '. ';
                $count_not_appropriate++;
            }
            elseif($value['is_appropriate'] == 1){
                $type = $record['type'];
                $text = $this->Document_verification_model->filter(" where document_id='$document_id' and type='$type' and is_delete=0 ")->getOne("dbms_lob.substr(text , 30000, 1 )");
                $standar = $this->Document_verification_model->getType($record['type']);
                $activity["activity"] .= "\n- ".$standar." SESUAI ";
                if ($record['text']!=''){
                    $activity["activity"] .= " dengan catatan ".$text;
                } 
                $activity["activity"] .= '. ';
                $count_not_appropriate++;
            }
        }
        if ($count_not_appropriate==0){
            $activity["activity"] = "Filled the document verification form and this document is appropriate with the given standard";
        }
        $this->Document_activity_model->create($activity,TRUE,TRUE);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to save the verification form'));
    }

    public function SetDocumentVerification($document_id){
        $this->load->model('Document_verification_model');
        $bab = explode('=//=',$this->input->post('bab', true));
        $old = explode('=//=',$this->input->post('old', true));
        $new = explode('=//=',$this->input->post('new', true));
        $record = array();
        $record['document_id'] = $document_id;
        $record['type'] = Document_verification_model::PERUBAHAN; 
        $record['is_appropriate'] = 1;
        $total_data = count($bab);
        $text = array();
        for ($i = 0; $i < $total_data; $i++) {
            if ($bab[$i]!='' and $old[$i]!='' and $new[$i]!=''){
                $text[] = array(
                    'bab' => $bab[$i],
                    'old' => $old[$i],
                    'new' => $new[$i]
                );
            }
        } 
        $record['text'] = json_encode($text);
        $this->deleteVerification($document_id, Document_verification_model::PERUBAHAN);
        if (dbInsert('DOCUMENT_VERIFICATION', $record)){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to save the verification form'));
        }
    }


    public function ajaxCancel(){
        $this->load->model('Document_evaluation_model');

        $document_id = (int)$this->input->post('document_id', true);
        $workflow_id = $this->model->getOne($document_id,'workflow_id');
        if($workflow_id == 2){
            // hapus hasil evaluasi 
            $delete = $this->Document_evaluation_model->deleteWith($document_id);
            // kembalikan workflow id 1 dan workflow urutan 6, status: D
            dbUpdate('documents', array('workflow_id' => 1, 'workflow_urutan' => 6, 'status' => 'D', 'drafter_evaluation' => null), "id=$document_id");
            // set history
            $this->addHistory($document_id, "Canceled this evaluation document because \"<b>".$this->input->post('text', true)."\"</b>");
        }
        elseif($workflow_id == 3){
            $prev_id = $this->model->getOne($document_id, 'parent');
            $prev_doc = array();
            $prev_doc['workflow_id'] = Workflow_model::PENGAJUAN;
            $prev_doc['status'] = Document_model::STATUS_SELESAI;
            $this->Document_evaluation_model->deleteWith($prev_id);
            $this->addHistory($document_id, "Canceled this revision document because \"<b>".$this->input->post('text', true)."\"</b>");
            $this->model->deleteDocument($document_id);
        }
        //echo $this->input->post('text', true);
		$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to cancel the evaluation Document'));																									   
    }

    function getActorName($data, $task){
        $this->load->model('Workflow_model');
        if ($task['user']==Role::CREATOR){
            $actor_name = $data['creator_name'];
        }
        else if($task['user']==Role::DRAFTER){
            $actor_name = $data['name'];
        }
         else if ($task['user']==Role::APPROVER){
            $actor_name = $data['approver_name'];
        } else if ($task['user']==Role::REVIEWER) {
            $this->load->model('Document_reviewer_model');
            $actor_name = $this->Document_reviewer_model->getAll($data['document_id'], $data['workflow_id']);
        } else if ($task['user']==Role::DOCUMENT_CONTROLLER){
            $this->load->model('Document_delegates_model');
            $document_id = $data['document_id'];
            $workflow_id = $data['workflow_id'];
            $workflow_urutan = $task['urutan'];
            $conditions = array(
                " document_id=$document_id ", 
                "workflow_id=$workflow_id", 
                "workflow_urutan=$workflow_urutan"
            );
            $condition = implode(' and ', $conditions);
            $delegated = $this->Document_delegates_model->filter($condition)->getBy();

            $this->load->model('User_model');

            // Kalau pada task ini ga delegasi, pakai kabiro SMSI
            // if (!$delegated or $workflow_urutan!=$task['urutan']){
            $role = Role::DOCUMENT_CONTROLLER;
            $company = $this->User_model->getBy($data['user_id'], 'company');
            if (!$delegated){
                $users = $this->User_model->getAll("where \"role\"='$role' and \"is_chief\"='X' and \"company\"='$company' ");
            } else {
                $user_id = $delegated['delegate'];
                $users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
            }
            $actor_name = $users[0]['name'];
        } else if($task['user']==Role::DRAFTER_EVALUATION){
            $this->load->model('User_model');
            $drafter_evaluation = dbGetOne('select drafter_evaluation from documents where id='.$data['document_id']);
            $users = $this->User_model->show_sql(false)->getAll(" where \"user_id\"='".$drafter_evaluation."' ");
            $actor_name = $users[0]['name'];
        }
        switch($data['workflow_id']){
            case Workflow_model::EVALUASI:
                switch($task['urutan']){
                    case 1:
                        $this->load->model('Document_delegates_model');
                        $document_id = $data['document_id'];
                        $workflow_id = $data['workflow_id'];
                        $workflow_urutan = $task['urutan'];
                        $conditions = array(
                            " document_id=$document_id ", 
                            "workflow_id=$workflow_id", 
                            "workflow_urutan=$workflow_urutan"
                        );
                        $condition = implode(' and ', $conditions);
                        $delegated = $this->Document_delegates_model->filter($condition)->getBy();

                        $this->load->model('User_model');
                        if (!$delegated){
                            $user_id = $data['creator_id'];
                            $users = $this->User_model->getAll("where \"user_id\"='$user_id' ");
                        } else {
                            $user_id = $delegated['delegate'];
                            $users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
                        }
                        $actor_name = $users[0]['name'];
                    break;
                }
            break;
            case 3:
                switch($task['urutan']){
                    case 1:
                        if($task['user']==Role::DRAFTER_REVISION){
                            $this->load->model('User_model');
                            $drafter_evaluation = dbGetOne('select drafter_evaluation from documents where id='.$data['document_id']);
                            $users = $this->User_model->show_sql(false)->getAll(" where \"user_id\"='".$drafter_evaluation."' ");
                            $actor_name = $users[0]['name'];
                        }
                        else{
                            $this->load->model('User_model');
                            $actor_name = $this->User_model->getBy($data['user_id'], 'name');
                            return $actor_name;
                        }
                        
                    break;
                }
            break;

        }        
        return $actor_name;
    }


    public function updateDocumentFiles(){
        $document_id = $this->input->post('document_id', true);
        $user_id = $this->model->getOne($document_id, 'user_id');

        if ($this->input->post('document_text')!=NULL and $this->input->post('document_text')!=""){
            if ($user_id==NULL)
                $user_id = $this->user['id'];
            $id = md5($user_id. $this->config->item('encryption_key'));
            $ciConfig = $this->config->item('utils');
            $folder_dest = 'document/files';
            $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
            $file = $this->model->getOne($document_id, 'document');
            $revise = $this->model->getOne($document_id, 'revision');
            // $basename = basename($file);
            // $fil = basename($file);
            // $hash = hash_hmac('sha256',$fil.$document_id, $user_id);
            // $file_name = File::getFileName($hash, $folder_dest) . '.txt';
            $files = explode('.', $file);
            // remove extension
            unset($files[count($files)-1]);
            // $file_name = implode('.', $files).'_R'.$revise.'.txt';
            $file_name = implode('.', $files).'.txt';
            file_put_contents($path_dest.$file_name, $this->input->post('document_text'));
        }
        if ($this->_moveDocumentTemp($document_id, 'document/temp',$this->model->getOne($document_id, 'code') , NULL)) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), 1); 
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $message), 0); 
        }
    }

    public function detailDoc($id){
        $data =  $this->model->getDocumentBy($this->user['id'], $id);


        // get reviewer name

        $this->load->model('Document_reviewer_model');
        $reviewer_name = $this->Document_reviewer_model->getAll($data['document_id'], $data['workflow_id']);
        // GET DOC CON NAME
        $this->load->model('Document_delegates_model');
        $document_id = $data['document_id'];
        $workflow_id = $data['workflow_id'];
        $workflow_urutan = $data['workflow_urutan'];
        $conditions = array(
            " document_id=$document_id ", 
            "workflow_id=$workflow_id", 
            "workflow_urutan=$workflow_urutan"
        );
        $condition = implode(' and ', $conditions);
        $delegated = $this->Document_delegates_model->filter($condition)->getBy();

        $this->load->model('User_model');

        // Kalau pada task ini ga delegasi, pakai kabiro SMSI
        // if (!$delegated or $workflow_urutan!=$task['urutan']){
        $role = Role::DOCUMENT_CONTROLLER;
        $company = $this->User_model->getBy($data['user_id'], 'company');
        if (!$delegated){
            $users = $this->User_model->getAll("where \"role\"='$role' and \"is_chief\"='X' and \"company\"='$company' ");
        } else {
            $user_id = $delegated['delegate'];
            $users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
        }
        $document_controller_name = $users[0]['name'];
        if ($data['type_id'] < Document_model::FORM)
            $stepProgress = array(
                array('key'=>'1','status'=>'Drafter','progress' =>'Document Drafting' ,'nama'=>$data['name']),
                array('key'=>'2','status'=>'Document Controller','progress' =>'Attribute Review' ,'nama'=>$document_controller_name),
                array('key'=>'3','status'=>'Reviewer','progress' =>'Draft Content Review' ,'nama'=> array_unique(explode('_ ',$reviewer_name))),
                array('key'=>'4','status'=>'Creator','progress' =>'Approval' ,'nama'=>$data['creator_name']),
                array('key'=>'5','status'=>'Document Controller','progress' =>'Verification' ,'nama'=>$document_controller_name),
                array('key'=>'6','status'=>'Approver','progress' =>'Review and Approval' ,'nama'=>$data['approver_name'])
            );
        else $stepProgress = array(
                array('key'=>'1','status'=>'Drafter','progress' =>'Document Drafting' ,'nama'=>$data['name']),
                array('key'=>'4','status'=>'Creator','progress' =>'Approval' ,'nama'=>$data['creator_name']),
                array('key'=>'5','status'=>'Document Controller','progress' =>'Verification' ,'nama'=>$document_controller_name),
                array('key'=>'6','status'=>'Approver','progress' =>'Review and Approval' ,'nama'=>$data['approver_name'])
            );
        // else

        $this->data['data'] = $data;
        $this->data['stepProgress'] = $stepProgress;
        $this->data['data']['iso'] = $this->model->getDocumentISOS($id);
        $this->load->model('Document_unitkerja_model');
        $this->data['data']['unitkerja'] = $this->Document_unitkerja_model->getAllBy($this->data['data']['document_id']);

        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'].'document/files/';
        $ukuran = $this->calculateSize(filesize($path.$this->data['data']['document']));;
        if ($ukuran!='0 Bytes') {
            $this->data['data']['ukuran'] =$ukuran;
        }

        // Related Workunit
        $this->load->model('Document_unitkerja_model');
        $related = $this->Document_unitkerja_model->filter(" where document_id=$id ")->getAll();
        $this->data['data']['related_workunit'] = Util::toList($related, 'document_unitkerja');

        // Related Document
        $this->data['data']['docRelated'] = $this->model->getRelatedDocument($id);

        // Cek History
        $this->load->model('Document_activity_model');
        $this->data['data']['history'] = $this->Document_activity_model->getHistory($id);


        // Cek Reviewer
        $this->load->model('Document_reviewer_model');
        $reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
        foreach ($reviewer as $value) {
            $this->data['data']['reviewer'][$value] = $value;
        }
        $this->load->model('Document_reviews_model');
        $this->data['data']['review'] = $this->Document_reviews_model
                                        ->column(array(
                                            "dr.document_id" => "\"document_id\"",
                                            "dr.document_reviewer" => "\"document_reviewer\"",
                                            "dr.review" => "\"review\"",
                                            "u.name" => "\"reviewer_name\"",
                                            "dr.is_agree" => "\"is_agree\"",
                                            "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                        ))
                                        ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                        ->filter(" dr.document_id=$id and workflow_id=".$this->data['data']['workflow_id'])
                                        ->getAll();
        // count reviewer
        $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);

        // count reviewer
        $this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);

        // reviewed
        $this->data['variables']['reviewed'] = array();
        foreach ($this->data['data']['review'] as $key => $review) {
            $this->data['data']['reviewed'][] = $review['document_reviewer'];
            unset($this->data['data']['reviewer'][$review['document_reviewer']]);
            $this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
            unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

        }

        // count jml review
        $this->data['counter']['reviewed'] = count($this->data['data']['reviewed']);
        if ($this->data['counter']['reviewer']<=$this->data['counter']['reviewed'] and $this->data['counter']['reviewed']>0){
            $this->data['document_controller']['allow_next'] = 1;
        } else {
            $this->data['document_controller']['allow_next'] = 0;
        }


        $this->load->model('User_model');
        $this->data['user'] = $this->User_model->getByAll($this->data['data']['user_id']);
        $this->load->model('Unitkerja_model');
        $this->data['user']['company_name'] = $this->Unitkerja_model->getOne($this->data['user']['company'], 'name'); 
        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
        $this->data['is_chief'] = false;

        $role = dbGetOne("select role from users where id='$this->user['id']'");
        if (($is_chief=='X') and $role==Role::DOCUMENT_CONTROLLER) {
            $this->data['is_chief'] = true;
            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->data['variables']['mycompany']."' ");
        } 
        $this->load->model('Workflow_model');
        $this->data['tasks']= $this->Workflow_model->getTasks($this->data['data']['workflow_id']);
        $task_keys = array();
        foreach ($this->data['tasks'] as $key => $task) {
            $skip = 0;
            switch($this->data['data']['workflow_id']){
                case Workflow_model::PENGAJUAN:
                    if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR){
                        if ($this->data['type']['need_reviewer']=='0'){
                            $skip=1;
                        }
                    }
                    // if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT))){
                    //  $skip=1;    
                    // }
                    if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT))){
                        $skip=1;    
                    }
                break;
                case Workflow_model::EVALUASI:
                    // Document delegates
                    //          $this->load->model('Document_delegates_model');
                    //          $document_id = $this->data['data']['document_id'];
                    // $workflow_id = Workflow_model::EVALUASI;
                    // $workflow_urutan = 1;
                    // $conditions = array(
                    //  " document_id=$document_id ", 
                    //  "workflow_id=$workflow_id", 
                    //  "workflow_urutan=$workflow_urutan"
                    // );
                    // $condition = implode(' and ', $conditions);
                    // $delegated = $this->Document_delegates_model->filter($condition)->getBy();
                    // switch($this->data['data']['workflow_urutan']){
                    //  case 1:
                    //      $this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm();
                    //      $this->data['delegated'] = $delegated;
                    //  break;
                    // }
                    if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT)))
                        $skip=1;
                    // if ($delegated==NULL and $task['urutan']==3){
                    if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['urutan']==3){
                        if ($this->data['type']['need_reviewer']=='0'){
                            $skip=1;
                        }
                        
                    }

                    if ($this->data['evaluation']['result']!=0 and $task['urutan']==4){
                        $skip=1;
                    }

                    if ($this->data['evaluation']['result']>0 and $this->data['evaluation']['result']<9 and $task['urutan']==5){
                        $skip=1;
                    }
                break;
                case Workflow_model::REVISI:
                    if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR){
                        if ($task['urutan']!=1 and $this->data['type']['need_reviewer']=='0'){
                            $skip=1;    
                        }
                    }
                    if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT)))
                        $skip=1;
                break;
            }

            // Get Klausul
            if ($this->data['type']['klausul']=='1' and $task['edit_attribute']==1){
                $this->load->model('Document_klausul_model');
                $this->load->model('Document_iso_klausul_model');
                $iso_id = implode(',',$this->data['data']['iso']);
                $this->data['variables']['klausul'] = Util::toMap($this->Document_iso_klausul_model->filter(" iso_id in ($iso_id) ")->getAll(),'id', 'klausul');
                $this->data['data']['klausul'] = Util::ToList($this->Document_klausul_model->
                                                column(array(
                                                    "dk.klausul_id" => "\"klausul_id\"",
                                                    "dik.klausul" => "\"klausul_name\""
                                                ))
                                                ->table("document_klausul dk")
                                                ->with(array("name"=>"Document_iso_klausul", "initial"=>"dik"), "dik.id=dk.klausul_id")
                                                ->filter(" document_id=$id ")
                                                ->getAll(), 'klausul_id');
            }
            if ($skip==1){
                unset($this->data['tasks'][$key]);
                continue;
            }
            // if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR and $this->data['data']['workflow_id']==Workflow_model::PENGAJUAN){
            //  unset($this->data['tasks'][$key]);
            //  continue;
            // }
            if ($task['urutan']==$this->data['data']['workflow_urutan']){

                $this->data['data']['edit'] = $task['edit'];
                // Check variables untuk form
                switch($task['form']){
                    case Workflow_model::VERIFICATION:
                        $this->load->model('Document_verification_model');
                        $this->data['data']['verification'] = $this->Document_verification_model->getTypeBy($id);
                        $counter = 0;
                        foreach ($this->data['data']['verification'] as $k_verification => $v_verification) {
                            if ($v_verification['is_appropriate']=='0')
                                $counter++;
                        }
                        $this->data['data']['is_verification_appropriate']=$counter;

                        // Approver -> sementara pengajuan saja
                        // if ($this->data['data']['workflow_id']==Workflow_model::PENGAJUAN){
                        //     $mycompany = $this->User_model->getBy($this->user['id'], 'company');
                        //     $user_approvers = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\""))->getAll(" where \"company\"='$mycompany' ");
                        //     $this->data['approvers'] = Util::toMap($user_approvers, "user_id", "name");
                        //     $this->data['approvers'][null] = "Choose Approver.";
                        //     // $this->data['approvers'][] = array(
                        //     //  "user_id" => null,
                        //     //  "name" => "Choose Approver"
                        //     // );
                        // }
                    break;
                    case Workflow_model::EVALUATION:
                        //Get Unitkerjanya
                        foreach ($this->data['variables']['myunitkerja'] as $k_myunitkerja => $v_myunitkerja) {
                            $myunitkerja = $k_myunitkerja;
                            break;
                        }
                        $min_drafter = $this->Document_types_model->filter(" id=".$this->data['type']['id'])->getOne('eselon_drafter');
                        $this->load->model('Atasan_model');
                        $nopeg = $this->User_model->getOne($this->user['id'], 'karyawan_id');
                        // $bawahan = $this->User_model->getAllBy(" \"user_id\"!='".SessionManagerWeb::getUserID()."' and \"unitkerja_id\"='".$myunitkerja."' ");
                        $bawahan = $this->Atasan_model->getDelegates($this->data['variables']['subordinates'], $min_drafter);
                        $this->data['subordinates'] = $bawahan;

                    break;
                    case Workflow_model::CREATION:
                        if ($this->data['data']['workflow_id']==Workflow_model::REVISI){
                            foreach ($this->data['variables']['myunitkerja'] as $k_myunitkerja => $v_myunitkerja) {
                                $myunitkerja = $k_myunitkerja;
                                break;
                            }
                            $this->load->model('Atasan_model');
                            // $bawahan = $this->User_model->getAllBy(" \"user_id\"!='".SessionManagerWeb::getUserID()."' and \"unitkerja_id\"='".$myunitkerja."' ");
                            
                            $min_drafter = $this->Document_types_model->filter(" id=".$this->data['type']['id'])->getOne('eselon_drafter');
                            $nopeg = $this->User_model->getOne($this->user['id'], 'karyawan_id');
                            $bawahan = $this->Atasan_model->getDelegates($this->data['variables']['subordinates'], $min_drafter);
                            $this->data['subordinates'] = $bawahan;
                            
                        }
                    break;
                    case Workflow_model::REVIEW_CONTENT || Workflow_model::REVIEW_ATRIBUT :
                        if ($this->data['data']['workflow_id']==Workflow_model::REVISI){
                            $this->data['approver_value'][0] = $this->data['data']['approver_id'];
                        }
                    break;
                }
                // echo $min_drafter.'<br>';
                // echo '<pre>';
                // vaR_dump($this->data['variables']);
                // die();

                // if ($task['form']==Workflow_model::VERIFICATION){
                //  $this->load->model('Document_verification_model');
                //  $this->data['data']['verification'] = $this->Document_verification_model->getTypeBy($id);
                // }
            }
            if ($task['end']==1)
                continue;
            // $this->data['tasks'][$key]['actor_name'] = $this->getActorName($this->data['data'],$task);
            $this->data['tasks'][$key]['comment'] = $this->model->getComment($id, $task['urutan']);
            $task_keys[] = $key;
        }
        // echo "<pre>";print_r($this->data);die();
        // echo "<pre>";print_r(array('data'=>$result,'stepProgress'=>$stepProgress));die(); 
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data); 
    }


    public function getDelegates($company){
        $is_chief = $this->User_model->getBy($this->user['id'], 'is_chief');
        $this->data['is_chief'] = false;
        $role = dbGetOne("select role from users where id='".$this->user['id']."'");
        if (($is_chief=='X') and $role==Role::DOCUMENT_CONTROLLER) {
            $this->data['is_chief'] = true;
            $this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$company."' ");
        }
        // echo "<pre>";print_r($this->data);die();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data);
    }

    // public function getVariables(){
    //     // Company
    //     $this->load->model('User_model');
    //     $mycompany = $this->User_model->getBy($this->user['id'], 'company');
    //     $data['mycompany'] = $mycompany;

    //     // Proses Bisnis
    //     $this->load->model('Document_prosesbisnis_model');
    //     $data['prosesbisnis'] = Util::toMap($this->Document_prosesbisnis_model->getAll(), 'id', 'prosesbisnis');

    //     // jenis doc
    //     $this->load->model('Document_types_model');
    //     // if (SessionManagerWeb::isAdministrator()) {
    //     //     $jenis = $this->Document_types_model->getAllJenis();
    //     // } else {
    //         $this->load->model('Tingkatan_document_types_model');
    //         // sebelumnya pakai jabatan
    //         // $tingkatan = $this->User_model->getJabatanGroup(SessionManagerWeb::getUserID());
    //         // sekarang pakai eselon karyawan
    //         $tingkatan = $this->User_model->getEselon($this->user['id']);
    //         $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
    //         $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

    //     // }
    //     $this->load->model("Document_model");
    //     foreach ($jenis as $k_jenis => $v_jenis) {
    //         if ($v_jenis['id']==Document_model::BOARD_MANUAL){
    //             unset($jenis[$k_jenis]);
    //         }
    //     }
    //     $data['jenis'] =  $jenis;

    //     $data['document_types'] = $this->Document_types_model->getAllJenis();

    //     // Reviewer, Approver, Unitkerja
    //     $user = $this->User_model->getBy($this->user['id'], 'unitkerja_id');
    //     // $this->data['reviewer'] = Util::toMap($this->User_model->getAllBy("\"role\"='".Role::REVIEWER."'"), 'user_id', 'name'); 
    //     // $data['reviewer'] = Util::toMap($this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\""))->getAll(" where \"company\"='$mycompany' "), 'user_id', 'name');
    //     $reviewer = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\""))->getAll(" where \"company\"='$mycompany' ");
    //     foreach ($reviewer as $key => $row) {
    //         $data['reviewer'][$row['user_id']] = $row['unitkerja_name'].' - '.$row['name'];
    //     }

    //     $this->load->model('Unitkerja_model');
    //     $companies = $this->Unitkerja_model->getAll(" and parent='0' ");
    //     foreach ($companies as $comp) {
    //         $company[ltrim($comp['id'], '0')] = $comp['name']; 
    //     }

    //     // get ALl User
    //     // $users = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\"", "\"company\""=>"\"company\"", "\"karyawan_id\""=>"\"karyawan_id\""))->getAll();
    //     // foreach ($users as $key => $row) {
    //     //     $data['karyawan'][$row['karyawan_id']] = $row['name'] .' - '.$row['unitkerja_name'].' - '.$company[$row['company']];
    //     // }
    //     // $data['karyawan'][NULL] = NULL;
    //     $data['unitkerja'] =$this->Unitkerja_model->order('name')->getAllList(" company='$mycompany' ");
    //     $myunitkerja = $this->User_model->getBy($this->user['id'],'unitkerja_id');
    //     $data['myunitkerja'] =$this->Unitkerja_model->order('name')->getMyUnitkerjaList($myunitkerja, $this->user['id']);
    //     $data['atasan'] = $this->Unitkerja_model->filter('')->getAtasan($myunitkerja, $this->user['id']);

    //     $this->load->model('Atasan_model');
    //     $_SESSION['all_bawahan']=array();
    //     $nopeg = $this->User_model->getBy($this->user['id'], 'karyawan_id');
    //     $data['subordinates'] = $this->Atasan_model->getSubordinates($nopeg);
    //     unset($_SESSION['all_bawahan']);

    //     $data['creator'] = $this->Unitkerja_model->filter('')->getCreator($myunitkerja, $jenis);
    //     // $data['creator'] = array(''=>NULL);
        
    //     // Iso
    //     $this->load->model('Document_iso_model');
    //     $data['document_iso'] = $this->Document_iso_model->getAll();

    //     echo "<pre>";print_r($data);die();
    //     return $data;
    // }


    protected function deleteVerification($id, $type=null){
        if ($type!=NULL){
            $filter_type= " and type in ($type) ";
        }
        $sql = "delete from document_verification where document_id=$id $filter_type";
        dbQuery($sql);
    }

    public function updateBecauseRevise($id){
        $this->load->model('Document_reviews_model');
        $workflow_id = $this->model->getOne($id, 'workflow_id');
        $this->Document_reviews_model->filter(" document_id=$id and workflow_id=$workflow_id")->delete($id);
    }

    public function addHistory($document_id, $history){
        $activity = array();
        $this->load->model('Document_activity_model');
        $activity["user_id"] = $this->user['id'];
        $activity["document_id"] = $document_id;
        $activity["activity"] = $history;
        $activity["type"] = Document_activity_model::HISTORY;
        $activity['workflow_id'] = $this->model->getOne($document_id, 'workflow_id');
        $activity['workflow_urutan'] = $this->model->getOne($document_id, 'workflow_urutan');
        $activity = array_change_key_case($activity, CASE_UPPER);
        $this->Document_activity_model->create($activity,TRUE,TRUE);
    }


    public function checkCode($document_id, $code){
        $code_now = $this->model->getOne($document_id, 'code');
        if ($code_now!=$code){
            $ciConfig = $this->config->item('utils');
            $filename = $this->model->getOne($document_id, 'document');

            $full_path = $ciConfig['full_upload_dir'].'document/files/';
            $full_filename = $full_path.$filename;
            $file_split = explode('.', $filename);

            $ext_file = end($file_split);
            
            // Untuk penamaan file
            $revision = '_R'.$this->model->getOne($document_id, 'revision');
            $code_split = explode('/', $code);
            $file_name = implode('_', $code_split);
            $f_name = $file_name;
            $file_name .= $revision.'.'.$ext_file;
            $full_new_filename = $full_path.$file_name;

            $user_id = $this->model->getOne($document_id, 'user_id');

            if (file_exists($full_filename)){
                rename($full_filename, $full_new_filename);
                if ($ext_file=='xls' or $ext_file=='xlsx'){
                    $exp_old_filename = explode('.', $filename);
                    unset($exp_old_filename[count($exp_old_filename)-1]);
                    $old_filename = $full_path.implode('.', $exp_old_filename).'.pdf';
                    $new_filename = $full_path.$f_name.$revision.'.pdf';
                    rename($old_filename, $new_filename);
                }
                if ($this->model->getOne($document_id, 'is_upload')=='0'){
                    $exp_old_filename = explode('.', $filename);
                    unset($exp_old_filename[count($exp_old_filename)-1]);
                    $old_filename = $full_path.implode('.', $exp_old_filename).'.txt';
                    $new_filename = $full_path.$f_name.$revision.'.txt';
                    rename($old_filename, $new_filename);
                }
                $this->model->updateDocumentWithFile($document_id, $file_name, $full_new_filename, $user_id);
            }


            // echo '<br>'.$full_filename;
            // echo '<br>'.$full_new_filename;
            // die();
        }
    }

    public function obsoleteWatermark($file, $idCompany, $extfile) {
        $this->load->model('Unitkerja_model');
        $namaCompany = $this->Unitkerja_model->getOne("0000".$idCompany."", 'name');

        // require_once(APPPATH."/third_party/fpdf/fpdf.php");
        require_once(APPPATH."/third_party/fpdf/rotation.php");
        // require_once(APPPATH."/third_party/fpdf/pdf.php");
        $dataCompany =  array(
                                    '2000'  => 'PT. Semen Indonesia (Persero) Tbk.',
                                    '3000'  => 'PT. SEMEN PADANG',
                                    '4000'  => 'PT. SEMEN TONASA',
                                    '5000'  => 'PT. SEMEN GRESIK',
                                    '9000'  => 'PT. SEMEN KUPANG INDONESIA'
                                );
        // $pdf = new FPDI();
        $pdf = new PDF_Rotate();

        $dirpng = "./assets/uploads/document/files/";
        $explodefilename = explode('.', $file);
        $filename = $explodefilename[0];
        $getFile = $filename.'_header.pdf';
        // $extfile  = $explodefilename[1];
        $file = "./assets/uploads/document/files/".$getFile;
        // echo $file;
        if (file_exists($file)){
           $pagecount = $pdf->setSourceFile($file);
           // echo $pagecount;
            // echo "b";
        } else {
            return FALSE;
            // echo "a";
        }

        for($i = 1; $i <= $pagecount; $i++){
            $tpl = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($tpl);
            // echo "<pre>";print_r($size);echo "</pre>";
            $pageSize = "A4";
            if($size['h'] == 297.00008333333){ $pageSize = "A4";}
            elseif($size['h'] == 355.6){ $pageSize = "Legal";}
            elseif($size['h'] == 279.4){ $pageSize = "Letter";}
            elseif($size['h'] == 419.99958333333){ $pageSize = "A3";}
            else {$pageSize = "A4";}
            $x = $size['w'] * (5/100);
            $y = ($size['h'] * (55/100))+10;
              // echo $dirpng."".$data_text['text1']['name'].'.png<br>';
            if($size['w'] <= $size['h']){
                // echo "masuk if<br>";
                $pdf->addPage("P", $pageSize);
                
                $pdf->RotatedImage($dirpng.'obsolete.png', $x, $y, 70, 7, 90);
            }
            else{
                // echo "masuk else<br>";
                $pdf->addPage("L", $pageSize);
                // $pdf->Image('./assets/web/images/logo/'.$logo,115,70,100,100);
                $pdf->RotatedImage($dirpng.'obsolete.png', $x, $y, 70, 7, 90);    
                
            }
           
                // $x -= 5;
                // $y += 10;
            // }
            $pdf->useTemplate($tpl, 0, 0, 0, 0, TRUE);
            unlink($dirpng."".$data_text['text']['name'].'.png');
           
        }
        // // echo './assets/uploads/document/files/'.$filename.'_'.$userid.'.'.$extfile;
        $pdf->Output('./assets/uploads/document/files/'.$filename.'_header.'.$extfile, 'F');
    }

    private function addNewDocument($data){
        $insert = $this->model->create($data, TRUE, TRUE);//Memasukkan data dokumen ke db
        if ($insert) {
            return $insert;
        }
        // return false;
    }

    protected function changeName($path, $prev, $after){
        $ciConfig = $this->config->item('utils');
        $full_path = $ciConfig['full_upload_dir'].$path;
        $full_filename = $full_path.$prev;
        if (file_exists($full_filename)){
            $full_new_filename = $full_path.$after;
            rename($full_filename, $full_new_filename);
        }
    }


    public function ajaxsetreason(){
        $document_id = $this->input->post('document_id', true);
        $data['reason'] = $this->input->post('reason', true);
        if ($this->model->updateDocument($this->user['id'],$document_id, $data)){
            // if ($this->_moveDocumentTemp($document_id, 'document/temp')) {
            //  return true;
            // }
        }
    }

    public function ajaxSetReview(){
        $data = $this->input->post(null, true);

        $data['workflow_id'] = $this->model->getOne($data['document_id'], 'workflow_id');
        if (dbInsert('document_reviews', $data)){
            // $is_agree = "<b style='color:red'>Tidak Setuju</b>";
            $is_agree = "Disagree";
            if ($data['is_agree']=='1'){
                // $is_agree = "<b style='color:green'>Setuju</b>";
                $is_agree = "Agree";
            }
            $this->addHistory($data['document_id'], "Is ".$is_agree." and give review \"".$data['review']."\"");
   //           $data_lama['USER_ID'][0] = $data['document_reviewer'];
   //           $data_lama['WORKFLOW_ID'] = $data['workflow_id'];
            // $data_lama['WORKFLOW_URUTAN'] = 2;
   //           $this->inputDocLog($data['document_id'], 0,$data_lama);

            $this->load->model('Document_reviewer_model');
            $reviewer = $this->Document_reviewer_model->getAllBy($data['document_id'], $data['workflow_id']);
            // count reviewer
            $counter['reviewer'] = count($reviewer);

            $this->load->model('Document_reviews_model');
            $review = $this->Document_reviews_model
                                            ->column(array(
                                                "dr.document_id" => "\"document_id\"",
                                                "dr.document_reviewer" => "\"document_reviewer\"",
                                                "dr.review" => "\"review\"",
                                                "u.name" => "\"reviewer_name\"",
                                                "dr.is_agree" => "\"is_agree\"",
                                                "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
                                            ))
                                            ->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
                                            ->filter(" dr.document_id=".$data['document_id']." and workflow_id=".$data['workflow_id'])
                                            ->getAll();
            // count jml review
            $counter['reviewed'] = count($review);

            if ($counter['reviewed']>=$counter['reviewer']){
                $data['continue'] = 1;
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'), $data);

            }else{
                $data['continue'] = 0;
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Succes'), $data);
            }
        }
    }

    public function ajaxSetUrutan(){
        $document_id = (int)$this->input->post('document_id', true);
        $data['workflow_urutan']=(int)$this->input->post('workflow_urutan', true);
        $data['status'] = $this->input->post('status', true);
        $data['updated_by'] = $this->user['id'];
        $workflow_id = $this->model->getOne($document_id,'workflow_id');
        $urutan_now = $this->model->getOne($document_id, 'workflow_urutan');
        $status_now = $this->model->getOne($document_id, 'status');
        $type_id = $this->model->getOne($document_id, 'type_id');
        $this->load->model('Document_types_model');
        $type = $this->Document_types_model->get($type_id);

        $this->load->model('Workflow_model');
        $task = $this->Workflow_model->getTasks($workflow_id, $urutan_now);



        ////////////// PENGECEKAN JIKA DOKUMEN DI REVISI ATAU DI REJECT /////////////////////////
        switch($data['status']){
            case Document_model::STATUS_REVISE:
                $revision['text'] = $this->input->post('text', true);
                $revision['user_id'] = $this->user['id'];
                $revision['document_id'] = $document_id;
                dbInsert('DOCUMENT_REVISES', $revision);
                $this->updateBecauseRevise($document_id);
                $this->addHistory($document_id, "Ask this document to be revised because ".$revision['text']);
            break;
            case Document_model::STATUS_REJECT:
                $this->addHistory($document_id, "Rejected this document because ".$this->input->post('text', true));
            break;
        }
        $this->load->model('Document_reviewer_model');
        $this->load->model('Workflow_model');


        $data_sebelum = $this->model->getDocumentFlow($document_id); // data id dokumen yang lama
        // echo "ini status: ".$data['status'];

        //////////////// PENGECEKAN KONDISI PADA SETIAP WORKFLOW ID  /////////////////////
        switch($workflow_id){ // workflow id yang lama
            case '1':
                switch($urutan_now){
                    case '1':
                        if ($status_now==Document_model::STATUS_REVISE){
                            $this->addHistory($document_id, "Revise Document");
                        } else {
                            $this->addHistory($document_id, "Submitted Document");
                        }
                    break;
                    default:
                        $this->addHistory($document_id, "Already ".$task['name']." document");
                    break;
                }
                if ($data['workflow_urutan']==4){
                    $this->load->model('Document_verification_model');
                    $this->Document_verification_model->delete($document_id);
                }
                if ($data['status']==Document_model::STATUS_SELESAI){
                    $data['is_published'] = 1;
                    $document_code = $this->model->getOne($document_id, 'code');
                    $codes = explode('/',$document_code);
                    $filter_code = $codes[0].'/'.$codes[1];
                    if ($type_id==Document_model::INSTRUKSI_KERJA){
                        $unitkerja_id = $this->model->getOne($document_id, 'unitkerja_id');
                        $param = " and unitkerja_id=$unitkerja_id ";
                    } else {
                        if ($type_id==Document_model::FORM){
                            if ($codes[0]=='FI'){
                                $unitkerja_id = $this->model->getOne($document_id, 'unitkerja_id');
                                $param = " and unitkerja_id=$unitkerja_id ";
                            } else {
                                $prosesbisnis_id = $this->model->getOne($document_id, 'prosesbisnis_id');
                                $param = " and prosesbisnis_id=$prosesbisnis_id ";
                            }
                        } else {
                            $prosesbisnis_id = $this->model->getOne($document_id, 'prosesbisnis_id');
                            $param = " and prosesbisnis_id=$prosesbisnis_id ";
                        }
                    }

                    // $data['code_number'] = $this->model->getLastCode($document_code);
                    $data['code_number'] = $this->model->getLastCode($filter_code, $param);
                    $data['code'] = $document_code.$data['code_number'];
                    $document = $this->model->getOne($document_id, 'document');
                    $file_explode = explode('.', $document); 
                    $code_explode = explode('/',$data['code']);
                    $code_implode = implode('_',$code_explode);
                    $data['document'] = $code_implode.'_R0'.'.'.end($file_explode);
                    $this->checkCode($document_id, $data['code']);
                    $this->model->updateValidityDate($document_id);
                    $this->model->updateDateBy($document_id, 'published_at');
                    $this->addHistory($document_id,"This document already published");
                }
            break;
            case '2':
                $this->load->model('Document_evaluation_model');
                $evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
                $evaluation['text'] = json_decode($evaluation['text'], true);
                if ($data['status']==Document_model::STATUS_SELESAI){ // Jika data status sudah selesai (D)
                    // $data['is_published'] = 1;
                    // $this->model->updateDateBy($document_id, 'published_at');
                    $ev_update = array();
                    if ($evaluation['text'][1]==0){
                        $ev_update['status'] = Document_evaluation_model::CABUT;
                        $data['is_obsolete'] = 1;
                        $data['show']=0;
                    } else {
                        $is_relevan = 1;
                        foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
                            if ($v_evaluation==0){
                                $is_relevan = 0;
                                break;
                            }
                        }
                        if ($is_relevan==0){
                            $ev_update['status'] = Document_evaluation_model::REVISI;
                        } else {
                            $ev_update['status'] = Document_evaluation_model::RELEVAN;
                            $this->model->updateValidityDate($document_id);
                        }
                    }
                    $this->Document_evaluation_model->update($document_id, $ev_update);
                    
                    $this->addHistory($document_id,"This document is evaluated");
                }
                switch($urutan_now){ // workflow urutan yang sekarang, masih menggunakan id yang sama / lama
                    case '1':
                        $creator_id = $this->model->getOne($document_id,'creator_id');
                        $user_id = $this->model->getOne($document_id,'user_id');
                        // if ($creator_id==$user_id){
                        //  $this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
                        // }
                        $this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
                        
                    break;
                    case '3':
                        $this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
                    break;
                    case '5':
                        $this->Document_evaluation_model->updateDateBy($document_id,'approver_date');
                    break;
                }
                switch($data['workflow_urutan']){ // workflow urutan berdasarkan post data
                    case '3':
                        $creator_id = $this->model->getOne($document_id,'creator_id');
                        $user_id = $this->model->getOne($document_id,'user_id');
                        if ($creator_id==$user_id and $type['need_reviewer']=='0'){
                            $data['workflow_urutan']++;
                            if ($evaluation['text'][1]!=0){
                                $data['workflow_urutan']++;
                                $result = 0;
                                foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
                                    if ($v_evaluation==0){
                                        $result++;
                                        break;
                                    }
                                }
                                switch ($result) {
                                    case 0:
                                    break;
                                    case 9:
                                    break;
                                    default:
                                        $data['workflow_urutan']++;
                                        $data['status']=Document_model::STATUS_SELESAI;
                                        $ev_update = array();
                                        $ev_update['status'] = Document_evaluation_model::REVISI;
                                        $this->Document_evaluation_model->update($document_id, $ev_update);
                                        $this->addHistory($document_id,"This Document Evaluation is Done");
                                    break;
                                }
                            }
                        }
                    break;
                    case '4':
                        if ($evaluation['text'][1]!=0){
                            $data['workflow_urutan']++;
                            $result = 0;
                            foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
                                if ($v_evaluation==0){
                                    $result++;
                                    break;
                                }
                            }
                            switch ($result) {
                                case 0:
                                break;
                                case 9:
                                break;
                                default:
                                    $data['workflow_urutan']++;
                                    $data['status']=Document_model::STATUS_SELESAI;
                                    $ev_update = array();
                                    $ev_update['status'] = Document_evaluation_model::REVISI;
                                    $this->Document_evaluation_model->update($document_id, $ev_update);
                                    $this->addHistory($document_id,"This Document Evaluation is Done");
                                break;
                            }
                        }
                    break;
                    default:
                    break;
                }
                // Jika Revisi langsung ke halaman revisi
                if ($ev_update['status']==Document_evaluation_model::REVISI and $data['status']==Document_model::STATUS_SELESAI){
                    // $data['show'] = 0;
                    // $data['workflow_id'] = 1;
                    if (dbUpdate('documents', $data, "id=$document_id")){
                        /////////////////////// BERIKAN OBSOLETE WATERMARK DI SEBELAH SINI SETELAH UPDATE STATUS ID YANG LAMA DAN SEBELUM CREATE DATA BARU
                        $ev_update = array();
                        $ev_update['status'] = Document_evaluation_model::REVISI;
                        $this->Document_evaluation_model->update($document_id, $ev_update);
                        
                        // Add new Document
                        $data = array();
                        $document = $this->model->getBy($document_id);
                        $doc = array_change_key_case($document, CASE_LOWER);

                        ////////  OBSOLETE WATERMARK  /////////////////////
                        $document = $this->model->getBy($doc['id']);
                        $this->load->model('Unitkerja_model');
                        $idCompany = $this->Unitkerja_model->getOneBy($doc['unitkerja_id'], "\"COMPANY\"");
                        $explodefilename = explode('.', $doc['document']);
                        $filename = $explodefilename[0].'.pdf';
                        $this->ObsoleteWatermark($filename, $idCompany, 'pdf');
                        ////////  ------------------  /////////////////////


                        $data['code'] =$doc['code'];
                        $data['user_id'] = $doc['user_id'];
                        $data['description'] = $doc['description'];
                        $data['title'] = $doc['title'];
                        $data['is_upload'] = $doc['is_upload']; 
                        $data['retension'] = $doc['retension'];
                        $data['hash'] = $doc['hash'];
                        // $data['revision'] = $doc['revision']+1;
                        $data['revision'] = $doc['revision'];
                        $data['status'] = Document_model::STATUS_BARU;
                        $data['workflow_id'] = Workflow_model::REVISI;
                        $data['workflow_urutan'] = 1;
                        $data['show'] = 0;
                        $data['parent'] = $document_id;
                        $data['is_published'] = 0;
                        $data['published_at'] = $doc['published_at'];
                        $data['validity_date'] = $doc['validity_date'];
                        $data['code_number'] = $doc['code_number'];

                        // Setting
                        $data['type_id'] = $doc['type_id'];
                        $data['creator_id'] = $doc['creator_id'];
                        $data['approver_id'] = $doc['approver_id'];                 
                        $data['prosesbisnis_id'] = $doc['prosesbisnis_id'];
                        $data['unitkerja_id'] = $doc['unitkerja_id'];
                        $data['user_jabatan_id'] = $doc['user_jabatan_id']; 
                        $data['creator_jabatan_id'] = $doc['creator_jabatan_id'];   
                        $data['approver_jabatan_id'] = $doc['approver_jabatan_id']; 

                        // Setting new Document
                        $file_explode = explode('.', $doc['document']); 
                        $code_explode = explode('/',$data['code']);
                        $code_implode = implode('_',$code_explode);
                        $data['document'] = $code_implode.'_Revise'.$data['revision'].'.'.end($file_explode);

                        $new_id = $this->addNewDocument($data);

                        if ($new_id){                           

                            // Update nama document pakai id
                            $append_id = "_".$new_id;
                            $record = array();
                            $record['document'] = $code_implode.'_Revise'.$data['revision']."_".$new_id.'.'.end($file_explode);
                            dbUpdate("documents", $record, "id=$new_id");

                            //Copy lama ke baru 
                            if ($data['is_upload']==0){
                                $CI = & get_instance();
                                $config = $CI->config->item('utils');
                                $folder = "document/files";
                                $full_path = $config['full_upload_dir'] . $folder . '/';

                                $new_docs = explode('.',$data['document']);
                                $ext = $new_docs[count($new_docs)-1]; 
                                unset($new_docs[count($new_docs)-1]);
                                $new_doc = implode('.', $new_docs);

                                $from = $full_path.$doc['document'];
                                $to = $full_path.$new_doc.$append_id.'.'.$ext;
                                copy($from, $to);

                                $old_docs = explode('.',$doc['document']);
                                unset($old_docs[count($old_docs)-1]);
                                $old_doc = implode('.', $old_docs);

                                $from = $full_path.$old_doc.'.txt';
                                $to = $full_path.$new_doc.$append_id.'.txt';
                                copy($from, $to);
                            }

                            // reviewer
                            if ($type['need_reviewer']=='1'){
                                $this->load->model('Document_reviewer_model');
                                $data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, $workflow_id);
                                $data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
                                $this->Document_reviewer_model->create($new_id,Workflow_model::REVISI, $data_reviewer);
                            }
                            
                            // ISO
                            $data_iso = array();
                            $data_iso['document_id'] = $new_id;
                            $iso_id = $this->model->getDocumentISOS($document_id);
                            foreach ($iso_id as $key => $value) {
                                $data_iso['iso_id'] = $value;
                                dbInsert("DOCUMENT_ISOS", $data_iso);
                            }

                            // Klausul
                            if ($type['klausul']=='1'){
                                $this->load->model('Document_klausul_model');
                                $data_klausul['document_id'] = $new_id;
                                $klausul = $this->Document_klausul_model->filter(" document_id=$document_id ")->getAll();
                                foreach ($klausul as $key => $value) {
                                    $data_klausul['klausul_id'] = $value['klausul_id'];
                                    dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
                                }
                            }
                        
                            // Unit Kerja
                            $this->load->model('Document_unitkerja_model');
                            $unitkerja_related = $this->Document_unitkerja_model->filter(" where document_id=$document_id ")->getAll();
                            if ($unitkerja_related!=NULL){
                                $data_unit_kerja = array();
                                foreach ($unitkerja_related as $k_unitkerja_related => $v_unitkerja_related) {
                                    $data_unit_kerja = $v_unitkerja_related;
                                    $data_unit_kerja['document_id'] = $new_id;
                                    dbInsert('DOCUMENT_UNITKERJA', $data_unit_kerja);
                                }   
                            }
                            
                            // Related
                            $relatedDocument = $this->model->getRelatedOneWay($document_id);
                            if($relatedDocument!=NULL){
                                $data_related_document = array();
                                foreach ($relatedDocument as $related => $column) {
                                    $data_related_document['document1'] = $new_id;
                                    $data_related_document['document2'] = $column['document2'];
                                    $data_related_document['created_at'] = date("Y/m/d h:i:s");
                                    $data_related_document['updated_at'] = date("Y/m/d h:i:s");
                                    dbInsert('DOCUMENT_RELATED', $data_related_document);
                                }
                            }
                            
                            // $relatedDocument = $this->input->post('related',true);
                            // $this->model->setRelatedDocument($insert,$relatedDocument);

                            $data = array();
                            $data['user_id'] = $doc['creator_id'];
                            $data['user_jabatan_id'] = $doc['creator_jabatan_id'];
                            dbUpdate('documents', $data, "id=$new_id");

                            $msg = 'Success to evaluate document. Revision is needed for this document.';
                            // echo $new_id;
                            // die();
                            // redirect("web/document/detail/".$new_id);
                        }
                    }
                }
            break;
            case '3':  // flow ini digunakan khusus untuk dokumen pengganti (duplicate dokumen)
                switch($urutan_now){
                    // echo "masuk switch urutan now<br>";
                    case '1':
                        if ($status_now==Document_model::STATUS_REVISE){
                            $this->addHistory($document_id, "Revise Document");
                        } else {
                            $this->addHistory($document_id, "Submitted Document Revision");
                        }
                        // $creator_id = $this->model->getOne($document_id,'creator_id');
                        // $user_id = $this->model->getOne($document_id,'user_id');
                        // if ($creator_id==$user_id){
                        //  $this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
                        // }
                    break;
                    default:
                        $this->addHistory($document_id, "Already ".$task['name']." document");
                    break;
                }
                switch($data['workflow_urutan']){
                    // echo "masuk switch workflow urutan<br>";
                    case '3':
                        $creator_id = $this->model->getOne($document_id,'creator_id');
                        $user_id = $this->model->getOne($document_id,'user_id');
                        if ($creator_id==$user_id and $type['need_reviewer']=='0'){
                            $data['workflow_urutan']++;
                        }
                    break;
                }
                if ($data['workflow_urutan']==4){
                    $this->load->model('Document_verification_model');
                    $this->Document_verification_model->delete($document_id);
                }
                
                if ($data['status']==Document_model::STATUS_REJECT){ // revisi
                    // Obsoletekan Document Baru
                    $data['is_published'] = 1;
                    $data['is_obsolete'] = 1;

                    // Publish Parent
                    $previous_document = $this->model->getOne($document_id, 'parent');
                    $prev_doc = array();
                    $prev_doc['workflow_id'] = Workflow_model::PENGAJUAN;
                    $prev_doc['status'] = Document_model::STATUS_SELESAI;

                    // Cek apakah masih valid atau tidak
                    $diff = $this->model->getOne($document_id, "(to_date(TO_CHAR(validity_date, 'MM-DD-YYYY'), 'MM-DD-YYYY')-sysdate) as diff");
                    if ($diff<=0){
                        $prev_doc['workflow_id'] = Workflow_model::EVALUASI;
                        $prev_doc['status'] = Document_model::STATUS_BARU;
                        $prev_doc['workflow_urutan'] = 1;
                        $this->load->model("Document_evaluation_model");
                        $this->Document_evaluation_model->deleteWith($previous_document);
                    }
                    dbUpdate("documents", $prev_doc, "id='$previous_document'");

                    $this->addHistory($document_id,"This Document revision is rejected because ".$this->input->post('text', true));
                }
                if ($data['status']==Document_model::STATUS_SELESAI){ // revisi
                    $data['is_published'] = 1;
                    $revisi_ke=(int)$this->model->getOne($document_id, 'revision');
                    $file_explode = explode('.', $this->model->getOne($document_id, 'document')); 
                    $code_explode = explode('/',$this->model->getOne($document_id, 'code'));
                    $code_implode = implode('_',$code_explode);
                    $append_id = "_".$document_id;
                    if ($type_id==Document_model::BOARD_MANUAL){
                        $append_id = '';
                    }
                    $prev = $code_implode.'_Revise'.$revisi_ke.$append_id.'.'.end($file_explode);
                    $data['show']=1;
                    $data['revision'] = $revisi_ke+1;
                    $data['document'] = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
                    $this->model->updateValidityDate($document_id);
                    $this->changeName("document/files/", $prev, $data['document']);
                    if(end($file_explode) == 'xlsx' || end($file_explode) == 'xls'){
                        $prev_pdf = $code_implode.'_Revise'.$revisi_ke.$append_id.'.pdf';
                        $new_pdf = $code_implode.'_R'.$data['revision'].'.pdf';
                        $this->changeName("document/files/", $prev_pdf, $new_pdf);
                    }
                    // $this->copyName("document/files/", $prev, $data['document']);

                    $is_upload = $this->model->getOne($document_id, 'is_upload');
                    if ($is_upload==0){
                        $prev_txt = $code_implode.'_Revise'.$revisi_ke.$append_id.'.txt';
                        $new_txt = $code_implode.'_R'.$data['revision'].'.txt';
                        $this->changeName("document/files/", $prev_txt, $new_txt);
                        // $this->copyName("document/files/", $prev_txt, $new_txt);
                    }
                    // $this->model->updateDateBy($document_id, 'published_at');

                    // Obsoletekan Parent
                    $previous_document = $this->model->getOne($document_id, 'parent');
                    $this->model->setToObsolete($previous_document);
                    
                    // Related Document
                    $sql_parent = "select parent from documents where id=".$document_id;
                    // echo ($sql_new_id);
                    $parent = dbGetOne($sql_parent);
                    // echo "parent:".$parent;
                    $update = "update DOCUMENT_RELATED set document2 =".$document_id." where document2=".$parent;
                    // echo "query update :".$update;
                    dbQuery($update);
                    // die("MASUK KE SINI") ;
                    // dbUpdate("DOCUMENT_RELATED",$record,"document2=$document_id");

                    $this->addHistory($document_id,"This Document is done revised");
                }
            break;
        }
        
        // if ($data['workflow_urutan']=='-1'){
        //  $this->updateBecauseReject($document_id);
        // }
        if ($this->Document_model->updateDocument($this->user['id'],$document_id, $data)){
            $data_baru = $this->model->getDocumentFlow($document_id);
            // echo "<pre>";print_r($document_id);echo "</pre>";
            // echo "<pre>";print_r($data_baru);echo "</pre>";
            // echo "<pre>";print_r($data_sebelum);echo "</pre>";
            $this->inputDocLog($document_id, $data_baru,$data_sebelum,$data['status']);
            $this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_DOCUMENT_STATUS_CHANGED, $document_id, $data['updated_by']);
            // return $document_id;
            // return true;
        }
        // return false; 
        // echo $document_id;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $msg), $document_id);
    }

    public function inputDocLog($doc_id,$data=0,$data_lama=0 , $status){
        $this->load->model('Documents_log_model');
        $this->load->model('Document_model');
        $this->load->model('Statistic_documents_model');
        // echo $doc_id;
        // echo 'doc_id='.$doc_id."<br>";
        // echo "<pre>";print_r($data);echo "</pre>";
        // echo "<pre>";print_r($data_lama);echo "</pre>";
        // echo "ini status: ".$status."<br>";
        // echo "data sebelum.".$data_sebelum['WORKFLOW_ID']."<br>";
        // echo Document_model::STATUS_REVISE."<br>";
        if($status == Document_model::STATUS_REVISE || $data_lama['WORKFLOW_URUTAN'] > $data['WORKFLOW_URUTAN']){
            $dataUpdate = array(
                'STATUS' => Documents_log_model::REVISE
            );
            dbUpdate('documents_log', $dataUpdate, "document_id ='".$doc_id."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan!=1");
            dbUpdate('documents_log', array('STATUS' => Documents_log_model::PROGRESS), "document_id ='".$doc_id."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=1");
        }
        elseif($status == Document_model::STATUS_REJECT && $data['WORKFLOW_ID'] == 1){
            // DELETE SEMUA DI DOCUMENT LOG DAN PENYESUAIAN DI STATISTIC_DOCUMENT
            // Status pasti belum publish, status di statistics document pasti belum L dan masih menghitung tot_doc, kurangin tot_doc dari setiap ketentuan
            // echo "masuk rejectinput<br>";
            $hapusData = $this->Documents_log_model->filter("where document_id ='".$doc_id."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=1 or workflow_urutan=3")->deleteData();
            if($hapusData){// jika berhasil hapus di documents_log_model, maka -1 setiap proses dari ketentuan
                // echo "masuk if<br>";
                $get_unitkerja = $this->getUnitkerja($data_lama['CREATOR_JABATAN_ID']);
                // echo "unitkerja:".$get_unitkerja."<br>";
                if($data_lama['WORKFLOW_URUTAN'] == $data['WORKFLOW_URUTAN']){
                    $workflow_urutan = " = 3";
                }
                else{
                    $workflow_urutan = " <= 3";
                }
                $minus_stat_doc = $this->Statistic_documents_model->filter(" where unitkerja_id='".$get_unitkerja."' and workflow_id=".$data_lama['WORKFLOW_ID']." and doc_type = ".$data_lama['TYPE_ID']." and workflow_urutan ".$workflow_urutan)->updateTotDoc();
                // echo $minus_stat_doc;
            }
        }
        // elseif($status == Document_model::STATUS_PROSES){
        //  echo "ini status proses<br>";
        // }
        // elseif()

        else{
            // echo "ini status lagi<br>";
            if($data_lama !=0) {
                $dataInsert = array(
                    'STATUS' => Documents_log_model::DONE
                );
                $get_data_log = $this->Documents_log_model->filter("where document_id=".$doc_id)->getBy();
                // echo "<pre>";print_r($get_data_log);echo "</pre>";
                // $data_stat_doc = $this->Statistic_documents_model->filter("where unitkerja_id='".$get_data_log[0]['unitkerja_id']."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=".$data_lama['WORKFLOW_URUTAN'])->getAll();
                // $dataUpdate = array(
                //  'TOTAL_DOCUMENT' => $data_stat_doc[0]['total_document']-1
                // );
                    // echo "<pre>";print_r($dataInsert);echo "</pre>";
                // $this->Documents_log_model->updateDate('updated_at',"where id='".$get_data_log['id']."'");
                dbUpdate('documents_log', $dataInsert, "document_id =".$doc_id." and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=".$data_lama['WORKFLOW_URUTAN']." and user_id=".$data['CREATOR_ID']);
                    // dbUpdate('Statistic_documents_model', $dataUpdate, "id =".$data_stat_doc[0]['id']);
            }
            if($data != 0 || $data['WORKFLOW_URUTAN'] != 6){ // insert data baru
                // echo "ini status1: ".$status."<br>";
                // echo "<pre>";print_r($data);echo "</pre>";
                // echo "masuk if workflow urutan<br>";
                $this->load->model('Workflow_model');
                $this->load->model('Jabatan_model');

                $tasks = $this->Workflow_model->getTasks($data['WORKFLOW_ID'], $data['WORKFLOW_URUTAN']);
                $get_unitkerja = $this->getUnitkerja($data['CREATOR_JABATAN_ID']);
                //ready to insert
                $record = array();
                $record['document_id'] = $doc_id;
                $record['unitkerja_id'] = $get_unitkerja;
                $record['workflow_id'] = $data['WORKFLOW_ID'];
                $record['workflow_urutan'] = $data['WORKFLOW_URUTAN'];
                $record['actor'] = $tasks['user'];
                $record['task_name'] = $tasks['name'];
                $record['status'] = Documents_log_model::PROGRESS;
                $record['user_id'] = $data['CREATOR_ID'];

                $last_insert = $this->Documents_log_model->filter("where document_id=".$record['document_id']." and unitkerja_id='".$record['unitkerja_id']."' and actor='".$record['actor']."' and workflow_id=".$record['workflow_id']." and workflow_urutan=".$record['workflow_urutan']." and status='".Documents_log_model::PROGRESS."' or status='".Documents_log_model::REVISE."' and user_id='".$record['user_id']."'")->getOne('id');
                        // jumlah hari langsung 0 jika tidak ada data
                if ($last_insert==NULL || $last_insert==false){
                    // echo "ini status2: ".$status."<br>";
                    $record['jumlah_hari'] = 0;
                    $insert = $this->Documents_log_model->insert($record, FALSE, FALSE);
                    if($insert){ // jika data telah diinsert , maka insert / update ke statistic_documents
                        $this->load->model('Statistic_documents_model');
                        $stat_doc_data = $this->Statistic_documents_model->filter("where unitkerja_id = '".$get_unitkerja."' and doc_type='".$data['TYPE_ID']."' and workflow_id=".$data['WORKFLOW_ID']." and workflow_urutan=".$data['WORKFLOW_URUTAN'])->getAll();
                        // echo "ini stat_doc_data<br>";
                        // echo "<pre>";print_r($stat_doc_data);echo "</pre>";
                        if($stat_doc_data){ // jika data telah ada, maka akan diupdate
                            $stat_doc['total_document'] = $stat_doc_data[0]['total_document'] + 1;
                            $query = "update statistic_documents set total_document = total_document+1 where id = ".$stat_doc_data[0]['id'];
                            // echo $query."<br>";
                            dbQuery($query);
                            // dbUpdate('Statistic_documents_model', $stat_doc, "id =".$stat_doc_data[0]['id']);
                        }
                        else{ // Jika Data Tidak ada di Stat_doc, maka akan insert
                            $stat_doc = array(
                                'unitkerja_id'      => $get_unitkerja,
                                'workflow_id'       => $data['WORKFLOW_ID'],
                                'workflow_urutan'   => $data['WORKFLOW_URUTAN'],
                                'jumlah_hari'       => 0,
                                'doc_type'          => $data['TYPE_ID'],
                                'type'              => Statistic_documents_model::PENDING_PROCESS,
                                'actor'             => $tasks['user'],
                                'total_document'    => 1
                            );
                            dbInsert('Statistic_documents', $stat_doc);
                        // echo "insert<br>";
                        }
                        
                    }
                } else { // biasanya karena revise
                    // echo "ini status3: ".$status."<br>";
                    // echo "id :".$last_insert."<br>";
                    // kalau ada diupdate
                    // $update = array();
                    // $jumlah = (int)$this->Documents_log_model->filter("where id=$last_insert")->getOne('jumlah_hari');
                    // $jumlah++;
                    $update['status'] = Documents_log_model::PROGRESS;
                    $ganti =$this->Documents_log_model->updateDate('updated_at',"where id=$last_insert");
                    // dbupdate($last_insert, $update);
                    $query = "update documents_log set status='P' where id=".$last_insert;
                    // echo $query;
                    $ganti = dbQuery($query);
                    // dbUpdate('Documents_log',$update," id=".$last_insert);
                    if($ganti){
                        $stat_doc_data = $this->Statistic_documents_model->filter("where unitkerja_id = '".$get_unitkerja."' and doc_type='".$data['TYPE_ID']."' and workflow_id=".$data['WORKFLOW_ID']." and workflow_urutan=".$data['WORKFLOW_URUTAN'])->getAll();
                        if(!$stat_doc_data){
                            $stat_doc = array(
                                'unitkerja_id'      => $get_unitkerja,
                                'workflow_id'       => $data['WORKFLOW_ID'],
                                'workflow_urutan'   => $data['WORKFLOW_URUTAN'],
                                'jumlah_hari'       => 0,
                                'doc_type'          => $data['TYPE_ID'],
                                'type'              => Statistic_documents_model::PENDING_PROCESS,
                                'actor'             => $tasks['user'],
                                'total_document'    => 1
                            );
                            dbInsert('Statistic_documents', $stat_doc);

                        }
                    }
                           
                }
            }
            if($data['WORKFLOW_URUTAN'] == 6){ // tanda telah publish, maka total_doc - 1 di stat doc
                $this->load->model('Statistic_documents_model');
                $get_unitkerja = $this->getUnitkerja($data['CREATOR_JABATAN_ID']);
                $data_stat = $this->Statistic_documents_model->filter("where unitkerja_id='".$get_unitkerja."' and workflow_id=".$data['WORKFLOW_ID']." and doc_type='".$data['TYPE_ID']."'")->getAll();
                foreach ($data_stat as $stat) {
                    $this->Documents_log_model->updateDate('updated_at',"where id='".$stat['id']."'");
                    $query = "update statistic_documents set total_document= total_document -1 where id=".$stat['id'];
                    dbQuery($query); // total doc -1 karena telah publish
                }
                
            }
        }
        
        // echo "baris luar inputdoclog<br>";

    }

    public function getUnitKerja($jabatan_id){
        $this->load->model('Jabatan_model');
        $this->load->model('Unitkerja_model');
        $unitkerja_id =$this->Jabatan_model->getOne($jabatan_id, 'unitkerja_id');
        if($unitkerja_id == null || $unitkerja_id == ''){
            // echo "aaa";
            return 0;
        }
        else{
            $d_unitkerja = $this->Unitkerja_model->getBy($unitkerja_id);
            if($d_unitkerja['level'] == 'GRP' || $d_unitkerja['level'] == '' || $d_unitkerja['level'] == null){
                // get unitkerja minimal sect
                $unitkerja = $this->Unitkerja_model->getParent($unitkerja_id, array('SECT','BIRO', 'DEPT'));
                if($unitkerja){
                    return $unitkerja;
                }
                else{
                    // echo "bbb";
                    return 0;
                }
                // return unitkerja
                
            }
            else{ // jika unitkerja sudah termasuk level yang dbutuhkan (min. sect)
                return $unitkerja_id;
            }
            // echo "<pre>";print_r($level);echo "</pre>";
        }
        

    }

    public function calculateSize($size=0){
        $size = (int) $size;
        if ($size<1000) {
            $size_str = $size.' Bytes';
        } else if (round($size/1000)<=1000) {
            $size_str = round($size/1000) . ' KB';
        } else if (round($size/1000000)<=1000) {
            $size_str = round($size/1000000,2) . ' MB';
        } else {
            $size_str = round($size/1000000000,2) . ' GB';
        } 
        return $size_str;
    }
    
    public function removeFileTemp(){
        $this->unsetFileTemp();
    }
    
    function postDoc()  {
        $user = $this->user;
        // $this->load->model("User_model");
        // $data = array();
        // $data['user_id'] = $user['id'];
        // $data['description'] = $this->input->post('log');
        // $data['title'] = $this->input->post('title');

        // $data['revision'] = 0;
        // $data['show'] = 1;
        // $data['type_id'] = $this->input->post('doctype');

        

        
        // $data['status'] = 'P'; //Document_model::STATUS_PROS
        // $data['creator_id'] = $this->input->post('creator');
        // $data['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$data['creator_id']."' ")->getOneFilter("jabatan_id");
        // $data['approver_id'] = $this->input->post('approver');
        // $data['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$data['approver_id']."' ")->getOneFilter("jabatan_id");
        // $data['prosesbisnis_id'] = $this->input->post('bussproc');

        // $data['is_upload'] = $this->input->post('method'); 
        // $data['unitkerja_id'] = $this->input->post('workunit');
        // $retensi = $this->input->post('periode');
        // if ($this->input->post('waktu')=='1'){
        //     $data['retension'] = $retensi*12;
        // } else {
        //     $data['retension'] = $retensi;
        // }

        // $data['code'] = $this->input->post('kodeAdministratif', true);
        // 
        // $this->model->getCodeInForm($data['type_id'], $data['prosesbisnis_id'], $data['unitkerja_id'], $this->input->post('formtype'));
        // 
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS,"ok"), "ok");

        // $this->load->model('User_model');
        // $data['company'] = $this->User_model->getBy($user['id'], 'company');

        // $this->load->model('Unitkerja_model');
        // $data['company'] = $this->Unitkerja_model->getOneBy(str_pad($data['company'],8,'0', STR_PAD_LEFT), "\"INITIAL\"");

        // $this->load->model('Document_prosesbisnis_model');
        // $data['proses_bisnis'] = $this->Document_prosesbisnis_model->getOne($data['prosesbisnis_id'],'code');

        // $data['code'] = $this->model->getCodeInForm($data['type_id'], $data['prosesbisnis_id'], $data['unitkerja_id'], $this->input->post('form_type', true));
        // $str = $data['code'].'/'.$data['company'].'/'.$data['proses_bisnis'].$data['unit_kerja'].'/'.$data['jumlah'];

        // $data['code'] = $str; 

         
        // Setting Workflow
        // $this->load->model('Workflow_model');
        // $data['workflow_id'] = Workflow_model::PENGAJUAN;
        // $flow = $this->Workflow_model->getTasks();

        // // set urutan
        // $data['workflow_urutan'] = 2;
        // $this->load->model('Document_types_model');

        // if ($data['creator_id']==$data['user_id']){
        //  $data['workflow_urutan'] = 4;
        // }
        // if (!$this->Document_types_model->getBy($data['type_id'], 'need_reviewer')){ // jika dia tidak butuh reviewer
        //     $data['workflow_urutan'] = 3;
        //     if ($data['creator_id']==$data['user_id']){ // Jika creator = drafter
        //         $data['workflow_urutan'] = 4;
        //     }
        // } else { // jika butuh reviewer
        //     $data['workflow_urutan'] = 2;
        // }



        
        // if ($data['creator_id']==$data['user_id']){
        //  if (in_array($data['type_id'], array('4', '5'))) {
        //      $data['workflow_urutan'] = 3;
        //  } else {
        //      $data['workflow_urutan'] = 2;
        //  }
        // }

        // if ($this->input->post('method')!='1'){
        //     $data['status'] = 'B'; //Document_model::STATUS_BARU;
        //     $data['document'] = str_replace("/","_",$data['code']).'_R'.$data['revision'].'.docx';
        //     $data['workflow_urutan'] = 1;
        //     $data['hash'] = $hash = hash_hmac('sha256',''.$this->config->item('salt_key'), $user_id);
        // }

        // $data = array_change_key_case($data, CASE_UPPER);
//         Array
// (
//     [code] => M/SMI/COM/
//     [title] => coba lagi
//     [doctype] => 2
//     [formtype] => null
//     [workunit] => 50045221
//     [bussproc] => 24
//     [require] => 9
//     [clause] => 525_523
//     [reviewer] => 60134_59988
//     [creator] => 61273
//     [approver] => 60574
//     [workunitrelated] => 50000179_50000299_50000396
//     [log] => test document
//     [periode] => undefined
//     [waktu] => 
// )
        $this->load->model("User_model");
        // echo "<pre>";print_r($this->input->post());die();
        $data = array();
        $data['user_id'] = $this->user['id'];
        $data['description'] = $this->input->post('log',true);
        $data['title'] = $this->input->post('title',true);
        $data['revision'] = 0;
        $data['show'] = 1;
        $data['type_id'] = $this->input->post('doctype',true);
        $data['status'] = Document_model::STATUS_PROSES;
        $data['creator_id'] = $this->input->post('creator',true);
        $data['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$data['creator_id']."' ")->getOneFilter("jabatan_id");
        $data['approver_id'] = $this->input->post('approver',true);
        $data['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$data['approver_id']."' ")->getOneFilter("jabatan_id");
        $data['prosesbisnis_id'] = $this->input->post('bussproc',true);
        $data['is_upload'] = $this->input->post('is_create_later')=='0' ? 1 : 0; 
        $data['unitkerja_id'] = $this->input->post('workunit', true);
        $retensi = $this->input->post('waktu', true);

        if ($this->input->post('periode', true)=='1'){
            $data['retension'] = $retensi*12;
        } else {
            $data['retension'] = $retensi;
        }

        if($this->input->post('company_header')){ // membuat dokumen untuk opco
            $data['header_logo'] = $this->input->post('company_header');
        }
        // echo "<pre>";print_r($data);die();
        // $data['code'] = $this->input->post('kodeAdministratif', true);
        $data['code'] = $this->model->getCodeInForm($data['type_id'], $data['prosesbisnis_id'], $data['unitkerja_id'], $this->input->post('formtype', true), $this->user['company']);

        // Setting Workflow
        $this->load->model('Workflow_model');
        $data['workflow_id'] = Workflow_model::PENGAJUAN;
        $flow = $this->Workflow_model->getTasks();

        // set urutan
        $data['workflow_urutan'] = 2;
        $this->load->model('Document_types_model');
        // if ($data['creator_id']==$data['user_id']){
        //  $data['workflow_urutan'] = 4;
        // }
        if (!$this->Document_types_model->getBy($data['type_id'], 'need_reviewer')){ // jika dia tidak butuh reviewer
            $data['workflow_urutan'] = 3;
            if ($data['creator_id']==$data['user_id']){ // Jika creator = drafter
                $data['workflow_urutan'] = 4;
            }
        } else { // jika butuh reviewer
            $data['workflow_urutan'] = 2;
        }

        // if ($data['creator_id']==$data['user_id']){
        //  if (in_array($data['type_id'], array('4', '5'))) {
        //      $data['workflow_urutan'] = 3;
        //  } else {
        //      $data['workflow_urutan'] = 2;
        //  }
        // }

        if ($this->input->post('is_create_later')=='1'){
            $data['status'] = Document_model::STATUS_BARU;
            $data['document'] = str_replace("/","_",$data['code']).'_R'.$data['revision'].'.docx';
            $data['workflow_urutan'] = 1;
            $data['hash'] = $hash = hash_hmac('sha256',''.$this->config->item('salt_key'), $user_id);
        }



        $data = array_change_key_case($data, CASE_UPPER);
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS,"ok"), $data);        
        $insert = $this->model->create($data, TRUE, TRUE);//Memasukkan data dokumen ke db
            // dbInsert('documents',$data);
            // die();
        // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS,"ok"), $data);
        // // print_r($insert);die();
        if ($insert) {
            //Memasukkan file ke dalam db
            if (!$this->_moveDocumentTemp($insert, 'document/temp', $data['CODE'])) {   
                $this->model->deleteDocument($insert);
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Submit Document'), array());
            }
            else {

                // Nama Dokumen jika create here
                if ($this->input->post('is_create_later')=='1'){
                    $append_id = "_".$insert;
                    if ($data['type_id']==Document_model::BOARD_MANUAL){
                        $append_id = '';
                    }
                    $record = array();
                    $record['document'] = str_replace("/","_",$data['CODE']).'_R'.$data['REVISION'].$append_id.'.docx';
                    dbUpdate("documents", $record, "id=$insert");
                }

                // Tag
                $this->load->model('Document_tags_model');
                $data2['tags'] = $this->input->post('tagIns',true);
                $data2 = array_change_key_case($data2, CASE_UPPER);
                $this->Document_tags_model->create($insert, $data2['TAGS']);
                
                // Unit Kerja
                $this->load->model('Document_unitkerja_model');
                $data_unit_kerja = $this->input->post('workunitrelated');
                $data_unit_kerja = array_change_key_case(explode('_',$data_unit_kerja), CASE_UPPER);
                $this->Document_unitkerja_model->create($insert, $data_unit_kerja);

                // reviewer
                $this->load->model('Document_reviewer_model');
                $data_reviewer = $this->input->post('reviewer');
                $data_reviewer = array_change_key_case(explode('_',$data_reviewer) , CASE_UPPER);
                $this->Document_reviewer_model->create($insert,$data['WORKFLOW_ID'], $data_reviewer);

                // Activity
                $this->load->model('Document_activity_model');
                $activity["user_id"] = $this->user['id'];
                $activity["document_id"] = $insert;
                $activity["activity"] = "Uploaded and submitted this document";
                if ($this->input->post('is_create_later')=='1'){
                    $activity["activity"] = "Membuat dokumen";
                }
                $activity["type"] = Document_activity_model::HISTORY;
                $activity = array_change_key_case($activity, CASE_UPPER);
                $this->Document_activity_model->create($activity,TRUE,TRUE);
                
                // iso
                $data_iso = array();
                $data_iso['document_id'] = $insert;
                $iso_id = $this->input->post('require');
                $iso_id = explode('_',$iso_id);
                foreach ($iso_id as $key => $value) {
                    $data_iso['iso_id'] = $value;
                    dbInsert("DOCUMENT_ISOS", $data_iso);
                }

                // Klausul
                $this->load->model('Document_types_model');
                $need_klausul = $this->Document_types_model->getBy($data['TYPE_ID'], 'klausul');
                if ($need_klausul){
                    $data_klausul['document_id'] = $insert;
                    $klausul = $this->input->post('clause');
                    $klausul = explode('_',$klausul);
                    foreach ($klausul as $key => $value) {
                        $data_klausul['klausul_id'] = $value;
                        dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
                    }
                }

                // Related
                $relatedDocument = $this->input->post('related',true);
                $this->model->setRelatedDocument($insert,$relatedDocument);

                if ($data['STATUS']==Document_model::STATUS_PROSES){
                    $this->load->model('Notification_model');
                    $this->Notification_model->generate(Notification_model::ACTION_DOCUMENT_SUBMIT, $insert);
                }
                $this->db->trans_commit();

                // $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to Submit Document'), array());
            }

            ///// INSERT UNTUK STATISTIK////////////
            // echo "<pre>";print_r($data);echo "</pre>";
            // $this->inputDocLog($insert, $data, 0, $data['status']);
            $send['document_id'] = $insert;
            if ($this->input->post('is_create_later')=='1'){
                $send['is_create_later'] =  $this->input->post('is_create_later');
            }
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to Submit Document'), $send);
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation)){
                    $msg = 'Gagal membuat postingan';
                }
                else{
                    $msg = implode('<br />', $validation);
                }
            } else{
                $msg = $insert;
            }

            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $msg), array());
           // $this->redirect();
            // header('Location: ' . site_url('web/document/dashboard'));
        }        
          
    }


    public function changeunitCreateNewDocument($id){
        $this->load->model('User_model');
        $this->load->model('Jabatan_model');
        $this->load->model('Atasan_model');
        $this->load->model('Unitkerja_model');
        $this->load->model('Workflow_model');

        // Get Document
        $document = $this->model->getDocumentBy($this->user['id'],$id);
        $creator = array(
            "id" => $document['creator_id'],
            "name" => $document['creator_name'],
            "jabatan_id" => $document['creator_jabatan_id'],
            "jabatan_name" => $document['creator_jabatan_name'],
            "unitkerja_id" => $this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"),
            "unitkerja_name" => $this->Unitkerja_model->getOne($this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"), "name")
        );

        $record = array();
        $record['is_moved'] = 1;
        dbUpdate("document_changeunit", $record, "document_id=$id");
        
        $record = array();
        $record['is_obsolete'] = 1;
        $record['show'] = 0;
        // Update Creator, Approver, Unitkerja
        $record['unitkerja_id'] = $creator['unitkerja_id'];
        $record['creator_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("user_id");
        $type_id = $this->model->getOne($id, 'type_id');
        $approver = $this->Unitkerja_model->getApprover($record['creator_id'], $type_id);
        $record['approver_id'] = $approver['approver'][0]['id'];
        $record['approver_jabatan_id'] = $this->Jabatan_model->getOne($record['creator_jabatan_id'], "unitkerja_id");
        if ($record['approver_id']==NULL){
            unset($record['approver_id']);
            unset($record['approver_jabatan_id']);
        } 
        dbUpdate('documents', $record, "id=$id");

        // Add new Document
        $data = array();
        $document = $this->model->getBy($id);
        $doc = array_change_key_case($document, CASE_LOWER);
        $data['user_id'] = $doc['user_id'];
        $data['description'] = $doc['description'];
        $data['title'] = $doc['title'];
        $data['is_upload'] = $doc['is_upload']; 
        $data['retension'] = $doc['retension'];
        $data['hash'] = $doc['hash'];
        $data['revision'] = 0;
        $data['status'] = Document_model::STATUS_SELESAI;
        $data['workflow_id'] = Workflow_model::PENGAJUAN;
        $data['workflow_urutan'] = 6;
        $data['show'] = 1;
        $data['is_published'] = 1;

        // Setting
        $data['type_id'] = $doc['type_id'];
        $data['creator_id'] = $doc['creator_id'];
        $data['approver_id'] = $doc['approver_id'];                 
        $data['prosesbisnis_id'] = $doc['prosesbisnis_id'];
        $data['user_jabatan_id'] = $doc['user_jabatan_id']; 
        $data['creator_jabatan_id'] = $doc['creator_jabatan_id'];   
        $data['approver_jabatan_id'] = $doc['approver_jabatan_id'];
        $data['unitkerja_id'] = $creator['unitkerja_id'];

        $data['is_converted'] = $this->model->getOne($id, "is_converted");
        $data['have_header'] = $this->model->getOne($id, "have_header");


        // check code
        $codes=explode('/',$doc['code']);
        if ($data['type_id']==Document_model::INSTRUKSI_KERJA){
            $param = " and unitkerja_id='".$data['unitkerja_id']."' ";
        } else {
            if ($type_id==Document_model::FORM){
                if ($codes[0]=='FI'){
                    $param = " and unitkerja_id='".$data['unitkerja_id']."' ";
                } else {
                    $param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
                }
            } else {
                $param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
            }
        }
        $code = $codes[0].'/'.$codes[1].'/'.$codes[2];
        if (strtoupper($codes[0])=='IK' or strtoupper($codes[0])=='FI'){
            $code  .= '/'.$data['unitkerja_id'];
        }
        $data['code_number'] = $this->model->getLastCode($code, $param);
        $data['code'] = $code.'/'.$data['code_number'];
        
        // Setting new Document
        $file_explode = explode('.', $doc['document']); 
        $code_explode = explode('/',$data['code']);
        $code_implode = implode('_',$code_explode);
        $data['document'] = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
        
        $new_id = $this->addNewDocument($data);
        if ($new_id){                           

            // update tanggal
            $sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$new_id  ";
            dbQuery($sql);

            $this->addHistory($id, "This document is moved to document no. ".$data['code']);
            $this->addHistory($new_id, "This document is moved from document no. ".$doc['code']);

            $document_id = $id;

            //Copy lama ke baru 
            $CI = & get_instance();
            $config = $CI->config->item('utils');
            $folder = "document/files";
            $full_path = $config['full_upload_dir'] . $folder . '/';


            $from = $full_path.$doc['document'];
            $to = $full_path.$data['document'];

            copy($from, $to);

            if ($data['is_converted']==1) {
                $old_docs = explode('.',$doc['document']);
                unset($old_docs[count($old_docs)-1]);
                $old_doc = implode('.', $old_docs);
                $new_docs = explode('.',$data['document']);
                unset($new_docs[count($new_docs)-1]);
                $new_doc = implode('.', $new_docs);

                $from = $full_path.$old_doc.'.pdf';
                $to = $full_path.$new_doc.'.pdf';
                copy($from, $to);
            }

            if ($data['is_upload']==0){
                $old_docs = explode('.',$doc['document']);
                unset($old_docs[count($old_docs)-1]);
                $old_doc = implode('.', $old_docs);
                $new_docs = explode('.',$data['document']);
                unset($new_docs[count($new_docs)-1]);
                $new_doc = implode('.', $new_docs);

                $from = $full_path.$old_doc.'.txt';
                $to = $full_path.$new_doc.'.txt';
                copy($from, $to);
            }

            // reviewer
            if ($type['need_reviewer']=='1'){
                $this->load->model('Document_reviewer_model');
                $data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, $workflow_id);
                $data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
                $this->Document_reviewer_model->create($new_id,Workflow_model::REVISI, $data_reviewer);
            }
            
            // ISO
            $data_iso = array();
            $data_iso['document_id'] = $new_id;
            $iso_id = $this->model->getDocumentISOS($document_id);
            foreach ($iso_id as $key => $value) {
                $data_iso['iso_id'] = $value;
                dbInsert("DOCUMENT_ISOS", $data_iso);
            }

            // Klausul
            if ($type['klausul']=='1'){
                $this->load->model('Document_klausul_model');
                $data_klausul['document_id'] = $new_id;
                $klausul = $this->Document_klausul_model->filter(" document_id=$document_id ")->getAll();
                foreach ($klausul as $key => $value) {
                    $data_klausul['klausul_id'] = $value['klausul_id'];
                    dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
                }
            }
        
            // Unit Kerja
            $this->load->model('Document_unitkerja_model');
            $unitkerja_related = $this->Document_unitkerja_model->filter(" where document_id=$document_id ")->getAll();
            if ($unitkerja_related!=NULL){
                $data_unit_kerja = array();
                foreach ($unitkerja_related as $k_unitkerja_related => $v_unitkerja_related) {
                    $data_unit_kerja = $v_unitkerja_related;
                    $data_unit_kerja['document_id'] = $new_id;
                    dbInsert('DOCUMENT_UNITKERJA', $data_unit_kerja);
                }   
            }
            
            // Related
            // $relatedDocument = $this->input->post('related',true);
            // $this->model->setRelatedDocument($insert,$relatedDocument);

            $data = array();
            $data['user_id'] = $doc['creator_id'];
            $data['user_jabatan_id'] = $doc['creator_jabatan_id'];
            dbUpdate('documents', $data, "id=$new_id");
        }
    }

    public function revisenow(){
        $document_id = (int)$this->input->post('id', true);
        $getNewid_sql = 'select id from documents where rownum<=1 and parent='.$document_id." order by id desc";
        $new_id = dbGetOne($getNewid_sql);
        $this->data['new_id'] = $new_id;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"), $this->data);
        // echo $new_id;
    }

    public function ajaxAskDownload(){
        $document_id = (int)$this->input->post('id', true);
        $data = array();
        $data['document_id'] = $document_id;
        $data['user_id'] = $this->user['id'];
        $data['reason'] = $this->input->post('reason', true);
        $this->load->model('Document_download_model');
        $exist = $this->Document_download_model->filter("where document_id='".$document_id."' and user_id='".$this->user['id']."'")->getOne('id');
        if($exist){
            $last_id =  $exist;
            $update['is_allowed'] = 0;
            $update['valid_until'] = null;
            $update['reason'] = $data['reason'];
            // $this->Document_download_model->updateParentFunction($last_id,$update);
        }
        else{
            $this->Document_download_model->insert($data);
            $last_id = $this->Document_download_model->getLastId();
        }
        // die();
        $this->addActivity($document_id, "Ask To Download this Document.");
        $this->load->model('Notification_model');
        $this->Notification_model->generate(Notification_model::ACTION_ASK_DOWNLOAD, $last_id, $this->user['id']);
        $this->data['last_id'] = $last_id;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"), $this->data);
    }

    public function download_watermark($id=null, $word=0){
        $have_header = $this->model->getOne($id, "have_header");
        $this->load->model('Document_download_model');
        $userid     = $this->user['id'];
        $dataDoc = $this->model->getDocumentFlow($id);
        $ciConfig = $this->config->item('utils');
        $access = 0;
        $access = $this->Document_download_model->filter('where document_id='.$id.' and user_id='.$userid.'  ')->getOne("is_allowed");
        if($dataDoc['CREATOR_ID'] == $this->user['id'] && ($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000')){
            $access = 1;
            $jenis = 'pdf';
            if($word == 1){
                $jenis = 'word';
            }
            $text = 'Download the '.$jenis.' file with Creator Role';
            $this->addActivity($id, $text);
        }
        elseif($dataDoc['CREATOR_ID'] == $this->user['id']){
            $access = 1;
            $jenis = 'pdf';
            if($word == 1){
                $jenis = 'word';
            }
            $text = 'Download the '.$jenis.' file with Creator Role';
            $this->addActivity($id, $text);
        }
        elseif($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
            $access = 1;
            $jenis = 'pdf';
            if($word == 1){
                $jenis = 'word';
            }
            $text = 'Download the '.$jenis.' file with Document Controller Holding Role';
            $this->addActivity($id, $text);
        }
        if($word == 1){
            if($access == 1){
                $fname = $dataDoc['DOCUMENT'];
                $path = $ciConfig['full_upload_dir'].'document/files/'.$fname;
                $fsize = filesize($path);
                header('Content-Description: File Transfer'); 
                header('Content-Type: application/octet-stream');
                header("Content-Transfer-Encoding: Binary"); 
                header('Expires: 0'); 
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
                header('Pragma: public'); 
                header("Content-disposition: attachment; filename=\"" . $fname . "\""); 
                header("Content-Length: ".$fsize);
                ob_clean();
                flush();
                // echo readfile($file['link']);
                // echo readfile($file['link_watermark']);
                return readfile($path);
            }
                
        }
        else{
            $explodeFile= explode('.', $dataDoc['DOCUMENT']);
            $extfile    = end($explodeFile); // extensi dari file yang di cursor
            unset($explodeFile[count($explodeFile)-1]);
            $nama_file = implode('.', $explodeFile);
            $document_name = $nama_file.'_'.$userid.'_download.pdf';
            // echo $access;
            if($have_header == 1 && $access == 1 ){
                
                $path = $ciConfig['full_upload_dir'].'document/files/'.$document_name;
                if(!file_exists($path)){
                    $userid     = $this->user['id'];
                    $idCompany  = ltrim($this->user['company'],"0");
                    $username   = $this->user['name'];
                    $this->generateFileDownload($nama_file.".pdf",$id, $idCompany, $username, $userid, 'pdf');
                    // create file download
                }   
                $watermarked_hash['user_id'] = $userid;
                $watermarked_hash['document_name'] = $document_name;
                $encryption =hash_hmac('sha256',$document_name.$userid, date("d-m-Y h:i:sa"));
                $watermarked_hash['encryption'] = $encryption;
                if ($this->model->isExistWatermarked($document_name, $userid)){
                    dbUpdate('document_watermarked_hash', $watermarked_hash, "document_name='".$document_name."' and user_id=".$userid);
                } else {
                    dbInsert('document_watermarked_hash', $watermarked_hash);
                }
                // echo $name;
                // $file = $this->model->getDocumentWithLink($id);
                $fsize = filesize($path);

                $fname = basename($document_name);
                header('Content-Description: File Transfer'); 
                header('Content-Type: application/octet-stream');
                header("Content-Transfer-Encoding: Binary"); 
                header('Expires: 0'); 
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
                header('Pragma: public'); 
                header("Content-disposition: attachment; filename=\"" . $fname . "\""); 
                header("Content-Length: ".$fsize);
                ob_clean();
                flush();
                // echo readfile($file['link']);
                // echo readfile($file['link_watermark']);
                return readfile($path);

                
            }
            else{
                // $download = $this->Document_download_model->getBy($id);
                $document = $this->model->getDocumentBy($this->user['id'],$id);
                // echo "<pre>";print_r($document);echo "</pre>";
                $this->data['title'] = $document['type_name'].' "'.$document['title'].'"';
                // $this->data['title'] = $;
                $buttons = array();
                $buttons[] = array('label' => ' Back', 'type' => 'success', 'icon' => 'chevron-left', 'click' => 'goBack()' );
                $this->data['buttons'] = $buttons;
                $this->data['access'] = $access;$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success"), $this->data);
                // $this->template->viewDefault('document_download_watermark',$this->data);
                // die('belum ada halaman utuk warning ini');
            }
        }
        
    }


}

?>
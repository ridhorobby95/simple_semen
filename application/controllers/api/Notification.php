<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
	
	/**
	 * Controller Notification
	 * @author Integra
	 * @version 1.0
	 */ 
	class Notification extends PrivateApiController {
		protected $title = 'Notifications';
		
		/**
		 * Halaman default
		 * @param int $page
		 */
		public function index() {
			redirect($this->ctl.'/me');
		}
		
		/**
		 * Halaman daftar notifikasi
		 * @param int $page
		 */
		public function me($page=0) {
			$this->data['buttons'] = $buttons;
			$user_id=$this->user['id'];
			$this->data['data'] = $this->model->with(" left join notification_users nu on nu.notification_id=n.id ")->filter(" nu.user_id=$user_id ")->order(' n.created_at desc ')->getAll(true);
			$this->load->model('Notification_sender_model');
			$this->load->model('Notification_user_model');
			foreach ($this->data['data'] as $key => $value) {
				$notification_id = $value['id'];
				$sender = $this->Notification_sender_model->filter(" where notification_id=$notification_id ")->getOne('user_id');
				$this->data['data'][$key]['sender'] = $this->User_model->getUser($sender);
				if (strtolower($this->data['data'][$key]['sender']['username'])=='user20'){
					$this->data['data'][$key]['sender']['name'] = '';
					$this->data['data'][$key]['sender']['photo'] = $this->User_model->getSystemPhoto();
				}
				$this->data['data'][$key]['users'] = $this->Notification_user_model->filter(" where notification_id=$notification_id and user_id=$user_id")->getBy();
			}
			// echo '<pre>';
			// print_r($this->data['data']);
			// die();
			$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
		}
		
		/**
		 * Set baca untuk semua notifikasi
		 * @param int $id
		 */
		public function read_all() {
			$this->load->model('Notification_user_model');
			$this->Notification_user_model->setAsUnRead($this->user['id']);
			
			$ok = true;
			$msg = 'Berhasil set baca semua notifikasi';
			
			$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $msg), $this->data);
		}
		
		/**
		 * Set baca untuk notifikasi
		 * @param int $id
		 */
		public function read($id) {
			$this->load->model('Notification_user_model');
			$this->Notification_user_model->update_by(array('user_id' => $this->user['id'], 'notification_id' => $id),array('status' => Notification_user_model::STATUS_READ));
			$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'));
		}
	}
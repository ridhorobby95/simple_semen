<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Unitkerja extends PrivateApiController {

    protected $title = 'Work Unit';

    public function index() {
        // $_SESSION['is_company'] = 0;
        if ($_GET['search']!='') {
            $param = strtoupper($this->input->get('search', true));
            $condition = "and (upper(name) like '%$param%' or upper(id) like '%$param%' or upper(\"LEVEL\") like '%$param%' ) ";
        }
        if ($_GET['company'] and $_GET['company']!='0'){
            $company = $this->input->get('company', true);
            $condition .= " and company='".ltrim($company,'0')."' ";
        }

        $condition .=" and upper(\"LEVEL\")!='COMP' ";
        
        $data = $this->model->getAll($condition);
        $this->data['data']   =$data;
        $this->data['title'] = 'Work Unit List';



        // tombol
        $buttons = array();
        if ($this->user['role'] == Role::ADMIN_UNIT or $this->user['role'] == Role::ADMINISTRATOR){
            $buttons[] = array('label' => ' Import Excel', 'type' => 'secondary', 'icon' => 'file-excel-o', 'click' => 'goExport()');
            $buttons[] = array('label' => 'Add Work Unit', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        }
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;
        // unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'data ditemukan'), $this->data);
    }

    /**
     * Halaman export
     * @param int $userid
     */
    public function add_import() {
        parent::add_import();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success'), $this->data);
        // $this->template->viewDefault($this->view, $this->data);
    }


    public function import(){
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'/masters/unitkerja/'.$id)) {
           $files = glob($path_source.'/masters/unitkerja/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."/masters/unitkerja/".$id."/".$metadata;

        if (!file_exists($file)){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to import. Metadata not exist'));
        }

        $this->load->library('Libexcel');

        $tmpfname = $file;
        
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $levels = array("KOMP", "DIR", "DEPT", "BIRO", "SECT", "GRP");

        $column = array();
        $success = array();
        $failed = array();
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data = array();
            $insert_other = array();
            for ($col = 0 ; $col < $worksheetData[0]['totalColumns']; $col++){
                $colString = PHPExcel_Cell::stringFromColumnIndex($col);
                $value =  $worksheet->getCell($colString.$row)->getValue();

                switch($col){
                    case 0:
                        $data['id'] = $value;
                    break;
                    case 1:
                        $data["\"LEVEL\""] = strtoupper($value);
                    break;
                    case 2:
                        $data['name'] = "{$value}";
                    break;
                    case 3:
                        $data['company'] = ltrim($value, '0');
                    break;
                    case 4:
                        $data['parent'] = $value; 
                    break;
                    case 5:
                        $data['start_date'] = "to_date('".$value."', 'dd/mm/yyyy')";
                    break;
                    case 6:
                        $data['end_date'] = "to_date('".$value."', 'dd/mm/yyyy')";
                        if ($value=='' or $value==NULL){
                            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
                        }
                        
                    break;
                }
            }
            if ($data['id']==NULL or $data['id']==''){
                break;
            }

            if ($this->user['role'] == Role::ADMIN_UNIT){
                if ($company!=$this->user['company']){
                    $failed[] = array("id"=>$data['id'], "reason"=>"Not Allowed : you cannot import the work unit to $company");
                    continue;
                }
            }

            $is_exist = $this->model->getOne($data['id'], '1');
            if (!$is_exist){
                // Check Metadata
                // cek id
                if (strlen($data['id'])!=8){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Id not 8 Digit");
                    continue;
                } else {
                    $data['id'] = "{$data['id']}";
                    $data['short'] = $data['id'];
                }

                // cek level
                if (!in_array($data["\"LEVEL\""], $levels)){
                     $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : LEVEL not Exist / Allowed");
                     continue;
                } else {
                    $data["\"LEVEL\""] = "{$data["\"LEVEL\""]}";
                }

                // nama
                if ($data['name']==NULL or $data['name']==''){
                     $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Empty Name");
                     continue;
                }

                //company
                $comp_id = str_pad($data['company'], 8, '0', STR_PAD_LEFT);            
                $is_company_exist = $this->model->show_sql(false)->filter("where id='".$comp_id."' and \"LEVEL\"='COMP' ")->getOneFilter('1');
                if (!$is_company_exist){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Company not Exist");
                    continue;
                } else {
                    $data['company'] = "{$data['company']}";
                }

                // parent
                $parent_id = str_pad($data['parent'], 8, '0', STR_PAD_LEFT);                
                $is_parent_exist = $this->model->filter("where id='".$parent_id."' ")->getOneFilter('1');
                if (!$is_parent_exist){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Upper Work Unit not Exist");
                    continue;
                } else {
                    $data['parent'] = "{$data['parent']}";
                }

                if (dbInsert("Unitkerja", $data)){
                    $success[] = array("id" => $data['id'], "reason" => "Successfully added");
                } else {
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata");
                }
                
            } else {
                // Keluarkan kalo sudah ada
                $failed[] = array("id"=>$data['id'], "reason"=>"Already Exist");
            }
            
        }
        // ECHO TABELNYA
        // echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
        // echo "<tr></tr>";
        // echo "<tr><td>Di Import Tanggal</td><td>".date("d M Y")."</td></tr>";
        // echo "<tr><td>Di Import Oleh</td><td colspan=3>".SessionManagerWeb::getName()."</td></tr>";
        // echo "<tr></tr><tr></tr>";

        // echo "<tr><td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td></tr>";
        // echo "<tr></tr>";
        // echo "<tr>
        //         <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Work Unit Code</td>
        //         <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>
        //     </tr>";
        // foreach ($failed as $fail) {
        //     echo "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
        //             {$fail['id']}
        //         </td>
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
        //             {$fail['reason']}
        //         </td>
        //     </tr>";
        // }
        // echo "<tr></tr><tr></tr>";

        // echo "<tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>";
        // echo "<tr></tr>";
        // echo "<tr>
        //         <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Work Unit Code</td>
        //         <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>
        //     </tr>";
        // foreach ($success as $s) {
        //     echo "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
        //             {$s['id']}
        //         </td>
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
        //             {$s['reason']}
        //         </td>
        //     </tr>";
        // }
        // echo '</table>';
        // $_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja'] = array(
        //     "success" => $success,
        //     "failed" => $failed
        // );
        $this->data['success'] = $success;
        $this->data['failed'] = $failed;
        $buttons['download'] = array('label' => 'Download Report', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownload()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;

        $this->_deleteFiles($path_source."masters/unitkerja/".$id);
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success!!'), $this->data);
    }

    public function add($is_company=0) {
        parent::add($is_company);
    }

    public function addCompany(){
        self::edit(null, 1);
    }

    public function edit($id = null, $is_company=0) {
        $this->data['title'] = $this->data['title'];
        $levels = $this->model->column(array("distinct \"LEVEL\""=>"\"level\""))->limit(100000)->order("\"LEVEL\" NULLS first")->getAll();
        $this->data['levels'] = Util::toMap($levels, 'level', 'level');

        $companies = $this->model->column(array("id"=>"\"id\"", "name"=>"\"name\""))->limit(1000)->order("name")->getAll("and \"LEVEL\"='COMP' ");
        $companies[""] = NULL;
        $this->data['companies'] = Util::toMap($companies, 'id', 'name');
        $this->data['workunits'] = array(''=>NULL);
        $this->data['is_company'] = $is_company;
        parent::edit($id);
        if (isset($id)) {
            $this->data['data'] = $this->model->defaultColumn()->getBy($id);
            $this->data['workunits'] = $this->model->order('name')->getAllList(" company='".$this->data['data']['company']."' ");
            unset($this->data['workunits'][0]);
            if ($this->data['data']['photo']!=NULL)
                $this->data['data']['photo'] = Image::getLocation($id, Image::IMAGE_THUMB, $this->data['data']['photo'], 'company');
            // var_dump($this->data['data']);
            // die();
            if ($this->data['data']['parent']=='0')
                $this->data['buttons']['back']['click'] = "goBack('company')";
            if ($this->user['role'] == Role::ADMIN_UNIT){
                if ($this->user['company']!=$this->data['data']['company']){
                    $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Cannot edit this work unit! You only can edit work unit in your company!"));
                }
            }
        }
        else
            $this->data['data'] = array("parent" => NULL);
        
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Success!!"), $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create($is_company=0) {
        $data = $this->input->post(null, true);

        // if (strlen($data['id'])!=8){
        //     SessionManagerWeb::setFlashMsg(false, 'Code must be 8 Characters');
        //     redirect($this->ctl.'/add/'.$_SESSION['is_company']);
        // }

        if ($is_company!=0){
            if (strlen($data['id'])!=4){
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Code Company must be 4 Characters"));
            }
        }
        $data['id'] = str_pad($data['id'], 8, '0', STR_PAD_LEFT);

        $is_exist = $this->model->getOne($data['id'], '1');
        if ($is_exist) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $data['id'].' already exist. Please Fill with Another Code.'));
        }

        $data["\"LEVEL\""] = $data['level'];
        unset($data['level']);

        

        // if ($_SESSION['is_company']){
        //     $data["\"LEVEL\""] = 'COMP';
        //     $data['parent'] = 0;
        //     $data['company'] = $data['id'];
        // }
        $data['company'] = ltrim($data['company'], '0');
        unset($data['company_name']);
        $data["\"INITIAL\""] = $data['initial'];
        unset($data['initial']);

        $data['start_date'] = "to_date('".date('d/m/Y')."', 'dd/mm/yyyy')";
        if ($data['end_date']==NULL or $data['end_date']==''){
            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
        } else {
            $data['end_date'] = "to_date('".$data['end_date']."','dd/mm/yyyy')";
        }

        $insert = dbInsert('Unitkerja', $data);

        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Successfully add new Data"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed to add Data"));
        }

    }

    /**
     * Edit data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
    public function update($id, $is_company=0) {
        $data = $this->input->post(null,true);
        unset($data['id']);

        $data["\"INITIAL\""] = $data['initial'];
        unset($data['initial']);
        $data['company'] = ltrim($data['company'], '0');

        if ($is_company==0){
            unset($data['company']);
            $data['parent'] = 0;
        } else {
            $data["\"LEVEL\""] = $data['level'];
            unset($data['level']);
        }

        if ($data['end_date']==NULL or $data['end_date']==''){
            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
        } else {
            $data['end_date'] = "to_date('".$data['end_date']."','dd/mm/yyyy')";
        }

        // update
        if ($_FILES['photo']['error'] == UPLOAD_ERR_OK){
            $upload = Image::upload('photo', $id, $_FILES['photo']['name'], 'company');
            if ($upload === TRUE) {
                $data['photo'] = $_FILES['photo']['name'];
                // echo "a";
            } else {
                unset($_FILES['photo']);
            }
        }
        else
            unset($_FILES['photo']);
        $update = dbUpdate('Unitkerja', $data, "id='".$id."'");
        Image::opacityLogo($id);
        // $update = $this->model->save($id, $data, FALSE);

        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Successfully update Data"));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed to update Data"));
        }
    }

    public function company(){
        // $_SESSION['is_company'] = 1;
        $condition = "";
        if ($_GET['search']!='') {
            $param = strtoupper($this->input->get('search', true));
            $condition = "and (upper(name) like '%$param%' or upper(id) like '%$param%' or upper(\"LEVEL\") like '%$param%' )";
        }
        $condition .=" and parent='0'";

        $data = $this->model->show_sql(false)->getAll($condition);
        $this->data['data']   =$data;
        $this->data['title'] = 'Company List';

        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Add Company', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd(1)');
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;
        
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, "Successfully get Data"), $this->data);
        // $this->template->viewDefault($this->class.'_index', $this->data);
    }

    function getUnitKerja($company='2000'){
        $unitkerja =$this->Unitkerja_model->order('name')->getAllList(" company='$company' ");
        // echo $a;
        if($unitkerja){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'data ditemukan'), $unitkerja);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $unitkerja));
        }
    }


    public function ajaxGetCreator($unitkerja_id){
        $post = $this->postData;
        $type = $post['type'];
        // $type = 4;
        // $data = $this->model->getCreatorAndApprover($unitkerja_id);
        // $type = $this->input->post('type', true);
        $this->load->model('User_model');
        $data = array();
        // $atasan = SessionManagerWeb::getVariablesIndex('atasan');
        $atasan = $this->Unitkerja_model->filter('')->getAtasan($unitkerja_id, $this->user['id']);
        $creator = $this->Unitkerja_model->filter('')->getCreator($unitkerja_id, $type);
        // $creators = SessionManagerWeb::getVariablesIndex('creator');
        $user_is_creator = false;
        foreach ($creators[$unitkerja_id][$type] as $key => $value) {
            if($this->user['id'] == $key){
                $user_is_creator = true;
                break;
            }
        }
        // echo "<pre>";print_r($creators[$unitkerja_id][$type]);die();
        // echo "<pre>";print_r($atasan);die();
        // set Creator
        $this->load->model('User_model');
        $eselon = $this->User_model->getBy($this->user['id'],'eselon_name');
    if(($eselon == 'Eselon 1' || $eselon == 'Eselon 2' || $eselon == 'Eselon 3') && $user_is_creator == true){ // Jika dia minim eselon 3 dan bisa jadi creator, maka nama pertama adalah nama dia
        $data['creator']['user_id'] = $this->user['id'];
    }
    else{
        $data['creator']['user_id'] = $atasan[$unitkerja_id];
        if ($atasan[$unitkerja_id]==NULL){
            $data['creator']['user_id'] = '';
        }
    }
        
        $data['creator']['name'] = $this->User_model->getBy($data['creator']['user_id'], 'name');
        if(($data['creator']['user_id'])){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'data ditemukan'), $data);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'gagal'));
        }
    }

    function ajaxGetListCreator($unitkerja_id,$type){
        $this->load->model('User_model');
        $this->load->model('Tingkatan_document_types_model');
        $this->load->model('Document_types_model');
        $post = $this->postData;

        $tingkatan = $this->User_model->getEselon($this->user['id']);
        $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
        $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);
    // $type = $post['type'];
        // $type = $this->input->post('type', true);
        // $type = 4;

        $data = array();
        
        $creators = $this->model->filter('')->getCreator($unitkerja_id, $jenis);
        
        // set Creator
        $cek = array();
        $no = 0;
        foreach ($creators[$unitkerja_id][$type] as $key => $value) {
            $data[$no]['id'] =$key;
            $data[$no]['text'] = $value;
            $cek[$no] = $key;
            $no++;
        }

        // getPgs
        $this->load->model('Atasan_model');
        $this->load->model('User_model');
        // $nopeg = $this->User_model->filter("where \"user_id\"='".SessionManagerWeb::getUserID()."'")->getOneFilter("karyawan_id");
        // $atasan = $this->Atasan_model->filter(" where k_nopeg='".$nopeg."' and (atasan1_unitkerja='$unitkerja_id' or atasan2_unitkerja='$unitkerja_id') ")->getAll();
        // $temp = array();
        // foreach ($atasan as $key => $value) {
        //     $id = $this->User_model->filter("where \"karyawan_id\"='".$value['atasan1_nopeg']."'")->getOneFilter("user_id");
        //     if (!in_array($id, $cek)){
        //         $name = $this->User_model->filter("where \"user_id\"='".$id."'")->getOneFilter("name");
        //         $data[$no]['id'] = $id;
        //         $data[$no]['text'] = $name;
        //         $temp[] = $id;
        //         $no++;
        //     }
        // }

        // tambah atasan
        $this->load->model("Unitkerja_model");
        $atasan_nopeg = $this->Unitkerja_model->getAtasanByLevel($unitkerja_id, $this->user['id']);
        if (!in_array($atasan_nopeg, $cek)){
            $name = $this->User_model->filter("where \"user_id\"='".$atasan_nopeg."'")->getOneFilter("name");
            $data[$no]['id'] = $atasan_nopeg;
            if ($data[$no]['id']==NULL){
                $data[$no]['id'] = '';
            }
            $data[$no]['text'] = $name;
        }
        // echo "<pre>";print_r($data);die();
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'data ditemukan'), $data);
        // echo json_encode($data);
    }

    public function ajaxGetApprover($mycompany=null, $creator_id=null, $type_id=null){
        $this->load->model('User_model');
        $this->load->model('Atasan_model');
        $data = array();
        // $post = $this->postData;
        // $creator_id = 61273;
        // $type_id = 4;
        // $creator_id = $post['creator_id'];
        // $creator_id = $this->input->post('creator_id');
        $creator_nopeg = $this->User_model->getBy($creator_id, 'karyawan_id');
        // $type_id = $post['type_id'];
        // $type_id = $this->input->post('type_id', null);
        if($mycompany == null){
            $this->load->model('User_model');
            $mycompany = $this->User_model->getBy($this->user['id'], 'company');
            if($this->user['role'] == Role::EKSTERNAL){
                $mycompany = $post['selectedCompany'];
            }
            $data['mycompany'] = $mycompany;
        }

        // $atasan = $this->Atasan_model->filter(" where k_nopeg='$creator_nopeg' and (atasan1_pgs is null and atasan2_pgs is null) ")->getBy();
        $atasan = $this->Atasan_model->filter(" where k_nopeg='$creator_nopeg' ")->getBy();
        // echo "<pre>";print_r($atasan);die();
        $atasan1_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan1_nopeg']."' ")->getOneFilter('user_id');
        $atasan2_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan2_nopeg']."' ")->getOneFilter('user_id');

        // set Approver
        // if ($this->User_model->getBy($creator_id, 'is_chief')=='X'){
        //     $data['approver'][0]['id'] = $atasan1_id;
        // } else {
        //     $data['approver'][0]['id'] = $atasan[$unitkerja_id];
        // }
        $data['approver'][0]['id'] = $atasan1_id;
        // if ($atasan['atasan1_pgs']=='X'){
        //     $data['approver'][0]['id'] = $atasan2_id;
        // }

        if ($this->user['id']==$creator_id){
            $data['approver'][0]['id'] = $atasan1_id;
        }
        
        
        $data['approver'][0]['text'] = $this->User_model->getBy($data['approver'][0]['id'], 'name');
        if ($type_id=='2'){
            // $data['approver'][1]['id'] = $atasan[$mycompany];
            $data['approver'][1]['id'] = $this->getDirut($mycompany);
            $data['approver'][1]['text'] = $this->User_model->getBy($data['approver'][1]['id'], 'name');
            if ($data['approver'][0]['id']==$data['approver'][1]['id']){
                unset($data['approver'][1]);
            }
            // else {
            //     if ($data['approver'][0]['id']==null){
            //         $data['approver'][0] = $data['approver'][1];
            //         unset($data['approver'][1]); 
            //     }
            // }
            
        }

        if ($data['approver'][0]['id']==null){
            if ($data['approver'][1]['id']==null){
                $data = array();
            } else {
                $data['approver'][0] = $data['approver'][1];
                unset($data['approver'][1]); 
            }
            
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'data ditemukan'), $data);
        // echo "<pre>";print_r($data);die();
        // echo json_encode($data);
    }

    public function getDirut($company='2000'){
        // if ($company=='2000'){
            $sql = "select id from unitkerja where lower(name) like '%president directorate%' and company='$company' and \"LEVEL\"='DIR' and sysdate < end_date order by id desc";
            $unitkerja_dirut = dbGetOne($sql);
            $sql_user = " select \"user_id\" from users_view where \"unitkerja_id\"='$unitkerja_dirut' and \"is_chief\"='X' and \"is_active\"=1 ";
            $dirut_id = dbGetOne($sql_user);
            return $dirut_id;
    }
}

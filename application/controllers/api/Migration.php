<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
class Migration extends PrivateApiController {

    public function __construct() {
        parent::__construct();
    }

    public function index($page=1) {
        $type = $this->input->get('type', null);
        if ($type==null)
            $type=1;
        $data['type']=$type;
       
        $this->load->model('Document_types_model');
        $types = $this->Document_types_model->getAllJenis();
        $data['types'] = Util::toMap($types, 'id', 'type');
        $data['page'] = $page;
        $data['files'] = $this->_getdraftmigrasi();
        if ($page==2){
            $data['metadata'] = $this->_getdraftfile('document/metadata');
        }

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $data);
    }

    public function overwrite($page=1){
        $this->data['data']   =$data;
        $this->data['title'] = 'Migration Overwrite';
        $type = $this->input->get('type', null);
        if ($type==null){
            $type=1;
        }
        $this->data['type']=$type;


        // tombol
        // $buttons = array();
        // $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        // $this->data['buttons'] = $buttons;
       
        $this->load->model('Document_types_model');
        // $types = $this->Document_types_model->getAllJenis();
        // $this->data['types'] = Util::toMap($types, 'id', 'type');
        // $this->data['types'][""] = "Choose document types...";
        $this->data['page'] = $page;
        $this->data['files'] = $this->_getdraftmigrasi();
        if ($page==2){
            $this->data['metadata'] = $this->_getdraftfile('document/metadata');
        }
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $this->data);
        // $this->template->viewDefault('migration_overwrite', $this->data);
    }
    public function migrateOverwrite(){
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'document/metadata/'.$id)) {
           $files = glob($path_source.'document/metadata/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."document/metadata/".$id."/".$metadata;

        if (!file_exists($file)){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to migrate document. Metadata not exist'));
        }
        
        $folder = "document/temp/";
        $full_path = $ciConfig['full_upload_dir'] . $folder . '/'.$id.'/';

        $this->load->model('User_model');
        $this->load->model('Document_model');
        $this->load->library('Libexcel');
        $data = array();
        $index = 0;
        $tmpfname = $file;
         // echo $tmpfname.'<br>';
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getActiveSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $index = 0;
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data[$index]['code'] = $worksheet->getCell('A'.$row)->getValue();
            $data[$index]['revision'] = $worksheet->getCell('B'.$row)->getValue();
            $code = $worksheet->getCell('A'.$row)->getValue();
            $rev  = $worksheet->getCell('B'.$row)->getValue();
            $sql = "SELECT * FROM DOCUMENTS WHERE \"CODE\"= '".$code."' AND \"REVISION\"=".$rev." ";
            $res = dbGetRow($sql);
            if($res != NULL){
                $data[$index]['exist'] = "Data ada";
                $data[$index]['document'] = $res['DOCUMENT'];
                // echo $filename."<br>";
                // echo $path_source."document/temp/".$id."/".$filename.".*";
                if(file_exists($path_source."document/temp/".$id."/".$res['DOCUMENT'])){
                    $data[$index]['document_overwrite'] = 'Ada';
                    if(file_exists($path_source."document/files/".$res['DOCUMENT'])){
                        $data[$index]['document_original'] = 'Ada';
                        $filename = $res['DOCUMENT'];
                        $explodeFile= explode('.', $filename);
                        $extfile    = end($explodeFile); // extensi dari file yang di cursor
                        unset($explodeFile[count($explodeFile)-1]);
                        $nama_file = implode('.', $explodeFile);
                        if($extfile == 'xlsx' || $extfile == 'xls' ){
                            if(file_exists($path_source."document/temp/".$id."/".$nama_file.'.pdf')){
                                rename($path_source.'document/files/'.$filename,$path_source.'document/files/'.$nama_file.'_temp.'.$extfile); // hapus xlsx
                                unlink($path_source.'document/files/'.$nama_file.".pdf"); // hapus pdf
                                rename($path_source."document/temp/".$id."/".$res['DOCUMENT'],$path_source.'document/files/'.$res['DOCUMENT']);  // pindahkan xlsx baru ke files
                                rename($path_source."document/temp/".$id."/".$nama_file.".pdf",$path_source.'document/files/'.$nama_file.".pdf"); // pindahkan pdf ke files
                                if(file_exists($path_source.'document/files/'.$res['DOCUMENT']) && $path_source.'document/files/'.$nama_file.".pdf"){
                                    $update = "UPDATE DOCUMENTS SET HAVE_HEADER=0 WHERE CODE='".$res['CODE']."' AND REVISION='".$res['REVISION']."'";
                                    $run = dbQuery($update);
                                    $data[$index]['status_overwrite'] = 'Berhasil';
                                    unlink($path_source.'document/files/'.$nama_file.'_temp.'.$extfile);
                                }
                                else{
                                    $data[$index]['status_overwrite'] = 'Gagal';
                                }
                            }
                            else{
                                $data[$index]['status_overwrite'] = 'Gagal';
                            }
                        }
                        else{
                            // echo "tes ".$extfile."<br>";
                            rename($path_source.'document/files/'.$filename,$path_source.'document/files/'.$nama_file.'_temp.'.$extfile);
                            unlink($path_source.'document/files/'.$nama_file.".pdf"); // hapus pdf
                            rename($path_source."document/temp/".$id."/".$res['DOCUMENT'],$path_source.'document/files/'.$res['DOCUMENT']);
                            if(file_exists($path_source.'document/files/'.$res['DOCUMENT'])){
                                $update = "UPDATE DOCUMENTS SET IS_CONVERTED=0,HAVE_HEADER=0 WHERE CODE='".$res['CODE']."' AND REVISION='".$res['REVISION']."'";
                                $run = dbQuery($update);
                                $data[$index]['status_overwrite'] = 'Berhasil';
                                unlink($path_source.'document/files/'.$nama_file.'_temp.'.$extfile);
                            }
                            else{
                                 $data[$index]['status_overwrite'] = 'Gagal';
                            }
                        }
                    }
                    else{
                        $data[$index]['document_original'] = 'Tidak ada';
                        $data[$index]['status_overwrite'] = 'Gagal';
                    }
                }
                else{
                    $data[$index]['document_overwrite'] = 'Tidak ada';
                    $data[$index]['document_original'] = 'Tidak ada';
                    $data[$index]['status_overwrite'] = 'Gagal';
                }
            }
            else{
                $data[$index]['exist'] = "Data tidak ada";
                $data[$index]['document'] = "-";
                $data[$index]['document_overwrite'] = 'Tidak ada';
                $data[$index]['document_original'] = 'Tidak ada';
                $data[$index]['status_overwrite'] = 'Gagal';
            }
            $index++;
                
        }
        $no = 1;
       ?>
       <table border="1">
           <thead>
               <tr>
                   <th>No</th>
                   <th>Document Code</th>
                   <th>Revision</th>
                   <th>Existed Data</th>
                   <th>Document</th>
                   <th>Document Overwrite</th>
                   <th>Document Original</th>
                   <th>Status</th>
               </tr>
           </thead>
           <tbody>
               <?php foreach ($data as $d): ?>
                   <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $d['code'] ?></td>
                        <td><?php echo $d['revision'] ?></td>
                        <td><?php echo $d['exist'] ?></td>
                        <td><?php echo $d['document'] ?></td>
                        <td><?php echo $d['document_overwrite'] ?></td>
                        <td><?php echo $d['document_original'] ?></td>
                        <td><?php echo $d['status_overwrite'] ?></td>
                   </tr>
                <?php $no++; ?>
               <?php endforeach ?>
           </tbody>
       </table>
       <?php
        $this->_deleteFiles($path_source."document/temp/".$id);
        $this->_deleteFiles($path_source."document/metadata/".$id);
            
    }

    public function metadataOverwrite(){
        $this->data['title'] = 'Metadata Overwrite';
        $type = $this->input->get('type', null);
        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;
        $this->load->model('Document_types_model');
        $this->data['metadata'] = $this->_getdraftfile('document/metadata');
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, ''), $this->data);
    }
    
    public function metadataOverwriteUpload(){
        $this->load->model('Document_iso_model');
        $this->load->model('Document_klausul_model');
        $this->load->model('Document_iso_klausul_model');
        $this->load->model('Document_model');
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'document/metadata/'.$id)) {
           $files = glob($path_source.'document/metadata/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."document/metadata/".$id."/".$metadata;

        if (!file_exists($file)){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to migrate document. Metadata not exist'));
        }
        
        $folder = "document/temp/";
        $full_path = $ciConfig['full_upload_dir'] . $folder . '/'.$id.'/';

        $this->load->model('User_model');
        $this->load->model('Document_model');
        $this->load->library('Libexcel');
        $data = array();
        $index = 0;
        $tmpfname = $file;
         // echo $tmpfname.'<br>';
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getActiveSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $index = 0;

        // echo "<pre>";print_r($worksheetData);echo "</pre>";
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data[$index]['code'] = $worksheet->getCell('A'.$row)->getValue();
            $data[$index]['revision'] = $worksheet->getCell('B'.$row)->getValue();
            $data[$index]['title'] = $worksheet->getCell('C'.$row)->getValue();
            // $data[$index]['published_at'] = $worksheet->getCell('D'.$row)->getValue();
            $data[$index]['published_at'] = date('m/d/Y', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCell('D'.$row)->getValue()));
            // $data[$index]['published_at'] = date('m/d/Y', PHPExcel_Shared_Date::ExcelToPHP($pub_date));
            // $val_date = $worksheet->getCell('E'.$row)->getValue();
            $data[$index]['validity_date'] = date('m/d/Y', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCell('E'.$row)->getValue()));
            $data[$index]['status'] = '';
            $sql = "SELECT ID FROM DOCUMENTS WHERE \"CODE\"= '".$data[$index]['code']."' AND \"REVISION\"=".$data[$index]['revision']." ";
            $document_id = dbGetOne($sql);
            if($document_id){
                $data[$index]['status'] .= " > Document Ada<br>";
            }
            else{
                $data[$index]['status'] .= " > Document Tidak Ada<br>";
            }
            
            $update = "UPDATE DOCUMENTS SET TITLE='".$data[$index]['title']."', PUBLISHED_AT=TO_DATE('".$data[$index]['published_at']."' , 'MM/DD/YYYY'), VALIDITY_DATE=TO_DATE('".$data[$index]['validity_date']."', 'MM/DD/YYYY') WHERE ID='".$document_id."'";
            echo $update."<br>";
            $run = dbQuery($update);
            if($run){
                $data[$index]['status'] .= " > Update Dokumen Berhasil<br>";
            }
            else{
                $data[$index]['status'] .= " > Update Dokumen Gagal<br>";
            }

            ///////// ISO //////////
            $iso = $worksheet->getCell('F'.$row)->getValue();
            $data[$index]['iso'] = $iso;
            $isos = explode('##', $iso);
            $this->Document_iso_model->deleteWith($document_id);
            $data[$index]['status_iso'] = '';
            foreach ($isos as $value) {
                $data_iso['document_id'] = $document_id;
                $data_iso['iso_id'] = $this->Document_iso_model->filter(" iso_code='$value' ")->getOne('id');
                if($data_iso['iso_id'] != false || $data_iso['iso_id'] != null){
                    dbInsert("DOCUMENT_ISOS", $data_iso);
                    $data[$index]['status_iso'] .= " > iso ".$value." berhasil diinsertkan , data iso ada<br>";
                }
                else{
                    $data[$index]['status_iso'] .= " > iso ".$value." gagal diinsertkan , data iso tidak ada<br>";
                }
                
            }

            ///////KLAUSUL////////////
            $clause = $worksheet->getCell('G'.$row)->getValue();
            $data[$index]['klausul'] = $clause;
            $sql = "SELECT TYPE_ID FROM DOCUMENTS WHERE \"CODE\"= '".$data[$index]['code']."' AND \"REVISION\"=".$data[$index]['revision']." ";
            $type_id = dbGetOne($sql);
            $need_klausul = $this->Document_types_model->getBy($type_id, 'klausul');
            $data[$index]['status_klausul'] = '';
            if ($need_klausul){
                $data[$index]['status_klausul'] .= " > Dokumen Membutuhkan Klausul<br>";
                $this->Document_klausul_model->deleteWith($document_id);
                $data_klausul['document_id'] = $document_id;
                $clause = $worksheet->getCell('G'.$row)->getValue();
                $clausuls = explode('##', $clause);
                foreach ($clausuls as $val) {
                    if ($val[strlen($val)-1]!='.'){
                        $val .='.';
                    }
                    $data_klausul['klausul_id'] = $this->Document_iso_klausul_model->filter(" code='$val' ")->getOne('id');
                    if($data_klausul['klausul_id'] != false || $data_klausul['klausul_id'] != null){
                        dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
                        $data[$index]['status_klausul'] .= " >> iso ".$value." berhasil diinsertkan , data klausul ada<br>";
                    }
                    else{
                        $data[$index]['status_klausul'] .= " >> iso ".$value." gagal diinsertkan , data klausul tidak sada<br>";
                    }
                    
                }
            }
            else{
                $data[$index]['status_klausul'] .= " > Dokumen Tidak Membutuhkan Klausul";
            }
        }

        ?>
        <table border="1">
            <tr>
                <th>CODE</th>
                <th>REVISION</th>
                <th>TITLE</th>
                <th>PUBLISH DATE</th>
                <th>VALIDITY_DATE</th>
                <th>REQUIREMENTS</th>
                <th>CLAUSES</th>
                <th>STATUS DOCUMENT</th>
                <th>STATUS ISO</th>
                <th>STATUS KLAUSUL</th>
            </tr>
            <?php foreach ($data as $d): ?>
            <tr>
                <td><?php echo $d['code'] ?></td>
                <td><?php echo $d['revision'] ?></td>
                <td><?php echo $d['title'] ?></td>
                <td><?php echo $d['published_at'] ?></td>
                <td><?php echo $d['validity_date'] ?></td>
                <td><?php echo $d['iso'] ?></td>
                <td><?php echo $d['klausul'] ?></td>
                <td><?php echo $d['status'] ?></td>
                <td><?php echo $d['status_iso'] ?></td>
                <td><?php echo $d['status_klausul'] ?></td>
            </tr>
            <?php endforeach ?>
        </table>
        <?php
        // echo "<pre>";print_r($data);echo "</pre>";
        $this->_deleteFiles($path_source."document/metadata/".$id);
    }

    public function migrate($type){
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'document/metadata/'.$id)) {
           $files = glob($path_source.'document/metadata/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."document/metadata/".$id."/".$metadata;

        if (!file_exists($file)){
            SessionManagerWeb::setFlashMsg(false, 'Failed to migrate document. Metadata not exist');
            redirect('/web/migration');
        }
        
        $folder = "document/temp/";
        $full_path = $ciConfig['full_upload_dir'] . $folder . '/'.$id.'/';

        $this->load->model('User_model');
        $this->load->model('Document_model');
        $this->load->library('Libexcel');

        $tmpfname = $file;
        
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        echo "FAILED DOCUMENTS MIGRATIONS : <br><br>";
        echo "<table style='border: 1px solid'>";
        echo "<td style='border: 1px solid'>";
        echo "DOCUMENT CODE";
        echo "</td>";
        echo "<td style='border: 1px solid'>";
        echo "Already Exist Document";
        echo "</td>";
        echo "<td style='border: 1px solid'>";
        echo "Missing Files";
        echo "</td>";
        echo "<td style='border: 1px solid'>";
        echo "False Metadata";
        echo "</td>";
        $column = array();
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data = array();
            $insert_other = array();
            echo "<tr style='border: 1px solid'>";
            for ($col = 0 ; $col < $worksheetData[0]['totalColumns']; $col++){
                $colString = PHPExcel_Cell::stringFromColumnIndex($col);
                $value =  $worksheet->getCell($colString.$row)->getValue();
                $data['type_id'] = $type;
                switch($type){
                    case 1 :
                        switch($col){
                            case 0:
                                $data['code'] = $value;
                                $code_explode = explode('/', $value);
                                $data['code_number'] = end($code_explode);
                                $sql = "select 1 from documents where code like '%$value%'";
                                $is_exist = dbGetOne($sql);
                                $data['is_exist'] = $is_exist;
                                if ($is_exist){
                                    break;
                                }
                            break;
                            case 1:
                                $data['revision'] = (int)$value;
                                $file = $full_path.implode('_',$code_explode).'_R'.$value;
                                $migration_files = glob($file . '*');
                                $ext = '';
                                $temp = '';
                                foreach ($migration_files as $migration_file) {
                                    $temp = end(explode('.',$migration_file));
                                    if ($temp != 'pdf'){
                                        $ext = $temp;
                                    } 
                                }
                                if ($ext==''){
                                    $ext = $temp;
                                }
                                $data['document'] = basename($file.'.'.$ext);
                                $data['is_exist_file'] = true;
                                if (empty($migration_files)){
                                    $data['is_exist_file'] = false;
                                    
                                    break;
                                }
                            break;
                            case 2:
                                $data['title'] = $value;
                            break;
                            case 3:
                                $data['unitkerja_id'] = (string)$value;
                            break;
                            case 4:
                                // Related workunit
                                $column[] = 'related_workunit';
                                $insert_other['related_workunit'] = $value; 
                            break;
                            case 5:
                                // Business process
                                $column[] = 'prosesbisnis';
                                $this->load->model('Document_prosesbisnis_model');
                                $data['prosesbisnis_id'] = $this->Document_prosesbisnis_model->filter(" where code='$value' ")->getOneFilter('id');
                            break;
                            case 6:
                                // Requirement
                                $column[] = 'iso';
                                $insert_other['iso'] = $value; 
                            break;
                            case 7:
                                //related document
                                $column[] = 'related_document';
                                $insert_other['related_document'] = $value; 
                            break;
                            case 8:
                                $column[] = 'published_at';
                                $insert_other['published_at'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 9:
                                $column[] = 'validity_date';
                                $insert_other['validity_date'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 10:
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                $data['creator_id'] = (int)$this->User_model->getUserIDfromNopeg($value);

                            break;
                                
                            case 11;
                                $data['creator_jabatan_id'] = (string)$value;
                            break;
                            case 12:
                                // reviewer
                                $column[] = 'reviewer';
                                $insert_other['reviewer'] = (string)$value; 
                            break;
                            case 13:
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                $data['approver_id'] = (int)$this->User_model->getUserIDfromNopeg($value);
                            break;
                            case 14:
                                $data['approver_jabatan_id'] = (string)$value;
                            break;
                            case 15:
                                $data['description'] = $value;
                            break;
                        }
                    break;
                    case 2:
                        switch($col){
                            case 0:
                                $data['code'] = $value;
                                $code_explode = explode('/', $value);
                                $data['code_number'] = end($code_explode);
                                $sql = "select 1 from documents where code like '%$value%'";
                                $is_exist = dbGetOne($sql);
                                $data['is_exist'] = $is_exist;
                                if ($is_exist){
                                    break;
                                }
                            break;
                            case 1:
                                $data['revision'] = (int)$value;
                                $file = $full_path.implode('_',$code_explode).'_R'.$value;
                                $migration_files = glob($file . '*');
                                $ext = '';
                                $temp = '';
                                foreach ($migration_files as $migration_file) {
                                    $temp = end(explode('.',$migration_file));
                                    if ($temp != 'pdf'){
                                        $ext = $temp;
                                    } 
                                }
                                if ($ext==''){
                                    $ext = $temp;
                                }
                                // die($migration_file);
                                $data['document'] = basename($file.'.'.$ext);
                                $data['is_exist_file'] = true;
                                if (empty($migration_files)){
                                    $data['is_exist_file'] = false;
                                    
                                    break;
                                }
                            break;
                            case 2:
                                // $column[] = 'title';
                                $data['title'] = $value;
                            break;
                            case 3:
                                // $column[] = 'unitkerja_id';
                                $data['unitkerja_id'] = (string)$value;
                            break;
                            case 4:
                                // Related workunit
                                $column[] = 'related_workunit';
                                $insert_other['related_workunit'] = $value; 
                            break;
                            case 5:
                                // Business process
                                $column[] = 'prosesbisnis';
                                $this->load->model('Document_prosesbisnis_model');
                                $data['prosesbisnis_id'] = $this->Document_prosesbisnis_model->filter(" where code='$value' ")->getOneFilter('id');
                            break;
                            case 6:
                                // Requirement
                                $column[] = 'iso';
                                $insert_other['iso'] = $value; 
                            break;
                            case 7:
                                $column[] = 'clausuls';
                                $insert_other['clausuls'] = $value;
                            break;
                            case 8:
                                //related document
                                $column[] = 'related_document';
                                $insert_other['related_document'] = $value; 
                            break;
                            case 9:
                                $column[] = 'published_at';
                                // die($value);
                                // $data['published_at'] = strtotime((string)$value);
                                $insert_other['published_at'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 10:
                                $column[] = 'validity_date';
                                $insert_other['validity_date'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 11:
                                // $column[] = 'creator_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['creator_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['creator_id'] = (int)$this->User_model->getUserIDfromNopeg($value);

                            break;
                                
                            case 12;
                                // $column[] = 'creator_jabatan_id';
                                $data['creator_jabatan_id'] = (string)$value;
                            break;
                            case 13:
                                // reviewer
                                $column[] = 'reviewer';
                                $insert_other['reviewer'] = (string)$value; 
                            break;
                            case 14:
                                // $column[] = 'approver_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['approver_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['approver_id'] = (int)$this->User_model->getUserIDfromNopeg($value);
                            break;
                            case 15:
                                // $column[] = 'approver_jabatan_id';
                                $data['approver_jabatan_id'] = (string)$value;
                            break;
                            case 16:
                                $data['description'] = $value;
                            break;
                        }
                    break;
                    case 3:
                        switch($col){
                            case 0:
                                // $column[] = 'code';
                                $data['code'] = $value;
                                $code_explode = explode('/', $value);
                                $data['code_number'] = end($code_explode);
                                $sql = "select 1 from documents where code = '%$value%'";
                                $is_exist = dbGetOne($sql);
                                $data['is_exist'] = $is_exist;
                                if ($is_exist){
                                    break;
                                }
                            break;
                            case 1:
                                // $column[] = 'revision';
                                $data['revision'] = (int)$value;
                                $file = $full_path.implode('_',$code_explode).'_R'.$value;
                                $migration_files = glob($file . '*');
                                $ext = '';
                                $temp = '';
                                foreach ($migration_files as $migration_file) {
                                    $temp = end(explode('.',$migration_file));
                                    if ($temp != 'pdf'){
                                        $ext = $temp;
                                    } 
                                }
                                if ($ext==''){
                                    $ext = $temp;
                                }
                                // die($migration_file);
                                $data['document'] = basename($file.'.'.$ext);
                                $data['is_exist_file'] = true;
                                if (empty($migration_files)){
                                    $data['is_exist_file'] = false;
                                    
                                    break;
                                }
                            break;
                            case 2:
                                // $column[] = 'title';
                                $data['title'] = $value;
                            break;
                            case 3:
                                // $column[] = 'unitkerja_id';
                                $data['unitkerja_id'] = (string)$value;
                            break;
                            case 4:
                                // Related workunit
                                $column[] = 'related_workunit';
                                $insert_other['related_workunit'] = $value; 
                            break;
                            case 5:
                                // Business process
                                $column[] = 'prosesbisnis';
                                $this->load->model('Document_prosesbisnis_model');
                                $data['prosesbisnis_id'] = $this->Document_prosesbisnis_model->filter(" where code='$value' ")->getOneFilter('id');
                            break;
                            case 6:
                                // Requirement
                                $column[] = 'iso';
                                $insert_other['iso'] = $value; 
                            break;
                            case 7:
                                $column[] = 'clausuls';
                                $insert_other['clausuls'] = $value;
                            break;
                            case 8:
                                //related document
                                $column[] = 'related_document';
                                $insert_other['related_document'] = $value; 
                            break;
                            case 9:
                                $column[] = 'published_at';
                                // die($value);
                                // $data['published_at'] = strtotime((string)$value);
                                $insert_other['published_at'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 10:
                                $column[] = 'validity_date';
                                $insert_other['validity_date'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 11:
                                // $column[] = 'creator_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['creator_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['creator_id'] = (int)$this->User_model->getUserIDfromNopeg($value);

                            break;
                                
                            case 12;
                                // $column[] = 'creator_jabatan_id';
                                $data['creator_jabatan_id'] = (string)$value;
                            break;
                            case 13:
                                // reviewer
                                $column[] = 'reviewer';
                                $insert_other['reviewer'] = (string)$value; 
                            break;
                            case 14:
                                // $column[] = 'approver_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['approver_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['approver_id'] = (int)$this->User_model->getUserIDfromNopeg($value);
                            break;
                            case 15:
                                // $column[] = 'approver_jabatan_id';
                                $data['approver_jabatan_id'] = (string)$value;
                            break;
                            case 16:
                                $data['description'] = $value;
                            break;
                        }
                    break;
                    case 4:
                        switch($col){
                            case 0:
                                // $column[] = 'code';
                                $data['code'] = $value;
                                $code_explode = explode('/', $value);
                                $data['code_number'] = end($code_explode);
                                $sql = "select 1 from documents where code like '%$value%'";
                                $is_exist = dbGetOne($sql);
                                $data['is_exist'] = $is_exist;
                                if ($is_exist){
                                    break;
                                }
                            break;
                            case 1:
                                // $column[] = 'revision';
                                $data['revision'] = (int)$value;
                                $file = $full_path.implode('_',$code_explode).'_R'.$value;
                                $migration_files = glob($file . '*');
                                $ext = '';
                                $temp = '';
                                foreach ($migration_files as $migration_file) {
                                    $temp = end(explode('.',$migration_file));
                                    if ($temp != 'pdf'){
                                        $ext = $temp;
                                    } 
                                }
                                if ($ext==''){
                                    $ext = $temp;
                                }
                                // die($migration_file);
                                $data['document'] = basename($file.'.'.$ext);
                                $data['is_exist_file'] = true;
                                if (empty($migration_files)){
                                    $data['is_exist_file'] = false;
                                    
                                    break;
                                }
                            break;
                            case 2:
                                // $column[] = 'title';
                                $data['title'] = $value;
                            break;
                            case 3:
                                // $column[] = 'unitkerja_id';
                                $data['unitkerja_id'] = (string)$value;
                            break;
                            case 4:
                                // Related workunit
                                $column[] = 'related_workunit';
                                $insert_other['related_workunit'] = $value; 
                            break;
                            case 5:
                                // Business process
                                $column[] = 'prosesbisnis';
                                $this->load->model('Document_prosesbisnis_model');
                                $data['prosesbisnis_id'] = $this->Document_prosesbisnis_model->filter(" where code='$value' ")->getOneFilter('id');
                            break;
                            case 6:
                                // Requirement
                                $column[] = 'iso';
                                $insert_other['iso'] = $value; 
                            break;
                            case 7:
                                //related document
                                $column[] = 'related_document';
                                $insert_other['related_document'] = $value; 
                            break;
                            case 8:
                                $column[] = 'published_at';
                                // die($value);
                                // $data['published_at'] = strtotime((string)$value);
                                $insert_other['published_at'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 9:
                                $column[] = 'validity_date';
                                $insert_other['validity_date'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 10:
                                // $column[] = 'creator_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['creator_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['creator_id'] = (int)$this->User_model->getUserIDfromNopeg($value);

                            break;
                                
                            case 11;
                                // $column[] = 'creator_jabatan_id';
                                $data['creator_jabatan_id'] = (string)$value;
                            break;
                            case 12:
                                // $column[] = 'approver_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['approver_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['approver_id'] = (int)$this->User_model->getUserIDfromNopeg($value);
                            break;
                            case 13:
                                // $column[] = 'approver_jabatan_id';
                                $data['approver_jabatan_id'] = (string)$value;
                            break;
                            case 14:
                                $data['description'] = $value;
                            break;
                        }
                    break;
                    case 5:
                        switch($col){
                            case 0:
                                // $column[] = 'code';
                                $data['code'] = $value;
                                $code_explode = explode('/', $value);
                                $data['code_number'] = end($code_explode);
                                $sql = "select 1 from documents where code like '%$value%'";
                                $is_exist = dbGetOne($sql);
                                $data['is_exist'] = $is_exist;
                                if ($is_exist){
                                    break;
                                }
                            break;
                            case 1:
                                // $column[] = 'revision';
                                $data['revision'] = (int)$value;
                                $file = $full_path.implode('_',$code_explode).'_R'.$value;
                                $migration_files = glob($file . '*');
                                $ext = '';
                                $temp = '';
                                foreach ($migration_files as $migration_file) {
                                    $temp = end(explode('.',$migration_file));
                                    if ($temp != 'pdf'){
                                        $ext = $temp;
                                    } 
                                }
                                if ($ext==''){
                                    $ext = $temp;
                                }

                                $data['document'] = basename($file.'.'.$ext);
                                $data['is_exist_file'] = true;
                                if (empty($migration_files)){
                                    $data['is_exist_file'] = false;
                                    
                                    break;
                                }
                                if ($ext=='xlsx' or $ext=='xls'){
                                    if (!file_exists($path.$file.'.pdf')){
                                        $data['is_exist_file'] = false;
                                    }
                                }
                            break;
                            case 2:
                                // $column[] = 'title';
                                $data['title'] = $value;
                            break;
                            case 3:
                                // $column[] = 'unitkerja_id';
                                $data['unitkerja_id'] = (string)$value;
                            break;
                            case 4:
                                // Related workunit
                                $column[] = 'related_workunit';
                                $insert_other['related_workunit'] = $value; 
                            break;
                            case 5:
                                // Business process
                                $column[] = 'prosesbisnis';
                                $this->load->model('Document_prosesbisnis_model');
                                $data['prosesbisnis_id'] = $this->Document_prosesbisnis_model->filter(" where code='$value' ")->getOneFilter('id');
                            break;
                            case 6:
                                // Requirement
                                $column[] = 'iso';
                                $insert_other['iso'] = $value; 
                            break;
                            case 7:
                                //related document
                                $column[] = 'related_document';
                                $insert_other['related_document'] = $value; 
                            break;
                            case 8:
                                //Retension
                                $column[] = 'retension_period';
                                $data['retension'] = $value; 
                            break;
                            case 9:
                                $column[] = 'published_at';
                                // die($value);
                                // $data['published_at'] = strtotime((string)$value);
                                $insert_other['published_at'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 10:
                                $column[] = 'validity_date';
                                $insert_other['validity_date'] = date('d-m-Y', PHPExcel_Shared_Date::ExcelToPHP($value));
                            break;
                            case 11:
                                // $column[] = 'creator_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['creator_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['creator_id'] = (int)$this->User_model->getUserIDfromNopeg($value);

                            break;
                                
                            case 12;
                                // $column[] = 'creator_jabatan_id';
                                $data['creator_jabatan_id'] = (string)$value;
                            break;
                            case 13:
                                // $column[] = 'approver_id';
                                $value = str_pad($value, 8, '0', STR_PAD_LEFT);
                                // $data['approver_id'] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                $data['approver_id'] = (int)$this->User_model->getUserIDfromNopeg($value);
                            break;
                            case 14:
                                // $column[] = 'approver_jabatan_id';
                                $data['approver_jabatan_id'] = (string)$value;
                            break;
                            case 15:
                                $data['description'] = $value;
                            break;
                        }
                    break;
                }
            }
            
            $data['show'] = 1;
            $data['is_published'] = 1;
            $data['status'] = 'D';
            $data['workflow_id'] = 1;
            $data['workflow_urutan'] = 6;
            $data['hash'] = hash_hmac('sha256',$data['document_name'], $user_id);
            $data['from_migration'] = 1;
            $data['user_id'] = $data['creator_id'];
            $data['user_jabatan_id'] = $data['creator_jabatan_id'];
            if ($ext=='xlsx' or $ext=='xls'){
                $data['is_converted']=1;
                // $data['have_header']=1;
            }

            echo "<td style='border: 1px solid'>";
            echo $data['code'];
            echo "</td>";
            echo "<td style='border: 1px solid'>";
            if ($data['is_exist']){
                echo 'YES';
            } else {
                echo '-';
            }
            echo "</td>";
            echo "<td style='border: 1px solid'>";
            if (!$data['is_exist_file']){
                echo 'YES';
            } else {
                echo '-';
            }
            echo "</td>";
            echo "<td style='border: 1px solid'>";
            // echo "-</td>";
            if ($data['is_exist']==NULL and $data['is_exist_file']){
                unset($data['is_exist_file']);
                unset($data['is_exist']);
                
                if (!dbInsert('Documents', $data)){
                    
                    echo 'YES';
                    
                } else {
                    echo  '-';

                    // Pindahin File
                    $from_folder = "document/temp/".$id;
                    $to_folder = "document/files";
                    $from = $ciConfig['full_upload_dir'] . $from_folder . '/'.$data['document'];
                    $to = $ciConfig['full_upload_dir'] . $to_folder . '/'.$data['document'];
                    // rename($from, $to);
                    copy($from, $to);

                    if ($ext=='xlsx' or $ext=='xls'){
                        $arr_doc = explode('.', $data['document']);
                        unset($arr_doc[count($arr_doc)-1]);
                        $pdf = implode('.', $arr_doc).'.pdf';
                        $pdf_from = $ciConfig['full_upload_dir'] . $from_folder . '/'.$pdf;
                        $pdf_to = $ciConfig['full_upload_dir'] . $to_folder . '/'.$pdf;
                        copy($pdf_from, $pdf_to);
                    }

                    $code = $data['code'];
                    $document_id = $this->Document_model->filter(" where code='$code' ")->getOneBy('id');
                    $this->Document_model->updateColumnDateBy($document_id, 'validity_date', $insert_other['validity_date']);
                    unset($insert_other['validity_date']);
                    $this->Document_model->updateColumnDateBy($document_id, 'published_at', $insert_other['published_at']);
                    unset($insert_other['published_at']);

                    // Update History Document
                    $history = array();
                    $history['user_id'] = $this->user['id'];
                    $history['document_id'] = $document_id;
                    $history['activity'] = "Migrate this document"; 
                    $history['type'] = 'H';
                    dbInsert("document_activities", $history);

                    foreach ($insert_other as $k_key => $v_insert) {
                        switch($k_key){
                            case 'related_workunit':
                                if ($v_insert!=NULL){
                                    if (strtolower($v_insert)=='all'){
                                        $v_insert = 0;
                                    }
                                    $workunits = explode('##', $v_insert);

                                    //INSERT
                                    $this->load->model('Document_unitkerja_model');
                                    $this->Document_unitkerja_model->create($document_id, $workunits);
                                }
                                
                            break;
                            case 'iso':
                                if ($v_insert!=NULL){
                                    $isos = explode('##', $v_insert);
                                    $data_iso = array();
                                    $data_iso['document_id'] = $document_id;
                                    $this->load->model('Document_iso_model');
                                    foreach ($isos as $value) {
                                        $data_iso['iso_id'] = $this->Document_iso_model->filter(" iso_code='$value' ")->getOne('id');
                                        dbInsert("DOCUMENT_ISOS", $data_iso);
                                    }
                                }
                            break;
                            case 'clausuls':
                                $clausuls = explode('##', $v_insert);
                                $this->load->model('Document_types_model');
                                $this->load->model('Document_iso_klausul_model');
                                $need_klausul = $this->Document_types_model->getBy($data['type_id'], 'klausul');
                                if ($need_klausul){
                                    $data_klausul['document_id'] = $document_id;
                                    $klausul = $clausuls;
                                    foreach ($klausul as $val) {
                                        if ($val[strlen($val)-1]!='.'){
                                            $val .='.';
                                        }
                                        $data_klausul['klausul_id'] = $this->Document_iso_klausul_model->filter(" code='$val' ")->getOne('id');
                                        dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
                                    }
                                }
                            break;
                            case 'reviewer':
                                if ($v_insert!=NULL){
                                    $values = explode('##', $v_insert);
                                    $reviewers = array();
                                    foreach ($values as $k_values => $v_values) {
                                        $value = str_pad($v_values, 8, '0', STR_PAD_LEFT);
                                        // $reviewers[] = (int)$this->User_model->filter(" where \"karyawan_id\"='$value' ")->getOneFilter('user_id');
                                        $reviewers[] = (int)$this->User_model->getUserIDfromNopeg($value);
                                    }
                                    $this->load->model('Document_reviewer_model');
                                    $this->Document_reviewer_model->create($document_id,1, $reviewers);
                                }
                                
                            break;
                            case 'related_document':
                                if ($v_insert!=NULL) {
                                    $related_documents = explode('##', $v_insert);
                                    $related_ids = array();
                                    foreach ($related_documents as $k_related => $v_related) {
                                        $related_ids[] = $this->Document_model->filter(" where code='$v_related' ")->getOneBy('id');
                                    }
                                    $this->Document_model->setRelatedDocument($document_id,$related_ids);
                                }
                            break;
                        }
                    }

                    // Tambah enkripsi
                    $encryptions = array();
                    $encryptions['document_id'] = $document_id;
                    $encryptions['encryption'] = hash_hmac('sha256',$data['hash'].$document_id, date("d-m-Y h:i:sa"));

                    $sql = " select 1 from document_hash where document_id=".$encryptions['document_id'];
                    if (dbGetOne($sql)){
                        $document_id = $encryptions['document_id'];
                        unset($encryptions['document_id']);
                        dbUpdate('document_hash', $encryptions, "document_id=$document_id");
                    } else{
                        dbInsert('document_hash', $encryptions); 
                    }
                }
                
            } else {
                echo '-';
            }
            echo "</td>";
            echo '</tr>';
        }
        echo "</table>";
        $this->_deleteFiles($path_source."document/metadata/".$id);
        $this->_deleteFiles($full_path);
    }

}
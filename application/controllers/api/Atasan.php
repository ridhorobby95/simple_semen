<?php
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Atasan extends PrivateApiController {

    protected $title = 'Superior';

    public function index($is_all_data=0) {
        $this->data['title'] = 'Superiors List';
        // tombol
        $buttons = array();
        if ($this->user['role'] == Role::DOCUMENT_CONTROLLER or $this->user['role'] == Role::ADMIN_UNIT){
            $buttons[] = array('label' => ' Import Excel', 'type' => 'secondary', 'icon' => 'file-excel-o', 'click' => 'goExport()');
        }
        $buttons[] = array('label' => 'Add Superior', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;

        // Data
        $this->load->model("Atasan_local_model");
        $this->load->model("User_model");
        $this->load->model("Unitkerja_model");
        $company = $this->input->get("company", true);
        if ($company!=NULL){
            $filter = "where company='$company'";
        } else {
            $filter = "where company='2000'";
        }
        $search = $this->input->get("search", true);
        if ($search!=NULL and $search!=""){
            $filter .= "and k_nopeg='$search'";
        }

        $datas = $this->Atasan_local_model->column("k_nopeg as \"k_nopeg\", k_unitkerja_id as \"k_unitkerja_id\", atasan1_nopeg as \"atasan1_nopeg\", atasan2_nopeg as \"atasan2_nopeg\", atasan1_unitkerja as \"atasan1_unitkerja\", atasan2_unitkerja as \"atasan2_unitkerja\", atasan1_pgs as \"atasan1_pgs\", atasan2_pgs as \"atasan2_pgs\" ")->filter($filter)->order("order by changed_on")->getAll();

        $is_pgs = array("" => 'No', 'X'=>'Yes');
        $this->data['data'] = array();
        foreach ($datas as $key => $data) {
            // user
            $this->data['data'][$key]['k_nopeg'] = $data['k_nopeg'];
            $this->data['data'][$key]['employee_name'] = $this->User_model->filter("where \"karyawan_id\"='".$data['k_nopeg']."' ")->getOneFilter("name");
            $this->data['data'][$key]['unitkerja_name'] = $this->Unitkerja_model->getOne($data['k_unitkerja_id'], 'name');

            // atasan 1
            $this->data['data'][$key]['atasan1_name'] = $this->User_model->filter("where \"karyawan_id\"='".$data['atasan1_nopeg']."' ")->getOneFilter("name");
            $this->data['data'][$key]['atasan1_unitkerja_name'] = $this->Unitkerja_model->getOne($data['atasan1_unitkerja'], 'name');
            $this->data['data'][$key]['atasan1_pgs'] = $is_pgs[$data['atasan1_pgs']];

            // atasan 2
            $this->data['data'][$key]['atasan2_name'] = $this->User_model->filter("where \"karyawan_id\"='".$data['atasan2_nopeg']."' ")->getOneFilter("name");
            $this->data['data'][$key]['atasan2_unitkerja_name'] = $this->Unitkerja_model->getOne($data['atasan2_unitkerja'], 'name');
            $this->data['data'][$key]['atasan2_pgs'] = $is_pgs[$data['atasan2_pgs']];

            $is_sync = $this->model->filter("where k_nopeg='".$data['k_nopeg']."'")->getOne("1");
            if ($is_sync){
                $this->data['data'][$key]['sync'] = "<b style='color:green'>Yes</b>";
            } else {
                $this->data['data'][$key]['sync'] = "<b style='color:red'>Not Yet</b>";
            }
        }
         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
        // $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Fungsi Delete user
     */
    public function delete($id) {
        $this->load->model("Atasan_local_model");
        $delete = $this->Atasan_local_model->delete($id);
        if ($delete){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to delete this data'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to delete this data'));
        }
    }

    /**
     * Fungsi reset password user
     */
    public function reset($id) {
        $ok = false;
        $user = $this->model->get_by('id', $id);
        $update = $this->model->update($id, SecurityManager::encode($user['username']), TRUE);
        if ($update) {
            $ok = true;
            $msg = 'Successfully changed password';
        } else
            $msg = 'Failed to change password';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        
        redirect($this->ctl);
    }

    /**
     * Halaman daftar teman user
     */
    public function list_me() {
        $this->data['title'] = 'Teman'; 
        $this->data['data'] = $this->model->getListFor($this->user['id']);
        // echo '<pre>';
        // var_dump($this->data['data']);
        // die();
        // ambil status hadir
        $this->data['presence'] = $this->model->getListPresence();

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    /**
     * Halaman profil user
     */
    public function me() {
        // tombol
        $buttons = array();
        $buttons['save'] = array('label' => 'Save', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Edit Profile';

        $this->edit($this->user['id']);
    }

    /**
     * Halaman tambah data
     * @param int $userid
     */
    public function add($userid = null) {
        parent::add();
    }

    /**
     * Halaman edit user
     * @param int $id
     */
    public function edit($id = null) {
        $this->data['title'] = $this->data['title'];

        // edit user lain hanya untuk admin
        if ($this->user['role'] != Role::ADMINISTRATOR){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Can\'t access this page!'));
        }
        parent::edit($id);
        if ($id!=NULL){
            
            $this->load->model("Atasan_local_model");
            $data = $this->Atasan_local_model->filter("where k_nopeg='$id'")->getBy();
            $this->data['data'] = array_change_key_case($data, CASE_LOWER);
        }
        
        
        $this->data['is_pgs'] = array(NULL => 'No', 'X'=>'Yes');
        // $this->data['variables'] = SessionManagerWeb::getVariables();

        $this->load->model("Unitkerja_model");
        $this->data['levels'] = $this->Unitkerja_model->getLevel();
        $this->data['levels'][NULL] = NULL;
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success access this page!'), $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        $this->load->model("User_model");
        $this->load->model("Unitkerja_model");

        $data = array();
        // karyawan
        $data['k_nopeg'] = $this->input->post('k_nopeg', true);
        $data['jabatan_id'] = $this->input->post('jabatan_id', true);
        $data['k_unitkerja_id'] = $this->input->post('k_unitkerja_id', true);
        $data['unitkerja_id'] = $data['k_unitkerja_id'];
        $data['company'] = $this->Unitkerja_model->getOne($data['unitkerja_id'], 'company');

        // atasan 1
        $data['atasan1_nopeg'] = $this->input->post('atasan1_nopeg', true);
        $data['atasan1_jabatan'] = $this->input->post('atasan1_jabatan', true);
        $data['atasan1_unitkerja'] = $this->input->post('atasan1_unitkerja', true);
        $data['atasan1_level'] = $this->input->post('atasan1_level', true);
        $data['atasan1_pgs'] = $this->input->post('atasan1_pgs', true);

        // atasan 2
        $data['atasan2_nopeg'] = $this->input->post('atasan2_nopeg', true);
        $data['atasan2_jabat'] = $this->input->post('atasan2_jabatan', true);
        $data['atasan2_unitkerja'] = $this->input->post('atasan2_unitkerja', true);
        $data['atasan2_level'] = $this->input->post('atasan2_level', true);
        $data['atasan2_pgs'] = $this->input->post('atasan2_pgs', true);

        $data['changed_on'] = "to_date('".date('d/m/Y')."', 'dd/mm/yyyy')";
        // $data['changed_on'] = date('d/m/Y');
        // echo '<pre>';
        // var_dump($data);
        // die();
        // $this->load->model("Atasan_local_model");
        // $insert = $this->Atasan_local_model->create($data, false, true);
        $insert = dbInsert("Atasan_local", $data);
        if ($insert) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully added Superior'), $this->data);
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Add Superior'));
        }

    }

    /**
     * Edit data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
    public function update($id = null) {
        $this->load->model("User_model");
        $this->load->model("Unitkerja_model");

        $data = array();
        // karyawan
        $data['k_nopeg'] = $this->input->post('k_nopeg', true);
        $data['jabatan_id'] = $this->input->post('jabatan_id', true);
        $data['k_unitkerja_id'] = $this->input->post('k_unitkerja_id', true);
        $data['unitkerja_id'] = $data['k_unitkerja_id'];
        $data['company'] = $this->Unitkerja_model->getOne($data['unitkerja_id'], 'company');

        // atasan 1
        $data['atasan1_nopeg'] = $this->input->post('atasan1_nopeg', true);
        $data['atasan1_jabatan'] = $this->input->post('atasan1_jabatan', true);
        $data['atasan1_unitkerja'] = $this->input->post('atasan1_unitkerja', true);
        $data['atasan1_level'] = $this->input->post('atasan1_level', true);
        $data['atasan1_pgs'] = $this->input->post('atasan1_pgs', true);

        // atasan 2
        $data['atasan2_nopeg'] = $this->input->post('atasan2_nopeg', true);
        $data['atasan2_jabat'] = $this->input->post('atasan2_jabatan', true);
        $data['atasan2_unitkerja'] = $this->input->post('atasan2_unitkerja', true);
        $data['atasan2_level'] = $this->input->post('atasan2_level', true);
        $data['atasan2_pgs'] = $this->input->post('atasan2_pgs', true);

        $data['changed_on'] = "to_date('".date('d/m/Y')."', 'dd/mm/yyyy')";

        $update = dbUpdate("atasan_local", $data, "k_nopeg='$id'");
        if ($update) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Successfully to change Superior'));
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to change Superior'));
        }
    }

    /**
     * Halaman export
     * @param int $userid
     */
    public function add_import() {
        parent::add_import();
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['user']);  
        $this->template->viewDefault($this->view, $this->data);
    }

    public function uploadMetadata(){
        $this->data['title'] = 'Import Superior';
        $this->metadataform('masters/atasan');
    }

    public function import(){

        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;

        if (is_dir($path_source.'/masters/atasan/'.$id)) {
           $files = glob($path_source.'/masters/atasan/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."/masters/atasan/".$id."/".$metadata;

        if (!file_exists($file)){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to import. Metadata not exist'));
        }

        $this->load->library('Libexcel');

        $tmpfname = $file;
        
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $this->load->model("Karyawan_model");
        $this->load->model("Atasan_local_model");
        $column = array();
        $success = array();
        $failed = array();
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data = array();
            $insert_other = array();
            for ($col = 0 ; $col < $worksheetData[0]['totalColumns']; $col++){
                $colString = PHPExcel_Cell::stringFromColumnIndex($col);
                $value =  $worksheet->getCell($colString.$row)->getValue();
                switch($col){   
                    case 0:
                        $data['k_nopeg'] = "{$value}";
                    break;
                    case 1:
                        $data['jabatan_id'] = "{$value}";
                    break;
                    case 2:
                        $data['k_unitkerja_id'] = "{$value}";
                        $data['unitkerja_id'] = $data['k_unitkerja_id'];
                    break;
                    case 3:
                        $data['company'] = "{$value}";
                    break;
                    case 4:
                        $data['atasan1_nopeg'] = "{$value}";
                    break;
                    case 5:
                        $data['atasan1_jabatan'] = "{$value}";
                    break;
                    case 6:
                        $data['atasan1_unitkerja'] = "{$value}";
                    break;
                    case 7:
                        $data['atasan1_level'] = "{$value}";
                    break;
                    case 8:
                        $data['atasan1_pgs'] = "{$value}";
                    break;
                    case 9:
                        $data['atasan2_nopeg'] = "{$value}";
                    break;
                    case 10:
                        $data['atasan2_jabat'] = "{$value}";
                    break;
                    case 11:
                        $data['atasan2_unitkerja'] = "{$value}";
                    break;
                    case 12:
                        $data['atasan2_level'] = "{$value}";
                    break;
                    case 13:
                        $data['atasan2_pgs'] = "{$value}";
                    break;
                }
            }

            if ($data['k_nopeg']==NULL or $data['k_nopeg']=="")
                break;

            $data['changed_on'] = "to_date('".date('d/m/Y')."', 'dd/mm/yyyy')";

            $is_exist = $this->Atasan_local_model->column('1')->filter("where k_nopeg='".$data['k_nopeg']."'")->getOne();
            if ($is_exist==NULL or $is_exist==false){
                // Check Metadata
                if (dbInsert("Atasan_local", $data)){
                    $success[] = array("id" => $data['k_nopeg'], "reason" => "Successfully added");
                } else {
                    $failed[] = array("id"=>$data['k_nopeg'], "reason"=>"False Metadata");
                }
            } else {
                // update
                if (dbUpdate("Atasan_local", $data, "k_nopeg='".$data['k_nopeg']."'")){
                    $success[] = array("id" => $data['k_nopeg'], "reason" => "Successfully updated");
                } else {
                    $failed[] = array("id"=>$data['k_nopeg'], "reason"=>"False Metadata");
                }
            }

            $id = $data['k_nopeg'];
            $this->sync($id, false);

        }
        $this->data['import']['atasan'] = array(
            "success" => $success,
            "failed" => $failed
        );
        $this->data['success'] = $success;
        $this->data['failed'] = $failed;
        $buttons['download'] = array('label' => 'Download Report', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownload()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;

        $this->_deleteFiles($path_source."masters/atasan/".$id);
        $this->template->viewDefault($this->view, $this->data);
    }

    public function downloadImportReport(){
        $filename = "import_superior_report";  
        $file_ending = "xls";
        //header info for browser
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");    
        header("Content-Disposition: attachment; filename=$filename.xls");  
        header("Pragma: no-cache"); 
        header("Expires: 0");

        /*******Start of Formatting for Excel*******/   
        $sep = "\t"; 

        echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
        echo "<tr></tr>";
        echo "<tr><td colspan='2'><b>Import Jabatan</b></td></tr>";
        echo "<tr><td>Di Import Tanggal</td><td>".date("d M Y")."</td></tr>";
        echo "<tr><td>Di Import Oleh</td><td colspan=3>".$this->user['name']."</td></tr>";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header = "<tr>
                <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Employee Number</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema='';
        // foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['atasan']['failed'] as $fail) {
        //     $schema.= "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;'>
        //             {$fail['id']}
        //         </td>{$sep}
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;'>
        //             {$fail['reason']}
        //         </td>{$sep}
        //     </tr>";
        // }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header ="<tr>
                <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Employee Number</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema = '';
        // foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['atasan']['success'] as $s) {
        //     $schema .= "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;overflow:hidden'>
        //             {$s['id']}
        //         </td>{$sep}
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;overflow:hidden'>
        //             {$s['reason']}
        //         </td>{$sep}
        //     </tr>";
        // }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo '</table>';
        // unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
    }

    public function syncAll(){
        $this->load->model("Atasan_local_model");
        
    }

    public function sync($id, $redirect=true){
        $this->load->model("Atasan_local_model");

        $atasan = $this->Atasan_local_model->column("*")->show_sql(false)->filter("where k_nopeg='$id'")->getBy();
        $atasan = array_change_key_case($atasan, CASE_LOWER);
        $delete = $this->model->delete($id);
        if ($delete){
            unset($atasan['changed_on']);
            if (dbInsert("atasan", $atasan)){
                if ($redirect){
                    $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to sync'));
                }
            }
        } 
        if ($redirect){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to sync'));
        }
        
    }
}

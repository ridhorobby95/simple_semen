<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
/**
 * Controller Thumb
 * @author Sevima
 * @version 1.0
 */
class Thumb extends PrivateApiController {
	protected $ispublic = true;
	
	/**
	 * Watermarking dengan gambar pangkat
	 * @param int $userid
	 * @param int $size
	 */

	public function files_watermark($encryption) {
		$sql = "select document_name from document_watermarked_hash where encryption='$encryption'";
		$name = dbGetOne($sql);

		if ($name=='' || $name==NULL){
			$this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
		}
		else{
			$CI = & get_instance();
			$config = $CI->config->item('utils');
			$folder = "document/files";
			$file = $config['full_upload_dir'] . $folder . '/' . $name;
			$this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $file);
		}
		
	}

	public function files_backup($hash) {
		// if (end(explode('.', $name))=='pdf'){
		// 	$is_authenticated = SessionManagerWeb::isAuthenticated();
		// 	$user_id = SessionManagerWeb::getUserID();
		// }
		// } else {
		// 	$is_authenticated = $_GET['is_authenticated'];
		// 	$user_id = $_GET['user_id'];
		// }

		// if ($name=='' or ! $is_authenticated){
		// 	echo "This directory is restricted.";
		// 	die();
		// }
		$this->load->model('Document_model');
		$name = $this->Document_model->filter(" where hash='$hash' ")->getOneBy('document');
		
		if ($name=='' || $name==NULL){
			echo "This directory is restricted.";
			die();
		}

		if (isset($_SESSION['dms_semen_indonesia'][$name])){
			header('Content-type: '. Image::getMime($name));
			echo $_SESSION['dms_semen_indonesia'][$name];
			// die('test');
			die();
		}

		$CI = & get_instance();
		$config = $CI->config->item('utils');
		$exts = explode('.', $name);
		$filename='';
		foreach ($exts as $key => $value) {
			if ($key<count($exts)-1) {
				$filename.=$value;
			} 
			
		}
		$filename .= '.backup';
		$folder = "document/files";
		// $file = $config['full_upload_dir'] . $folder . '/' . $filename;
		$file = $config['full_upload_dir'] . $folder . '/' . $name;


		// Check file udh dibuka apa belum
		// $url_encrypted = hash_hmac('sha256',$config['full_upload_dir'] . $folder . '/' .$name, $user_id);
		// $sql ="select 1 from 
  //   			document_urls where url_encrypted='$url_encrypted' and is_expired='0' ";
		// if (!dbGetOne($sql)){
		// 	echo "This directory is restricted.";
		// 	die();
		// } else {
		// 	$urls['is_expired']=1;
		// 	dbUpdate('document_urls', $urls, "url_encrypted='$url_encrypted'");
		// }

		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			die('File not exist.');
		header('Content-type: '. Image::getMime(strtolower($name)));
		$image = file_get_contents($file);
		// $image = readfile($file);
		$_SESSION['dms_semen_indonesia'][$name] = $image;
		echo $_SESSION['dms_semen_indonesia'][$name];
		// echo $image;
	}

}
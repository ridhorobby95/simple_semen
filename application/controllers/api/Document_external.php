<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }

//Ada tabel Role ItemRole Item

class Document_external extends PrivateApiController {

    protected $title = 'Document External';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
    	$data = $this->model->show_sql(true)->getAll("x.company_id='0000".$this->user['company']."'");
        if(!isset($data)){
            $message = 'data kosong';
            $this->data = null;
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $message), $this->data);
        }
        else{
            $message = 'Berhasil';
            $this->data = $data;
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, $message), $this->data);
        }
    	
    }

    public function postExternal(){
        $data = $this->postData;
        $data['code'] = 'DOK.E/'.$data['unitkerja_id'].'/'.$this->model->getLastCode($data['unitkerja_id']);
        $data['company_id'] = '0000'.$this->user['company'];
        $data['tanggal'] = date("Y/m/d h:i:s");
        $insert = $this->model->create($data, false, TRUE);
        if($insert){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success to create new external document'), $this->insert);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to create new external document'));
        }
    }

    public function editExternal($id){
        $data = $this->model->getBy($id);
        if($data){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $data);
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
        }
    }

    public function saveExternal($id){
        $data = $this->postData;
        $update = $this->model->save($id, $data, TRUE);
        if($update){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success update data'), $id);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed update data'));
        }
    }

    public function deleteExternal($id){
        $delete = dbQuery("delete from document_external where id=".$id);
        if($delete){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success delete data'));
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed update data'), $id);
        }
    }

    public function exportExternal(){
        // export pdf
    }

}

<?php
header('Access-Control-Allow-Origin: *');
    if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Authorization,Content-Type');
        exit;
    }
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Helpdesk extends PrivateApiController {

    protected $title = 'Helpdesk';


    public function index(){
        $company = $this->user['company'];
        if($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
            $helpdesk = $this->model->show_sql(true)->getAll("where is_delete=0 and l.user_id=".$this->user['id']." order by h.id desc");
        }
        else{
            $helpdesk = $this->model->show_sql(false)->getAll("where company='".$company."' and is_delete=0 order by h.id desc");
        }

        if($helpdesk){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $helpdesk);
        }else{
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed get data"));
        }
        
    }

    public function populerQuest(){
        $company = $this->user['company'];
        $helpdesk = $this->model->show_sql(true)->limitData("where company='".$company."' and is_delete=0 order by h.view_count desc");
        if($helpdesk){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $helpdesk);
        }else{
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed get data"));
        }
    }

    public function pinQuest(){
        $helpdesk = $this->model->show_sql(true)->limitData("where pin=1 and is_delete=0 order by h.id");
        if($helpdesk){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $helpdesk);
        }else{
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed get data"));
        }
    }

    public function myQuestion($limit=false){
        if($limit == true){
            $helpdesk = $this->model->show_sql(true)->limitData("where user_id=".$this->user['id']." and is_delete=0 order by h.id desc");
        }else{
            $helpdesk = $this->model->show_sql(false)->getAll("where user_id=".$this->user['id']." and is_delete=0 order by h.id desc");
        }
        if($helpdesk){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $helpdesk);
        }else{
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed get data"));
        }
    }

    public function postQuestion(){
        // postdata: title, description
        $data = $this->postData;
        $data['user_id'] = $this->user['id'];
        $data['company'] = $this->user['company'];
        $insert = $this->model->create($data, false, true);
        if($insert){
            if (!$this->_moveHelpdeskTemp($insert)) {
                $this->deleteQuestion($insert);
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to Submit Document because the attachments can\'t be upload'));
            }
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success insert data'), $insert);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed insert data"));
        }
    }

    public function postPinQuestion($post_id){
        $count = dbGetCount("select id from helpdesk where pin=1");
        if($count >= 5){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed pin question because question pin more than 5"));
        }else{
            $data['pin'] = 1;
            $update = dbUpdate('helpdesk', $data, "id=".$post_id);
            if($update){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success pin question'), $update);
            }
            else{
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed pin question"));
            }
        }
        
    }

    public function postAnswer($post_id){
        // postdata: answer
        $data = $this->postData;
        $data['answer_user_id'] = $this->user['id'];
        $data['answer_at'] =  date('Y-m-d H:i:s');
        $update = dbUpdate('helpdesk', $data, "id=".$post_id);
        if($update){
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success insert answer'), $post_id);
        }
        else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed get answer"));
        }
    }

    public function deleteQuestion($post_id){
        if($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
            $update = dbUpdate('helpdesk', array('is_delete' => 1), "id=".$post_id);
            if($update){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success delete question'), $post_id);
            }else{
                 $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed delete question"));
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "You Don't have access"));
        }
    }

    public function deleteAnswer($post_id){
        if($this->user['role'] == Role::DOCUMENT_CONTROLLER && $this->user['company'] == '2000'){
            $update = dbUpdate('helpdesk', array('answer' => null, 'answer_user_id' => null, 'answer_at' => null), "id=".$post_id);
            if($update){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success delete question'), $post_id);
            }else{
                 $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed delete question"));
            }
        }else{
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "You Don't have access"));
        }
    }


    public function detailQuestion($post_id){
        $helpdesk = $this->model->show_sql(false)->getAll("where id=".$post_id, true);
        if($helpdesk){
            dbQuery("update helpdesk set view_count = view_count+1 where id=".$post_id);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success insert data'), $helpdesk);
        }else{
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed insert data"));
        }
    }

    // public function likeQuestion($post_id){
    //     $insertLike = dbInsert('helpdesk_likes',array('user_id' => $this->user['id'], 'post_id'));
    //     if($insertLike){
    //         dbQuery("update helpdesk set like_count = like_count+1 where id=".$post_id);
    //         $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success insert data'), $insertLike);
    //     }else{
    //          $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed insert data"));
    //     }
    // }

    public function viewQuestion($post_id){
        $insertViews = dbInsert('helpdesk_views',array('user_id' => $this->user['id'], 'post_id'));
        if($insertViews){
            dbQuery("update helpdesk set view_count = view_count+1 where id=".$post_id);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success insert data'), $insertLike);
        }else{
             $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed insert data"));
        }
    }


    public function editQuestion($post_id){
        $data = dbGetRow("select user_id as \"user_id\", company as \"company\" from helpdesk where id".$post_id);
        $company =  array($data['company'], '2000');
        if(($this->user['role'] == DOCUMENT_CONTROLLER && in_array($this->user['company'], $company)) || $this->user['id'] == $data['user_id']){
            $postData = $this->postData;
            $update = dbUpdate('helpdesk',$postData, "id=".$post_id);
            if($update){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success edit question'), $post_id);
            }else{
                 $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed edit question"));
            }
        }
    }

    public function editAnswer($post_id){
        $data = dbGetRow("select company as \"company\" from helpdesk where id".$post_id);
        $company =  array($data['company'], '2000');
        if($this->user['role'] == DOCUMENT_CONTROLLER && in_array($this->user['company'], $company)){
            $postData = $this->postData;
            $update = dbUpdate('helpdesk',$postData, "id=".$post_id);
            if($update){
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success edit answer'), $post_id);
            }else{
                 $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Failed edit answer"));
            }
        }
    }




}

?>
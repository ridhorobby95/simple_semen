<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends PublicApiController {

    public function index() {
        $status = TRUE;

        $uuid = $this->input->get('file');
        /*$token = $this->input->get('token');
        $validate = AuthManager::validateToken($token);
        if (empty($uuid) || empty($token) || is_string($validate)) {
            $status = File::INVALID_TOKEN;
        } else {*/
            list($secureId, $id, $path) = File::getData($uuid);
            if ($secureId != File::getFileName($id, $path)) {
                $status = File::INVALID_TOKEN;
            } else {
                switch ($path) {
                    case 'posts/files':
                        //list($token, $user) = $validate; 
                        $this->load->model('Post_model');
                        $data = $this->Post_model->get_by('posts.id', $id);
                        if ($data) {
                            $status = File::download($uuid, $data['file']['name']);
                        } else {
                            $status = File::UNAUTHORIZED;
                        }
                        break;
                    default:
                        list($class, $field) = explode('/', $path);
                        $model = ucfirst(singular($class)) . '_model';
                        $this->load->model($model);
                        $data = $this->{$model}->get($id);
                        $filename = $data[singular($field)]['name'];
                        $status = File::download($uuid, $filename);
                        break;
                }
            }
        //}

        switch ($status) {
            case File::INVALID_TOKEN:
                // echo('Invalid URL to access file.');
                echo 'File Tidak Ditemukan';
                break;
            case File::ERROR:
                // echo('Error to access file.');
                echo 'File Tidak Ditemukan';
                break;
            case File::UNAUTHORIZED:
                // echo('Unauthorized user to access this file.');
                echo 'File Tidak Ditemukan';
                break;
            case File::NOT_FOUND:
                echo 'File Tidak Ditemukan';
                break;
            default :
                show_404();
                break;
        }
    }

}

?>
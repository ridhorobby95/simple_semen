<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Quick_link extends WEB_Controller {

    protected $title = 'Custom Quick Link';

    public function index() {
        if (SessionManagerWeb::isAdminUnit() or SessionManagerWeb::isAdministrator()){
            $buttons[] = array('label' => 'Add Menu', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        }
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['quicklink'] = $this->model->show_sql(false)->getAll();
        $this->data['buttons'] = $buttons;
        // echo "<pre>";print_r($this->data['quicklink']);die();
        $this->template->viewDefault($this->view, $this->data);
    }

    public function add() {
        parent::add();
    }

    public function edit($id = null) {

        $this->data['title'] = $this->data['title'];

        parent::edit($id);
        
        if (isset($id)) {
            $data = $this->model->getBy($id);
        }
        else{
            $data = array();
        }

        $this->data['a_icon'] = array(
            'home' => 'home',
            'user' => 'user',
            'dashboard' => 'dashboard',
            'megaphone' => 'megaphone',
            'file' => 'file',
            'list' => 'list',
            'book' => 'book',
            'stats' => 'stats',
        );

        $this->data['data'] = $data;

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        // $data = $this->input->post(null, true);
        $data['name'] = $this->input->post("name", true);
        $data['link'] = $this->input->post("link", true);
        $data['description'] = trim($this->input->post("description", true));
        $data['icon'] = $this->input->post("icon", true);
        
        // echo "<pre>";print_r($data);die();

        $insert = dbInsert('quick_links', $data);
        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Successfully add Menu');
            $this->redirect();
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Failed to add Menu');
            redirect($this->ctl . '/add');
        }

    }

    public function update($id = null) {
        $data['name'] = $this->input->post("name", true);
        $data['link'] = $this->input->post("link", true);
        $data['description'] = trim($this->input->post("description", true));
        $data['icon'] = $this->input->post("icon", true);
        $data['status'] = $this->input->post("status", true);
        
        // echo "<pre>";print_r($data);die();
        $update = dbUpdate('quick_links', $data, "id='$id'");
        if ($update) {
            SessionManagerWeb::setFlashMsg(true, 'Successfully update Menu');
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Failed to update Menu');
        }
        redirect($this->ctl . '/edit/'.$id);
    }

    public function delete($id) {
        if($id){
            $delete = dbQuery("delete from quick_links where id=".$id);
            if($delete){
                SessionManagerWeb::setFlashMsg(true, 'Successfully delete Menu');
            }
            else{
                SessionManagerWeb::setFlashMsg(true, 'Failed to delete Menu');
            }
            redirect($this->ctl);
        }
        else{
            SessionManagerWeb::setFlashMsg(false, 'Failed to delete Menu');
            redirect($this->ctl);
        }
    }

}

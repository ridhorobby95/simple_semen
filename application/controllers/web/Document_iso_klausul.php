<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Document_iso_klausul extends WEB_Controller {

    protected $title = 'Iso Dokumen';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
    	$this->load->model('Document_types_model');
    	$data = $this->Document_types_model->getAllJenis();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    function add($name) {
        $name = urldecode($name);
        $this->model->add($name);
        redirect($this->ctl );        
    }

    public function edit($id=NULL){
    	if (isset($id))
            $data = $this->model->getDocumentType($id);
        else
            $data = array();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function delete($id) {
        $delete = $this->model->delete($id);

        if ($delete === true) {
            $ok = true;
            $msg = 'Berhasil menghapus jenis dokumen';
        } else {
            $ok = false;
            $msg = 'Gagal menghapus jenis dokumen';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl);
    }
    
    public function update($id = null) {
        $data = $this->postData;
        $update = $this->model->save($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Berhasil mengubah Jenis Dokumen';

        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal mengubah Jenis Dokumen';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl );
    }

    public function ajaxGetKlausul(){
        $iso_id = $this->input->post('iso_id',true);
        $iso_id = implode(',',$iso_id);
        // $klausul = Util::toMap($this->model->filter(" iso_id=$iso_id ")->getAll(), 'id', 'klausul');
        $klausul = $this->model->filter(" iso_id in ($iso_id) ")->getAll();
        // echo '<pre>';var_dump($klausul);
        // die();
        echo json_encode($klausul);
    }

    public function getDataExcel($filename){
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        if (is_dir($path_source.'document/master/klausul/'.$id)) {
           $files = glob($path_source.'document/master/klausul/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."document/master/klausul/".$id."/".$metadata;

        if (!file_exists($file)){
            SessionManagerWeb::setFlashMsg(false, 'Failed to migrate document. Metadata not exist');
            redirect('/web/migration');
        }
        
        $this->load->library('Libexcel');
        $data = array();
        $index = 0;
        $tmpfname = $file;
        // echo $tmpfname.'<br>';
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getActiveSheet(0);
        $lastRow = $worksheet->getHighestRow();
        // // echo "<pre>";print_r($worksheet);echo "</pre>";
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data[$index]['klausul'] = $worksheet->getCell('A'.$row)->getValue();
            $data[$index]['code_klausul'] = $worksheet->getCell('B'.$row)->getValue(); 
            // echo $worksheet->getCell('A'.$row)->getValue().'||'.$worksheet->getCell('B'.$row)->getValue()."<br>";
            $index++;
        }
        // echo "<pre>";print_r($data);echo "</pre>";
        // echo $file;
        echo json_encode($data);
    }

    public function uploadKlausul(){
        // echo $this->input->post('filename');
        // die();
        
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        if (is_dir($path_source.'document/master/klausul/'.$id)) {
           $files = glob($path_source.'document/master/klausul/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."document/master/klausul/".$id."/".$metadata;

        if (!file_exists($file)){
            SessionManagerWeb::setFlashMsg(false, 'Failed to migrate document. Metadata not exist');
            redirect('/web/migration');
        }
        
        $this->load->library('Libexcel');
        $data = array();
        $index = 0;
        $tmpfname = $file;
        // echo $tmpfname.'<br>';
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getActiveSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $this->load->model('Document_iso_klausul_model');
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $clause = $worksheet->getCell('A'.$row)->getValue();
            $code = $worksheet->getCell('B'.$row)->getValue();
            $dataExist = $this->Document_iso_klausul_model->filter(" \"code\" = '".$code."' ")->getAll();
            if($dataExist == NULL){
                $data = array(
                        'iso_id'    => $this->input->post('id_iso'),
                        'klausul'   => $clause,
                        'code'      => $code
                );
                dbInsert('document_iso_klausul', $data);
            }
        }

        $this->_deleteFiles($path_source."document/master/klausul/".$id);
        $ok = true;
        $msg = "Successfully Add Iso Clause Document";
        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect('web/document_iso/edit/'.$this->input->post('id_iso'));
        // $clause = $this->input->post('clause');
        // $code = $this->input->post('code');
        // $data = array(
        //     'klausul'   => $clause,
        //     'code'      => $code
        // );
        // $id = $this->input->post('id_clause');
        // $update = $this->klausul->update($id, $data);
        // if ($update === true) {
        //     $ok = true;
        //     $msg = 'Successfully changed Iso Clause Document';
        // }
        // else{
        //     $ok = false;
        //      $msg = 'Failed changed document iso clause';
        // }
        // SessionManagerWeb::setFlashMsg($ok, $msg);
        // redirect($this->ctl.'/formEditClause/'.$id);
        // $this->_deleteFiles($path_source."document/metadata/".$id);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Document extends WEB_Controller {

    protected $title = 'Document';
    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->data['nocover'] = base_url('assets/web/img/cover.jpg');
        $this->data['noimg'] = base_url('assets/web/img/logo.png');
    }

    /**
     * Halaman daftar post public
     * @param int $page
     */
    function edo(){
    	$CI = & get_instance();
        $config = $CI->config->item('utils');
        $full_path = $config['full_upload_dir'].'/';
        $file = 'ini_file_tester.docx';
        $copy = copy($full_path.$file, $full_path.'ini_copy_tester.docx');
        if($copy){
        	echo "berhasil";
        }
        else{
        	echo "gagal";
        }
    }
	function generateRandomString($length = 64) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function checkAkses($idOrganisasi){
		$listOrganisasi = $this->model->listOrganisasi();
    	$isAkses = false;
    	foreach($listOrganisasi as $obj){
    		if(strcmp($obj['id_organisasi'],$idOrganisasi)==0){
    			$isAkses = true;
    			break;
    		}
    	}
    	return $isAkses;
	}
	
	public function createNodes($arr,$parent){
		$val = array();
		$temp = 0;
		$val["st"] = null;
		for($i=0;$i<sizeof($arr);$i++){
			if($arr[$i]['used']==0 && $arr[$i]['parent']==$parent){
				if($temp==0){
					$val["st"] = "";
				}
				$val["st"] = $val["st"]. "<li style='text-overflow:clip;font-size:12px;' val='". $arr[$i]['nama']."' value='". $arr[$i]['id_organisasi'] ."'>" . $arr[$i]['nama'];
				$arr[$i]['used'] = 1;
				$nodes = $this->createNodes($arr,$i);
				if($nodes["st"]!=null){
					$val["st"] = $val["st"]."<ul>".$nodes["st"]."</ul>";
				}
				$val["st"] = $val["st"] . "</li>";
				$arr = $nodes['arr'];
				$temp++;
			}
		}
		$val['arr'] = $arr;
		return $val;
	}

	public function allDocument(){
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = $this->input->get('term');
		$filter = "d.show = 1 and (d.title ilike '%{$term}%')";
		$document = $this->model->getDocument($filter);
		$arr = array();
		foreach($document as $obj){
			$arr[] = array('id'=>$obj['id'],
							'label'=> $obj['title'],
						   'value'=>$obj['title'],
						   );
		}
		echo json_encode($arr);
		exit;
	} 

	public function allUser(){
		parse_str($_SERVER['QUERY_STRING'], $_GET);
		$term = $this->input->get('term');
		$sql = "select * from users where lower(name) like '%$term%'";
		$rows = dbGetRows($sql);
		$arr = array();
		foreach($rows as $obj){
			$arr[] = array('id'=>$obj['id'],
							'label'=> $obj['name'],
						   'value'=>$obj['name'],
						   );
		}
		echo json_encode($arr);
		exit;
	} 

	public function getFilter($filter = NULL){
		if ($this->method!='dashboard'){
			unset($_SESSION['document_search']['show_by']);
		} else if (!isset($_SESSION['document_search']['show_by'])) {
			// $_SESSION['document_search']['show_by'] = 'Need Action';
		}
		if (count($_SESSION['document_search'])==0)
			unset($_SESSION['document_search']);
    	
		$filter=$filter;
		if (isset($_SESSION['document_search'])){
	   		foreach ($_SESSION['document_search'] as $key => $value) {
	   			if ($value==''){
	   				unset($_SESSION['document_search'][$key]);
	   				continue;
	   			}
	   			$val = strtoupper($value);
		   		switch($key){
		   			case 'keyword':
		   				$filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%' or upper(\"unitkerja_name\") like '%$val%') ";
		   				$_SESSION['document_search']['title'] = $value;
		   			break;

		   			case 'user':
		   				$filter .= " and (upper(\"username\") like '%$val%' or upper(\"name\") like '%$val%' or upper(\"creator_username\") like '%$val%' or upper(\"creator_name\") like '%$val%') ";
		   			break;

		   			case 'type':
			   			if ($_SESSION['document_search']['arr_type'][0]=='0'){
		   					$filter_type = '';
		   				} else {
		   					$filter_type = " and \"type_id\" in ($val) ";
		   				}
		   				$filter .= $filter_type;
		   			break;

		   			case 'status':
		   				// if ($_SESSION['document_search']['arr_status'][0]=='0'){
		   				// 	$filter_status = '';
		   				// } else {
		   				// 	echo "<pre>";print_r($_SESSION['document_search']['arr_status']);die();
		   				// 	$filter_status = " and \"workflow_id\" in ($val) ";
		   				// }
		   				// $filter .= $filter_status;
		   			break;
		   			case 'title':
		   				$filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%') ";
		   			break;

		   			case 'show_by':
		   				switch($val){
		   					case 'ME':
		   						$filter .= " and \"user_id\"='".SessionManagerWeb::getUserID()."'";
		   					break;
		   					// case 'ALL':
		   					// 	unset($_SESSION['document_search']['show_by']);
		   					// break;
		   					case 'NEED ACTION':
		   						$filter .= " and (\"user_id\"!='".SessionManagerWeb::getUserID()."' or (\"workflow_urutan\"='1' and \"user_id\"='".SessionManagerWeb::getUserID()."') )";
		   					break;
		   				}
		   			break;

		   			case 'unitkerja':
		   				if ($_SESSION['document_search']['arr_unitkerja'][0]=='0'){
		   					$filter_unitkerja = '';
		   				} else {
		   					$filter_unitkerja = " and \"unitkerja_id\" in ($val) ";
		   				}
		   				$filter .= $filter_unitkerja;
		   			break;

		   			case 'datestart':
		   				$filter .= " and (upper(\"updated_at\") >= '$val') ";
		   			break;

		   			case 'dateend':
		   				$filter .= " and (upper(\"updated_at\") <= '$val%') ";
		   			break;
		   			case 'obsolete':
		   				// $filter .= " and de.status='C' ";

		   			break;

		   			// case 'related':
		   			// 	$filter .= " and \"unitkerja_id\" not in ($val) ";
		   			// break;
		   		}
		   	}
	   	}
	   	// echo '<prE>';
	   	// vaR_dump($filter);
	   	// die();
	   	return $filter;
	}

	public function changeCompany($company){
		SessionManagerWeb::setSelectedCompany($company);
		redirect("web/document/dashboard");
	}

    public function index() {
    	
    	$ciConfig = $this->config->item('utils');
	   	$this->load->model('Tag_model');
	   	$this->load->model('Document_shares_model');
	   	$this->load->model('user_model');
	   	$this->load->model('Workflow_model');
	   	$this->load->model('Document_download_model');
	   	$this->load->model('Unitkerja_model');

	 	if ($_GET){
	 		unset($_SESSION['page']);
	 		unset($_SESSION['document_search']);
	 		if ($_GET['keyword']){
	 			$_SESSION['document_search']['keyword'] = $_GET['keyword'];
	 		}
	 		$_SESSION['selected_menu'] = $_GET['selected_menu'];
	 		if($_GET['selected_menu'] == 'type_1' || $_GET['selected_menu'] == 'type_2' || $_GET['selected_menu'] == 'type_3' || $_GET['selected_menu'] == 'type_4' || $_GET['selected_menu'] == 'type_5'){
	 			$_SESSION['document_search']['arr_type'][0] = substr($_GET['selected_menu'], -1);
		   		$_SESSION['document_search']['type'] = substr($_GET['selected_menu'], -1);
	 		}
	 		else{
	 			unset($_SESSION['document_search']);
	 		}
	 		// die('masuk');
	 		unset($_GET);
	 		$this->redirect();
	 	} 
	 	if (!isset($_SESSION['page'])){
	 		$_SESSION['page'] = 1;
	 	}
	 	// $header = "All Documents";
	   	// $filter = " where \"show\"='1' and \"status\"='D' ";
	   	$company = ltrim(SessionManagerWeb::getCompany(), '0');
	   	$where = '';
	   	if(SessionManagerWeb::getRole() == Role::EKSTERNAL){
	   		$where = " and \"type_id\" in('";
            $where .= implode('\',\'', SessionManagerWeb::getDocumentTypeAccess());
            $where .= "')";
        }
	   	$filter = " where \"is_published\"=1 and (\"company\"='$company' or nvl(\"company\",0)='0' ) $where ";
	 	$filter = $this->getFilter($filter);
	 	$column = 'd.*, rownum r';
	 	$table = '';
	 	$on = '';
	 	$filter_obsolete = " and \"show\"='1' ";
	 	if ($_SESSION['selected_menu']=='obsolete'){
	 		// $column = 'd.*';
	 		// $table = 'document_evaluation de';
	 		// $on =" de.document_id=d.\"document_id\" ";
	 		$filter_obsolete = " and \"is_obsolete\"='1' ";
	 	}
	 	$filter .= $filter_obsolete;
	 	$limit = 10;
	 	$this->data['variables'] = $this->getVariables();
	 	// $this->data['variables'] = $this->getVariables();
	 	// echo "<pre>";print_r($_SESSION['document_search']);die();
		$doc = $this->model->column($column)->with($table, $on)->filter($filter)->limit($limit)->getDocument();

		// echo "<pre>";print_r($doc);die();
		$document = array();

    	$path_source = $ciConfig['full_upload_dir'];
    	// $document_shares_ids = $this->Document_shares_model->getSharedDocument();
    	$total_doc = 0;
    	$numdoc = $_SESSION['page'];
    	$min = ($numdoc-1)*$limit;
    	$max = $min+$limit;
    	$this->data['min_data'] = $min+1;
    	$this->data['max_data'] = $max;
    	$path = $ciConfig['full_upload_dir'].'document/files/';

    	
    	// echo "<pre>";print_r($this->data['variables']);die();
    	$allTypes = array();
    	foreach ($this->data['variables']['document_types'] as $types) {
    		$allTypes[] = $types['id'];
    	}
    	$unitkerja = $this->User_model->show_sql()->getBy(SessionManagerWeb::getUserID(),'unitkerja_id'); // unitkerja dari user
		foreach ($doc as $key => $value) {
			// $total_doc++;
			// if ($key<$min or $key>=$max)
			// 	continue;

			$numdoc++;

			$user = $value['user_id'];
			$hash = hash_hmac('sha256',$value['filename'], $user);
			$id = md5($user . $this->config->item('encryption_key'));
			$ext = end(explode('.',$value['filename']));
			$ukuran = 0;
			if ($ext=='docx' || $ext=='doc' || $ext=='pptx' || $ext=='ppt' || $ext=='xlsx' || $ext=='xls' || $ext=='pdf' || 1==1) {
				// $file = File::getFile($hash,$value['filename'],'document/files');
				// $path = 
				
				$ukuran = $this->calculateSize(filesize($path.$value['filename']));
				// if (!empty($file) and $ukuran!='0 Bytes') {
					$document[$key] = $doc[$key];
					$document[$key]['file'] = $file;
					$document[$key]['ukuran'] =$ukuran;
					$published_dates = explode(' ',$doc[$key]['published_at']);
					$published_date = $published_dates[0];
					$document[$key]['published_date'] = $published_date;
					$document[$key]['have_header'] = dbGetOne('select have_header from documents where id='.$document[$key]['document_id']);
				// }
			} 
			switch($value['workflow_id']){
				case Workflow_model::EVALUASI:
					$this->load->model('Document_evaluation_model');
					$document_id = $value['document_id'];
					$hasil_evaluasi = $this->Document_evaluation_model->filter(" where document_id=$document_id ")->getOne('status');
					$document[$key]['hasil_evaluasi'] = $hasil_evaluasi;
				break;
			}
			$is_allow_download = false;
			if ($this->Document_download_model->filter("where user_id=".SessionManagerWeb::getUserID()." and document_id=".$value['document_id'])->getOne('is_allowed')==1){
				$is_allow_download = true;
			}
			$document[$key]['is_allow_download'] = $is_allow_download;
			if (SessionManagerWeb::isDocumentController() || SessionManagerWeb::isAdministrator()){
				$document[$key]['reconvert']		 = 1;
			}
			else{
				$document[$key]['reconvert']		 = 0;
			}

			//// CEK ACCESS EVAL /////
			$document[$key]['access_eval'] = 0;
			// 1. Cek akses tipe dokumen
			$access_type = in_array($value['type_id'], $allTypes);
			if($access_type && $document[$key]['creator_id'] != SessionManagerWeb::getUserID()){
				//2. Cek Unitkerja
				$myunitkerja = $this->data['variables']['myunitkerja'];
				$access_unitkerja = array_key_exists($value['unitkerja_id'], $myunitkerja);
				if($access_unitkerja){
					$getAtasan = $this->Unitkerja_model->getAtasan($unitkerja, SessionManagerWeb::getUserID());
					$access_eval = in_array($value['creator_id'], $getAtasan);
					if($access_eval){
						$document[$key]['access_eval'] = 1;
					}
					
					// echo "<pre>";print_r($getAtasan);die();
				}
			}

			// $document[$key]['publish_date'] = date_format(date_create($document[$key]['published_at']), 'd-M-Y');
			// echo "<pre>";
			// var_dump(strtotime($document[$key]['published_at']));
			// die();
			// $document[$key]['publish_date'] = strftime('%d %b %Y', strtotime($document[$key]['published_at']));

			
			
		}
		$this->data['numDoc']=$numdoc;
		// $this->data['total_document']=$total_doc;
		$this->data['total_document']=$_SESSION['total_doc'];
		unset($_SESSION['total_doc']);
		if ($this->data['total_document']<$this->data['max_data']){
			// $this->data['max_data']=$total_doc;
			$this->data['max_data']=$this->data['total_document'];
		}
		$this->data['type'] = $this->title;
		$this->data['title'] = $this->title;

		$selectedCompany = SessionManagerWeb::getCompany();
		$this->data['unitkerja'] =$this->Unitkerja_model->order('name')->getAllList(" company='$selectedCompany' ");
		// die($selectedCompany);
		// jangan dihapus
		// $this->load->model('Document_tags_model');
		// $tags = $this->Document_tags_model->getTags();
		// $this->data['tags'] = $tags;
		$this->data['documents'] = $document;
		
		// echo '<pre>';
		// vaR_dump($this->data['documents']);
		// die();
		// echo "<pre>";print_r($this->data);die();
		// echo "<pre>";print_r($_SESSION['document_search']);die();
		$this->template->viewDefault($this->view, $this->data);
    }

    public function removeDocumentFilter($method=NULL){
    	unset($_SESSION['document_search']);
    	redirect("web/document/".$method);
    }

     public function removeDocumentFilterListAll($method=NULL){
    	unset($_SESSION['document_search_listall']);
    	redirect("web/document/".$method);
    }

    public function removeFilter($method, $id=NULL){
    	switch ($method) {
    		case 'type':
	    		if (count($_SESSION['document_search']['arr_type'])==1){
	    			unset($_SESSION['document_search']['type']);
	    			unset($_SESSION['document_search']['arr_type']);
	    		} else {
	    			foreach ($_SESSION['document_search']['arr_type'] as $key => $value) {
	    				if ($value==$id){
	    					unset($_SESSION['document_search']['arr_type'][$key]);
	    				}
	    			}
	    			$_SESSION['document_search']['type'] = implode(',', $_SESSION['document_search']['arr_type']);
	    		}
    		break;
    		case 'title':
    			unset($_SESSION['document_search']['keyword']);
	    		unset($_SESSION['document_search'][$method]);
    		break;

    		case 'status':
    			if (count($_SESSION['document_search']['arr_status'])==1){
	    			unset($_SESSION['document_search']['status']);
	    			unset($_SESSION['document_search']['arr_status']);
	    		} else {
	    			foreach ($_SESSION['document_search']['arr_status'] as $key => $value) {
	    				if ($value==$id){
	    					unset($_SESSION['document_search']['arr_status'][$key]);
	    				}
	    			}
	    			$_SESSION['document_search']['status'] = implode(',', $_SESSION['document_search']['arr_status']);
	    		}
    		break;

    		case 'workunit':
	    		if (count($_SESSION['document_search']['arr_unitkerja'])==1){
	    			unset($_SESSION['document_search']['unitkerja']);
	    			unset($_SESSION['document_search']['arr_unitkerja']);
	    		} else {
	    			foreach ($_SESSION['document_search']['arr_unitkerja'] as $key => $value) {
	    				if ($value==$id){
	    					unset($_SESSION['document_search']['arr_unitkerja'][$key]);
	    				} else {
	    					$_SESSION['document_search']['arr_unitkerja'][$key] = "'".$value."'";
	    				}
	    			}
	    			$_SESSION['document_search']['unitkerja'] = implode(',', $_SESSION['document_search']['arr_unitkerja']);
	    		}
    		break;
    		case 'datestart':
    			unset($_SESSION['document_search']['datestart']);
    		break;
    		case 'dateend':
    			unset($_SESSION['document_search']['dateend']);
    		break;

    	}
    	redirect("web/document/index");
    }

    public function removeFilterListAll($method, $id=NULL){
    	switch ($method) {
    		case 'type':
	    		if (count($_SESSION['document_search_listall']['arr_type'])==1){
	    			unset($_SESSION['document_search_listall']['type']);
	    			unset($_SESSION['document_search_listall']['arr_type']);
	    		} else {
	    			foreach ($_SESSION['document_search_listall']['arr_type'] as $key => $value) {
	    				if ($value==$id){
	    					unset($_SESSION['document_search_listall']['arr_type'][$key]);
	    				}
	    			}
	    			$_SESSION['document_search_listall']['type'] = implode(',', $_SESSION['document_search']['arr_type']);
	    		}
    		break;
    		case 'title':
    			unset($_SESSION['document_search_listall']['keyword']);
	    		unset($_SESSION['document_search_listall'][$method]);
    		break;
    		case 'status':
    			if (count($_SESSION['document_search_listall']['arr_status'])==1){
	    			unset($_SESSION['document_search_listall']['status']);
	    			unset($_SESSION['document_search_listall']['arr_status']);
	    		} else {
	    			foreach ($_SESSION['document_search_listall']['arr_status'] as $key => $value) {
	    				if ($value==$id){
	    					unset($_SESSION['document_search_listall']['arr_status'][$key]);
	    				}
	    			}
	    			$_SESSION['document_search_listall']['status'] = implode(',', $_SESSION['document_search_listall']['arr_status']);
	    		}
    		break;
    		case 'workunit':
	    		if (count($_SESSION['document_search_listall']['arr_unitkerja'])==1){
	    			unset($_SESSION['document_search_listall']['unitkerja']);
	    			unset($_SESSION['document_search_listall']['arr_unitkerja']);
	    		} else {
	    			foreach ($_SESSION['document_search_listall']['arr_unitkerja'] as $key => $value) {
	    				if ($value==$id){
	    					unset($_SESSION['document_search_listall']['arr_unitkerja'][$key]);
	    				} else {
	    					$_SESSION['document_search_listall']['arr_unitkerja'][$key] = "'".$value."'";
	    				}
	    			}
	    			$_SESSION['document_search_listall']['unitkerja'] = implode(',', $_SESSION['document_search']['arr_unitkerja']);
	    		}
    		break;
    		case 'datestart':
    			unset($_SESSION['document_search_listall']['datestart']);
    		break;
    		case 'dateend':
    			unset($_SESSION['document_search_listall']['dateend']);
    		break;

    	}
    	redirect("web/document/list_all");
    }

    public function calculateSize($size=0){
    	$size = (int) $size;
    	if ($size<1000) {
    		$size_str = $size.' Bytes';
    	} else if (round($size/1000)<=1000) {
    		$size_str = round($size/1000) . ' KB';
    	} else if (round($size/1000000)<=1000) {
    		$size_str = round($size/1000000,2) . ' MB';
    	} else {
    		$size_str = round($size/1000000000,2) . ' GB';
    	} 
    	return $size_str;
    }

    public function prepareSearch(){
    	$_SESSION['document_search']['keyword'] = $_GET['search'];
    	redirect("web/document");
    }

    function search($keyword){
    	$listOrganisasi = $this->model->listOrganisasi();
		$temp=0;
		$updated = array();
		$document = array();
		foreach($listOrganisasi as $org){
			$res=$this->model->getDocumentOrganisasi($org['id_organisasi']);
			if($res){
				foreach($res as $obj){
					$document[$temp] = $obj;	
					$temp++;
				}
			}
		}
		$search = array();
		$temp = 0;
		foreach($document as $obj){
			$title = strtolower($obj['title']);
			$pos = stripos($title, $keyword);
			if($pos === false){
				$tags = $obj['tags'];
				foreach($obj['tags'] as $tags){
					$pos = strpos(strtolower($tags),$keyword);
					if($pos !== false){
						$search[$temp] = $obj;
						$updated[$temp] = $obj['updated_at'];
						$temp++;	
					}
				}
			}else{
				$search[$temp] = $obj;
				$updated[$temp] = $obj['updated_at'];
				$temp++;
			}
		}
		array_multisort($updated, SORT_DESC, $search);
    	return $search;
    }

    public function searchAdvance(){
    	$this->load->model('user_model');
    	$company = SessionManagerWeb::getCompany();
		// $filter = " where \"show\"='1' and \"status\"='D' ";
		$filter = " where \"show\"='1' and \"is_published\"=1 and \"company\" = '$company'";
		
		$keywordSearch = strtoupper($this->input->post('title',true));
		if ($keywordSearch!='')
			$filter .= " and (upper(\"title\") like '%$keywordSearch%' or upper(\"code\") like '%$keywordSearch%' or upper(\"unitkerja_name\") like '%$keywordSearch%') ";
		$typeSearch = $this->input->post('type', true);
		if ($typeSearch!='')
			$filter .= " and \"type_id\"=$typeSearch ";


		$search = $this->model->filter($filter)->getDocumentOld();

		array_multisort($updated, SORT_DESC, $search);
		echo json_encode($search);
    }

    public function searchAdvance2(){
    	$this->load->model('user_model');

    	$titleSearch = strtolower($this->input->post('title',true));
    	if(strcmp($titleSearch,"_miss_")==0)
    		$titleSearch = "";
    	
    	$pemilik = strtolower($this->input->post('pemilik',true));
    	$document_id = $this->input->post('id');
    	if(strcmp($pemilik,"a")==0)
    		$idPemilik = "0";
    	else
    		$idPemilik = $this->user_model->getUserID($this->input->post('pemilik',true));
    	// $klasifikasi = $this->input->post('klasifikasi',true);
    	// if(strcmp($klasifikasi,"a")!=0)
    	// 	$idKlasifikasi = $this->model->getNamaKlasifikasi($klasifikasi);
    	// else
    		$idKlasifikasi = 0;
    	// $lokasi = $this->input->post('lokasi',true);
    	// if(strcmp($lokasi,"a")!=0)
    	// 	$idLokasi = $this->model->getKodeLokasi($lokasi);
    	// else
    		$idLokasi = 0;
    	// if(strcmp($this->input->post('tags',true),"null")!=0)
    	// 	$key = json_decode($this->input->post('tags',true));
    	// else
    		$key = array();

    	// if(strcmp($this->input->post('idNow',true),"null")!=0)
    	// 	$idDoc = $this->input->post('idNow',true);
    	// else
    		$idDoc = 0;

		// $listOrganisasi = $this->model->listOrganisasi();
		$temp=0;
		$updated = array();
		// $document = array();
		// foreach($listOrganisasi as $org){
		// 	$res=$this->model->getDocumentOrganisasi($org['id_organisasi']);
		// 	if($res){
		// 		foreach($res as $obj){
		// 			$document[$temp] = $obj;	
		// 			$temp++;
		// 		}
		// 	}
		// }

		$relatedDocument = $this->model->getRelatedDocument($document_id);
    	foreach($relatedDocument as $obj){
    		if($obj['document1'] == $document_id)
    			$related_ids[] = $obj['document2'];
    		else
    			$related_ids[] = $obj['document1'];
    	}
    	$related_ids[] = $document_id;
    	$str_related_ids = implode(',', $related_ids);
		$document = $this->model->getDocument("d.show=1 and d.id not in (".$str_related_ids.')');

		$search = array();
		$temp = 0;
		$isResult = 0;
		$size = sizeof($key);
		foreach($document as $obj){
			$isResult = 0;
			$isSame = 0;
			$title = strtolower($obj['title']);
			$pos = stripos($title, $titleSearch);
			if($obj['document_id'] != $idDoc)
				$isSame++;
			if(strcmp($titleSearch,"")==0 || $pos !== false)
				$isSame++;
			if(strcmp($idPemilik,"0")==0 || strcmp($obj['user_id'],$idPemilik)==0)
				$isSame++;
			if($idLokasi==0 || $obj['lokasi_id']==$idLokasi)
				$isSame++;
			if($idKlasifikasi == 0 || $obj['klasifikasi_id']==$idKlasifikasi)				
				$isSame++;
			
			$tags = $obj['tags'];
			foreach($obj['tags'] as $tags){
				foreach($key as $keyword){
					if(strcmp($keyword,$tags)==0){
						$isResult++;
						break;
					}
				}
			}
			if($isResult == $size){
				$isSame++;
			}
			if($isSame == 6){

				$search[$temp] = $obj;
				$updated[$temp] = $obj['updated_at'];
				$temp++;	
			}
		}
		array_multisort($updated, SORT_DESC, $search);
		echo json_encode($search);
    }

    public function docOrganisasi(){
    	$this->load->model('user_model');
    	$idOrganisasi = $this->input->post('idOrganisasi',true);
    	$isAkses = $this->checkAkses($idOrganisasi);
    	if($isAkses){
    		$document = $this->model->getDocumentOrganisasi($idOrganisasi);
	    	$temp = 0;
	    	if($document){
		    	foreach($document as $obj){
		    		$document[$temp]['updated_at'] = strftime('%d %b %Y %H:%M:%S', strtotime($obj['updated_at']));
		    		$temp ++;
		    	}
		    	echo json_encode($document);	
		    }else{
				$document[0]['namafile'] = "kosong";
				echo json_encode($document);
		    }
    	}else{
    		$document[0]['namafile'] = "false";
    		echo json_encode($document);
    	}
    }

    public function getPrevVersion(){
    	$id = $this->input->post('idDokumen',true);
    	$document = $this->model->getPrevDocument($id);
    	if(!isset($document[0]))
    		$document[0]['namafile'] = "kosong";
    	else{
    		$temp = 0;
    		foreach($document as $obj){
    			$document[$temp]['updated'] = strftime('%d %b %Y %H:%M:%S', strtotime($obj['updated_at'])) . " oleh " . $obj['name_updated'];
    			$temp++;
    		}
    	}
    	echo json_encode($document);
    }

    public function ajaxGetLastEvaluation(){
    	$this->load->model('Document_evaluation_model');
    	$this->load->model('Workflow_model');
    	$this->load->model('User_model');
    	$id = $this->input->post('idDokumen',true);
    	
		$evaluation = $this->Document_evaluation_model->filter("where document_id=".$id." and status is not null")->getBy();
		if ($evaluation!=NULL){
			$evaluation_text_counter = 0;
			$evaluation['text'] = json_decode($evaluation['text'], true);
			$evaluation['forms']=$this->Workflow_model->getEvaluationForm();
			$evaluation['creator_name'] = $this->User_model->filter("where \"user_id\"='".$evaluation['creator_id']."' ")->getOneFilter('name');
			$evaluation['drafter_name'] = $this->User_model->filter("where \"user_id\"='".$evaluation['drafter_id']."' ")->getOneFilter('name');
			if ($evaluation['drafter_name']==NULL){
				$evaluation['drafter_name'] = $evaluation['creator_name'];
			}
			$evaluation['approver_name'] = $this->User_model->filter("where \"user_id\"='".$evaluation['approver_id']."' ")->getOneFilter('name');
			$evaluation['creator_date'] = strftime('%d %b %Y %H:%M:%S',strtotime($evaluation['creator_date']));
			// $evaluation['creator_date'] = $evaluation['creator_date'];
			$evaluation['approver_date'] = strftime('%d %b %Y %H:%M:%S',strtotime($evaluation['approver_date']));
			// $evaluation['approver_date'] = $evaluation['approver_date'];

		}
		
		echo json_encode($evaluation);
    }

    public function getRelatedDocument(){
    	$id = $this->input->post('id_dokumen',true);
    	// $id = 5532;
    	$relatedDocument = $this->model->getRelatedDocument($id);
    	$temp = 0;
    	foreach($relatedDocument as $obj){
    		if($obj['document1'] == $id){
    			$relatedDocument[$temp]['document'] = $this->model->getDocument('d.id='.$obj['document2']);
    			// echo 'select type from document_type where id='.$obj['TYPE_ID'];die();
    			$relatedDocument[$temp]['TYPE_NAME'] = dbGetOne('select type from document_types where id='.$obj['TYPE_ID']);
    			$relatedDocument[$temp]['WORKUNIT'] = dbGetOne("select name from unitkerja where id='".$obj['UNITKERJA_ID']."'");
    		}
    		else{
    			$relatedDocument[$temp]['document'] = $this->model->getDocument('d.id='.$obj['document1']);
    			// echo 'select name from unitkerja where id='.$obj['UNITKERJA_ID'];die();
    			$relatedDocument[$temp]['TYPE_NAME'] = dbGetOne('select type from document_types where id='.$obj['TYPE_ID']);
    			$relatedDocument[$temp]['WORKUNIT'] = dbGetOne("select name from unitkerja where id='".$obj['UNITKERJA_ID']."'");
    		}
    		$temp++;
    	}
    	// echo "<pre>";print_r($relatedDocument);die();
    	echo json_encode($relatedDocument);
    }	

    public function getChoosenDocument(){

    	$id = json_decode($this->input->post('listid',true));
    	// echo "<pre>";print_r($listID);die();
    	// $doc = array();
    	$doc = $this->model->getRelatedDocument($id);
    	// $temp = 0;
    	// foreach($listID as $id){
    	// 	$doc[$temp] = $this->model->getDocument("d.id=".$id);
    	// 	$temp++;
    	// }
    	for ($i=0; $i < count($doc) ; $i++) {
    		// echo "select name from unitkerja where id='".$doc[$i]['UNITKERJA_ID']."'";
    		$doc[$i]['TYPE_NAME'] = dbGetOne("select type from document_types where id=".$doc[$i]['TYPE_ID']);
    		$doc[$i]['UNITKERJA'] = dbGetOne("select name from unitkerja where id='".$doc[$i]['UNITKERJA_ID']."'");
    	}
    	// echo "<pre>";print_r($doc);die();
    	echo json_encode($doc);
    }

    public function rollbackDocument(){
    	$id = $this->input->post('idDokumen',true);
    	$asalID = $this->input->post('asalid',true);
    	$this->model->rollback($id,$asalID);
    	$this->load->model('Document_activity_model');

		$dataAktifitas["user_id"] = SessionManagerWeb::getUserID();
		$dataAktifitas["document_id"] = $id;
		$dataAktifitas["activity"] = "Dokumen dikembalikan ke versi sebelumnya";
		$dataAktifitas["type"] = Document_activity_model::HISTORY;
		$this->Document_activity_model->create($dataAktifitas,TRUE,TRUE);
    }

    public function doc(){
		$idOrg = $this->input->post('id',true);
		$isAkses = $this->checkAkses($idOrg);
    	if($isAkses){
    		$document = $this->model->getDocumentOrganisasi($idOrg);
    		$listOrganisasi = $this->model->listOrganisasi();
			$tree = $this->createNodes($listOrganisasi,-1);
			$this->data['tree'] = $tree["st"];
			$this->data['documents'] = $document;
			$this->data['isShowTree'] = true;
			$this->data['isDoc'] = true;
			$this->template->viewDefault('document_index',$this->data);
    	}else{
    		redirect("web/document/");
		}
	}
	
	public function viewdoc_backup($id){
		$is_published = '0';
		if ($_GET['is_published']){
			$is_published = '1';
		}

		// $document = $this->model->getDocument("d.id=$id", $is_published);
		$document = $this->model->filter("where d.\"document_id\"=$id")->getDocument();
		$file = $this->model->getDocumentWithLink($id);
		$res['document'] = $document;
		$res['file'] = $file;

		// INSERT DOCUMENT DYNAMIC URL
		// $arr_filename = explode('.',$file['link']);
		// $arr_fileexts = explode('.',$file['nama_file']);
		// $url_filename="";
		// foreach ($arr_filename as $key => $value) {
		// 	if ($key==count($arr_filename)-1)
		// 		continue;
		// 	$url_filename .= $value;
		// }
		// $ext = end($arr_fileexts);

		// $CI = & get_instance();
        
  //       // Manage Url
		// $urls['URL'] = $url_filename.'.'.$ext;
		// $url_encrypted = hash_hmac('sha256',$urls['URL'], SessionManagerWeb::getUserID());
		// $sql ="select 1 from 
  //   			document_urls where url_encrypted='$url_encrypted' and is_expired='1'";
  //   	if (dbGetOne($sql)) {
  //   		$url['is_expired'] = 0;
  //   		// dbQuery("delete from document_urls where url_encrypted='$url_encrypted' ");
  //   		dbUpdate('document_urls', $url, "url_encrypted='$url_encrypted'");
  //   	} else {
  //   		$urls['URL_ENCRYPTED'] = $url_encrypted;
		// 	$urls['IS_EXPIRED'] = 0;
		// 	dbInsert('document_urls',$urls);
  //   	}
		echo json_encode($res);
	}

	public function view_real_docx($id){

		$document = $this->model->filter("where d.\"document_id\"=$id")->getDocument();
		$file = $this->model->getDocumentWithLink($id);
		$res['document'] = $document;
		$res['file'] = $file;
		// $res['file']['link'] = $res['file']['nama_file'];
		echo json_encode($res);
	}

	public function viewdoc($id, $is_echo=1){
		$is_published = '0';
		if ($_GET['is_published']){
			$is_published = '1';
		}

		// $document = $this->model->getDocument("d.id=$id", $is_published);
		$document = $this->model->filter("where d.\"document_id\"=$id")->getDocument();
		$file = $this->model->getDocumentWithLink($id);
		$res['document'] = $document;
		$res['file'] = $file;
		$res['username'] = SessionManagerWeb::getUserName();
		$res['date'] = date("d-m-Y");
		$res['ip'] = $_SERVER['REMOTE_ADDR'];
		$have_header = $this->model->getOne($id, "have_header");

		if($have_header == 1){
			if(!file_exists($res['file']['link_watermark'])){
				$unitkerja_id = $this->model->getOne($id, "unitkerja_id");
				$this->load->model('Unitkerja_model');
				$idCompany = $this->Unitkerja_model->getOneBy($unitkerja_id, "\"COMPANY\"");
				// $idCompany =    $this->model->get
				// $idCompany 	= SessionManagerWeb::getCompany();
		    	$dataCompany =  array(
									'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
									'3000'	=> 'PT. SEMEN PADANG',
									'4000'	=> 'PT. SEMEN TONASA',
									'5000'	=> 'PT. SEMEN GRESIK',
									'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
								);
		    	
		    	$username 	= SessionManagerWeb::getUserName();
    			$userid 	= SessionManagerWeb::getUserID();
    			$explodeFile= explode('.', $res['file']['nama_file']);
    			$extfile   	= end($explodeFile);
    			// echo $extfile;
    			unset($explodeFile[count($explodeFile)-1]);
    			$nama_file = implode('.', $explodeFile).'.pdf';
    			$this->load->model('Unitkerja_model');
				// $photoCompany = $this->Unitkerja_model->getOneBy('0000'.$idCompany, "\"PHOTO\"");
				$hasil = $this->PlaceWatermark($nama_file, $idCompany , $username, $userid, 'pdf' );
			}
			$watermarked_hash = array();
			$watermarked_hash['user_id'] = SessionManagerWeb::getUserID();
			$watermarked_hash['document_name'] = $res['file']['watermark'];
			$watermarked_hash['encryption'] = hash_hmac('sha256',$watermarked_hash['document_name'].$watermarked_hash['user_id'], date("d-m-Y h:i:sa")); 
			if ($this->model->isExistWatermarked($watermarked_hash['document_name'], $watermarked_hash['user_id'])){
				dbUpdate('document_watermarked_hash', $watermarked_hash, "document_name='".$watermarked_hash['document_name']."' and user_id=".$watermarked_hash['user_id']);
			} else {
				dbInsert('document_watermarked_hash', $watermarked_hash);
			}
			
			// $res['file']['link'] = $res['file']['link_watermark'];
			$res['file']['link'] = $watermarked_hash['encryption'];
			$res['is_converted'] = 1;
			$res['have_header'] = 1;

			$addView = $this->model->updateDocumentPopuler($id); // add view;
			$this->addActivity($id, "Viewed this Document.");
			if ($is_echo){
				echo json_encode($res);
			} else {
				return $watermarked_hash['encryption'];
			}
			// print_r($hasil);
		} else {
			$res['is_converted'] = 0;
			$res['have_header'] = 0;
			if ($is_echo){
				echo json_encode($res);
			}
			
		}		
	}

	public function diedoc($id){
		$document = $this->model->getDocument("d.id=$id", $is_published);
		echo '<pre>';
		var_dump($document);
		die();
	}	

	public function download(){
		if($_GET['file']){
			$id = $_GET['file'];
			
			$this->load->model('Document_activity_model');
			$activity["user_id"] = SessionManagerWeb::getUserID();
			$activity["document_id"] = $id;
			$activity["activity"] = "Downloaded this Document.";
			$activity["type"] = Document_activity_model::ACTIVITY;
			//$this->Document_activity_model->create($activity,TRUE,TRUE);

			$file = $this->model->getDocumentWithLink($id);

			// $fsize = filesize($file['local']);

			// download baru 
			$ciConfig = $this->config->item('utils');
	        $path = $ciConfig['full_upload_dir'].'document/files/';
			$fsize = filesize($path.$file['nama_file']);
			// echo '<pre>';
			// vaR_dump($file);
			// die();
			// $fullmime = Image::getMime($file['nama_file']);
			// $mimes = explode('/', $fullmime);
		
			$fname = basename($file['nama_file']);
			header('Content-Description: File Transfer'); 
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary"); 
			header('Expires: 0'); 
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
			header('Pragma: public'); 
			header("Content-disposition: attachment; filename=\"" . $fname . "\""); 
			header("Content-Length: ".$fsize);
			ob_clean();
			flush();
			// echo readfile($file['link']);
			echo readfile($file['real_link']);
		}
	}

	public function createTextWatermarkA($filename, $userid, $idCompany){
		echo "<br>ini create".$filename;
		$data_text = array();
		$dataCompany =  array(
						'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
						'3000'	=> 'PT. SEMEN PADANG',
						'4000'	=> 'PT. SEMEN TONASA',
						'5000'	=> 'PT. SEMEN GRESIK',
						'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
					);
		//font
        $font = './assets/web/fonts/arial.ttf';
        //font size
        $font_size = 15;
        //image width
        $width = 1500;
        //text margin
        $margin = 0;
        $data_text['text1']['value']         = "Dokumen ini terbatas untuk kalangan sendiri dan milik ".$dataCompany[$idCompany].", Dilarang membagikan, menyalin, dan";
        $data_text['text1']['name']          = $filename.'_'.$userid.'_text1';
        $data_text['text2']['value']          = "mencetak tanpa izin. Kesalahan dan perbedaan isi diluar tanggung jawab ".$dataCompany[$idCompany];
        $data_text['text2']['name']            = $filename.'_'.$userid.'_text2';
        $data_text['text3']['value']     = "Diakses oleh ".$username." (".$_SERVER['REMOTE_ADDR']."), Tanggal ".date("d-m-Y");
        $data_text['text3']['name']      = $filename.'_'.$userid.'_text3';
        echo "<pre>";print_r($data_text);
        // $data_text['tgl_view']['value']     = date("d-m-Y");
        // $data_text['tgl_view']['name']      = uniqid();

        foreach ($data_text as $text) {
            $text_a = explode(' ', $text['value']);
            $text_new = '';
            foreach($text_a as $word){
                //Create a new text, add the word, and calculate the parameters of the text
                $box = imagettfbbox($font_size, 0, $font, $text_new.' '.$word); 
                //if the line fits to the specified width, then add the word with a space, if not then add word with new line
                // echo "<pre>";print_r($box);echo "</pre>";
                if($box[2] > $width - $margin*2){
                    $text_new .= "\n".$word;
                } else {
                    $text_new .= " ".$word;
                }
            }
            // //trip spaces
            $text_new = trim($text_new);
            //new text box parameters
            // echo "<pre>";print_r($text_new);echo "</pre>";
            $box = imagettfbbox($font_size, 0, $font, $text_new);
            //new text height
            $height = $box[1] + $font_size + $margin * 2; // set height
            ///////////////////////CREATE FILE 1///////////////////////////////
            $img = imagecreatetruecolor($width,$height); // create new img and set width height
            // Background color
            $bg = imagecolorallocate($img, 255, 255, 255);  // set color from new img
            imagefilledrectangle($img, 0, 0,$width ,$height , $bg); //make background from new img, ()
            imagecolortransparent($img, $bg);
            ///////////////////////////////END OF FILE 1/////////////////////////////////////
            ////////////////////////////////FILE 2////////////////////////////////////////////
                // //create image
            $im = imagecreatetruecolor($width, $height);
             
            //create colors
            // $tbg = imagecolorallocate($im, 255, 255, 255);
            $white = imagecolorallocate($im, 255, 255, 255);
            $red = imagecolorallocate($im, 255, 0, 0);
            //color image
            imagefilledrectangle($im, 0, 0, $width, $height, $white);
            // $color = imagecolorallocate($img, 0, 0, 0);
            //add text to image
            imagettftext($im, $font_size, 0, $margin, $font_size+$margin, $red, $font, $text_new);
             $op = 100;
                if ( ($op < 0) OR ($op >100) ){
                    $op = 100;
                }
            imagecopymerge($img, $im, 0, 0, 0, 0, $width, $height, $op); // menggabung file $blank dan $img
            $dirpng = "./assets/uploads/document/files/";
            imagepng($dirpng."aaaaaaaaaaaaaaa.png");
            ////////////////////////////////END OF FILE 2////////////////////////////////////////////
        }
        return $data_text;
	}


	

	public function tesOpacity(){
		// return $photoCompany;
		$image = imagecreatefrompng('./assets/uploads/company/00002000.png');
		$opacity = 0.2;
		imagealphablending($image, false); // imagesavealpha can only be used by doing this for some reason
		imagesavealpha($image, true); // this one helps you keep the alpha. 
		$transparency = 1 - $opacity;
		imagefilter($image, IMG_FILTER_COLORIZE, 0,0,0,127*$transparency); // the fourth parameter is alpha
		// header('Content-type: image/png');/
		imagepng($image, './assets/uploads/company/00002000_opa.png' );
	}
	public function createTextWatermark($filename, $userid, $username,$idCompany){
   		$data_text = array();
        $name = uniqid();
        $dataCompany =  array(
						'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
						'3000'	=> 'PT. SEMEN PADANG',
						'4000'	=> 'PT. SEMEN TONASA',
						'5000'	=> 'PT. SEMEN GRESIK',
						'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
					);
        //font
        $font = './assets/web/fonts/arial.ttf';
        //font size
        $font_size = 15;
        //image width
        $width = 1500;
        //text margin
        $margin = 0;
        $data_text['text1']['value']         = "Dokumen ini terbatas untuk kalangan sendiri dan milik ".$dataCompany[$idCompany].", Dilarang membagikan, menyalin, dan";
        $data_text['text1']['name']          = $filename.'_'.$userid.'_text1';
        $data_text['text2']['value']          = "mencetak tanpa izin. Kesalahan dan perbedaan isi diluar tanggung jawab ".$dataCompany[$idCompany] ;
        $data_text['text2']['name']            = $filename.'_'.$userid.'_text2';
        $data_text['text3']['value']     = "Diakses oleh ".$username." (".$_SERVER['REMOTE_ADDR']."), Tanggal ".date("d-m-Y");
        $data_text['text3']['name']      = $filename.'_'.$userid.'_text3';
        // $data_text['tgl_view']['value']     = date("d-m-Y");
        // $data_text['tgl_view']['name']      = uniqid();

        foreach ($data_text as $text) {
            $text_a = explode(' ', $text['value']);
            $text_new = '';
            foreach($text_a as $word){
                //Create a new text, add the word, and calculate the parameters of the text
                $box = imagettfbbox($font_size, 0, $font, $text_new.' '.$word); 
                //if the line fits to the specified width, then add the word with a space, if not then add word with new line
                // echo "<pre>";print_r($box);echo "</pre>";
                if($box[2] > $width - $margin*2){
                    $text_new .= "\n".$word;
                } else {
                    $text_new .= " ".$word;
                }
            }
            // //trip spaces
            $text_new = trim($text_new);
            //new text box parameters
            // echo "<pre>";print_r($text_new);echo "</pre>";
            $box = imagettfbbox($font_size, 0, $font, $text_new);
            //new text height
            $height = $box[1] + $font_size + $margin * 2; // set height
            ///////////////////////CREATE FILE 1///////////////////////////////
            $img = imagecreatetruecolor($width,$height); // create new img and set width height
            // Background color
            $bg = imagecolorallocate($img, 255, 255, 255);  // set color from new img
            imagefilledrectangle($img, 0, 0,$width ,$height , $bg); //make background from new img, ()
            imagecolortransparent($img, $bg);
            ///////////////////////////////END OF FILE 1/////////////////////////////////////
            ////////////////////////////////FILE 2////////////////////////////////////////////
                // //create image
            $im = imagecreatetruecolor($width, $height);
             
            //create colors
            // $tbg = imagecolorallocate($im, 255, 255, 255);
            $white = imagecolorallocate($im, 255, 255, 255);
            $red = imagecolorallocate($im, 255, 0, 0);
            //color image
            imagefilledrectangle($im, 0, 0, $width, $height, $white);
            // $color = imagecolorallocate($img, 0, 0, 0);
            //add text to image
            imagettftext($im, $font_size, 0, $margin, $font_size+$margin, $red, $font, $text_new);
             $op = 100;
                if ( ($op < 0) OR ($op >100) ){
                    $op = 100;
                }
            imagecopymerge($img, $im, 0, 0, 0, 0, $width, $height, $op); // menggabung file $blank dan $img
            $dirpng = "./assets/uploads/document/files/";
            imagepng($img,$dirpng.''.$text['name'].".png");
            ////////////////////////////////END OF FILE 2////////////////////////////////////////////
        }
        return $data_text;
   	}

	public function tryDownload(){
		$userid 	= SessionManagerWeb::getUserID();
		$idCompany  = ltrim(SessionManagerWeb::getCompany(),"0");
		$username 	= SessionManagerWeb::getUserName();
		$this->generateFileDownload('IK__AUD_50045221_002_R0.pdf', 4502, $idCompany, $username, $userid, 'pdf' );
	}
	public function generateFileDownload($file,$document_id, $idCompany, $username, $userid, $extfile){
		require_once(APPPATH."/third_party/fpdf/pdf.php");
		$pdf = new PDF();

		$explodefilename = explode('.', $file);
        $filename = $explodefilename[0];

  //       if($company ==  'PT. Semen Indonesia (Persero) Tbk.'){
		// 	$logo = 'Untitled-1-04.png';
		// }
		// elseif($company == 'PT. SEMEN PADANG'){
		// 	$logo = 'Untitled-1-02.png';
		// }
		// elseif($company == 'PT. SEMEN TONASA'){
		// 	$logo = 'Untitled-1-03.png';
		// }
		// elseif($company == 'PT. SEMEN GRESIK'){
		// 	$logo = 'Untitled-1-03.png';
		// }
		// elseif($company == 'PT. SEMEN KUPANG INDONESIA'){

		// }
		$this->load->model('Unitkerja_model');
		$id = str_pad($idCompany,8,"0",STR_PAD_LEFT);
		$data_company = $this->Unitkerja_model->getBy($id);

		$size = Image::IMAGE_ORIGINAL;
		$name = str_replace('_', '-', $data_company['photo']);
		$logo_header = strtolower($size).'-' . md5($id.$size) . md5($id . $name) . '-' . $name;
		if(!file_exists($dir.'assets/uploads/company/'.$logo_header)){
			$logo_header = "logo-simple.png";
		}

		$size = Image::IMAGE_OPACITY;
		$name = str_replace('_', '-', $data_company['photo']);
		$logo = strtolower($size).'-' . md5($id.$size) . md5($id . $name) . '-' . $name;

		if(!file_exists($dir.'assets/uploads/company/'.$logo)){
			$logo = "opacity-simple.png";
		}
		
		$dataDoc = $this->model->getDocumentFlow($document_id);
		$this->load->model('Document_types_model');
		$docType = $this->Document_types_model->getBy($dataDoc['TYPE_ID'], 'type');
		$get_tanggal_revisi = "select to_char(add_months(VALIDITY_DATE, '-12'), 'DD MON YYYY') AS tanggal_revisi from documents where ID='".$document_id."'";
		$tanggal_revisi = dbGetOne($get_tanggal_revisi);

		/////////////////////////// Data Header /////////////////////////////////
        $data = array(
			    		'id_document'	=> $document_id,
			    		'judul'			=> $dataDoc['TITLE'],
			    		'kode_dokumen'	=> $dataDoc['CODE'],
			    		'revisi'		=> $dataDoc['REVISION'],
			    		'tanggal_revisi'=> $tanggal_revisi,
			    		'nama_company'	=> $data_company['name'],
			    		'id_company'	=> $idCompany,
			    		'logo'			=> $logo_header,
			    		'tipe_dokumen'	=> $docType,
			    		'filename'		=> $dataHF['filename']
			    	);
        $pdf->setDataHeader($data);
        
        //////////////////////// Data Footer //////////////////////////////////////
        $dataFooter = array();
        $index = 0;
        $getRelated = "SELECT DOCUMENT_RELATED.DOCUMENT2, DOCUMENTS.CODE FROM DOCUMENT_RELATED INNER JOIN DOCUMENTS ON DOCUMENT_RELATED.DOCUMENT2 = DOCUMENTS.ID WHERE DOCUMENT_RELATED.DOCUMENT1 = '".$document_id."'";
        $related = dbGetRows($getRelated);
        foreach ($related as $r) {
        	$dataFooter[$index]['document'] = $r['DOCUMENT2'];
        	$dataFooter[$index]['kode']		= $r['CODE'];
        	$index++;
        }
        $pdf->setDataFooter($dataFooter);
		
		///////////////////create text watermark (samping halaman)/////////////////////

		$data_text = $this->createTextWatermark($filename, $userid, $username, $idCompany);
		// echo $filename;
		// echo "<pre>";print_r($data_text);
        ////////////////////////////// Proses Manipulate PDF //////////////////////////////
        // echo "<pre>";print_r($pdf->getDataHeader());echo "</pre>";
        // echo "<pre>";print_r($pdf->getDataFooter());echo "</pre>";
		$file = "./assets/uploads/document/files/".$file;
        if (file_exists($file)){
           $pagecount = $pdf->setSourceFile($file);
           // echo $pagecount;
            // echo "b";
        } else {
            return FALSE;
            // echo "a";
        }
		$dirpng = "./assets/uploads/document/files/";
        for($i = 1; $i <= $pagecount; $i++){
        	$tpl = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($tpl);
            $pageSize = "A4";
            if($size['h'] == 297.00008333333){ $pageSize = "A4";}
            elseif($size['h'] == 355.6){ $pageSize = "Legal";}
            elseif($size['h'] == 279.4){ $pageSize = "Letter";}
            elseif($size['h'] == 419.99958333333){ $pageSize = "A3";}

            if($i == 1 || $i == $pagecount){ // jika halaman 1
                if($size['w'] <= $size['h']){ // jika portrait
                    $pdf->setOrientationForHeader('P');
                    $pdf->setOrientationForFooter('P');
                    $tempForFooter[$i] = 'P';
                    $pdf->addPage("P", $pageSize);
                    if($dataHF['id_tipe_dokumen'] != 5){
                    	$pdf->RotatedImage($dir.'assets/uploads/company/'.$logo,55,90,100,100,0);
                    	$pdf->RotatedImage($dir.'assets/web/images/table-2.png', 5, 6, 200, 24, 0);
                	}
                	$pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 207, 60, 250, 3, 270);
					$pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 202, 95, 250, 3, 270);
		 			$pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 197, 100, 250, 3, 270);
                    $pdf->setOrientationForFooter('P');
                }
                else{ // jika landscape
                    $pdf->setOrientationForHeader('L');
                    $pdf->setOrientationForFooter('L');
                    $tempForFooter[$i] = 'L';
                    $pdf->addPage("L", $pageSize);
                    if($dataHF['id_tipe_dokumen'] != 5){
                    	$pdf->Image($dir.'assets/uploads/company/'.$logo,115,70,100,100);
                    	$pdf->RotatedImage($dir.'assets/web/images/landscape-01.png', 5, 6, 287, 24, 0);
                    }
                    $pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 290, 45, 175, 3, 270);
		            $pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 285, 55, 175, 3, 270);    
		            $pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 280, 65, 250, 3, 270);
                    $pdf->setOrientationForFooter('L');
                }
            }
            else{  // jika selain halaman 1
                if($size['w'] <= $size['h']){  // jika portrait
                    $pdf->setOrientationForFooter($tempForFooter[$i-1]);
                    $pdf->setOrientationForHeader('P');
                    $tempForFooter[$i] = 'P';
                    $pdf->addPage("P", $pageSize);
                    if($dataHF['id_tipe_dokumen'] != 5){
                    	$pdf->RotatedImage($dir.'assets/uploads/company/'.$logo,55,90,100,100,0);
                    	$pdf->RotatedImage($dir.'assets/web/images/table-2.png', 5, 6, 200, 24, 0);
                	}
                	$pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 207, 60, 250, 3, 270);
					$pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 202, 95, 250, 3, 270);
		 			$pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 197, 100, 250, 3, 270);
                }
                else{ // jika landscape
                    $pdf->setOrientationForFooter($tempForFooter[$i-1]); // ambil value footer dr halaman sebelumnya
                    $pdf->setOrientationForHeader('L');
                    $tempForFooter[$i] = 'L'; //set value footer untuk diambil di halaman berikutnya
                    $pdf->addPage("L", $pageSize);
                    if($dataHF['id_tipe_dokumen'] != 5){
                    	$pdf->Image($dir.'assets/uploads/company/'.$logo,115,70,100,100);
                    	$pdf->RotatedImage($dir.'assets/web/images/landscape-01.png', 5, 6, 287, 24, 0);
                	}
                	$pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 290, 45, 175, 3, 270);
	                $pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 285, 55, 175, 3, 270);
	                $pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 280, 65, 250, 3, 270);
                }
            }
            $pdf->useTemplate($tpl, 0, 0, 0, 0, TRUE);
           
        }
        unlink($dirpng."".$data_text['text1']['name'].'.png');
		unlink($dirpng."".$data_text['text2']['name'].'.png');
		unlink($dirpng."".$data_text['text3']['name'].'.png');
        $pdf->Output('./assets/uploads/document/files/'.$filename.'_'.$userid.'_download.pdf', 'F');
	}

	// public function tryConvert(){
	// 	$this->reconvert(4601);
	// }
	public function reconvert(){
		$document_id = $this->input->post('id');
		$getData = $this->model->getOne($document_id, 'DOCUMENT');
		$extensionFile = pathinfo($getData, PATHINFO_EXTENSION);
		if($extensionFile == 'xlsx' || $extensionFile == 'xls'){
			$data = array(
				'is_converted'	=> 1,
				'have_header'	=> 0
			);
		}
		else{
			$data = array(
				'is_converted'	=> 0,
				'have_header'	=> 0 
			);
			$updateDoc = $this->model->updateDocument(SessionManagerWeb::getUserID(),$document_id,$data);
			if($updateDoc){
				// echo "berhasil update<br>";
				$hapus = $this->model->hapusConvertChecker($document_id);
				if($hapus){
					// echo "berhasil hapus<br>";
					$document_name = $this->model->getOne($document_id, 'DOCUMENT');
					$explodeFile= explode('.', $document_name);
			    	$extfile   	= end($explodeFile); // extensi dari file yang di cursor
					unset($explodeFile[count($explodeFile)-1]);
					$nama_file = implode('.', $explodeFile);
					foreach (glob('./assets/uploads/document/files/'.$nama_file."*") as $file) {
						if(pathinfo($file, PATHINFO_EXTENSION) == 'pdf'){
							unlink($file);
						}
					}
				}
				$title_name = $this->model->getOne($document_id, 'TITLE');
			}
			SessionManagerWeb::setFlashMsg(true, 'Success to reconvert Document "'.$title_name.'"');
			echo $document_id;
		}
		// echo "<pre>";print_r($getData);echo "</pre>"
		
	}

	public function download_checklist(){
		$id = $this->input->post('checklist_download');
		// echo "<pre>";print_r($id);die();
		$this->load->library('zip');
		$ciConfig = $this->config->item('utils');

		$date = date('d-m-Y');
		// $id = array();
		// echo "<pre>";print_r($id);die();
		foreach ($id as $doc) {
			$filename = dbGetOne('select document from documents where id='.$doc);
			$name = explode('.', $filename);
			$watermark = $ciConfig['full_upload_dir'].'document/files/'.$name[0].'_'.SessionManagerWeb::getUserID().'_watermarked.pdf';
			if(!file_exists($watermark)){
				echo "masuk ini<br>";
				$unitkerja_id = $this->model->getOne($doc, "unitkerja_id");
				$this->load->model('Unitkerja_model');
				$idCompany = $this->Unitkerja_model->getOneBy($unitkerja_id, "\"COMPANY\"");
				// $idCompany =    $this->model->get
				// $idCompany 	= SessionManagerWeb::getCompany();
		    	$dataCompany =  array(
									'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
									'3000'	=> 'PT. SEMEN PADANG',
									'4000'	=> 'PT. SEMEN TONASA',
									'5000'	=> 'PT. SEMEN GRESIK',
									'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
								);
		    	
		    	$username 	= SessionManagerWeb::getUserName();
    			$userid 	= SessionManagerWeb::getUserID();
    			$explodeFile= explode('.', $filename);
    			$extfile   	= end($explodeFile);
    			// echo $extfile;
    			unset($explodeFile[count($explodeFile)-1]);
				$nama_file = implode('.', $explodeFile).'.pdf';
    			$this->load->model('Unitkerja_model');
    			// echo $nama_file."---".$idCompany."----".$username."----".$userid."<br>";die();
				// $photoCompany = $this->Unitkerja_model->getOneBy('0000'.$idCompany, "\"PHOTO\"");
				$hasil = $this->PlaceWatermark($nama_file, $idCompany , $username, $userid, 'pdf' );
			}
			// echo $name[0]."<br>";
			// $this->zip->read_file($ciConfig['full_upload_dir'].'document/files/'.$name[0].'_header.pdf');
		}
		// echo $date;
		foreach ($id as $doc) {
			$filename = dbGetOne('select document from documents where id='.$doc);
			$name = explode('.', $filename);
			// echo $name[0]."<br>";
			$this->zip->read_file($ciConfig['full_upload_dir'].'document/files/'.$name[0].'_'.SessionManagerWeb::getUserID().'_watermarked.pdf');
		}
		
		// $name = 'IK_SMI_ASM_50045221_001_R0.docx';
		
		// $this->zip->read_file($ciConfig['full_upload_dir'].'document/files/obsolete.png');
		$download = $this->zip->download('download_doccon_holding_'.$date.'.zip');
		// $name = 'mydata2.txt';
		$data = 'Another Data String!';
		$this->zip->add_data($name, $data);
	}
	public function download_watermark($id=null, $word=0){
		$have_header = $this->model->getOne($id, "have_header");
		$this->load->model('Document_download_model');
		$userid 	= SessionManagerWeb::getUserID();
		$dataDoc = $this->model->getDocumentFlow($id);
		$ciConfig = $this->config->item('utils');
		$access = 0;
		$access = $this->Document_download_model->filter('where document_id='.$id.' and user_id='.$userid.'  ')->getOne("is_allowed");
		if($dataDoc['CREATOR_ID'] == SessionManagerWeb::getUserID() && (SessionManagerWeb::isDocumentController() == true && SessionManagerWeb::getCompany() == '2000')){
			$access = 1;
			$jenis = 'pdf';
			if($word == 1){
				$jenis = 'word';
			}
			$text = 'Download the '.$jenis.' file with Creator Role';
			$this->addActivity($id, $text);
		}
		elseif($dataDoc['CREATOR_ID'] == SessionManagerWeb::getUserID()){
			$access = 1;
			$jenis = 'pdf';
			if($word == 1){
				$jenis = 'word';
			}
			$text = 'Download the '.$jenis.' file with Creator Role';
			$this->addActivity($id, $text);
		}
		elseif(SessionManagerWeb::isDocumentController() == true && SessionManagerWeb::getCompany() == '2000'){
			$access = 1;
			$jenis = 'pdf';
			if($word == 1){
				$jenis = 'word';
			}
			$text = 'Download the '.$jenis.' file with Document Controller Holding Role';
			$this->addActivity($id, $text);
		}
		if($word == 1){
			if($access == 1){
				$fname = $dataDoc['DOCUMENT'];
				$path = $ciConfig['full_upload_dir'].'document/files/'.$fname;
				$fsize = filesize($path);
				header('Content-Description: File Transfer'); 
				header('Content-Type: application/octet-stream');
				header("Content-Transfer-Encoding: Binary"); 
				header('Expires: 0'); 
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
				header('Pragma: public'); 
				header("Content-disposition: attachment; filename=\"" . $fname . "\""); 
				header("Content-Length: ".$fsize);
				ob_clean();
				flush();
				// echo readfile($file['link']);
				// echo readfile($file['link_watermark']);
				echo readfile($path);
			}
				
		}
		else{
			$explodeFile= explode('.', $dataDoc['DOCUMENT']);
	    	$extfile   	= end($explodeFile); // extensi dari file yang di cursor
			unset($explodeFile[count($explodeFile)-1]);
			$nama_file = implode('.', $explodeFile);
			$document_name = $nama_file.'_'.$userid.'_download.pdf';
			// echo $access;
			if($have_header == 1 && $access == 1 ){
				
				$path = $ciConfig['full_upload_dir'].'document/files/'.$document_name;
				if(!file_exists($path)){
					$userid 	= SessionManagerWeb::getUserID();
					$idCompany  = ltrim(SessionManagerWeb::getCompany(),"0");
					$username 	= SessionManagerWeb::getUserName();
					$this->generateFileDownload($nama_file.".pdf",$id, $idCompany, $username, $userid, 'pdf');
					// create file download
				}	
				$watermarked_hash['user_id'] = $userid;
				$watermarked_hash['document_name'] = $document_name;
				$encryption =hash_hmac('sha256',$document_name.$userid, date("d-m-Y h:i:sa"));
				$watermarked_hash['encryption'] = $encryption;
				if ($this->model->isExistWatermarked($document_name, $userid)){
					dbUpdate('document_watermarked_hash', $watermarked_hash, "document_name='".$document_name."' and user_id=".$userid);
				} else {
					dbInsert('document_watermarked_hash', $watermarked_hash);
				}
				// echo $name;
				// $file = $this->model->getDocumentWithLink($id);
				$fsize = filesize($path);

				$fname = basename($document_name);
				header('Content-Description: File Transfer'); 
				header('Content-Type: application/octet-stream');
				header("Content-Transfer-Encoding: Binary"); 
				header('Expires: 0'); 
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0'); 
				header('Pragma: public'); 
				header("Content-disposition: attachment; filename=\"" . $fname . "\""); 
				header("Content-Length: ".$fsize);
				ob_clean();
				flush();
				// echo readfile($file['link']);
				// echo readfile($file['link_watermark']);
				echo readfile($path);

				
			}
			else{
				// $download = $this->Document_download_model->getBy($id);
				$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(),$id);
				// echo "<pre>";print_r($document);echo "</pre>";
				$this->data['title'] = $document['type_name'].' "'.$document['title'].'"';
				// $this->data['title'] = $;
				$buttons = array();
		        $buttons[] = array('label' => ' Back', 'type' => 'success', 'icon' => 'chevron-left', 'click' => 'goBack()' );
		        $this->data['buttons'] = $buttons;
		        $this->data['access'] = $access;
				$this->template->viewDefault('document_download_watermark',$this->data);
				// die('belum ada halaman utuk warning ini');
			}
		}
		
		


		// $is_published = '0';
		// if ($_GET['is_published']){
		// 	$is_published = '1';
		// }

		// $document = $this->model->getDocument("d.id=$id", $is_published);
		// $document = $this->model->filter("where d.\"document_id\"=$id")->getDocument();
		// $file = $this->model->getDocumentWithLink($id);
		// $res['document'] = $document;
		// $res['file'] = $file;
		// $is_convert = $this->model->getOne($id, "is_converted");

		// if($is_convert == 1){
		// 	if(!file_exists($res['file']['link_watermark'])){
		// 		$unitkerja_id = $this->model->getOne($id, "unitkerja_id");
		// 		$this->load->model('Unitkerja_model');
		// 		$idCompany = $this->Unitkerja_model->getOneBy($unitkerja_id, "\"COMPANY\"");
		// 		// $idCompany =    $this->model->get
		// 		// $idCompany 	= SessionManagerWeb::getCompany();
		//     	$dataCompany =  array(
		// 							'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
		// 							'3000'	=> 'PT. SEMEN PADANG',
		// 							'4000'	=> 'PT. SEMEN TONASA',
		// 							'5000'	=> 'PT. SEMEN GRESIK',
		// 							'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
		// 						);
		    	
		//     	$username 	= SessionManagerWeb::getUserName();
  //   			$userid 	= SessionManagerWeb::getUserID();
  //   			$explodeFile= explode('.', $res['file']['nama_file']);
  //   			$extfile   	= end($explodeFile);
  //   			// echo $extfile;
  //   			unset($explodeFile[count($explodeFile)-1]);
  //   			$nama_file = implode('.', $explodeFile).'.pdf';
  //   			$this->load->model('Unitkerja_model');
		// 		// $photoCompany = $this->Unitkerja_model->getOneBy('0000'.$idCompany, "\"PHOTO\"");
		// 		$hasil = $this->PlaceWatermark($nama_file, $idCompany , $username, $userid, 'pdf' );
		// 	}
		// 	$watermarked_hash = array();
		// 	$watermarked_hash['user_id'] = SessionManagerWeb::getUserID();
		// 	$watermarked_hash['document_name'] = $res['file']['watermark'];
		// 	$watermarked_hash['encryption'] = hash_hmac('sha256',$watermarked_hash['document_name'].$watermarked_hash['user_id'], date("d-m-Y h:i:sa")); 
		// 	if ($this->model->isExistWatermarked($watermarked_hash['document_name'], $watermarked_hash['user_id'])){
		// 		dbUpdate('document_watermarked_hash', $watermarked_hash, "document_name='".$watermarked_hash['document_name']."' and user_id=".$watermarked_hash['user_id']);
		// 	} else {
		// 		dbInsert('document_watermarked_hash', $watermarked_hash);
		// 	}
			
		// 	// $res['file']['link'] = $res['file']['link_watermark'];
		// 	$res['file']['link'] = $watermarked_hash['encryption'];
		// 	$res['is_converted'] = 1;
		// 	$res['have_header'] = 1;

		// 	$addView = $this->model->updateDocumentPopuler($id); // add view;
		// 	$this->addActivity($id, "Viewed this Document.");
		// 	echo json_encode($res);
		// 	// print_r($hasil);
		// } else {
		// 	$res['is_converted'] = 0;
		// 	$res['have_header'] = 0;
		// 	echo json_encode($res);
		// }		
	}

	public function removeFileTemp(){
		$this->unsetFileTemp();
	}

	public function removeFileTempPDF(){
		$this->unsetFileTempPDF();
	}

	public function upload() {
		$this->unsetFileTemp();
		unset($_SESSION['home_status']);
		unset($_SESSION['group_selected']);
		$_SESSION['group_selected'] = '0';
		unset($_SESSION['document_status']);
		$_SESSION['document_status']['U'] = 'U';
		$this->load->model('Tag_model');
		$this->data['lokasi'] = $this->model->getLokasi();
		$this->data['klasifikasi'] = $this->model->getKlasifikasi();
		$tags = $this->Tag_model->getTags();
		$temp = 0;
		foreach($tags as $obj){
			if($temp>0){
				$val = $val . ", ";
			}
			$val = $val . $obj['name'];
			$temp++;
		}
		$this->data['Tags'] = $val;
		$listOrganisasi = $this->model->listOrganisasi();
		$temp=0;
		foreach($listOrganisasi as $org){
			$document[$temp]=$this->model->getDocumentOrganisasi($org['id_organisasi'],$userID);
			$temp++;
		}
		$tree = $this->createNodes($listOrganisasi,-1);
		unset($tree["arr"]);
		$this->data['tree'] = $tree["st"];
		$this->data['isShowTree'] = false;
        $this->template->viewDefault($this->view, $this->data);
    }
	
	public function Edit($id = null){
		$this->load->model('Tag_model');
		$filter = "d.id=".$id;
		$this->data = $this->model->getDocument($filter);
		$this->data['idDokumen'] = $id;
		$this->data['title'] = "Edit Dokumen";
		$this->data['lokasi'] = $this->model->getLokasi();
		$this->data['klasifikasi'] = $this->model->getKlasifikasi();
		$tags = $this->Tag_model->getTags();
		$temp = 0;
		foreach($tags as $obj){
			if($temp>0){
				$val = $val . ", ";
			}
			$val = $val . $obj['name'];
			$temp++;
		}
		$this->data['Tags'] = $val;
		$listOrganisasi = $this->model->listOrganisasi();
		$temp=0;
		foreach($listOrganisasi as $org){
			$document[$temp]=$this->model->getDocumentOrganisasi($org['id_organisasi'],$userID);
			$temp++;
		}
		$tree = $this->createNodes($listOrganisasi,-1);
		unset($tree["arr"]);
		$this->data['tree'] = $tree["st"];
		$this->data['isShowTree'] = false;
		$this->template->viewDefault($this->view, $this->data);
	}

	public function editRelated(){
		$this->load->model('Document_activity_model');
		$id = $this->input->post('id',true);
		$relatedDocument = json_decode($this->input->post('relatedEdit',true));
		// echo "<pre>";print_r($relatedDocument);die();
		$this->model->updateRelatedDocumentNew($id,$relatedDocument);
		// $activity["user_id"] = SessionManagerWeb::getUserID();
		// $activity["document_id"] = $id;
		// $activity["activity"] = " Change Related Document";
		// $activity["type"] = Document_activity_model::HISTORY;
		// $this->Document_activity_model->create($activity,TRUE,TRUE);
	}
	
	public function editDocument(){
		$this->load->model('Document_activity_model');
		$id = $this->input->post('id_dokumen',true);
		$note = $this->input->post('note',true);
		$filter = "d.id=".$id;
		$res = $this->model->getDocument($filter);
		$doc = $res[0];
		//$data['user_id'] = $this->input->post('userID',true);
		$data['title'] = $doc['title'];
		$data['description'] = $doc['description'];
		// $data['lokasi_id'] = $doc['lokasi_id'];
		// $data['klasifikasi_id'] = $doc['klasifikasi_id'];
		$data['revision'] = $doc['revision'] + 1;
		$data['show'] = 1;
		$data['user_id'] = SessionManagerWeb::getUserID();
		if($doc['document'][0]['parent'] == null)
			$data['parent'] = $id;
		else
			$data['parent'] = $doc['document'][0]['parent'];
		$data['update_note'] = $note;
		$this->db->trans_begin();
		$insert = $this->model->create($data, TRUE, TRUE);
		if ($insert) {
			if (!$this->_moveDocumentTemp($insert)) {
				$this->db->trans_rollback();
				$this->model->deleteDocument($insert);
				SessionManagerWeb::setFlashMsg(false, 'Failed to Edit Document');
			}
			else {
				unset($_SESSION['sigap']['draft_post']);
				// $this->model->copyTag($id,$insert);
				// $this->model->updateDocumentTag($insert,$this->input->post('tags',true));
				$dataUpdate['show'] = 0;
				$this->model->updateDocument(SessionManagerWeb::getUserID(),$id,$dataUpdate);
				// $dataUpdate = array();
				// $dataUpdate['namafile'] = $doc['document'][0]['namafile'];
				// $this->model->updateDocument($insert,$dataUpdate);
				$activity["user_id"] = SessionManagerWeb::getUserID();
				$activity["document_id"] = $insert;
				$activity["activity"] = " Change Document";
				$activity["type"] = Document_activity_model::HISTORY;
				$this->Document_activity_model->create($activity,TRUE,TRUE);
				$this->db->trans_commit();
		        SessionManagerWeb::setFlashMsg(true, 'Success to Change Document');
			}
		}
		// $this->redirect();
	}

	function editDeskripsi(){
		$this->load->model('Document_activity_model');
		$id = $this->input->post('id_dokumen',true);
		$filter = "d.id=".$id;
		$doc = $this->model->getDocument($filter);
		$doc = $doc['document'];
		$data['title'] = $this->input->post('title',true);
		$data['description'] = $this->input->post('deskripsi',true);
		// $data['lokasi_id'] = $this->model->getKodeLokasi($this->input->post('lokasi',true));
		// $data['klasifikasi_id'] = $this->model->getNamaKlasifikasi($this->input->post('klasifikasi',true));
		$data['type_id'] = $this->model->getNamaJenis($this->input->post('jenis',true));
		$parent = $doc[0]['parent'];
		$data['show'] = 1;
		$data['user_id'] = SessionManagerWeb::getUserID();
		
		$this->model->updateDocument(SessionManagerWeb::getUserID(),$id,$data);
		// $this->model->updateDocumentTag($id,json_decode($this->input->post('tags',true)));
		$activity["user_id"] = SessionManagerWeb::getUserID();
		$activity["document_id"] = $id;
		$activity["activity"] = "Change Document Description";
		$activity["type"] = Document_activity_model::HISTORY;
		$this->Document_activity_model->create($activity,TRUE,TRUE);
	}

	function getLokasi(){
		$val = array();
		if(isset($_REQUEST['query'])){
			$val = $this->model->getLokasi($_REQUEST['query']);
		}
		echo json_encode($val);
	}

    //Insert Dokumen
    function kirim()  {
        if ($_POST) {
        	$this->load->model("User_model");
        	$data = array();
            $data['user_id'] = SessionManagerWeb::getUserID();
            $data['description'] = $this->input->post('descriptionIns',true);
            $data['title'] = $this->input->post('judul',true);
			$data['revision'] = 0;
			$data['show'] = 1;
			$data['type_id'] = $this->input->post('jenisIns',true);
			$data['status'] = Document_model::STATUS_PROSES;
			$data['creator_id'] = $this->input->post('creator_id',true);
			$data['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$data['creator_id']."' ")->getOneFilter("jabatan_id");
			$data['approver_id'] = $this->input->post('approver_id',true);
			$data['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$data['approver_id']."' ")->getOneFilter("jabatan_id");
			$data['prosesbisnis_id'] = $this->input->post('prosesbisnis',true);
			$data['is_upload'] = $this->input->post('is_create_later')=='0' ? 1 : 0; 
			$data['unitkerja_id'] = $this->input->post('myunitkerja', true);
			$retensi = $this->input->post('retension_input', true);
			if ($this->input->post('retension_select', true)=='1'){
				$data['retension'] = $retensi*12;
			} else {
				$data['retension'] = $retensi;
			}
			if($this->input->post('company_header')){ // membuat dokumen untuk opco
				$data['header_logo'] = $this->input->post('company_header');
			}
			// $data['code'] = $this->input->post('kodeAdministratif', true);
			$data['code'] = $this->model->getCodeInForm($data['type_id'], $data['prosesbisnis_id'], $data['unitkerja_id'], $this->input->post('form_type', true));

			// Setting Workflow
			$this->load->model('Workflow_model');
			$data['workflow_id'] = Workflow_model::PENGAJUAN;
			$flow = $this->Workflow_model->getTasks();

			// set urutan
			$data['workflow_urutan'] = 2;
			$this->load->model('Document_types_model');
			// if ($data['creator_id']==$data['user_id']){
			// 	$data['workflow_urutan'] = 4;
			// }
			if (!$this->Document_types_model->getBy($data['type_id'], 'need_reviewer')){ // jika dia tidak butuh reviewer
				$data['workflow_urutan'] = 3;
				if ($data['creator_id']==$data['user_id']){ // Jika creator = drafter
					$data['workflow_urutan'] = 4;
				}
			} else { // jika butuh reviewer
				$data['workflow_urutan'] = 2;
			}

			// if ($data['creator_id']==$data['user_id']){
			// 	if (in_array($data['type_id'], array('4', '5'))) {
			// 		$data['workflow_urutan'] = 3;
			// 	} else {
			// 		$data['workflow_urutan'] = 2;
			// 	}
			// }

			if ($this->input->post('is_create_later')=='1'){
				$data['status'] = Document_model::STATUS_BARU;
				$data['document'] = str_replace("/","_",$data['code']).'_R'.$data['revision'].'.docx';
				$data['workflow_urutan'] = 1;
				$data['hash'] = $hash = hash_hmac('sha256',''.$this->config->item('salt_key'), $user_id);
			}



			$data = array_change_key_case($data, CASE_UPPER);
			// echo "<pre>";print_r($data);die();
			$insert = $this->model->create($data, TRUE, TRUE);//Memasukkan data dokumen ke db

            if ($insert) {
            	//Memasukkan file ke dalam db
				if (!$this->_moveDocumentTemp($insert, 'document/temp', $data['CODE'])) {	
					$this->model->deleteDocument($insert);
					SessionManagerWeb::setFlashMsg(false, 'Failed to Submit Document');
				}
				else {

					// Nama Dokumen jika create here
					if ($this->input->post('is_create_later')=='1'){
						$append_id = "_".$insert;
						if ($data['type_id']==Document_model::BOARD_MANUAL){
							$append_id = '';
						}
						$record = array();
						$record['document'] = str_replace("/","_",$data['CODE']).'_R'.$data['REVISION'].$append_id.'.docx';
						dbUpdate("documents", $record, "id=$insert");
					}

					// Tag
					$this->load->model('Document_tags_model');
					$data2['tags'] = $this->input->post('tagIns',true);
					$data2 = array_change_key_case($data2, CASE_UPPER);
					$this->Document_tags_model->create($insert, $data2['TAGS']);
					
					// Unit Kerja
					$this->load->model('Document_unitkerja_model');
					$data_unit_kerja = $this->input->post('unit_kerja_terkait');
					$data_unit_kerja = array_change_key_case($data_unit_kerja, CASE_UPPER);
					$this->Document_unitkerja_model->create($insert, $data_unit_kerja);

					// reviewer
					$this->load->model('Document_reviewer_model');
					$data_reviewer = $this->input->post('reviewer');
					$data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
					$this->Document_reviewer_model->create($insert,$data['WORKFLOW_ID'], $data_reviewer);

					// Activity
					$this->load->model('Document_activity_model');
					$activity["user_id"] = SessionManagerWeb::getUserID();
					$activity["document_id"] = $insert;
					$activity["activity"] = "Uploaded and submitted this document";
					if ($this->input->post('is_create_later')=='1'){
						$activity["activity"] = "Membuat dokumen";
					}
					$activity["type"] = Document_activity_model::HISTORY;
					$activity = array_change_key_case($activity, CASE_UPPER);
					$this->Document_activity_model->create($activity,TRUE,TRUE);
					
					// iso
					$data_iso = array();
					$data_iso['document_id'] = $insert;
					$iso_id = $this->input->post('input_iso', true);
					foreach ($iso_id as $key => $value) {
						$data_iso['iso_id'] = $value;
						dbInsert("DOCUMENT_ISOS", $data_iso);
					}

					// Klausul
					$this->load->model('Document_types_model');
					$need_klausul = $this->Document_types_model->getBy($data['TYPE_ID'], 'klausul');
					if ($need_klausul){
						$data_klausul['document_id'] = $insert;
						$klausul = $this->input->post('input_iso_klausul', true);
						foreach ($klausul as $key => $value) {
							$data_klausul['klausul_id'] = $value;
							dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
						}
					}

					// Related
					$relatedDocument = $this->input->post('related',true);
					$this->model->setRelatedDocument($insert,$relatedDocument);

					if ($data['STATUS']==Document_model::STATUS_PROSES){
						$this->load->model('Notification_model');
						$this->Notification_model->generate(Notification_model::ACTION_DOCUMENT_SUBMIT, $insert);
					}
					$this->db->trans_commit();
			        SessionManagerWeb::setFlashMsg(true, 'Success to Submit Document');
				}

				///// INSERT UNTUK STATISTIK////////////
				// echo "<pre>";print_r($data);echo "</pre>";
				$this->inputDocLog($insert, $data, 0, $data['status']);

				if ($this->input->post('is_create_later')=='1'){
					header('Location: ' . site_url('web/document/detail').'/'.$insert);
				} else
					header('Location: ' . site_url('web/document/dashboard'));
            } else {
			    if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Gagal membuat postingan';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
               // $this->redirect();
                header('Location: ' . site_url('web/document/dashboard'));
            }            
        }    
    }

    function kirimBoardManual()  {
        if ($_POST) {
        	$this->load->model("User_model");
        	$data = array();
        	$data['title'] = $this->input->post('judul_bm',true);
        	$data['code'] = $this->input->post('kodeAdministratif_bm', true);
        	$codes = explode('/', $data['code']);
        	$data['code_number'] = end($codes);
        	$data['revision'] =  $this->input->post('revision_bm',true);
        	$data['unitkerja_id'] = str_pad($this->input->post('company_bm', true), 8, "0", STR_PAD_LEFT);
        	// $data['unitkerja_id'] = str_pad($this->input->post('company_bm', true), 8, '0');
            $data['description'] = $this->input->post('descriptionIns_bm',true);
			$data['show'] = 1;
			$data['type_id'] = Document_model::BOARD_MANUAL;
			$data['status'] = Document_model::STATUS_SELESAI;
			$data['is_published']= 1;
			$data['is_converted']=1;
			$data['have_header'] = 1;
			$data['is_upload'] = 1;
			

			// Setting Workflow
			$this->load->model('Workflow_model');
			$data['workflow_id'] = Workflow_model::PENGAJUAN;

			// set urutan
			$data['workflow_urutan'] = 6;

			$data = array_change_key_case($data, CASE_UPPER);
			// echo "<pre>";print_r($data);die();
			$insert = $this->model->create($data, TRUE, TRUE);//Memasukkan data dokumen ke db
            if ($insert) {
            	// Update date
	        	$this->model->updateColumnDateBy($insert, 'published_at', $this->input->post("publish_date_bm", true));
	        	$this->model->updateColumnDateBy($insert, 'validity_date', $this->input->post("validity_date_bm", true));

            	//Memasukkan file ke dalam db
				if (!$this->_moveDocumentTemp($insert, 'document/temp', $data['CODE'])) {	
					$this->model->deleteDocument($insert);
					SessionManagerWeb::setFlashMsg(false, 'Failed to Upload Document');
				}
				else {
					
					// Unit Kerja
					$this->load->model('Document_unitkerja_model');
					$data_unit_kerja = $this->input->post('unit_kerja_terkait_bm');
					$data_unit_kerja = array_change_key_case($data_unit_kerja, CASE_UPPER);
					$this->Document_unitkerja_model->create($insert, $data_unit_kerja);

					// reviewer
					// $this->load->model('Document_reviewer_model');
					// $data_reviewer = $this->input->post('reviewer');
					// $data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
					// $this->Document_reviewer_model->create($insert,$data['WORKFLOW_ID'], $data_reviewer);

					// Activity
					$this->load->model('Document_activity_model');
					$activity["user_id"] = SessionManagerWeb::getUserID();
					$activity["document_id"] = $insert;
					$activity["activity"] = "Upload this Board Manual Document";
					$activity["type"] = Document_activity_model::HISTORY;
					$activity = array_change_key_case($activity, CASE_UPPER);
					$this->Document_activity_model->create($activity,TRUE,TRUE);
					
					// iso
					// $data_iso = array();
					// $data_iso['document_id'] = $insert;
					// $iso_id = $this->input->post('input_iso', true);
					// foreach ($iso_id as $key => $value) {
					// 	$data_iso['iso_id'] = $value;
					// 	dbInsert("DOCUMENT_ISOS", $data_iso);
					// }

					// Klausul
					// $this->load->model('Document_types_model');
					// $need_klausul = $this->Document_types_model->getBy($data['TYPE_ID'], 'klausul');
					// if ($need_klausul){
					// 	$data_klausul['document_id'] = $insert;
					// 	$klausul = $this->input->post('input_iso_klausul', true);
					// 	foreach ($klausul as $key => $value) {
					// 		$data_klausul['klausul_id'] = $value;
					// 		dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
					// 	}
					// }

					// Related
					$relatedDocument = $this->input->post('related',true);
					$this->model->setRelatedDocument($insert,$relatedDocument);

					$this->db->trans_commit();
			        SessionManagerWeb::setFlashMsg(true, 'Success to Upload Board Manual');
			        unset($_SESSION['page_evaluation']);
			        unset($_SESSION['max_evaluation']);
			        SessionManagerWeb::setCompany($this->input->post("company_bm",true));
				}
				header('Location: ' . site_url('web/document/'));
            } else {
			    if (!is_string($insert)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Failed to Upload Document';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $insert;

                SessionManagerWeb::setFlashMsg(false, $msg);
               // $this->redirect();
                header('Location: ' . site_url('web/document/dashboard'));
            }            
        }    
    }

    public function addHistory($document_id, $history){
    	$activity = array();
    	$this->load->model('Document_activity_model');
		$activity["user_id"] = SessionManagerWeb::getUserID();
		$activity["document_id"] = $document_id;
		$activity["activity"] = $history;
		$activity["type"] = Document_activity_model::HISTORY;
		$activity['workflow_id'] = $this->model->getOne($document_id, 'workflow_id');
		$activity['workflow_urutan'] = $this->model->getOne($document_id, 'workflow_urutan');
		$activity = array_change_key_case($activity, CASE_UPPER);
		$this->Document_activity_model->create($activity,TRUE,TRUE);
    }

    public function addActivity($document_id, $text){
    	$activity = array();
    	$this->load->model('Document_activity_model');
		$activity["user_id"] = SessionManagerWeb::getUserID();
		$activity["document_id"] = $document_id;
		$activity["activity"] = $text;
		$activity["type"] = Document_activity_model::ACTIVITY;
		$activity['workflow_id'] = $this->model->getOne($document_id, 'workflow_id');
		$activity['workflow_urutan'] = $this->model->getOne($document_id, 'workflow_urutan');
		$activity = array_change_key_case($activity, CASE_UPPER);
		$this->Document_activity_model->create($activity,TRUE,TRUE);
    }

    public function tesNotif(){
    	$this->load->model('Notification_model');
    	$this->Notification_model->generate(Notification_model::ACTION_DOCUMENT_CREATE, '469');
    }

    public function checkSameDoc(){
    	$id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));
        $folder_source .= 'document/temp/'.$id;
        
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        
        if (is_dir($path_source)) {
			$files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
				list($mime,$ext) = explode('/',Image::getMime($file));
				$basename = basename($file);
				if ($mime=='image') {
					$image = Image::getName($basename);
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        $ukuran = strlen($file);
				        $isi_file = file_get_contents($file);
						$hash = hash_hmac('sha256', $isi_file.$this->config->item('salt_key'), SessionManagerWeb::getUserID());
						if($this->model->checkUniqeDocument($hash)){
							echo "true";
							exit;
						}else{
							echo "false";
							exit;
						}
                    }
                } else if ($mime=='video') {
                	$fil = $basename;
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;
                        $ukuran = strlen($file);
				        $isi_file = file_get_contents($file);
						$hash = hash_hmac('sha256', $isi_file.$this->config->item('salt_key'), SessionManagerWeb::getUserID());
						if($this->model->checkUniqeDocument($hash)){
							echo "true";
							exit;
						}else{
							echo "false";
							exit;
						}
                    }
                }
                else {
					// $fil = File::getName($basename);
					$fil = $basename;
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;
                        $ukuran = strlen($file);
				        $isi_file = file_get_contents($file);
						$hash = hash_hmac('sha256', $isi_file.$this->config->item('salt_key'), SessionManagerWeb::getUserID());
						if($this->model->checkUniqeDocument($hash)){
							echo "true";
							exit;
						}else{
							echo "false";
							exit;
						}
                    }
                }
                if (!rename($file, $path_dest . '/' . $basename)) {
                    echo "false";
                    exit;
                }
            }
        }else {
			echo "true";
			exit;
        }
		echo "false";
		exit;
    }

    //Delete Dokumen
    public function deleteDocument(){
    	$id = $this->input->post('id_dokumen',true);
    	$data = array(
    		'is_delete' => 1
    	);
    	die($this->model->update($id, $data, TRUE));
    	// die($this->model->softDeleteDocument($id));
    }

    public function view($name){
    	redirect(site_url("/ViewPDF/web/viewer.html")."?file=".site_url("/web/thumb/files/$name"));
    	// redirect(site_url("/ViewPDF/web/viewer.html?file=".site_url("/web/thumb/files/$name")));
    }

    public function view_watermark($name){
    	redirect(site_url("/ViewPDF/web/viewer.html")."?file=".site_url("/web/thumb/files_watermark/$name"));
    }

	//----------- Mengambil kapasitas drive server ------------------------
    public function coba(){
    	// $df = disk_free_space("/");
    	// $ds = disk_total_space("/");
    	// $symbols = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
	    // $exp = floor(log($df)/log(1024));

	    // echo $df . " ";
    	// echo sprintf('%.2f '.$symbol[$exp], ($df/pow(1024, floor($exp))));
    	// echo $ds;

  //   	$somePath = "C:/";
  //   	$dirs = glob($somePath . '/*' , GLOB_ONLYDIR);
		// echo json_encode( $dirs);

		$device = array();
		$device['secure_id'] = "1234567890";
		$device['name'] = "A_PHONE";
		$device['brand'] = "SAMSUNG";
		$device['manufacture'] = "SAMSUNG";
		$device['model'] = "J10";
		$device['product'] = "mobile";
		$device['serial'] = "ABC123";
		$device['hardware'] = "SAMSUNG";
		$device['version'] = "6.1";
		$device['reg_id'] = "fszeA0138mmmwekz77";
		echo json_encode($device);
    }

    //-------------------------------- Bagian Tag -------------------------------------
    public function tag(){
    	$this->load->model('Tag_model');
    	if($this->input->post('searchTag',true)!=null){
    		$filter = $this->input->post('searchTag',true);
    		$this->data['filter'] = $filter;
    	}
    	else{
    		$filter = "_#abc#_";//random char
    		$this->data['filter'] = "";
    	}
    	$tags = $this->Tag_model->getTags();
		$temp = 0;
		$numDocs = array();
		$filteredTags = array();
		if(strcmp($filter,"_#abc#_")!=0){
			foreach($tags as $obj){
				$pos = strpos(strtolower($obj['name']),$filter);
				if($pos !== false){
					$num = $this->Tag_model->getDocWithTag($obj['name']);
					if($num > 0){
						$numDocs[$temp] = $num;
						$filteredTags[$temp] = $obj;
						$temp++;
					}
				}
			}
		}else{
			foreach($tags as $obj){
				$num = $this->Tag_model->getDocWithTag($obj['name']);
				if($num > 0){
					$numDocs[$temp] = $num;
					$filteredTags[$temp] = $obj;
					$temp++;
				}
			}
		}
		$this->data['NumDoc'] = $numDocs;
		$this->data['Tags'] = $filteredTags;
		if($this->input->post('pageNow',true)!=null)
			$this->data['pageNow'] = $this->input->post('pageNow',true);
		else
			$this->data['pageNow'] = 1;
		$this->data['NumTags'] = sizeof($filteredTags);
		$this->data['role'] = SessionManagerWeb::getRole();
		$listOrganisasi = $this->model->listOrganisasi();
		$tree = $this->createNodes($listOrganisasi,-1);
		unset($tree["arr"]);
		$this->data['tree'] = $tree["st"];
		$this->data['isShowTree'] = true;
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function setSessionStatistik($keyword=NULL, $value=NULL){
    	if ($keyword!=NULL and $value!=NULL){
    		$_SESSION['dms_semen_indonesia']['statistik'][$keyword] = $value;
    	}
    	redirect("web/document/statistik");
    }

    public function statistik($session=0){
    	if ($session==1){
    		unset($_SESSION['dms_semen_indonesia']['statistik']);
    		redirect("web/document/statistik");
    	}
    	if (SessionManagerWeb::isDocumentController() or SessionManagerWeb::isAdministrator()){

    		$this->load->model("Document_model");
    		$this->load->model("Workflow_model");
    		$this->load->model("Statistic_documents_model");

    		// cek filternya
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'] = Workflow_model::PENGAJUAN;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'] = 1;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] = 'DEPT';
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow']= Workflow_model::PENGAJUAN;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'] = 1;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] = 'DEPT';
    		}

    		$filter = $_SESSION['dms_semen_indonesia']['statistik'];
    		
    		$this->data['tasks']= $this->Workflow_model->getTasks(Workflow_model::PENGAJUAN);
    		$color = array(
    			"red-prog",
    			"purple-prog",
    			"blue-prog",
    			"green-prog",
    			"yellow-prog",
    			"orange-prog",
    			"brown-prog"

    		);
    		$this->data['color'] = $color;
    		$workflow_id = Workflow_model::PENGAJUAN;
    		$stats = $this->Statistic_documents_model->filter("where type='".Statistic_documents_model::PENDING_PROCESS."' and workflow_id=$workflow_id")->order("order by jumlah_hari desc")->getAll();

    		// get Type
    		$this->load->model("Document_types_model");
    		$all_types = $this->Document_types_model->getAllJenis();
    		$types = Util::toMap($all_types, 'id', 'type');
    		// $types[0] = "All";
    		ksort($types);
    		$this->data['type_statistics'] = $types;

    		// get flow pending process
    		$this->data['pending_process']['flow'] = array(
    			Workflow_model::PENGAJUAN =>"Submission",
    			Workflow_model::REVISI =>"Revision"
    		);

    		// get Flow lead time
    		$this->data['lead_time']['flow'] = array(
    			Workflow_model::PENGAJUAN =>"Submission",
    			Workflow_model::EVALUASI =>"Evaluation",
    			Workflow_model::REVISI =>"Revision"
    		);

    		//get UnitKerja
    		$this->load->model('Unitkerja_model');
    		$this->data['pending_process']['workunit'] = array(
    			'DEPT' => 'Department',
    			'BIRO' => 'Bureau',
    			'SECT' => 'Section',
    		);
    		$this->data['lead_time']['workunit'] = $this->data['pending_process']['workunit'];

    		$this->load->model('Statistic_model');
    		$dataStatistik_p = array();
    		$dataStatistik_l = array();
    		// if($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'DEPT'){
    		//////////////////PENDING PROCESS//////////////////////////////////////////////
			// get yang memang benar2 dari filter workunit
			$dataStatistik_p_workunit =$this->Statistic_model->getDataStatistic($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'],$_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'],'P', 'unitkerja_id', $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'],ltrim(SessionManagerWeb::getCompany(),"0")); // hanya akan keluar per 1 unitkerja_id dengan syarat ini.(tidak akan muncul 2 row dengan unitkerja_id yang sama)
			if($dataStatistik_p_workunit){
				foreach ($dataStatistik_p_workunit as $workunit) {
					$dataUrutan = json_decode($workunit['URUTAN'], true);
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['UNITKERJA_ID'] = $workunit['UNITKERJA_ID'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['NAME'] = $workunit['NAME'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['WORKFLOW_ID'] = $workunit['WORKFLOW_ID'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['DOC_TYPE'] = $workunit['DOC_TYPE'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['DATA_URUTAN'] = $dataUrutan;
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['JUMLAH_DATA'] = 1;
				}
				
			}
			// get dari parent(biro, dept)_idnya
			if($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'DEPT' || $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'BIRO'){
				$column = $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'].'_ID';
				$dataStatistik_p_unworkunit = $this->Statistic_model->groupByStatistik($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],$_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'], 'P',ltrim(SessionManagerWeb::getCompany(),"0") );
				if($dataStatistik_p_unworkunit){
					foreach ($dataStatistik_p_unworkunit as $unworkunit) {
						$dataUrutan = json_decode($unworkunit['URUTAN'], true);
						if(isset($dataStatistik_p[$unworkunit[$column]])){ // Jika data dari workunit pada array P ada
							for ($i=0; $i <= 6 ; $i++) { // perulangan dari urutan
								$dataStatistik_p[$unworkunit[$column]]['DATA_URUTAN'][$i] += $dataUrutan[$i];
							}
							$dataStatistik_p[$unworkunit[$column]]['JUMLAH_DATA'] += 1;
						}
						// Membuat data baru dari dept/biro yang tidak mempunyai data di statistic tapi bawahannya punya
						else{
							$dataStatistik_p[$unworkunit[$column]]['UNITKERJA_ID'] = $unworkunit[$column];
							$dataStatistik_p[$unworkunit[$column]]['NAME'] = $unworkunit['NAME'];
							$dataStatistik_p[$unworkunit[$column]]['WORKFLOW_ID'] = $unworkunit['WORKFLOW_ID'];
							$dataStatistik_p[$unworkunit[$column]]['DOC_TYPE'] = $unworkunit['DOC_TYPE'];
							$dataStatistik_p[$unworkunit[$column]]['DATA_URUTAN'] = $dataUrutan;
							$dataStatistik_p[$unworkunit[$column]]['JUMLAH_DATA'] = 1;
						}
					}
				}
			}
			
				
			// echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
			// echo "a";
			// die();
    		// }
    		
    		// echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
    		//////////////////LEAD TIME//////////////////////////////////////////////
			// get sesuai filter workunit
			// echo "ini dari luar=".$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type']);
    		$dataStatistik_l_workunit =$this->Statistic_model->getDataStatistic($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],'L', 'unitkerja_id', $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'],ltrim(SessionManagerWeb::getCompany(),"0"));
    		if($dataStatistik_l_workunit){
    			foreach ($dataStatistik_l_workunit as $workunit) {
    				$dataUrutan = json_decode($workunit['URUTAN'], true);
    				$dataStatistik_l[$workunit['UNITKERJA_ID']]['UNITKERJA_ID'] = $workunit['UNITKERJA_ID'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['NAME'] = $workunit['NAME'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['WORKFLOW_ID'] = $workunit['WORKFLOW_ID'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['DOC_TYPE'] = $workunit['DOC_TYPE'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['DATA_URUTAN'] = $dataUrutan;
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['JUMLAH_DATA'] = 1;
    			}
    		}

    		// get dari parent(biro / dept)_idnya
    		if($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] == 'DEPT' || $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] == 'BIRO'){
				$column = $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'].'_ID';
				$dataStatistik_l_unworkunit = $this->Statistic_model->groupByStatistik($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'], 'L', ltrim(SessionManagerWeb::getCompany(),"0"));
				if($dataStatistik_l_unworkunit){
					foreach ($dataStatistik_l_unworkunit as $unworkunit) {
						$dataUrutan = json_decode($unworkunit['URUTAN'], true);
						// echo "<pre>";print_r($dataUrutan);echo "</pre>";
						if(isset($dataStatistik_l[$unworkunit[$column]])){
							// echo "masuk if<br>";
							for ($i=0; $i <= 6 ; $i++) { 
								$dataStatistik_l[$unworkunit[$column]]['DATA_URUTAN'][$i] += $dataUrutan[$i];
							}
							$dataStatistik_l[$unworkunit[$column]]['JUMLAH_DATA'] += 1;
						}
						else{
							$dataStatistik_l[$unworkunit[$column]]['UNITKERJA_ID'] = $unworkunit[$column];
							$dataStatistik_l[$unworkunit[$column]]['NAME'] = $unworkunit['NAME'];
							$dataStatistik_l[$unworkunit[$column]]['WORKFLOW_ID'] = $unworkunit['WORKFLOW_ID'];
							$dataStatistik_l[$unworkunit[$column]]['DOC_TYPE'] = $unworkunit['DOC_TYPE'];
							$dataStatistik_l[$unworkunit[$column]]['DATA_URUTAN'] = $dataUrutan;
							$dataStatistik_l[$unworkunit[$column]]['JUMLAH_DATA'] = 1;
						}
					}
				}

    		}
    		
    		
    		// echo "<pre>";print_r($dataStatistik_l);echo "</pre>";

    		// $dataDept = "SELECT S.DEPT_ID, U.NAME FROM STATISTIC_DOCUMENTS S LEFT JOIN UNITKERJA U ON S.DEPT_ID = U.ID GROUP BY S.DEPT_ID, U.NAME ORDER BY S.DEPT_ID";
    		// $getData = dbGetRows($dataDept);

    		// // echo "<pre>";print_r($getData);echo "</pre>";
    		// foreach ($getData as $data) {
    		// 	$dataStatistik_p[$data['DEPT_ID']]['name'] = $data['NAME'];
    		// 	for ($i=1; $i <=5 ; $i++) { 
    		// 		for ($j=1; $j<=7 ; $j++) {
    		// 			if($j >= 2){
    		// 				if($j == 2){
    		// 					$getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=2 AND ACTOR='E'";
    		// 				}
    		// 				elseif($j == 3){
    		// 					$getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=2 AND ACTOR='R'";
    		// 				}
    		// 				else{
    		// 					$urutan = $j-1;
    		// 					$getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=".$urutan;
    		// 				}
    		// 			}
    		// 			else{
    		// 				$getStatistik = "SELECT MAX(JUMLAH_HARI) AS JUM FROM STATISTIC_DOCUMENTS WHERE DEPT_ID=".$data['DEPT_ID']." AND WORKFLOW_ID=".$i." AND WORKFLOW_URUTAN=".$j;
	    					
    		// 			}
    		// 			// echo $getStatistik."<br>";
    		// 			$getStat = dbGetOne($getStatistik);
	    	// 			if($getStat == null || $getStat == ''){
	    	// 				$dataStatistik_p[$data['DEPT_ID']][$i][$j] = 0;
	    	// 			}
	    	// 			else{
	    	// 				$dataStatistik_p[$data['DEPT_ID']][$i][$j] = $getStat;
	    	// 			}
    					
    					
    		// 		}
    		// 	}
    		// }
    		// echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
    		// echo "<pre>";print_r($dataStatistik_l);echo "</pre>";
    		$total_data_p = 0;
    		$total_data_l = 0;
    		foreach ($dataStatistik_p as $p) {
    			$total_data_p++;
    			for ($i=0; $i <=6 ; $i++) { 
    				$p['DATA_URUTAN'][$i] = floor($p['DATA_URUTAN'][$i] / $p['JUMLAH_DATA']);
    			}
    		}
    		foreach ($dataStatistik_l as $l) {
    			$total_data_l++;
    			for ($i=0; $i <=6 ; $i++) { 
    				$l['DATA_URUTAN'][$i] = floor($l['DATA_URUTAN'][$i] / $l['JUMLAH_DATA']);
    			}
    		}
    		// // $this->dashboard();
    		$this->data['dataStatistik_p'] = $dataStatistik_p;
    		$this->data['dataStatistik_l'] = $dataStatistik_l;
    		$this->data['total_data_p'] = $total_data_p;
    		$this->data['total_data_l'] = $total_data_l;
    		// // echo "<pre>";print_r($this->data['dataStatistik_p']);echo "</pre>";
    		// die();

    	}else{
    		// redirect("web/document/dashboard");
    	}
    	// echo $_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'];
	// echo "<pre>";print_r($this->data);echo "</pre>";die();
	// echo "<pre>";print_r($_SESSION['dms_semen_indonesia']);echo "</pre>";	
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function statistik_text($session=0){
    	if ($session==1){
    		unset($_SESSION['dms_semen_indonesia']['statistik']);
    		redirect("web/document/statistik");
    	}
    	if (SessionManagerWeb::isDocumentController() or SessionManagerWeb::isAdministrator()){

    		$this->load->model("Document_model");
    		$this->load->model("Workflow_model");
    		$this->load->model("Statistic_documents_model");

    		// cek filternya
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'] = Workflow_model::PENGAJUAN;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'] = 1;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] = 'DEPT';
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow']= Workflow_model::PENGAJUAN;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'] = 1;
    		}
    		if (!isset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'])){
    			$_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] = 'DEPT';
    		}

    		$filter = $_SESSION['dms_semen_indonesia']['statistik'];
    		
    		$this->data['tasks']= $this->Workflow_model->getTasks(Workflow_model::PENGAJUAN);
    		$color = array(
    			"red-prog",
    			"purple-prog",
    			"blue-prog",
    			"green-prog",
    			"yellow-prog",
    			"orange-prog",
    			"brown-prog"

    		);
    		$this->data['color'] = $color;
    		$workflow_id = Workflow_model::PENGAJUAN;
    		$stats = $this->Statistic_documents_model->filter("where type='".Statistic_documents_model::PENDING_PROCESS."' and workflow_id=$workflow_id")->order("order by jumlah_hari desc")->getAll();

    		// get Type
    		$this->load->model("Document_types_model");
    		$all_types = $this->Document_types_model->getAllJenis();
    		$types = Util::toMap($all_types, 'id', 'type');
    		// $types[0] = "All";
    		ksort($types);
    		$this->data['type_statistics'] = $types;

    		// get flow pending process
    		$this->data['pending_process']['flow'] = array(
    			Workflow_model::PENGAJUAN =>"Submission",
    			Workflow_model::REVISI =>"Revision"
    		);

    		// get Flow lead time
    		$this->data['lead_time']['flow'] = array(
    			Workflow_model::PENGAJUAN =>"Submission",
    			Workflow_model::EVALUASI =>"Evaluation",
    			Workflow_model::REVISI =>"Revision"
    		);

    		//get UnitKerja
    		$this->load->model('Unitkerja_model');
    		$this->data['pending_process']['workunit'] = array(
    			'DEPT' => 'Department',
    			'BIRO' => 'Bureau',
    			'SECT' => 'Section',
    		);
    		$this->data['lead_time']['workunit'] = $this->data['pending_process']['workunit'];

    		$this->load->model('Statistic_model');
    		$dataStatistik_p = array();
    		$dataStatistik_l = array();
    		// if($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'DEPT'){
    		//////////////////PENDING PROCESS//////////////////////////////////////////////
			// get yang memang benar2 dari filter workunit
			$dataStatistik_p_workunit =$this->Statistic_model->getDataStatistic($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'],$_SESSION['dms_semen_indonesia']['statistik']['pending_process_type'],'P', 'unitkerja_id', $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'],ltrim(SessionManagerWeb::getCompany(),"0")); // hanya akan keluar per 1 unitkerja_id dengan syarat ini.(tidak akan muncul 2 row dengan unitkerja_id yang sama)
			if($dataStatistik_p_workunit){
				echo "if p workunit<br>";
				echo "<pre>";print_r($dataStatistik_p_workunit);echo "</pre>";
				foreach ($dataStatistik_p_workunit as $workunit) {
					echo "masuk foreach<br>";
					$dataUrutan = json_decode($workunit['URUTAN'], true);
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['UNITKERJA_ID'] = $workunit['UNITKERJA_ID'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['NAME'] = $workunit['NAME'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['WORKFLOW_ID'] = $workunit['WORKFLOW_ID'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['DOC_TYPE'] = $workunit['DOC_TYPE'];
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['DATA_URUTAN'] = $dataUrutan;
					$dataStatistik_p[$workunit['UNITKERJA_ID']]['JUMLAH_DATA'] = 1;
				}
				
			}
			// get dari parent(biro, dept)_idnya
			if($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'DEPT' || $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'] == 'BIRO'){
				echo "if get parent<br>";
				// echo "<pre>";print_r($dataStatistik_p_workunit);echo "</pre>";
				$column = $_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'].'_ID';
				$dataStatistik_p_unworkunit = $this->Statistic_model->groupByStatistik($_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],$_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'], 'P',ltrim(SessionManagerWeb::getCompany(),"0") );
				if($dataStatistik_p_unworkunit){
					echo "if p data unworkunit<br>";
					echo "<pre>";print_r($dataStatistik_p_unworkunit);echo "</pre>";
					foreach ($dataStatistik_p_unworkunit as $unworkunit) {
						$dataUrutan = json_decode($unworkunit['URUTAN'], true);
						if(isset($dataStatistik_p[$unworkunit[$column]])){ // Jika data dari workunit pada array P ada
							for ($i=0; $i <= 6 ; $i++) { // perulangan dari urutan
								$dataStatistik_p[$unworkunit[$column]]['DATA_URUTAN'][$i] += $dataUrutan[$i];
							}
							$dataStatistik_p[$unworkunit[$column]]['JUMLAH_DATA'] += 1;
						}
						// Membuat data baru dari dept/biro yang tidak mempunyai data di statistic tapi bawahannya punya
						else{
							$dataStatistik_p[$unworkunit[$column]]['UNITKERJA_ID'] = $unworkunit[$column];
							$dataStatistik_p[$unworkunit[$column]]['NAME'] = $unworkunit['NAME'];
							$dataStatistik_p[$unworkunit[$column]]['WORKFLOW_ID'] = $unworkunit['WORKFLOW_ID'];
							$dataStatistik_p[$unworkunit[$column]]['DOC_TYPE'] = $unworkunit['DOC_TYPE'];
							$dataStatistik_p[$unworkunit[$column]]['DATA_URUTAN'] = $dataUrutan;
							$dataStatistik_p[$unworkunit[$column]]['JUMLAH_DATA'] = 1;
						}
					}
				}
			}
			echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
			
				
			// echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
			// echo "a";
			// die();
    		// }
    		
    		// echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
    		//////////////////LEAD TIME//////////////////////////////////////////////
			// get sesuai filter workunit
			// echo "ini dari luar=".$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type']);
    		$dataStatistik_l_workunit =$this->Statistic_model->getDataStatistic($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],'L', 'unitkerja_id', $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'],ltrim(SessionManagerWeb::getCompany(),"0"));
    		if($dataStatistik_l_workunit){
    			echo "if l workunit<br>";
				echo "<pre>";print_r($dataStatistik_l_workunit);echo "</pre>";
    			foreach ($dataStatistik_l_workunit as $workunit) {
    				$dataUrutan = json_decode($workunit['URUTAN'], true);
    				$dataStatistik_l[$workunit['UNITKERJA_ID']]['UNITKERJA_ID'] = $workunit['UNITKERJA_ID'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['NAME'] = $workunit['NAME'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['WORKFLOW_ID'] = $workunit['WORKFLOW_ID'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['DOC_TYPE'] = $workunit['DOC_TYPE'];
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['DATA_URUTAN'] = $dataUrutan;
					$dataStatistik_l[$workunit['UNITKERJA_ID']]['JUMLAH_DATA'] = 1;
    			}
    		}

    		// get dari parent(biro / dept)_idnya
    		if($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] == 'DEPT' || $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'] == 'BIRO'){
				echo "if get parent<br>";
				$column = $_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'].'_ID';
				$dataStatistik_l_unworkunit = $this->Statistic_model->groupByStatistik($_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type'],$_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow'], 'L', ltrim(SessionManagerWeb::getCompany(),"0"));
				if($dataStatistik_l_unworkunit){
					echo "if p data unworkunit<br>";
					echo "<pre>";print_r($dataStatistik_p_unworkunit);echo "</pre>";
					foreach ($dataStatistik_l_unworkunit as $unworkunit) {
						$dataUrutan = json_decode($unworkunit['URUTAN'], true);
						// echo "<pre>";print_r($dataUrutan);echo "</pre>";
						if(isset($dataStatistik_l[$unworkunit[$column]])){
							// echo "masuk if<br>";
							for ($i=0; $i <= 6 ; $i++) { 
								$dataStatistik_l[$unworkunit[$column]]['DATA_URUTAN'][$i] += $dataUrutan[$i];
							}
							$dataStatistik_l[$unworkunit[$column]]['JUMLAH_DATA'] += 1;
						}
						else{
							$dataStatistik_l[$unworkunit[$column]]['UNITKERJA_ID'] = $unworkunit[$column];
							$dataStatistik_l[$unworkunit[$column]]['NAME'] = $unworkunit['NAME'];
							$dataStatistik_l[$unworkunit[$column]]['WORKFLOW_ID'] = $unworkunit['WORKFLOW_ID'];
							$dataStatistik_l[$unworkunit[$column]]['DOC_TYPE'] = $unworkunit['DOC_TYPE'];
							$dataStatistik_l[$unworkunit[$column]]['DATA_URUTAN'] = $dataUrutan;
							$dataStatistik_l[$unworkunit[$column]]['JUMLAH_DATA'] = 1;
						}
					}
				}

    		}
    		echo "<pre>";print_r($dataStatistik_l);echo "</pre>";
    		
    		
    		
    		// echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
    		// echo "<pre>";print_r($dataStatistik_l);echo "</pre>";
    		foreach ($dataStatistik_p as $p) {
    			for ($i=0; $i <=6 ; $i++) { 
    				$p['DATA_URUTAN'][$i] = floor($p['DATA_URUTAN'][$i] / $p['JUMLAH_DATA']);
    			}
    		}
    		foreach ($dataStatistik_l as $l) {
    			for ($i=0; $i <=6 ; $i++) { 
    				$l['DATA_URUTAN'][$i] = floor($l['DATA_URUTAN'][$i] / $l['JUMLAH_DATA']);
    			}
    		}
    		// // $this->dashboard();
    		$this->data['dataStatistik_p'] = $dataStatistik_p;
    		$this->data['dataStatistik_l'] = $dataStatistik_l;
    		echo "<pre>";print_r($this->data['dataStatistik_p']);echo "</pre>";
    		echo "<pre>";print_r($this->data['dataStatistik_l']);echo "</pre>";
    		die();

    	}else{
    		// redirect("web/document/dashboard");
    	}
    	// echo $_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow'];
	// echo "<pre>";print_r($this->data);echo "</pre>";
	// echo "<pre>";print_r($_SESSION['dms_semen_indonesia']);echo "</pre>";	
    	// );
    }

    public function dashboardSetSession(){
    	$_SESSION['document_search']['show_by'] = 'Need Action';
    	header('Location: ' . site_url('web/document/dashboard'));
    }

    public function dashboard($page=0){
    	unset($_SESSION['document_search']);
    	$filter = $this->getFilter($filter);
    	// $this->data['page'] = $page;
    	$this->load->model('Workflow_model', 'Workflow');
    	// $tasks = $this->Workflow->getTasks();
    	$role = SessionManagerWeb::getRole();
    	// $data = $this->model->filter($filter)->page($page)->getDocumentNeedAction();
    	$data = $this->model->filter($filter." and \"status\"!='C' ")->getDocumentNeedAction(2, SessionManagerWeb::getUserID(),SessionManagerWeb::getRole(),SessionManagerWeb::getCompany());
    	// echo "<pre>";print_r($data);die();
    	$this->data['total_document'] = $data['total_document'];
    	unset($data['total_document']);
    	$this->data['total_all_document'] = $data['total_all_document'];
    	unset($data['total_all_document']);
    	$this->data['need_action'] = $data['need_action'];
    	unset($data['need_action']);
    	$this->data['in_process'] = $data['in_process'];
    	unset($data['in_process']);
    	$this->data['next'] = $data['next'];
    	unset($data['next']);

    	// Rejected Document
    	$rejected_documents = $this->model->filter(" and \"status\"='C' ")->getDocumentNeedAction(2, SessionManagerWeb::getUserID(),SessionManagerWeb::getRole(),SessionManagerWeb::getCompany());
    	// echo "<pre>";print_r($rejected_documents);die();
    	$this->data['rejected_document_counter'] = $rejected_documents['total_all_document'];
    	unset($rejected_documents['total_document']);
    	unset($rejected_documents['total_all_document']);
    	unset($rejected_documents['need_action']);
    	unset($rejected_documents['in_process']);
    	unset($rejected_documents['next']);
    	$this->data['rejected_documents'] = $rejected_documents;
    	
    	// Ask Download
    	$this->load->model('Document_download_model');
    	// if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()){
    	if (SessionManagerWeb::isDocumentController()){
    		$filter = " and (user_id=".SessionManagerWeb::getUserID()." or is_allowed=0) ";
    	} else {
    		$filter = " and user_id=".SessionManagerWeb::getuserID();
    	}

    	$filter .= " and (valid_until>sysdate or valid_until is null) ";

    	$downloads = $this->Document_download_model->getAll($filter);
    	// echo '<pre>';
    	// var_dump($downloads);
    	// die();
    	$download_counter = 0;
    	if ($downloads){
    		$this->load->model('Document_types_model');
    		$this->load->model('User_model');
    		$this->load->model('Unitkerja_model');
    		foreach ($downloads as $k_download => $v_download) {
    			if (SessionManagerWeb::isDocumentController()){
    				$company = $this->User_model->filter(" where \"user_id\"='".SessionManagerWeb::getUserID()."' ")->getOneFilter("company");
    				$unitkerja_document = $this->model->getOne($v_download['document_id'], "unitkerja_id");
    				$company_document = $this->Unitkerja_model->getOne($unitkerja_document, "company");

    				if ($company!=$company_document){
    					unset($downloads[$k_download]);
    					continue;
    				}
    			}
    			$download_counter++;
    			$downloads[$k_download]['requestor'] = $this->User_model->getBy($v_download['user_id'], 'name');
    			$downloads[$k_download]['requestor_workunit'] = $this->User_model->getBy($v_download['user_id'], 'unitkerja_name');
    			$workunit = $this->model->getOne($v_download['document_id'], 'unitkerja_id');
    			$downloads[$k_download]['document_workunit'] = $this->Unitkerja_model->getOneBy($workunit, 'name');
    			$downloads[$k_download]['name'] = $this->model->getOne($v_download['document_id'], 'title');
    			$type = $this->model->getOne($v_download['document_id'], 'type_id');
    			$downloads[$k_download]['type_name'] = $this->Document_types_model->filter(" id=$type ")->getOne('type');
    		}
    	}
    	$this->data['downloads'] = $downloads;
    	$this->data['download_counter'] = $download_counter;

    	// Change Workunit
    	$workunit_change = $this->model->getAllWorkunitChange(SessionManagerWeb::getUserID());
    	$this->data['workunit_change'] = $workunit_change;
    	if ($workunit_change){
    		$this->data['workunit_change_counter'] = count($workunit_change);
    	} else {
    		$this->data['workunit_change_counter'] = 0;
    	}
    	// echo '<pre>';
    	// vaR_dump($workunit_change);
    	// die();

    	$this->data['data'] = $data;
    	$this->data['variables'] = SessionManagerWeb::getVariables();
    	
    	$mycompany = $this->data['variables']['mycompany'];
    	// $filter = " where \"show\"='1' and \"status\"='D' and rownum<=5 and \"code\" like '%/$mycompany/%' ";
    	// $filter = " where \"show\"='1' and \"status\"='D' and rownum<=5 ";
    	$filter = " where \"show\"='1' and \"is_published\"=1 and rownum<=5 and \"company\"='".ltrim(SessionManagerWeb::getCompany(), '0')."' ";
    	$this->data['newest_doc'] = $this->model->filter($filter)->getDocumentOld();
    	$company = str_pad(SessionManagerWeb::getCompany(), 8, "0", STR_PAD_LEFT);
    	$this->data['total_type'] = array_change_key_case($this->model->getStatistikTotalType($company),CASE_LOWER);
    	// echo '<pre>';
    	// vaR_dump($this->data['variables']);
    	// die();
    	// $this->setVariables();
    	$this->load->model('document_model');
    	$this->data['document_popular'] = $this->document_model->getDocumentPopuler();
    	$this->load->model('Statistic_model');
        $this->data['dataVisitor'] = $this->Statistic_model->getDataVisitor();


        $this->load->model('Statistik_unitkerja_model');
        if (!isset($_SESSION['page_evaluation'])){
	 		 $_SESSION['page_evaluation'] = 1;
	 	}
       	$getCompany = ltrim(SessionManagerWeb::getCompany(), '0');
    	$this->data['PendingDocEvaluation'] = $this->Statistik_unitkerja_model->getStatistikEvaluation($getCompany,  $_SESSION['page_evaluation']);
        if ($_SESSION['page_evaluation'] == 1){
        	$_SESSION['max_evaluation'] = $this->data['PendingDocEvaluation']['row'][0]['PENDING_TOTAL'];
        }
        // $this->data['PendingDocEvaluation'] = $this->Statistik_unitkerja_model->getStatistik('E');
        // echo "<pre>";print_r($this->data);echo "</pre>";die();
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function download_detail($id){
		$buttons = array();
        $buttons[] = array('label' => ' Back', 'type' => 'success', 'icon' => 'chevron-left', 'click' => 'goBack()' );
        $this->data['buttons'] = $buttons;

    	$this->load->model('Document_download_model');
    	$this->load->model('User_model');
    	$download = $this->Document_download_model->getBy($id);
    	if ($download==NULL){
    		SessionManagerWeb::setFlashMsg(false,"Download request not found!");
    		redirect("web/document/dashboard");
    	}
    	$this->data['download'] = $download;
    	$this->data['requestor'] = $this->User_model->getByAll($download['user_id']);
    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(),$download['document_id']);
    	// $this->title = 
    	$this->data['title'] = $document['type_name'].' "'.$document['title'].'"';
    	$document['creator_name'] = $this->User_model->getBy($document['creator_id'], 'name');
    	$document['approver_name'] = $this->User_model->getBy($document['approver_id'], 'name');
    	$this->data['document'] = $document;
    	// echo '<pre>';
    	// vaR_dump($this->data['download']);
    	// die();
    	if (SessionManagerWeb::isDocumentController()){
    		$this->load->model('Unitkerja_model');

    		$this->data['is_allow_approve'] = 1;

			$company = $this->User_model->filter(" where \"user_id\"='".SessionManagerWeb::getUserID()."' ")->getOneFilter("company");
			$unitkerja_document = $this->model->getOne($download['document_id'], "unitkerja_id");
			$company_document = $this->Unitkerja_model->getOne($unitkerja_document, "company");

			if ($company!=$company_document){
				$this->data['is_allow_approve']=0;
				SessionManagerWeb::setFlashMsg(false, "This document is not belongs to your company, you cant  approve or reject this download request");
			}
		}
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function approve_download($id){
    	$data = array();
    	$data['is_allowed'] = 1;
    	$this->load->model("Document_download_model");
    	$download = $this->Document_download_model->getBy($id);
    	$this->load->model('User_model');
    	$user = $this->User_model->getBy($download['user_id'], 'name');
    	$this->addActivity($download['document_id'], "Approved download request from ".$user);
    	if (dbUpdate('document_download', $data, "id=$id")){
    		if ($this->Document_download_model->updateColumn(" valid_until = sysdate+3 ", $id)){
    			$this->load->model('Notification_model');
		        $this->Notification_model->generate(Notification_model::ACTION_ASK_DOWNLOAD, $id, SessionManagerWeb::getUserID());
    		}
    	}
    	SessionManagerWeb::setFlashMsg(true, "Success to Approve Download Request");
    	redirect('web/document/dashboard');
    }

    public function setDownloadReason($id){
    	$this->load->model('Document_download_model');
    	$data = array();
    	$data['reject_reason'] = $this->input->post('reason_textarea', true);
    	$data['is_allowed'] = -1;
    	if (dbUpdate('document_download', $data, "id=$id")){
    		$this->load->model('Notification_model');
	        $this->Notification_model->generate(Notification_model::ACTION_ASK_DOWNLOAD, $id, SessionManagerWeb::getUserID());
    		SessionManagerWeb::setFlashMsg(true, 'Success to Reject Document');
    		redirect('web/document/download_detail/'.$id);
    	}
    }

    private function setVariables(){
    	// Proses Bisnis
    	$this->load->model('Document_prosesbisnis_model');
    	$this->data['prosesbisnis'] = Util::toMap($this->Document_prosesbisnis_model->getAll(), 'id', 'prosesbisnis');

    	// jenis doc
    	$this->load->model('Document_types_model');
    	$this->load->model('Tingkatan_document_types_model');
  //   	if (SessionManagerWeb::isAdministrator()) {
		// 	$jenis = $this->Document_types_model->getAllJenis();
		// } else {
			$this->load->model('Tingkatan_document_types_model');
			$tingkatan = $this->User_model->getJabatanGroup(SessionManagerWeb::getUserID());
			$tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
			$jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

		// }
		$this->data['jenis'] =  $jenis;

    	// Reviewer, Approver, Unitkerja
    	$this->load->model('User_model');
    	$user = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'unitkerja_id');
		// $this->data['reviewer'] = Util::toMap($this->User_model->getAllBy("\"role\"='".Role::REVIEWER."'"), 'user_id', 'name'); 
		$this->data['reviewer'] = Util::toMap($this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\""))->getAll(), 'user_id', 'name');
		$this->data['unitkerja'] =$this->Unitkerja_model->order('name')->getAllList();
		$myunitkerja = $this->User_model->getBy(SessionManagerWeb::getUserID(),'unitkerja_id');
		$this->data['myunitkerja'] =$this->Unitkerja_model->order('name')->getMyUnitkerjaList($myunitkerja);

		// Iso
		$this->load->model('Document_iso_model');
		$this->data['document_iso'] = $this->Document_iso_model->getAll();

    }

    public function detail($id){
    	if ($_GET){
    		$_SESSION['edit_evaluation'] = $this->input->get('edit_evaluation', null);
    		unset($_GET);
    		redirect("web/document/detail/".$id);
    	} 

    	$this->load->model('Workflow_model');
    	$buttons = array();
        $buttons[] = array('label' => ' Back', 'type' => 'success', 'icon' => 'chevron-left', 'click' => 'goBack()' );
        $this->data['buttons'] = $buttons;

        $ciConfig = $this->config->item('utils');
		$path = $ciConfig['full_upload_dir'].'document/files/';

        $this->data['is_allow_edit'] = FALSE;

    	$this->data['data'] = $this->model->getDocumentBy(SessionManagerWeb::getUserID(), $id);

    	// Get Type
    	$this->load->model('Document_types_model');
    	$this->data['type'] = $this->Document_types_model->get($this->data['data']['type_id']);
    	$this->data['type']['type_form'] = Document_types_model::FORM;
    	// Cek ISO
    	$this->data['data']['iso'] = $this->model->getDocumentISOS($id);

    	// Cek Unitkerja
    	$this->load->model('Document_unitkerja_model');
    	$this->data['data']['unitkerja'] = $this->Document_unitkerja_model->getAllBy($this->data['data']['document_id']);

    	// Cek ProsesBisnis
    	$this->load->model('Document_prosesbisnis_model');
    	$this->data['data']['prosesbisnis_name'] = $this->Document_prosesbisnis_model->getOne($this->data['data']['prosesbisnis_id'], 'prosesbisnis');

    	// Cek Ukuran
    	// $hash = hash_hmac('sha256',$this->data['data']['filename'], $this->data['data']['user_id']);
    	// $file = File::getFile($hash,$this->data['data']['filename'],'document/files');
    	$ukuran = $this->calculateSize(filesize($path.$this->data['data']['document']));
		if ($ukuran!='0 Bytes') {
			$this->data['data']['ukuran'] =$ukuran;
		}

		// Related Workunit
		$this->load->model('Document_unitkerja_model');
		$related = $this->Document_unitkerja_model->filter(" where document_id=$id ")->getAll();
		$this->data['data']['related_workunit'] = Util::toList($related, 'document_unitkerja');

		// Related Document
		$this->data['data']['docRelated'] = $this->model->getRelatedDocument($id);
		for ($i=0; $i < count($this->data['data']['docRelated']) ; $i++) {
    		// echo "select name from unitkerja where id='".$doc[$i]['UNITKERJA_ID']."'";
    		$this->data['data']['docRelated'][$i]['TYPE_NAME'] = dbGetOne("select type from document_types where id=".$this->data['data']['docRelated'][$i]['TYPE_ID']);
    		$this->data['data']['docRelated'][$i]['UNITKERJA'] = dbGetOne("select name from unitkerja where id='".$this->data['data']['docRelated'][$i]['UNITKERJA_ID']."'");
    	}
		// Cek History
		$this->load->model('Document_activity_model');
		$this->data['data']['history'] = $this->Document_activity_model->getHistory($id);

    	$this->data['title'] = $this->data['data']['type_name'].' "'.$this->data['data']['title'].'"';

    	// Cek Variable
    	$this->data['variables'] = SessionManagerWeb::getVariables();
    	switch($this->data['data']['workflow_id']){
    		case Workflow_model::PENGAJUAN:
    			$this->data['is_revisi'] = false;
    			if ($this->data['data']['status']==Document_model::STATUS_REVISE){
    				$this->load->model('Document_verification_model');
    				$this->data['revise_verification'] = $this->Document_verification_model->getTypeBy($id);
    			}
    			if ($this->data['data']['status']==Document_model::STATUS_REVISE){
					$this->load->model('Document_revises_model');
					$this->data['revise_requestor'] = $this->Document_revises_model->getLastUser($id);
				}
    		break;
			case Workflow_model::EVALUASI:
				$this->data['is_revisi'] = false;

				$this->data['must_revise'] = false;
				$date_publish = $this->model->getDatePublished($id);
				$published_at =strftime('%Y/%m/%d', strtotime($date_publish));
				$diff = abs(time() - strtotime($published_at));
				$diff = $diff/60/60/24/365;
				if ($diff>=5){
					$this->data['must_revise'] = true;
				}

				// Evaluation form
				$this->load->model('Document_evaluation_model');
				$evaluation = $this->Document_evaluation_model->filter("where document_id=$id")->getBy();
				if ($evaluation!=NULL){
					$evaluation_text_counter = 0;
					$evaluation['text'] = json_decode($evaluation['text'], true);
					foreach ($evaluation['text'] as $key => $value) {
						if ($value=='1')
							$evaluation_text_counter++;
					}
					$evaluation['result'] = $evaluation_text_counter;
					$this->data['evaluation'] = $evaluation;
				}
				// echo "<pre>";print_r($this->data['evaluation']);die();
				// Document delegates
	    		$this->load->model('Document_delegates_model');
	    		$document_id = $this->data['data']['document_id'];
				$workflow_id = Workflow_model::EVALUASI;
				$workflow_urutan = 1;
				$conditions = array(
					" document_id=$document_id ", 
					"workflow_id=$workflow_id", 
					"workflow_urutan=$workflow_urutan"
				);
				$condition = implode(' and ', $conditions);
				$delegated = $this->Document_delegates_model->filter($condition)->getBy();
				$this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm($id);
				switch($this->data['data']['workflow_urutan']){
					case 1:
						$this->data['delegated'] = $delegated;
					break;
				}
				if ($this->data['data']['status']==Document_model::STATUS_REVISE){
					$this->load->model('Document_revises_model');
					$this->data['revise_requestor'] = $this->Document_revises_model->getLastUser($document_id);
				}
			break;
			case Workflow_model::REVISI:
				$this->data['is_revisi'] = true;
				$compare = array(1, 2, 3, 4);
				$this->data['is_allow_compare'] = false;
				$this->data['is_file_exist'] = false;
				if (in_array($this->data['data']['workflow_urutan'], $compare)){
					$this->data['is_allow_compare'] = true;
					$document = $this->model->filter("where d.\"document_id\"=$id")->getDocument();

					$p = $this->model->getOne($id, 'parent');
					$prev_document = $this->viewdoc($p, 0);

					$this->data['prev_document'] = $prev_document;
				}
				$ciConfig = $this->config->item('utils');
	            $path = $ciConfig['full_upload_dir'] . 'document/files/' ;
	            $full_filename = $path.$this->data['data']['filename'];
	            if (file_exists($full_filename) or $this->data['data']['is_upload']==0){
	            	$this->data['is_file_exist'] = true;
	            }
    			if ($this->data['data']['status']==Document_model::STATUS_REVISE){
    				$this->load->model('Document_verification_model');
    				$this->data['revise_verification'] = $this->Document_verification_model->getTypeBy($id);
    				$this->load->model('Document_revises_model');
					$this->data['revise_requestor'] = $this->Document_revises_model->getLastUser($id);
    			}
			break;
		}

		// Cek Reviewer
    	$this->load->model('Document_reviewer_model');
    	$reviewer = $this->Document_reviewer_model->getAllBy($this->data['data']['document_id'], $this->data['data']['workflow_id']);
    	foreach ($reviewer as $value) {
    		$this->data['data']['reviewer'][$value] = $value;
    	}
    	$this->load->model('Document_reviews_model');
    	$this->data['data']['review'] = $this->Document_reviews_model
    									->column(array(
    										"dr.document_id" => "\"document_id\"",
									        "dr.document_reviewer" => "\"document_reviewer\"",
									        "dr.review" => "\"review\"",
									        "u.name" => "\"reviewer_name\"",
									        "dr.is_agree" => "\"is_agree\"",
									        "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
    									))
    									->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
    									->filter(" dr.document_id=$id and workflow_id=".$this->data['data']['workflow_id'])
    									->getAll();
    	// echo "<pre>";print_r($this->data['variables']['reviewer']);die();
    	// count reviewer
		$this->data['counter']['reviewer'] = count($this->data['data']['reviewer']);

		// reviewed
		$this->data['variables']['reviewed'] = array();
    	foreach ($this->data['data']['review'] as $key => $review) {
    		$this->data['data']['reviewed'][] = $review['document_reviewer'];
    		unset($this->data['data']['reviewer'][$review['document_reviewer']]);
    		$this->data['variables']['reviewed'][$review['document_reviewer']] = $this->data['variables']['reviewer'][$review['document_reviewer']];
    		unset($this->data['variables']['reviewer'][$review['document_reviewer']]);

    	}
    	// count jml review
    	$this->data['counter']['reviewed'] = count($this->data['data']['reviewed']);
    	if ($this->data['counter']['reviewer']<=$this->data['counter']['reviewed'] and $this->data['counter']['reviewed']>0){
    		$this->data['document_controller']['allow_next'] = 1;
    	} else {
    		$this->data['document_controller']['allow_next'] = 0;
    	}

    	$this->load->model('User_model');
    	$this->data['user'] = $this->User_model->getByAll($this->data['data']['user_id']);
    	$this->load->model('Unitkerja_model');
    	$this->data['user']['company_name'] = $this->Unitkerja_model->getOne($this->data['user']['company'], 'name'); 
    	$is_chief = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'is_chief');
    	$this->data['is_chief'] = false;
    	// if (($is_chief=='X' or SessionManagerWeb::isAdministrator()) and SessionManagerWeb::getRole()==Role::DOCUMENT_CONTROLLER) {
    	if (($is_chief=='X') and SessionManagerWeb::getRole()==Role::DOCUMENT_CONTROLLER) {
    		$this->data['is_chief'] = true;
    		$this->data['delegates'] = $this->User_model->getAllBy(" \"role\"='".Role::DOCUMENT_CONTROLLER."' and (\"is_chief\"!='X' or \"is_chief\" is null ) and \"company\"='".$this->data['variables']['mycompany']."' ");
    	}    	

    	$drafter_evaluation = dbgetOne('select drafter_evaluation from documents where id='.$this->data['data']['document_id']);
    	$this->data['tasks']= $this->Workflow_model->getTasks($this->data['data']['workflow_id'], null ,$drafter_evaluation);
    	// echo "<pre>";print_r($this->data['tasks']);echo "</pre>";
    	$task_keys = array();
    	$this->data['urutan_drafter'] = 2;
    	foreach ($this->data['tasks'] as $key => $task) {
    		$skip = 0;
			switch($this->data['data']['workflow_id']){
				case Workflow_model::PENGAJUAN:
					if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR){
						if ($this->data['type']['need_reviewer']=='0'){
							$skip=1;
						}
					}
					// if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT))){
					// 	$skip=1;	
					// }
					if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT))){
						$skip=1;	
					}
				break;
				case Workflow_model::EVALUASI:
					$drafter_evaluation = dbgetOne('select drafter_evaluation from documents where id='.$this->data['data']['document_id']);
					$this->data['drafter_evaluation'] = $drafter_evaluation;
					if ($drafter_evaluation == null && $task['urutan']==0){
						$skip=1;
					}
					if ($drafter_evaluation != null && $task['urutan']==1 ){
						$skip=1;
						
					}
					// Document delegates
					//  		$this->load->model('Document_delegates_model');
					//  		$document_id = $this->data['data']['document_id'];
					// $workflow_id = Workflow_model::EVALUASI;
					// $workflow_urutan = 1;
					// $conditions = array(
					// 	" document_id=$document_id ", 
					// 	"workflow_id=$workflow_id", 
					// 	"workflow_urutan=$workflow_urutan"
					// );
					// $condition = implode(' and ', $conditions);
					// $delegated = $this->Document_delegates_model->filter($condition)->getBy();
					// switch($this->data['data']['workflow_urutan']){
					// 	case 1:
					// 		$this->data['evaluation_forms'] = $this->Workflow_model->getEvaluationForm();
					// 		$this->data['delegated'] = $delegated;
					// 	break;
					// }
					if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT))){
							$skip=1;
						}
					// if ($delegated==NULL and $task['urutan']==3){
					if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $drafter_evaluation==null and $task['urutan']==3){
						if ($this->data['type']['need_reviewer']=='0'){
							$skip=1;
						}
						
					}


					if ($this->data['evaluation']['result']!=0 and $task['urutan']==4){ // 'Withdraw Verification di lewati jika ada minim 1 yes di verification form. (proses ini di lewatin ketika hasilnya revisi / relevan)
						$skip=1;
						$this->data['urutan_drafter'] = 3;
					}


					if ($this->data['evaluation']['result']>0 and $this->data['evaluation']['result']<=9 and ($task['urutan']==5 || $task['urutan']==2)){ // evaluation approval di lewati jika ada yes tapi masih ada no nya d verification form; (proses ini di lewatin ketika hasilnya revisi / relevan)
						$skip=1;
						$this->data['urutan_drafter'] = 3;
					}

					if($this->data['evaluation']['result']>0 and $this->data['evaluation']['result']<=9 and $drafter_evaluation==null and $task['urutan']== 3){ // relevan dan revisi dimulai dari creator
						$skip=1;
					}
					if($drafter_evaluation!=null && $this->data['evaluation']['result']==0 && ($this->data['data']['type_id'] == 4 || $this->data['data']['type_id'] == 5)){
						$this->data['urutan_drafter'] = 3;
					}
					// jika evaluation resultnya 9 (yes semua) maka hasilnya cabut
					// if($this->data['evaluation']['result']== 0 and $this->data['evaluation']['result']!=0 and ($task['urutan'] == 2 || $task['urutan'] == 5)){
					// 	$skip=1;
					// }
				break;
				case Workflow_model::REVISI:
					$this->data['drafter_evaluation'] = $drafter_evaluation;
					if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR and $drafter_evaluation == null){
						if ($task['urutan']!=1 and $this->data['type']['need_reviewer']=='0'){
							$skip=1;	
						}
					}
					if ($this->data['type']['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT)))
						$skip=1;
				break;
			}

			// Get Klausul
			if ($this->data['type']['klausul']=='1' and $task['edit_attribute']==1){
				$this->load->model('Document_klausul_model');
				$this->load->model('Document_iso_klausul_model');
		        $iso_id = implode(',',$this->data['data']['iso']);
		        $this->data['variables']['klausul'] = Util::toMap($this->Document_iso_klausul_model->filter(" iso_id in ($iso_id) ")->getAll(),'id', 'klausul');
				$this->data['data']['klausul'] = Util::ToList($this->Document_klausul_model->
												column(array(
											        "dk.klausul_id" => "\"klausul_id\"",
											        "dik.klausul" => "\"klausul_name\""
											    ))
												->table("document_klausul dk")
												->with(array("name"=>"Document_iso_klausul", "initial"=>"dik"), "dik.id=dk.klausul_id")
												->filter(" document_id=$id ")
												->getAll(), 'klausul_id');
			}
			if ($skip==1){
				unset($this->data['tasks'][$key]);
				continue;
			}
    		// if ($this->data['data']['user_id']==$this->data['data']['creator_id'] and $task['user']==Role::CREATOR and $this->data['data']['workflow_id']==Workflow_model::PENGAJUAN){
    		// 	unset($this->data['tasks'][$key]);
    		// 	continue;
    		// }
    		if ($task['urutan']==$this->data['data']['workflow_urutan']){

    			$this->data['data']['edit'] = $task['edit'];
    			// Check variables untuk form
    			switch($task['form']){
    				case Workflow_model::VERIFICATION:
	    				$this->load->model('Document_verification_model');
	    				$this->data['data']['verification'] = $this->Document_verification_model->getTypeBy($id);
	    				$counter = 0;
	    				foreach ($this->data['data']['verification'] as $k_verification => $v_verification) {
	    					if ($v_verification['is_appropriate']=='0')
	    						$counter++;
	    				}
	    				$this->data['data']['is_verification_appropriate']=$counter;

	    				// Approver -> sementara pengajuan saja
	    				if ($this->data['data']['workflow_id']==Workflow_model::PENGAJUAN){
	    					$mycompany = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'company');
	    					$user_approvers = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\""))->getAll(" where \"company\"='$mycompany' ");
	    					$this->data['approvers'] = Util::toMap($user_approvers, "user_id", "name");
	    					$this->data['approvers'][null] = "Choose Approver.";
	    					// $this->data['approvers'][] = array(
	    					// 	"user_id" => null,
	    					// 	"name" => "Choose Approver"
	    					// );
	    				}
    				break;
    				case Workflow_model::EVALUATION:
    					//Get Unitkerjanya
    					foreach ($this->data['variables']['myunitkerja'] as $k_myunitkerja => $v_myunitkerja) {
    						$myunitkerja = $k_myunitkerja;
    						break;
    					}
    					$min_drafter = $this->Document_types_model->filter(" id=".$this->data['type']['id'])->getOne('eselon_drafter');
    					$this->load->model('Atasan_model');
    					$nopeg = $this->User_model->getOne(SessionManagerWeb::getUserID(), 'karyawan_id');
    					// $bawahan = $this->User_model->getAllBy(" \"user_id\"!='".SessionManagerWeb::getUserID()."' and \"unitkerja_id\"='".$myunitkerja."' ");
    					$bawahan = $this->Atasan_model->getDelegates($this->data['variables']['subordinates'], $min_drafter);
    					$this->data['subordinates'] = $bawahan;

    				break;
    				case Workflow_model::CREATION:
    					if ($this->data['data']['workflow_id']==Workflow_model::REVISI){
    						foreach ($this->data['variables']['myunitkerja'] as $k_myunitkerja => $v_myunitkerja) {
	    						$myunitkerja = $k_myunitkerja;
	    						break;
	    					}
	    					$this->load->model('Atasan_model');
	    					// $bawahan = $this->User_model->getAllBy(" \"user_id\"!='".SessionManagerWeb::getUserID()."' and \"unitkerja_id\"='".$myunitkerja."' ");
	    					
	    					$min_drafter = $this->Document_types_model->filter(" id=".$this->data['type']['id'])->getOne('eselon_drafter');
	    					$nopeg = $this->User_model->getOne(SessionManagerWeb::getUserID(), 'karyawan_id');
	    					$bawahan = $this->Atasan_model->getDelegates($this->data['variables']['subordinates'], $min_drafter);
	    					$this->data['subordinates'] = $bawahan;
	    					
	    				}
    				break;
    				case Workflow_model::REVIEW_CONTENT || Workflow_model::REVIEW_ATRIBUT :
    					if ($this->data['data']['workflow_id']==Workflow_model::REVISI){
    						$this->data['approver_value'][0] = $this->data['data']['approver_id'];
	    				}
    				break;
    			}
    			// echo $min_drafter.'<br>';
    			// echo '<pre>';
    			// vaR_dump($this->data['variables']);
    			// die();

    			// if ($task['form']==Workflow_model::VERIFICATION){
    			// 	$this->load->model('Document_verification_model');
    			// 	$this->data['data']['verification'] = $this->Document_verification_model->getTypeBy($id);
    			// }
    		}
    		if ($task['end']==1)
    			continue;
    		
    		$this->data['tasks'][$key]['actor_name'] = $this->getActorName($this->data['data'],$task);
    		$this->data['tasks'][$key]['comment'] = $this->model->getComment($id, $task['urutan']);
    		$task_keys[] = $key;
    	}
    	// echo '<pre>';
    	// vaR_dump($this->data['tasks']);
    	// die();
    	$total=count($this->data['tasks'])-1;
    	$counter = 0;
    	for ($i=$total;$i>=0;$i--) {
    		$this->data['tasks_ordered'][$counter] = $this->data['tasks'][$task_keys[$i]];
    		$counter++;
    	}
    	// echo '<pre>';
    	// var_dump($this->data['tasks_ordered']);
    	// die();
    	$this->data['forms'] = $this->Workflow_model->getForms($this->data['data']['workflow_id']);
    	
    	$document_iso = $this->data['variables']['document_iso'];
    	$this->data['variables']['isos'] = $document_iso;
    	unset($this->data['variables']['document_iso']);
    	$this->data['variables']['document_iso'] = Util::toMap($document_iso, 'id', 'iso');

    	// Cek Delegasi
    	$this->load->model('Document_delegates_model');
    	$this->data['delegated'] = $this->Document_delegates_model->filter(" workflow_urutan=".$this->data['data']['workflow_urutan']." and workflow_id=".$this->data['data']['workflow_id']. " and document_id=".$this->data['data']['document_id'])->getBy();

    	// $this->setVariables();

    	// echo '<pre>';
    	// vaR_dump($this->data['type']);
    	// die();
		// echo '<Pre>';
		// var_dump($this->data['data']);
		// die();
		// echo '<pre>';
  //   	vaR_dump($this->data['variables']);
    	// die();
    	// echo "<pre>";print_r($this->data);die();
    	$this->template->viewDefault($this->view, $this->data);
    }


    public function getActorName($data, $task){
    	$this->load->model('Workflow_model');
    	if ($task['user']==Role::CREATOR){
    		$actor_name = $data['creator_name'];
    	} else if ($task['user']==Role::APPROVER){
    		$actor_name = $data['approver_name'];
    	} else if ($task['user']==Role::REVIEWER) {
    		$this->load->model('Document_reviewer_model');
    		$actor_name = $this->Document_reviewer_model->getAll($data['document_id'], $data['workflow_id']);
    	} else if ($task['user']==Role::DOCUMENT_CONTROLLER){
    		$this->load->model('Document_delegates_model');
    		$document_id = $data['document_id'];
    		$workflow_id = $data['workflow_id'];
    		$workflow_urutan = $task['urutan'];
    		$conditions = array(
    			" document_id=$document_id ", 
    			"workflow_id=$workflow_id", 
    			"workflow_urutan=$workflow_urutan"
    		);
    		$condition = implode(' and ', $conditions);
    		$delegated = $this->Document_delegates_model->filter($condition)->getBy();

    		$this->load->model('User_model');

    		// Kalau pada task ini ga delegasi, pakai kabiro SMSI
    		// if (!$delegated or $workflow_urutan!=$task['urutan']){
    		$role = Role::DOCUMENT_CONTROLLER;
    		$company = $this->User_model->getBy($data['user_id'], 'company');
    		if (!$delegated){
	    		$users = $this->User_model->getAll("where \"role\"='$role' and \"is_chief\"='X' and \"company\"='$company' ");
    		} else {
    			$user_id = $delegated['delegate'];
    			$users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
    		}
    		$actor_name = $users[0]['name'];
    	} else if($task['user']==Role::DRAFTER_EVALUATION){
    		$this->load->model('User_model');
    		$drafter_evaluation = dbGetOne('select drafter_evaluation from documents where id='.$data['document_id']);
    		$users = $this->User_model->show_sql(false)->getAll(" where \"user_id\"='".$drafter_evaluation."' ");
    		$actor_name = $users[0]['name'];
    	}
    	switch($data['workflow_id']){
    		case Workflow_model::EVALUASI:
    			switch($task['urutan']){
    				case 1:
    					$this->load->model('Document_delegates_model');
			    		$document_id = $data['document_id'];
			    		$workflow_id = $data['workflow_id'];
			    		$workflow_urutan = $task['urutan'];
			    		$conditions = array(
			    			" document_id=$document_id ", 
			    			"workflow_id=$workflow_id", 
			    			"workflow_urutan=$workflow_urutan"
			    		);
			    		$condition = implode(' and ', $conditions);
			    		$delegated = $this->Document_delegates_model->filter($condition)->getBy();

			    		$this->load->model('User_model');
			    		if (!$delegated){
			    			$user_id = $data['creator_id'];
				    		$users = $this->User_model->getAll("where \"user_id\"='$user_id' ");
			    		} else {
			    			$user_id = $delegated['delegate'];
			    			$users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
			    		}
			    		$actor_name = $users[0]['name'];
    				break;
    			}
    		break;
    		case 3:
    			switch($task['urutan']){
    				case 1:
    					if($task['user']==Role::DRAFTER_REVISION){
    						$this->load->model('User_model');
				    		$drafter_evaluation = dbGetOne('select drafter_evaluation from documents where id='.$data['document_id']);
				    		$users = $this->User_model->show_sql(false)->getAll(" where \"user_id\"='".$drafter_evaluation."' ");
				    		$actor_name = $users[0]['name'];
    					}
    					else{
    						$this->load->model('User_model');
				    		$actor_name = $this->User_model->getBy($data['user_id'], 'name');
				    		return $actor_name;
    					}
			    		
    				break;
    			}
    		break;

    	}
    	return $actor_name;
    }

    public function is_delegated(){

    }

    public function getSessionVariables(){
    	echo '<pre>';
    	$this->load->model('Unitkerja_model');
    	vaR_dump($this->Unitkerja_model->order('name')->getAllList(" company='2000' "));
    	die();
    }

    public function ajaxgetcode(){
    	$post = $this->input->post(null, true);
    	$str = $this->model->getCodeInForm($post['type_id'],$post['prosesbisnis_id'],$post['unitkerja_id'],$post['form_type']);
    	echo $str;
    }

    public function ajaxCancel(){
    	$this->load->model('Document_evaluation_model');

    	$document_id = (int)$this->input->post('document_id', true);
    	$workflow_id = $this->model->getOne($document_id,'workflow_id');
    	if($workflow_id == 2){
    		// hapus hasil evaluasi	
    		$delete = $this->Document_evaluation_model->deleteWith($document_id);
    		// kembalikan workflow id 1 dan workflow urutan 6, status: D
    		dbUpdate('documents', array('workflow_id' => 1, 'workflow_urutan' => 6, 'status' => 'D', 'drafter_evaluation' => null), "id=$document_id");
    		// set history
    		$this->addHistory($document_id, "Canceled this evaluation document because \"<b>".$this->input->post('text', true)."\"</b>");
    	}
    	elseif($workflow_id == 3){
    		$prev_id = $this->model->getOne($document_id, 'parent');
			$prev_doc = array();
			$prev_doc['workflow_id'] = Workflow_model::PENGAJUAN;
			$prev_doc['status'] = Document_model::STATUS_SELESAI;
		    $this->Document_evaluation_model->deleteWith($prev_id);
		    $this->addHistory($prev_id, "Canceled this revision document because \"<b>".$this->input->post('text', true)."\"</b>");
		    $this->model->deleteDocument($document_id);
    	}
    	echo $this->input->post('text', true);
    }

    public function ajaxSetUrutan(){
    	unset($_SESSION['edit_evaluation']);
    	$document_id = (int)$this->input->post('document_id', true);
    	$data['workflow_urutan']=(int)$this->input->post('workflow_urutan', true);
    	$data['status'] = $this->input->post('status', true);
    	$data['updated_by'] = SessionManagerWeb::getUserID();
    	$workflow_id = $this->model->getOne($document_id,'workflow_id');
    	$urutan_now = $this->model->getOne($document_id, 'workflow_urutan');
    	$status_now = $this->model->getOne($document_id, 'status');
    	$type_id = $this->model->getOne($document_id, 'type_id');
    	$this->load->model('Document_types_model');
    	$type = $this->Document_types_model->get($type_id);
    	// $this->load->model('Workflow_model');
    	// $task = $this->Workflow_model->getTasks($workflow_id,$urutan_now);
    	// if ($task['change_to_creator']==1){

    	// }
   //  	if ($data['workflow_urutan']<$urutan_now) {
   // //  		for ($i = $data['workflow_urutan']; $i <=$urutan_now; $i++) {
			// //     $this->model->deleteComment($document_id, $i);
			// // }
   //  		$this->updateBecauseRevise($document_id);
   //  	}

    	$this->load->model('Workflow_model');
    	$task = $this->Workflow_model->getTasks($workflow_id, $urutan_now);
    	switch($data['status']){
    		case Document_model::STATUS_REVISE:
	    		$revision['text'] = $this->input->post('text', true);
	    		$revision['user_id'] = SessionManagerWeb::getUserID();
		    	$revision['document_id'] = $document_id;
		    	dbInsert('DOCUMENT_REVISES', $revision);
	    		$this->updateBecauseRevise($document_id);
	    		$this->addHistory($document_id, "Ask this document to be revised because ".$revision['text']);
    		break;
    		case Document_model::STATUS_REJECT:
	    		$this->addHistory($document_id, "Rejected this document because ".$this->input->post('text', true));
    		break;
    	}
    	$this->load->model('Document_reviewer_model');
    	$this->load->model('Workflow_model');
    	// $data_sebelum['WORKFLOW_ID'] = $workflow_id;
    	// $data_sebelum['WORKFLOW_URUTAN'] = $urutan_now;
    	$data_sebelum = $this->model->getDocumentFlow($document_id); // data id dokumen yang lama
    	// echo "ini status: ".$data['status'];
    	switch($workflow_id){ // workflow id yang lama
    		case '1':
    			switch($urutan_now){
    				case '1':
    					if ($status_now==Document_model::STATUS_REVISE){
    						$this->addHistory($document_id, "Revise Document");
    					} else {
    						$this->addHistory($document_id, "Submitted Document");
    					}
    				break;
    				default:
	    				$this->addHistory($document_id, "Already ".$task['name']." document");
    				break;
    			}
    			if ($data['workflow_urutan']==4){
    				$this->load->model('Document_verification_model');
    				$this->Document_verification_model->delete($document_id);
    			}
    			if ($data['status']==Document_model::STATUS_SELESAI){
	    			$data['is_published'] = 1;
	    			$document_code = $this->model->getOne($document_id, 'code');
	    			$codes = explode('/',$document_code);
	    			$filter_code = $codes[0].'/'.$codes[1];
	    			if ($type_id==Document_model::INSTRUKSI_KERJA){
	    				$unitkerja_id = $this->model->getOne($document_id, 'unitkerja_id');
	    				$param = " and unitkerja_id=$unitkerja_id ";
	    			} else {
	    				if ($type_id==Document_model::FORM){
	    					if ($codes[0]=='FI'){
	    						$unitkerja_id = $this->model->getOne($document_id, 'unitkerja_id');
			    				$param = " and unitkerja_id=$unitkerja_id ";
	    					} else {
	    						$prosesbisnis_id = $this->model->getOne($document_id, 'prosesbisnis_id');
			    				$param = " and prosesbisnis_id=$prosesbisnis_id ";
	    					}
	    				} else {
	    					$prosesbisnis_id = $this->model->getOne($document_id, 'prosesbisnis_id');
		    				$param = " and prosesbisnis_id=$prosesbisnis_id ";
	    				}
	    			}

	    			// $data['code_number'] = $this->model->getLastCode($document_code);
	    			$data['code_number'] = $this->model->getLastCode($filter_code, $param);
	    			$data['code'] = $document_code.$data['code_number'];
	    			$document = $this->model->getOne($document_id, 'document');
	    			$file_explode = explode('.', $document); 
					$code_explode = explode('/',$data['code']);
					$code_implode = implode('_',$code_explode);
					$data['document'] = $code_implode.'_R0'.'.'.end($file_explode);
					$this->checkCode($document_id, $data['code']);
	    			$this->model->updateValidityDate($document_id);
	    			$this->model->updateDateBy($document_id, 'published_at');
	    			$this->addHistory($document_id,"This document already published");
	    		}
    		break;
    		case '2':
    			$this->load->model('Document_evaluation_model');
				$evaluation = $this->Document_evaluation_model->filter("where document_id=$document_id")->getBy();
				$evaluation['text'] = json_decode($evaluation['text'], true);
    			if ($data['status']==Document_model::STATUS_SELESAI){ // Jika data status sudah selesai (D)
	    			// $data['is_published'] = 1;
	    			// $this->model->updateDateBy($document_id, 'published_at');
					$ev_update = array();
					if ($evaluation['text'][1]==0){
						$ev_update['status'] = Document_evaluation_model::CABUT;
						$data['is_obsolete'] = 1;
						$data['show']=0;
					} else {
						$is_relevan = 1;
						foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
							if ($v_evaluation==0){
								$is_relevan = 0;
								break;
							}
						}
						if ($is_relevan==0){
							$ev_update['status'] = Document_evaluation_model::REVISI;
						} else {
							$ev_update['status'] = Document_evaluation_model::RELEVAN;
							dbUpdate('documents', array('drafter_evaluation' => null), "id=$document_id");/// reset drafter_evaluation
							$this->model->updateValidityDate($document_id);
						}
					}
					$this->Document_evaluation_model->update($document_id, $ev_update);
					
	    			$this->addHistory($document_id,"This document is evaluated");
	    		}
	    		switch($urutan_now){ // workflow urutan yang sekarang, masih menggunakan id yang sama / lama
	    			case '1':
	    				$creator_id = $this->model->getOne($document_id,'creator_id');
    					$user_id = $this->model->getOne($document_id,'user_id');
	    				// if ($creator_id==$user_id){
    					// 	$this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
    					// }
    					$this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
	    				
	    			break;
	    			case '3':
	    				$this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
	    			break;
	    			case '5':
	    				$this->Document_evaluation_model->updateDateBy($document_id,'approver_date');
	    			break;
	    		}
    			switch($data['workflow_urutan']){ // workflow urutan berdasarkan post data
    				case '3':
    					$creator_id = $this->model->getOne($document_id,'creator_id');
    					$user_id = $this->model->getOne($document_id,'user_id');
    					if ($creator_id==$user_id and $type['need_reviewer']=='0' and $data_sebelum['DRAFTER_EVALUATION'] == null){
    						$data['workflow_urutan']++;
    						if ($evaluation['text'][1]!=0){
	    						$data['workflow_urutan']++;
	    						$result = 0;
								foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
									if ($v_evaluation==0){
										$result++;
										break;
									}
								}
								switch ($result) {
									case 0:
									break;
									case 9:
									break;
									default:
										$data['workflow_urutan']++;
										$data['status']=Document_model::STATUS_SELESAI;
										$ev_update = array();
										$ev_update['status'] = Document_evaluation_model::REVISI;
										$this->Document_evaluation_model->update($document_id, $ev_update);
						    			$this->addHistory($document_id,"This Document Evaluation is Done");
									break;
								}
	    					}
    					}
    				break;
    				case '4':
    					if ($evaluation['text'][1]!=0){
    						$data['workflow_urutan']++;
    						$result = 0;
							foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
								if ($v_evaluation==0){
									$result++;
									break;
								}
							}
							switch ($result) {
								case 0:
								break;
								case 9:
								break;
								default:
									$data['workflow_urutan']++;
									$data['status']=Document_model::STATUS_SELESAI;
									$ev_update = array();
									$ev_update['status'] = Document_evaluation_model::REVISI;
									$this->Document_evaluation_model->update($document_id, $ev_update);
					    			$this->addHistory($document_id,"This Document Evaluation is Done");
								break;
							}
    					}
    				break;
    				default:
    				break;
    			}
    			// Jika Revisi langsung ke halaman revisi
    			if ($ev_update['status']==Document_evaluation_model::REVISI and $data['status']==Document_model::STATUS_SELESAI){
			    	// $data['show'] = 0;
			    	// $data['workflow_id'] = 1;
			    	if (dbUpdate('documents', $data, "id=$document_id")){
			    		/////////////////////// BERIKAN OBSOLETE WATERMARK DI SEBELAH SINI SETELAH UPDATE STATUS ID YANG LAMA DAN SEBELUM CREATE DATA BARU
			    		$ev_update = array();
						$ev_update['status'] = Document_evaluation_model::REVISI;
						$this->Document_evaluation_model->update($document_id, $ev_update);
			    		
						// Add new Document
			    		$data = array();
			    		$document = $this->model->getBy($document_id);
			    		$doc = array_change_key_case($document, CASE_LOWER);

			    		////////  OBSOLETE WATERMARK  /////////////////////
			    		$document = $this->model->getBy($doc['id']);
				    	$this->load->model('Unitkerja_model');
				    	$idCompany = $this->Unitkerja_model->getOneBy($doc['unitkerja_id'], "\"COMPANY\"");
				    	$explodefilename = explode('.', $doc['document']);
				        $filename = $explodefilename[0].'.pdf';
				        $ciConfig = $this->config->item('utils');
						$full_path = $ciConfig['full_upload_dir'].'document/files/';
						if($doc['have_header'] == 1){
							rename($full_path.$explodefilename[0].'_header.pdf', './assets/uploads/document/files/'.$explodefilename[0].'_header_old.pdf');
					        $this->ObsoleteWatermark($filename, $idCompany, 'pdf');
					        unlink($full_path.'./assets/uploads/document/files/'.$explodefilename[0].'_header_old.pdf');
						}
				        
				        ////////  ------------------  /////////////////////


				    	$data['code'] =$doc['code'];
				        $data['user_id'] = $doc['user_id'];
				        $data['description'] = $doc['description'];
				        $data['title'] = $doc['title'];
						$data['is_upload'] = $doc['is_upload']; 
						$data['retension'] = $doc['retension'];
						$data['hash'] = $doc['hash'];
						// $data['revision'] = $doc['revision']+1;
						$data['revision'] = $doc['revision'];
						$data['status'] = Document_model::STATUS_BARU;
						$data['workflow_id'] = Workflow_model::REVISI;
						$data['workflow_urutan'] = 1;
						$data['show'] = 0;
						$data['parent'] = $document_id;
						$data['is_published'] = 0;
						$data['published_at'] = $doc['published_at'];
						$data['validity_date'] = $doc['validity_date'];
						$data['code_number'] = $doc['code_number'];
						$data['drafter_evaluation'] = $doc['drafter_evaluation'];

						// Setting
						$data['type_id'] = $doc['type_id'];
						$data['creator_id'] = $doc['creator_id'];
						$data['approver_id'] = $doc['approver_id'];					
						$data['prosesbisnis_id'] = $doc['prosesbisnis_id'];
						$data['unitkerja_id'] = $doc['unitkerja_id'];
						$data['user_jabatan_id'] = $doc['user_jabatan_id'];	
						$data['creator_jabatan_id'] = $doc['creator_jabatan_id'];	
						$data['approver_jabatan_id'] = $doc['approver_jabatan_id'];	

						// Setting new Document
						$file_explode = explode('.', $doc['document']); 
						$code_explode = explode('/',$data['code']);
						$code_implode = implode('_',$code_explode);
						$data['document'] = $code_implode.'_Revise'.$data['revision'].'.'.end($file_explode);

						$new_id = $this->addNewDocument($data);

						if ($new_id){							

							// Update nama document pakai id
							$append_id = "_".$new_id;
							$record = array();
							$record['document'] = $code_implode.'_Revise'.$data['revision']."_".$new_id.'.'.end($file_explode);
							dbUpdate("documents", $record, "id=$new_id");

							//Copy lama ke baru 
							if ($data['is_upload']==0){
								$CI = & get_instance();
						        $config = $CI->config->item('utils');
						        $folder = "document/files";
						        $full_path = $config['full_upload_dir'] . $folder . '/';

						        $new_docs = explode('.',$data['document']);
						        $ext = $new_docs[count($new_docs)-1]; 
						        unset($new_docs[count($new_docs)-1]);
						        $new_doc = implode('.', $new_docs);

						        $from = $full_path.$doc['document'];
		                        $to = $full_path.$new_doc.$append_id.'.'.$ext;
		                        copy($from, $to);

						        $old_docs = explode('.',$doc['document']);
						        unset($old_docs[count($old_docs)-1]);
						        $old_doc = implode('.', $old_docs);

						        $from = $full_path.$old_doc.'.txt';
		                        $to = $full_path.$new_doc.$append_id.'.txt';
		                        copy($from, $to);
							}

							// reviewer
							if ($type['need_reviewer']=='1'){
								$this->load->model('Document_reviewer_model');
								$data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, $workflow_id);
								$data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
								$this->Document_reviewer_model->create($new_id,Workflow_model::REVISI, $data_reviewer);
							}
							
							// ISO
							$data_iso = array();
							$data_iso['document_id'] = $new_id;
							$iso_id = $this->model->getDocumentISOS($document_id);
							foreach ($iso_id as $key => $value) {
								$data_iso['iso_id'] = $value;
								dbInsert("DOCUMENT_ISOS", $data_iso);
							}

							// Klausul
							if ($type['klausul']=='1'){
								$this->load->model('Document_klausul_model');
								$data_klausul['document_id'] = $new_id;
								$klausul = $this->Document_klausul_model->filter(" document_id=$document_id ")->getAll();
								foreach ($klausul as $key => $value) {
									$data_klausul['klausul_id'] = $value['klausul_id'];
									dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
								}
							}
						
							// Unit Kerja
							$this->load->model('Document_unitkerja_model');
							$unitkerja_related = $this->Document_unitkerja_model->filter(" where document_id=$document_id ")->getAll();
							if ($unitkerja_related!=NULL){
								$data_unit_kerja = array();
								foreach ($unitkerja_related as $k_unitkerja_related => $v_unitkerja_related) {
									$data_unit_kerja = $v_unitkerja_related;
									$data_unit_kerja['document_id'] = $new_id;
									dbInsert('DOCUMENT_UNITKERJA', $data_unit_kerja);
								}	
							}
							
							// Related dari dokumen ini ke dokumen lain
							$relatedDocument = $this->model->getRelatedOneWay($document_id);
							if($relatedDocument!=NULL){
								$data_related_document = array();
								foreach ($relatedDocument as $related => $column) {
									$data_related_document['document1'] = $new_id;
									$data_related_document['document2'] = $column['DOCUMENT2'];
									// $data_related_document['created_at'] = date("Y/m/d h:i:s");
									// $data_related_document['updated_at'] = date("Y/m/d h:i:s");
									$data_related_document['is_form'] = $column['IS_FORM'];
									dbInsert('DOCUMENT_RELATED', $data_related_document);
								}
							}
							
							//related dari dokumen lain ke dokumen ini
							dbUpdate('document_related', array('document2' => $new_id), "document_id=$document_id");
							// $relatedDocument = $this->input->post('related',true);
							// $this->model->setRelatedDocument($insert,$relatedDocument);

							$data = array();
			                $data['user_id'] = $doc['user_id'];
			                $data['user_jabatan_id'] = $doc['user_jabatan_id'];
			                dbUpdate('documents', $data, "id=$new_id");

							SessionManagerWeb::setFlashMsg(true, 'Success to evaluate document. Revision is needed for this document.');
							// echo $new_id;
							// die();
							// redirect("web/document/detail/".$new_id);
						}
			    	}
    			}
    		break;
    		case '3':
    			switch($urutan_now){
    				// echo "masuk switch urutan now<br>";
    				case '1':
    					if ($status_now==Document_model::STATUS_REVISE){
    						$this->addHistory($document_id, "Revise Document");
    					} else {
    						$this->addHistory($document_id, "Submitted Document Revision");
    					}
    					// $creator_id = $this->model->getOne($document_id,'creator_id');
    					// $user_id = $this->model->getOne($document_id,'user_id');
	    				// if ($creator_id==$user_id){
    					// 	$this->Document_evaluation_model->updateDateBy($document_id,'creator_date');
    					// }
    				break;
    				default:
	    				$this->addHistory($document_id, "Already ".$task['name']." document");
    				break;
    			}
    			switch($data['workflow_urutan']){
    				// echo "masuk switch workflow urutan<br>";
    				case '3':
    					$creator_id = $this->model->getOne($document_id,'creator_id');
    					$user_id = $this->model->getOne($document_id,'user_id');
    					if ($creator_id==$user_id and $type['need_reviewer']=='0' and $data_sebelum['DRAFTER_EVALUATION'] == null){
    						$data['workflow_urutan']++;
    					}
    				break;
    			}
    			if ($data['workflow_urutan']==4){
    				$this->load->model('Document_verification_model');
    				$this->Document_verification_model->delete($document_id);
    			}
    			
	    		if ($data['status']==Document_model::STATUS_REJECT){ // revisi
	    			// Obsoletekan Document Baru
	    			$data['is_published'] = 1;
	    			$data['is_obsolete'] = 1;

	    			// Publish Parent
	    			$previous_document = $this->model->getOne($document_id, 'parent');
	    			$prev_doc = array();
	    			$prev_doc['workflow_id'] = Workflow_model::PENGAJUAN;
	    			$prev_doc['status'] = Document_model::STATUS_SELESAI;

	    			// Cek apakah masih valid atau tidak
	    			$diff = $this->model->getOne($document_id, "(to_date(TO_CHAR(validity_date, 'MM-DD-YYYY'), 'MM-DD-YYYY')-sysdate) as diff");
		            if ($diff<=0){
		                $prev_doc['workflow_id'] = Workflow_model::EVALUASI;
		    			$prev_doc['status'] = Document_model::STATUS_BARU;
		    			$prev_doc['workflow_urutan'] = 1;
		    			$this->load->model("Document_evaluation_model");
		    			$this->Document_evaluation_model->deleteWith($previous_document);
		            }
	    			dbUpdate("documents", $prev_doc, "id='$previous_document'");

	    			$this->addHistory($document_id,"This Document revision is rejected because ".$this->input->post('text', true));
	    		}
	    		if ($data['status']==Document_model::STATUS_SELESAI){ // revisi
	    			$data['is_published'] = 1;
	    			$revisi_ke=(int)$this->model->getOne($document_id, 'revision');
	    			$file_explode = explode('.', $this->model->getOne($document_id, 'document')); 
					$code_explode = explode('/',$this->model->getOne($document_id, 'code'));
					$code_implode = implode('_',$code_explode);
					$append_id = "_".$document_id;
					if ($type_id==Document_model::BOARD_MANUAL){
						$append_id = '';
					}
	    			$prev = $code_implode.'_Revise'.$revisi_ke.$append_id.'.'.end($file_explode);
	    			$data['show']=1;
	    			$data['revision'] = $revisi_ke+1;
					$data['document'] = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
					$data['drafter_evaluation'] = null;
	    			$this->model->updateValidityDate($document_id);
	    			$this->changeName("document/files/", $prev, $data['document']);
	    			if(end($file_explode) == 'xlsx' || end($file_explode) == 'xls'){
	    				$prev_pdf = $code_implode.'_Revise'.$revisi_ke.$append_id.'.pdf';
	    				$new_pdf = $code_implode.'_R'.$data['revision'].'.pdf';
	    				$this->changeName("document/files/", $prev_pdf, $new_pdf);
	    			}
	    			// $this->copyName("document/files/", $prev, $data['document']);

	    			$is_upload = $this->model->getOne($document_id, 'is_upload');
	    			if ($is_upload==0){
	    				$prev_txt = $code_implode.'_Revise'.$revisi_ke.$append_id.'.txt';
	    				$new_txt = $code_implode.'_R'.$data['revision'].'.txt';
	    				$this->changeName("document/files/", $prev_txt, $new_txt);
	    				// $this->copyName("document/files/", $prev_txt, $new_txt);
	    			}
	    			// $this->model->updateDateBy($document_id, 'published_at');

	    			// Obsoletekan Parent
	    			$previous_document = $this->model->getOne($document_id, 'parent');
	    			$this->model->setToObsolete($previous_document);
	    			


	    			// Related dari dokumen lama ke dokumen lain
					$relatedDocument = dbGetRows("SELECT DOCUMENT2 as document2 FROM DOCUMENT_RELATED WHERE DOCUMENT1=".$parent);
					$related = array();
					for ($i=0; $i < count($relatedDocument) ; $i++) { 
						$related[$i]['document2'] = $relatedDocument[$i]['DOCUMENT2'];
					}
					$this->model->setRelatedDocument2($document_id,$related);


	    			// Related Document
	    			$sql_parent = "select parent from documents where id=".$document_id;
					// echo ($sql_new_id);
					$parent = dbGetOne($sql_parent);
					// echo "parent:".$parent;
		    		$update = "update DOCUMENT_RELATED set document2 =".$document_id." where document2=".$parent;
					// echo "query update :".$update;
		    		dbQuery($update);
		   //  		$ciConfig = $this->config->item('utils');
					// $full_path = $ciConfig['full_upload_dir'].'document/files/';
		   //  		$relatedDocument2 = dbGetRows("SELECT r.DOCUMENT1 as \"document_id\", d.document as \"document\" FROM DOCUMENT_RELATED r LEFT JOIN DOCUMENTS d ON r.DOCUMENT1 = d.id WHERE r.DOCUMENT2=".$parent);
					// foreach ($relatedDocument2 as $related) {
					// 	$doc_name = explode('.', $related['document']);
					// 	unlink($full_path.'./assets/uploads/document/files/'.$doc_name[0].'_header.pdf');
					// 	dbUpdate("documents", array('have_header' => 0), "id=".$related['document_id']);
					// }

	    			// die("MASUK KE SINI") ;
	    			// dbUpdate("DOCUMENT_RELATED",$record,"document2=$document_id");

	    			$this->addHistory($document_id,"This Document is done revised");
	    		}
    		break;
    	}
    	// if ($data['workflow_urutan']=='-1'){
    	// 	$this->updateBecauseReject($document_id);
    	// }
    	if ($this->Document_model->updateDocument(SessionManagerWeb::getUserID(),$document_id, $data)){
    		$data_baru = $this->model->getDocumentFlow($document_id);
    		// echo "<pre>";print_r($document_id);echo "</pre>";
    		// echo "<pre>";print_r($data_baru);echo "</pre>";
    		// echo "<pre>";print_r($data_sebelum);echo "</pre>";
			$this->inputDocLog($document_id, $data_baru,$data_sebelum,$data['status']);
    		$this->load->model('Notification_model');
    		$this->Notification_model->generate(Notification_model::ACTION_DOCUMENT_STATUS_CHANGED, $document_id, $data['updated_by']);
    		// return $document_id;
    		// return true;
    	}
    	// return false; 
    	echo $document_id;
    }

    public function getBy($id){
    	echo '<pre>';
    	vaR_dump($this->model->getBy($id));
    	die();
    }

    private function addNewDocument($data){
		$insert = $this->model->create($data, TRUE, TRUE);//Memasukkan data dokumen ke db
        if ($insert) {
	        SessionManagerWeb::setFlashMsg(true, 'Success to create new document');
	        return $insert;
		}
		// return false;
    }

    public function ajaxEvaluateDocument(){
    	$document_id = (int)$this->input->post('id', true);
    	$access_eval = (int)$this->input->post('access_eval', true);

    	$this->load->model('Workflow_model');
    	$data = array();
    	$data['workflow_id'] = Workflow_model::EVALUASI;
    	
    	$data['status'] = Document_model::STATUS_BARU;
    	// $data['user_id'] = $this->model->getOne($document_id, 'creator_id');
    	$data['user_jabatan_id'] = $this->model->getOne($document_id, 'creator_jabatan_id');
    	if($access_eval == 1){
    		$data['drafter_evaluation'] = $this->input->post('drafter_eval', true);
    		$data['workflow_urutan'] = 0;
    	}
    	else{
    		$data['workflow_urutan'] = 1;
    	}
    	if (dbUpdate('documents', $data, "id=$document_id")){
    		$this->deleteEvaluateDocument($document_id);
    		$this->load->model('Document_reviewer_model');
			$data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, Workflow_model::PENGAJUAN);
			if ($data_reviewer==NULL){
				$data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, Workflow_model::REVISI);
			}
			$this->Document_reviewer_model->create($document_id,Workflow_model::EVALUASI, $data_reviewer);
			$data_baru = $this->model->getDocumentFlow($document_id);
			$this->inputDocLog($document_id,$data_baru,0,$data['status']);
    	}
    	echo $document_id;
    	// $this->addHistory($document_id, "Menolak dokumen ini karena ".$this->input->post('text', true));
    }

    public function tesNotifDownload(){
    	$this->load->model('Notification_model');
        $this->Notification_model->generate(Notification_model::ACTION_ASK_DOWNLOAD, 45, SessionManagerWeb::getUserID());
    }

    public function download_document($id){
    	$this->load->model('Document_download_model');
    	$document_id = $this->Document_download_model->filter(" where id='$id' ")->getOne('document_id');
    	redirect("web/document/download_watermark/".$document_id);
    }


    public function checkAskDownloadExist(){
    	$document_id = (int)$this->input->post('id', true);
    	$this->load->model('Document_download_model');
    	$exist = $this->Document_download_model->filter("where document_id='".$document_id."' and user_id='".SessionManagerWeb::getUserID()."'")->getOne('id');
    	if($exist){
    		echo $exist;
    	}
    	else{
    		echo 0;
    	}
    	// echo $exist/;
    }
    public function ajaxAskDownload(){
    	$document_id = (int)$this->input->post('id', true);
    	$data = array();
    	$data['document_id'] = $document_id;
    	$data['user_id'] = SessionManagerWeb::getUserID();
    	$data['reason'] = $this->input->post('reason', true);
    	$this->load->model('Document_download_model');
    	$exist = $this->Document_download_model->filter("where document_id='".$document_id."' and user_id='".SessionManagerWeb::getUserID()."'")->getOne('id');
    	if($exist){
    		$last_id =  $exist;
    		$update['is_allowed'] = 0;
    		$update['valid_until'] = null;
    		$update['reason'] = $data['reason'];
    		// $this->Document_download_model->updateParentFunction($last_id,$update);
    	}
    	else{
    		$this->Document_download_model->insert($data);
    		$last_id = $this->Document_download_model->getLastId();
    	}
    	// die();
    	$this->addActivity($document_id, "Ask To Download this Document.");
    	$this->load->model('Notification_model');
        $this->Notification_model->generate(Notification_model::ACTION_ASK_DOWNLOAD, $last_id, SessionManagerWeb::getUserID());

    	echo $last_id;
    }

    public function ajaxAllowDownload(){
    	$document_id = (int)$this->input->post('id', true);
    	$data = array();
    	$data['is_allowed'] = 1;
    	$this->load->model('Document_download_model');
    	$this->Document_download_model->update($document_id, SessionManagerWeb::getUserID(),$data);
    	$this->addActivity($document_id, "Ask To Download this Document.");

    	echo $document_id;
    }

    public function ajaxSetReviseDocument(){
    	$document_id = (int)$this->input->post('id', true);

    	$this->load->model('Workflow_model');
    	$data = array();
		$data['workflow_id'] = Workflow_model::REVISI;
    	$data['workflow_urutan'] = 1;
    	$data['status'] = Document_model::STATUS_BARU;
    	// if (dbUpdate('documents', $data, "id=$document_id")){
    	// 	$this->deleteEvaluateDocument($document_id);
    	// }
    	dbUpdate('documents', $data, "id=$document_id");
    	echo $document_id;
    	// $this->addHistory($document_id, "Menolak dokumen ini karena ".$this->input->post('text', true));
    }

    public function revisenow(){
    	$document_id = (int)$this->input->post('id', true);
    	$getNewid_sql = 'select id from documents where rownum<=1 and parent='.$document_id." order by id desc";
    	$new_id = dbGetOne($getNewid_sql);
    	echo $new_id;
    }

    public function deleteEvaluateDocument($document_id){
    	$workflow_id = 2;
    	
    	// Delete Form 
    	$this->load->model('Document_evaluation_model');
    	$this->Document_evaluation_model->deleteWith($document_id);

    	// Delete Reviewer
    	$this->load->model('Document_reviewer_model');
    	$this->Document_reviewer_model->deleteWith($document_id, $workflow_id);

    	// Delete Review
    	$this->load->model('Document_reviews_model');
    	$this->Document_reviews_model->deleteWith($document_id, $workflow_id);
    }

    public function updateBecauseRevise($id){
    	// $this->load->model('Document_delegates_model');
    	// $this->Document
    	$this->load->model('Document_reviews_model');
    	$workflow_id = $this->model->getOne($id, 'workflow_id');
    	$this->Document_reviews_model->filter(" document_id=$id and workflow_id=$workflow_id")->delete($id);
    }

    public function delete($id){
    	// dbQuery("delete from documents where id=$id");
    	if ($this->model->delete($id)){
    		SessionManagerWeb::setFlashMsg(true, 'Success to delete document');
    		redirect("web/document/dashboard");
    	} else {
    		SessionManagerWeb::setFlashMsg(false, 'Failed to delete document');
    		redirect("web/document/detail/".$id);
    	}
    }

    public function setReason($document_id){
    	// $document_id = $this->input->post('document_id', true);
    	$data['reason'] = $this->input->post('reason_textarea', true);
    	$this->model->updateDocument(SessionManagerWeb::getUserID(),$document_id, $data);
    	redirect("web/document/detail/".$document_id);
    }

    public function ajaxsetreason(){
    	$document_id = $this->input->post('document_id', true);
    	$data['reason'] = $this->input->post('reason', true);
    	if ($this->model->updateDocument(SessionManagerWeb::getUserID(),$document_id, $data)){
    		// if ($this->_moveDocumentTemp($document_id, 'document/temp')) {
    		// 	return true;
    		// }
    	}
    }

    public function ajaxinsertdocumentform(){
    	$document_id = $this->input->post('document_id', true);
    	$user_id = $this->model->getOne($document_id, 'user_id');
    	$data = $this->input->post(null, true);
		$data['user_id'] = SessionManagerWeb::getUserID();
    	$data = array_change_key_case($data, CASE_UPPER);
    	if (dbInsert('document_forms',$data)){
    		if ($this->_moveDocumentTemp($document_id, 'document/temp',$user_id)) {
    			return true;
    		}
    	}
    }

    public function testGetFilename($document_id){
    		die("you shall not pass!");
	        $ciConfig = $this->config->item('utils');
	        $folder_dest = 'document/files';
            $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
            $file = $this->model->getOne($document_id, 'document');
            $files = explode('.', $file);

            // remove extension
            unset($files[count($files)-1]);
            $file_name = implode('.', $files).'.txt';
            die($file_name);
    }

    public function updateDocumentFiles(){
    	$document_id = $this->input->post('document_id', true);
    	$user_id = $this->model->getOne($document_id, 'user_id');

    	if ($this->input->post('document_text')!=NULL and $this->input->post('document_text')!=""){
    		if ($user_id==NULL)
	            $user_id = SessionManagerWeb::getUserID();
	        $id = md5($user_id. $this->config->item('encryption_key'));
	        $ciConfig = $this->config->item('utils');
	        $folder_dest = 'document/files';
            $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
            $file = $this->model->getOne($document_id, 'document');
            $revise = $this->model->getOne($document_id, 'revision');
            // $basename = basename($file);
            // $fil = basename($file);
            // $hash = hash_hmac('sha256',$fil.$document_id, $user_id);
            // $file_name = File::getFileName($hash, $folder_dest) . '.txt';
            $files = explode('.', $file);
            // remove extension
            unset($files[count($files)-1]);
            // $file_name = implode('.', $files).'_R'.$revise.'.txt';
            $file_name = implode('.', $files).'.txt';
            file_put_contents($path_dest.$file_name, $this->input->post('document_text'));
    	}
		if ($this->_moveDocumentTemp($document_id, 'document/temp',$this->model->getOne($document_id, 'code') , NULL)) {
			echo true;
		} else {
			echo false;
		}
    }

    public function moveDocumentReview(){
    	$id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                
                    
                $folder_dest = 'document/review';
                $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                $basename = basename($file);
                $fil = basename($file);
                $hash = hash_hmac('sha256',$fil, SessionManagerWeb::getUserID());
                $file_name = File::getFileName($hash, $folder_dest) . '.backup';
                
                if (!in_array($fil, $arr_files)) {
                    $arr_files[] = $fil;
                    if (!$this->model->updateDocumentWithFile($document_id, $fil, $file)) {
                        return false;
                    }
                }
                if (!rename($file, $path_dest . '/' . $file_name)) {
                    return false;
                }
            }
        }else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

	function docx2text() {
		$filename = $this->input->post('filename', true);
	   return readZippedXML($filename, "word/document.xml");
	 }

	function readZippedXML($archiveFile, $dataFile) {
		// Create new ZIP archive
		$zip = new ZipArchive;

		// Open received archive file
		if (true === $zip->open($archiveFile)) {
		    // If done, search for the data file in the archive
		    if (($index = $zip->locateName($dataFile)) !== false) {
		        // If found, read it to the string
		        $data = $zip->getFromIndex($index);
		        // Close archive file
		        $zip->close();
		        // Load XML from a string
		        // Skip errors and warnings
		        $xml = new DOMDocument();
		    $xml->loadXML($data, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);
		        // Return data without XML formatting tags
		        return strip_tags($xml->saveXML());
		    }
		    $zip->close();
		}

		// In case of failure return empty string
		return "";
	}

	public function read_doc() {
	    $fileHandle = fopen($this->filename, "r");
	    $line = @fread($fileHandle, filesize($this->filename));   
	    $lines = explode(chr(0x0D),$line);
	    $outtext = "";
	    foreach($lines as $thisline)
	      {
	        $pos = strpos($thisline, chr(0x00));
	        if (($pos !== FALSE)||(strlen($thisline)==0))
	          {
	          } else {
	            $outtext .= $thisline." ";
	          }
	      }
	     $outtext = preg_replace("/[^a-zA-Z0-9\s\,\.\-\n\r\t@\/\_\(\)]/","",$outtext);
	    return $outtext;
	}

	public function read_docx($id=NULL){
		$ciConfig = $this->config->item('utils');
		if ($id!=NULL){
			$document = $this->model->getDocumentWithLink($id);
			$filename = $document['link'];
		} else {
			$filename = $this->config->item('path').'/test.docx';
		}
		// $filename = $this->config->item('path').'/test.docx';
		// $filename = $ciConfig['full_upload_dir'].'document/files/'.$document;
		
		// $filename = 'C:/www/semen_indonesia/test.docx';
		
        $striped_content = '';
        $content = '';

        $zip = zip_open($filename);

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);

        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $striped_content = strip_tags($content);
        // echo '<pre>';
        // vaR_dump($striped_content);
        // die();
        echo nl2br($striped_content);
    }

    public function update($document_id){
    	// echo "<pre>";print_r($this->input->post('related'));die();
    	// die();
    	// echo '<pre>';
    	// var_dump($this->input->post(null, true));
    	// die();
   		// Update data dokumen
   		$data = array();
		$data['code'] = $this->input->post('kodeAdministratif', true);
		$data['prosesbisnis_id'] = $this->input->post('prosesbisnis',true);
        // $data['user_id'] = SessionManagerWeb::getUserID();
        $data['description'] = $this->input->post('input_deskripsi',true);
        $data['title'] = $this->input->post('input_judul',true);
        $data['retension'] = (int)$this->input->post('retension_input',true);
        if ($this->input->post('retension_select',true)=='1'){
        	$data['retension'] *= 12;
        }
		// $data['creator_id'] = $this->input->post('input_creator',true);
		$data['approver_id'] = $this->input->post('approver_id',true);
		foreach ($data as $key => $value) {
			if ($value==NULL)
				unset($data[$key]);
		}

		$data = array_change_key_case($data, CASE_UPPER);
		// echo "<pre>";print_r($this->input->post());die();
   		if (isset($data['CODE'])){
	   		$this->checkCode($document_id, $data['CODE']);
	   	}
   		$this->model->save($document_id, $data, true);

   		// Check apakah kode berubah
   		// if (isset($data['CODE'])){
   		// 	$ciConfig = $this->config->item('utils');
   		// 	$filename = $this->model->getOne($document_id, 'document');
   		// 	$full_path = $ciConfig['full_upload_dir'].'document/files/';
   		// 	$full_filename = $full_path.$filename;
   		// 	$file_split = explode('.', $filename);
   		// 	$ext_file = $file_split[1];
   		// 	// $split_name = explode('/', $document_code);
     //        $revision = '_R'.$this->model->getOne($document_id, 'revision');
	    //     // $file_name = $pecah_nama[0].'_'.$pecah_nama[1]. '_'.$pecah_nama[2].'_'.$pecah_nama[3].'_'.$revision.'.'.$ext_file;
	    //     $code_split = explode('/', $data['CODE']);
	    //     $file_name = implode('_', $code_split);
	    //     $file_name .= $revision.'.'.$ext_file;
   		// }

   		$workflow_id = $this->model->getOne($document_id, 'workflow_id');

   		// already review
		$this->load->model('Document_reviews_model');
    	$review = $this->Document_reviews_model
    									->column(array(
    										"dr.document_id" => "\"document_id\"",
									        "dr.document_reviewer" => "\"document_reviewer\"",
									        "dr.review" => "\"review\"",
									        "u.name" => "\"reviewer_name\"",
									        "dr.is_agree" => "\"is_agree\"",
									        "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
    									))
    									->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
    									->filter(" dr.document_id=$document_id and workflow_id=".$workflow_id)
    									->getAll();
		// reviewed
		$reviewed = array();
    	foreach ($review as $key => $review) {
    		$reviewed[] = $review['document_reviewer'];
    	}
    	// $counter['reviewed'] = count($this->data['data']['reviewed']);

		// Add Reviewer
		$this->load->model('Document_types_model');
		$type_id = $this->model->getOne($document_id, 'type_id');
		if ($this->Document_types_model->getBy($type_id, 'need_reviewer')){ // jika dia tidak butuh reviewer
			if ($this->input->post('input_reviewer', true)!=NULL or $reviewed!=NULL){
				$this->load->model('Document_reviewer_model');
				$reviewer = $this->input->post('input_reviewer', true);
				foreach ($reviewed as $rev) {
					if (!in_array($rev, $reviewer)){
						$reviewer[] = $rev;
					}
				}
				$this->Document_reviewer_model->deleteWith($document_id, $workflow_id);
				if ($reviewer!=NULL)
					$this->Document_reviewer_model->create($document_id, $workflow_id, $reviewer);
			} else {
				SessionManagerWeb::setFlashMsg(false, 'Reviewer cant be Emplty!');
		   		redirect('web/document/detail/'.$document_id);
			}
		}
		

		
		
		//Change Title
		if($this->input->post('title', true)!=NULL){
   			$title = $this->input->post('title', true);
   			$this->model->updateTitle($document_id,$title);
   			$id = SessionManagerWeb::getUserID();
   			$this->load->model('User_model');
   			$name = $this->User_model->filter("WHERE ID=$id")->getOneFilter('NAME');
   			$this->addHistory($document_id, "Changed title attribute of this document to \"".$title."\"");
   		}

		// Add unitkerja terkait
		if ($this->input->post('input_unitkerjaterkait', true)) {
			$this->load->model('Document_unitkerja_model');
			$unitkerja = $this->input->post('input_unitkerjaterkait', true);
			$this->Document_unitkerja_model->deleteWith($document_id);
			if ($unitkerja!=NULL)
				$this->Document_unitkerja_model->create($document_id, $unitkerja);
		}
					
		// ISO
		if ($this->input->post('input_iso', true)){
			$data_iso = array();
			$data_iso['document_id'] = $document_id;
			$iso_id = $this->input->post('input_iso', true);
			$this->deleteDocumentISOS($document_id);
			foreach ($iso_id as $iso_id_key => $iso_id_value) {
				$data_iso['iso_id'] = $iso_id_value;
				dbInsert("DOCUMENT_ISOS", $data_iso);
			}			
		}


		// Klausul
		if ($this->input->post('input_iso_klausul', true)){
			$this->load->model('Document_types_model');
			$type_id = $this->model->getOne($document_id, 'type_id');
			$need_klausul = $this->Document_types_model->getBy($type_id, 'klausul');
			if ($need_klausul){
				$data_klausul['document_id'] = $document_id;
				$klausul = $this->input->post('input_iso_klausul', true);
				foreach ($klausul as $key_klausul => $value_klausul) {
					$data_klausul['klausul_id'] = $value_klausul;
					dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
				}
			}
		}

		// Related
		if ($this->input->post('related',true)){
			$relatedDocument = $this->input->post('related',true);
			$this->model->updateRelatedDocumentNew($document_id,$relatedDocument);
			// $this->model->setRelatedDocument($document_id,$relatedDocument);			
		}
		else{
			dbQuery("delete from document_related where document1=".$document_id);
		}


		// Activity
		$this->load->model('Document_activity_model');
		$activity["user_id"] = SessionManagerWeb::getUserID();
		$activity["document_id"] = $insert;
		$activity["activity"] = "Mengganti atribut dokumen";
		$activity["type"] = Document_activity_model::HISTORY;
		$activity = array_change_key_case($activity, CASE_UPPER);
		$this->Document_activity_model->create($activity,TRUE,TRUE);

		SessionManagerWeb::setFlashMsg(true, 'Success to change the document attributes');
   		header('Location: ' . site_url('web/document/detail').'/'.$document_id);
   	}

   	public function checkCode($document_id, $code){
   		$code_now = $this->model->getOne($document_id, 'code');
   		if ($code_now!=$code){
			$ciConfig = $this->config->item('utils');
			$filename = $this->model->getOne($document_id, 'document');

			$full_path = $ciConfig['full_upload_dir'].'document/files/';
			$full_filename = $full_path.$filename;
			$file_split = explode('.', $filename);

			$ext_file = end($file_split);
		    
			// Untuk penamaan file
		    $revision = '_R'.$this->model->getOne($document_id, 'revision');
		    $code_split = explode('/', $code);
		    $file_name = implode('_', $code_split);
		    $f_name = $file_name;
		    $file_name .= $revision.'.'.$ext_file;
		    $full_new_filename = $full_path.$file_name;

		    $user_id = $this->model->getOne($document_id, 'user_id');

		    if (file_exists($full_filename)){
			    rename($full_filename, $full_new_filename);
			    if ($ext_file=='xls' or $ext_file=='xlsx'){
			    	$exp_old_filename = explode('.', $filename);
			    	unset($exp_old_filename[count($exp_old_filename)-1]);
			    	$old_filename = $full_path.implode('.', $exp_old_filename).'.pdf';
			    	$new_filename = $full_path.$f_name.$revision.'.pdf';
			    	rename($old_filename, $new_filename);
			    }
			    if ($this->model->getOne($document_id, 'is_upload')=='0'){
			    	$exp_old_filename = explode('.', $filename);
			    	unset($exp_old_filename[count($exp_old_filename)-1]);
			    	$old_filename = $full_path.implode('.', $exp_old_filename).'.txt';
			    	$new_filename = $full_path.$f_name.$revision.'.txt';
			    	rename($old_filename, $new_filename);
			    }
			    $this->model->updateDocumentWithFile($document_id, $file_name, $full_new_filename, $user_id);
		    }


		    // echo '<br>'.$full_filename;
		    // echo '<br>'.$full_new_filename;
		    // die();
   		}
   	}

   	public function tesChangeName(){
   		$document_id = 5012;
   		$code_explode = explode('/',$this->model->getOne($document_id, 'code'));
		$code_implode = implode('_',$code_explode);
		// echo $code_implode;
   		// $code_explode = 
   		$prev_pdf = $code_implode.'_Revise'.$revisi_ke.$append_id.'.pdf';
	   	$new_pdf = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
   		// $this->changeName("document/files/", "M_2000_FNC_50039258_.docx", "test_ganti.docx");
   	}

   	protected function changeName($path, $prev, $after){
   		$ciConfig = $this->config->item('utils');
   		$full_path = $ciConfig['full_upload_dir'].$path;
   		$full_filename = $full_path.$prev;
   		if (file_exists($full_filename)){
   			$full_new_filename = $full_path.$after;
		    rename($full_filename, $full_new_filename);
	    }
   	}

   	protected function copyName($path, $prev, $after){
   		$ciConfig = $this->config->item('utils');
   		$full_path = $ciConfig['full_upload_dir'].$path;
   		$full_filename = $full_path.$prev;
   		if (file_exists($full_filename)){
   			$full_new_filename = $full_path.$after;
		    copy($full_filename, $full_new_filename);
	    }
   	}

   	private function deleteDocumentISOS($document_id){
        $sql ="delete from document_isos where document_id=$document_id";
        return dbQuery($sql);
   	}

   	public function ajaxAddReviewer(){
   		$document_id = $this->input->post('document_id');
		$this->load->model('Document_reviewer_model');
		$reviewer = $this->input->post('reviewer', true);
		$workflow_id = $this->model->getOne($document_id, 'workflow_id');

		$this->load->model('Document_reviews_model');
    	$review = $this->Document_reviews_model
    									->column(array(
    										"dr.document_id" => "\"document_id\"",
									        "dr.document_reviewer" => "\"document_reviewer\"",
									        "dr.review" => "\"review\"",
									        "u.name" => "\"reviewer_name\"",
									        "dr.is_agree" => "\"is_agree\"",
									        "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
    									))
    									->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
    									->filter(" dr.document_id=$document_id and workflow_id=".$workflow_id)
    									->getAll();
		// reviewed
		$reviewed = array();
    	foreach ($review as $key => $review) {
    		$reviewed[] = $review['document_reviewer'];
    	}
		// $reviewer = explode(',', $reviewer);
		$this->Document_reviewer_model->deleteWith($document_id, $workflow_id);
		if ($reviewer!=NULL or $reviewed!=NULL){
			foreach ($reviewed as $rev) {
				if (!in_array($rev, $reviewer)){
					$reviewer[] = $rev;
				}
			}
			$this->Document_reviewer_model->create($document_id, $workflow_id, $reviewer);
			$this->addHistory($document_id, "Added Reviewer ");
		}
   	}

    // NEW AJAX
   	public function ajaxSetDelegates(){
   		$data = $this->input->post(null, true);
   		$this->load->model('Document_delegates_model');
   		echo $this->Document_delegates_model->deleteBy($data);
   		if ($this->Document_delegates_model->create($data)){

   			// change drafter to delegated
   			if ($data['workflow_urutan']==1){
	   			$record = array();
	   			$record['user_id'] = $data['delegate'];
	   			$this->load->model('User_model');
	   			$record['user_jabatan_id'] = $this->User_model->getBy($record['user_id'], 'user_jabatan_id');
	   			dbUpdate('Documents', $record, "id=".$data['document_id']);   				
   			}

   			// add History
   			$this->load->model('Workflow_model');
   			$task = $this->Workflow_model->getTasks($data['workflow_id'],$data['workflow_urutan']);
   			$this->load->model('User_model');
   			$delegate_name = $this->User_model->filter("where \"user_id\"=".$data['delegate'])->getOneFilter( "name");
   			$this->addHistory($data['document_id'], "Delegates ".$delegate_name." to ".$task['name']);

   			$this->load->model('Notification_model');
            $this->Notification_model->generate(Notification_model::ACTION_DELEGATE, $data['document_id'], SessionManagerWeb::getUserID());
            echo true;
   		}
   	}

   	
   	public function ajaxSetReview(){
   		$data = $this->input->post(null, true);

   		$data['workflow_id'] = $this->model->getOne($data['document_id'], 'workflow_id');
   		if (dbInsert('document_reviews', $data)){
   			// $is_agree = "<b style='color:red'>Tidak Setuju</b>";
   			$is_agree = "Disagree";
   			if ($data['is_agree']=='1'){
   				// $is_agree = "<b style='color:green'>Setuju</b>";
   				$is_agree = "Agree";
   			}
   			$this->addHistory($data['document_id'], "Is ".$is_agree." and give review \"".$data['review']."\"");
   // 			$data_lama['USER_ID'][0] = $data['document_reviewer'];
   // 			$data_lama['WORKFLOW_ID'] = $data['workflow_id'];
			// $data_lama['WORKFLOW_URUTAN'] = 2;
   // 			$this->inputDocLog($data['document_id'], 0,$data_lama);

   			$this->load->model('Document_reviewer_model');
	    	$reviewer = $this->Document_reviewer_model->getAllBy($data['document_id'], $data['workflow_id']);
	    	// count reviewer
			$counter['reviewer'] = count($reviewer);

	    	$this->load->model('Document_reviews_model');
	    	$review = $this->Document_reviews_model
	    									->column(array(
	    										"dr.document_id" => "\"document_id\"",
										        "dr.document_reviewer" => "\"document_reviewer\"",
										        "dr.review" => "\"review\"",
										        "u.name" => "\"reviewer_name\"",
										        "dr.is_agree" => "\"is_agree\"",
										        "TO_CHAR(dr.created_at, 'YYYY-MM-DD HH24:MI:SS')" => "\"created_at\""
	    									))
	    									->with(array('name' => 'users', 'initial' => 'u'), "u.id=dr.document_reviewer")->table('document_reviews dr')
	    									->filter(" dr.document_id=".$data['document_id']." and workflow_id=".$data['workflow_id'])
	    									->getAll();
	    	// count jml review
	    	$counter['reviewed'] = count($review);

	    	if ($counter['reviewed']>=$counter['reviewer'])
	   			return true;
   		}
   	}

   	public function _isAllowReview($reviews, $reviewers){
   		$counter = 0;
   		foreach ($reviews as $key => $review) {
   			if (in_array($review['document_reviewer'], $reviewers))
   				$counter++;
   		}
   		if ($counter>0)
   			return false;
   		return true;
   	}

   	public function popularDocument($id){
   		unset($_SESSION['document_search']);
   		$document = $this->model->getBy($id);
   		$document = array_change_key_case($document, CASE_LOWER);
   		$_SESSION['document_search']['page'] =1;
   		$_SESSION['document_search']['arr_type'] = array(0=>$document['type_id']);
   		$_SESSION['document_search']['type'] = $document['type_id'];
   		$_SESSION['document_search']['arr_unitkerja'] = array(0=>$document['unitkerja_id']);
   		$_SESSION['document_search']['unitkerja'] = $document['unitkerja_id'];

   		if ($document['title']!=NULL and $document['title']!=''){
	   		$_SESSION['document_search']['title'] = $document['title'];
   		}

   		$_SESSION['selected_menu']='all';

   		header('Location: ' . site_url("web/".$this->class.'/index'));
   	}

   	public function searchDocumentById($id){
   		if ($id==NULL){
   			SessionManagerWeb::setFlashMsg(false, "Id cant be empty!");
   			$this->redirect();
   		}

   		$documents = $this->model->show_sql(false)->table("documents_view")->column("*")->filter("where \"document_id\"=$id ")->order("order by \"document_id\"")->getAll();
   		$document = $documents[0];

   		if ($document==NULL or $document==false){
   			SessionManagerWeb::setFlashMsg(false, "No Documents was found!");
   			$this->redirect();
   		}

   		unset($_SESSION['document_search']);
   		$_SESSION['document_search']['page'] =1;
   		$_SESSION['document_search']['arr_unitkerja'][] = $document['unitkerja_id'];
   		$_SESSION['document_search']['unitkerja'] = implode(',',$_SESSION['document_search']['arr_unitkerja']);
   		$_SESSION['document_search']['title'] = $document['title'];
   		$_SESSION['document_search']['arr_type'][] = $document['type_id'];
   		$_SESSION['document_search']['type'] = $document['type_id'];

   		header('Location: ' . site_url("web/".$this->class));
   	}

   	public function setSessionSearch($method=NULL){
   		if(count($this->input->post()) == 1){
   			if ($this->input->post('keyword', true)!=NULL and $this->input->post('keyword', true)!=''){//from search navbar
		   		$_SESSION['document_search']['keyword'] = $this->input->post('keyword', true);
		   	}
   		}
   		else{
   			// unset($_SESSION['document_search']);
	   		if ($this->input->post('numPage', true)!=NULL and $this->input->post('numPage', true)!='')
		   		$_SESSION['page'] = $this->input->post('numPage', true);
		   	else 
		   		$_SESSION['document_search']['page'] =1;

	   		if ($this->input->post('keyword', true)!=NULL and $this->input->post('keyword', true)!='')//from search navbar
		   		$_SESSION['document_search']['keyword'] = $this->input->post('keyword', true);

	   		if ($this->input->post('title', true)!=NULL and $this->input->post('title', true)!='')//from filter keyword
		   		$_SESSION['document_search']['title'] = $this->input->post('title', true);

		   	if ($this->input->post('type', true)!=NULL and $this->input->post('type', true)!=''){ // from filter type
		   		$_SESSION['document_search']['arr_type'] = $this->input->post('type', true);
		   		$_SESSION['document_search']['type'] = implode(',',$_SESSION['document_search']['arr_type']);
		   		
		   	}

		   	if($this->input->post('status', true)!=NULL and $this->input->post('status', true)!=''){
		   		$_SESSION['document_search']['arr_status'] = $this->input->post('status', true);
		   		$_SESSION['document_search']['status'] = implode(',',$_SESSION['document_search']['arr_status']);
		   	}

		   	if ($this->input->post('unitkerja', true)!=NULL and $this->input->post('unitkerja', true)!=''){
		   		$_SESSION['document_search']['arr_unitkerja'] = $this->input->post('unitkerja', true);
		   		$workunits = array();
		   		foreach ($_SESSION['document_search']['arr_unitkerja'] as $k_unitkerja => $v_unitkerja) {
		   			$workunits[]="'".$v_unitkerja."'";
		   		}
		   		$_SESSION['document_search']['unitkerja'] = implode(',',$workunits);
		   	}

		   	if($this->input->post('datestart', true)!=NULL and  $this->input->post('datestart', true)!='') // from datestart
		   		$_SESSION['document_search']['datestart'] = $this->input->post('datestart', true);

		   	if($this->input->post('dateend', true)!=NULL and  $this->input->post('dateend', true)!='') // from dateend
		   		$_SESSION['document_search']['dateend'] = $this->input->post('dateend', true);

	   		if (isset($_SESSION['document_search']['type']) and $_SESSION['document_search']['type']!=''){
	   			$_SESSION['selected_menu']="type_".$_SESSION['document_search']['type'];
	   		}
	   		// if (isset($_SESSION['document_search']['keyword']))
	   		// 	$method = 'index?selected_menu=all&keyword='.$_SESSION['document_search']['keyword'];
	   		
   		}
   		// echo "<pre>";print_r($_SESSION['document_search']);die();
   		header('Location: ' . site_url("web/".$this->class.'/'.$method));
   	}

   	public function setSessionSearchListAll(){
   		// echo "<pre>";print_r($this->input->post('unitkerja'));die();
   		unset($_SESSION['document_search_listall']);
   		if ($this->input->post('keyword', true)!=NULL and $this->input->post('keyword', true)!='')//from search navbar
	   		$_SESSION['document_search_listall']['keyword'] = $this->input->post('keyword', true);

   		if ($this->input->post('title', true)!=NULL and $this->input->post('title', true)!='')//from filter keyword
	   		$_SESSION['document_search_listall']['title'] = $this->input->post('title', true);

	   	if ($this->input->post('type', true)!=NULL and $this->input->post('type', true)!=''){ // from filter type
	   		$_SESSION['document_search_listall']['arr_type'] = $this->input->post('type', true);
	   		$_SESSION['document_search_listall']['type'] = implode(',',$_SESSION['document_search_listall']['arr_type']);
	   		
	   	}

	   	if($this->input->post('status', true)!=NULL and $this->input->post('status', true)!=''){
	   		$_SESSION['document_search_listall']['arr_status'] = $this->input->post('status', true);
	   		$_SESSION['document_search_listall']['status'] = implode(',',$_SESSION['document_search_listall']['arr_status']);
	   	}

	   	if ($this->input->post('unitkerja', true)!=NULL and $this->input->post('unitkerja', true)!=''){
	   		$_SESSION['document_search_listall']['arr_unitkerja'] = $this->input->post('unitkerja', true);
	   		$workunits = array();
	   		foreach ($_SESSION['document_search_listall']['arr_unitkerja'] as $k_unitkerja => $v_unitkerja) {
	   			$workunits[]="'".$v_unitkerja."'";
	   		}
	   		echo "masuk sini";
	   		$_SESSION['document_search_listall']['unitkerja'] = implode(',',$workunits);
	   	}

	   	if($this->input->post('datestart', true)!=NULL and  $this->input->post('datestart', true)!='') // from datestart
	   		$_SESSION['document_search_listall']['datestart'] = $this->input->post('datestart', true);

	   	if($this->input->post('dateend', true)!=NULL and  $this->input->post('dateend', true)!='') // from dateend
	   		$_SESSION['document_search_listall']['dateend'] = $this->input->post('dateend', true);

   		// if (isset($_SESSION['document_search_listall']['type']) and $_SESSION['document_search']['type']!=''){
   		// 	$_SESSION['selected_menu']="type_".$_SESSION['document_search']['type'];
   		// }
	   	// echo "<pre>";print_r($_SESSION['document_search_listall']);die();
	   	// echo "<pre>";print_r($_SESSION['document_search_listall']);die();
	   		// if (isset($_SESSION['document_search']['keyword']))
	   		// 	$method = 'index?selected_menu=all&keyword='.$_SESSION['document_search']['keyword'];

   		header('Location: ' . site_url("web/".$this->class.'/list_all'));
   	}

   	public function ajaxSetDocumentVerification(){
   		$this->load->model('Document_verification_model');
   		$this->load->model('Document_activity_model');
   		$document_id = $this->input->post('document_id', true);
   		$input = $this->input->post('json', true);
   		$this->deleteVerification($document_id, Document_verification_model::TEMPLATE.','.Document_verification_model::TUGAS. ','.Document_verification_model::PENULISAN);
   		$count_not_appropriate = 0;
   		$activity = array();
   		$activity["user_id"] = SessionManagerWeb::getUserID();
		$activity["document_id"] = $document_id;
		$activity["activity"] = "Fill the Verification Form. ";
		$activity["type"] = Document_activity_model::HISTORY;
   		foreach ($input as $key => $value) {
   			$record = array();
   			$record['document_id'] = $document_id;
   			$record['text'] = $value['text'];
   			$record['is_appropriate'] = $value['is_appropriate'];
   			switch($key){
   				case 'template':
   					$record['type'] = Document_verification_model::TEMPLATE; 
   				break;
   				case 'tugas':
   					$record['type'] = Document_verification_model::TUGAS; 
   				break;
   				case 'penulisan':
   					$record['type'] = Document_verification_model::PENULISAN; 
   				break;

   			}
   			dbInsert('DOCUMENT_VERIFICATION', $record);
   			if ($value['is_appropriate']==0){
   				$type = $record['type'];
   				$text = $this->Document_verification_model->filter(" where document_id='$document_id' and type='$type' and is_delete=0 ")->getOne("dbms_lob.substr(text , 30000, 1 )");
   				$standar = $this->Document_verification_model->getType($record['type']);
   				$activity["activity"] .= "\n- ".$standar." BELUM SESUAI ";
   				if ($record['text']!=''){
   					$activity["activity"] .= " dengan catatan ".$text;
   				} 
   				$activity["activity"] .= '. ';
   				$count_not_appropriate++;
   			}
   			elseif($value['is_appropriate'] == 1){
   				$type = $record['type'];
   				$text = $this->Document_verification_model->filter(" where document_id='$document_id' and type='$type' and is_delete=0 ")->getOne("dbms_lob.substr(text , 30000, 1 )");
   				$standar = $this->Document_verification_model->getType($record['type']);
   				$activity["activity"] .= "\n- ".$standar." SESUAI ";
   				if ($record['text']!=''){
   					$activity["activity"] .= " dengan catatan ".$text;
   				} 
   				$activity["activity"] .= '. ';
   				$count_not_appropriate++;
   			}
   		}
   		if ($count_not_appropriate==0){
   			$activity["activity"] = "Filled the document verification form and this document is appropriate with the given standard";
   		}
   		$this->Document_activity_model->create($activity,TRUE,TRUE);
   		SessionManagerWeb::setFlashMsg(true, 'Success to save the verification form');
   	}

   	protected function deleteVerification($id, $type=null){
   		if ($type!=NULL){
   			$filter_type= " and type in ($type) ";
   		}
   		$sql = "delete from document_verification where document_id=$id $filter_type";
   		dbQuery($sql);
   	}

   	public function SetDocumentVerification($document_id){
   		$this->load->model('Document_verification_model');
   		$bab = $this->input->post('bab', true);
   		$old = $this->input->post('old', true);
   		$new = $this->input->post('new', true);
   		$record = array();
	    $record['document_id'] = $document_id;
	    $record['type'] = Document_verification_model::PERUBAHAN; 
		$record['is_appropriate'] = 1;
   		$total_data = count($bab);
   		$text = array();
   		for ($i = 0; $i < $total_data; $i++) {
   			if ($bab[$i]!='' and $old[$i]!='' and $new[$i]!=''){
   				$text[] = array(
	   				'bab' => $bab[$i],
	   				'old' => $old[$i],
	   				'new' => $new[$i]
	   			);
   			}
		} 
		$record['text'] = json_encode($text);
		$this->deleteVerification($document_id, Document_verification_model::PERUBAHAN);
		if (dbInsert('DOCUMENT_VERIFICATION', $record)){
			SessionManagerWeb::setFlashMsg(true, 'Success to save the verification form');
		}
   		header('Location: ' . site_url('web/document/detail').'/'.$document_id);
   	}

   	public function updateAllJabatanDocumentByUserID($password){
   		if ($password!='Selalugant1')
   			die('Anda tidak dapat mengakses fungsi ini.');
   		$this->load->model('User_model');
   		$document = $this->model->getDocument(0);
   		foreach ($document as $k_document => $v_document) {
   			$document_id = $v_document['document_id'];
   			$record = array();
   			$record['user_jabatan_id'] = $this->User_model->getBy($v_document['user_id'], 'jabatan_id');
   			$record['creator_jabatan_id'] = $this->User_model->getBy($v_document['creator_id'], 'jabatan_id');
   			$record['approver_jabatan_id'] = $this->User_model->getBy($v_document['approver_id'], 'jabatan_id');
   			if (!dbUpdate('Documents', $record, "id=$document_id")){
   				echo $document_id.'<br>';
   			}
   		}
   		die();
   	}

   	public function setDocumentEvaluation($document_id){
   		$post = $this->input->post(null, true);

   		$data['document_id'] = $document_id;
   		$text = array();
   		for ($i = 1; $i < 10; $i++) {
   			$text[$i] = $post['select_evaluation_'.$i];
		}
		$data['text'] = json_encode($text);
   		$data['evaluation_comment'] = $post['input_evaluation_comment'];
   		$drafter_evaluation = dbGetOne('select drafter_evaluation from documents where id='.$document_id);
   		if($drafter_evaluation){
   			$data['drafter_id'] = $drafter_evaluation;
   		}
   		$this->load->model('Document_evaluation_model');
   		$is_exist = $this->Document_evaluation_model->filter("where document_id=$document_id")->getOne('1');
   		if ($is_exist){
   			$this->Document_evaluation_model->update($document_id, $data);
   			$this->addHistory($document_id, "Change document evaluation form");
   		} else {
   			$this->Document_evaluation_model->insert($data);
   			$this->addHistory($document_id, "Change document evaluation form");
   		}
   		unset($_SESSION['edit_evaluation']);
   		SessionManagerWeb::setFlashMsg(true, 'Success to save the evaluation form');
   		header('Location: ' . site_url('web/document/detail').'/'.$document_id);
   	}

   	// public function updateDocumentName($from, $to){        
    //     $ciConfig = $this->config->item('utils');
    //             $explode = explode('/',$from);
    //             $filename = $explode[8]; // namafile
    //             $ext_files = explode('.',$file);
    //             $ext_file = $ext_files[1];
                    
    //             $folder_dest = 'document/files';
    //             $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
    //             $basename = basename($file);
    //             $pecah_nama = explode('/', $document_code);
    //             $revision = '_R'.$this->model->getOne($document_id, 'revision');
    //             $file_name = $pecah_nama[0].'_'.$pecah_nama[1]. '_'.$pecah_nama[2].'_'.$pecah_nama[3].'_'.$revision.'.'.$ext_file;
    //             $fil = $file_name;
                
    //             if (!in_array($fil, $arr_files)) {
    //                 $arr_files[] = $fil;
    //                 if (!$this->model->updateDocumentWithFile($document_id, $fil, $file, $user_id)) {
    //                     SessionManagerWeb::setFlashMsg(false, 'Failed to change the documents');
    //                     return false;
    //                 }
    //             }
    //             if (!rename($file, $path_dest . '/' . $file_name)) {
    //                 SessionManagerWeb::setFlashMsg(false, 'Failed to change the documents');
    //                 return false;
    //             }
    //         }
    //     unlink($path_file);
   	// }


   	// Change Work Unit
   	public function changeunit($id){

   		$this->load->model('User_model');
   		$this->load->model('Jabatan_model');
   		$this->load->model('Atasan_model');
   		$this->load->model('Unitkerja_model');

		$buttons = array();
        $buttons[] = array('label' => ' Back', 'type' => 'success', 'icon' => 'chevron-left', 'click' => 'goBack()' );
        $this->data['buttons'] = $buttons;

        // Get Document
    	$document = $this->model->show_sql(true)->getDocumentBy(SessionManagerWeb::getUserID(),$id);
    	$document['changeunit'] = $this->model->getChangeWorkunit($id);
    	$document['changeunit_verification'] = $this->model->getChangeWorkunitVerification($id);
    	$document['changeunit_moved'] = $this->model->changeUnitMoved($id);
    	$document['creator_jabatan_name'] = $this->Jabatan_model->getOne($document['creator_jabatan_id'], "name");
    	$document['approver_jabatan_name'] = $this->Jabatan_model->getOne($document['approver_jabatan_id'], "name");
    	$document_company = $this->Unitkerja_model->getOne($document['unitkerja_id'], "company");
    	$creator = array(
    		"id" => $document['creator_id'],
    		"name" => $document['creator_name'],
    		"jabatan_id" => $document['creator_jabatan_id'],
    		"jabatan_name" => $document['creator_jabatan_name'],
    		"unitkerja_id" => $this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"),
    		"unitkerja_name" => $this->Unitkerja_model->getOne($this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"), "name")
    	);


    	$btn = array();
    	$users = array();
    	$approvers = array();
    	// echo "<pre>";print_r($document);die();
    	switch ($document['changeunit']) {
    		case 'A':
	    		$document['creator_status'] = "<b style='color:green'>OK</b>";
	    		$document['creator_solution'] = NULL;

	    		$actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['approver_jabatan_id']."' ")->getOneFilter("name");
	    		if ($actor!=NULL) {
		    		$document['approver_status'] = "<b style='color:red'> This Approver is not served at this position </b>";
		    		$document['approver_solution'] = " The Approver will be chosen automatically by system. The new Approver will be <b style='color:green'>".$actor."</b>.";
		    		$btn['approver_agree'] = 1;
		    	} else {
		    		$document['approver_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
		    		$document['approver_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
		    		$btn['approver_agree'] = 0;
		    	}

		    	// Get Approver
		    	$karyawan_id = $this->User_model->filter(" where \"user_id\"=".$document['creator_id'])->getOneFilter("karyawan_id");
		    	$approver_nopeg = $this->Atasan_model->filter(" where k_nopeg='".$karyawan_id."' ")->getOne("ATASAN1_NOPEG");
		    	$approvers[0]['id'] = $this->User_model->filter(" where \"karyawan_id\"='$approver_nopeg' ")->getOneFilter("user_id");
		    	$approvers[0]['name'] = $this->User_model->filter(" where \"user_id\"=".$approvers[0]['id'])->getOneFilter("name");
			break;
    		case 'B':
	    		$actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("name");
	    		if ($actor!=NULL) {
		    		$document['creator_status'] = "<b style='color:red'> This Creator is not served at this position </b>";
		    		$document['creator_solution'] = " The Creator will be chosen automatically by system. The new Creator will be <b style='color:green'>".$actor."</b>.";
		    		$btn['creator_agree'] = 1;
		    	} else {
		    		$document['creator_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
		    		$document['creator_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
		    		$btn['creator_agree'] = 0;
		    	}

	    		$actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['approver_jabatan_id']."' ")->getOneFilter("name");
	    		if ($actor!=NULL) {
		    		$document['approver_status'] = "<b style='color:red'> This Approver is not served at this position </b>";
		    		$document['approver_solution'] = " The Approver will be chosen automatically by system. The new Approver will be <b style='color:green'>".$actor."</b>.";
		    		$btn['approver_agree'] = 1;
		    	} else {
		    		$document['approver_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
		    		$document['approver_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
		    		$btn['approver_agree'] = 0;
		    	}

				// Users bisa choose manual
		    	$users = $this->User_model->getAll(" where \"is_active\"=1 and \"company\"='$document_company' ");

		    	// Get Approver
		    	$karyawan_id = $this->User_model->filter(" where \"user_id\"=".$document['creator_id'])->getOneFilter("karyawan_id");
		    	$approver_nopeg = $this->Atasan_model->filter(" where k_nopeg='".$karyawan_id."' ")->getOne("ATASAN1_NOPEG");
		    	$approvers[0]['id'] = $this->User_model->filter(" where \"karyawan_id\"='$approver_nopeg' ")->getOneFilter("user_id");
		    	$approvers[0]['name'] = $this->User_model->filter(" where \"user_id\"=".$approvers[0]['id'])->getOneFilter("name");
    		break;
	    	case 'C':
		    	$actor = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("name");
		    	if ($actor!=NULL) {
		    		$document['creator_status'] = "<b style='color:red'> This Creator is not served at this position </b>";
		    		$document['creator_solution'] = " The Creator will be chosen automatically by system. The new Creator will be <b style='color:green'>".$actor."</b>.";
		    		$btn['creator_agree'] = 1;
		    	} else {
		    		$document['creator_status'] = "<b style='color:red'> This position is empty or already inactive </b>";
		    		$document['creator_solution'] = " Choose another position to handle this document by clicking the button \"<b style='color:blue'>Choose Manually</b>\" ";
		    		$btn['creator_agree'] = 0;
		    	}
	    		
	    		$document['approver_status'] = "<b style='color:green'>OK</b>";
				$document['approver_solution'] =NULL;

				// Users bisa choose manual
		    	$users = $this->User_model->getAll(" where \"is_active\"=1 and \"company\"='$document_company' ");
	    	break;
	    	case 'X':
	    		$document['workunit_status'] = "<b style='color:red'> This Workunit is not serve </b>";
	    		$document['creator_status'] = "<b>-</b>";
	    		$document['creator_solution'] = NULL;
	    		$document['approver_status'] = "<b>-</b>";
				$document['approver_solution'] =NULL;
	    	case 'O':
	    		$document['creator_status'] = "<b style='color:green'>OK</b>";
	    		$document['creator_solution'] = NULL;
	    		$document['approver_status'] = "<b style='color:green'>OK</b>";
				$document['approver_solution'] =NULL;
	    	break;
    	}

    	$reason = $this->model->getChangeunitRejection($id);
    	$document['creator_reason'] = $reason['creator_reason'];
    	$document['approver_reason'] = $reason['approver_reason'];
    	// $document['workunit_reason'] = $reason['workunit_reason'];

    	if ($document['changeunit_verification']['creator']==1 or $document['changeunit_verification']['approver']==1){
    		$new = $this->model->getChangeWorkunitUserId($id);
    		$new_users = array();
    		if ($new['creator_id']!=NULL) {
    			$new_users['creator'] = $this->User_model->filter(" where \"user_id\"='".$new['creator_id']."' ")->getOneFilter("name");
    		}
    		if ($new['approver_id']!=NULL) {
    			$new_users['approver'] = $this->User_model->filter(" where \"user_id\"='".$new['approver_id']."' ")->getOneFilter("name");
    		}
    		$this->data['new'] = $new;
    		$this->data['new_users'] = $new_users;
    	}

   		// if (!SessionManagerWeb::isAdministrator() and !SessionManagerWeb::isDocumentController()){
    	if (!SessionManagerWeb::isDocumentController()){
   			if (!isset($new)){
   				$new = $this->model->getChangeWorkunitUserId($id);
   				if ($new['creator_id']!=SessionManagerWeb::getUserID() and $new['approver_id']!=SessionManagerWeb::getUserID()){
   					die('Permission Denied');
   				}
   			} else {
   				if ($new['creator_id']!=SessionManagerWeb::getUserID() and $new['approver_id']!=SessionManagerWeb::getUserID()){
   					die('Permission Denied');
   				}
   			}
   		}


    	$this->data['title'] = $document['type_name'].' "'.$document['title'].'"';

    	$this->data['document'] = $document;
    	$this->data['btn'] = $btn;
    	$this->data['users'] = $users;
    	$this->data['company_choose'] = dbGetRows("select \"ID\" as \"id\", \"NAME\" as \"name\" from unitkerja where \"LEVEL\"='COMP'");
    	$this->data['approvers'] = $approvers;
    	$this->data['creator'] = $creator;
		// echo '<pre>';
		// print_r($this->data);
		// die();
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function listChangeUnit($show_all=0){
    	// if (!SessionManagerWeb::isAdministrator() and !SessionManagerWeb::isDocumentController()){
    	if (!SessionManagerWeb::isDocumentController() && !SessionManagerWeb::isAdministrator()){
    		die('You cant access this page!');
    	}
    	$this->load->model("User_model");
    	$this->load->model("Jabatan_model");
    	$this->load->model("Document_types_model");

    	$this->data['title'] = "Document List Change Unit";
    	$company = $this->User_model->filter(" where \"user_id\"='".SessionManagerWeb::getUserID()."' ")->getOneFilter("company");
    	$filter = '';
    	if($_SESSION['filter_changeunit']['keyword']){
    		$filter .= "and lower(d.title) like '%".strtolower($_SESSION['filter_changeunit']['keyword'])."%'";
    	}
    	if($_SESSION['filter_changeunit']['arr_type']){
    		$filter .= " and d.type_id in (".$_SESSION['filter_changeunit']['type'].")";
    	}
    	if($_SESSION['filter_changeunit']['arr_unitkerja']){
    		$filter .= " and d.unitkerja_id in (".$_SESSION['filter_changeunit']['unitkerja'].")";
    	}
    	$data = $this->model->filter($filter)->getAllUnitChange($company);
    	foreach ($data as $k_data => $v_data) {
    		$data[$k_data]['old']['creator_name'] = $this->User_model->filter(" where \"user_id\"='".$v_data['creator_id']."' ")->getOneFilter("name");
    		$data[$k_data]['old']['approver_name'] = $this->User_model->filter(" where \"user_id\"='".$v_data['approver_id']."' ")->getOneFilter("name");

    		// Get New Creator and Approver
    		$data[$k_data]['new']['creator_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$v_data['creator_jabatan_id']."' ")->getOneFilter("user_id");
    		$data[$k_data]['new']['approver_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$v_data['approver_jabatan_id']."' ")->getOneFilter("user_id");
    		$data[$k_data]['new']['creator_name'] = $this->User_model->filter(" where \"user_id\"='".$data[$k_data]['new']['creator_id']."' ")->getOneFilter("name");
    		$data[$k_data]['new']['approver_name'] = $this->User_model->filter(" where \"user_id\"='".$data[$k_data]['new']['approver_id']."' ")->getOneFilter("name");
    		$data[$k_data]['new']['unitkerja_name'] = null;
    		
    		if ($data[$k_data]['new']['creator_id']==null or $data[$k_data]['new']['approver_id']==NULL){
    			// echo "id:".$data[$k_data]['code']."<br>";
    			// echo "creator:".$data[$k_data]['new']['creator_id']." ||| approver:".$data[$k_data]['new']['approver_id']."<br>";
    			if ($show_all==0){
    				unset($data[$k_data]);
	    		} else {
	    			$data[$k_data]['is_problematic'] = 1;
	    		}
				
			}
			elseif($data[$k_data]['new']['creator_id']!=null && $data[$k_data]['new']['approver_id']!=NULL && $v_data['unitkerja'] == 1){
				// echo "masuk unitkerja kosong<br>";
				if ($show_all==0){
    				unset($data[$k_data]);
	    		} else {
	    			$data[$k_data]['is_problematic'] = 1;
	    		}
			}

    	}

    	// jenis dokumen
    	// $all_types= $this->Document_types_model->getAllJenis();
    	// $types = Util::toMap($all_types, 'id', 'type');

    	$this->data['data'] = $data;
    	$this->data['show_all'] = $show_all;
    	$this->data['variables'] = SessionManagerWeb::getVariables();
    	// echo "<pre>";print_r($this->data);die();
    	// $this->template->viewDefault($this->view, $this->data);
    	$this->template->viewDefault('document_listchangeunit', $this->data);
    }

    public function bulkChangeunit(){
    	$this->load->model("User_model");
    	$ids = $this->input->post('approve', true);
    	foreach ($ids as $id) {
    		$record = array();
			$record['approver'] = 0;
			$record['creator'] = 0;
			if (dbUpdate("document_changeunit", $record, "id=$id")){
				$document_id = $this->model->getOneChangeunit($id, "document_id");
				$this->changeunitCreateNewDocument($document_id);
			}


    		// $document_id = $this->model->getOneChangeunit($id, "document_id");
    		// $creator_jabatan_id = $this->model->getOne($document_id, "creator_jabatan_id");
    		// $approver_jabatan_id = $this->model->getOne($document_id, "approver_jabatan_id");

    		// $data = array();
    		// $data['creator_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$creator_jabatan_id."' ")->getOneFilter("user_id");
    		// $data['approver_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$approver_jabatan_id."' ")->getOneFilter("user_id");

    		// if (dbUpdate("documents", $data, "id=$document_id")){
    		// 	$record = array();
    		// 	$record['approver'] = 0;
    		// 	$record['creator'] = 0;
    		// 	dbUpdate("document_changeunit", $record, "id=$id");
    		// }
    	}
    	SessionManagerWeb::setFlashMsg(true, 'Success to move to new workunit document.');
    	redirect("web/document/listchangeunit");

    }

    public function changeunitCreateNewDocument($id){
    	$this->load->model('User_model');
   		$this->load->model('Jabatan_model');
   		$this->load->model('Atasan_model');
   		$this->load->model('Unitkerja_model');
   		$this->load->model('Workflow_model');

        // Get Document
    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(),$id);
    	$creator = array(
    		"id" => $document['creator_id'],
    		"name" => $document['creator_name'],
    		"jabatan_id" => $document['creator_jabatan_id'],
    		"jabatan_name" => $document['creator_jabatan_name'],
    		"unitkerja_id" => $this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"),
    		"unitkerja_name" => $this->Unitkerja_model->getOne($this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"), "name")
    	);

    	$record = array();
    	$record['is_moved'] = 1;
    	dbUpdate("document_changeunit", $record, "document_id=$id");
		
    	$record = array();
    	$record['is_obsolete'] = 1;
    	$record['show'] = 0;
    	// Update Creator, Approver, Unitkerja
    	$record['unitkerja_id'] = $creator['unitkerja_id'];
    	$record['creator_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("user_id");
    	$type_id = $this->model->getOne($id, 'type_id');
		$approver = $this->Unitkerja_model->getApprover($record['creator_id'], $type_id);
		$record['approver_id'] = $approver['approver'][0]['id'];
		$record['approver_jabatan_id'] = $this->Jabatan_model->getOne($record['creator_jabatan_id'], "unitkerja_id");
		if ($record['approver_id']==NULL){
			unset($record['approver_id']);
			unset($record['approver_jabatan_id']);
		} 
    	dbUpdate('documents', $record, "id=$id");

		// Add new Document
		$data = array();
		$document = $this->model->getBy($id);
		$doc = array_change_key_case($document, CASE_LOWER);
        $data['user_id'] = $doc['user_id'];
        $data['description'] = $doc['description'];
        $data['title'] = $doc['title'];
		$data['is_upload'] = $doc['is_upload']; 
		$data['retension'] = $doc['retension'];
		$data['hash'] = $doc['hash'];
		$data['revision'] = 0;
		$data['status'] = Document_model::STATUS_SELESAI;
		$data['workflow_id'] = Workflow_model::PENGAJUAN;
		$data['workflow_urutan'] = 6;
		$data['show'] = 1;
		$data['is_published'] = 1;

		// Setting
		$data['type_id'] = $doc['type_id'];
		$data['creator_id'] = $doc['creator_id'];
		$data['approver_id'] = $doc['approver_id'];					
		$data['prosesbisnis_id'] = $doc['prosesbisnis_id'];
		$data['user_jabatan_id'] = $doc['user_jabatan_id'];	
		$data['creator_jabatan_id'] = $doc['creator_jabatan_id'];	
		$data['approver_jabatan_id'] = $doc['approver_jabatan_id'];
		$data['unitkerja_id'] = $creator['unitkerja_id'];

		$data['is_converted'] = $this->model->getOne($id, "is_converted");
		$data['have_header'] = $this->model->getOne($id, "have_header");


		// check code
		$codes=explode('/',$doc['code']);
		if ($data['type_id']==Document_model::INSTRUKSI_KERJA){
			$param = " and unitkerja_id='".$data['unitkerja_id']."' ";
		} else {
			if ($type_id==Document_model::FORM){
				if ($codes[0]=='FI'){
					$param = " and unitkerja_id='".$data['unitkerja_id']."' ";
				} else {
    				$param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
				}
			} else {
				$param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
			}
		}
		$code = $codes[0].'/'.$codes[1].'/'.$codes[2];
		if (strtoupper($codes[0])=='IK' or strtoupper($codes[0])=='FI'){
	    	$code  .= '/'.$data['unitkerja_id'];
    	}
    	$data['code_number'] = $this->model->getLastCode($code, $param);
    	$data['code'] = $code.'/'.$data['code_number'];
    	
		// Setting new Document
		$file_explode = explode('.', $doc['document']); 
		$code_explode = explode('/',$data['code']);
		$code_implode = implode('_',$code_explode);
		$data['document'] = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
		
		$new_id = $this->addNewDocument($data);
		if ($new_id){							

			// update tanggal
			$sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$new_id  ";
			dbQuery($sql);

			$this->addHistory($id, "This document is moved to document no. ".$data['code']);
			$this->addHistory($new_id, "This document is moved from document no. ".$doc['code']);

			$document_id = $id;

			//Copy lama ke baru 
			$CI = & get_instance();
	        $config = $CI->config->item('utils');
	        $folder = "document/files";
	        $full_path = $config['full_upload_dir'] . $folder . '/';


	        $from = $full_path.$doc['document'];
	        $to = $full_path.$data['document'];

	        copy($from, $to);

	        if ($data['is_converted']==1) {
	        	$old_docs = explode('.',$doc['document']);
		        unset($old_docs[count($old_docs)-1]);
		        $old_doc = implode('.', $old_docs);
		        $new_docs = explode('.',$data['document']);
		        unset($new_docs[count($new_docs)-1]);
		        $new_doc = implode('.', $new_docs);

		        $from = $full_path.$old_doc.'.pdf';
	            $to = $full_path.$new_doc.'.pdf';
	            copy($from, $to);
	        }

	        if ($data['is_upload']==0){
		        $old_docs = explode('.',$doc['document']);
		        unset($old_docs[count($old_docs)-1]);
		        $old_doc = implode('.', $old_docs);
		        $new_docs = explode('.',$data['document']);
		        unset($new_docs[count($new_docs)-1]);
		        $new_doc = implode('.', $new_docs);

		        $from = $full_path.$old_doc.'.txt';
	            $to = $full_path.$new_doc.'.txt';
	            copy($from, $to);
			}

			// reviewer
			if ($type['need_reviewer']=='1'){
				$this->load->model('Document_reviewer_model');
				$data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, $workflow_id);
				$data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
				$this->Document_reviewer_model->create($new_id,Workflow_model::REVISI, $data_reviewer);
			}
			
			// ISO
			$data_iso = array();
			$data_iso['document_id'] = $new_id;
			$iso_id = $this->model->getDocumentISOS($document_id);
			foreach ($iso_id as $key => $value) {
				$data_iso['iso_id'] = $value;
				dbInsert("DOCUMENT_ISOS", $data_iso);
			}

			// Klausul
			if ($type['klausul']=='1'){
				$this->load->model('Document_klausul_model');
				$data_klausul['document_id'] = $new_id;
				$klausul = $this->Document_klausul_model->filter(" document_id=$document_id ")->getAll();
				foreach ($klausul as $key => $value) {
					$data_klausul['klausul_id'] = $value['klausul_id'];
					dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
				}
			}
		
			// Unit Kerja
			$this->load->model('Document_unitkerja_model');
			$unitkerja_related = $this->Document_unitkerja_model->filter(" where document_id=$document_id ")->getAll();
			if ($unitkerja_related!=NULL){
				$data_unit_kerja = array();
				foreach ($unitkerja_related as $k_unitkerja_related => $v_unitkerja_related) {
					$data_unit_kerja = $v_unitkerja_related;
					$data_unit_kerja['document_id'] = $new_id;
					dbInsert('DOCUMENT_UNITKERJA', $data_unit_kerja);
				}	
			}
			
			// Related
			// $relatedDocument = $this->input->post('related',true);
			// $this->model->setRelatedDocument($insert,$relatedDocument);

			$data = array();
	        $data['user_id'] = $doc['creator_id'];
	        $data['user_jabatan_id'] = $doc['creator_jabatan_id'];
	        dbUpdate('documents', $data, "id=$new_id");
	    }
    }


    public function moveChangeUnit($id, $is_redirect=true, $newCreatorApp=null){
    	if($newCreatorApp!=null){
    		// echo $id;die();
	    	$this->load->model('User_model');
	   		$this->load->model('Jabatan_model');
	   		$this->load->model('Atasan_model');
	   		$this->load->model('Unitkerja_model');
	   		$this->load->model('Workflow_model');

	   		$data = array();
			$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(), $id);
			$doc = array_change_key_case($document, CASE_LOWER);
			$record = array();
	    	$record['is_moved'] = 1;
	    	dbUpdate("document_changeunit", $record, "document_id=$id");

			$data['user_id'] = $newCreatorApp['creator_id'];
	        $data['title'] = $doc['title'];
			$data['is_upload'] = $doc['is_upload']; 
			$data['retension'] = $doc['retension'];
			$data['hash'] = $doc['hash'];
			$data['revision'] = 0;
			$data['status'] = Document_model::STATUS_SELESAI;
			$data['workflow_id'] = Workflow_model::PENGAJUAN;
			$data['workflow_urutan'] = 6;
			$data['show'] = 1;
			$data['is_published'] = 1;

			// Setting
			$data['type_id'] = $doc['type_id'];
			$data['creator_id'] = $newCreatorApp['creator_id'];
			$data['approver_id'] = $newCreatorApp['approver_id'];					
			$data['prosesbisnis_id'] = $doc['prosesbisnis_id'];
			$data['user_jabatan_id'] = $newCreatorApp['creator_jabatan_id'];	
			$data['creator_jabatan_id'] = $newCreatorApp['creator_jabatan_id'];	
			$data['approver_jabatan_id'] = $newCreatorApp['approver_jabatan_id'];
			$data['unitkerja_id'] = $newCreatorApp['unitkerja_id'];

			$data['is_converted'] = $this->model->getOne($id, "is_converted");
			$data['have_header'] = 0;

			$unitkerja_baru = dbGetOne("select name from unitkerja where id='".$data['unitkerja_id']."'");


			// check code
			$codes=explode('/',$doc['code']);
			if($doc['company'] == null){
				$code = $this->model->getCodeInForm($data['type_id'],$data['prosesbisnis_id'],$data['unitkerja_id'],null, $newCreatorApp['company']);
				$data['code_number'] = $this->model->getLastCode($code, $param);
				$data['code'] = $code.''.$data['code_number'];
			}
			else{
				if ($data['type_id']==Document_model::INSTRUKSI_KERJA){
					$param = " and unitkerja_id='".$data['unitkerja_id']."' ";
				} 
				else {
					if ($type_id==Document_model::FORM){
						if ($codes[0]=='FI'){
							$param = " and unitkerja_id='".$data['unitkerja_id']."' ";
						} else {
		    				$param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
						}
					} else {
						$param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
					}
				}

				$code = $codes[0].'/'.$codes[1].'/'.$codes[2];
				if (strtoupper($codes[0])=='IK' or strtoupper($codes[0])=='FI'){
			    	$code  .= '/'.$data['unitkerja_id'];
		    	}
		    	$data['code_number'] = $this->model->getLastCode($code, $param);
		    	$data['code'] = $code.'/'.$data['code_number'];
			}

	    	//CHANGE LOG
	    	$data['description'] = "This document is moved from document no. ".$doc['code']." Rev ".$doc['revision'];
	    	// Setting new Document
			$file_explode = explode('.', $doc['filename']); 
			$code_explode = explode('/',$data['code']);
			$code_implode = implode('_',$code_explode);
			// echo "<pre>";print_r($code_explode);die();
			$data['document'] = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
			$new_id = $this->addNewDocument($data);
			if ($new_id){	// jika berhasil insert, dokumen lama di obsoletkan
				$record = array();
		    	$record['is_obsolete'] = 1;
		    	$record['show'] = 0;
		    	dbUpdate('documents', $record, "id=$id"); // update obsolete dokumen				
			// echo "berhasil buat dokumen baru";
				// update tanggal
				$sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$new_id  ";
				dbQuery($sql);


				$this->addHistory($id, "This document is moved to document no. ".$data['code']);
				$this->addHistory($new_id, "This document is moved from document no. ".$doc['code']." Rev ".$doc['revision']);

				$document_id = $id;

				//Copy lama ke baru 
				$CI = & get_instance();
		        $config = $CI->config->item('utils');
		        $folder = "document/files";
		        $full_path = $config['full_upload_dir'] . $folder . '/';
		        $explodefilename = explode('.', $doc['filename']);
		        $filename = $explodefilename[0].'.pdf';
		        $idCompany = $this->Unitkerja_model->getOneBy($doc['unitkerja_id'], "\"COMPANY\"");


		        $from = $full_path.$doc['filename'];
	            $to = $full_path.$data['document'];

	            copy($from, $to);

	            if ($data['is_converted']==1) {
	            	$old_docs = explode('.',$doc['filename']);
			        unset($old_docs[count($old_docs)-1]);
			        $old_doc = implode('.', $old_docs);
			        $new_docs = explode('.',$data['document']);
			        unset($new_docs[count($new_docs)-1]);
			        $new_doc = implode('.', $new_docs);

			        $from = $full_path.$old_doc.'.pdf';
	                $to = $full_path.$new_doc.'.pdf';
	                copy($from, $to);
	            }

	            if ($data['have_header']==1) {

	            	rename($full_path.$explodefilename[0].'_header.pdf', './assets/uploads/document/files/'.$explodefilename[0].'_header_old.pdf');
			        $this->ObsoleteWatermark($filename, $idCompany, 'pdf');
			        unlink($full_path.'./assets/uploads/document/files/'.$explodefilename[0].'_header_old.pdf');


	          //   	$old_docs = explode('.',$doc['filename']);
			        // unset($old_docs[count($old_docs)-1]);
			        // $old_doc = implode('.', $old_docs);
			        // $new_docs = explode('.',$data['document']);
			        // unset($new_docs[count($new_docs)-1]);
			        // $new_doc = implode('.', $new_docs);

			        // $from = $full_path.$old_doc.'_header.pdf';
	          //       $to = $full_path.$new_doc.'_header.pdf';
	          //       copy($from, $to);
	            }

	            if ($data['is_upload']==0){
			        $old_docs = explode('.',$doc['filename']);
			        unset($old_docs[count($old_docs)-1]);
			        $old_doc = implode('.', $old_docs);
			        $new_docs = explode('.',$data['document']);
			        unset($new_docs[count($new_docs)-1]);
			        $new_doc = implode('.', $new_docs);

			        $from = $full_path.$old_doc.'.txt';
	                $to = $full_path.$new_doc.'.txt';
	                copy($from, $to);
				}

				// reviewer
				if ($type['need_reviewer']=='1'){
					$this->load->model('Document_reviewer_model');
					$data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, $workflow_id);
					$data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
					$this->Document_reviewer_model->create($new_id,Workflow_model::REVISI, $data_reviewer);
				}
				
				// ISO
				$data_iso = array();
				$data_iso['document_id'] = $new_id;
				$iso_id = $this->model->getDocumentISOS($document_id);
				foreach ($iso_id as $key => $value) {
					$data_iso['iso_id'] = $value;
					dbInsert("DOCUMENT_ISOS", $data_iso);
				}

				// Klausul
				if ($type['klausul']=='1'){
					$this->load->model('Document_klausul_model');
					$data_klausul['document_id'] = $new_id;
					$klausul = $this->Document_klausul_model->filter(" document_id=$document_id ")->getAll();
					foreach ($klausul as $key => $value) {
						$data_klausul['klausul_id'] = $value['klausul_id'];
						dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
					}
				}
			
				// Unit Kerja
				$this->load->model('Document_unitkerja_model');
				$unitkerja_related = $this->Document_unitkerja_model->filter(" where document_id=$document_id ")->getAll();
				if ($unitkerja_related!=NULL){
					$data_unit_kerja = array();
					foreach ($unitkerja_related as $k_unitkerja_related => $v_unitkerja_related) {
						$data_unit_kerja = $v_unitkerja_related;
						$data_unit_kerja['document_id'] = $new_id;
						dbInsert('DOCUMENT_UNITKERJA', $data_unit_kerja);
					}	
				}
				
				// Related dari dokumen lama ke dokumen lain
				$relatedDocument = dbGetRows("SELECT DOCUMENT2 as document2 FROM DOCUMENT_RELATED WHERE DOCUMENT1=".$document_id);
				$related = array();
				for ($i=0; $i < count($relatedDocument) ; $i++) { 
					$related[$i]['document2'] = $relatedDocument[$i]['DOCUMENT2'];
				}
				$this->model->setRelatedDocument2($new_id,$related);

				//related dari dokumen lain ke dokumen ini
				dbUpdate("document_related", array('document2' => $new_id), "document2=".$document_id);
				$relatedDocument2 = dbGetRows("SELECT r.DOCUMENT1 as \"document_id\", d.document as \"document\" FROM DOCUMENT_RELATED r LEFT JOIN DOCUMENTS d ON r.DOCUMENT1 = d.id WHERE r.DOCUMENT2=".$document_id);
				foreach ($relatedDocument2 as $related) {
					$doc_name = explode('.', $related['document']);
					unlink($full_path.'./assets/uploads/document/files/'.$doc_name[0].'_header.pdf');
					dbUpdate("documents", array('have_header' => 0), "id=".$related['document_id']);
				}
				// $relatedDocument = $this->input->post('related',true);
				

				// $data = array();
	   //          $data['user_id'] = $data['creator_id'];
	   //          $data['user_jabatan_id'] =$data['creator_jabatan_id'];

	   //          dbUpdate('documents', $data, "id=$document_id");
	            $unitkerja_lama = $this->Unitkerja_model->show_sql(false)->getBy($doc['unitkerja_id']);
	            // $unitkerja_baru = dbGetOne("select name from unitkerja where id='".$data['unitkerja_id']."'");
	            // $unitkerja_baru = $this->Unitkerja_model->column(array('name' => 'name'))->getBy($data['unitkerja_id']);
	    			// print_r($unitkerja_lama);
	            $this->addHistory($new_id, "This document moved from workunit ".$unitkerja_lama['name']." to ".$unitkerja_baru);
	            $this->addHistory($id, "This document moved from workunit ".$unitkerja_lama['name']." to ".$unitkerja_baru);
	            $text = "This document is moved from ".$doc['creator_name']." as creator";
				$this->addHistory($new_id, $text);
	    //         if ($is_redirect){
	    //         	SessionManagerWeb::setFlashMsg(true, 'Success to move to new workunit document.');
					// redirect("web/document/changeunit/".$id);
	    //         }
				
			}
			return $new_id;
    	}
    	else{
    		redirect('web/document/index');
    	}
    	
    }

    public function relatedcoba(){
    	$this->load->model('Document_download_model');
    	$download_detail = $this->Document_download_model->getBy(647);
        echo 'select "company" from documents_view where "document_id"='.$download_detail['document_id'];die();
  //   	$CI = & get_instance();
  //       $config = $CI->config->item('utils');
  //       $folder = "document/files";
  //       $full_path = $config['full_upload_dir'] . $folder . '/';
  //   	$relatedDocument2 = dbGetRows("SELECT r.DOCUMENT1 as \"document_id\", d.document as \"document\" FROM DOCUMENT_RELATED r LEFT JOIN DOCUMENTS d ON r.DOCUMENT1 = d.id WHERE r.DOCUMENT2=5608");
		// foreach ($relatedDocument2 as $related) {
		// 	$doc_name = explode('.', $related['document']);
		// 	echo $full_path.'./assets/uploads/document/files/'.$doc_name[0].'_header.pdf';
		// 	echo "<br>";
		// 	// unlink($full_path.'./assets/uploads/document/files/'.$doc_name[0].'_header.pdf');
		// 	// dbUpdate("documents", array('have_header' => 0), "id=".$related['document_id']);
		// }
  //   	$relatedDocument = $this->model->getRelatedOneWay(5600);
  //   	// echo "<pre>";print_r($relatedDocument);die();
		// if($relatedDocument!=NULL){
		// 	$data_related_document = array();
		// 	foreach ($relatedDocument as $related => $column) {
		// 		$data_related_document['document1'] = 5601;
		// 		$data_related_document['document2'] = $column['DOCUMENT2'];
		// 		// $data_related_document['created_at'] = date("Y/m/d h:i:s");
		// 		// $data_related_document['updated_at'] = date("Y/m/d h:i:s");
		// 		$data_related_document['is_form'] = $column['IS_FORM'];
		// 		echo "<pre>";print_r($data_related_document);echo "</pre>";
		// 		dbInsert('DOCUMENT_RELATED', $data_related_document);
		// 	}
		// }
    	// $relatedDocument = array(0=> 5536, 1=>5533);
    	// $id = 5535;
    	// $this->model->updateRelatedDocumentNew($id,$relatedDocument);
    	// // dbUpdate('document_related', array('document2' => 5529), "document_id=5527");
  //   	// Related
    	// $a = $this->model->getRelatedDocument(5535);
    	// echo "<pre>";print_r($a);die();
		// $relatedDocument = dbGetRows("SELECT DOCUMENT2 as document2 FROM DOCUMENT_RELATED WHERE DOCUMENT1=5508");
		// $related = array();
		// for ($i=0; $i < count($relatedDocument) ; $i++) { 
		// 	$related[0]['document2'] = $relatedDocument[$i]['DOCUMENT2'];
		// }
		// // echo "<pre>";print_r($related);die();
		// $this->model->setRelatedDocument2(5509,$related);
    }
    public function moveChangeUnit_($id, $is_redirect=true, $newCreatorApp=null){
    	$this->load->model('User_model');
   		$this->load->model('Jabatan_model');
   		$this->load->model('Atasan_model');
   		$this->load->model('Unitkerja_model');
   		$this->load->model('Workflow_model');

        // Get Document
    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(),$id);
    	$creator = array(
    		"id" => $newCreatorApp['creator_id'],
    		// "name" => $document['creator_name'],
    		"jabatan_id" => $newCreatorApp['creator_jabatan_id'],
    		// "jabatan_name" => $document['creator_jabatan_name'],
    		"unitkerja_id" => $this->Jabatan_model->getOne($newCreatorApp['creator_jabatan_id'], "unitkerja_id"),
    		// "unitkerja_name" => $this->Unitkerja_model->getOne($this->Jabatan_model->getOne($document['creator_jabatan_id'], "unitkerja_id"), "name")
    	);

    	$unitkerja_lama = $this->Unitkerja_model->show_sql(false)->getBy($document['unitkerja_id']);

    	$record = array();
    	$record['is_moved'] = 1;
    	dbUpdate("document_changeunit", $record, "document_id=$id");
		$codes_doc=explode('/',$document['code']);
		// echo "tipe_data:".$document['type_id'];die();
		if($document['type_id']==Document_model::INSTRUKSI_KERJA || $codes_doc[0] == 'FI'){ // Jika instruksi kerja atau form instruksi kerja, yang alam di obsolete, dan create doc baru.
			

	    	//Add new Document
			$data = array();
			$document = $this->model->getBy($id);
			$doc = array_change_key_case($document, CASE_LOWER);


	        $data['user_id'] = null;
	        $data['description'] = $doc['description'];
	        $data['title'] = $doc['title'];
			$data['is_upload'] = $doc['is_upload']; 
			$data['retension'] = $doc['retension'];
			$data['hash'] = $doc['hash'];
			$data['revision'] = 0;
			$data['status'] = Document_model::STATUS_SELESAI;
			$data['workflow_id'] = Workflow_model::PENGAJUAN;
			$data['workflow_urutan'] = 6;
			$data['show'] = 1;
			$data['is_published'] = 1;

			// Setting
			$data['type_id'] = $doc['type_id'];
			$data['creator_id'] = $newCreatorApp['creator_id'];
			$data['approver_id'] = $newCreatorApp['approver_id'];					
			$data['prosesbisnis_id'] = $doc['prosesbisnis_id'];
			$data['user_jabatan_id'] = $doc['user_jabatan_id'];	
			$data['creator_jabatan_id'] = $newCreatorApp['creator_jabatan_id'];	
			$data['approver_jabatan_id'] = $newCreatorApp['approver_jabatan_id'];
			$data['unitkerja_id'] = $creator['unitkerja_id'];

			$data['is_converted'] = $this->model->getOne($id, "is_converted");
			$data['have_header'] = $this->model->getOne($id, "have_header");

			// check code
			$codes=explode('/',$doc['code']);
			if ($data['type_id']==Document_model::INSTRUKSI_KERJA){
				$param = " and unitkerja_id='".$data['unitkerja_id']."' ";
			} else {
				if ($type_id==Document_model::FORM){
					if ($codes[0]=='FI'){
						$param = " and unitkerja_id='".$data['unitkerja_id']."' ";
					} else {
	    				$param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
					}
				} else {
					$param = " and prosesbisnis_id='".$data['prosesbisnis_id']."' ";
				}
			}
			$code = $codes[0].'/'.$codes[1].'/'.$codes[2];
			if (strtoupper($codes[0])=='IK' or strtoupper($codes[0])=='FI'){
		    	$code  .= '/'.$data['unitkerja_id'];
	    	}
	    	$data['code_number'] = $this->model->getLastCode($code, $param);
	    	$data['code'] = $code.'/'.$data['code_number'];

	    	// Setting new Document
			$file_explode = explode('.', $doc['document']); 
			$code_explode = explode('/',$data['code']);
			$code_implode = implode('_',$code_explode);
			$data['document'] = $code_implode.'_R'.$data['revision'].'.'.end($file_explode);
			// echo "<pre>";print_r($data);echo "</pre>";
			if($doc['unitkerja_id'] != $creator['unitkerja_id']){
				$new_id = $this->addNewDocument($data);
			}
			else{
				dbUpdate("documents", $newCreatorApp, "id=$id");
				echo "update dsaflndsff";
				die();
			}
			
			if ($new_id){	// jika berhasil insert
			// dokumen lama di obsoletkan

				$record = array();
		    	$record['is_obsolete'] = 1;
		    	$record['show'] = 0;
		    	dbUpdate('documents', $record, "id=$id"); // update obsolete dokumen				
			// echo "berhasil buat dokumen baru";
				// update tanggal
				$sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$new_id  ";
				dbQuery($sql);

				$this->addHistory($id, "This document is moved to document no. ".$data['code']);
				$this->addHistory($new_id, "This document is moved from document no. ".$doc['code']);

				$document_id = $id;

				//Copy lama ke baru 
				$CI = & get_instance();
		        $config = $CI->config->item('utils');
		        $folder = "document/files";
		        $full_path = $config['full_upload_dir'] . $folder . '/';


		        $from = $full_path.$doc['document'];
	            $to = $full_path.$data['document'];

	            copy($from, $to);

	            if ($data['is_converted']==1) {
	            	$old_docs = explode('.',$doc['document']);
			        unset($old_docs[count($old_docs)-1]);
			        $old_doc = implode('.', $old_docs);
			        $new_docs = explode('.',$data['document']);
			        unset($new_docs[count($new_docs)-1]);
			        $new_doc = implode('.', $new_docs);

			        $from = $full_path.$old_doc.'.pdf';
	                $to = $full_path.$new_doc.'.pdf';
	                copy($from, $to);
	            }

	            if ($data['have_header']==1) {
	            	$old_docs = explode('.',$doc['document']);
			        unset($old_docs[count($old_docs)-1]);
			        $old_doc = implode('.', $old_docs);
			        $new_docs = explode('.',$data['document']);
			        unset($new_docs[count($new_docs)-1]);
			        $new_doc = implode('.', $new_docs);

			        $from = $full_path.$old_doc.'_header.pdf';
	                $to = $full_path.$new_doc.'_header.pdf';
	                copy($from, $to);
	            }

	            if ($data['is_upload']==0){
			        $old_docs = explode('.',$doc['document']);
			        unset($old_docs[count($old_docs)-1]);
			        $old_doc = implode('.', $old_docs);
			        $new_docs = explode('.',$data['document']);
			        unset($new_docs[count($new_docs)-1]);
			        $new_doc = implode('.', $new_docs);

			        $from = $full_path.$old_doc.'.txt';
	                $to = $full_path.$new_doc.'.txt';
	                copy($from, $to);
				}

				// reviewer
				if ($type['need_reviewer']=='1'){
					$this->load->model('Document_reviewer_model');
					$data_reviewer = $this->Document_reviewer_model->getAllBy($document_id, $workflow_id);
					$data_reviewer = array_change_key_case($data_reviewer, CASE_UPPER);
					$this->Document_reviewer_model->create($new_id,Workflow_model::REVISI, $data_reviewer);
				}
				
				// ISO
				$data_iso = array();
				$data_iso['document_id'] = $new_id;
				$iso_id = $this->model->getDocumentISOS($document_id);
				foreach ($iso_id as $key => $value) {
					$data_iso['iso_id'] = $value;
					dbInsert("DOCUMENT_ISOS", $data_iso);
				}

				// Klausul
				if ($type['klausul']=='1'){
					$this->load->model('Document_klausul_model');
					$data_klausul['document_id'] = $new_id;
					$klausul = $this->Document_klausul_model->filter(" document_id=$document_id ")->getAll();
					foreach ($klausul as $key => $value) {
						$data_klausul['klausul_id'] = $value['klausul_id'];
						dbInsert("DOCUMENT_KLAUSUL", $data_klausul);
					}
				}
			
				// Unit Kerja
				$this->load->model('Document_unitkerja_model');
				$unitkerja_related = $this->Document_unitkerja_model->filter(" where document_id=$document_id ")->getAll();
				if ($unitkerja_related!=NULL){
					$data_unit_kerja = array();
					foreach ($unitkerja_related as $k_unitkerja_related => $v_unitkerja_related) {
						$data_unit_kerja = $v_unitkerja_related;
						$data_unit_kerja['document_id'] = $new_id;
						dbInsert('DOCUMENT_UNITKERJA', $data_unit_kerja);
					}	
				}
				
				// Related
				$relatedDocument = $this->input->post('related',true);
				$this->model->setRelatedDocument($insert,$relatedDocument);

				$data = array();
	            $data['user_id'] = $doc['creator_id'];
	            $data['user_jabatan_id'] = $doc['creator_jabatan_id'];
	            dbUpdate('documents', $data, "id=$new_id");
	            $unitkerja_baru = $this->Unitkerja_model->getBy($document['unitkerja_id']);
	    			// print_r($unitkerja_lama);
	            $this->addHistory($id, "This document move from workunit ".$unitkerja_lama['name']." to ".$unitkerja_baru['name']);
	    //         if ($is_redirect){
	    //         	SessionManagerWeb::setFlashMsg(true, 'Success to move to new workunit document.');
					// redirect("web/document/changeunit/".$id);
	    //         }
				
			}
		}
		else{ // selain instr. kerja dan form instr. kerja tidak perlu apa2.langsung redirect aja ya
			// SessionManagerWeb::setFlashMsg(true, 'Success to move to new workunit document.');
			// redirect("web/document/changeunit/".$id);
		}
		// SessionManagerWeb::setFlashMsg(true, 'Success to move to new workunit document.');
		// redirect("web/document/changeunit/".$id);
    }


    // Agree With System Solution
    // Selected Creator Cant deny it!
    public function agreeWithSolution($id, $actor){
    	// echo ",masuk fungsi agree";die();
    	$this->load->model('User_model');
    	$this->load->model("Unitkerja_model");
    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(),$id);
    	// echo "<pre>";print_r($document);die();
    	$record = array();
    	$changeunit = array();
    	if (strtolower($actor)=='creator'){
    		$changeunit['creator_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$document['creator_jabatan_id']."' ")->getOneFilter("user_id");
    		// $changeunit['creator_verification'] = 1;
    		$changeunit['creator'] = 0;

    		$type_id = $this->model->getOne($id, 'type_id');
    		$approver = $this->Unitkerja_model->getApprover($changeunit['creator_id'], $type_id);
    		$changeunit['approver'] = 0;
    		$changeunit['approver_id'] = $approver['approver'][0]['id'];
    		if ($changeunit['approver_id']==NULL){
    			unset($changeunit['approver_id']);
    			unset($changeunit['approver']);
    		}
    	} else if (strtolower($actor)=='approver') {
    		$changeunit['approver_id'] = $this->User_model->filter(" where \"jabatan_id\"='".$document['approver_jabatan_id']."' ")->getOneFilter("user_id");
    		// $changeunit['approver_verification'] = 1;
    		$changeunit['approver'] = 0;
    		$is_move_creator = dbGetOne($sql);
			if (!$is_move_creator){
				$changeunit['is_moved']=1;
			}
    	}
    	// echo "<pre>";print_r($changeunit);die();
    	if (dbUpdate("document_changeunit", $changeunit, "document_id=$id")){
    		$this->load->model("Notification_model");
    		if (strtolower($actor)=='creator'){

    			$this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_CREATOR, $id, SessionManagerWeb::getUserID());

    			$record = array();
				$record['creator_id'] = $changeunit['creator_id'];
	    		$record['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['creator_id']."' ")->getOneFilter("jabatan_id");

	    		
	    		$record['approver_id'] = $changeunit['approver_id'];
	    		$record['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['approver_id']."' ")->getOneFilter("jabatan_id");
	    		$text .= "This document is moved from ".$document['creator_name']." as creator";

	    	} else if (strtolower($actor)=='approver') {
	    		$record = array();
				$record['approver_id'] = $changeunit['approver_id'];
	    		$record['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['approver_id']."' ")->getOneFilter("jabatan_id");
	    		$this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, SessionManagerWeb::getUserID());
	    		$text .= "This document is moved from ".$document['approver_name']." as approver";
	    	}
	    	if (dbUpdate("documents", $record, "id=$id")){
	    		$sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$id  ";
				dbQuery($sql);
	    		if (strtolower($actor)=='creator'){
	    			$this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, SessionManagerWeb::getUserID());
	    			// $this->moveChangeUnit($id, true);
	    		}
	    	}
	    	$this->addHistory($id, $text);
    		SessionManagerWeb::setFlashMsg(true, 'Success to Choose new User.');
    	} else {
			SessionManagerWeb::setFlashMsg(false, 'Failed to Choose new User.');
    	}

    	redirect("web/document/changeunit/$id");
    }

    public function verifyChangeunit($id,$is_approve,  $actor){
    	$this->load->model('User_model');

    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(),$id);
    	$user_ids = $this->model->getChangeWorkunitUserId($id);
    	$record = array();
    	$changeunit = array();
    	if (strtolower($actor)=='creator'){
    		$record['creator_id'] = $user_ids['creator_id'];
    		$record['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['creator_id']."' ")->getOneFilter("jabatan_id");
    		$changeunit['creator'] = 0;
    		$changeunit['creator_verification'] = 0;
    		$changeunit['creator_reason'] = null;
    	} else if (strtolower($actor)=='approver') {
    		$record['approver_id'] = $user_ids['approver_id'];
    		$record['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['approver_id']."' ")->getOneFilter("jabatan_id");
    		$changeunit['approver'] = 0;
    		$changeunit['approver_verification'] = 0;
    		$changeunit['approver_reason'] = null;
    	}
    	if (dbUpdate("documents", $record, "id=$id")){
    		SessionManagerWeb::setFlashMsg(true, 'Success to Change Unit Document');
    		dbUpdate("document_changeunit", $changeunit, "document_id=$id");
    		$this->load->model("Notification_model");
			$this->Notification_model->generate(Notification_model::ACTION_APPROVE_CHANGEUNIT, $id, SessionManagerWeb::getUserID());
    	} else {
    		SessionManagerWeb::setFlashMsg(false, 'Failed to Change Unit Document');
    	}
    	redirect("web/document/changeunit/$id");
    }

    public function ajaxGetPosition(){
    	$user = $this->input->post('user', true);
    	$this->load->model("User_model");
    	$this->load->model("Jabatan_model");
    	$position_id = $this->User_model->filter(" where \"user_id\"='$user' ")->getByFilter("jabatan_id");
    	$position_name = $this->Jabatan_model->getOne($position_id, 'name');
    	$position['id'] = $position_id;
    	$position['name'] = $position_name;
    	echo json_encode($position);
    }

    public function ajaxUpdateCreator($id){
    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(), $id);
		$doc = array_change_key_case($document, CASE_LOWER);
		// echo "<pre>";print_r($doc);die();
    	$this->load->model('Jabatan_model');
		$changeunit = array();
		// $changeunit['creator_verification'] = 1;
		$changeunit['creator'] = 0;
		$changeunit['creator_id'] = $this->input->post('creator_id', true);
		$changeunit['approver'] = 0;
		$changeunit['approver_id'] = $this->input->post('approver_id', true);
		// echo "<pre>";print_r($changeunit);die();
		if (dbUpdate("document_changeunit", $changeunit, "document_id=$id")){
			$this->load->model('Notification_model');
			$this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_CREATOR, $id, SessionManagerWeb::getUserID());
			$this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, SessionManagerWeb::getUserID());
			$record = array();
			// mengganti creator dan approver beserta jabatan masing2 ke dokumen yang sekarang(lama sebelum di gantikan)
			$record['creator_id'] = $changeunit['creator_id'];
    		$record['creator_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['creator_id']."' ")->getOneFilter("jabatan_id");
    		$record['approver_id'] = $changeunit['approver_id']; // meng
    		$record['approver_jabatan_id'] = $this->input->post('approver_jabatan_id', true);
    		$record['unitkerja_id'] = $this->Jabatan_model->getOne($record['creator_jabatan_id'], "unitkerja_id");
    		$codes=explode('/',$doc['code']);
    		// echo "<pre>";print_r($record);die();
    		// die();
    		if($doc['unitkerja_id'] != $record['unitkerja_id']){
    			if($doc['type_id'] == Document_model::INSTRUKSI_KERJA || $codes[0] == 'FI'){
	    			$this->moveChangeUnit($id, false, $record); // harus kirim creator,approver beserta jabatannya untuk diinsertkan di data doc yang baru
	    		}
	    		else{
	    			$this->load->model('Unitkerja_model');
	    			dbUpdate("documents", $record, "id=$id");
	    			$unitkerja_lama = $this->Unitkerja_model->show_sql(false)->getBy($doc['unitkerja_id']);
		            $unitkerja_baru = $this->Unitkerja_model->getBy($record['unitkerja_id']);
		    			// print_r($unitkerja_lama);
		            $this->addHistory($id, "This document move from workunit ".$unitkerja_lama['name']." to ".$unitkerja_baru['name']);
	    			$text = "This document is moved from ".$doc['creator_name']." as creator";
					$this->addHistory($id, $text);
	    		}
    		}
    		else{
    			dbUpdate("documents", $record, "id=$id");
    			$text = "This document is moved from ".$doc['creator_name']." as creator";
				$this->addHistory($id, $text);
    		}
    		$sql = " update documents set published_at = sysdate, validity_date=add_months(sysdate, +1) where id=$id  ";
			dbQuery($sql);
    		// if($doc['type_id'] == Document_model::INSTRUKSI_KERJA || $codes[0] == 'FI'){
    		// 	$this->moveChangeUnit($id, false, $record); // harus kirim creator,approver beserta jabatannya untuk diinsertkan di data doc yang baru
    		// }
    		// else{
    		// 	if (dbUpdate("documents", $record, "id=$id")){
	    	// 		$this->moveChangeUnit($id, false); // hanya update creator, approver beserta jabatannya di doc lama, lalu tidak ngapa2in
	    	// 	}
    		// }
    		
   //  		// mengganti creator dan approver beserta jabatan masing2 ke dokumen yang sekarang(lama sebelum di gantikan)
			SessionManagerWeb::setFlashMsg(true, 'Success to Choose new User.');
		} 
		else {
			SessionManagerWeb::setFlashMsg(false, 'Failed to Choose new User.');
		}
    	echo $id;
    }

    public function ajaxUpdateApprover($id){
    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(), $id);
		$doc = array_change_key_case($document, CASE_LOWER);
		$changeunit = array();
		// $changeunit['approver_verification'] = 1;
		$changeunit['approver'] = 0;
		$changeunit['approver_id'] = $this->input->post('approver_id', true);
		$sql = "select creator from document_changeunit where document_id=$id";
		$is_move_creator = dbGetOne($sql);
		if (!$is_move_creator){
			$changeunit['is_moved']=1;
		}
		if (dbUpdate("document_changeunit", $changeunit, "document_id=$id")){
			$this->load->model('Notification_model');
			$this->Notification_model->generate(Notification_model::ACTION_CHANGEUNIT_APPROVER, $id, SessionManagerWeb::getUserID());
			$text .= "This document is moved from ".$doc['approver_name']." as approver";
			$this->addHistory($id, $text);
			$record = array();
			$record['approver_id'] = $changeunit['approver_id'];
    		$record['approver_jabatan_id'] = $this->User_model->filter(" where \"user_id\"='".$record['approver_id']."' ")->getOneFilter("jabatan_id");
    		dbUpdate("documents", $record, "id=$id");

			SessionManagerWeb::setFlashMsg(true, 'Success to Choose new User.');
		} else {
			SessionManagerWeb::setFlashMsg(false, 'Failed to Choose new User.');
		}
    	echo $id;
    }

    public function ajaxUpdateCompany($id){
    	$document = $this->model->getDocumentBy(SessionManagerWeb::getUserID(), $id);
		$doc = array_change_key_case($document, CASE_LOWER);    	
    	$this->load->model('Jabatan_model');
		$changeunit = $this->input->post();
		if(dbUpdate("document_changeunit", array('unitkerja' => 0, 'unitkerja_id' => $changeunit['workunit']), "document_id=$id")){
			$record['creator_id'] = $changeunit['creator_id'];
    		$record['creator_jabatan_id'] = $changeunit['creator_jabatan_id'];
    		$record['approver_id'] = $changeunit['approver_id'];
    		$record['approver_jabatan_id'] =  $changeunit['approver_jabatan_id'];
    		$record['unitkerja_id'] = $changeunit['workunit'];
    		$record['company'] = $changeunit['company'];
    		$new_id = $this->moveChangeUnit($id, false, $record);
    		SessionManagerWeb::setFlashMsg(true, 'Success to Choose new Workunit.');
    		echo $new_id;
    		// echo $id;
    	}
		// echo "<pre>";print_r($changeunit);die();
    }

    public function setChangeUnitReason($id){
    	$this->load->model("User_model");
    	$is_approver = $this->input->post('is_approver', true);
    	$record = array();
    	$new = $this->model->getChangeWorkunitUserId($id);
    	if (!$is_approver){
    		$creator = $this->User_model->filter(" where \"user_id\"='".$new['creator_id']."' ")->getOneFilter("name");
    		$record['creator_reason'] = "<b style='color:green'>".$creator."</b> <b style='color:red'>REJECTED</b>, because <b>".$this->input->post('reason_textarea', true)."</b>";
    		$record['creator_verification'] = 0;
    		$record['creator_id'] = null;
    	} else {
    		$approver = $this->User_model->filter(" where \"user_id\"='".$new['approver_id']."' ")->getOneFilter("name");
    		$record['approver_reason'] = "<b style='color:green'>".$approver."</b> <b style='color:red'>REJECTED</b>, because <b>".$this->input->post('reason_textarea', true)."</b>";
    		$record['approver_verification'] = 0;
    		$record['approver_id'] = null;
    	}

    	if (dbUpdate("document_changeunit", $record, "document_id=".$id)){
    		if (!$is_approver){
    			$this->load->model("Notification_model");
				$this->Notification_model->generate(Notification_model::ACTION_REJECT_CHANGEUNIT_CREATOR, $id, SessionManagerWeb::getUserID());
    		} else {
    			$this->load->model("Notification_model");
				$this->Notification_model->generate(Notification_model::ACTION_REJECT_CHANGEUNIT_APPROVER, $id, SessionManagerWeb::getUserID());
    		}
    		
    		SessionManagerWeb::setFlashMsg(true, 'Success to Reject Verification');
    	} else {
			SessionManagerWeb::setFlashMsg(false, 'Failed to Reject Verification');
    	}

    	redirect("web/document/changeunit/$id");
    }

   	public function ajaxGetVerification($id){

   	}

   	public function ajaxDeleteVerification($id){

   	}

   	public function ajaxUpdateVerification($id=NULL){

   	}

   	public function tryWatermark(){
   		$this->watermarkNew('IK__AUD_50045221_002_R0.pdf', '3000', 'M.REZA' , '62627', 'pdf');
   	}

   	public function PlaceWatermark($file, $idCompany, $username, $userid, $extfile) {
   		$this->load->model('Unitkerja_model');
   		$namaCompany = $this->Unitkerja_model->getOne("0000".$idCompany."", 'name');

        // require_once(APPPATH."/third_party/fpdf/fpdf.php");
        require_once(APPPATH."/third_party/fpdf/rotation.php");
        // require_once(APPPATH."/third_party/fpdf/pdf.php");
        $dataCompany =  array(
									'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
									'3000'	=> 'PT. SEMEN PADANG',
									'4000'	=> 'PT. SEMEN TONASA',
									'5000'	=> 'PT. SEMEN GRESIK',
									'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
								);
        // $pdf = new FPDI();
        $pdf = new PDF_Rotate();

       	$name = uniqid();
        //font
        $font = './assets/web/fonts/arial.ttf';
        //font size
        $font_size = 15;
        //image width
        $width = 1500;
        //text margin
        $margin = 0;

        $data_text = array();
        $explodefilename = explode('.', $file);
        $filename = $explodefilename[0];
        $data_text['text1']['value']         = "Dokumen ini terbatas untuk kalangan sendiri dan milik ".$namaCompany.", Dilarang membagikan, menyalin, dan";
        $data_text['text1']['name']          = $filename.'_'.$userid.'_text1';
        $data_text['text2']['value']          = "mencetak tanpa izin. Kesalahan dan perbedaan isi diluar tanggung jawab ".$namaCompany;
        $data_text['text2']['name']            = $filename.'_'.$userid.'_text2';
        $data_text['text3']['value']     = "Diakses oleh ".$username." (".$_SERVER['REMOTE_ADDR']."), Tanggal ".date("d-m-Y");
        $data_text['text3']['name']      = $filename.'_'.$userid.'_text3';
        // $data_text['tgl_view']['value']     = date("d-m-Y");
        // $data_text['tgl_view']['name']      = uniqid();

        foreach ($data_text as $text) {
            $text_a = explode(' ', $text['value']);
            $text_new = '';
            foreach($text_a as $word){
                //Create a new text, add the word, and calculate the parameters of the text
                $box = imagettfbbox($font_size, 0, $font, $text_new.' '.$word); 
                //if the line fits to the specified width, then add the word with a space, if not then add word with new line
                // echo "<pre>";print_r($box);echo "</pre>";
                if($box[2] > $width - $margin*2){
                    $text_new .= "\n".$word;
                } else {
                    $text_new .= " ".$word;
                }
            }
            // //trip spaces
            $text_new = trim($text_new);
            //new text box parameters
            // echo "<pre>";print_r($text_new);echo "</pre>";
            $box = imagettfbbox($font_size, 0, $font, $text_new);
            //new text height
            $height = $box[1] + $font_size + $margin * 2; // set height
            ///////////////////////CREATE FILE 1///////////////////////////////
            $img = imagecreatetruecolor($width,$height); // create new img and set width height
            // Background color
            $bg = imagecolorallocate($img, 255, 255, 255);  // set color from new img
            imagefilledrectangle($img, 0, 0,$width ,$height , $bg); //make background from new img, ()
            imagecolortransparent($img, $bg);
            ///////////////////////////////END OF FILE 1/////////////////////////////////////
            ////////////////////////////////FILE 2////////////////////////////////////////////
                // //create image
            $im = imagecreatetruecolor($width, $height);
             
            //create colors
            // $tbg = imagecolorallocate($im, 255, 255, 255);
            $white = imagecolorallocate($im, 255, 255, 255);
            $red = imagecolorallocate($im, 255, 0, 0);
            //color image
            imagefilledrectangle($im, 0, 0, $width, $height, $white);
            // $color = imagecolorallocate($img, 0, 0, 0);
            //add text to image
            imagettftext($im, $font_size, 0, $margin, $font_size+$margin, $red, $font, $text_new);
             $op = 100;
                if ( ($op < 0) OR ($op >100) ){
                    $op = 100;
                }
            imagecopymerge($img, $im, 0, 0, 0, 0, $width, $height, $op); // menggabung file $blank dan $img
            $dirpng = "./assets/uploads/document/files/";
            imagepng($img,$dirpng.''.$text['name'].".png");
            ////////////////////////////////END OF FILE 2////////////////////////////////////////////
        }
        // $dirpng = "./assets/uploads/document/files/";
        /////////////////////////////////OPACITY LOGO////////////////////////////////////////////////
  //       $this->load->model('Unitkerja_model');
		// $photoCompany = $this->Unitkerja_model->getOneBy('0000'.$idCompany, "\"PHOTO\"");
		// // echo "<pre>";print_r($initialCompany);echo "</pre>";
		// if(!file_exists("./assets/uploads/company/0000".$idCompany."_opa.png")){
		// 	// return $photoCompany;
		// 	$image = imagecreatefrompng('./assets/uploads/company/'.$photoCompany);
		// 	$opacity = 0.2;
		// 	imagealphablending($image, false); // imagesavealpha can only be used by doing this for some reason
		// 	imagesavealpha($image, true); // this one helps you keep the alpha. 
		// 	$transparency = 1 - $opacity;
		// 	imagefilter($image, IMG_FILTER_COLORIZE, 0,0,0,127*$transparency); // the fourth parameter is alpha
		// 	// header('Content-type: image/png');/
		// 	imagepng($image, './assets/uploads/company/0000'.$idCompany.'_opa.png' );
		// } 
		/////////////////////////////////END OF OPACITY LOGO///////////////////////////
        $explodefilename = explode('.', $file);
        $filename = $explodefilename[0];
        $getFile = $filename.'_header.pdf';
        // $extfile  = $explodefilename[1];
        $file = "./assets/uploads/document/files/".$getFile;
        // echo $file;
        if (file_exists($file)){
           $pagecount = $pdf->setSourceFile($file);
           // echo $pagecount;
            // echo "b";
        } else {
            return FALSE;
            // echo "a";
        }
        // $logo = '0000'.$idCompany.'_opa.png';
        $company = $dataCompany[$idCompany];
		if($company ==  'PT. Semen Indonesia (Persero) Tbk.'){
			$logo = 'Untitled-1-04.png';
		}
		elseif($company == 'PT. SEMEN PADANG'){
			$logo = 'Untitled-1-02.png';
		}
		elseif($company == 'PT. SEMEN TONASA'){
			$logo = 'Untitled-1-03.png';
		}
		elseif($company == 'PT. SEMEN GRESIK'){
			$logo = 'Untitled-1-01.png';
		}
		elseif($company == 'PT. SEMEN KUPANG INDONESIA'){

		}

        for($i = 1; $i <= $pagecount; $i++){
            $tpl = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($tpl);
            // echo "<pre>";print_r($size);echo "</pre>";
            $pageSize = "A4";
            if($size['h'] == 297.00008333333){ $pageSize = "A4";}
            elseif($size['h'] == 355.6){ $pageSize = "Legal";}
            elseif($size['h'] == 279.4){ $pageSize = "Letter";}
            elseif($size['h'] == 419.99958333333){ $pageSize = "A3";}
            else {$pageSize = "A4";}
              // echo $dirpng."".$data_text['text1']['name'].'.png<br>';
            if($size['w'] <= $size['h']){
            	// echo "masuk if<br>";
                $pdf->addPage("P", $pageSize);
                // $pdf->Image('./assets/uploads/company/'.$logo,55,90,100,77);
                // echo $dirpng."".$data_text['text1']['name'].'.png<br>';
                // $pdf->RotatedImage('./assets/web/images/logo/'.$logo,55,90,100,100);
                $pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 207, 60, 250, 3, 270);
                // unlink($dirpng."".$data_text['text1']['name'].'.png');
                //echo $dirpng."".$data_text['text2']['name'].'.png<br>';
                $pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 202, 95, 250, 3, 270);
                // unlink($dirpng."".$data_text['text2']['name'].'.png');
                //echo $dirpng."".$data_text['text3']['name'].'.png<br>';
                $pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 197, 100, 250, 3, 270);
                // unlink($dirpng."".$data_text['text3']['name'].'.png');
            }
            else{
            	// echo "masuk else<br>";
                $pdf->addPage("L", $pageSize);
                // $pdf->Image('./assets/web/images/logo/'.$logo,115,70,100,100);
                $pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 290, 45, 175, 3, 270);    
                $pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 285, 55, 175, 3, 270);
                $pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 280, 65, 250, 3, 270);
                
            }
           
                // $x -= 5;
                // $y += 10;
            // }
            $pdf->useTemplate($tpl, 0, 0, 0, 0, TRUE);
            unlink($dirpng."".$data_text['text1']['name'].'.png');
			unlink($dirpng."".$data_text['text2']['name'].'.png');
			unlink($dirpng."".$data_text['text3']['name'].'.png');
           
        }
        // // echo './assets/uploads/document/files/'.$filename.'_'.$userid.'.'.$extfile;
        $pdf->Output('./assets/uploads/document/files/'.$filename.'_'.$userid.'_watermarked.'.$extfile, 'F');
    }

    public function tesobsoleteWatermark(){
    	$ciConfig = $this->config->item('utils');
		$full_path = $ciConfig['full_upload_dir'].'document/files/';
    	rename($full_path.'IK_SMI_ASM_50045221_001_R0_header.pdf', $full_path.'IK_SMI_ASM_50045221_001_R0_header_yuk.pdf');
    	// $document = $this->model->getBy(4867);
    	// $this->load->model('Unitkerja_model');
    	// $idCompany = $this->Unitkerja_model->getOneBy($document['UNITKERJA_ID'], "\"COMPANY\"");
    	// $explodefilename = explode('.', $document['DOCUMENT']);
     //    $filename = $explodefilename[0].'.pdf';
    	// echo "<pre>";print_r($filename);die();
    	// $this->ObsoleteWatermark('IK_SMI_FNC_50045259_001_R0_header.pdf', 2000, 'pdf');
    }
    public function obsoleteWatermark($file, $idCompany, $extfile) {
   		$this->load->model('Unitkerja_model');
   		$namaCompany = $this->Unitkerja_model->getOne("0000".$idCompany."", 'name');

        // require_once(APPPATH."/third_party/fpdf/fpdf.php");
        require_once(APPPATH."/third_party/fpdf/rotation.php");
        // require_once(APPPATH."/third_party/fpdf/pdf.php");
        $dataCompany =  array(
									'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
									'3000'	=> 'PT. SEMEN PADANG',
									'4000'	=> 'PT. SEMEN TONASA',
									'5000'	=> 'PT. SEMEN GRESIK',
									'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
								);
        // $pdf = new FPDI();
        $pdf = new PDF_Rotate();

        $dirpng = "./assets/uploads/document/files/";
        $explodefilename = explode('.', $file);
        $filename = $explodefilename[0];
        $getFile = $filename.'_header_old.pdf';
        // $extfile  = $explodefilename[1];
        $file = "./assets/uploads/document/files/".$getFile;
        // echo $file;
        if (file_exists($file)){
           $pagecount = $pdf->setSourceFile($file);
           // echo $pagecount;
            // echo "b";
        } else {
            return FALSE;
            // echo "a";
        }

        for($i = 1; $i <= $pagecount; $i++){
            $tpl = $pdf->importPage($i);
            $size = $pdf->getTemplateSize($tpl);
            // echo "<pre>";print_r($size);echo "</pre>";
            $pageSize = "A4";
            if($size['h'] == 297.00008333333){ $pageSize = "A4";}
            elseif($size['h'] == 355.6){ $pageSize = "Legal";}
            elseif($size['h'] == 279.4){ $pageSize = "Letter";}
            elseif($size['h'] == 419.99958333333){ $pageSize = "A3";}
            else {$pageSize = "A4";}
            $x = $size['w'] * (5/100);
            $y = ($size['h'] * (55/100))+10;
              // echo $dirpng."".$data_text['text1']['name'].'.png<br>';
            if($size['w'] <= $size['h']){
            	// echo "masuk if<br>";
                $pdf->addPage("P", $pageSize);
                
                $pdf->RotatedImage($dirpng.'obsolete.png', $x, $y, 70, 7, 90);
            }
            else{
            	// echo "masuk else<br>";
                $pdf->addPage("L", $pageSize);
                // $pdf->Image('./assets/web/images/logo/'.$logo,115,70,100,100);
                $pdf->RotatedImage($dirpng.'obsolete.png', $x, $y, 70, 7, 90);    
                
            }
           
                // $x -= 5;
                // $y += 10;
            // }
            $pdf->useTemplate($tpl, 0, 0, 0, 0, TRUE);
            unlink($dirpng."".$data_text['text']['name'].'.png');
           
        }
        // // echo './assets/uploads/document/files/'.$filename.'_'.$userid.'.'.$extfile;
        $pdf->Output('./assets/uploads/document/files/'.$filename.'_header.'.$extfile, 'F');
    }

    function watermarkNew($file, $idCompany, $username, $userid, $extfile){
    	require_once(APPPATH."/third_party/fpdf/pdf.php");
		$pdf = new PDF();

		$explodefilename = explode('.', $file);
        $filename = $explodefilename[0];
        $file = "./assets/uploads/document/files/".$file;
        if (file_exists($file)){
           $pagecount = $pdf->setSourceFile($file);
           // echo $pagecount;
            // echo "b";
        } else {
            return FALSE;
            // echo "a";
        }

        $data = array(
			    		'id_document'	=> 0,
			    		'judul'			=> "Judul Judul",
			    		'kode_dokumen'	=> "A/B/C/000",
			    		'revisi'		=> 0,
			    		'tanggal_revisi'=> "28-08-2018",
			    		'unit_kerja'	=> $dataHF['unit_kerja'],
			    		'nama_company'	=> "Semen Indonesia",
			    		'id_company'	=> '2000',
			    		'logo'			=> $logo,
			    		'tipe_dokumen'=> "Instruksi Kerja",
			    		'filename'		=> $dataHF['filename']
			    	);
        $pdf->setDataHeader($data);
        $dataCompany =  array(
									'2000'	=> 'PT. Semen Indonesia (Persero) Tbk.',
									'3000'	=> 'PT. SEMEN PADANG',
									'4000'	=> 'PT. SEMEN TONASA',
									'5000'	=> 'PT. SEMEN GRESIK',
									'9000'	=> 'PT. SEMEN KUPANG INDONESIA'
								);
        $company = $dataCompany[$idCompany];
		if($company ==  'PT. Semen Indonesia (Persero) Tbk.'){
			$logo = 'Untitled-1-04.png';
		}
		elseif($company == 'PT. SEMEN PADANG'){
			$logo = 'Untitled-1-02.png';
		}
		elseif($company == 'PT. SEMEN TONASA'){
			$logo = 'Untitled-1-03.png';
		}
		elseif($company == 'PT. SEMEN GRESIK'){
			$logo = 'Untitled-1-03.png';
		}
		elseif($company == 'PT. SEMEN KUPANG INDONESIA'){

		}
		$this-> createTextWatermark($filename, $userid, $username,$idCompany);
		$data_text['text1']['name']          = $filename.'_'.$userid.'_text1';
		$data_text['text2']['name']            = $filename.'_'.$userid.'_text2';
		$data_text['text3']['name']      = $filename.'_'.$userid.'_text3';
		$dirpng = "./assets/uploads/document/files/";
        for($i = 1; $i <= $pagecount; $i++){
            $tpl = $pdf->importPage($i);
            $pdf->setOrientationForHeader('P');
            $pdf->addPage();
            $pdf->RotatedImage($dir.'assets/web/images/table-2.png', 5, 6, 200, 24, 0);
            $pdf->RotatedImage('./assets/web/images/logo/'.$logo,55,90,100,100);
            $pdf->RotatedImage($dirpng."".$data_text['text1']['name'].'.png', 207, 60, 250, 3, 270);
			$pdf->RotatedImage($dirpng."".$data_text['text2']['name'].'.png', 202, 95, 250, 3, 270);
 			$pdf->RotatedImage($dirpng."".$data_text['text3']['name'].'.png', 197, 100, 250, 3, 270);
            $pdf->useTemplate($tpl, 0, 0, 0, 0, TRUE);
           
        }
     //    $file_location = './assets/uploads/document/files/watermark_coba.pdf';
    	// ob_clean(); 
    	// $pdf->Output($file_location,'F');
        // // echo './assets/uploads/document/files/'.$filename.'_'.$userid.'.'.$extfile;
        $pdf->Output('./assets/uploads/document/files/watermark_coba.pdf', 'F');
    }

    public function changeUploadToCreate($id){
    	$data = array();
    	$data['is_upload']=0;
    	dbUpdate('documents', $data, "id=$id");
    	header('Location: ' . site_url('web/document/detail').'/'.$id);
    }

    public function PendingDocEvaluation(){
    	$getCompany = SessionManagerWeb::getCompany();
    	// echo $getCompany;
        $company = substr($getCompany,4);
        // echo $company;
    	$this->load->model('Unitkerja_model');
    	$tes = $this->Unitkerja_model->limit(11)->getAllClone("and COMPANY='".$company."'");
    	// echo "<pre>";print_r($tes);echo "</pre>";
    	// $temp = array();
    	foreach ($tes as $a) {
    		// echo $a['id']."<br>";
    		$this->load->model('Document_model');
    		$data = $this->Document_model->getPendingDocEvaluation($a['id']);
    		$temp[$a['id']] = array(
    			'name'			 => $a['name'],
    			'jumlah_approve' => $data['approve']['JUMLAH'],
    			'jumlah_evaluasi'=> $data['evaluasi']['JUMLAH'],
    			'jumlah_akhir'	 => $data['approve']['JUMLAH']+$data['evaluasi']['JUMLAH']
    			
    		);
    		// echo "<pre>";print_r($temp);echo "</pre>";
    	}
    	return $temp;
    }

    public function ApproveEvaluationDocSub($company){
	        ////////////////////APPROVE////////////////////////
    		$this->load->model('Document_model');
    		$data = $this->Document_model->getPendingDocApprove($company);
    		// echo "<pre>";print_r($data);echo "</pre>";
    		$tempAprrove = array();
    		foreach ($data as $d) {
    			$this->load->model('Workflow_model');
    			$data1 = $this->Workflow_model->getTasks($d['WORKFLOW_ID'], $d['WORKFLOW_URUTAN']);
    			// echo "<pre>";print_r($data1);echo "</pre>";
    			if($data1['user'] == 'C'){
    				$userid = $d['CREATOR_ID'];
    				// echo "c ".$userid."<br>";
    			}
    			elseif($data1['user'] == 'P'){
    				$userid= $d['APPROVER_ID'];
    				// echo "p ".$userid."<br>";
    			}
    			elseif($data1['user'] == 'D'){
    				$userid = $d['USER_ID'];
    			}
    			elseif($data1['user'] == 'R'){
    				// echo "RRRR".$d['ID'];
    				$this->load->model('Document_reviewer_model');
    				$getUserId = $this->Document_reviewer_model->getDocumentReviewers($d['ID'], $d['WORKFLOW_ID']);
    				$userid = $getUserId['DOCUMENT_REVIEWER'];
    				// echo "r ".$userid."<br>";
    			}
    			elseif($data1['user'] == 'E'){
    				$this->load->model('Document_delegates_model');
		    		$document_id = $d['ID'];
		    		$workflow_id = $d['WORKFLOW_ID'];
		    		$workflow_urutan = $data1['urutan'];
		    		$conditions = array(
		    			" document_id=$document_id ", 
		    			"workflow_id=$workflow_id", 
		    			"workflow_urutan=$workflow_urutan"
		    		);
		    		$condition = implode(' and ', $conditions);
		    		$delegated = $this->Document_delegates_model->filter($condition)->getBy();
		    		$this->load->model('User_model');
		    		if (!$delegated){
			    		$users = $this->User_model->getAll("where \"role\"='$role' and \"is_chief\"='X' and \"company\"='$company' ");
		    		} else {
		    			$user_id = $delegated['delegate'];
		    			$users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
		    		}
		    		$actor_name = $users[0]['id'];
    			}
    			$this->load->model('Unitkerja_model');
    			$this->load->model('User_model');
    			$unitkerja_id = $this->User_model->getUnitKerjaFromUser($userid);
    			// echo "unitkerja_id<br>";
    			// echo "<pre>";print_r($unitkerja_id);echo "</pre>";
    			$parentCompany = $this->Unitkerja_model->getBy($unitkerja_id['PARENT']);
    			// echo "parentCompany<br>";
    			// echo "<pre>";print_r($parentCompany);echo "</pre>";
    			if($unitkerja_id){
    				if($unitkerja_id['LEVEL'] == 'DEPT'){
    					
    					if(isset($tempAprrove[$unitkerja_id['unitkerja_id']])){
    						$tempAprrove[$unitkerja_id['unitkerja_id']] += 1;
	    				}
	    				else{
	    					$tempAprrove[$unitkerja_id['unitkerja_id']] = 1;
	    				}
	    				// echo "MASUK SINI IF <br>";
    				}
    				elseif($unitkerja_id['LEVEL'] == 'BIRO'){
    					// echo $unitkerja_id['unitkerja_id']." dengan level ".$unitkerja_id['LEVEL']."<br>";
    					// echo $parentCompany['id']." dengan level ".$parentCompany['level']."<br>";
    					if(isset($tempAprrove[$parentCompany['id']])){
    						$tempAprrove[$parentCompany['id']] += 1;
	    				}
	    				else{
	    					$tempAprrove[$parentCompany['id']] = 1;
	    				}
	    				// echo "MASUK SINI ELSEIF <br>";
    				}
    				elseif($unitkerja_id['LEVEL'] == 'SECT'){
    					$this->load->model('Unitkerja_model');
		    			$dataDept = $this->Unitkerja_model->getBy($parentCompany['parent']);
		    			if(isset($temp[$parentCompany['parent']])){
		    				$tempAprrove[$parentCompany['id']] += 1;
		    			}
		    			else{
		    				$tempAprrove[$parentCompany['id']] = 1;
		    			}
    				}


    			}
    				
    		}
    		// echo "<pre>";print_r($tempAprrove);echo "</pre>";
    		////////////////////END OF APPROVE////////////////////////
    		////////////////////EVALUASI////////////////////////
    		$this->load->model('Document_model');
	    	$data = $this->Document_model->getPendingDocEvaluation($company);
	    	$temp = array();
	    	// echo "<pre>";print_r($data);echo "</pre>";
	    	foreach ($data as $d) {
	    		// echo $d['PARENTCOMPANY']."<br>";
	    		$this->load->model('Unitkerja_model');
	    		$parentCompany = $this->Unitkerja_model->getBy($d['PARENTCOMPANY']);
	    		// echo "<pre>";print_r($parentCompany);echo "</pre>";
	    		// echo "id unitkerja ".$d['UNITKERJA_ID']." levelnya adalah ".$d['LEVEL']."<br>";
	    		if($d['LEVEL'] == 'DEPT'){
	    			if(isset($temp[$d['UNITKERJA_ID']])){
		    				$temp[$d['UNITKERJA_ID']]['jumlah_evaluasi'] += 1;	
		    			}
		    		else{
		    			$temp[$d['UNITKERJA_ID']] = array(
						    			'name'			 => $d['NAME'],
						    			'jumlah_evaluasi' => 1,
						    			'jumlah_approve' => 0,
						    			'grand_total'	=> 0
						    			
						);
		    		}
	    		}
	    		elseif($d['LEVEL'] == 'BIRO'){ // AMBIL DATA DEPARTMENTNYA
	    			if(isset($temp[$parentCompany['id']])){
		    				$temp[$parentCompany['id']]['jumlah_evaluasi'] += 1;	
		    			}
		    		else{
		    			$temp[$parentCompany['id']] = array(
						    			'name'			 => $parentCompany['name'],
						    			'jumlah_evaluasi' => 1,
						    			'jumlah_approve' => 0,
						    			'grand_total'	=> 0
						    			
						);
		    		}
	    		}
	    		elseif($d['LEVEL'] == 'SECT'){ // ambil data parent(biro) untuk ambil data departmentnya
	    			$this->load->model('Unitkerja_model');
		    		$dataDept = $this->Unitkerja_model->getBy($parentCompany['parent']);
		    		if(isset($temp[$dataDept['id']])){
		    				$temp[$dataDept['id']]['jumlah_evaluasi'] += 1;	
		    			}
		    		else{
		    			$temp[$dataDept['id']] = array(
						    			'name'			 => $dataDept['name'],
						    			'jumlah_evaluasi' => 1,
						    			'jumlah_approve' => 0,
						    			'grand_total'	=> 0
						    			
						);
		    		}

		    	}
		    	elseif($d['LEVEL'] == 'GRP'){ // ambil data parent(sect) untuk ambil data biro lalu departmennya
		    		$this->load->model('Unitkerja_model');
		    		$dataBiro = $this->Unitkerja_model->getBy($parentCompany['parent']);
		    		$dataDept = $this->Unitkerja_model->getBy($dataBiro['parent']);
		    		if(isset($temp[$dataDept['id']])){
		    				$temp[$dataDept['id']]['jumlah_evaluasi'] += 1;	
		    			}
		    		else{
		    			$temp[$dataDept['id']] = array(
						    			'name'			 => $dataDept['name'],
						    			'jumlah_evaluasi' => 1,
						    			'jumlah_approve' => 0,
						    			'grand_total'	=> 0
						    			
						);
		    		}

		    	}
	    	}
	    	// echo "<pre>";print_r($temp);echo "</pre>";


    		// $this->load->model('Unitkerja_model');
	    	// $tes = $this->Unitkerja_model->limit(50)->getAll("and COMPANY='".$company."' AND \"LEVEL\"='DEPT' ");
	    	// echo "<pre>";print_r($tes);echo "</pre>";

	    	// $temp = array();
	    	// foreach ($tes as $a) {
	    	// 	$parentCompany = $this->Unitkerja_model->getBy($a['parent']);
	    	// 	// echo "<pre>";print_r($parentCompany);echo "</pre>";
	    	// 	$this->load->model('Document_model');
	    	// 	$data = $this->Document_model->getPendingDocEvaluation($a['id']);
	    	// 	echo "<pre>";print_r($data);echo "</pre>";
	    	// 	if($data){
	    	// 		// echo "ada data<br>";
		    // 		if($a['level'] == 'DEPT' || $parentCompany['level'] == 'COMP'){
		    // 			if(isset($temp[$a['id']])){
		    // 				$temp[$a['id']]['jumlah_evaluasi'] += $data['JUMLAH'];	
		    // 			}
		    // 			else{
		    // 				$temp[$a['id']] = array(
						//     			'name'			 => $a['name'],
						//     			'jumlah_evaluasi' => $data['JUMLAH'],
						//     			'jumlah_approve' => 0
						    			
						// 	);
		    // 			}
		    			
		    // 		}
		    // 		elseif($a['level'] == 'BIRO'){
		    // 			$this->load->model('Unitkerja_model');
		    // 			$dataDept = $this->Unitkerja_model->getBy($a['parent']);
		    // 			if(isset($temp[$a['parent']])){
		    // 				$temp[$a['parent']]['jumlah_evaluasi'] += $data['JUMLAH'];
		    // 			}
		    // 			else{
		    // 				$temp[$a['parent']] = array(
						//     			'name'			 => $dataDept['name'],
						//     			'jumlah_evaluasi' => $data['JUMLAH'],
						//     			'jumlah_approve' => 0,
						//     			'grand_total'	=> 0
						    			
						// 	);
		    // 			}
		    // 			// echo "<pre>";print_r($dataDept);echo "</pre>";

		    // 		}
		    // 		elseif($a['level'] == 'SECT'){
		    // 			$this->load->model('Unitkerja_model');
		    // 			$dataDept = $this->Unitkerja_model->getBy($parentCompany['parent']);
		    // 			if(isset($temp[$parentCompany['parent']])){
		    // 				$temp[$parentCompany['parent']]['jumlah_evaluasi'] += $data['JUMLAH'];
		    // 			}
		    // 			else{
		    // 				$temp[$parentCompany['parent']] = array(
						//     			'name'			 => $dataDept['name'],
						//     			'jumlah_evaluasi' => $data['JUMLAH'],
						//     			'jumlah_approve' => 0,
						//     			'grand_total'	=> 0
						    			
						// 	);
		    // 			}
		    // 			// echo "ini data sect<br>";
		    // 			// echo "<pre>";print_r($dataDept);echo "</pre>";
		    // 		}
	    	// 	}
	    	// 	else{
	    	// 		echo "tidak ada data<br>";
	    	// 	}
	    	// 	// echo $a['level']."<br>";
	    		
	    		

	    	// 	// echo "<pre>";print_r($temp);echo "</pre>";
	    	// }
	    	////////////////////END OF EVALUASI////////////////////////

	    	$unitkerja = $temp;
	    	$this->load->model('Unitkerja_model');
	    	foreach ($tempAprrove as $key => $value) {
	        	if (!isset($unitkerja[$key])){
					$unitkerja[$key]['name'] = $this->Unitkerja_model->getOneBy($key, 'name');
		        	$unitkerja[$key]['jumlah_evaluasi'] = 0;
		        	$unitkerja[$key]['jumlah_approve'] = $value;
		        } else {
		        	$unitkerja[$key]['jumlah_approve'] = $value;
	        	}
	        	// $unitkerja[$key]['grand_total'] = $unitkerja[$key]['jumlah_evaluasi'] + $unitkerja[$key]['jumlah_approve'];
	    	
	    	}
	    	foreach ($unitkerja as $key => $value) {
	    		// echo $key."<br>";
	    		$unitkerja[$key]['grand_total'] = $unitkerja[$key]['jumlah_evaluasi'] + $unitkerja[$key]['jumlah_approve'];
	    	}
	    	// echo "<pre>";print_r($unitkerja);echo "</pre>";
	    	// return $unitkerja;
	    	// array_multisort($unitkerja,SORT_DESC);
	    	return $unitkerja;
	    	// echo '<pre>';print_r($unitkerja);echo "</pre>";
	    // 	var_dump($unitkerja);
	    // 	die();
    		
    		
    		
    }

    public function cekCompany(){
	        $this->load->model('Unitkerja_model');
	        $listCompany = $this->Unitkerja_model->getCompany();
	        echo "<pre>";print_r($listCompany);echo "</pre>";
	        // $rows = dbGetRows($sql);
    }

    public function ApproveEvaluationDoc(){
    	$this->load->model('Unitkerja_model');
    	$temp = array();
	    $listCompany = $this->Unitkerja_model->getCompany();
	    foreach ($listCompany as $list) {
	    	$company = $list['COMPANY'];
	    	$temp[$company] = $this->ApproveEvaluationDocSub($company);
	    	foreach ($temp[$company] as $listDep => $value) {
	    		// echo "<pre>";print_r($listDep);echo "</pre>";
	    		$this->load->model('Statistik_unitkerja_model');
	    		$cekData = $this->Statistik_unitkerja_model->filter("WHERE UNITKERJA_ID='".$listDep."' AND COMPANY='".$company."' AND STATISTIC_TYPE='E' AND PENDING_APPROVE=".$value['jumlah_approve']." AND PENDING_EVALUATION=".$value['jumlah_evaluasi'])->getAll();
	    		echo "<pre>";print_r($cekData);echo "</pre>";
	    		if($cekData){
	    			$dataUpdate = array(
	    				'PENDING_APPROVE'	=> $value['jumlah_approve'],
	    				'PENDING_EVALUATION'=> $value['jumlah_evaluasi'],
	    				'PENDING_TOTAL'		=> $value['grand_total']
	    			);
	    			$updateData = $this->Statistik_unitkerja_model->updateStatistic($dataUpdate,"UNITKERJA_ID='".$listDep."' AND COMPANY='".$company."' AND STATISTIC_TYPE='EVALUATION' ");
	    		}
	    		else{
	    			$dataInsert = array(
	    				'UNITKERJA_ID'		=> $listDep,
	    				'COMPANY'			=> $company,
	    				'STATISTIC_TYPE'    => Statistik_unitkerja_model::EVALUASI,
	    				'PENDING_APPROVE'	=> $value['jumlah_approve'],
	    				'PENDING_EVALUATION'=> $value['jumlah_evaluasi'],
	    				'PENDING_TOTAL'		=> $value['grand_total']
	    			);
	    			$insertData = $this->Statistik_unitkerja_model->insertStatistic($dataInsert);
	    		}
	    	}
	    	
	    }
	    // echo "<pre>";print_r($temp);echo "</pre>";
    }

    public function cobaApprove($page){
    	$getCompany = SessionManagerWeb::getCompany();
    	$this->load->model('Statistik_unitkerja_model');
    	$tes = $this->Statistik_unitkerja_model->getStatistikEvaluation($getCompany,$page);
    	echo "<pre>";print_r($tes);echo "</pre>";
    }


    public function ajaxSetSessionPage(){
    	$_SESSION['page_evaluation'] = $this->input->post('page',TRUE);
    }

    public function setFilterChangeUnit($is_show=0){
    	if(isset($_SESSION['filter_changeunit'])){
    		unset($_SESSION['filter_changeunit']);
    	}
    	
   		if ($this->input->post('numPage', true)!=NULL and $this->input->post('numPage', true)!='')
	   		$_SESSION['page'] = $this->input->post('numPage', true);
	   	else 
	   		$_SESSION['filter_changeunit']['page'] =1;

   		if ($this->input->post('keyword', true)!=NULL and $this->input->post('keyword', true)!='')//from search navbar
	   		$_SESSION['filter_changeunit']['keyword'] = $this->input->post('keyword', true);


	   	if ($this->input->post('type', true)!=NULL and $this->input->post('type', true)!=''){ // from filter type
	   		$_SESSION['filter_changeunit']['arr_type'] = $this->input->post('type', true);
	   		$_SESSION['filter_changeunit']['type'] = implode(',',$_SESSION['filter_changeunit']['arr_type']);
	   		
	   	}
	   	if ($this->input->post('unitkerja', true)!=NULL and $this->input->post('unitkerja', true)!=''){
	   		$_SESSION['filter_changeunit']['arr_unitkerja'] = $this->input->post('unitkerja', true);
	   		$_SESSION['filter_changeunit']['unitkerja'] = implode(',',$_SESSION['filter_changeunit']['arr_unitkerja']);
	   	}
	   	redirect("web/document/listChangeUnit/$is_show");
    }

    public function removeChangeUnitFilter($method=0){
    	unset($_SESSION['filter_changeunit']);
    	redirect("web/document/listChangeUnit/".$method);
    }

    public function coba_tes(){
    	$tes = $this->Document_model->getDocumentFlow('4082');
    	// echo "<pre>";print_r($tes);echo "</pre>";
    }

    public function inputDocLog($doc_id,$data=0,$data_lama=0 , $status){
    	$this->load->model('Documents_log_model');
    	$this->load->model('Document_model');
    	$this->load->model('Statistic_documents_model');
    	// echo $doc_id;
    	// echo 'doc_id='.$doc_id."<br>";
    	// echo "<pre>";print_r($data);echo "</pre>";
    	// echo "<pre>";print_r($data_lama);echo "</pre>";
    	// echo "ini status: ".$status."<br>";
    	// echo "data sebelum.".$data_sebelum['WORKFLOW_ID']."<br>";
    	// echo Document_model::STATUS_REVISE."<br>";
    	if($status == Document_model::STATUS_REVISE || $data_lama['WORKFLOW_URUTAN'] > $data['WORKFLOW_URUTAN']){
    		$dataUpdate = array(
    			'STATUS' => Documents_log_model::REVISE
    		);
    		dbUpdate('documents_log', $dataUpdate, "document_id ='".$doc_id."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan!=1");
    		dbUpdate('documents_log', array('STATUS' => Documents_log_model::PROGRESS), "document_id ='".$doc_id."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=1");
    	}
    	elseif($status == Document_model::STATUS_REJECT && $data['WORKFLOW_ID'] == 1){
    		// DELETE SEMUA DI DOCUMENT LOG DAN PENYESUAIAN DI STATISTIC_DOCUMENT
    		// Status pasti belum publish, status di statistics document pasti belum L dan masih menghitung tot_doc, kurangin tot_doc dari setiap ketentuan
			// echo "masuk rejectinput<br>";
			$hapusData = $this->Documents_log_model->filter("where document_id ='".$doc_id."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=1 or workflow_urutan=3")->deleteData();
    		if($hapusData){// jika berhasil hapus di documents_log_model, maka -1 setiap proses dari ketentuan
    			// echo "masuk if<br>";
    			$get_unitkerja = $this->getUnitkerja($data_lama['CREATOR_JABATAN_ID']);
    			// echo "unitkerja:".$get_unitkerja."<br>";
    			if($data_lama['WORKFLOW_URUTAN'] == $data['WORKFLOW_URUTAN']){
    				$workflow_urutan = " = 3";
    			}
    			else{
    				$workflow_urutan = " <= 3";
    			}
    			$minus_stat_doc = $this->Statistic_documents_model->filter(" where unitkerja_id='".$get_unitkerja."' and workflow_id=".$data_lama['WORKFLOW_ID']." and doc_type = ".$data_lama['TYPE_ID']." and workflow_urutan ".$workflow_urutan)->updateTotDoc();
    			// echo $minus_stat_doc;
    		}
    	}
    	// elseif($status == Document_model::STATUS_PROSES){
    	// 	echo "ini status proses<br>";
    	// }
    	// elseif()

    	else{
    		// echo "ini status lagi<br>";
    		if($data_lama !=0) {
	    		$dataInsert = array(
	    			'STATUS' => Documents_log_model::DONE
	    		);
	    		$get_data_log = $this->Documents_log_model->filter("where document_id=".$doc_id)->getBy();
	    		// echo "<pre>";print_r($get_data_log);echo "</pre>";
	    		// $data_stat_doc = $this->Statistic_documents_model->filter("where unitkerja_id='".$get_data_log[0]['unitkerja_id']."' and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=".$data_lama['WORKFLOW_URUTAN'])->getAll();
	    		// $dataUpdate = array(
	    		// 	'TOTAL_DOCUMENT' => $data_stat_doc[0]['total_document']-1
	    		// );
	    			// echo "<pre>";print_r($dataInsert);echo "</pre>";
				// $this->Documents_log_model->updateDate('updated_at',"where id='".$get_data_log['id']."'");
				dbUpdate('documents_log', $dataInsert, "document_id =".$doc_id." and workflow_id=".$data_lama['WORKFLOW_ID']." and workflow_urutan=".$data_lama['WORKFLOW_URUTAN']." and user_id=".$data['CREATOR_ID']);
	    			// dbUpdate('Statistic_documents_model', $dataUpdate, "id =".$data_stat_doc[0]['id']);
	    	}
	    	if($data != 0 || $data['WORKFLOW_URUTAN'] != 6){ // insert data baru
	    		// echo "ini status1: ".$status."<br>";
	    		// echo "<pre>";print_r($data);echo "</pre>";
	    		// echo "masuk if workflow urutan<br>";
	    		$this->load->model('Workflow_model');
	    		$this->load->model('Jabatan_model');

	    		$tasks = $this->Workflow_model->getTasks($data['WORKFLOW_ID'], $data['WORKFLOW_URUTAN']);
	    		$get_unitkerja = $this->getUnitkerja($data['CREATOR_JABATAN_ID']);
	    		//ready to insert
	    		$record = array();
	            $record['document_id'] = $doc_id;
	            $record['unitkerja_id'] = $get_unitkerja;
	            $record['workflow_id'] = $data['WORKFLOW_ID'];
	            $record['workflow_urutan'] = $data['WORKFLOW_URUTAN'];
	            $record['actor'] = $tasks['user'];
	            $record['task_name'] = $tasks['name'];
	            $record['status'] = Documents_log_model::PROGRESS;
	            $record['user_id'] = $data['CREATOR_ID'];

	            $last_insert = $this->Documents_log_model->filter("where document_id=".$record['document_id']." and unitkerja_id='".$record['unitkerja_id']."' and actor='".$record['actor']."' and workflow_id=".$record['workflow_id']." and workflow_urutan=".$record['workflow_urutan']." and status='".Documents_log_model::PROGRESS."' or status='".Documents_log_model::REVISE."' and user_id='".$record['user_id']."'")->getOne('id');
	                    // jumlah hari langsung 0 jika tidak ada data
	            if ($last_insert==NULL || $last_insert==false){
	            	// echo "ini status2: ".$status."<br>";
	                $record['jumlah_hari'] = 0;
	                $insert = $this->Documents_log_model->insert($record, FALSE, FALSE);
	                if($insert){ // jika data telah diinsert , maka insert / update ke statistic_documents
	                	$this->load->model('Statistic_documents_model');
	                	$stat_doc_data = $this->Statistic_documents_model->filter("where unitkerja_id = '".$get_unitkerja."' and doc_type='".$data['TYPE_ID']."' and workflow_id=".$data['WORKFLOW_ID']." and workflow_urutan=".$data['WORKFLOW_URUTAN'])->getAll();
	                	// echo "ini stat_doc_data<br>";
	                	// echo "<pre>";print_r($stat_doc_data);echo "</pre>";
	                	if($stat_doc_data){ // jika data telah ada, maka akan diupdate
	                		$stat_doc['total_document'] = $stat_doc_data[0]['total_document'] + 1;
	                		$query = "update statistic_documents set total_document = total_document+1 where id = ".$stat_doc_data[0]['id'];
	                		// echo $query."<br>";
	                		dbQuery($query);
	                		// dbUpdate('Statistic_documents_model', $stat_doc, "id =".$stat_doc_data[0]['id']);
	                	}
	                	else{ // Jika Data Tidak ada di Stat_doc, maka akan insert
	                		$stat_doc = array(
		                		'unitkerja_id'		=> $get_unitkerja,
		                		'workflow_id'		=> $data['WORKFLOW_ID'],
		                		'workflow_urutan'	=> $data['WORKFLOW_URUTAN'],
		                		'jumlah_hari'		=> 0,
		                		'doc_type'			=> $data['TYPE_ID'],
		                		'type'				=> Statistic_documents_model::PENDING_PROCESS,
		                		'actor'				=> $tasks['user'],
		                		'total_document'	=> 1
	                		);
	                		dbInsert('Statistic_documents', $stat_doc);
	                    // echo "insert<br>";
	                	}
	                	
	                }
	            } else { // biasanya karena revise
	            	// echo "ini status3: ".$status."<br>";
	            	// echo "id :".$last_insert."<br>";
	                // kalau ada diupdate
	                // $update = array();
	                // $jumlah = (int)$this->Documents_log_model->filter("where id=$last_insert")->getOne('jumlah_hari');
	                // $jumlah++;
	                $update['status'] = Documents_log_model::PROGRESS;
	                $ganti =$this->Documents_log_model->updateDate('updated_at',"where id=$last_insert");
	                // dbupdate($last_insert, $update);
	                $query = "update documents_log set status='P' where id=".$last_insert;
	                // echo $query;
	                $ganti = dbQuery($query);
	                // dbUpdate('Documents_log',$update," id=".$last_insert);
	                if($ganti){
	                    $stat_doc_data = $this->Statistic_documents_model->filter("where unitkerja_id = '".$get_unitkerja."' and doc_type='".$data['TYPE_ID']."' and workflow_id=".$data['WORKFLOW_ID']." and workflow_urutan=".$data['WORKFLOW_URUTAN'])->getAll();
	                	if(!$stat_doc_data){
	                		$stat_doc = array(
		                		'unitkerja_id'		=> $get_unitkerja,
		                		'workflow_id'		=> $data['WORKFLOW_ID'],
		                		'workflow_urutan'	=> $data['WORKFLOW_URUTAN'],
		                		'jumlah_hari'		=> 0,
		                		'doc_type'			=> $data['TYPE_ID'],
		                		'type'				=> Statistic_documents_model::PENDING_PROCESS,
		                		'actor'				=> $tasks['user'],
		                		'total_document'	=> 1
	                		);
	                		dbInsert('Statistic_documents', $stat_doc);

	                	}
	                }
	                       
	            }
	    	}
	    	if($data['WORKFLOW_URUTAN'] == 6){ // tanda telah publish, maka total_doc - 1 di stat doc
	    		$this->load->model('Statistic_documents_model');
	    		$get_unitkerja = $this->getUnitkerja($data['CREATOR_JABATAN_ID']);
	    		$data_stat = $this->Statistic_documents_model->filter("where unitkerja_id='".$get_unitkerja."' and workflow_id=".$data['WORKFLOW_ID']." and doc_type='".$data['TYPE_ID']."'")->getAll();
	    		foreach ($data_stat as $stat) {
	    			$this->Documents_log_model->updateDate('updated_at',"where id='".$stat['id']."'");
	    			$query = "update statistic_documents set total_document= total_document -1 where id=".$stat['id'];
	    			dbQuery($query); // total doc -1 karena telah publish
	    		}
	    		
	    	}
    	}
    	
    	// echo "baris luar inputdoclog<br>";

    }
    public function inputDocLogBackup($data, $doc_id){
   		// echo "<pre>";print_r($data);echo "</pre>";
    	$dataInsert = array(
    		'DOCUMENT_ID'		=> $doc_id,
    		'WORKFLOW_ID'		=> $data['WORKFLOW_ID'],
    		'WORKFLOW_URUTAN'	=> $data['WORKFLOW_URUTAN'],
    		'JUMLAH_HARI'		=> 0,
    		'CREATED_AT'		=> "'".date("d/m/Y h:i:s")."'" ,
    		'STATUS'			=> 'P'
    	);
    	/////// Get Department ////////////////
    	$this->load->model('Unitkerja_model');
	    $cekLevel = $this->Unitkerja_model->getBy($data['UNITKERJA_ID']);
	    // print_r($cekLevel);
	    if($cekLevel['level'] == 'DEPT'){
	    	$dataInsert['DEPT_ID'] = $data['UNITKERJA_ID'];
	    }
	    elseif($cekLevel['level'] == 'BIRO'){
	    	$getDept = $this->Unitkerja_model->getBy($cekLevel['parent']);
	    	$dataInsert['DEPT_ID'] = $getDept['id'];
	    }
	    elseif($cekLevel['level'] == 'SECT'){
	    	$getBiro = $this->Unitkerja_model->getBy($cekLevel['parent']);
	    	$getDept = $this->Unitkerja_model->getBy($getBiro['parent']);
	    	$dataInsert['DEPT_ID'] = $getDept['id'];	
	    }
	    elseif($cekLevel['level'] == 'GRP'){
	    	$getSect = $this->Unitkerja_model->getBy($cekLevel['parent']);
	    	$getBiro = $this->Unitkerja_model->getBy($getSect['parent']);
	    	$getDept = $this->Unitkerja_model->getBy($getBiro['parent']);
	    	$dataInsert['DEPT_ID'] = $getDept['id'];
	    }

	    //// Get Tasks//////
	    $this->load->model('Workflow_model');
	    $getTasks = $this->Workflow_model->getTasks($data['WORKFLOW_ID'], $data['WORKFLOW_URUTAN']);
	    $dataInsert['TASK_NAME'] = $getTasks['name'];
	    $dataInsert['ACTOR']     = $getTasks['user'];
	    // echo "<pre>";print_r($dataInsert);echo "</pre>";
	    $insert = dbInsert('documents_log', $dataInsert);
	    if($insert){
	    	// echo "a";
	    }
	    else{
	    	// echo "b";
	    }
	    die();
	    // echo "<pre>";print_r($getTasks);echo "</pre>";
	    
	    die();


    }

    function tes(){
    	$this->load->model('Workflow_model');
    	$user_id = $this->Workflow_model->getTasks(1,3);
    	// $data_sebelum = array(
    	// 	'USER_ID'			=> $user_id,
    	// 	'WORKFLOW_ID'		=> $workflow_id,
    	// 	'WORKFLOW_URUTAN'	=> $urutan_now
    	// );
    	// echo "<pre>";print_r($user_id);echo "</pre>";
    	// $a = $this->getActorDept(4080, 1, 2, 'E');
    	// print_r($a);
    	// $data_baru = $this->model->getDocumentFlow(4082);
    	// echo "<pre>";print_r($data_baru);echo "</pre>";
  //   	$this->load->model('Workflow_model');
		// $urutan = $this->Workflow_model->getTasks($workflow['WORKFLOW_ID']);
    }


    

    public function ajaxChangeDocType(){
    	$docType = $this->input->post('docType', true);
    	$jenis_statistik = $this->input->post('jenis_statistik', true);
    	if($jenis_statistik == 'P'){
    		unset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_type']);
        	$_SESSION['dms_semen_indonesia']['statistik']['pending_process_type']=$docType;
    	}
    	else{
    		unset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_type']);
        	$_SESSION['dms_semen_indonesia']['statistik']['lead_time_type']=$docType;
    	}
        
        return $docType;
    }
    public function ajaxChangeDocFlow(){
    	$docFlow = $this->input->post('docFlow', true);
    	$jenis_statistik = $this->input->post('jenis_statistik', true);
    	if($jenis_statistik == 'P'){
    		unset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow']);
        	$_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow']=$docFlow;
    	}
    	else{
    		unset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow']);
        	$_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow']=$docFlow;
    	}
        return $docFlow;
    }

    public function ajaxChangeWorkUnit(){
    	$workUnit = $this->input->post('workUnit', true);
    	$jenis_statistik = $this->input->post('jenis_statistik', true);
    	if($jenis_statistik == 'P'){
    		unset($_SESSION['dms_semen_indonesia']['statistik']['pending_process_flow']);
        	$_SESSION['dms_semen_indonesia']['statistik']['pending_process_workunit']=$workUnit;
    	}
    	else{
    		unset($_SESSION['dms_semen_indonesia']['statistik']['lead_time_flow']);
        	$_SESSION['dms_semen_indonesia']['statistik']['lead_time_workunit']=$workUnit;
    	}
        return $workUnit;
    }

    public function getActorDept($document_id, $workflow_id, $workflow_urutan, $actor=null){
        $this->load->model("Workflow_model");
        $this->load->model("Document_model");
        $this->load->model("Unitkerja_model");

        // get user idnya
        switch ($actor) {
            case Role::DRAFTER:
                $users[] = $this->Document_model->getOne($document_id, 'user_id');
            break;
            case Role::CREATOR:
                $users[] = $this->Document_model->getOne($document_id, 'creator_id');
            break;
            case Role::APPROVER:
                $users[] = $this->Document_model->getOne($document_id, 'approver_id');
            break;
            case Role::REVIEWER:
                $this->load->model('Document_reviewer_model');
                $reviewers = $this->Document_reviewer_model->filter("where workflow_id=$workflow_id and document_id=".$document_id)->getAllFilter();
                $users = Util::toList($reviewers, 'document_reviewer');
            break;
            case Role::DOCUMENT_CONTROLLER:
                $this->load->model('Document_delegates_model');
                $conditions = array(
                    " document_id=$document_id ", 
                    "workflow_id=$workflow_id", 
                    "workflow_urutan=$workflow_urutan"
                );
                $condition = implode(' and ', $conditions);
                $delegated = $this->Document_delegates_model->filter($condition)->getBy();

                $this->load->model('User_model');

                // Kalau pada task ini ga delegasi, pakai kabiro SMSI
                $role = Role::DOCUMENT_CONTROLLER;
                $unitkerja_id = $this->Document_model->getOne($document_id, 'unitkerja_id');
                $company = $this->Unitkerja_model->getOne($unitkerja_id, 'company');
                // $company = str_pad($this->Unitkerja_model->getOne($unitkerja_id, 'company'), 4, '0', STR_PAD_LEFT);
                if (!$delegated){
                    $users[] = $this->User_model->filter("where \"role\"='$role' and \"is_chief\"='X' and \"company\"='$company' ")->getOneFilter('user_id');
                } else {
                    $user_id = $delegated['delegate'];
                    $users[] = $this->User_model->filter(" where \"user_id\"='$user_id' ")->getOneFilter('user_id');
                }
            break;
        }
        // echo "<pre>";print_r($users);echo "</pre>";
        // get departmenetnya
        $this->load->model("User_model");
        $data = array();
        $index = 0;
        foreach ($users as $k_user => $v_user) {
        	// echo "masuk foreach $v_users<br>";
            $unitkerja_id = $this->User_model->filter("where \"user_id\"=$v_user")->getOneFilter('unitkerja_id');
            // echo "unitkerja_id=$unitkerja_id<br>";
            $level = $this->Unitkerja_model->getBy($unitkerja_id);
            // if($level !="BIRO" || $level != "SECT" || $level != "DEPT" || $level != "DIR"){
            	
            // }
            // echo "dept=$dept<br>";
            if ($dept!=NULL){
                $data[$index]['depts'] = $dept;
                $data[$index]['user'] = $v_user;
            }
            else{
                $data[$index]['depts'] = $unitkerja_id;
                $data[$index]['user'] = $v_user;
            }
            $index++;
        }
        return $data;
    }

    public function getUnitKerja($jabatan_id){
    	$this->load->model('Jabatan_model');
    	$this->load->model('Unitkerja_model');
    	$unitkerja_id =$this->Jabatan_model->getOne($jabatan_id, 'unitkerja_id');
    	if($unitkerja_id == null || $unitkerja_id == ''){
    		// echo "aaa";
    		return 0;
    	}
    	else{
    		$d_unitkerja = $this->Unitkerja_model->getBy($unitkerja_id);
	    	if($d_unitkerja['level'] == 'GRP' || $d_unitkerja['level'] == '' || $d_unitkerja['level'] == null){
	    		// get unitkerja minimal sect
	    		$unitkerja = $this->Unitkerja_model->getParent($unitkerja_id, array('SECT','BIRO', 'DEPT'));
	    		if($unitkerja){
	    			return $unitkerja;
	    		}
	    		else{
	    			// echo "bbb";
	    			return 0;
	    		}
	    		// return unitkerja
	    		
	    	}
	    	else{ // jika unitkerja sudah termasuk level yang dbutuhkan (min. sect)
	    		return $unitkerja_id;
	    	}
	    	// echo "<pre>";print_r($level);echo "</pre>";
    	}
    	

    }

    public function cobaUnitKerja(){
    }

    public function list_all(){
    	parent::list_all();
    	$this->data['variables'] = SessionManagerWeb::getVariables();
    	$this->template->viewDefault($this->view, $this->data);
    }
}

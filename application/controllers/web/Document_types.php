<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Document_types extends WEB_Controller {

    protected $title = 'Document Type';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
    	$this->load->model('Document_types_model');
    	$data = $this->model->order('urutan')->getAllJenis();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    function add($name) {
        $name = urldecode($name);
        $this->model->add($name);
        redirect($this->ctl );        
    }

    public function edit($id=NULL){
    	if (isset($id))
            $data = $this->model->getDocumentType($id);
        else
            $data = array();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function delete($id) {
        $delete = $this->model->delete($id);

        if ($delete === true) {
            $ok = true;
            $msg = 'Successfully deleted the document type';
        } else {
            $ok = false;
            $msg = 'Failed to delete document type';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl);
    }
    
    public function update($id = null) {
        $data = $this->postData;
        $update = $this->model->save($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Successfully changed Document Type';

        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change Document Type';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl );
    }

    public function ajaxGetDetail(){
        $id = $this->input->post('id', true);
        $data = $this->model->get($id);
        echo json_encode($data);

    }

    public function ajaxSetUrutan(){
        $type_id = $this->input->post('id', true);
        $urutan = $this->input->post('urutan', true);
        $urutan_exist = $this->model->filter(" urutan=$urutan ")->getOne('id');
        if ($urutan_exist!=NULL){
            $urutan_now = $this->model->filter(" id=$type_id ")->getOne('urutan');
            $data = array();
            $data['urutan'] = $urutan_now;
            $this->model->updateJenis($urutan_exist, $data);
        }
        $data = array();
        $data['urutan'] = $urutan;
        $this->model->updateJenis($type_id, $data);
    }
}

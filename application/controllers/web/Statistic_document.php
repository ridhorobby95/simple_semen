<?php

# untuk keperluan public tanpa membutuhkan login

class Statistic_document extends CI_Controller
{
    private $uri_segments = array();
    
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        die();
    }


    // BUG => Kalo dept sama, harinya nambah 2 hari, harusnya di filter dept mana aja, baru di tambahn harinya
    public function generatePendingProcessBackup(){
        // Load Model
        $this->load->model("Document_model");
        $this->load->model("Workflow_model");
        $this->load->model("Statistic_documents_model", "model");
        $this->load->model("Documents_log_model");

        // Masukkin ke Document_log
        // Select document pengajuan dan revisi yang belum selesai
        $data = $this->Document_model->column("id as \"id\", workflow_id as \"workflow_id\", workflow_urutan as \"workflow_urutan\" , user_id as \"user_id\" ")
                                    ->filter("where is_delete!=1 and is_obsolete!=1 and status!='D' and workflow_id in ('".Workflow_model::PENGAJUAN."','".Workflow_model::REVISI."','".Workflow_model::EVALUASI."') ")
                                    ->getAll();
        // get departmentnya 
        $list_depts = array();
        // get usernya
        $list_users = array();
        //var grand_rata2
        $rata2 = array();
        //index untuk rata2 
        $index = 0;
        // var untuk insert ke statistic_documents
        $record = array();       
        foreach ($data as $k_data => $v_data) {
            // get actor dan task dari workflow (namanya)
            $workflow = $this->Workflow_model->getTasks($v_data['workflow_id']);
            $actors = array();
            $tasks = array();
            foreach ($workflow as $k_workflow => $v_workflow) {
                if ($v_workflow['urutan']==$v_data['workflow_urutan']){
                    $actors[] = $v_workflow['user'];
                    $tasks[] = $v_workflow['name'];
                }
            }
            // echo "actors<br>";
            // echo "<pre>";print_r($actors);echo "</pre>";
            // echo "v_data<br>";
            // echo "<pre>";print_r($v_data);echo "</pre>";
            foreach ($actors as $k_actor => $v_actor) { 
                // var untuk grand total data
                $grand_total = 0;
                $user_departments = $this->getActorDept($v_data['id'], $v_data['workflow_id'], $v_data['workflow_urutan'] ,$v_actor);
                // echo "user_departments<br>";
                // echo "<pre>";print_r($user_departments);echo "</pre>";
                foreach ($user_departments as $k_department => $v_department) {
                    // cek apakah user pada workflow id dan urutan itu ada
                    if (in_array($v_department['user'], $list_users[$v_data['id']])){
                        // kalau ada brarti tidak perlu dihitung lagi
                        continue;
                    }

                     // masukin user ke array
                    $list_users[$v_data['id']][] = $v_department['user']; 

                    // Ready to insert
                    $record['document_id'] = $v_data['id'];
                    $record['dept_id'] = $v_department['depts'];
                    $record['workflow_id'] = $v_data['workflow_id'];
                    $record['workflow_urutan'] = $v_data['workflow_urutan'];
                    $record['actor'] = $v_actor;
                    $record['task_name'] = $tasks[$k_actor];
                    $record['status'] = Documents_log_model::PROGRESS;
                    $record['user_id'] = $v_department['user'];
                    
                    // hitung hari
                    $jumlah = 0;
                    // get last insert dari id document dan rolenya beda, dan dept nya beda dan yang masih progress
                    $last_insert = $this->Documents_log_model->show_sql(false)->filter("where document_id=".$record['document_id']." and dept_id='".$v_department['depts']."' and actor='".$v_actor."' and workflow_id=".$v_data['workflow_id']." and workflow_urutan=".$v_data['workflow_urutan']." and status='".Documents_log_model::PROGRESS."' and user_id='".$v_department['user']."'")->getOne('id');
                    
                    // jumlah hari langsung 0 jika tidak ada data
                    if ($last_insert==NULL || $last_insert==false){
                        $record['jumlah_hari'] = $jumlah;
                        $this->Documents_log_model->insert($record, FALSE, FALSE);
                    } else {
                        // kalau ada diupdate
                        $update = array();
                        $jumlah = (int)$this->Documents_log_model->filter("where id=$last_insert")->getOne('jumlah_hari');
                        $jumlah++;
                        $update['jumlah_hari'] = $jumlah;
                        $this->Documents_log_model->updateDate('updated_at',"where id=$last_insert")->update($last_insert, $update);
                    }

                    /* === Masukking ke Statistics_document === */

                    // get type document
                    $doc_type_id = $this->Document_model->getOne($record['document_id'], 'type_id');

                    // get id Statistics_document
                    $stat_doc_id = $this->model->filter("where dept_id='".$record['dept_id']."' and workflow_id=".$record['workflow_id']." and workflow_urutan=".$record['workflow_urutan']." and doc_type='".$doc_type_id."' and type='".Statistic_documents_model::PENDING_PROCESS."'
                                                        ")
                                                ->getOne('id');
                    $stat_doc = array();
                    // tambahin jumlah dokumen
                    $doc_ids = $this->Documents_log_model->show_sql(false)->column(array("document_id"=>"document_id"))->filter("where dept_id='".$v_department['depts']."' and workflow_id=".$v_data['workflow_id']." and workflow_urutan=".$v_data['workflow_urutan']." and status='".Documents_log_model::PROGRESS."'")->group_by("group by document_id")->getAll();
                    $this->Documents_log_model->group_by();
                    $stat_doc['total_document'] = count($doc_ids);

                    if ($stat_doc_id==NULL || $stat_doc_id==false){
                        // insert
                        $stat_doc['dept_id'] = $record['dept_id'];
                        $stat_doc['workflow_id'] = $record['workflow_id'];
                        $stat_doc['workflow_urutan'] = $record['workflow_urutan'];
                        $stat_doc['jumlah_hari'] = 0;
                        $stat_doc['actor'] = $v_actor;
                        $stat_doc['type'] = Statistic_documents_model::PENDING_PROCESS;
                        $stat_doc['doc_type'] = $doc_type_id;
                        $this->model->insert($stat_doc);
                    } else {
                        // update
                        
                        // hitung rata2 hari
                        $logs = $this->Documents_log_model->show_sql()->column(array(
        "id" => "\"id\"",
        "document_id" => "\"document_id\"",
        "dept_id" => "\"dept_id\"",
        "workflow_id" => "\"workflow_id\"", 
        "workflow_urutan" => "\"workflow_urutan\"" ,
        "jumlah_hari" => "\"jumlah_hari\"",
        "task_name" => "\"task_name\"",
        "actor" => "\"actor\"",
        "status" => "\"status\"",
        "created_at" => "\"created_at\""
    ))->filter("where status='".Documents_log_model::PROGRESS."' and dept_id='".$record['dept_id']."' and workflow_id=".$record['workflow_id']." and workflow_urutan=".$record['workflow_urutan']."")
                                                            ->getAll();
                        $total_data = 0;// total data per dept_id,w_id,w_u, doc_id
                        $total_days = 0;// total hari per dept_id,w_id,w_u, doc_id
                        $longest_day = 0; // hari terlama per dept_id,w_id,w_u, doc_id
                        // echo "logs<br>";
                        // echo "<pre>";print_r($logs);echo "</pre>";
                        foreach ($logs as $v_log) {
                            $log_doc_type = $this->Document_model->getOne($v_log['document_id'], 'type_id');
                            if ($log_doc_type==$doc_type_id){
                                // untuk rata2
                                $total_days += (int)$v_log['jumlah_hari']; 
                                $total_data++;
                                $grand_total++;
                                if(!isset($rata2[$record['dept_id']])){
                                    $rata2[$record['dept_id']]['dept_id'] = $record['dept_id'];
                                   
                                }
                                if(isset($rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']])){
                                }
                                else{
                                    // $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['total_data'] = $grand_total;
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['total_document'] = $stat_doc['total_document'];
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['dept_id'] = $record['dept_id'];
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['workflow_id'] = $record['workflow_id'];
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['workflow_urutan'] = $record['workflow_urutan'];
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['jumlah_hari'] = 0;
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['actor'] = $v_actor;
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['type'] = Statistic_documents_model::PENDING_PROCESS;
                                    $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['doc_type'] = $doc_type_id;
                                }
                                $rata2[$record['dept_id']][$record['workflow_id']][$record['workflow_urutan']]['data_dokumen'][$v_log['document_id']] =floor($total_days/$total_data);
                                // untuk yang terlama
                                // if ($longest_day<$v_log['jumlah_hari']){
                                //     $longest_day = $v_log['jumlah_hari'];
                                // }
                            }
                        }
                        
                        
                       
                        // $stat_doc['jumlah_hari'] = floor($total_days/$total_data);
                        // terlama
                        // $stat_doc['jumlah_hari'] = $longest_day;
                        // echo "longest_days =".$longest_day."<br>";
                        // die();
                        // $this->model->updateDate('last_updated',"where id=$stat_doc_id")->update($stat_doc_id, $stat_doc);
                        $index++;
                         // echo "<pre>";print_r($rata2);echo "</pre>";
                    }
                }
                //
                
                // echo "ending dari per dept<br>";
                // echo "<pre>";print_r($rata2);echo "</pre>";
                // $stat_doc['jumlah_hari'] = 0;
                // foreach ($rata2 as $rata) {
                //   if()  
                // }
                // foreach ($rata2 as $rata) {
                //     foreach ($rata as $key => $value) {
                //         # code...
                //     }
                // }

            }
        }
        //MANIPULATE
        foreach ($rata2 as $rata) {
            // echo "<pre>";print_r($rata);echo "</pre>";
            for ($i=1; $i <=5 ; $i++) {
                if(isset($rata[$i])){
                    // echo "<pre>";print_r($rata[$i]);echo "</pre>";
                    for ($j=1; $j <=6 ; $j++) {
                        if(isset($rata[$i][$j])){
                            $total = 0;
                            // echo "<pre>";print_r($rata[$i][$j]);echo "</pre>";
                            foreach ($rata[$i][$j]['data_dokumen'] as $dokumen) {
                                $total += $dokumen;
                                // echo "<pre>";print_r($dokumen);echo "</pre>";
                            }

                            $rata2[$rata['dept_id']][$i][$j]['jumlah_hari'] = $total /$rata[$i][$j]['total_document'];
                        }
                       
                    }
                }
            }
        }

        foreach ($rata2 as $rata) {
            // echo "<pre>";print_r($rata);echo "</pre>";
            for ($i=1; $i <=5 ; $i++) {
                if(isset($rata[$i])){
                    // echo "<pre>";print_r($rata[$i]);echo "</pre>";
                    for ($j=1; $j <=6 ; $j++) {
                        if(isset($rata[$i][$j])){
                            // echo "<pre>";print_r($rata[$i][$j]);echo "</pre>";
                            $stat_doc_id = $this->model->filter("where dept_id='".$rata[$i][$j]['dept_id']."' and workflow_id=".$rata[$i][$j]['workflow_id']." and workflow_urutan=".$rata[$i][$j]['workflow_urutan']." and doc_type='".$rata[$i][$j]['doc_type']."' and type='".Statistic_documents_model::PENDING_PROCESS."'
                                                        ")
                                                ->getOne('id');
                            $stat_doc = array(
                                'dept_id'           => $rata[$i][$j]['dept_id'],
                                'workflow_id'       => $rata[$i][$j]['workflow_id'],
                                'workflow_urutan'   => $rata[$i][$j]['workflow_urutan'],
                                'jumlah_hari'       => $rata[$i][$j]['jumlah_hari'],
                                'doc_type'          => $rata[$i][$j]['doc_type'],
                                'type'              => Statistic_documents_model::PENDING_PROCESS,
                                'actor'             => $rata[$i][$j]['actor'],
                                'total_document'    => $rata[$i][$j]['total_document']
                            );
                            $this->model->updateDate('last_updated',"where id=$stat_doc_id")->update($stat_doc_id, $stat_doc);
                        }
                       
                    }
                }
            }
        }
        
        echo "<pre>";print_r($rata2);echo "</pre>";
        echo '<br>Success!';
    }

    // BELUM OPTIMISE
    public function generateLeadTimeBackup(){
        // Load Model
        $this->load->model("Documents_log_model");
        $this->load->model("Document_model");
        $this->load->model("Statistic_documents_model", "model");

        $logs = $this->Documents_log_model->filter("where status='".Documents_log_model::DONE."'")->getAll();
        // check dept apakah sudah diupdate
        $check_depts = array();
        foreach ($logs as $v_log) {
            // kalau sebelumnya udh di masukan ga usah di masukkan lagi
            if (in_array($v_log['dept_id'], $check_depts)){
                continue;
            }

            // kalau status dokumen belum selesai, tidak usah dimasukkan
            $status_document = $this->Document_model->getOne($v_log['document_id'], 'status');
            if (!in_array($status_document, array(Document_model::STATUS_SELESAI, Document_model::STATUS_REJECT))){
                continue;
            }
            
            $check_depts[] = $v_log['dept_id'];

            // get type document
            $doc_type_id = $this->Document_model->getOne($v_log['document_id'], 'type_id');

            // get id Statistics_document
            $stat_doc_id = $this->model->filter("where dept_id='".$v_log['dept_id']."' and workflow_id=".$v_log['workflow_id']." and workflow_urutan=".$v_log['workflow_urutan']." and doc_type='".$doc_type_id."' and type='".Statistic_documents_model::LEAD_TIME_PUBLISH."'
                                                ")
                                        ->getOne('id');
            $stat_doc = array();
            if ($stat_doc_id==NULL || $stat_doc_id==false){
                // insert
                $stat_doc['dept_id'] = $v_log['dept_id'];
                $stat_doc['workflow_id'] = $v_log['workflow_id'];
                $stat_doc['workflow_urutan'] = $v_log['workflow_urutan'];
                $stat_doc['jumlah_hari'] = $v_log['jumlah_hari'];
                $stat_doc['actor'] = $v_log['actor'];
                $stat_doc['type'] = Statistic_documents_model::LEAD_TIME_PUBLISH;
                $stat_doc['doc_type'] = $doc_type_id;
                $this->model->insert($stat_doc);
            } else {
                // hitung rata2 hari
                $dept_logs = $this->Documents_log_model
                                ->filter("where status='".Documents_log_model::DONE."' 
                                        and dept_id='".$v_log['dept_id']."' and workflow_id=".$v_log['workflow_id']." 
                                        and workflow_urutan=".$v_log['workflow_urutan']." ")
                                ->getAll();
                $total_data = 0;
                $total_days = 0;
                $longest_day = 0;
                foreach ($dept_logs as $v_dept_log) {
                    $log_doc_type = $this->Document_model->getOne($v_dept_log['document_id'], 'type_id');
                    if ($log_doc_type==$doc_type_id){
                        // untuk rata2
                        $total_days += (int)$v_dept_log['jumlah_hari'];
                        $total_data++;
                        // untuk yang terlama
                        if ($longest_day<$v_dept_log['jumlah_hari']){
                            $longest_day = $v_dept_log['jumlah_hari'];
                        }
                    }
                }
                // rata2
                $stat_doc['jumlah_hari'] = floor($total_days/$total_data);
                // terlama
                // $stat_doc['jumlah_hari'] = $longest_day;

                $this->model->updateDate('last_updated',"where id=$stat_doc_id")->update($stat_doc_id, $stat_doc);
            }
        }
        echo 'Success!';

    }

    public function getActorDept($document_id, $workflow_id, $workflow_urutan, $actor=null){
        $this->load->model("Workflow_model");
        $this->load->model("Document_model");
        $this->load->model("Unitkerja_model");

        // get user idnya
        switch ($actor) {
            case Role::DRAFTER:
                $users[] = $this->Document_model->getOne($document_id, 'user_id');
            break;
            case Role::CREATOR:
                $users[] = $this->Document_model->getOne($document_id, 'creator_id');
            break;
            case Role::APPROVER:
                $users[] = $this->Document_model->getOne($document_id, 'approver_id');
            break;
            case Role::REVIEWER:
                $this->load->model('Document_reviewer_model');
                $reviewers = $this->Document_reviewer_model->filter("where workflow_id=$workflow_id and document_id=".$document_id)->getAllFilter();
                $users = Util::toList($reviewers, 'document_reviewer');
            break;
            case Role::DOCUMENT_CONTROLLER:
                $this->load->model('Document_delegates_model');
                $conditions = array(
                    " document_id=$document_id ", 
                    "workflow_id=$workflow_id", 
                    "workflow_urutan=$workflow_urutan"
                );
                $condition = implode(' and ', $conditions);
                $delegated = $this->Document_delegates_model->filter($condition)->getBy();

                $this->load->model('User_model');

                // Kalau pada task ini ga delegasi, pakai kabiro SMSI
                $role = Role::DOCUMENT_CONTROLLER;
                $unitkerja_id = $this->Document_model->getOne($document_id, 'unitkerja_id');
                $company = $this->Unitkerja_model->getOne($unitkerja_id, 'company');
                // $company = str_pad($this->Unitkerja_model->getOne($unitkerja_id, 'company'), 4, '0', STR_PAD_LEFT);
                if (!$delegated){
                    $users[] = $this->User_model->filter("where \"role\"='$role' and \"is_chief\"='X' and \"company\"='$company' ")->getOneFilter('user_id');
                } else {
                    $user_id = $delegated['delegate'];
                    $users[] = $this->User_model->filter(" where \"user_id\"='$user_id' ")->getOneFilter('user_id');
                }
            break;
        }

        // get departmenetnya
        $this->load->model("User_model");
        $data = array();
        $index = 0;
        foreach ($users as $k_user => $v_user) {
            $unitkerja_id = $this->User_model->filter("where \"user_id\"=$v_user")->getOneFilter('unitkerja_id');
            $dept = $this->Unitkerja_model->getDept($unitkerja_id);
            if ($dept!=NULL){
                $data[$index]['depts'] = $dept;
                $data[$index]['user'] = $v_user;
            }
            $index++;
        }
        return $data;
    }

    public function generatePendingProcessBackup1(){
        // Load Model
        $this->load->model("Document_model");
        $this->load->model("Workflow_model");
        $this->load->model("Statistic_documents_model", "model");
        $this->load->model("Documents_log_model");

        // Masukkin ke Document_log
        // Select document pengajuan dan revisi yang belum selesai
        $data = $this->Document_model->column("id as \"id\", workflow_id as \"workflow_id\", workflow_urutan as \"workflow_urutan\" , user_id as \"user_id\", creator_jabatan_id as \"creator_jabatan_id\", creator_id as \"creator_id\"")
                                    ->filter("where is_delete!=1 and is_obsolete!=1 and status!='D' and workflow_id in ('".Workflow_model::PENGAJUAN."','".Workflow_model::REVISI."','".Workflow_model::EVALUASI."') ")
                                    ->getAll();
        echo "<pre>";print_r($data);echo "</pre>";
        foreach ($data as $k_data => $v_data) {
            // echo "<pre>";print_r($v_data);echo "</pre>";
             // get actor dan task dari workflow (namanya)
            $workflow = $this->Workflow_model->getTasks($v_data['workflow_id'],$v_data['workflow_urutan']);
            $get_unitkerja = $this->getUnitkerja($v_data['creator_jabatan_id']);
            echo "creator jabatan_id = ".$v_data['creator_jabatan_id']." || dan unitkerjanya: ".$get_unitkerja."<br>";
            // echo "<pre>";print_r($get_unitkerja);echo "</pre>";
            // Ready to insert
            $record['document_id'] = $v_data['id'];
            $record['unitkerja_id'] = $get_unitkerja;
            $record['workflow_id'] = $v_data['workflow_id'];
            $record['workflow_urutan'] = $v_data['workflow_urutan'];
            $record['actor'] = $workflow['user'];
            $record['task_name'] = $workflow['name'];
            $record['status'] = Documents_log_model::PROGRESS;
            $record['user_id'] = $v_data['creator_id'];
            // echo "atas<br>";
            // echo "<pre>";print_r($record);echo "</pre>";
            $last_insert = $this->Documents_log_model->show_sql(false)->filter("where document_id=".$record['document_id']." and unitkerja_id='".$record['unitkerja_id']."' and actor='".$record['actor']."' and workflow_id=".$record['workflow_id']." and workflow_urutan=".$record['workflow_urutan']." and status='".Documents_log_model::PROGRESS."' and user_id='".$record['user_id']."'")->getOne('id');
                    // jumlah hari langsung 0 jika tidak ada data
            if ($last_insert==NULL || $last_insert==false){
                echo "insert documents_log<br>";
                $record['jumlah_hari'] = 0;
                $insert = $this->Documents_log_model->insert($record, FALSE, FALSE);
                if($insert){
                    echo "insert<br>";
                }
            } else {
                // kalau ada diupdate
                echo "update documents_log<br>";
                $update = array();
                $jumlah = (int)$this->Documents_log_model->filter("where id=$last_insert")->getOne('jumlah_hari');
                $jumlah++;
                $update['jumlah_hari'] = $jumlah;
                $ganti =$this->Documents_log_model->updateDate('created_at',"where id=$last_insert")->update($last_insert, $update);
                if($ganti){
                    echo "update<br>";
                }
                       
            }

            // /* === Masukking ke Statistics_document === */

            // get type document
            $doc_type_id = $this->Document_model->getOne($record['document_id'], 'type_id');

            // get id Statistics_document
            $stat_doc_id = $this->model->filter("where unitkerja_id='".$record['unitkerja_id']."' and workflow_id=".$record['workflow_id']." and workflow_urutan=".$record['workflow_urutan']." and doc_type='".$doc_type_id."' and type='".Statistic_documents_model::PENDING_PROCESS."'
                                                ")
                                        ->getOne('id');
            $stat_doc = array();
            // tambahin jumlah dokumen
            $doc_id = $this->Documents_log_model->show_sql(false)->filter("where l.workflow_id=".$i." and l.workflow_urutan=".$j." and l.unitkerja_id=".$data['unitkerja_id']." and d.type_id='".$getDoc['TYPE_ID']."' and l.status='P' ")->joinDocumentsDetail();
            // $doc_id = $this->Documents_log_model->show_sql(true)->column(array("document_id"=>"document_id"))->filter("where unitkerja_id='".$record['unitkerja_id']."' and workflow_id=".$v_data['workflow_id']." and workflow_urutan=".$v_data['workflow_urutan']." and status='".Documents_log_model::PROGRESS."'")->group_by("group by document_id")->getAll();
            $reset = $this->Documents_log_model->column()->filter()->group_by()->getAll();
            // $this->Documents_log_model->group_by();
            $stat_doc['total_document'] = count($doc_id);
            echo "stat_doc_id:".$stat_doc_id."<br>";
            if ($stat_doc_id==NULL || $stat_doc_id==false){
                echo "tidak ada, akan insert<br>";
                // // insert
                $stat_doc['unitkerja_id'] = $record['unitkerja_id'];
                $stat_doc['workflow_id'] = $record['workflow_id'];
                $stat_doc['workflow_urutan'] = $record['workflow_urutan'];
                $stat_doc['jumlah_hari'] = 0;
                $stat_doc['actor'] = $workflow['user'];
                $stat_doc['type'] = Statistic_documents_model::PENDING_PROCESS;
                $stat_doc['doc_type'] = $doc_type_id;
                echo "<pre>";print_r($stat_doc);echo "</pre>";
                // echo "--------------------------------------------<br>";
                $this->model->insert($stat_doc);
            }
            else{
                echo "ada, akan update<br>";
                echo "<pre>";print_r($stat_doc);echo "</pre>";
                dbUpdate('Statistic_documents', $stat_doc, "id='".$stat_doc_id."'");
            }

        }
        
        echo '<br>Success!';
        // $this->generateLeadTime();
    }

    public function getUnitkerja($jabatan_id){
        $this->load->model('Jabatan_model');
        $this->load->model('Unitkerja_model');
        $unitkerja_id =$this->Jabatan_model->getOne($jabatan_id, 'unitkerja_id');
        if($unitkerja_id == null || $unitkerja_id == ''){
            return '';
        }
        else{
            $d_unitkerja = $this->Unitkerja_model->getBy($unitkerja_id);
            if($d_unitkerja['level'] == 'GRP' || $d_unitkerja['level']){
                // get unitkerja minimal sect
                $unitkerja = $this->Unitkerja_model->getParent($unitkerja_id, array('SECT','BIRO', 'DEPT'));
                if($unitkerja){
                    return $unitkerja;
                }
                else{
                    return 0;
                }
                // return unitkerja
                
            }
            else{ // jika unitkerja sudah termasuk level yang dbutuhkan (min. sect)
                return $unitkerja_id;
            }
            // echo "<pre>";print_r($level);echo "</pre>";
        }
    }

    public function generateLeadTime(){
        // Load Model
        $this->load->model("Documents_log_model");
        $this->load->model("Document_model");
        $this->load->model("Workflow_model");
        $this->load->model("Statistic_documents_model", "model");
       // Select document pengajuan dan revisi yang belum selesai
        // $dataDoc = $this->Document_model->column("id as \"id\", workflow_id as \"workflow_id\", workflow_urutan as \"workflow_urutan\" , user_id as \"user_id\", type_id as \"type_id\" ")
        //                             ->filter("where is_delete!=1 and is_obsolete!=1 and status='D' and workflow_id in ('".Workflow_model::PENGAJUAN."','".Workflow_model::REVISI."','".Workflow_model::EVALUASI."') ")
        //                             ->getAll();
        $dataDoc = $this->Documents_log_model->filter("where workflow_urutan=5 and status='D'")->getAll();
        echo "<pre>";print_r($dataDoc);echo "</pre>";
        echo "----------------------------------------------<br>";
        $rata2 = array();
        $jumlahData = count($dataDoc);
        $counter = 0;
        foreach ($dataDoc as $data) {
            $counter++;
            // echo $data['jumlah_hari']."<br>";
            // $get_workflow_id = 
            $getDoc = $this->Document_model->getBy($data['document_id']);
            echo "<pre>";print_r($getDoc);echo "</pre>";
            for ($i=1; $i <= 3; $i++) { // workflow_id
                $cekPublish = $this->Documents_log_model->show_sql(false)->filter("where workflow_id=".$i." and workflow_urutan=5 and status='D'")->getAll();
                if($cekPublish){
                     for ($j=1; $j <=5 ; $j++) { // workflow_urutan
                        $true = false;
                        if($i==2 && $j==1){
                            $true = true;
                        }
                        $getDocLog = $this->Documents_log_model->show_sql(false)->filter("where document_id=".$data['document_id']." and workflow_id=".$i." and workflow_urutan=".$j." and status='".Documents_log_model::DONE."'")->getAll();
                        echo "getDocLog:".$getDocLog."<br>";
                        echo "<pre>";print_r($getDocLog);echo "</pre>";
                        // $record = $getDocLog[0];
                        // echo "<pre>";print_r($getDocLog);echo "</pre>";
                        if($getDocLog){
                            if(!$rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]){
                                $getActor = $this->Workflow_model->getTasks($i, $j);
                                // $get_total_doc_p = $this->Documents_log_model->show_sql(false)->filter("where l.workflow_id=".$i." and l.workflow_urutan=".$j." and l.unitkerja_id=".$data['unitkerja_id']." and d.type_id='".$getDoc['TYPE_ID']."' and l.status='P' ")->joinDocumentsDetail();
                                // if($get_total_doc_p){
                                //     $total_doc = count($get_total_doc_p);
                                // }
                                // else{
                                //     $total_doc = 0;
                                // }
                                // echo "<pre>";print_r($get_total_doc_p);echo "</pre>";
                                // echo "count: ".count($get_total_doc_p)."<br>";
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['unitkerja_id'] = $data['unitkerja_id'];
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['workflow_id'] = $i;
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['workflow_urutan'] = $j;
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari'] = $getDocLog[0]['jumlah_hari'];
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['total_data_jumlah_hari'] = 1;
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['doc_type'] = $getDoc['TYPE_ID'];
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['type'] = Statistic_documents_model::PENDING_PROCESS;
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['actor'] = $getActor['user'];
                                // $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['total_document'] = $total_doc;
                            }
                            else{
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari']+$getDocLog[0]['jumlah_hari'];
                                $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['total_data_jumlah_hari'] += 1;
                            }
                        }
                        // if($counter == $jumlahData){
                        //     if($rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari'] != 0){
                        //         $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari'] = floor($rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari']/$rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['total_data_jumlah_hari']);
                        //     }
                        // }
                    }
                }
               
            }
            // echo "<pre>";print_r($rata2);echo "</pre>";

            // echo "jumlah Counter sekarang: ".$counter." dan jumlah total data:".$jumlahData." saat ini<br>";
            // if($counter == $jumlahData){
            //     echo "<pre>";print_r($rata2);echo "</pre>";
                // for ($i=1; $i <=3 ; $i++) { 
                //     for ($j=1; $j <=5 ; $j++) { 
                //         
                //         // proses insert / proses update
                //         // echo "CEKKKKK<br>";
                //         $stat_doc_id = $this->model->filter("where unitkerja_id='".$data['unitkerja_id']."' and workflow_id=".$i." and workflow_urutan=".$j." and doc_type='".$getDoc['TYPE_ID']."' and type='".Statistic_documents_model::PENDING_PROCESS."'
                //                                         ")->show_sql(false)
                //                                 ->getAll();
                //         $reset = $this->model->filter()->getOne();
                //         echo "data yang disearch :";
                //         echo "<pre>";print_r($stat_doc_id);echo "</pre>";
                //         if($stat_doc_id == false || $stat_doc_id == null){
                //             echo "data tidak ada<br>";
                //              // echo "<pre>";print_r($stat_doc);echo "</pre>";
                //             $stat_doc['unitkerja_id'] =  $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['unitkerja_id'];
                //             $stat_doc['workflow_id'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['workflow_id'];
                //             $stat_doc['workflow_urutan'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['workflow_urutan'];
                //             $stat_doc['jumlah_hari'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari'];
                //             $stat_doc['doc_type'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['doc_type'];
                //             $stat_doc['type'] = Statistic_documents_model::PENDING_PROCESS;
                //             $stat_doc['actor'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['actor'];
                //             $stat_doc['total_document'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['total_document'];
                //             echo "<pre>";print_r($stat_doc);echo "</pre>";
                //             // $this->model->insert($stat_doc);
                //         }
                //         else{
                //             echo "data ada<br>";
                //             // echo "<pre>";print_r($stat_doc);echo "</pre>";
                //             $stat_doc['jumlah_hari'] =  $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['jumlah_hari'];
                //             $stat_doc['total_document'] = $rata2[$data['unitkerja_id']][$getDoc['TYPE_ID']][$i][$j]['total_document'];
                //             echo "<pre>";print_r($stat_doc);echo "</pre>";
                //             // $this->model->updateDate('last_updated',"where id=$stat_doc_id")->update($stat_doc_id, $stat_doc);
                //         }
                //     }
                //     echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx<br>";
                // }
                
                
                // echo "<pre>";print_r($rata2[$record['unitkerja_id']][$data['type_id']][$i][$j]);echo "</pre>";
            // }
            
            // foreach ($getDocLog as $docLog) {

            //     if($docLog['workflow_id'] == 1){
            //         for ($i=1; $i <= 5 ; $i++) { 
                        
            //         }
            //     }
            // }
            // echo "<pre>";print_r($getDocLog);echo "</pre>";
        }
        echo "<pre>";print_r($rata2);echo "</pre>";
        foreach ($rata2 as $rata) {
            for ($i=1; $i <=5 ; $i++) {  // doc_type
                for ($j=1; $j <=3 ; $j++) { // workflow_id
                    for ($k=1; $k <=5 ; $k++) { // workflow_urutan
                        $stat_doc = array();
                        $stat_doc_id = $this->model->filter("where unitkerja_id='".$rata[$i][$j][$k]['unitkerja_id']."' and workflow_id=".$j." and workflow_urutan=".$k." and doc_type='".$i."'")->show_sql(false)->getOne('id');
                        $reset = $this->model->filter()->getOne();
                        echo "unitkerja_id:".$rata[$i][$j][$k]['unitkerja_id']." || doc_type=".$i." || w_id=".$j." || w_urutan=".$k."<br>";
                        echo "<pre>";print_r($stat_doc_id);echo "</pre>";
                        if($stat_doc_id == false || $stat_doc_id == null){
                            if($rata[$i][$j][$k]['unitkerja_id']){
                                echo "insert<br>";

                                $stat_doc['unitkerja_id'] =  $rata[$i][$j][$k]['unitkerja_id'];
                                $stat_doc['workflow_id'] = $j;
                                $stat_doc['workflow_urutan'] = $k;
                                $stat_doc['jumlah_hari'] = floor($rata[$i][$j][$k]['jumlah_hari'] / $rata[$i][$j][$k]['total_data_jumlah_hari'] );
                                $stat_doc['doc_type'] = $i;
                                $stat_doc['type'] = Statistic_documents_model::LEAD_TIME_PUBLISH;
                                $stat_doc['actor'] = $rata[$i][$j][$k]['actor'];
                                // $stat_doc['total_document'] = $rata[$i][$j][$k]['total_document'];
                                echo "<pre>";print_r($stat_doc);echo "</pre>";
                                $this->model->insert($stat_doc);
                            }
                            else{
                                echo "data dengan ketentuan memang tidak ada<br>";
                            }
                            
                        }
                        else{
                            echo "update<br>";
                            $stat_doc['jumlah_hari'] =  floor($rata[$i][$j][$k]['jumlah_hari'] / $rata[$i][$j][$k]['total_data_jumlah_hari'] );
                            // // $stat_doc['total_document'] = $rata[$i][$j][$k]['total_document'];
                            $stat_doc['type'] = Statistic_documents_model::LEAD_TIME_PUBLISH;
                            // echo "<pre>";print_r($stat_doc);echo "</pre>";
                            $this->model->updateDate('last_updated',"where id=$stat_doc_id")->update($stat_doc_id, $stat_doc);
                        }
                    }
                }
            }
             
        }
       
        


    }

    function hapusDuplikat(){
        $this->load->model('Documents_log_model');
        $data = $this->Documents_log_model->getAll();
        echo "<pre>";print_r($data);echo "</pre>";
        foreach ($data as $d) {
            $cek = $this->Documents_log_model->show_sql(false)->filter("where document_id='".$d['document_id']."' and unitkerja_id='".$d['unitkerja_id']."' and workflow_id='".$d['workflow_id']."' and workflow_urutan=".$d['workflow_urutan']." and unitkerja_id=".$d['unitkerja_id']." and status='".$d['status']."' and user_id='".$d['user_id']."' ")->getAll();
            $jumlah = count($cek);
            if($jumlah > 1){
                echo "aaaaaaaaaa<br>";
                $idterbesar = 0;
                $counter = 0;
                foreach ($cek as $c) {
                    $counter++;
                    if($c['id'] > $idterbesar){
                        $idterbesar = $c['id'];
                    }
                    if($counter == $jumlah){
                        echo "masuk delete<br>";
                        $sql_hapus = "delete from documents_log where id=".$idterbesar;
                        echo $sql_hapus."<br>";
                        dbQuery($sql_hapus);
                    }
                    echo "idterbesar:".$idterbesar."<br>";

                }
            }
        }
    }

    public function tambahHariPendingProcess(){
        $this->load->model('Documents_log_model');
        $getPending = $this->Documents_log_model->filter(" where status='".Documents_log_model::PROGRESS."'")->getAll();
        // echo "sebelum update:<br>";
        echo "<pre>";print_r($getPending);echo "</pre>";
        foreach ($getPending as $pending) {
            $d_update['jumlah_hari'] = $pending['jumlah_hari']+1;
            // echo $d_update['hari'];
            // echo $pending['id']."<br>";
            $this->Documents_log_model->updateDate('updated_at',"where id='".$pending['id']."'")->update($pending['id'], $d_update);

        }
        // $this->generatePendingProcess();
        // $this->generateLeadTime();
    }

    public function cronStatistikPendingPublish(){
        $this->tambahHariPendingProcess();
        $this->generateLeadTime();
        $this->inputStatistic();
    }

    public function cronStatistikPendingPublish_maintenance(){
        $this->generateLeadTime();
        $this->inputStatistic();
    }

    public function generatePendingProcess_n(){
        $this->load->model("Document_model");
        $this->load->model("Workflow_model");
        $this->load->model("Statistic_documents_model", "model");
        $this->load->model("Documents_log_model");
        $insertPending = array();
        $data = $this->Documents_log_model->filter(" where status='".Documents_log_model::PROGRESS."' and unitkerja_id=50045258")->getAll();
        echo "<pre>";print_r($data);echo "</pre>";
        foreach ($data as $pending) {
            $getDoc = $this->Document_model->getBy($pending['document_id']);
            // echo "<pre>";print_r($getDoc);echo "</pre>";
            $get_total_doc_p = $this->Documents_log_model->show_sql(false)->filter("where l.workflow_id=".$pending['workflow_id']." and l.workflow_urutan=".$pending['workflow_urutan']." and l.unitkerja_id=".$pending['unitkerja_id']." and d.type_id='".$getDoc['TYPE_ID']."' and l.status='P' ")->joinDocumentsDetail();

            if($get_total_doc_p){
                $total_doc = count($get_total_doc_p);
            }
            else{
                $total_doc = 0;
            }
            if(!$insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]){
                $data_stat_doc = $this->model->show_sql(false)->filter("where unitkerja_id = '".$pending['unitkerja_id']."' and doc_type='".$getDoc['TYPE_ID']."' and workflow_id=".$pending['workflow_id']." and workflow_urutan=".$pending['workflow_urutan'])->getAll();
                // print_r($data_stat_doc);
                if(!$data_stat_doc){ // jika data di statistic_document masih tidak ada, maka masukkan ke array
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['unitkerja_id'] = $pending['unitkerja_id'];
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['workflow_id'] = $pending['workflow_id'];
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['workflow_urutan'] = $pending['workflow_urutan'];
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['doc_type'] = $getDoc['TYPE_ID'];
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['type'] = Statistic_documents_model::PENDING_PROCESS;
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['actor'] = $pending['actor'];
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['jumlah_hari'] = $pending['jumlah_hari'];
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['total_data_jumlah_hari'] = 1;
                    $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['total_document'] = $total_doc;
                }
                else{ // jika data ada, tambah hari dan diupdate
                    // if($data_stat_doc[0]['type'] == Statistic_documents_model::PENDING_PROCESS){ // Jika data masih belum publish (belum selesai), maka update hari disini
                        // echo "jumlah hari: ".$pending['jumlah_hari'];
                        $dataUpdate['jumlah_hari'] = $data_stat_doc[0]['jumlah_hari']+1;
                        $dataUpdate['total_document'] = $total_doc;
                        // echo "update ".$data_stat_doc[0]['id']."<br>";
                        dbUpdate('Statistic_documents', $dataUpdate, "id=".$data_stat_doc[0]['id']);
                        // $this->model->updateDate('last_updated',"where id=".$data_stat_doc[0]['id'])->update($data_stat_doc[0]['id'], $dataUpdate);
                    // }
                    // elseif(){

                    // }
                    
                }
                
            }
            else{
                $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['jumlah_hari'] = $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['jumlah_hari']+$pending['jumlah_hari'];
                $insertPending[$pending['unitkerja_id']][$getDoc['TYPE_ID']][$pending['workflow_id']][$pending['workflow_urutan']]['total_data_jumlah_hari'] += 1;
            }
        }
        // echo "<pre>";print_r($insertPending);echo "</pre>";
        foreach ($insertPending as $insert) {
            // $doc_type = array_keys($insert);
            // print_r($keys);
            for ($i=1; $i <=5 ; $i++) { // doc_type 
                for ($j=1; $j <= 3 ; $j++) { // w_id
                    for ($k=1; $k < 5 ; $k++) { // w_urutan
                        $stat_doc = array();
                        if($insert[$i][$j][$k]['unitkerja_id']){
                            echo "insert<br>";
                            $stat_doc['unitkerja_id'] =  $insert[$i][$j][$k]['unitkerja_id'];
                            $stat_doc['workflow_id'] = $j;
                            $stat_doc['workflow_urutan'] = $k;
                            $stat_doc['jumlah_hari'] = floor($insert[$i][$j][$k]['jumlah_hari'] / $insert[$i][$j][$k]['total_data_jumlah_hari'] );
                            $stat_doc['doc_type'] = $i;
                            $stat_doc['type'] = Statistic_documents_model::PENDING_PROCESS;
                            $stat_doc['actor'] = $insert[$i][$j][$k]['actor'];
                            $stat_doc['total_document'] = $insert[$i][$j][$k]['total_document'];
                            echo "<pre>";print_r($stat_doc);echo "</pre>";
                            $this->model->insert($stat_doc);
                        }
                        else{
                            echo "data dengan ketentuan memang tidak ada<br>";
                        }
                    }
                }
            }
        }
        
    }

    public function inputStatistic(){
        $this->load->model('Statistic_documents_model');
        $this->load->model('Workflow_model');
        $this->load->model('Unitkerja_model');
        $dataStatistik_p = array();
        $dataStatistik_l = array();
        $data_uk = $this->Statistic_documents_model->filter('group by unitkerja_id')->order('order by unitkerja_id')->column(array( "unitkerja_id" => "\"unitkerja_id\""))->getAll();
        // $dataDept = "SELECT UNITKERJA_ID FROM STATISTIC_DOCUMENTS GROUP BY UNITKERJA_ID ORDER BY UNITKERJA_ID";
        // $getData = dbGetRows($dataDept);
        // echo "<pre>";print_r($data_uk);echo "</pre>";
        foreach ($data_uk as $data) { // pecah unitkerja
            $detail =  $this->Unitkerja_model->getBy($data['unitkerja_id']);
            if($detail['level'] == 'DEPT'){
                $dataStatistik_p[$data['unitkerja_id']]['UNITKERJA_ID'] = $data['unitkerja_id'];
                $dataStatistik_p[$data['unitkerja_id']]['BIRO_ID'] = '0';
                $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'] = '0';
            }
            elseif($detail['level'] == 'BIRO'){
                $dataStatistik_p[$data['unitkerja_id']]['UNITKERJA_ID'] = $data['unitkerja_id'];
                $dataStatistik_p[$data['unitkerja_id']]['BIRO_ID'] = '0';
                $detail_parent = $this->Unitkerja_model->getBy($detail['parent']); 
                if($detail_parent['level'] == 'DEPT'){
                    $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'] = $detail['parent'];    
                }
                else{
                    $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'] = '0';
                }
                
            }
            elseif($detail['level'] == 'SECT'){
                $dataStatistik_p[$data['unitkerja_id']]['UNITKERJA_ID'] = $data['unitkerja_id'];
                $dataStatistik_p[$data['unitkerja_id']]['BIRO_ID'] = '0';
                $detail_parent = $this->Unitkerja_model->getBy($detail['parent']); 
                if($detail_parent['level'] == 'BIRO'){
                    $dataStatistik_p[$data['unitkerja_id']]['BIRO_ID'] = $detail['parent'];
                    $detail_parent_dept = $this->Unitkerja_model->getBy($detail_parent['parent']);
                    if($detail_parent_dept['level'] == 'DEPT'){
                        $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'] = $detail_prent['parent'];
                    }
                    else{
                        $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'] = '0';
                    }
                }
                elseif($detail_parent['level'] == 'DEPT'){
                    $dataStatistik_p[$data['unitkerja_id']]['BIRO_ID'] = '0';
                    $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'] = $detail['parent'];
                }
                else{
                    $dataStatistik_p[$data['unitkerja_id']]['BIRO_ID'] = '0';
                    $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'] = '0';
                }
                
            }
            $this->load->model('Unitkerja_model');
            $getCompany = $this->Unitkerja_model->getBy($data['unitkerja_id']);
            $dataStatistik_p[$data['unitkerja_id']]['COMPANY'] = $getCompany['company'];
            $dataStatistik_l[$data['unitkerja_id']]['COMPANY'] = $getCompany['company'];
            // echo "<pre>";print_r($detail);echo "</pre>";
            $dataStatistik_l[$data['unitkerja_id']]['UNITKERJA_ID'] = $dataStatistik_p[$data['unitkerja_id']]['UNITKERJA_ID'];
            $dataStatistik_l[$data['unitkerja_id']]['BIRO_ID'] = $dataStatistik_p[$data['unitkerja_id']]['BIRO_ID'];
             $dataStatistik_l[$data['unitkerja_id']]['DEPT_ID'] = $dataStatistik_p[$data['unitkerja_id']]['DEPT_ID'];
            $dataStatistik_p[$data['unitkerja_id']]['JENIS_UNITKERJA'] = $detail['level'];
            $dataStatistik_l[$data['unitkerja_id']]['JENIS_UNITKERJA'] = $detail['level'];
            $dataStatistik_p[$data['unitkerja_id']]['JENIS_STATISTIK'] = Statistic_documents_model::PENDING_PROCESS;
            $dataStatistik_l[$data['unitkerja_id']]['JENIS_STATISTIK'] = Statistic_documents_model::LEAD_TIME_PUBLISH;
            $dataDocType = $this->Statistic_documents_model->filter("where unitkerja_id='".$data['unitkerja_id']."' group by doc_type")->order('order by doc_type')->column(array( "doc_type" => "\"doc_type\""))->show_sql(false)->getAll();
            // echo "<pre>";print_r($dataDocType);echo "</pre>";
            foreach ($dataDocType as $docType) { /// PECAH DOC TYPE
                // $getWorkflow = "SELECT WORKFLOW_ID FROM STATISTIC_DOCUMENTS WHERE DEPT_ID =".$data['DEPT_ID']." AND DOC_TYPE='".$docType['DOC_TYPE']."' GROUP BY WORKFLOW_ID ORDER BY WORKFLOW_ID";
                $dataWorkflow = $this->Statistic_documents_model->filter("where unitkerja_id='".$data['unitkerja_id']."' and doc_type=".$docType['doc_type']." group by workflow_id")->order('order by workflow_id')->column(array( "workflow_id" => "\"workflow_id\""))->getAll();
                // $dataWorkflow = dbGetRows($getWorkflow);
                // echo "<pre>";print_r($dataWorkflow);echo "</pre>";
                $dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']]['DOC_TYPE'] = $docType['doc_type'];
                $dataStatistik_l[$data['unitkerja_id']][$docType['doc_type']]['DOC_TYPE'] = $docType['doc_type'];
                foreach ($dataWorkflow as $workflow) { // pecah workflow_id
                    // echo "masuk foreach workflow<br>";
                    $dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['WORKFLOW_ID'] = $workflow['workflow_id'];
                    $dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] = '{';
                    $dataStatistik_l[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['WORKFLOW_ID'] = $workflow['workflow_id'];
                    $dataStatistik_l[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] = '{';
                    $urutan = $this->Workflow_model->getTasks($workflow['workflow_id']);
                    // echo "urutan<br>";
                    // echo "<pre>";print_r($urutan);echo "</pre>";
                    
                    for ($i=0; $i <count($urutan) ; $i++) {
                        $totalDoc  = 0;
                        $totalHari = 0;
                        //////////////////////// Jumlah Dokumen///////////////////////////
                        // echo "workfow_id:".$workflow['workflow_id'];
                        // echo "<pre>";print_r($doc);echo "</pre>";
                        echo "unitkerja_id:".$data['unitkerja_id']." || workflow_id:".$workflow['workflow_id']." || workflow_urutan:".$urutan[$i]['urutan']." || doc_type:".$docType['doc_type']."<br>";
                        if($workflow['workflow_id'] == 2 && $urutan[$i]['urutan'] == 1 && $docType['doc_type'] == 3){
                            $true = true;
                        }
                        else{
                            $true = false;
                        }
                        $total_b = $this->Statistic_documents_model->filter("where unitkerja_id='".$data['unitkerja_id']."' and doc_type=".$docType['doc_type']." and workflow_id=".$workflow['workflow_id']." and workflow_urutan=".$urutan[$i]['urutan']." and actor='".$urutan[$i]['user']."'")->order()->column(array( "total_document" => "\"total_document\"" , "jumlah_hari" => "\"jumlah_hari\""))->show_sql(false)->getAll();
                        // echo "ini total<br>";
                        // echo "<pre>";print_r($total_b);echo "</pre>";
                        $total = $total_b[0];
                        echo "<pre>";print_r($total);echo "</pre>";
                        if($total['total_document'] == null || $total['total_document'] == ''){
                            echo "masuk if<br>";
                            $totalDoc = 0;
                        }
                        else{
                            echo "masuk else total Doc:".$total['total_document']."<br>";
                            $totalDoc = $total['total_document'];
                        }

                        if($total['jumlah_hari'] == null || $total['jumlah_hari'] == ''){
                            echo "masuk if hari<br>";
                            $totalHari = 0;
                        }
                        else{
                            // echo "<pre>"
                            echo "masuk else total hari:".$total['jumlah_hari']."<br>";
                            $totalHari = $total['jumlah_hari'];
                        }

                        if($i == count($urutan)-1){
                            $dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] .= '"'.$i.'":"'.$totalDoc.'"';
                            $dataStatistik_l[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] .= '"'.$i.'":"'.$totalHari.'"';
                            // echo "status urutan saat ini:<br>";
                            // echo "ini urutan: ".$dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN']."<br>";
                            // echo "unitkerja_id:".$dataStatistik_p[$data['unitkerja_id']]['UNITKERJA_ID']." || "," workflow_id:".$dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['WORKFLOW_ID']." || type_id:".$dataStatistik_l[$data['unitkerja_id']][$docType['doc_type']]['DOC_TYPE']."<br>";
                            // echo $dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']]['URUTAN']."<br>";
                        }
                        else{
                            $dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] .= '"'.$i.'":"'.$totalDoc.'",';
                            $dataStatistik_l[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] .= '"'.$i.'":"'.$totalHari.'",';
                        }
                        
                    }
                    $dataStatistik_p[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] .= '}';
                    $dataStatistik_l[$data['unitkerja_id']][$docType['doc_type']][$workflow['workflow_id']]['URUTAN'] .= '}';
                    // echo "<pre>";print_r($dataStatistik_p[$data['DEPT_ID']]['URUTAN']);echo "</pre>";
                }
                
            }

            
        }
        echo "<pre>";print_r($dataStatistik_p);echo "</pre>";
        echo "<pre>";print_r($dataStatistik_l);echo "</pre>";

        $this->load->model('Statistic_model');
        foreach ($dataStatistik_p as $p) {
            echo "<pre>";print_r($p);echo "<pre>";
            $doc_type = array_keys($p);
            echo "<pre>";print_r($doc_type);echo "</pre>";
            $jumlahIndex = count($doc_type);
            echo "jumlahIndex:".$jumlahIndex."<br>";
            for ($i=5; $i < $jumlahIndex ; $i++) {
                // echo "a<br>";
                // echo "<pre>";print_r($p[$doc_type[$i]]);echo "</pre>";
                $workflow_id = array_keys($p[$doc_type[$i]]);
                // echo "<pre>";print_r($workflow_id);echo "</pre>";
                for ($j=1; $j < count($workflow_id) ; $j++) { 
                    echo "<pre>";print_r($p[$doc_type[$i]][$workflow_id[$j]]);echo "</pre>";
                    $data_urutan = json_decode($p[$doc_type[$i]][$workflow_id[$j]]['URUTAN'], true);
                    echo "<pre>";print_r($data_urutan);echo "</pre>";
                    foreach ($data_urutan as $urutan) {
                        if($urutan != 0){
                            // insert disini;
                            $cekData =$this->Statistic_model->cekDataStatistic($p['UNITKERJA_ID'],$p[$doc_type[$i]][$workflow_id[$j]]['WORKFLOW_ID'],$p[$doc_type[$i]]['DOC_TYPE'],$p['JENIS_STATISTIK'] );
                            echo "<pre>";print_r($cekData);echo "</pre>";
                            if($cekData){ // jika ada data, maka update
                                $dataUpdate = array(
                                    'URUTAN'    => $p[$doc_type[$i]][$workflow_id[$j]]['URUTAN']
                                );
                                $this->Statistic_documents_model->table('statistics')->update($cekData['ID'], $dataUpdate);
                                echo "update P<br>";
                            }
                            else{ // Jika tidak ada data, insert
                                $dataInsert = array(
                                    'UNITKERJA_ID'      => $p['UNITKERJA_ID'],
                                    'JENIS_UNITKERJA'   => $p['JENIS_UNITKERJA'],
                                    'WORKFLOW_ID'       => $p[$doc_type[$i]][$workflow_id[$j]]['WORKFLOW_ID'],
                                    'URUTAN'            => $p[$doc_type[$i]][$workflow_id[$j]]['URUTAN'],
                                    'DOC_TYPE'          => $p[$doc_type[$i]]['DOC_TYPE'],
                                    'JENIS_STATISTIK'   => $p['JENIS_STATISTIK'],
                                    'BIRO_ID'           => $p['BIRO_ID'],
                                    'DEPT_ID'           => $p['DEPT_ID'],
                                    'COMPANY'           => $p['COMPANY']
                                );
                                $this->Statistic_documents_model->table('statistics')->insert($dataInsert);
                                echo "insert P<br>";
                                // echo "<pre>";print_r($dataInsert);echo "</pre>";
                            }
                            

                            break;
                        }
                    }
                }
            }
            
            
            
        }
        foreach ($dataStatistik_l as $l) {
            echo "<pre>";print_r($l);echo "<pre>";
            $doc_type = array_keys($l);
            echo "<pre>";print_r($doc_type);echo "</pre>";
            $jumlahIndex = count($doc_type);
            echo "jumlahIndex:".$jumlahIndex."<br>";
            for ($i=5; $i < $jumlahIndex ; $i++) {
                // echo "a<br>";
                // echo "<pre>";print_r($p[$doc_type[$i]]);echo "</pre>";
                $workflow_id = array_keys($l[$doc_type[$i]]);
                // echo "<pre>";print_r($workflow_id);echo "</pre>";
                for ($j=1; $j < count($workflow_id) ; $j++) { 
                    echo "<pre>";print_r($l[$doc_type[$i]][$workflow_id[$j]]);echo "</pre>";
                    $data_urutan = json_decode($l[$doc_type[$i]][$workflow_id[$j]]['URUTAN'], true);
                    echo "<pre>";print_r($data_urutan);echo "</pre>";
                    foreach ($data_urutan as $urutan) {
                        if($urutan != 0){
                            // insert disini;
                            $cekData =$this->Statistic_model->cekDataStatistic($l['UNITKERJA_ID'],$l[$doc_type[$i]][$workflow_id[$j]]['WORKFLOW_ID'],$l[$doc_type[$i]]['DOC_TYPE'],$l['JENIS_STATISTIK'] );
                            echo "<pre>";print_r($cekData);echo "</pre>";
                            if($cekData){ // jika ada data, maka update
                                $dataUpdate = array(
                                    'URUTAN'    => $l[$doc_type[$i]][$workflow_id[$j]]['URUTAN'],
                                    'BIRO_ID'   => $l['BIRO_ID'],
                                    'DEPT_ID'   => $l['DEPT_ID'],
                                    'COMPANY'   => $l['COMPANY']
                                );
                                $this->Statistic_documents_model->table('statistics')->update($cekData['ID'], $dataUpdate);
                                echo "update L<br>";
                            }
                            else{ // Jika tidak ada data, insert
                                $dataInsert = array(
                                    'UNITKERJA_ID'      => $l['UNITKERJA_ID'],
                                    'JENIS_UNITKERJA'   => $l['JENIS_UNITKERJA'],
                                    'WORKFLOW_ID'       => $l[$doc_type[$i]][$workflow_id[$j]]['WORKFLOW_ID'],
                                    'URUTAN'            => $l[$doc_type[$i]][$workflow_id[$j]]['URUTAN'],
                                    'DOC_TYPE'          => $l[$doc_type[$i]]['DOC_TYPE'],
                                    'JENIS_STATISTIK'   => $l['JENIS_STATISTIK'],
                                    'BIRO_ID'           => $l['BIRO_ID'],
                                    'DEPT_ID'           => $l['DEPT_ID'],
                                    'COMPANY'           => $l['COMPANY']
                                );
                                $this->Statistic_documents_model->table('statistics')->insert($dataInsert);
                                echo "insert L<br>";
                                // echo "<pre>";print_r($dataInsert);echo "</pre>";
                            }
                            

                            break;
                        }
                    }
                }
            }
        }
        
    }
}
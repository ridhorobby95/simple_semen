<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	/**
	 * Controller Notification
	 * @author Sevima
	 * @version 1.0
	 */ 
	class Notification extends WEB_Controller {
		protected $title = 'Notifications';
		
		/**
		 * Halaman default
		 * @param int $page
		 */
		public function index() {
			redirect($this->ctl.'/me');
		}
		
		/**
		 * Halaman daftar notifikasi
		 * @param int $page
		 */
		public function me($page=0) {
			// tombol
			$buttons = array();
			$buttons[] = array('label' => 'Read All', 'type' => 'warning', 'icon' => 'check', 'click' => 'goReadAll()');
			
			$this->data['buttons'] = $buttons;
			$user_id=SessionManagerWeb::getUserID();
			$this->data['data'] = $this->model->with(" left join notification_users nu on nu.notification_id=n.id ")->filter(" and nu.user_id=$user_id ")->order(' n.created_at desc ')->getAll();
			$this->load->model('Notification_sender_model');
			$this->load->model('Notification_user_model');
			foreach ($this->data['data'] as $key => $value) {
				$notification_id = $value['id'];
				$sender = $this->Notification_sender_model->filter(" where notification_id=$notification_id ")->getOne('user_id');
				$this->data['data'][$key]['sender'] = $this->User_model->getUser($sender);
				if (strtolower($this->data['data'][$key]['sender']['username'])=='user20'){
					$this->data['data'][$key]['sender']['name'] = '';
					$this->data['data'][$key]['sender']['photo'] = $this->User_model->getSystemPhoto();
				}
				$this->data['data'][$key]['users'] = $this->Notification_user_model->filter(" where notification_id=$notification_id and user_id=$user_id")->getBy();
			}
			// echo '<pre>';
			// var_dump($this->data['data']);
			// die();
			$this->template->viewDefault($this->class.'_index',$this->data);
		}
		
		/**
		 * Set baca untuk semua notifikasi
		 * @param int $id
		 */
		public function read_all() {
			$this->load->model('Notification_user_model');
			$this->Notification_user_model->setAsUnRead(SessionManagerWeb::getUserID());
			
			$ok = true;
			$msg = 'Berhasil set baca semua notifikasi';
			
			SessionManagerWeb::setFlashMsg($ok, $msg);
			redirect($this->ctl.'/me');
		}
		
		/**
		 * Set baca untuk notifikasi
		 * @param int $id
		 */
		public function read($id) {
			$this->load->model('Notification_user_model');
			$this->Notification_user_model->update_by(array('user_id' => SessionManagerWeb::getUserID(), 'notification_id' => $id),array('status' => Notification_user_model::STATUS_READ));
		}
	}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Document_prosesbisnis extends WEB_Controller {

    protected $title = 'Business Process';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
    	$data = $this->model->getAll();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    function add() {
        $record = $this->input->post(null, true);
        $this->model->add($record);
        redirect($this->ctl );        
    }

    public function edit($id=NULL){
    	if (isset($id))
            $data = $this->model->getBy($id);
        else
            $data = array();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function delete($id) {
        $delete = $this->model->delete($id);

        if ($delete === true) {
            $ok = true;
            $msg = 'Successfully deleted business process';
        } else {
            $ok = false;
            $msg = 'Failed to delete business process';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl);
    }
    
    public function update($id = null) {
        $data = $this->input->post(null, true);
        $update = $this->model->save($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Successfully changed business process';

        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change business process';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl );
    }
}

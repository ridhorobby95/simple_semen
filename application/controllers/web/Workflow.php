<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Workflow extends WEB_Controller {

    protected $title = 'Workflow';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
    	$data = $this->model->getWorkflows();
    	$this->data['data'] = $data;
        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Tambah Workflow', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        $this->data['buttons'] = $buttons;
    	$this->template->viewDefault($this->view, $this->data);
    }

    function add2($name) {
        $name = urldecode($name);
        $this->model->add($name, SessionManagerWeb::getUserID());
        redirect($this->ctl );        
    }

    public function add(){
        $this->title = 'New Workflow';

        if ($_POST) {
            $record = array();

            $rule = array();
            $rule['nama'] = $_POST['nama'];
            $rule['jumlah_node'] = (int) $_POST['jumlah_node'];
            $rule['created'] = date('Y-m-d H:i:s');
            $rule['created_by'] = SessionManagerWeb::getUserID();

            if (!$rule['jumlah_node'])
                $rule['jumlah_node'] = 3;
            $model = array('class'=>'go.GraphLinksModel', 'copiesArrays'=> true, 'copiesArrayObjects'=> true);
            for ($i=1;$i<=$rule['jumlah_node'];$i++) {
                $name = 'Node';
                $category = '';
                if ($i == 1) {
                    $name = 'Start';
                    $category = 'Source';
                }
                elseif ($i == $rule['jumlah_node']) {
                    $name = 'End';
                    $category = 'Source';
                }
                else {
                    $name .= ' ' . $i;
                }
                $nodes[] = array('key'=>'node_'.$i, 'text'=> $name, 'category'=>$category);
                $links[] = array('from'=>'node_'.$i, 'to'=>'node_'.($i+1));
            }
            $model['nodeDataArray'] = $nodes;
            $model['linkDataArray'] = $links;

            $rule['model'] = $model;

            $record['rule'] = json_encode($rule);



            dbInsert('workflows' ,$record);

            $id = dbGetOne('select max(id) from workflows');
            redirect('web/workflow/configure/' . $id);
        }

        $this->template->viewDefault($this->view, $this->data);
    }

    public function configure($id) {
        $sql = "select * from workflows where id=$id";
        $row = array_change_key_case(dbGetRow($sql));
        if ($_POST) {
            $rule = json_decode($row['rule'], true);
            
            foreach ($rule['model']['nodeDataArray'] as &$node) {
                $temp = explode('_', $node['key']);
                $i_node = $temp[1];
                $node['text'] = $_POST['node_' . $i_node];
                $node['actor'] = $_POST['actor_' . $i_node];
                $node['form'] = $_POST['form_' . $i_node];
                $node['keterangan'] = $_POST['keterangan_' . $i_node];

            }

            $links = array();
            foreach ($_POST as $k=>$v) {
                if (strstr($k, 'approve') && $v) {
                    $i_node = str_replace('approve_', '', $k);
                    $links[] = array('from'=> 'node_' . $i_node, 'to'=>$v);
                }
            }

            foreach ($_POST as $k=>$v) {
                if (strstr($k, 'refuse') && $v) {
                    $i_node = str_replace('refuse_', '', $k);
                    $links[] = array('from'=> 'node_' . $i_node, 'to'=>$v);
                }
            }

            $rule['model']['linkDataArray'] = $links;
            $rule['nama'] = $_POST['nama'];
            $rule['tenggat_hari'] = (int) $_POST['tenggat_hari'];
            $rule['file_pdf'] = $_POST['file_pdf'];
            $rule['updated'] = date('Y-m-d H:i:s');
            $rule['updated_by'] = SessionManagerWeb::getUserID();
            $rule['nama'] = $_POST['nama'];

            $rule = json_encode($rule);
            $sql = "update workflows set rule='$rule' where id=$id";
            dbQuery($sql);

            redirect('web/workflow/configure/'. $id);
        }

        $row['rule'] = json_decode($row['rule'], true);
        foreach ($row['rule']['model']['nodeDataArray'] as &$node) {
            $node['text2'] = $node['text'];
            $node['text'] = $node['text'] . "\n-------------------------\n" . $this->data['actors'][$node['actor']];
        }
        $this->data['row'] = $row;

        $this->template->viewDefault($this->view, $this->data);
    }

    function task($id) {

        $sql = "select * from workflow_tasks where id=$id";
        $task = array_change_key_case(dbGetRow($sql));
        $task['header'] = json_decode($task['header'], true);
        $task['content'] = json_decode($task['content'], true);
        $task['footer'] = json_decode($task['footer'], true);

        $id_submit = $task['submit_id'];
        $id_workflow = $task['workflow_id'];

        $sql = "select * from workflows where id=$id_workflow";
        $workflow = array_change_key_case(dbGetRow($sql));
        $workflow['rule'] = $rule_ori = json_decode($workflow['rule'], true);

        // $sql = "select isi from workflow_forms where id={$workflow['rule']['model']['nodeDataArray'][0]['form']}";
        // $isi = array_change_key_case(dbGetOne($sql));
        // ob_start();
        $data = array();

        foreach ($task['content'] as $k=>$v) {
            $data[$k] = $v;
        }

        // $file = $this->config->item('view_path')  . 'workflow_temp.php';
        // file_put_contents($file , $isi);
        // echo '<pre>';
        // var_dump($task);
        // die();
        // $this->load->view('workflow_temp', $data);
        // $generated_html = ob_get_contents();
        // ob_end_clean();     
        // $task['isi'] = $generated_html;
        // die();
        $sql = "select node_id from workflow_tasks where submit_id={$task['submit_id']} and is_active=1";
        $task['id_node_active'] = array_change_key_case(dbGetOne($sql));
        $this->data['task'] = $task;

        $node_actors = array();
        foreach ($workflow['rule']['model']['nodeDataArray'] as &$node) {
            $node_actors[$node['key']] = $node['actor'];
            // if ($node['form']) {
            //     $sql = "select isi from workflow_form where form_id='{$node['form']}'";
            //     $node['form'] = array_change_key_case(dbGetOne($sql));
            // }
        }

        if ($_POST) {
            $id_task = $_POST['id_task'];
            $sql = "select * from workflow_tasks where id=$id_task";
            $task2 = array_change_key_case(dbGetRow($sql));
            $id_node = $task2['node_id'];

            foreach ($workflow['rule']['model']['linkDataArray'] as $link) {
                if ($link['from'] == $id_node) {
                    $i_from = $temp[1];
                    $temp = explode('_', $link['to']);
                    $i_to = $temp[1];

                    if ($i_from < $i_to)
                        $next_node = $link['to'];   
                    else
                        $prev_node = $link['to'];   

                }
            }

            $record = array();
            $header = array(
                'id_user'=>SessionManagerWeb::getUserID(),
                'created'=>date("Y-m-d H:i:s"),
                );
            $record['header'] = json_encode($header);

            $content = $_POST;
            unset($content['act']);
            unset($content['id_task']);

            $record['content'] = json_encode($content);
            $ret = dbUpdate('task', $record, "id=$id_task");

            // if ($ret) {
            //     if (strtolower($content['recommendation']) == 'proceed') {
            //         $next_id_node = $next_node;
            //     }
            //     else {
            //         $next_id_node = $prev_node; 
            //     }

            //     $sql = "update task set is_active=null where id_submit=$id_submit and id_workflow=$id_workflow";
            //     dbQuery($sql);

            //     $sql = "update task set is_active=1 where id_submit=$id_submit and id_workflow=$id_workflow and id_node='$next_id_node'";
            //     dbQuery($sql);

            //     $rule_ori['notification'][$_SESSION[G_SESSION]['id_user']] = abs((int) $rule_ori['notification'][$_SESSION[G_SESSION]['id_user']]-1);
            //     $rule_ori['notification'][$node_actors[$next_id_node]] = (int) $rule_ori['notification'][$node_actors[$next_id_node]]+1;                
            //     $rule = json_encode($rule_ori);
            //     $sql = "update workflow set rule='$rule' where id_workflow=$id_workflow";
            //     dbQuery($sql);

            //     $this->createpdf($id_task);
            // }

            redirect('home/task/'.$id);
        }

        $sql = "select * from workflow_tasks where submit_id=$id_submit order by id";
        $list_task = array_change_key_case(dbGetRows($sql));
        foreach ($list_task as &$task) {
            $task = array_change_key_case($task);
            $task['header'] = json_decode($task['header'], true);
            $task['content'] = json_decode($task['content'], true);
            $task['footer'] = json_decode($task['footer'], true);       
            $sql = "select node_id from workflow_tasks where submit_id={$task['submit_id']} and is_active=1";
            $task['id_node_active'] = $id_node_active = array_change_key_case(dbGetOne($sql));
            $last_node = 'node_' . $workflow['rule']['jumlah_node'];
            if ($id_node_active == $last_node) {
                $list_task[0]['header']['finished'] = $last_date;
            }
            $last_date = $task['header']['created'];
        }
        $this->data['list_task'] = $list_task;
        $this->data['workflow'] = $workflow;
        $this->data['page_title'] = $workflow['rule']['nama'];// .  ': ' . $workflow['rule']['model']['nodeDataArray'][0]['text'];
        $this->data['title'] = $this->title.' '.$workflow['rule']['nama'];
        $this->template->viewDefault($this->view,$this->data);
        // $this->renderView('task');
    }

    function submit($id_workflow) {
        $sql = "select * from workflows where id=$id_workflow";
        $row = dbGetRow($sql);

        $row['rule'] = $rule_ori = json_decode($row['rule'], true);

        $node_actors = array();
        foreach ($row['rule']['model']['nodeDataArray'] as &$node) {
            $node_actors[$node['key']] = $node['actor'];
        }

        if ($_POST) {
            $sql = "select max(submit_id) from workflow_tasks";
            $id_submit = dbGetOne($sql) + 1;

            $record = array();
            $record['workflow_id'] = $id_workflow;
            $record['node_id'] = 'node_1';
            $record['submit_id'] = $id_submit;

            $header = array(
                'id_user'=>$_SESSION[G_SESSION]['id_user'],
                'created'=>date("Y-m-d H:i:s"),
                );
            $record['header'] = json_encode($header);

            $content = $_POST;
            unset($content['act']);

            $record['content'] = json_encode($content);
            $ret = dbInsert('task', $record);

            if ($ret) {
                $sql = "select max(id_task) from task where id_workflow=$id_workflow";
                $id_task = dbGetOne($sql);

                $j=1;
                foreach ($row['rule']['model']['nodeDataArray'] as $node) {
                    if ($node['key'] == 'node_1') {
                        $j++;
                        continue;
                    }

                    $record = array();
                    $record['id_workflow'] = $id_workflow;
                    $record['id_node'] = 'node_' . $j;//$node['key'];
                    $record['id_submit'] = $id_submit;
                    dbInsert('task', $record);
                    $j++;
                }

                $next_id_node = 'node_2';

                $sql = "update task set is_active=1 where id_submit=$id_submit and id_workflow=$id_workflow and id_node='{$next_id_node}'";
                dbQuery($sql);

                $rule_ori['notification'][$node_actors[$next_id_node]] = (int) $rule_ori['notification'][$node_actors[$next_id_node]]+1;                
                $rule = json_encode($rule_ori);
                $sql = "update workflow set rule='$rule' where id_workflow=$id_workflow";
                dbQuery($sql);

                $this->createpdf($id_task);

                redirect('home/task/' . $id_task);
            }

        }

        $this->data['page_title'] = $row['rule']['nama'] .  ': ' . $row['rule']['model']['nodeDataArray'][0]['text'];

        $sql = "select isi from form where id_form={$row['rule']['model']['nodeDataArray'][0]['form']}";
        $isi = dbGetOne($sql);
        ob_start();
        $data = array();
        $data['edit'] = 1;
        $file = $this->config->item('view_path')  . 'temp.php';
        file_put_contents($file , $isi);
        $this->load->view('temp', $data);
        $generated_html = ob_get_contents();
        ob_end_clean();     

        $this->data['include_form'] = $generated_html;

        $this->data['row'] = $row;

        $this->renderView('submit');
    }

    public function edit($id=NULL){
        $workflow = $this->model->getWorkflow($id);
    	// if (isset($id))
        $tasks = $this->model->getWorkflowTasks($id);
        $flows = $this->model->getTaskFlows($id);
        // else
        //     $tasks = array();
        $this->data['workflow']['id_workflow'] = $workflow['id'];
        $this->data['workflow']['rule']['nama'] = $workflow['name'];
        $this->data['workflow']['rule']['jumlah_node'] = count($tasks);
        $this->data['workflow']['rule']['model']['class'] = 'go.GraphLinksModel';
        // $this->data['workflow']['rule']['model']["copiesArrays"] = true;
        // $this->data['workflow']['rule']['model']["copiesArrayObjects"] = true;
        $this->data['workflow']['rule']['model']["linkFromPortIdProperty"] = 'fromPort';
        $this->data['workflow']['rule']['model']["linkToPortIdProperty"] = 'toPort';
        $this->data['workflow']['rule']['model']['nodeDataArray'] = $tasks;
        $this->data['workflow']['rule']['model']['linkDataArray'] = $flows;
        $this->data['workflow']['rule']['file_pdf'] = "";
        $this->data['workflow']['rule']['tenggat_hari'] = (int) $workflow['day_limit'];
        $this->data['workflow']['rule']['updated'] = $workflow['updated_at'];
        $this->load->model('User_model');
        $username = $this->User_model->getName($workflow['creator']);
        $this->data['workflow']['rule']['updated_by'] = $username['name'];
        $this->data['workflow']['rule']['notification'] = "";
        $temp2 = -10;
        foreach ($this->data['workflow']['rule']['model']['nodeDataArray'] as $key => $node) {
            $this->data['workflow']['rule']['model']['nodeDataArray'][$key]['text2'] = $node['name'];
            $this->data['workflow']['rule']['model']['nodeDataArray'][$key]['text'] = $node['name'] . "\n-------------------------\n" . $node['actor'];
            if ($node['category']=='Decision' || $node['category']=='Start' || $node['category']=='End')
                $this->data['workflow']['rule']['model']['nodeDataArray'][$key]['text'] = $node['name'];
            $x=(($node['position']-3)*200);
            $this->data['workflow']['rule']['model']['nodeDataArray'][$key]['loc'] = $x." ".$temp;
            $temp+=100;
        }
        // echo '<pre>';
        // var_dump($this->data['workflow']);
        // die();

    	$this->template->viewDefault($this->view, $this->data);
    }

    public function delete($id) {
        $delete = $this->model->delete($id);

        if ($delete === true) {
            $ok = true;
            $msg = 'Berhasil menghapus workflow';
        } else {
            $ok = false;
            $msg = 'Gagal menghapus workflow';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl);
    }
    
    public function update($id = null) {
        $data = $this->postData;
        $update = $this->model->save($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Berhasil mengubah Workflow Dokumen';

        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Gagal mengubah Workflow Dokumen';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl );
    }
}

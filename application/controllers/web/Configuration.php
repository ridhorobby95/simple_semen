<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller Group_type
 * @author Integra
 * @version 1.0
 */
class Configuration extends WEB_Controller {

    protected $title = 'Configuration';

    /**
     * Halaman default
     * @param int $page
     */
    public function index() {
        $this->data['data'] = $this->model->getConfigurations();
        $this->data['title'] = 'Configuration';

        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Add Configuration', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        $this->data['buttons'] = $buttons;

        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Fungsi Delete group_type
     */
    public function delete($id) {
        $ok = false;
        $delete = $this->model->delete($id);
        if ($delete) {
            $ok = true;
            $msg = 'Successfully deleted the Configuration ';
        } else
            $msg = 'Failed to delete Configuration';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        
        redirect($this->ctl);
    }

    /**
     * Halaman edit group_type
     * @param int $id
     */
    public function edit($id = null) {
        parent::edit($id);

        $this->model->show_all(true);

        if (isset($id))
            $data = $this->model->getDetailConfiguration($id);
        else
            $data = array();
        $this->data['data'] = $data;

        $this->data['a_type'] = array('image' => 'Image', 'integer' => 'Integer', 'string' => 'String');
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat konfigurasi baru
     */
    public function create() {
        $data = $this->postData;

        $insert = $this->model->create($data, false, true);
        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Successfully added Configuration');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to add Configuration';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/add');
        }

    }

    /**
     * Edit data tipe
     * @param int $id jika tidak ada dianggap gagal
     */
    public function update($id = null) {
        $data = $this->postData;
        if ($_FILES['upload']['error'] == UPLOAD_ERR_OK)
            $data['value'] = $_FILES['upload']['name'];
        else
            unset($_FILES['upload']);
        if($id){
            $update = $this->model->save($id, $data);
            if ($update === true) {
                $ok = true;
                $msg = 'Successfully changed Configuration';
            } else {
                $ok = false;
                if (!is_string($update)) {
                    $validation = $this->model->getErrorValidate();
                    if (empty($validation))
                        $msg = 'Failed to change Configuration';
                    else
                        $msg = implode('<br />', $validation);
                } else
                    $msg = $update;
            } 
        }else{
            $msg = 'Failed to change Configuration';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl . '/edit/' . $id);
    }
}

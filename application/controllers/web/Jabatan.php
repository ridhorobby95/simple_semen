<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Jabatan extends WEB_Controller {

    protected $title = 'Position';

    public function index() {
        // $condition = "where \"jabatan_id\" is not null";
        // if ($_GET){
        //     $param = strtoupper($this->input->get('search', true));
        //     $condition .= " and (upper(\"jabatan_name\") like '%$param%' or upper(\"jabatan_id\") like '%$param%' ) ";
        //     if ($_GET['company'] and $_GET['company']!='0'){
        //         $company = $this->input->get('company', true);
        //         $condition .= " and \"company\"='".ltrim($company,'0')."' ";
        //     }
        // }

        // $this->load->model("User_model");
        // $users = $this->User_model->getAllUsersWithRole(TRUE, $condition);
        // $data = array();
        // foreach ($users as $key => $value) {
        //     $data[$key] = $this->model->getBy($value['jabatan_id']);
        //     $data[$key]['company'] = $value['company'];
        // }

        $condition = "where j.id is not null";
        if ($_GET){
            $param = strtoupper($this->input->get('search', true));
            $condition .= " and (upper(j.name) like '%$param%' or upper(j.id) like '%$param%' ) ";
            if ($_GET['company'] and $_GET['company']!='0'){
                $company = $this->input->get('company', true);
                $condition .= " and u.company='".ltrim($company,'0')."' ";
            }
        }
        // limit
        $condition .= " and rownum<51";
        $data = $this->model->table("jabatan j")
                            ->with("left join unitkerja u on u.id=j.unitkerja_id")
                            ->column(array(
                                "j.id" => "\"id\"",
                                "j.name" => "\"name\"",
                                "j.is_chief" => "\"is_chief\"",
                                "j.unitkerja_id" => "\"unitkerja_id\"",
                                "j.subgroup_id" => "\"subgroup_id\"",
                                "j.subgroup_name" => "\"subgroup_name\"",
                                "to_char(j.start_date, 'dd/mm/yyyy')" =>"\"start_date\"",
                                "to_char(j.end_date, 'dd/mm/yyyy')" => "\"end_date\""
                            ))->filter($condition)
                            ->order("j.id")
                            ->show_sql(false)->getAllFilter();

        $this->data['data']   =$data;
        $this->data['title'] = 'Position List';

        
        
        // tombol
        $buttons = array();
        if (SessionManagerWeb::isAdministrator() or SessionManagerWeb::isAdminUnit()){
            $buttons[] = array('label' => ' Import Excel', 'type' => 'secondary', 'icon' => 'file-excel-o', 'click' => 'goExport()');
            $buttons[] = array('label' => 'Add Position', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        }
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Fungsi Delete user
     */
    public function delete($id) {
        $ok = false;
        $data['status'] = Status::VOID;
        $update = $this->model->update($id, $data, TRUE);
        if ($update) {
            $ok = true;
            $msg = 'Successfully deleted user';
        } else
            $msg = 'Failed to delete user';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        
        redirect($this->ctl);
    }

    /**
     * Halaman profil user
     */
    public function me() {
        // tombol
        $buttons = array();
        $buttons['save'] = array('label' => 'Save', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Edit Profile';

        $this->edit(SessionManagerWeb::getUserID());
    }

    /**
     * Halaman tambah data
     * @param int $userid
     */
    public function add($userid = null) {
        parent::add();
    }

    /**
     * Halaman edit user
     * @param int $id
     */
    public function edit($id = null) {

        $this->data['title'] = $this->data['title'];

        parent::edit($id);
        
        if (isset($id)) {
            $data = $this->model->getBy($id);
            if ($data['is_chief']=='X')
                $data['is_chief']="Yes";
            else {
                $data['is_chief']='No';
            }
            $this->load->model("User_model");
            $data['company'] = $this->User_model->filter("where \"jabatan_id\"='$id' ")->getOneFilter('company');
            if (SessionManagerWeb::isAdminUnit()){
                if (SessionManagerWeb::getVariablesIndex('mycompany')!=$data['company']){
                    SessionManagerWeb::setFlashMsg(false, "Cannot edit this position! You only can edit position in your company!");
                    redirect("web/jabatan");
                }
            }
        }
        else{
            $data = array();
        }

        $this->data['data'] = $data;

        // Eselon Jabatan
        $groups = $this->model->column(array("subgroup_id"=>"\"eselon_id\"", "subgroup_name" =>"\"eselon_name\""))->group_by("group by subgroup_id, subgroup_name")->order("subgroup_id nulls first")->getAllFilter();
        foreach ($groups as $group) {
            if ($group['eselon_name']!=NULL and $group['eselon_id']!=NULL){
                $this->data['groups'][$group['eselon_id']] = $group['eselon_name'];
            }
        }
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create() {
        $data = $this->input->post(null, true);

        // cek data sudah ada atau belum
        $is_exist = $this->model->getOne($data['id'], '1');
        if ($is_exist) {
            SessionManagerWeb::setFlashMsg(false, $data['id'].' already exist. Please Fill with Another Code.');
            redirect($this->ctl . '/add/');
        }

        // cek unitkerjanya ada atau tidak
        $this->load->model("Unitkerja_model");
        $is_unitkerja_exist = $this->Unitkerja_model->getOne($data['unitkerja_id'], '1');
        if (!$is_unitkerja_exist) {
            SessionManagerWeb::setFlashMsg(false, "Work Unit ".$data['unitkerja_id'].' is not exist. Please Fill with Existed Work Unit Code.');
            redirect($this->ctl . '/add/');
        }


        // Eselon Jabatan
        $groups = $this->model->column(array("subgroup_id"=>"\"eselon_id\"", "subgroup_name" =>"\"eselon_name\""))->group_by("group by subgroup_id, subgroup_name")->order("subgroup_id nulls first")->getAllFilter();
        foreach ($groups as $group) {
            if ($group['eselon_name']!=NULL and $group['eselon_id']!=NULL and $group['eselon_id']==$data['subgroup_id']){
                $data['subgroup_name'] = $group['eselon_name'];
                break;
            }
            
        }
    
        $data['start_date'] = "to_date('".date('d/m/Y')."', 'dd/mm/yyyy')";
        if ($data['end_date']==NULL or $data['end_date']==''){
            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
        } else {
            $data['end_date'] = "to_date('".$data['end_date']."','dd/mm/yyyy')";
        }
        $insert = dbInsert('Jabatan', $data);
        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Successfully added user');
            $this->redirect();
        } else {
            SessionManagerWeb::setFlashMsg(false, 'Failed to add Position');
            redirect($this->ctl . '/add');
        }

    }

    /**
     * Edit data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
    public function update($id = null) {
        SessionManagerWeb::setFlashMsg(false, "Sorry, this feature is still unavailable");
        redirect($this->ctl.'/edit/'.$id);
        die("fitur ini sedang dimatikan");
        $data = $this->postData;
        $isself = (empty($id) ? true : false);

        if ($isself) {
            $id = SessionManagerWeb::getUserID();
            $data['username'] = SessionManagerWeb::getUserName();
            unset($data['status']);
        }
        if ($data['karyawan_id']!=NULL) {
            $data['karyawan_id'] = ltrim($data['karyawan_id'], '0');
        }
        if ($_FILES['photo']['error'] == UPLOAD_ERR_OK)
            $data['photo'] = $_FILES['photo']['name'];
        else
            unset($_FILES['photo']);
        $update = $this->model->save($id, $data, TRUE);

        if ($update === true) {
            $ok = true;
            $msg = 'Successfully changed profile';
                
            if(SessionManagerWeb::getUserID() == $id){
                // sekalian foto
                $user = $this->model->get($id);
                SessionManagerWeb::setPhoto($user);
            }

        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change profile';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        if ($isself)
            redirect($this->ctl . '/me');
        else
            redirect($this->ctl . '/edit/' . $id);
    }

    // reset all thumbnail to watermark
    public function resetPPThumbnail(){
        $users = $this->model->getAllUsersWithRole(FALSE);
        $suc = false;
        foreach ($users as $user) {
            if($this->_watermark($user['id'])){
                $suc = true;
            }
        }
        var_dump($suc);
    }

    public function ajaxGetPosition(){
        $id = $this->input->post("id", true);
        $position = $this->model->getBy($id);
        $var['position'] = $position['name'];

        // Unitkejra
        $this->load->model("Unitkerja_model");
        $unitkerja = $this->Unitkerja_model->getBy($position['unitkerja_id']);
        $var['unitkerja'] = $unitkerja['name'];

        // Company
        $var['company'] = $this->Unitkerja_model->getOne(str_pad($unitkerja['company'], 8,'0', STR_PAD_LEFT),'name');

        echo json_encode($var);
    }

    /**
     * Halaman export
     * @param int $userid
     */
    public function add_import() {
        parent::add_import();
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);  
        $this->template->viewDefault($this->view, $this->data);
    }

    public function uploadMetadata(){
        $this->metadataform('masters/jabatan');
    }

    public function import(){
        $this->load->model("Unitkerja_model");

        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'/masters/jabatan/'.$id)) {
           $files = glob($path_source.'/masters/jabatan/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."/masters/jabatan/".$id."/".$metadata;

        if (!file_exists($file)){
            SessionManagerWeb::setFlashMsg(false, 'Failed to import. Metadata not exist');
            redirect('/web/jabatan');
        }

        $this->load->library('Libexcel');

        $tmpfname = $file;
        
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $eselons = array(10,12,15,20,30,40,50,51,52,70);

        $column = array();
        $success = array();
        $failed = array();
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data = array();
            $insert_other = array();
            for ($col = 0 ; $col < $worksheetData[0]['totalColumns']; $col++){
                $colString = PHPExcel_Cell::stringFromColumnIndex($col);
                $value =  $worksheet->getCell($colString.$row)->getValue();

                switch($col){
                    case 0:
                        $data['id'] = $value;
                    break;
                    case 1:
                        $data['name'] = "{$value}";
                    break;
                    case 2:
                        $data['is_chief'] = "{$value}";
                    break;
                    case 3:
                        $data['unitkerja_id'] = $value;
                    break;
                    case 4:
                        $data['subgroup_id'] = "{$value}"; 
                    break;
                    case 5:
                        $data['start_date'] = "to_date('".$value."', 'dd/mm/yyyy')";
                    break;
                    case 6:
                        $data['end_date'] = "to_date('".$value."', 'dd/mm/yyyy')";
                        if ($value=='' or $value==NULL){
                            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
                        }
                        
                    break;
                }
            }
            if ($data['id']==NULL or $data['id']==''){
                break;
            }

            if (SessionManagerWeb::isAdminUnit()){
                $company = ltrim($this->model->filter("where \"jabatan_id\"='".$data['id']."' ")->getOneFilter('company'));
                if ($company!=SessionManagerWeb::getVariablesIndex('mycompany')){
                    $failed[] = array("id"=>$data['id'], "reason"=>"Not Allowed : you cannot import the position from $company");
                    continue;
                }
            }

            $is_exist = $this->model->getOne($data['id'], '1');
            if (!$is_exist){
                // Check Metadata
                // cek id
                if (strlen($data['id'])!=8){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Id not 8 Digit");
                    continue;
                } else {
                    $data['id'] = "{$data['id']}";
                }

                // nama
                if ($data['name']==NULL or $data['name']==''){
                     $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Empty Name");
                     continue;
                } 

                // subgroup
                if (!in_array($data["subgroup_id"], $eselons)){
                     $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Eselon not Exist / Allowed");
                     continue;
                } else {
                    $data['subgroup_name'] = $this->model->filter("where subgroup_id='{$data['subgroup_id']}'")->getOneFilter('subgroup_name');
                }

                // unitkerja id
                $unitkerja_id = str_pad($data['unitkerja_id'], 8, '0', STR_PAD_LEFT);                
                $is_unitkerja_exist = $this->Unitkerja_model->filter("where id='".$unitkerja_id."' ")->getOneFilter('1');
                if (!$is_unitkerja_exist){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : WorkUnit not Exist");
                    continue;
                } else {
                    $data['unitkerja_id'] = "{$data['unitkerja_id']}";
                }

                if (dbInsert("Jabatan", $data)){
                    $success[] = array("id" => $data['id'], "reason" => "Successfully added");
                } else {
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata");
                }
                
            } else {
                // Keluarkan kalo sudah ada
                $failed[] = array("id"=>$data['id'], "reason"=>"Already Exist");
            }
            
        }
        $_SESSION[SessionManagerWeb::INDEX]['import']['jabatan'] = array(
            "success" => $success,
            "failed" => $failed
        );
        $this->data['success'] = $success;
        $this->data['failed'] = $failed;
        $buttons['download'] = array('label' => 'Download Report', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownload()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;

        $this->_deleteFiles($path_source."masters/jabatan/".$id);
        $this->template->viewDefault($this->view, $this->data);
    }

    public function downloadImportReport(){
        $filename = "import_position_report";  
        $file_ending = "xls";
        //header info for browser
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");    
        header("Content-Disposition: attachment; filename=$filename.xls");  
        header("Pragma: no-cache"); 
        header("Expires: 0");

        /*******Start of Formatting for Excel*******/   
        $sep = "\t"; 

        echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
        echo "<tr></tr>";
        echo "<tr><td colspan='2'><b>Import Jabatan</b></td></tr>";
        echo "<tr><td>Di Import Tanggal</td><td>".date("d M Y")."</td></tr>";
        echo "<tr><td>Di Import Oleh</td><td colspan=3>".SessionManagerWeb::getName()."</td></tr>";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header = "<tr>
                <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Position Code</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema='';
        foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['jabatan']['failed'] as $fail) {
            $schema.= "<tr>
                <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
                    {$fail['id']}
                </td>{$sep}
                <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
                    {$fail['reason']}
                </td>{$sep}
            </tr>";
        }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header ="<tr>
                <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Position Code</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema = '';
        foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['jabatan']['success'] as $s) {
            $schema .= "<tr>
                <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
                    {$s['id']}
                </td>{$sep}
                <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
                    {$s['reason']}
                </td>{$sep}
            </tr>";
        }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo '</table>';
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
    }


    public function ajaxGetJabatan($is_check_exist=0){
        $this->load->model("Unitkerja_model");

        $id = $this->input->post("id", true);
        $jabatan = $this->model->getBy($id);
        if ($jabatan==null or $jabatan==false){
            $jabatan='0';
        } else {
            $jabatan['unitkerja_name'] = $this->Unitkerja_model->getOne($jabatan['unitkerja_id'], 'name');
        }

        if (!$is_check_exist){
            echo json_encode($jabatan);
            die();
        } else {
            if ($jabatan!='0'){
                echo '1';
            } else {
                echo json_encode($jabatan);
            }
        }
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller Thumb
 * @author Sevima
 * @version 1.0
 */
class Thumb extends WEB_Controller {
	protected $ispublic = true;
	
	/**
	 * Watermarking dengan gambar pangkat
	 * @param int $userid
	 * @param int $size
	 */
	public function watermark($userid, $size=null) {
		// ambil user
		ini_set('display_errors','On');
		$this->load->model('User_model');
		
		$this->User_model->show_all(true); // biar muncul yang privat
		$user = $this->User_model->get($userid);
		
		// ambil image thumbnail
		$CI = & get_instance();
		$config = $CI->config->item('utils');
		$thumb = $user['photo'][strtolower(Image::IMAGE_THUMB)];
		$image = new Imagick();
		if ($thumb!=NULL) {
			//echo $config['upload_dir'].'users/photos/'.$thumb['name'];
			$image->readImage($config['full_upload_dir'].'users/photos/'.$thumb['name']);
		} else {
			$image->readImage(base_url('assets/web/img/nopic.png'));
		}

		$iWidth = $image->getImageWidth();
		$iHeight = $image->getImageHeight();
		
		if($iWidth != $iHeight) {
			if($iWidth > $iHeight)
				$iWidth = $iHeight;
			else
				$iHeight = $iWidth;
			
			$image->cropImage($iWidth, $iHeight, 0, 0);
		}
		
		if(!empty($size)) {
			$iWidth = $iHeight = $size;
			
			$image->scaleImage($iWidth, $iHeight);
		}

		// jika ada pangkatnya
		if(!empty($user['rank'])) {
			$image->setImageFormat('png');
			$image->setImageOpacity(0.75);
			
			// proses image pangkat
			$ranks = $this->User_model->getRank();
			
			$watermark = new Imagick();
			$watermark->readImage($config['full_upload_dir'].'pangkat/'.strtolower($ranks[$user['rank']]).'.png');
			
			$wWidth = $watermark->getImageWidth();
			$nWidth = $wWidth * (($iWidth-($iWidth/16)) / 60);
			
			$watermark->scaleImage($nWidth, 0);
			
			$wWidth = $watermark->getImageWidth();
			$wHeight = $watermark->getImageHeight();
			
			// $watermark->evaluateImage(Imagick::EVALUATE_DIVIDE, 2, Imagick::CHANNEL_ALPHA);
			
			// buat image composite
			/* $x = ($iWidth - $wWidth) / 2;
			$y = ($iHeight - $wHeight) / 2; */
			$x = $iWidth / 32;
			$y = $iHeight - $wHeight - ($iHeight / 32);
			
			$image->compositeImage($watermark, imagick::COMPOSITE_OVER, $x, $y);
		}
		header('Content-type: '.$thumb['mime']);
		echo $image;
	}

	// public function files($name) {

	public function files($encryption) {
		$sql = "select document_id from document_hash where encryption='$encryption'";
		
		$document_id = dbGetOne($sql);
		$this->load->model('Document_model');
		$name = $this->Document_model->show_sql(false)->filter(" where id='$document_id' ")->getOneBy('document');

		if ($name=='' || $name==NULL){
			echo "This directory is restricted.";
			die();
		}

		$CI = & get_instance();
		$config = $CI->config->item('utils');
		$exts = explode('.', $name);
		$filename='';
		foreach ($exts as $key => $value) {
			if ($key<count($exts)-1) {
				$filename.=$value;
			} 
			
		}
		$filename .= '.backup';
		$folder = "document/files";
		$file = $config['full_upload_dir'] . $folder . '/' . $name;

		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			die('File not exist.');
		header('Content-type: '. Image::getMime(strtolower($name)));
		$image = file_get_contents($file);
		echo $image;
	}

	public function files_watermark($encryption) {
		$sql = "select document_name from document_watermarked_hash where encryption='$encryption'";
		$name = dbGetOne($sql);

		if ($name=='' || $name==NULL){
			echo "This directory is restricted.";
			die();
		}

		$CI = & get_instance();
		$config = $CI->config->item('utils');
		// $exts = explode('.', $name);
		// $filename='';
		// foreach ($exts as $key => $value) {
		// 	if ($key<count($exts)-1) {
		// 		$filename.=$value;
		// 	} 
			
		// }
		$filename .= '.backup';
		$folder = "document/files";
		$file = $config['full_upload_dir'] . $folder . '/' . $name;

		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			die('File not exist.');
		header('Content-type: '. Image::getMime(strtolower($name)));
		$image = file_get_contents($file);
		echo $image;
	}

	public function files_backup($hash) {
		// if (end(explode('.', $name))=='pdf'){
		// 	$is_authenticated = SessionManagerWeb::isAuthenticated();
		// 	$user_id = SessionManagerWeb::getUserID();
		// }
		// } else {
		// 	$is_authenticated = $_GET['is_authenticated'];
		// 	$user_id = $_GET['user_id'];
		// }

		// if ($name=='' or ! $is_authenticated){
		// 	echo "This directory is restricted.";
		// 	die();
		// }
		$this->load->model('Document_model');
		$name = $this->Document_model->filter(" where hash='$hash' ")->getOneBy('document');
		
		if ($name=='' || $name==NULL){
			echo "This directory is restricted.";
			die();
		}

		if (isset($_SESSION['dms_semen_indonesia'][$name])){
			header('Content-type: '. Image::getMime($name));
			echo $_SESSION['dms_semen_indonesia'][$name];
			// die('test');
			die();
		}

		$CI = & get_instance();
		$config = $CI->config->item('utils');
		$exts = explode('.', $name);
		$filename='';
		foreach ($exts as $key => $value) {
			if ($key<count($exts)-1) {
				$filename.=$value;
			} 
			
		}
		$filename .= '.backup';
		$folder = "document/files";
		// $file = $config['full_upload_dir'] . $folder . '/' . $filename;
		$file = $config['full_upload_dir'] . $folder . '/' . $name;


		// Check file udh dibuka apa belum
		// $url_encrypted = hash_hmac('sha256',$config['full_upload_dir'] . $folder . '/' .$name, $user_id);
		// $sql ="select 1 from 
  //   			document_urls where url_encrypted='$url_encrypted' and is_expired='0' ";
		// if (!dbGetOne($sql)){
		// 	echo "This directory is restricted.";
		// 	die();
		// } else {
		// 	$urls['is_expired']=1;
		// 	dbUpdate('document_urls', $urls, "url_encrypted='$url_encrypted'");
		// }

		$glob = glob($file . '*');
		$file = $glob[0];
		if (!file_exists($file) || is_dir($file))
			die('File not exist.');
		header('Content-type: '. Image::getMime(strtolower($name)));
		$image = file_get_contents($file);
		// $image = readfile($file);
		$_SESSION['dms_semen_indonesia'][$name] = $image;
		echo $_SESSION['dms_semen_indonesia'][$name];
		// echo $image;
	}

}
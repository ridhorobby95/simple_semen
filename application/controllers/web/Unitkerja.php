<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class Unitkerja extends WEB_Controller {

    protected $title = 'Work Unit';

    // public function getCompany(){
    //     $company[1000]
    // }

    public function index() {
        $_SESSION['is_company'] = 0;
        if ($_GET['search']!='') {
            $param = strtoupper($this->input->get('search', true));
            $condition = "and (upper(name) like '%$param%' or upper(id) like '%$param%' or upper(\"LEVEL\") like '%$param%' ) ";
        }
        if ($_GET['company'] and $_GET['company']!='0'){
            $company = $this->input->get('company', true);
            $condition .= " and company='".ltrim($company,'0')."' ";
        }

        $condition .=" and upper(\"LEVEL\")!='COMP' ";
        
        $data = $this->model->getAll($condition);
        $this->data['data']   =$data;
        $this->data['title'] = 'Work Unit List';



        // tombol
        $buttons = array();
        if (SessionManagerWeb::isAdminUnit() or SessionManagerWeb::isAdministrator()){
            $buttons[] = array('label' => ' Import Excel', 'type' => 'secondary', 'icon' => 'file-excel-o', 'click' => 'goExport()');
            $buttons[] = array('label' => 'Add Work Unit', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        }
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
        $this->template->viewDefault($this->view, $this->data);
    }

    public function company(){
        $_SESSION['is_company'] = 1;
        $condition = "";
        if ($_GET['search']!='') {
            $param = strtoupper($this->input->get('search', true));
            $condition = "and (upper(name) like '%$param%' or upper(id) like '%$param%' or upper(\"LEVEL\") like '%$param%' )";
        }
        $condition .=" and parent='0'";

        $data = $this->model->show_sql(false)->getAll($condition);
        $this->data['data']   =$data;
        $this->data['title'] = 'Company List';

        // tombol
        $buttons = array();
        $buttons[] = array('label' => 'Add Company', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd(1)');
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;
        
        $this->template->viewDefault($this->class.'_index', $this->data);
    }

    /**
     * Fungsi Delete user
     */
    public function delete($id) {
        $ok = false;
        $data['status'] = Status::VOID;
        $update = $this->model->update($id, $data, TRUE);
        if ($update) {
            $ok = true;
            $msg = 'Successfully deleted user';
        } else
            $msg = 'Failed to delete user';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        
        redirect($this->ctl);
    }

    /**
     * Halaman profil user
     */
    public function me() {
        // tombol
        $buttons = array();
        $buttons['save'] = array('label' => 'Save', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Edit Profile';

        $this->edit(SessionManagerWeb::getUserID());
    }

    /**
     * Halaman tambah data
     * @param int $userid
     */
    public function add() {
        
        if ($_SESSION['is_company']==1){
            $buttons = array();
            $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack('company')");
            $this->data['buttons'] = $buttons;
        }
        parent::add();

    }

    /**
     * Halaman export
     * @param int $userid
     */
    public function add_import() {
        parent::add_import();
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
        $this->template->viewDefault($this->view, $this->data);
    }

    public function uploadMetadata(){
        $this->metadataform('masters/unitkerja');
    }

    public function downloadTemplate(){

    }

    public function import(){
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'/masters/unitkerja/'.$id)) {
           $files = glob($path_source.'/masters/unitkerja/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."/masters/unitkerja/".$id."/".$metadata;

        if (!file_exists($file)){
            SessionManagerWeb::setFlashMsg(false, 'Failed to import. Metadata not exist');
            redirect('/web/unitkerja');
        }

        $this->load->library('Libexcel');

        $tmpfname = $file;
        
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $levels = array("KOMP", "DIR", "DEPT", "BIRO", "SECT", "GRP");

        $column = array();
        $success = array();
        $failed = array();
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data = array();
            $insert_other = array();
            for ($col = 0 ; $col < $worksheetData[0]['totalColumns']; $col++){
                $colString = PHPExcel_Cell::stringFromColumnIndex($col);
                $value =  $worksheet->getCell($colString.$row)->getValue();

                switch($col){
                    case 0:
                        $data['id'] = $value;
                    break;
                    case 1:
                        $data["\"LEVEL\""] = strtoupper($value);
                    break;
                    case 2:
                        $data['name'] = "{$value}";
                    break;
                    case 3:
                        $data['company'] = ltrim($value, '0');
                    break;
                    case 4:
                        $data['parent'] = $value; 
                    break;
                    case 5:
                        $data['start_date'] = "to_date('".$value."', 'dd/mm/yyyy')";
                    break;
                    case 6:
                        $data['end_date'] = "to_date('".$value."', 'dd/mm/yyyy')";
                        if ($value=='' or $value==NULL){
                            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
                        }
                        
                    break;
                }
            }
            if ($data['id']==NULL or $data['id']==''){
                break;
            }

            if (SessionManagerWeb::isAdminUnit()){
                if ($company!=SessionManagerWeb::getVariablesIndex('mycompany')){
                    $failed[] = array("id"=>$data['id'], "reason"=>"Not Allowed : you cannot import the work unit to $company");
                    continue;
                }
            }

            $is_exist = $this->model->getOne($data['id'], '1');
            if (!$is_exist){
                // Check Metadata
                // cek id
                if (strlen($data['id'])!=8){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Id not 8 Digit");
                    continue;
                } else {
                    $data['id'] = "{$data['id']}";
                    $data['short'] = $data['id'];
                }

                // cek level
                if (!in_array($data["\"LEVEL\""], $levels)){
                     $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : LEVEL not Exist / Allowed");
                     continue;
                } else {
                    $data["\"LEVEL\""] = "{$data["\"LEVEL\""]}";
                }

                // nama
                if ($data['name']==NULL or $data['name']==''){
                     $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Empty Name");
                     continue;
                }

                //company
                $comp_id = str_pad($data['company'], 8, '0', STR_PAD_LEFT);            
                $is_company_exist = $this->model->show_sql(false)->filter("where id='".$comp_id."' and \"LEVEL\"='COMP' ")->getOneFilter('1');
                if (!$is_company_exist){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Company not Exist");
                    continue;
                } else {
                    $data['company'] = "{$data['company']}";
                }

                // parent
                $parent_id = str_pad($data['parent'], 8, '0', STR_PAD_LEFT);                
                $is_parent_exist = $this->model->filter("where id='".$parent_id."' ")->getOneFilter('1');
                if (!$is_parent_exist){
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata : Upper Work Unit not Exist");
                    continue;
                } else {
                    $data['parent'] = "{$data['parent']}";
                }

                if (dbInsert("Unitkerja", $data)){
                    $success[] = array("id" => $data['id'], "reason" => "Successfully added");
                } else {
                    $failed[] = array("id"=>$data['id'], "reason"=>"False Metadata");
                }
                
            } else {
                // Keluarkan kalo sudah ada
                $failed[] = array("id"=>$data['id'], "reason"=>"Already Exist");
            }
            
        }
        // ECHO TABELNYA
        // echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
        // echo "<tr></tr>";
        // echo "<tr><td>Di Import Tanggal</td><td>".date("d M Y")."</td></tr>";
        // echo "<tr><td>Di Import Oleh</td><td colspan=3>".SessionManagerWeb::getName()."</td></tr>";
        // echo "<tr></tr><tr></tr>";

        // echo "<tr><td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td></tr>";
        // echo "<tr></tr>";
        // echo "<tr>
        //         <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Work Unit Code</td>
        //         <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>
        //     </tr>";
        // foreach ($failed as $fail) {
        //     echo "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
        //             {$fail['id']}
        //         </td>
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
        //             {$fail['reason']}
        //         </td>
        //     </tr>";
        // }
        // echo "<tr></tr><tr></tr>";

        // echo "<tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>";
        // echo "<tr></tr>";
        // echo "<tr>
        //         <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Work Unit Code</td>
        //         <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>
        //     </tr>";
        // foreach ($success as $s) {
        //     echo "<tr>
        //         <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
        //             {$s['id']}
        //         </td>
        //         <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
        //             {$s['reason']}
        //         </td>
        //     </tr>";
        // }
        // echo '</table>';
        $_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja'] = array(
            "success" => $success,
            "failed" => $failed
        );
        $this->data['success'] = $success;
        $this->data['failed'] = $failed;
        $buttons['download'] = array('label' => 'Download Report', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownload()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;

        $this->_deleteFiles($path_source."masters/unitkerja/".$id);
        $this->template->viewDefault($this->view, $this->data);
    }

    public function downloadImportReport(){
        $filename = "import_workunit_report";  
        $file_ending = "xls";
        //header info for browser
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");    
        header("Content-Disposition: attachment; filename=$filename.xls");  
        header("Pragma: no-cache"); 
        header("Expires: 0");

        /*******Start of Formatting for Excel*******/   
        $sep = "\t"; 

        echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
        echo "<tr></tr>";
        echo "<tr><td colspan='2'><b>Import Unit Kerja</b></td></tr>";
        echo "<tr><td>Di Import Tanggal</td><td>".date("d M Y")."</td></tr>";
        echo "<tr><td>Di Import Oleh</td><td colspan=3>".SessionManagerWeb::getName()."</td></tr>";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header = "<tr>
                <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Work Unit Code</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema='';
        foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']['failed'] as $fail) {
            $schema.= "<tr>
                <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
                    {$fail['id']}
                </td>{$sep}
                <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
                    {$fail['reason']}
                </td>{$sep}
            </tr>";
        }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header ="<tr>
                <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Work Unit Code</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema = '';
        foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']['success'] as $s) {
            $schema .= "<tr>
                <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
                    {$s['id']}
                </td>{$sep}
                <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
                    {$s['reason']}
                </td>{$sep}
            </tr>";
        }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo '</table>';
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
        // SessionManagerWeb::setFlashMsg(true, "Success to Download Import Report");
        // redirect('web/unitkerja');
    }

    /**
     * Halaman edit user
     * @param int $id
     */
    public function edit($id = null) {
        $this->data['title'] = $this->data['title'];
        $levels = $this->model->column(array("distinct \"LEVEL\""=>"\"level\""))->limit(100000)->order("\"LEVEL\" NULLS first")->getAll();
        $this->data['levels'] = Util::toMap($levels, 'level', 'level');

        $companies = $this->model->column(array("id"=>"\"id\"", "name"=>"\"name\""))->limit(1000)->order("name")->getAll("and \"LEVEL\"='COMP' ");
        $companies[""] = NULL;
        $this->data['companies'] = Util::toMap($companies, 'id', 'name');
        $this->data['workunits'] = array(''=>NULL);
        parent::edit($id);
        if (isset($id)) {
            $this->data['data'] = $this->model->defaultColumn()->getBy($id);
            $this->data['workunits'] = $this->model->order('name')->getAllList(" company='".$this->data['data']['company']."' ");
            unset($this->data['workunits'][0]);
            if ($this->data['data']['photo']!=NULL)
                $this->data['data']['photo'] = Image::getLocation($id, Image::IMAGE_THUMB, $this->data['data']['photo'], 'company');
            // var_dump($this->data['data']);
            // die();
            if ($this->data['data']['parent']=='0')
                $this->data['buttons']['back']['click'] = "goBack('company')";
            if (SessionManagerWeb::isAdminUnit()){
                if (SessionManagerWeb::getVariablesIndex('mycompany')!=$this->data['data']['company']){
                    SessionManagerWeb::setFlashMsg(false, "Cannot edit this work unit! You only can edit work unit in your company!");
                    redirect("web/unitkerja");
                }
            }
        }
        else
            $this->data['data'] = array("parent" => NULL);
        
        $this->template->viewDefault($this->view, $this->data);
    }

    /**
     * Membuat data baru
     */
    public function create($is_company=0) {
        $data = $this->input->post(null, true);

        // if (strlen($data['id'])!=8){
        //     SessionManagerWeb::setFlashMsg(false, 'Code must be 8 Characters');
        //     redirect($this->ctl.'/add/'.$_SESSION['is_company']);
        // }

        if ($is_company){
            if (strlen($data['id'])!=4){
                SessionManagerWeb::setFlashMsg(false, 'Code Company must be 4 Characters');
                redirect($this->ctl.'/add/'.$_SESSION['is_company']);
            }
        }
        $data['id'] = str_pad($data['id'], 8, '0', STR_PAD_LEFT);

        $is_exist = $this->model->getOne($data['id'], '1');
        if ($is_exist) {
            SessionManagerWeb::setFlashMsg(false, $data['id'].' already exist. Please Fill with Another Code.');
            redirect($this->ctl . '/add/'.$_SESSION['is_company']);
        }

        $data["\"LEVEL\""] = $data['level'];
        unset($data['level']);

        

        if ($_SESSION['is_company']){
            $data["\"LEVEL\""] = 'COMP';
            $data['parent'] = 0;
            $data['company'] = $data['id'];
        }
        $data['company'] = ltrim($data['company'], '0');
        unset($data['company_name']);
        $data["\"INITIAL\""] = $data['initial'];
        unset($data['initial']);

        $data['start_date'] = "to_date('".date('d/m/Y')."', 'dd/mm/yyyy')";
        if ($data['end_date']==NULL or $data['end_date']==''){
            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
        } else {
            $data['end_date'] = "to_date('".$data['end_date']."','dd/mm/yyyy')";
        }

        $insert = dbInsert('Unitkerja', $data);

        if ($insert) {
            SessionManagerWeb::setFlashMsg(true, 'Successfully add new Data');
            if ($_SESSION['is_company']){
                redirect($this->ctl.'/company');
            } else {
                $this->redirect();
            }
        } else {

            SessionManagerWeb::setFlashMsg(false, 'Failed to add Data');
            redirect($this->ctl . '/add/'.$_SESSION['is_company']);
        }

    }

    /**
     * Edit data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
    public function update($id) {
        $data = $this->input->post(null,true);
        unset($data['id']);

        $data["\"INITIAL\""] = $data['initial'];
        unset($data['initial']);
        $data['company'] = ltrim($data['company'], '0');

        if ($_SESSION['is_company']){
            unset($data['company']);
            $data['parent'] = 0;
        } else {
            $data["\"LEVEL\""] = $data['level'];
            unset($data['level']);
        }

        if ($data['end_date']==NULL or $data['end_date']==''){
            $data['end_date'] = "to_date('31/12/9999', 'dd/mm/yyyy')";
        } else {
            $data['end_date'] = "to_date('".$data['end_date']."','dd/mm/yyyy')";
        }

        // update
        if ($_FILES['photo']['error'] == UPLOAD_ERR_OK){
            $upload = Image::upload('photo', $id, $_FILES['photo']['name'], 'company');
            if ($upload === TRUE) {
                $data['photo'] = $_FILES['photo']['name'];
                // echo "a";
            } else {
                unset($_FILES['photo']);
            }
        }
        else
            unset($_FILES['photo']);
        $update = dbUpdate('Unitkerja', $data, "id='".$id."'");
        Image::opacityLogo($id);
        // $update = $this->model->save($id, $data, FALSE);

        if ($update) {
            SessionManagerWeb::setFlashMsg(true, 'Successfully update Data');
            if ($_SESSION['is_company']){
                redirect($this->ctl . '/edit/'.$id.'/'.$_SESSION['is_company']);
            } else {
                redirect($this->ctl . '/edit/'.$id);
            }
        } else {

            SessionManagerWeb::setFlashMsg(false, 'Failed to update Data');
            redirect($this->ctl . '/edit/'.$id.'/'.$_SESSION['is_company']);
        }
    }

    public function ajaxGetCreatorAndApprover($unitkerja_id){
        // $data = $this->model->getCreatorAndApprover($unitkerja_id);

        $this->load->model('User_model');
        $data = array();
        $atasan = SessionManagerWeb::getVariablesIndex('atasan');
        
        // set Creator
        $data['creator']['user_id'] = $atasan[$unitkerja_id];
        $data['creator']['name'] = $this->User_model->getBy($data['creator']['user_id'], 'name');

        // set Approver
        $parent = $this->model->getOne($unitkerja_id, 'parent');
        if ($parent!=NULL and $parent!='0'){
            $data['approver']['user_id'] = $atasan[$parent];
        } else {
            $data['approver']['user_id'] = $atasan[$unitkerja_id];
        }
        
        $data['approver']['name'] = $this->User_model->getBy($data['approver']['user_id'], 'name');

        echo json_encode($data);
    }

    public function ajaxGetCreator($unitkerja_id){
        // $data = $this->model->getCreatorAndApprover($unitkerja_id);
        $type = $this->input->post('type', true);
        $this->load->model('User_model');
        $data = array();
        $atasan = SessionManagerWeb::getVariablesIndex('atasan');
        $creators = SessionManagerWeb::getVariablesIndex('creator');
        $user_is_creator = false;
        foreach ($creators[$unitkerja_id][$type] as $key => $value) {
            if(SessionManagerWeb::getUserID() == $key){
                $user_is_creator = true;
                break;
            }
        }
        // echo "<pre>";print_r($creators[$unitkerja_id][$type]);die();
        // echo "<pre>";print_r($atasan);die();
        // set Creator

    if((SessionManagerWeb::getEselon() == 'Eselon 1' || SessionManagerWeb::getEselon() == 'Eselon 2' || SessionManagerWeb::getEselon() == 'Eselon 3') && $user_is_creator == true){ // Jika dia minim eselon 3 dan bisa jadi creator, maka nama pertama adalah nama dia
        $data['creator']['user_id'] = SessionManagerWeb::getUserID();
    }
    else{
        $data['creator']['user_id'] = $atasan[$unitkerja_id];
        if ($atasan[$unitkerja_id]==NULL){
            $data['creator']['user_id'] = '';
        }
    }
        
        // if ($data['creator']['user_id']==null){
        //     $this->load->model('Atasan_model');
        //     $user_id = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'karyawan_id');
        //     $atasan1 = $this->Atasan_model->filter(" where k_nopeg='$user_id' ")->getOne("atasan1_nopeg");
        //     $data['creator']['user_id'] = $this->User_model->filter(" where karyawan_id='$atasan1' ")->getOneFilter('id');
        // }
        $data['creator']['name'] = $this->User_model->getBy($data['creator']['user_id'], 'name');

        // if ($data['creator']['user_id']!=NULL)
            echo json_encode($data);
    }


    public function ajaxGetUserCreator($unitkerja_id){
        $this->load->model('User_model');
        $users = $this->User_model->columns(array("\"user_id\"" => "\"id\"", "\"name\"" => "\"text\""))->getAll(" where \"is_active\"=1 and \"unitkerja_id\"='$unitkerja_id' ");
        // echo "<pre>";print_r($users);die();
        if($users == false){
            echo "null";
        }else{
            // echo "tidak";
            echo json_encode($users);
        }
        // die();
        
    }
    public function ajaxGetListCreator($unitkerja_id){

        $type = $this->input->post('type', true);
        // $type = 4;

        $data = array();
        $creators = SessionManagerWeb::getVariablesIndex('creator');
        // echo "<pre>";print_r($creators);die();
        // set Creator
        $cek = array();
        $no = 0;
        foreach ($creators[$unitkerja_id][$type] as $key => $value) {
            $data[$no]['id'] =$key;
            $data[$no]['text'] = $value;
            $cek[$no] = $key;
            $no++;
        }

        // getPgs
        $this->load->model('Atasan_model');
        $this->load->model('User_model');
        // $nopeg = $this->User_model->filter("where \"user_id\"='".SessionManagerWeb::getUserID()."'")->getOneFilter("karyawan_id");
        // $atasan = $this->Atasan_model->filter(" where k_nopeg='".$nopeg."' and (atasan1_unitkerja='$unitkerja_id' or atasan2_unitkerja='$unitkerja_id') ")->getAll();
        // $temp = array();
        // foreach ($atasan as $key => $value) {
        //     $id = $this->User_model->filter("where \"karyawan_id\"='".$value['atasan1_nopeg']."'")->getOneFilter("user_id");
        //     if (!in_array($id, $cek)){
        //         $name = $this->User_model->filter("where \"user_id\"='".$id."'")->getOneFilter("name");
        //         $data[$no]['id'] = $id;
        //         $data[$no]['text'] = $name;
        //         $temp[] = $id;
        //         $no++;
        //     }
        // }

        // tambah atasan
        $this->load->model("Unitkerja_model");
        $atasan_nopeg = $this->Unitkerja_model->getAtasanByLevel($unitkerja_id, SessionManagerWeb::getUserID());
        if (!in_array($atasan_nopeg, $cek)){
            $name = $this->User_model->filter("where \"user_id\"='".$atasan_nopeg."'")->getOneFilter("name");
            $data[$no]['id'] = $atasan_nopeg;
            if ($data[$no]['id']==NULL){
                $data[$no]['id'] = '';
            }
            $data[$no]['text'] = $name;
        }
        
        echo json_encode($data);
    }

    public function ajaxGetApprover2(){
        // $data = $this->model->getCreatorAndApprover($unitkerja_id);
        $this->load->model('User_model');
        $data = array();
        $mycompany = SessionManagerWeb::getVariablesIndex('mycompany');
        $atasan = SessionManagerWeb::getVariablesIndex('atasan');
        $creator_id = $this->input->post('creator_id');
        $unitkerja_id = $this->User_model->getBy($creator_id, 'unitkerja_id');
        $type_id = $this->input->post('type_id', null);

        // set Approver
        if ($this->User_model->getBy($creator_id, 'is_chief')=='X'){
            $parent = $this->model->getOne($unitkerja_id, 'parent');
            if ($parent!=NULL and $parent!='0'){
                $data['approver'][0]['id'] = $atasan[$parent];
                // die(var_dump($this->model->getBy($parent)));
            } else {
                $data['approver'][0]['id'] = $atasan[$unitkerja_id];
                // die(var_dump($this->model->getBy($unitkerja_id)));
            }
        } else {
            $data['approver'][0]['id'] = $atasan[$unitkerja_id];
        }
        // if ($type_id=='2'){
        //     die('test');
        // }
        
        $data['approver'][0]['text'] = $this->User_model->getBy($data['approver'][0]['id'], 'name');
        if ($type_id=='2'){
            $data['approver'][1]['id'] = $atasan[$mycompany];
            $data['approver'][1]['text'] = $this->User_model->getBy($data['approver'][1]['id'], 'name');
        }

        if ($data['approver'][0]['id']==null){
            $data = array();
        }

        echo json_encode($data);
    }

    public function getAtasan(){
        $this->load->model('Atasan_model');
        $atasan = $this->Atasan_model->filter("where rownum<21 and atasan1_pgs='X' or atasan2_pgs='X'")->getAll();
        echo '<pre>';
        vaR_dump($atasan);
        die();
    }

    public function getDirut($company='2000'){
        // if ($company=='2000'){
            $sql = "select id from unitkerja where lower(name) like '%president directorate%' and company='$company' and \"LEVEL\"='DIR' and sysdate < end_date order by id desc";
            $unitkerja_dirut = dbGetOne($sql);
            $sql_user = " select \"user_id\" from users_view where \"unitkerja_id\"='$unitkerja_dirut' and \"is_chief\"='X' and \"is_active\"=1 ";
            $dirut_id = dbGetOne($sql_user);
            return $dirut_id;
        // }
        // return false;
    }

    public function ajaxGetApprover($mycompany=null){
        $this->load->model('User_model');
        $this->load->model('Atasan_model');
        $data = array();
        $creator_id = $this->input->post('creator_id');
        $creator_nopeg = $this->User_model->getBy($creator_id, 'karyawan_id');
        $type_id = $this->input->post('type_id', null);
        if($mycompany == null){
            $mycompany = SessionManagerWeb::getVariablesIndex('mycompany');
        }

        // $atasan = $this->Atasan_model->filter(" where k_nopeg='$creator_nopeg' and (atasan1_pgs is null and atasan2_pgs is null) ")->getBy();
        $atasan = $this->Atasan_model->filter(" where k_nopeg='$creator_nopeg' ")->getBy();
        // echo "<pre>";print_r($atasan);die();
        $atasan1_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan1_nopeg']."' ")->getOneFilter('user_id');
        $atasan2_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan2_nopeg']."' ")->getOneFilter('user_id');

        // set Approver
        // if ($this->User_model->getBy($creator_id, 'is_chief')=='X'){
        //     $data['approver'][0]['id'] = $atasan1_id;
        // } else {
        //     $data['approver'][0]['id'] = $atasan[$unitkerja_id];
        // }
        $data['approver'][0]['id'] = $atasan1_id;
        // if ($atasan['atasan1_pgs']=='X'){
        //     $data['approver'][0]['id'] = $atasan2_id;
        // }

        if (SessionManagerWeb::getUserID()==$creator_id){
            $data['approver'][0]['id'] = $atasan1_id;
        }
        
        
        $data['approver'][0]['text'] = $this->User_model->getBy($data['approver'][0]['id'], 'name');
        if ($type_id=='2'){
            // $data['approver'][1]['id'] = $atasan[$mycompany];
            $data['approver'][1]['id'] = $this->getDirut($mycompany);
            $data['approver'][1]['text'] = $this->User_model->getBy($data['approver'][1]['id'], 'name');
            if ($data['approver'][0]['id']==$data['approver'][1]['id']){
                unset($data['approver'][1]);
            }
            // else {
            //     if ($data['approver'][0]['id']==null){
            //         $data['approver'][0] = $data['approver'][1];
            //         unset($data['approver'][1]); 
            //     }
            // }
            
        }

        if ($data['approver'][0]['id']==null){
            if ($data['approver'][1]['id']==null){
                $data = array();
            } else {
                $data['approver'][0] = $data['approver'][1];
                unset($data['approver'][1]); 
            }
            
        }
        // echo "<pre>";print_r($data);die();
        echo json_encode($data);
    }

    public function ajaxGetAtasan($unitkerja_id){
        $data = $this->model->getAtasan($unitkerja_id);
        echo json_encode($data);
    }

    public function getTop($unitkerja){
        $sql ="select id as \"id\" from unitkerja where parent='$unitkerja' and id!='sevima'";
        echo'<pre>';
        var_dump(dbGetone($sql));
        die();
    }

    public function ajaxSetCompany(){
        $company = $this->input->post('company', true);
        unset($_SESSION['page_evaluation']);
        unset($_SESSION['max_evaluation']);
        return SessionManagerWeb::setCompany($company);
    }

    public function ajaxGetUnitkerjaName($is_company=false){
        $id = $this->input->post('id', true);
        if ($is_company){
            $filter_company = "and \"LEVEL\"='COMP' ";
        }
        $name = $this->model->filter($filter_company)->getOne($id,'name');
        echo '"'.$name.'"';
    }

    public function ajaxGetListUnitkerja(){
        $company_id = $this->input->post('company_id', true);
        $filter_company = " company='".ltrim($company_id,'0')."' ";
        $arr_workunits = $this->model->order('name')->getAllList($filter_company);
        unset($arr_workunits[0]);
        $workunits = array();
        $no=0;
        foreach ($arr_workunits as $key => $value) {
            $workunits[$no]['id'] = $key;
            $workunits[$no]['text'] = $value;
            $no++;
        }

        echo json_encode($workunits);
    }

    public function tesKode(){
        $company_id = '0000C0001';
        $filter_company = " company='".ltrim($company_id,'0')."' ";
        $arr_workunits = $this->model->show_sql('true')->order('name')->getAllList($filter_company);
        // unset($arr_workunits[0]);
        // $workunits = array();
        // $no=0;
        // foreach ($arr_workunits as $key => $value) {
        //     $workunits[$no]['id'] = $key;
        //     $workunits[$no]['text'] = $value;
        //     $no++;
        // }
    }
}

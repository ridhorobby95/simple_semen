<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Tingkatan_document_types extends WEB_Controller {

    protected $title = 'Filing of Document Type';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
        $tingkatan_document_types = $this->model->getAll();
        $this->load->model('Document_types_model');
        $data = array();
        foreach ($tingkatan_document_types as $key => $tingkatan) {
            $document_types = $this->model->getDocumentTypesByTingkatan($key);
            $data[$key]['str_types_id'] = $document_types['types_id'];
            $data[$key]['str_types_name'] = $document_types['types_name'];
        }
        
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    function add($name) {
        $name = urldecode($name);
        $this->model->add($name);
        redirect($this->ctl );        
    }

    public function edit($id=NULL){
    	if (isset($id))
            $data = $this->model->getDocumentType($id);
        else
            $data = array();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function delete($id) {
        $delete = $this->model->delete($id);

        if ($delete === true) {
            $ok = true;
            $msg = 'Successfully deleted the document type';
        } else {
            $ok = false;
            $msg = 'Failed to delete document type';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl);
    }
    
    public function update($id = null) {
        $data = $this->postData;
        $update = $this->model->save($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Successfully changed Document Type';

        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change Document Type';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl );
    }
}

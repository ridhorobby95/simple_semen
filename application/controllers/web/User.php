<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller User
 * @author Sevima
 * @version 1.0
 */
class User extends WEB_Controller {

    protected $title = 'User';

    public function index() {
        // if (!SessionManagerWeb::isAdministrator())
        //     redirect($this->ctl . '/me');

        // $this->data['data'] = $this->model->order_by('name')->is_role(TRUE)->get_all();
        // if ($_GET['search']=='')
        //     unset($_GET['search']);
        if ($_GET){
            // echo "<pre>";print_r($_GET);die();
            $param = strtoupper($this->input->get('search', true));
            $condition = "where (upper(\"username\") like '%$param%' or upper(\"name\") like '%$param%' or upper(\"eselon_name\") like '%$param%') ";
            if ($_GET['role'] or $_GET['role']=='0'){
                $role = $this->input->get('role', true);
                if($_GET['role']!='0'){
                    $condition .= " and \"role\"='$role'  ";
                }
                if($role != 'X'){
                    if ($_GET['company'] and $_GET['company']!='0'){
                        $company = $this->input->get('company', true);
                        $condition .= " and \"company\"='".ltrim($company,'0')."' ";
                    }
                }
            }
            else{
                $condition .= " and \"role\"='X'  ";
            }
        }
        
        $this->data['data'] = $this->model->getAllUsersWithRole(TRUE, $condition);
        // $this->load->model('Unitkerja_model');
        // $this->data['company'] = $this->Unitkerja_model->getAllList(" \"LEVEL\"='COMP' and parent='0' and id<'10000' ");
        // $this->data['company'][0] = "All Company";
        
        $this->data['role'] = array(0=>'All Role',
                                    Role::ADMINISTRATOR => "Administrator",
                                    Role::ADMIN_UNIT => "Admin Unit",
                                    Role::DOCUMENT_CONTROLLER => "Document Controller",
                                    Role::DRAFTER => "Drafter",
                                    Role::EKSTERNAL => "Eksternal"
                                    );

        $this->data['title'] = 'User List';
        // tombol
        $buttons = array();
        if (SessionManagerWeb::isAdministrator() or SessionManagerWeb::isAdminUnit()){
            $buttons[] = array('label' => ' Import Excel', 'type' => 'secondary', 'icon' => 'file-excel-o', 'click' => 'goExport()');
            $buttons[] = array('label' => 'Add User', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        }
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        $this->data['buttons'] = $buttons;
        
        $this->template->viewDefault($this->class . '_list', $this->data);
    }

    /**
     * Fungsi Delete user
     */
    public function delete($id) {
        $ok = false;
        $data['status'] = Status::VOID;
        $update = $this->model->update($id, $data, TRUE);
        if ($update) {
            $ok = true;
            $msg = 'Successfully deleted user';
        } else
            $msg = 'Failed to delete user';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        
        redirect($this->ctl);
    }

    /**
     * Fungsi reset password user
     */
    public function reset($id) {
        $ok = false;
        $user = $this->model->get_by('id', $id);
        $update = $this->model->update($id, SecurityManager::encode($user['username']), TRUE);
        if ($update) {
            $ok = true;
            $msg = 'Successfully changed password';
        } else
            $msg = 'Failed to change password';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        
        redirect($this->ctl);
    }

    /**
     * Halaman daftar teman user
     */
    public function list_me() {
        $this->data['title'] = 'Teman'; 
        $this->data['data'] = $this->model->getListFor(SessionManagerWeb::getUserID());
        // echo '<pre>';
        // var_dump($this->data['data']);
        // die();
        // ambil status hadir
        $this->data['presence'] = $this->model->getListPresence();

        $this->template->viewDefault($this->class . '_index', $this->data);
    }

    /**
     * Halaman profil user
     */
    public function me() {
        // tombol
        $buttons = array();
        $buttons['save'] = array('label' => 'Save', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Edit Profile';

        $this->edit(SessionManagerWeb::getUserID());
    }

    /**
     * Halaman tambah data
     * @param int $userid
     */
    public function add($userid = null) {
        parent::add();
    }

    /**
     * Halaman edit user
     * @param int $id
     */
    public function edit($id = null) {

        $this->data['title'] = $this->data['title'];

        // edit user lain hanya untuk admin
        if ($id != SessionManagerWeb::getUserID() and ! SessionManagerWeb::isAdministrator() and !SessionManagerWeb::isAdminUnit())
            redirect($this->ctl . '/me');

        parent::edit($id);
        
        if (isset($id)) {
            $data = $this->model->get($id);
            $this->load->model('Karyawan_model');
            $karyawan = $this->Karyawan_model->show_sql(true)->column(array('tgl_masuk' => 'tgl_masuk', 'tgl_pensiun' => 'tgl_pensiun'))->getBy($data['karyawan_id']);
            // echo "<pre>";print_r($karyawan);die();
            $data['tgl_masuk'] = $karyawan['TGL_MASUK'];
            $data['tgl_pensiun'] = $karyawan['TGL_PENSIUN'];
            $access = dbGetRow('select company, document_type from user_eksternal where user_id='.$id);
            $data['company_access'] = json_decode($access['COMPANY']);
            $data['document_type_access'] = json_decode($access['DOCUMENT_TYPE']);
            $company = ltrim($this->model->filter("where \"user_id\"='$id' ")->getOneFilter('company'), '0');
            $data['company'] = $company;
            if (SessionManagerWeb::isAdminUnit()){
                if (SessionManagerWeb::getVariablesIndex('mycompany')!=$company){
                    SessionManagerWeb::setFlashMsg(false, "Cannot edit this user! You only can edit user in your company!");
                    redirect("web/user");
                }
            }
        }
        else
            $data = array();
        if ($data['photo']!=NULL)
            $data['photo'] = Image::getLocation($data['id'], Image::IMAGE_THUMB, $data['photo'], 'users/photos');
        $this->data['data'] = $data;

        // combo box
        $this->load->model('User_model');
        // $rows = $this->User_model->order_by('name')->get_all();

        $a_user = array();
        foreach ($rows as $row)
            $a_user[$row['id']] = $row['name'];
        $this->data['a_user'] = $a_user;
        $this->data['a_status'] = Status::getArray();
        // $this->data['a_role'] = Role::name();
        $this->data['a_role'] = array(
            Role::DRAFTER => 'Drafter',
            Role::ADMINISTRATOR => 'Administrator',
            Role::ADMIN_UNIT => 'Admin Unit',            
            Role::DOCUMENT_CONTROLLER => 'Document Controller',
            Role::EKSTERNAL => 'Eksternal'
        );
        $this->load->model('Document_types_model');
        $document_types = $this->Document_types_model->order('urutan')->getAllJenis();
        $this->data['document_types'] = $document_types;
        // echo "<pre>";print_r($this->data['data']);die();
        $this->template->viewDefault($this->view, $this->data);
    }

    public function getDropdownRole(){
        return array('E' => 'Employee', 'A' => 'Administrator');
    }

    /**
     * Mengganti password user
     */
    public function change_password() {
        $this->form_validation->set_rules('oldPassword', 'Password Lama', 'required');
        $this->form_validation->set_rules('newPassword', 'Password Baru', 'required|min_length[6]');
        $this->form_validation->set_rules('confirmPassword', 'Konfirmasi Password', 'required|min_length[6]|matches[newPassword]');

        $ok = false;
        $user = $this->model->is_private(TRUE)->get(SessionManagerWeb::getUserID());
        if ($user['password'] == SecurityManager::hashPassword($this->postData['oldPassword'], $user['passwordSalt'])) {
            if ($this->form_validation->run() == TRUE) {
                $update = $this->model->update(SessionManagerWeb::getUserID(), SecurityManager::encode($this->postData['newPassword']), TRUE);
                if ($update) {
                    $ok = true;
                    $msg = 'Successfully changed password';
                } else
                    $msg = 'Failed to change password';
            }
            else {
                $validation = $this->model->getErrorManualValidation(array('oldPassword', 'newPassword', 'confirmPassword'));
                $msg = implode('<br />', $validation);
            }
        } else
            $msg = 'The old password does not match';

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl . '/me');
    }

    /**
     * Logout
     */
    public function logout() {
        $this->load->model('Statistic_model');
        $addUserLog = $this->Statistic_model->insertUserLog(SessionManagerWeb::getUserID(), 'logout');
        AuthManagerWeb::logout();
        redirect($this->getCTL('site'));
    }

    /**
     * Membuat data baru
     */
    public function create() {
        $data = $this->postData;
        if ($data['karyawan_id']!=NULL)
            $data['karyawan_id'] = ltrim($data['karyawan_id'], '0');
        if ($_FILES['photo']['error'] == UPLOAD_ERR_OK)
            $data['photo'] = $_FILES['photo']['name'];
        else
            unset($_FILES['photo']);

        $password = ($data['password'] ? $data['password'] : $data['username']);
        $data['password_salt'] = $auth_key = SecurityManager::generateAuthKey();
        $data['password'] = SecurityManager::hashPassword($password, $auth_key);

        $getUser = $this->model->get_by('users.username', $data['username']);
        if ($getUser) {
            SessionManagerWeb::setFlashMsg(false, 'Username already used. Please use another username.');
            redirect($this->ctl . '/add');
        }
        
        if($data['role'] == Role::EKSTERNAL){
            $data['karyawan_id'] = $data['username'];
            $karyawan= array(
                'id'            => $data['karyawan_id'],
                'name'          => $data['name'],
                'jabatan_id'    => null,
                'tgl_masuk'     => strtoupper("'".$data['tgl_masuk']."'"),
                'tgl_pensiun'   => strtoupper("'".$data['tgl_pensiun']."'"),
                'eselon_code'   => '0',
                'eselon_name'   => 'Eksternal',
                'status'            => 3,
                'karyawan_lokasi'   => null,
                'lokasi'        => null,
                'email'         => $data['email'],
                'updated_at'    => "to_date('".date("d/m/Y")."','DD-MM-YYYY')"
            );

            $user_eksternal = array(
                'company'       => json_encode($data['company_access']),
                'document_type' => json_encode($data['type_access'])
            );

            unset($data['tgl_masuk']);
            unset($data['tgl_pensiun']);
            unset($data['company_access']);
            unset($data['type_access']);
        }
        else{
            $data['karyawan_id'] = str_pad($data['karyawan_id'], 8, '0', STR_PAD_LEFT);
        }

        $insert = $this->model->create($data, true, true);
        if ($insert) {
            if($data['role'] == Role::EKSTERNAL){
                dbInsert('Karyawan', $karyawan);
                $user_eksternal['user_id'] = $insert;
                dbInsert('user_eksternal', $user_eksternal);
            }
            
            SessionManagerWeb::setFlashMsg(true, 'Successfully added user');
            $this->redirect();
        } else {
            if (!is_string($insert)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to add user';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $insert;

            SessionManagerWeb::setFlashMsg(false, $msg);
            redirect($this->ctl . '/add');
        }

    }

    /**
     * Edit data user
     * @param int $id jika tidak ada dianggap data pribadi
     */
    public function update($id = null) {
        $data = $this->postData;
        $isself = (empty($id) ? true : false);

        if ($isself) {
            $id = SessionManagerWeb::getUserID();
            $data['username'] = SessionManagerWeb::getUserName();
            unset($data['status']);
        }
        if ($data['karyawan_id']!=NULL) {
            $data['karyawan_id'] = $data['karyawan_id'];
        }
        if ($_FILES['photo']['error'] == UPLOAD_ERR_OK)
            $data['photo'] = $_FILES['photo']['name'];
        else
            unset($_FILES['photo']);

        if($data['role'] == Role::EKSTERNAL){
            $karyawan = array(
                'tgl_masuk'     => strtoupper("'".$data['tgl_masuk']."'"),
                'tgl_pensiun'   => strtoupper("'".$data['tgl_pensiun']."'")
            );
            $user_eksternal = array(
                'company'       => json_encode($data['company_access']),
                'document_type' => json_encode($data['type_access'])
            );
            unset($data['tgl_masuk']);
            unset($data['tgl_pensiun']);
            unset($data['company_access']);
            unset($data['type_access']);
        }
        $update = $this->model->save($id, $data, TRUE);

        if ($update === true) {
            if($data['role'] == Role::EKSTERNAL){
                dbUpdate("karyawan", $karyawan, "id='".$data['karyawan_id']."'");
                dbUpdate("user_eksternal", $user_eksternal, "user_id='".$id."'");
            }
            $ok = true;
            $msg = 'Successfully changed profile';
                
            if(SessionManagerWeb::getUserID() == $id){
                // sekalian foto
                $user = $this->model->get($id);
                SessionManagerWeb::setPhoto($user);
            }

        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change profile';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        if ($isself)
            redirect($this->ctl . '/me');
        else
            redirect($this->ctl . '/edit/' . $id);
    }

    // reset all thumbnail to watermark
    public function resetPPThumbnail(){
        $users = $this->model->getAllUsersWithRole(FALSE);
        $suc = false;
        foreach ($users as $user) {
            if($this->_watermark($user['id'])){
                $suc = true;
            }
        }
        var_dump($suc);
    }

   // WATERMARK
    private function _watermark($userid, $size=128){
        // ambil user
        ini_set('display_errors','On');
        $this->load->model('User_model');
        
        $this->User_model->show_all(true); // biar muncul yang privat
        $user = $this->User_model->get($userid);
        
        // ambil image thumbnail
        $config = $this->config->item('utils');
        $thumb = $user['photo'];
        $image = new Imagick();

        $upload = true;
        if ($thumb) {
            $image->readImage($config['full_upload_dir'].'users/photos/'.$thumb[strtolower(Image::IMAGE_ORIGINAL)]['name']);

            $iWidth = $image->getImageWidth();
            $iHeight = $image->getImageHeight();
            
            if($iWidth != $iHeight) {
                if($iWidth > $iHeight)
                    $iWidth = $iHeight;
                else
                    $iHeight = $iWidth;
                
                $image->cropImage($iWidth, $iHeight, 0, 0);
            }
            
            if(!empty($size)) {
                $iWidth = $iHeight = $size;
                
                $image->scaleImage($iWidth, $iHeight);
            }

            // jika ada pangkatnya
            if(!empty($user['rank'])) {
                $image->setImageFormat('png');
                $image->setImageOpacity(0.75);
                
                // proses image pangkat
                $ranks = $this->User_model->getRank();
                
                $watermark = new Imagick();
                $watermark->readImage($config['full_upload_dir'].'pangkat/'.strtolower($ranks[$user['rank']]).'.png');
                
                $wWidth = $watermark->getImageWidth();
                $nWidth = $wWidth * (($iWidth-($iWidth/16)) / 60);
                
                $watermark->scaleImage($nWidth, 0);
                
                $wWidth = $watermark->getImageWidth();
                $wHeight = $watermark->getImageHeight();
                
                // $watermark->evaluateImage(Imagick::EVALUATE_DIVIDE, 2, Imagick::CHANNEL_ALPHA);
                
                // buat image composite
                /* $x = ($iWidth - $wWidth) / 2;
                $y = ($iHeight - $wHeight) / 2; */
                $x = $iWidth / 32;
                $y = $iHeight - $wHeight - ($iHeight / 32);
                
                $image->compositeImage($watermark, imagick::COMPOSITE_OVER, $x, $y);
            }
            
            $upload = $image->writeImage($config['full_upload_dir'].'users/photos/'.$thumb[strtolower(Image::IMAGE_THUMB)]['name']);
        }
        return $upload;
    }


    /**
     * Halaman export
     * @param int $userid
     */
    public function add_import() {
        parent::add_import();
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['user']);  
        $this->template->viewDefault($this->view, $this->data);
    }

    public function uploadMetadata(){
        $this->metadataform('masters/user');
    }

    public function import(){

        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));

        $path_source = $ciConfig['full_upload_dir'] . $folder_source;
        // $type = $this->input->post('type', null);

        if (is_dir($path_source.'/masters/user/'.$id)) {
           $files = glob($path_source.'/masters/user/'.$id.'/'.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $metadata = $file_src;
               break;
           }
        }
        $file = $path_source."/masters/user/".$id."/".$metadata;

        if (!file_exists($file)){
            SessionManagerWeb::setFlashMsg(false, 'Failed to import. Metadata not exist');
            redirect('/web/user');
        }

        $this->load->library('Libexcel');

        $tmpfname = $file;
        
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $worksheetData = $excelReader->listWorksheetInfo($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $this->load->model("Karyawan_model");

        $roles = array(Role::ADMINISTRATOR,Role::DRAFTER, Role::DOCUMENT_CONTROLLER, Role::ADMIN_UNIT);
        $column = array();
        $success = array();
        $failed = array();
        $this->load->model("User_model");
        for ($row = 2; $row <= $worksheetData[0]['totalRows']; $row++) {
            $data = array();
            $insert_other = array();
            for ($col = 0 ; $col < $worksheetData[0]['totalColumns']; $col++){
                $colString = PHPExcel_Cell::stringFromColumnIndex($col);
                $value =  $worksheet->getCell($colString.$row)->getValue();

                switch($col){
                    case 0:
                        $data['karyawan_id'] = $value;
                    break;
                    case 1:
                        $data['role'] = "{$value}";
                    break;
                    case 2:
                        $data['password'] = "{$value}";
                    break;
                }
            }
            if ($data['karyawan_id']==NULL or $data['karyawan_id']==''){
                break;
            }
            
            if (SessionManagerWeb::isAdminUnit()){
                $company = ltrim($this->model->filter("where \"karyawan_id\"='".$data['karyawan_id']."' ")->getOneFilter('company'));
                if ($company!=SessionManagerWeb::getVariablesIndex('mycompany')){
                    $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"Not Allowed : you cannot import the employee from $company");
                    continue;
                }
            }

            // Check Metadata
            // cek id
            if (strlen($data['karyawan_id'])!=8){
                $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata : Employee number not 8 Digit");
                continue;
            } else {
                $data['karyawan_id'] = "{$data['karyawan_id']}";
            }

            // karyawan id               
            $is_karyawan_exist = $this->Karyawan_model->filter("where id='".$data['karyawan_id']."' ")->getOneFilter('1');
            if (!$is_karyawan_exist){
                $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata : Employee data from this employee number not Exist");
                continue;
            }

            // cek role
            if (!in_array($data["role"], $roles)){
                 $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata : Role not Exist");
                 continue;
            }

            if ($data['password']==NULL or $data['password']==''){
                $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata : Password cannot be empty");
                 continue;
            } else {
                $data['password_salt'] = SecurityManager::generateAuthKey();
                $password = $data['password'];
                $data['password'] = SecurityManager::hashPassword($password, $data['password_salt']);
            }
            

            $is_exist = $this->model->show_sql(false)->filter(" where \"karyawan_id\"='".$data['karyawan_id']."' ")->getOneFilter('karyawan_id');
            if ($is_exist==NULL or $is_exist==false){                

                $karyawan = $this->Karyawan_model->getBy($data['karyawan_id']);
                $data['email'] = $karyawan['email'];
                $data['name'] = $karyawan['name'];

                // cek kalo email ini sudah ada yang pake

                // yang lama (berdasar email)
                // if ($data['email']!=NULL and $data['email']!='') {
                //     $usernames = explode('@', $data['email']);

                //     // Check user exist
                //     $is_exist_username = $this->model->show_sql(false)->filter(" where lower(\"username\")='".strtolower($usernames[0])."' ")->getOneFilter('karyawan_id');
                //     if ($is_exist_username!=NULL and $is_exist_username!=false){
                //         $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata : Username ".$usernames[0]." from this email( ".$data['email']." ) Already Exist");
                //         continue;
                //     } else {
                //         $data['username'] = $usernames[0];
                //         $data['password_salt'] = SecurityManager::generateAuthKey();
                //         $password = $data['password'];
                //         $data['password'] = SecurityManager::hashPassword($password, $data['password_salt']);
                //     }
                // }

                if ($data['email']!=NULL and $data['email']!='') {
                    // Check user exist
                    $is_exist_username = $this->model->show_sql(false)->filter(" where lower(\"username\")='".strtolower($data['email'])."' ")->getOneFilter('karyawan_id');
                    if ($is_exist_username!=NULL and $is_exist_username!=false){
                        $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata : Username ".$data['email']." Already Exist");
                        continue;
                    }
                }

                $data['is_active'] = 1;

                if (dbInsert("Users", $data)){
                    $success[] = array("id" => $data['karyawan_id'], "reason" => "Successfully added");
                } else {
                    $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata");
                }
            } else {
                // update
                if (dbUpdate("users", $data, "karyawan_id='".$data['karyawan_id']."'")){
                    $success[] = array("id" => $data['karyawan_id'], "reason" => "Successfully updated");
                } else {
                    $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"False Metadata");
                }
                // Keluarkan kalo sudah ada
                // $failed[] = array("id"=>$data['karyawan_id'], "reason"=>"User that use this Employee Number Already Exist");
            }
            
        }
        $_SESSION[SessionManagerWeb::INDEX]['import']['jabatan'] = array(
            "success" => $success,
            "failed" => $failed
        );
        $this->data['success'] = $success;
        $this->data['failed'] = $failed;
        $buttons['download'] = array('label' => 'Download Report', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownload()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;

        $this->_deleteFiles($path_source."masters/user/".$id);
        $this->template->viewDefault($this->view, $this->data);
    }

    public function downloadImportReport(){
        $filename = "import_employee_report";  
        $file_ending = "xls";
        //header info for browser
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");    
        header("Content-Disposition: attachment; filename=$filename.xls");  
        header("Pragma: no-cache"); 
        header("Expires: 0");

        /*******Start of Formatting for Excel*******/   
        $sep = "\t"; 

        echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
        echo "<tr></tr>";
        echo "<tr><td colspan='2'><b>Import Jabatan</b></td></tr>";
        echo "<tr><td>Di Import Tanggal</td><td>".date("d M Y")."</td></tr>";
        echo "<tr><td>Di Import Oleh</td><td colspan=3>".SessionManagerWeb::getName()."</td></tr>";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header = "<tr>
                <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Employee Number</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema='';
        foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['jabatan']['failed'] as $fail) {
            $schema.= "<tr>
                <td style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;'>
                    {$fail['id']}
                </td>{$sep}
                <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;'>
                    {$fail['reason']}
                </td>{$sep}
            </tr>";
        }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo "<tr></tr><tr></tr>";

        echo "<tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>";
        // echo "<tr></tr>";
        $header ="<tr>
                <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Employee Number</td>{$sep}
                <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>{$sep}
            </tr>";
        $header = str_replace($sep."$", "", $header);
        $header = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header);
        $header .= "\t";
        print(trim($header));
        echo '</tr>';
        print("\n");

        $schema = '';
        foreach ($_SESSION[SessionManagerWeb::INDEX]['import']['jabatan']['success'] as $s) {
            $schema .= "<tr>
                <td style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;overflow:hidden'>
                    {$s['id']}
                </td>{$sep}
                <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;max-height:300px;overflow:hidden'>
                    {$s['reason']}
                </td>{$sep}
            </tr>";
        }
        $schema = str_replace($sep."$", "", $schema);
        $schema = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema);
        $schema .= "\t";
        print(trim($schema));
        $schema .= "</tr>";
        print "\n";
        echo '</table>';
        unset($_SESSION[SessionManagerWeb::INDEX]['import']['unitkerja']);
    }
}

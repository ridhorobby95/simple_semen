<?php

# untuk keperluan public tanpa membutuhkan login

class Cron extends CI_Controller
{
	private $uri_segments = array();
	
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        die();
    }


    // every 00.00
    public function generateStatistikTotalType(){
        $this->load->model('Document_model');
        $data = $this->Document_model->generateStatistikTotalType();
        echo '<pre>';
        var_dump($data);
        die();
    }

    public function sendEmail($type='D'){
        $this->load->model('Cron_notifications_model');
        $this->load->model('Email_model');
        $this->load->model('User_model');
        $filter = " where is_send=0 and type='".$type."' ";

        switch ($type) {
            // every 5 mins
            case Cron_notifications_model::TYPE_DIRECT:
                $filter .= "and rownum<50";
                $type_email = Email_model::NOTIFICATION;
            break;
            // every 7 AM
            case Cron_notifications_model::TYPE_SUMMARY:
                $type_email = Email_model::NOTIFICATION;
            break;
            // every 6 AM
            case Cron_notifications_model::TYPE_REMINDER:
                $type_email = Email_model::NOTIFICATION;
            break;
            case Cron_notifications_model::TYPE_CHANGEPASSWORD:
                $type_email = Email_model::FORGET_PASSWORD;
            break;
        }

        $emails = $this->Cron_notifications_model->filter($filter)->order("type")->getAll();
        echo "<table style='border:1px solid'>";
        echo "<td style='border: 1px solid'>EMAIL</td>";
        echo "<td style='border: 1px solid'>SUCCESS?</td>";
        foreach ($emails as $email) {
            $user = array_change_key_case($this->User_model->getByColumn('id', $email['user_id']), CASE_LOWER);
            // echo "<pre>";print_r($user);die();
            echo '<tr style="border:1px solid">';
            // $to = 'ridho.robby50@gmail.com';
            $to = $user['email'];
            echo "<td style='border:1px solid'>".$to."</td>";
            
            $user_id = $email['user_id'];
            if($type == Cron_notifications_model::TYPE_CHANGEPASSWORD){
                $id = explode('/', $email['url']);
                $dataReset = dbGetRow("select * from change_password where id=".$id[7]);
                $email['message'] .="<br><br> Username: ".$dataReset['USERNAME']."<br> New Password: ".$dataReset['PASSWORD']."<br><br><b>You can change your password in your profile</b>";
            }
            
            $success = $this->Email_model->to($to)
                        ->subject('Simple - Document Management - Notification')
                        ->message(array(
                            'url' => $email['url'],
                            'type' => $type_email,
                            'body' => array('content' => "Dear <b>".$user['name'].",</b><br>".$email['message']),
                            'user_id' => $user_id
                        ))
                        ->send();
            if ($success == 'success'){
                echo "<td style='border:1px solid'>YES</td>";
                $record = array();
                $record['is_send'] = 1;
                dbUpdate('cron_notifications', $record, "id=".$email['id']);
            } else {
                echo "<td style='border:1px solid'>NO</td>";
            }
            $this->Email_model->reset();
            echo '</tr>';
        }
        echo "</table>";
    }

    // Every 4 AM
    public function generateSummary(){
        $this->load->model('Cron_notifications_model');
        $this->load->model('Email_model');
        $this->load->model('User_model');
        $this->load->model('Document_model');

        // $this->Document_model->filter("where \"status\" in  ")->getAll();
    }

    // Every  
    public function generateReminder(){
        $this->load->model('Cron_notifications_model');
        $this->load->model('Email_model');
        $this->load->model('User_model');
        $this->load->model('Document_model');
        $this->load->model("Workflow_model");
        $this->load->model("Notification_model");

        // Select Document yang belum selesai
        $filter = "where 
                        status not in ('".Document_model::STATUS_SELESAI."', '".Document_model::STATUS_REJECT."')
                        and is_obsolete=0 
                        ";
        $documents = $this->Document_model->show_sql(false)->filter($filter)->order(" order by id ")->getAll();
        foreach ($documents as $k_document => $v_document) {
            $v_document = array_change_key_case($v_document, CASE_LOWER);
            $receiver_ids = $this->Document_model->filter("where id={$v_document['id']}")->getActorIds($v_document['id'], false);
            $email_message = "We are reminding you that the document ".'"'.$v_document['title'].'" is needing your action';
            $email_url = site_url('web/document/detail').'/'.$v_document['id'];   
            $this->Cron_notifications_model->insert($receiver_ids, $email_message, $email_url, Cron_notifications_model::TYPE_REMINDER);
            $this->Notification_model->generate(Notification_model::ACTION_REMINDER, $v_document['id'], NULL, false);
        }

        // Select document yang perlu evaluasi
        $filter_evaluation = "where 
                        \"status\" in ('".Document_model::STATUS_SELESAI."') 
                        and \"workflow_id\" not in ('".Workflow_model::EVALUASI."')
                        and \"is_obsolete\"=0 and \"is_published\"=1 
                        and add_months(\"validity_date\", '-1') < sysdate ";
        $evaluations = $this->Document_model  ->show_sql(false)
                                            ->table("documents_view")
                                            ->column(" \"document_id\", \"revision\",\"creator_id\",  \"title\", \"type_name\", \"validity_date\", \"is_obsolete\" ")
                                            ->filter($filter_evaluation)
                                            ->order(" order by \"document_id\" ")
                                            ->getAll();
        foreach ($evaluations as $k_evaluation => $v_evaluation) {
            $receiver_ids = array();
            $receiver_ids[] = $v_evaluation['creator_id'];
            $email_message = $v_evaluation['type_name'].' "'.$v_evaluation['title'].'" that you created has entered the evaluation period and should be evaluated';

            // Check apakah sudah melewati masa valid
            $filter_evaluation = "where 
                        \"status\" in ('".Document_model::STATUS_SELESAI."') 
                        and \"workflow_id\" not in ('".Workflow_model::EVALUASI."')
                        and \"is_obsolete\"=0 and \"is_published\"=1 
                        and \"validity_date\" < sysdate and \"document_id\"={$v_evaluation['document_id']} ";
            $temp = $this->Document_model  ->show_sql(false)
                                            ->table("documents_view")
                                            ->column(" \"document_id\", \"revision\",\"creator_id\",  \"title\", \"type_name\", \"validity_date\", \"is_obsolete\" ")
                                            ->filter($filter_evaluation)
                                            ->order(" order by \"document_id\" ")
                                            ->getAll();
            if ($temp==NULL or $temp==false){
                $email_message = $v_evaluation['type_name'].' "'.$v_evaluation['title'].'" that you created has passed the validity date';
            }
            $email_url = site_url('web/document/searchDocumentById').'/'.$v_evaluation['document_id'];   
            $this->Cron_notifications_model->insert($receiver_ids, $email_message, $email_url, Cron_notifications_model::TYPE_REMINDER);
            $this->Notification_model->generate(Notification_model::ACTION_NEED_EVALUATE, $v_evaluation['document_id'], NULL, false);
        }
    }
    
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller Issue_category
 * @author Integra
 * @version 1.0
 */
class Setting extends WEB_Controller {

    protected $title = 'Kategori Tugas';

    /**
     * Halaman default
     * @param int $page
     */
    public function index() {
        $this->data['data'] = array();

        /* Kategori settings */
        $this->data['data']['pengguna'] = array("title" => "User");
        // $this->data['data']['post'] = array("title" => "Post");
		// $this->data['data']['group'] = array("title" => "Group");        
        $this->data['data']['document'] = array("title" => "Document");
        $this->data['data']['quicklink'] = array("title" => "Custom Quick Link");
        // $this->data['data']['workflow'] = array("title" => "Workflow");
        $this->data['data']['configuration'] = array("title" => "Other");

        /* Item yang dapat diakses semua user*/
        
        /* Item khusus admin*/
        if(SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()){
            $this->data['data']['pengguna']['item'][] = array("name" => "Edit your profile", "logo" => "user", "link" => "user/me");
            $this->data['data']['document']['item'][] = array("name" => "Migration", "logo" => "file", "link" => "migration");

            // company
            $this->load->model("User_model");
            $company = ltrim($this->User_model->filter(" where \"user_id\"='".SessionManagerWeb::getUserID()."' ")->getOneFilter('company'), '0');
            if (SessionManagerWeb::isAdministrator()){
                $this->data['data']['document']['item'][] = array("name" => "Migration Overwrite", "logo" => "file", "link" => "migration/overwrite");
            }

            $this->data['data']['document']['item'][] = array("name" => "Change Unit", "logo" => "file", "link" => "document/listchangeunit");
            if (SessionManagerWeb::isAdministrator() || (SessionManagerWeb::isDocumentController() and $company=='2000')){
                $this->data['data']['document']['item'][] = array("name" => "Metadata Overwrite", "logo" => "file", "link" => "migration/metadataOverwrite");
                $this->data['data']['document']['item'][] = array("name" => "Business Processes", "logo" => "file", "link" => "document_prosesbisnis");
                $this->data['data']['document']['item'][] = array("name" => "Document Requirement (ISO) and Clause", "logo" => "file", "link" => "document_iso");
                 $this->data['data']['document']['item'][] = array("name" => "Manage All Documents", "logo" => "file", "link" => "manage_document");
                if (SessionManagerWeb::isAdministrator()){
                    $this->data['data']['document']['item'][] = array("name" => "Document Types", "logo" => "file", "link" => "document_types");
                    $this->data['data']['document']['item'][] = array("name" => "Document Types Submission", "logo" => "file", "link" => "tingkatan_document_types");
                }
                
            }
            
            // $this->data['data']['configuration']['item'][] = array("name" => "Configuration", "logo" => "cogs", "link" => "configuration");
        }

        /* Item khusus admin / management*/
        
            $this->data['data']['pengguna']['item'][] = array("name" => "User List", "logo" => "group", "link" => "user");
        if(SessionManagerWeb::isAdministrator() || SessionManagerWeb::isAdminUnit()){
            $this->data['data']['pengguna']['item'][] = array("name" => "Employee List", "logo" => "group", "link" => "karyawan");
            $this->data['data']['pengguna']['item'][] = array("name" => "Superiors", "logo" => "group", "link" => "atasan");
            $this->data['data']['pengguna']['item'][] = array("name" => "Position List", "logo" => "group", "link" => "jabatan");
            $this->data['data']['pengguna']['item'][] = array("name" => "Work Unit List", "logo" => "group", "link" => "unitkerja");
            if (SessionManagerWeb::isAdministrator()){
                $this->data['data']['pengguna']['item'][] = array("name" => "Company List", "logo" => "group", "link" => "unitkerja/company");
                $this->data['data']['quicklink']['item'][] = array("name" => "Custom Quick Link", "logo" => "bars", "link" => "quick_link");
            }
            
        }

        $this->data['title'] = 'Configuration';

        // tombol
        // $buttons = array();
        // $buttons[] = array('label' => 'Tambah Kategori', 'type' => 'success', 'icon' => 'plus', 'click' => 'goAdd()');
        // $this->data['buttons'] = $buttons;
        
        $this->template->viewDefault($this->view, $this->data);
    }
}

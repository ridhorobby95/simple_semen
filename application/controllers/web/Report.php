<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Report extends WEB_Controller {

    protected $title = 'Report';
    function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->data['nocover'] = base_url('assets/web/img/cover.jpg');
        $this->data['noimg'] = base_url('assets/web/img/logo.png');
    }

    public function index(){
    	die();
    }

    public function getFilter($filter = NULL){    	
		$filter=$filter;
		if (isset($_SESSION['document_search'])){
	   		foreach ($_SESSION['document_search'] as $key => $value) {
	   			if ($value==''){
	   				unset($_SESSION['document_search'][$key]);
	   				continue;
	   			}
	   			$val = strtoupper($value);
		   		switch($key){
		   			case 'keyword':
		   				$filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%' or upper(\"unitkerja_name\") like '%$val%') ";
		   				$_SESSION['document_search']['title'] = $value;
		   			break;

		   			case 'user':
		   				$filter .= " and (upper(\"username\") like '%$val%' or upper(\"name\") like '%$val%' or upper(\"creator_username\") like '%$val%' or upper(\"creator_name\") like '%$val%') ";
		   			break;

		   			case 'type':
			   			if ($_SESSION['document_search']['arr_type'][0]=='0'){
		   					$filter_type = '';
		   				} else {
		   					$filter_type = " and \"type_id\" in ($val) ";
		   				}
		   				$filter .= $filter_type;
		   			break;

		   			case 'title':
		   				$filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%') ";
		   			break;

		   			case 'show_by':
		   				switch($val){
		   					case 'ME':
		   						$filter .= " and \"user_id\"='".SessionManagerWeb::getUserID()."'";
		   					break;
		   					case 'NEED ACTION':
		   						$filter .= " and (\"user_id\"!='".SessionManagerWeb::getUserID()."' or (\"workflow_urutan\"='1' and \"user_id\"='".SessionManagerWeb::getUserID()."') )";
		   					break;
		   				}
		   			break;

		   			case 'unitkerja':
		   				if ($_SESSION['document_search']['arr_unitkerja'][0]=='0'){
		   					$filter_unitkerja = '';
		   				} else {
		   					$filter_unitkerja = " and \"unitkerja_id\" in ($val) ";
		   				}
		   				$filter .= $filter_unitkerja;
		   			break;

		   			case 'datestart':
		   				$filter .= " and (upper(\"updated_at\") >= '$val') ";
		   			break;

		   			case 'dateend':
		   				$filter .= " and (upper(\"updated_at\") <= '$val%') ";
		   			break;
		   		}
		   	}
	   	}
	   	return $filter;
	}

    public function exportDID($company='2000', $type_id=NULL){
    	if (!SessionManagerWeb::isAdministrator() and !SessionManagerWeb::isDocumentController()){
    		SessionManagerWeb::setFlashMsg(false, 'You have no permission to export "Dokumen Indek Dokumen" this!');
    		redirect("web/document");
    	}
        $this->load->model("Document_types_model");
        $this->load->model("Document_delegates_model");
        $this->load->model("User_model");
        $this->load->model("Workflow_model");
        $this->load->model("Document_activity_model");
        $this->load->model("Unitkerja_model");
        $this->load->model("Document_iso_model");
        $this->load->model("Document_klausul_model");
        $this->load->model("Document_model");

        $filename = "daftar_induk_dokumen";  
		$file_ending = "xls";

		//header info for browser
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");    
		header("Content-Disposition: attachment; filename=$filename.xls");  
		header("Pragma: no-cache"); 
		header("Expires: 0");

		// Workflow Pengajuan
		$workflow = $this->Workflow_model->getTasks(Workflow_model::PENGAJUAN);
		// cek verifikasi di urutan mana
		$urutans = array();
		$urutans['verification']=4;
		$urutans['review'] =2;
		$urutans['approval']=5;
    	foreach ($workflow as $task) {
    		if ($task['user']==Role::REVIEWER and $task['form']==Workflow_model::REVIEW_CONTENT){
    			$urutans['review'] = $task['urutan'];
    		}
    		if ($task['user']==Role::DOCUMENT_CONTROLLER and $task['form']==Workflow_model::VERIFICATION){
    			$urutans['verification'] = $task['urutan'];
    		}
    		if ($task['user']==Role::APPROVER and $task['form']==Workflow_model::APPROVAL){
    			$urutans['approval'] = $task['urutan'];
    		}
    	}


    	// setting variable
    	if ($type_id==0){
        	$types = Util::toList($this->Document_types_model->getAllJenis(), 'id');
        } else {
        	$types[] = $type_id;
        }

    	// $filter = "and \"company\"='".ltrim($company,'0')."'";
    	$comp_id = str_pad($company, 8, '0', STR_PAD_LEFT);
    	$filter = " and \"is_published\"=1 and \"company\"='".ltrim(SessionManagerWeb::getCompany(), '0')."' and \"show\"=1 ";
    	if ($company=='these'){
		 	$filter .= $this->getFilter($filter);

		 	$filter_obsolete = " and \"show\"='1' and \"is_obsolete\"='0' ";
		 	// if ($_SESSION['selected_menu']=='obsolete'){
		 	// 	$filter_obsolete = " and \"is_obsolete\"='1' ";
		 	// }
		 	$filter .= $filter_obsolete;

    		if (isset($_SESSION['document_search']['arr_type'])){
    			unset($types);
    			$types = $_SESSION['document_search']['arr_type'];
    		} 

    		$comp_id = str_pad(SessionManagerWeb::getCompany(), 8, '0', STR_PAD_LEFT);
    	}

    	$comp_name = $this->Unitkerja_model->getOne($comp_id, 'name');

		echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
		echo "<tr></tr>";
		echo "<tr><td colspan=4 style='font-weight:bold'>Daftar Induk Dokumen ".$comp_name."</td></tr>";
		echo "<tr></tr>";
		echo "<tr><td>Dibuat Tanggal</td><td>".date("d M Y")."</td></tr>";
		echo "<tr><td>Dibuat Oleh</td><td colspan=3>".SessionManagerWeb::getName()."</td></tr>";
		echo "<tr></tr>";
        foreach ($types as $type) {
        	

        	$column_names = array();
        	switch ($type) {
        		case Document_types_model::BOARD_MANUAL:
        			$column_names = array(
		        		"type_name" => "Document Type",
		        		"code" => "Document Code",
		        		"title" => "Title",
		        		"revision" => "Revision",
		        		"published_at" => "Published Date",
		        		"validity_date" => "Evaluation Schedule",
		        		"creator_name" => "Created By",
		        		"reviewed_by" => "Reviewed By", 			// Tidak ada di DB
		        		"verified_by" => "Verified by Document Controller",
		        		"approver_name" => "Approved By",
		        		"description" => "Change Log"
		        	);
    			break;
    			case Document_types_model::PEDOMAN:
        			$column_names = array(
		        		"type_name" => "Document Type",
		        		"code" => "Document Code",
		        		"title" => "Title",
		        		"revision" => "Revision",
		        		"prosesbisnis_name" => "Business Process",
		        		"requirements" => "ISO Requirements",
		        		"clausuls" => "Clauses",
		        		"related" => "Related Documents",
		        		"published_at" => "Published Date",
		        		"validity_date" => "Evaluation Schedule",
		        		"creator_name" => "Created By",
		        		"reviewed_by" => "Reviewed By", 			// Tidak ada di DB
		        		"verified_by" => "Verified by Document Controller",
		        		"approver_name" => "Approved By",
		        		"description" => "Change Log"
		        	);
    			break;
    			case Document_types_model::PROSEDUR:
        			$column_names = array(
		        		"type_name" => "Document Type",
		        		"code" => "Document Code",
		        		"title" => "Title",
		        		"revision" => "Revision",
		        		"prosesbisnis_name" => "Business Process",
		        		"requirements" => "ISO Requirements",
		        		"clausuls" => "Clauses",
		        		"related" => "Related Documents",
		        		"published_at" => "Published Date",
		        		"validity_date" => "Evaluation Schedule",
		        		"creator_name" => "Created By",
		        		"reviewed_by" => "Reviewed By", 			// Tidak ada di DB
		        		"verified_by" => "Verified by Document Controller",
		        		"approver_name" => "Approved By",
		        		"department_name" => "Department",
		        		"description" => "Change Log"
		        	);
    			break;
    			case Document_types_model::INSTRUKSI_KERJA:
        			$column_names = array(
		        		"type_name" => "Document Type",
		        		"code" => "Document Code",
		        		"title" => "Title",
		        		"revision" => "Revision",
		        		"prosesbisnis_name" => "Business Process",
		        		"requirements" => "ISO Requirements",
		        		"related" => "Related Documents",
		        		"published_at" => "Published Date",
		        		"validity_date" => "Evaluation Schedule",
		        		"creator_name" => "Created By",
		        		"verified_by" => "Verified by Document Controller",
		        		"approver_name" => "Approved By",
		        		"unitkerja_name" => "Work Unit",
		        		"department_name" => "Department",
		        		"description" => "Change Log"
		        	);
    			break;
    			case Document_types_model::FORM:
        			$column_names = array(
		        		"type_name" => "Document Type",
		        		"code" => "Document Code",
		        		"title" => "Title",
		        		"revision" => "Revision",
		        		"prosesbisnis_name" => "Business Process",
		        		"requirements" => "ISO Requirements",
		        		"related" => "Related Documents",
		        		"published_at" => "Published Date",
		        		"validity_date" => "Evaluation Schedule",
		        		"creator_name" => "Created By",
		        		"verified_by" => "Verified by Document Controller",
		        		"approver_name" => "Approved By",
		        		"unitkerja_name" => "Work Unit",
		        		"department_name" => "Department",
		        		"retension" => "Retention Time",
		        		"description" => "Change Log"
		        	);
    			break;
        	}
        	
        	$columns = array(
        		"\"document_id\"" => "\"document_id\"",
        		"\"company\"" => "\"company\"",
        		"\"type_name\"" => "\"type_name\"",
        		"\"code\"" => "\"code\"",
        		"\"title\"" => "\"title\"",
        		"\"revision\"" => "\"revision\"",
        		"\"prosesbisnis_name\"" => "\"prosesbisnis_name\"",
        		"to_date(TO_CHAR(\"published_at\", 'MM-DD-YYYY'), 'MM-DD-YYYY')" => "\"published_at\"",
        		"\"validity_date\"" => "\"validity_date\"",
        		"\"creator_name\"" => "\"creator_name\"",
        		"NULL" => "\"reviewed_by\"", 			// Tidak ada di DB
        		"NULL" => "\"verified_by\"",
        		"\"approver_name\"" => "\"approver_name\"",
        		"\"retension\"" => "\"retension\"",
        		"\"unitkerja_id\"" => "\"unitkerja_id\"",
        		"\"unitkerja_name\"" => "\"unitkerja_name\"",
        		"\"description\"" => "\"description\""
        	);
        	$data = $this->model->show_sql(false)->column($columns)->filter("where \"type_id\"=$type $filter")->getAll();
        	// $data = $this->model->column($columns)->filter("where \"type_id\"=$type")->getAll();

	        /*******Start of Formatting for Excel*******/   
			$sep = "\t"; 
			// echo "<pre>";print_r($columns);echo "</pre>";
			
			$header_insert = '<tr>'; 
			foreach ($data as $k_data => $v_data) {
				// get actor
				$actors = $this->getActors($v_data['document_id'],$urutans);
				$v_data['approver_name'] = $actors['approval'];
				$v_data['verified_by'] = $actors['verification'];
				$v_data['reviewed_by'] = $actors['review'];

				// jadikan retensi 1 tahun
				$retension = '';
				if (floor($v_data['retension']/12)>0){
					$retension = floor($v_data['retension']/12).' Tahun ';
				} 
				$sisa = $v_data['retension']%12;
				if ($sisa>0){
					$retension .= $sisa.' Bulan';
				}
				$v_data['retension'] = $retension;

				// get dept
				$dept_id = $this->Unitkerja_model->getDept($v_data['unitkerja_id']);
				$v_data['department_name']= $this->Unitkerja_model->getOne($dept_id, 'name');

				// get requirement
				$v_data['requirements'] = implode(";",
											Util::toList(
												$this->Document_iso_model->filter(" dis.document_id=".$v_data['document_id'])
																		->table(" document_iso di ")
																		->with(array("name"=>"document_isos", "initial"=>"dis"),"dis.iso_id=di.id")
																		->column(array("iso"=>"\"iso\""))->getAll()
											,'iso')
										);

				// get clausul
				$v_data['clausuls'] = implode(";",Util::toList($this->Document_klausul_model->filter(" dk.document_id=".$v_data['document_id'])->table(" document_klausul dk ")->with(array("name"=>"document_iso_klausul", "initial"=>"dik"),"dik.id=dk.klausul_id")->column(array("klausul"=>"\"klausul\""))->getAll(),'klausul'));

				// get Related Document
				$related = $this->Document_model->getRelatedDocument($v_data['document_id']);
				$v_data['related']='';
				foreach ($related as $v_related) {
					$rel_doc_id = $v_related['IDDOCUMENT'];
					$v_data['related'].= $this->model->filter("where \"document_id\"=$rel_doc_id ")->getOne("\"code\"").';';
				}
				$schema_insert = "<tr>";
				foreach ($column_names as $k => $v) {
					if ($k_data==0){
						$header_insert .= "<td style='border: 1px solid black;background-color:yellow;font-weight:bold;max-height:300px;overflow:hidden'>$v</td>".$sep;
						// $header_insert .= "$v".$sep;
						// echo "<b>$v</b>".$sep;
					}
	                $schema_insert .= "<td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>$v_data[$k]</td>".$sep;

	                // $schema_insert .= "$v_data[$k]".$sep;
				}
				if ($k_data==0){
					$header_insert = str_replace($sep."$", "", $header_insert);
			        $header_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header_insert);
			        $header_insert .= "\t";
			        print(trim($header_insert));
			        echo '</tr>';
			        print("\n");
				}
		        $schema_insert = str_replace($sep."$", "", $schema_insert);
		        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
		        $schema_insert .= "\t";
		        print(trim($schema_insert));
		        $schema_insert .= "</tr>";
		        print "\n";
			}
			if ($data!=NULL and $data!=false)
				print "\n\n<tr></tr><tr></tr>";
			// print "\n\n";
        }
        echo '</table>';
        die();
        
        // redirect("web/document/statistik");
    }

    public function exportListAll(){
    	parent::list_all();

    	$filename = "list_all_documents";  
		$file_ending = "xls";

		//header info for browser
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");    
		header("Content-Disposition: attachment; filename=$filename.xls");  
		header("Pragma: no-cache"); 
		header("Expires: 0");


    	echo '<table style="table-layout:fixed;overflow:hidden;" align="center" width="1000" height="300">';
		echo "<tr></tr>";
		echo "<tr><td colspan=4 style='font-weight:bold'>List Semua Dokumen ".$comp_name."</td></tr>";
		echo "<tr></tr>";
		echo "<tr><td>Dibuat Tanggal</td><td>".date("d M Y")."</td></tr>";
		echo "<tr><td>Dibuat Oleh</td><td colspan=3>".SessionManagerWeb::getName()."</td></tr>";
		echo "<tr></tr>";

        $column_names = array(
    		"updated_at" => "Last Modified",
    		"type_name" => "Document Type",
    		"document_id" => "ID",
    		"code" => "Document Code",
    		"title" => "Title",
    		"revision" => "Revision",
    		"prosesbisnis_name" => "Business Process", 			// Tidak ada di DB
    		"requirements" => "ISO Requirements",
    		"clauses" => "Clauses",
    		"related_documents" => "Related Documents",
    		"published_at" => "Published Date",
    		"validity_date" => "Evaluation Schedule",
    		"name" => "Drafter",
    		"drafter_date" => "Drafter Date",
    		"creator_name" => "Creator",
    		"created_date" => "Created Date",
    		"reviewers" => "Reviewer",
    		"document_controller" => "Verified by Document Controller",
    		"verified_date" => "Verified Date",
    		"approver_name" => "Approver",
    		"approved_date" => "Approved Date",
    		"unitkerja_name" => "Work Unit",
    		"department_name" => "Departement",
    		"retention" => "Retention Time",
    		"task" => "Status",
    		"description"	=> "Change Log"
    	);

        /*******Start of Formatting for Excel*******/   
		$sep = "\t"; 
		
		$header_insert = '<tr>'; 
		foreach ($this->data['data'] as $k_data => $v_data) {

			$schema_insert = "<tr>";
			foreach ($column_names as $k => $v) {
				if ($k_data==0){
					$header_insert .= "<td style='border: 1px solid black;background-color:yellow;font-weight:bold;max-height:300px;overflow:hidden'>$v</td>".$sep;
					// $header_insert .= "$v".$sep;
					// echo "<b>$v</b>".$sep;
				}
                $schema_insert .= "<td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>$v_data[$k]</td>".$sep;

                // $schema_insert .= "$v_data[$k]".$sep;
			}
			if ($k_data==0){
				$header_insert = str_replace($sep."$", "", $header_insert);
		        $header_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $header_insert);
		        $header_insert .= "\t";
		        print(trim($header_insert));
		        echo '</tr>';
		        print("\n");
			}
	        $schema_insert = str_replace($sep."$", "", $schema_insert);
	        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
	        $schema_insert .= "\t";
	        print(trim($schema_insert));
	        $schema_insert .= "</tr>";
	        print "\n";
		}
		if ($data!=NULL and $data!=false)
			print "\n\n<tr></tr><tr></tr>";
		// print "\n\n";

        echo '</table>';
    }

    public function getActors($document_id, $tasks=array()){
    	$this->load->model("Workflow_model");
    	$this->load->model("Document_activity_model");
    	$this->load->model("User_model");
    	$this->load->model("Document_reviewer_model");

    	$actors = array();

    	foreach ($tasks as $k_task => $v_task) {
    		$actor="";
    		if ($k_task!='review'){
    			$actor = $this->Document_activity_model->getLastData("user_id", "where document_id=".$document_id." and workflow_id in ('".Workflow_model::PENGAJUAN."','".Workflow_model::REVISI."') and workflow_urutan=".$v_task." and type='H'", "order by id desc");
				if ($actor==NULL or $actor==false){
					$actor="";
					if ($k_task=='verification'){
						$company = $this->model->filter("where \"document_id\"=$document_id ")->getOne("\"company\"");
						$chief_document_controllers = $this->User_model->getDocumentController($company, 1);
						if ($chief_document_controllers==NULL or $chief_document_controllers==false){
							$actor = "";
						} else {
							$actor = $this->User_model->filter("where \"user_id\"='".$chief_document_controllers[0]['user_id']."' ")->getOneFilter('name');
						}
					} else if ($k_task=='approval'){
						$actor = $this->model->filter("where \"document_id\"=$document_id ")->getOne("\"approver_name\"");
						if ($actor==NULL or $actor==false){
							$actor = "";
						}
					}
					
				} else {
					$actor = $this->User_model->filter("where \"user_id\"='".$actor['USER_ID']."' ")->getOneFilter('name');
				}
				
				$actors[$k_task] = $actor;
    		} else {
    			$reviewers = $this->Document_reviewer_model->filter("where document_id=$document_id and workflow_id=".Workflow_model::PENGAJUAN)->getAllFilter();
    			$reviewer_string ='';
    			foreach ($reviewers as $v_reviewer) {
    				$reviewer_string .= $this->User_model->filter("where \"user_id\"=".$v_reviewer['document_reviewer']." ")->getOneFilter('name').';';
    			}
    			$actors[$k_task] = $reviewer_string;
    		}
			
    	}
		return $actors;
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Ada tabel Role ItemRole Item

class Document_iso extends WEB_Controller {

    protected $title = 'Document Iso';

    /**
     * Halaman daftar post public
     * @param int $page
     */

    public function index() {
    	$data = $this->model->getAll();
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function add($name) {
        $name = urldecode($name);
        $this->model->add($name);
        redirect($this->ctl );        
    }

    public function ajaxadd() {
        $name = $this->input->post('iso', true);
        $code = $this->input->post('iso_code', true);
        $this->model->add($name, $code);
        // redirect($this->ctl );        
    }

    public function edit($id=NULL){
    	if (isset($id))
            $data = $this->model->getBy($id);
        else
            $data = array();
        if ($data!=NULL) {
            $this->load->model('Document_iso_klausul_model', 'klausul');
            $iso_id = $data['id'];
            $data['klausul']=  Util::toList($this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"" ))->filter(" iso_id=$iso_id ")->getAll(),'id');
        }
        $this->data['var_klausul'] = Util::toMap($this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"" ))->getAll(),'id', 'klausul');
    	$this->data['data'] = $data;
    	$this->template->viewDefault($this->view, $this->data);
    }

    public function delete($id) {
        $delete = $this->model->filter(" id=$id ")->delete();

        if ($delete === true) {
            $ok = true;
            $msg = 'Successfully deleted the document iso';
        } else {
            $ok = false;
            $msg = 'Failed to delete document iso';
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl);
    }

    public function deleteClause($id_clause, $id_iso){
        $this->load->model('Document_iso_klausul_model', 'klausul');
        // echo $id_clause;
        $delete = $this->klausul->filter('id='.$id_clause)->delete();
        if ($delete === true) {
            $ok = true;
            $msg = 'Successfully deleted the document iso clause';
        } else {
            $ok = false;
            $msg = 'Failed to delete document iso clause';
        }
        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl.'/editClause/'.$id_iso);
    }
    
    public function update($id = null) {
        $data['iso'] = $this->input->post('iso', true);
        $update = $this->model->save($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Successfully changed Iso Document';
            unset($data);
            $this->load->model('Document_iso_klausul_model', 'klausul');
            
            // $this->klausul->filter(" iso_id=$id ")->delete();

            $klausul = $this->input->post('klausul_iso', true);
            $klausul_diff = array_diff(Util::toList($this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"" ))->filter(" iso_id=$id ")->getAll(),'id'),$klausul);
            $klausul_diff_str = implode(',',$klausul_diff);
            $this->klausul->filter(" iso_id=$id and id in ($klausul_diff_str) ")->delete();
            foreach ($klausul as $key => $value) {
                $is_exist = $this->klausul->filter(" iso_id=$id and id='$value' ")->getOne('1');
                if (!$is_exist){
                    $data['iso_id'] = $id;
                    $data['klausul'] = $value;
                    dbInsert('DOCUMENT_ISO_KLAUSUL', $data);
                }
            }
        } else {
            $ok = false;
            if (!is_string($update)) {
                $validation = $this->model->getErrorValidate();
                if (empty($validation))
                    $msg = 'Failed to change Document iso';
                else
                    $msg = implode('<br />', $validation);
            } else
                $msg = $update;
        }

        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl );
    }

    public function editClause($id){
        if (isset($id))
            $data = $this->model->getBy($id);
        else
            $data = array();
        if ($data!=NULL) {
            $this->load->model('Document_iso_klausul_model', 'klausul');
            $iso_id = $data['id'];
            $data['klausul']=  $this->klausul->column(array("id" =>"\"id\"", "klausul" => "\"klausul\"", "code" => "\"code\"" ))->filter(" iso_id=$iso_id ")->order('code')->getAll();
        }
        $this->data['data'] = $data;
        // echo "<pre>";print_r($this->data['data']);echo "</pre>";

        $buttons = array();
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack('.$id.')');
        $this->data['buttons'] = $buttons;


        $this->template->viewDefault($this->view, $this->data);
    }

    public function formEditClause($id_clause){
        $this->load->model('Document_iso_klausul_model', 'klausul');
        $data = $this->klausul->filter('id='.$id_clause)-> getAll();

        $this->data['data']= $data[0];
        $buttons = array();
        $buttons[] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack('.$data[0]['iso_id'].')');
        $this->data['buttons'] = $buttons;
        $this->template->viewDefault($this->view, $this->data);
        // echo "<pre>";print_r($data);echo "</pre>";
    }

    public function updateClause(){
        $this->load->model('Document_iso_klausul_model', 'klausul');
        $clause = $this->input->post('clause');
        $code = $this->input->post('code');
        $data = array(
            'klausul'   => $clause,
            'code'      => $code
        );
        $id = $this->input->post('id_clause');
        $update = $this->klausul->update($id, $data);
        if ($update === true) {
            $ok = true;
            $msg = 'Successfully changed Iso Clause Document';
        }
        else{
            $ok = false;
             $msg = 'Failed changed document iso clause';
        }
        SessionManagerWeb::setFlashMsg($ok, $msg);
        redirect($this->ctl.'/formEditClause/'.$id);
    }
}

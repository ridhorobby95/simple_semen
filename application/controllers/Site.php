<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    /**
	 * Controller Site
	 * @author Sevima
	 * @version 1.0
	 */ 
    class Site extends WEB_Controller {
		protected $ispublic = true;
		
        /**
		 * Halaman login user
		 */
        public function index() {

			// jika sudah login lempar ke post
			if(SessionManagerWeb::isAuthenticated()) {
				SessionManagerWeb::setFlashMsg(false,'You already logged in!');
				redirect($this->getCTL('document/dashboard'));
			}
			
			// lakukan validasi form
            $this->form_validation->set_rules('username','Username','required');
			$this->form_validation->set_rules('password','Password','required');
			
			if ($this->form_validation->run() === true) {
				list($ok,$msg,$user) = AuthManagerWeb::login($this->input->post('username'),$this->input->post('password'));
				
				if($ok) {
					// simpan cookie
					setcookie('username',$user['username'],time() + 2592000,'/'); // 30 hari
					SessionManagerWeb::setUser($user);
					$variables = $this->getVariables();
					SessionManagerWeb::setVariables($variables);
					redirect($this->getCTL('document/index?selected_menu=all'));
				}
				else {
					SessionManagerWeb::setFlash(array('errmsg' => array($msg)));
					redirect($this->ctl);
				}
			}
			else {
				$errstr = validation_errors();
				if(!empty($errstr)) {
					SessionManagerWeb::setFlash(array('errmsg' => array($errstr)));
					redirect($this->ctl);
				}
			}
			
			// load semua user
			// $this->load->model('User_model');
			
			// $users = $this->User_model->order_by('name')->get_many_by(array('status' => Status::ACTIVE));
			// $userck = $_COOKIE['username'];
			
			// foreach($users as $user) {
			// 	if($user['username'] == $userck) {
			// 		$this->data['user'] = $user;
			// 		break;
			// 	}
			// }

			        	// test db connect
			$sql = "select 1 from document_types";
        	if (!dbGetOne($sql)){
				SessionManagerWeb::setFlashMsg(false,'Database not Connected!');
				$this->data['title'] = 'Document Management Semen Indonesia';
				$this->data['data'] = $users;
				$this->load->view(static::VIEW_PATH.$this->view,$this->data);
			} else {
				$this->data['title'] = 'Document Management - Semen Indonesia';
				$this->data['data'] = $users;

				$this->load->view(static::VIEW_PATH.$this->view,$this->data);
			}
			
	        
			
        }
    }
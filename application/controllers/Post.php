<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends PrivateWebController {

    public function index() {
        $posts = $this->model->getPublic($this->page);

        $this->setData(array(
            'title' => 'Pengumuman',
            'posts' => $posts
        ));

        $this->template->render('post/index', $this->data);
    }

    public function create() {
        $data = $this->postData;
        $data['user_id'] = $this->user['id'];
        $data['image'] = isset($_FILES['image']) ? $_FILES['image']['name'] : NULL;
        $data['video'] = isset($_FILES['video']) ? $_FILES['video']['name'] : NULL;
        $data['file'] = isset($_FILES['file']) ? $_FILES['file']['name'] : NULL;

        $insert = $this->model->create($data);
        if ($insert === TRUE) {
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil membuat postingan'));
        } elseif (is_string($insert)) {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, $insert));
        } else {
            $validation = $this->model->getErrorValidate();
            if (empty($validation)) {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Gagal membuat postingan'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::VALIDATION_ERROR, NULL, $validation));
            }
        }
    }

    /**
     * Digunakan untuk mengambil post dari group dengan ID group
     */
    public function group($id, $page = 0) {
        $posts = $this->model->getGroup($id, $page);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

    /**
     * Digunakan untuk mengambil post public, dari group yang diikuti, dan dari tag friend
     */
    public function me() {
        $posts = $this->model->getMe($this->user['id'], $this->page);

        //SET RESPONSE
        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $posts);
    }

    /**
     * Digunakan untuk mengambil post dengan ID post beserta commentnya
     */
    public function detail($id) {
        //SET READ NOTIFICATION
        $this->load->model('Notification_model');
        $this->load->model('Notification_user_model');
        $this->Notification_user_model->setAsRead($this->user['id'], Notification_model::TYPE_POST_DETAIL, $id);

        $post = $this->model->with(array('Category', 'Comment' => 'User', 'User', 'Post_user', 'Group'))->get($id);

        $this->setResponse($this->setSystem(ResponseStatus::SUCCESS), $post);
    }

    public function delete($id) {
        $post = $this->model->get($id);
        if ($post['userId'] == $this->user['id']) {
            $delete = $this->model->delete($id);
            if ($delete === TRUE) {
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Berhasil menghapus postingan'));
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Gagal menghapus postingan"));
            }
        } else {
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, "Tidak dapat menghapus postingan ini"));
        }
    }

}

?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . 'update/' . $data['id'], array('id' => 'form_data')) ?>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Document Iso</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'iso', 'value' => $data['iso'], 'class' => 'form-control input-sm', 'id' => 'iso')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Clause Iso Document</label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('klausul_iso[]', $var_klausul, $data['klausul'], 'class="form-control input-sm select2 klausul_iso" multiple="multiple" style="border: 1px solid; width:100%;"') ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_excel()" type="button" data-toggle="tooltip" title="Update Document" style="margin-left:0.5%;">
                            <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
                            <span>Upload Excel</span>
                        </button>
                        <a href="<?= site_url('web/document_iso/editClause/'.$data['id']) ?>"><button id="activeEdit" class="btn btn-sm btn-secondary btn-rad"  data-toggle="tooltip" type="button" title="Edit Form" style="margin-left:0.5%;">
                            <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                            <span>Edit Clause</span>
                        </button></a>
                        <button id="btn_simpan" type="submit" class="btn btn-sm btn-success btn-rad pull-right" title="Save" style="margin-left:0.5%;">
                            <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                            <span>Save</span>
                        </button>
                        <!-- <a class="post-right pull-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a> -->
                   

                    </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<div id="upload_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Upload Excel</h4>
                <p class="sub-one-revisi">Upload Excel</p>
            </div>
            <div class="modal-body">
                <form id="form_data" method="POST" action="<?php echo site_url('web/document_iso_klausul/uploadklausul') ?>" >
                    <input type="hidden" name="filename" id="filename" value="">
                    <input type="hidden" name="id_iso" value="<?php echo  $data['id'] ?>">
                    <div class="row bord-bottom">
                        <label for="type" class="col-sm-4">Document Iso</label>
                        <div class="col-sm-8">
                            <label><?= $data['iso'] ?></label>
                        </div>
                    </div>
                    <div class="row bord-bottom">
                        <label for="migration data" class="col-sm-4">File Excel (1 file)</label>
                        <div class="col-sm-8">
                            <div class="btn btn-default btn-sm" onclick="$('#file_metadata_upload').click()"><span class="fa fa-file" style="cursor:pointer;color:#777;"></span> &nbsp;File Metadata</div>
                        </div>
                    </div>
                    <div class="row bord-bottom">
                        <label for="migration data" class="col-sm-4">Filename</label>
                            <div class="col-sm-8" style="padding-left: 0px">
                                <div id="metadata_preview" name="metadata_preview" class="col-md-12"><?= ($metadata=='') ? '-' : $metadata ?></div>
                            </div>
                    </div>
                    <div class="row bord-bottom">
                        <label for="migration_data" class="col-sm-2">Data Excel</label>
                            <div class="col-sm-10" style="padding-left: 0px">
                                <div id="data_preview" name="data_preview" class="col-md-12">
                                    <table >
                                        <thead>
                                            <tr>
                                                <th align="center" width="10%">No</th>
                                                <th align="center" width="60%">Clause</th>
                                                <th align="center" width="45%">Clause Code</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tampil">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                   
                
                
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type='submit' class="btn btn-sm btn-secondary btn-rad"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Upload</button>
            </div>
            </form>
            <form method="post" name="file_metadata_form" id="file_metadata_form" enctype="multipart/form-data" action="<?= site_url('web/migration/masterform') ?>" class="hidden">
                <input type="file" id="file_metadata_upload" name="file_metadata_upload" class="hidden">
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>
<script type="text/javascript">
    var selected_color = "<?= $data['color'] ? $data['color'] : 0 ?>";
    var selected_logo = "<?= $data['logo'] ? $data['logo'] : 0 ?>";
    var name = "<?= $data['name'] ?>";
    $(function() {
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

    function upload_excel(){
        $("#upload_modal").modal('show');
    }

    $(".klausul_iso").select2({
        placeholder: 'Input Klausul Iso...',
        minimumResultsForSearch: Infinity,
        tags: true,
        width: '100%',
    });
    $(".klausul_iso").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);

        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });


    $("#file_metadata_form :input").change(function() {
        var filename = document.getElementById("file_metadata_upload").value;
        var text = filename.substring(filename.lastIndexOf("\\") + 1, filename.length);
        document.getElementById("filename").value = text;
    });
     $('#file_metadata_upload').on('change', function() {
        $('#file_metadata_form').ajaxForm({
            target: '#metadata_preview',

            error: function(e) {
                //target: '#images_preview';
                alert('Terjadi kesalahan. Silakan diulangi lagi');
            },
            success: function(){
                var filename = document.getElementById("file_metadata_upload").value;
                var text = filename.substring(filename.lastIndexOf("\\") + 1, filename.length);
                $.ajax({
                    url : '<?= site_url('web/document_iso_klausul/getDataExcel') ?>/'+text,
                    type: 'post',
                    cache: false,
                    data: {'filename':text},
                    success: function(resp){
                        json_resp = JSON.parse(resp);
                        var tes = '';
                        for(var key in json_resp){
                            var no = key+1;
                            tes = tes+"<tr><td>"+no+"</td><td>"+json_resp[key].klausul+"</td><td>"+json_resp[key].code_klausul+"</td></tr>";
                        }
                        
                        document.getElementById('tampil').innerHTML = tes ;
                        // alert(json_resp[0].KLAUSUL);
                        // alert(resp);
                        // $("#creator_id").select2({
                        //     placeholder : 'Choose creator',
                        //     data:json_resp 
                        // });
                        // $('#creator_id').val(response.creator.user_id).trigger('change');
                    }
                });
            }
        }).submit();
    });
</script>
<script>
    $("#mainContent").addClass('col-sm-12');
    $("#mainContent").removeClass('col-sm-9');
</script>

<br>
<form action="<?= site_url('web/atasan') ?>" class="search-form" style="position:relative ;width:100%;bottom:5px">
    <div class="form-group has-feedback">
        <label for="company" class="sr-only">Company</label>
        <?php echo form_dropdown('company', $company, $_GET['company'], 'id="company" class="form-control input-sm" style="width:20%; font-size: 12px; letter-spacing: 0.5px;display:inline;margin-bottom:10px" onchange="goSearch()" '); ?>
        <label for="search" class="sr-only">Search</label>
        <input type="text" class="form-control" name="search" id="search" placeholder="Search Employee Number" value="<?= $_GET['search'] ?>">
        <a href="javascript:goSearch()"><i class="glyphicon glyphicon-search form-control-feedback" style="position:absolute;left:97%;top:70%"></i></a>
    </div>       
</form>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Employee Number</th>
                                        <th class="text-center">Employee</th>
                                        <th class="text-center">Work Unit</th>
                                        <th class="text-center">1st Superior</th>
                                        <th class="text-center">Work Unit</th>
                                        <th class="text-center">PGS?</th>
                                        <th class="text-center">2nd Superior</th>
                                        <th class="text-center">Work Unit</th>
                                        <th class="text-center">PGS?</th>
                                        <th class="text-center">Already Sync?</th>
                                        <?php if (SessionManagerWeb::isAdministrator()) { ?>
                                        <th class="text-center" style="width:10%">Action</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $v) {
                                        ?>
                                        <tr>
                                            <td><?= $v['k_nopeg'] ?></td>
                                            <td><?= $v['employee_name'] ?></td>
                                            <td><?= $v['unitkerja_name'] ?></td>
                                            <td><?= $v['atasan1_name'] ?></td>
                                            <td><?= $v['atasan1_unitkerja_name'] ?></td>
                                            <td><?= $v['atasan1_pgs'] ?></td>
                                            <td><?= $v['atasan2_name'] ?></td>
                                            <td><?= $v['atasan2_unitkerja_name'] ?></td>
                                            <td><?= $v['atasan2_pgs'] ?></td>
                                            <?php if (SessionManagerWeb::isAdministrator() or (SessionManagerWeb::isAdminUnit() and $v['company']==SessionManagerWeb::getVariablesIndex('mycompany'))) { ?>
                                                <td><?= $v['sync'] ?></td>
                                                <td style="text-align: center">
                                                    <a class="text-primary" data-toggle="tooltip" title="Sync" type="button" onClick="goSync('<?= $v['k_nopeg'] ?>')" style="cursor:pointer">
                                                        <i class="fa fa-refresh"></i>
                                                    </a>
                                                    <a class="text-success" data-toggle="tooltip" title="Edit" type="button" onClick="goEdit('<?= $v['k_nopeg'] ?>')" style="cursor:pointer">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a class="text-danger" data-toggle="tooltip" title="Delete" type="button" onClick="goDelete('<?= $v['k_nopeg'] ?>')" style="cursor:pointer">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            <?php } ?>
                                            
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function goExport() {
    location.href = "<?php echo site_url($path . $class . '/add_import') ?>";
}

function goAdd() {
    location.href = "<?php echo site_url($path . $class . '/add') ?>";
}

function goBack(){
    location.href = "<?php echo site_url($path . 'setting') ?>";
}

function goEdit(id=null){
    if (confirm("Are you sure to edit this superior data?")){
        location.href = "<?php echo site_url($path . $class . '/edit') ?>/"+id;
    }
}

function goSync(id=null){
    if (confirm("Are you sure to sync this superior data? The Previous superior data will be deleted")){
        location.href = "<?php echo site_url($path . $class . '/sync') ?>/"+id;
    }
}

function goDelete(id=null){
    if (confirm("Are you sure to delete this superior data?")){
        location.href = "<?php echo site_url($path . $class . '/delete') ?>/"+id;
    }
}

function goSearch(){
    var search = document.getElementById('search').value;
    var company = document.getElementById('company').value;
    location.href = "<?= site_url('web/atasan') ?>?search="+search+"&company="+company;
}

</script>
<?php
    $readonly = isset($id) ? '' : '';
    $disabled = isset($id) ? '' : '';
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-11">
                        <div class="row bord-bottom">
                            <label for="id" class="col-sm-4">Employee Number</label>
                            <div class="col-sm-8">
                                <input name="id" value="<?= $data['id'] ?>" required class="form-control input-sm" placeholder="fill the employee code" id="id" <?= $readonly ?>>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <input name="name" placeholder="Fill the Employee's Name" value="<?= $data['name'] ?>" required class="form-control input-sm" id="name" <?= $readonly ?>>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="email" class="col-sm-4">Email</label>
                            <div class="col-sm-7">
                                <input type="email" name="email" placeholder="Fill the Email then Check" value="<?= $data['email'] ?>" required class="form-control input-sm" id="email" <?= (isset($id)) ? 'readonly' : '' ?>>
                                <label class="hidden" style="font-size:10px;color:red" id="label_email_exist">
                                    <b style="color:red">*</b>Email Already Exist!
                                </label>
                                <label class="hidden" style="font-size:10px;color:green" id="label_email_available">
                                    <b style="color:green">*</b>Available
                                </label>
                                <label class="hidden" style="font-size:10px;color:orange" id="label_email_warning">
                                    <b style="color:orange">*</b>Warning! this email cant be used as username (in front of "@") because username already exist.
                                </label>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="checkEmail()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="subgroup_id" class="col-sm-4">Eselon</label>
                            <div class="col-sm-8">
                                <?php echo form_dropdown('eselon_code',$eselons, $data['eselon_code'], 'id="eselon_code" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required '.$disabled); ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="jabatan_id" class="col-sm-4">Position Code</label>
                            <div class="col-sm-7">
                                <input name="jabatan_id" placeholder="Fill the Code then Check" value="<?= $data['jabatan_id'] ?>" required class="form-control input-sm" id="jabatan_id" <?= $readonly ?>>
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getPositionName()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="div_position_name">
                            <label for="jabatan_name" class="col-sm-4">Position Name</label>
                            <div class="col-sm-8">
                                <input name="jabatan_name" class="form-control input-sm" id="jabatan_name" readonly>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="div_unitkerja_name">
                            <label for="unitkerja_name" class="col-sm-4">Work Unit</label>
                            <div class="col-sm-8">
                                <input name="unitkerja_name" class="form-control input-sm" id="unitkerja_name" readonly>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="div_company_name">
                            <label for="company_name" class="col-sm-4">Company</label>
                            <div class="col-sm-8">
                                <input name="company_name" class="form-control input-sm" id="company_name" readonly>
                            </div>
                        </div>
                        
                        <div class="row bord-bottom">
                            <label for="tgl_masuk" class="col-sm-4">Begin Date</label>
                            <div class="col-sm-8">
                                <input type="text" data-provide="datepicker" name="tgl_masuk" id="tgl_masuk" value="<?= $data['tgl_masuk'] ?>" class="form-control input-md inputplus" placeholder="01-JAN-2018" <?= $readonly ?> required>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="tgl_pensiun" class="col-sm-4">End Date</label>
                            <div class="col-sm-8">
                                <input type="text" data-provide="datepicker" name="tgl_pensiun" id="tgl_pensiun" value="<?= $data['tgl_pensiun'] ?>" class="form-control input-md inputplus" placeholder="31-DES-9999" <?= $readonly ?>>
                                <label style="font-size:10px">
                                    <b style="color:red">*</b> Keep blank if there's no End Date
                                </label>
                            </div>
                        </div>
                        
                        <?php if (isset($id) and 1==0) { ?>
                            <div class="row bord-bottom">
                                <label for="account number" class="col-sm-4">Status</label>
                                <div class="col-sm-8">
                                    <input name="status" value="<?= $data['status'] ?>" required class="form-control input-sm" id="status" <?= $readonly ?>>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-11 text-right">
                        <a class="post-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $.fn.datepicker.defaults.format = "dd-M-yyyy";

    $(document).ready(function() {
        $("#eselon_code").select2({
            placeholder: 'Choose Group of Position...',
            width: '100%'
        });
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function getPositionName(){        
        var jabatan_id = document.getElementById("jabatan_id").value;
        
        $.ajax({
            url : "<?php echo site_url('/web/jabatan/ajaxGetPosition');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'id':jabatan_id},
            success:function(respon){
                $("#div_position_name").removeClass("hidden");
                if (respon.position==null || respon.position==false){
                    document.getElementById("jabatan_name").value = "Not Found!";
                } else {
                    document.getElementById("jabatan_name").value = respon.position;    
                }

                $("#div_unitkerja_name").removeClass("hidden");
                if (respon.unitkerja==null || respon.unitkerja==false){
                    document.getElementById("unitkerja_name").value = "Not Found!";
                } else {
                    document.getElementById("unitkerja_name").value = respon.unitkerja;    
                }

                $("#div_company_name").removeClass("hidden");
                if (respon.company==null || respon.company==false){
                    document.getElementById("company_name").value = "Not Found!";
                } else {
                    document.getElementById("company_name").value = respon.company;    
                }

            } 
        });
    }

    function checkEmail(){        
        var email = document.getElementById("email").value;
        
        $.ajax({
            url : "<?php echo site_url('/web/karyawan/ajaxCheckEmail');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'email':email},
            success:function(respon){
                if (respon=='0'){
                    $("#label_email_available").removeClass("hidden");
                    $("#label_email_exist").addClass("hidden");
                    $("#label_email_warning").addClass("hidden");
                } else if (respon=='1'){
                    $("#label_email_available").addClass("hidden");
                    $("#label_email_exist").removeClass("hidden");
                    $("#label_email_warning").addClass("hidden");
                } else {
                    $("#label_email_available").addClass("hidden");
                    $("#label_email_exist").addClass("hidden");
                    $("#label_email_warning").removeClass("hidden");
                }
            } 
        });
    }



    

</script>
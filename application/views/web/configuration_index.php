<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Number</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Data Type</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td class="text-left">
                                                <?= $v['name'] ?>
                                            </td>
                                            <td class="text-left">
                                                <?= $v['type'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                echo anchor('web/configuration/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


function goAdd() {
    location.href = "<?php echo site_url($path . $class . '/add') ?>";
}

</script>
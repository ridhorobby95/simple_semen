<div id="RelatedBox" class="modal fade">
    <div class="modal-dialog" style=" width:1200px !important;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-body row" style="margin-left:10px;">
                <div class="row col-sm-12">
                    <div class="col-sm-3" id="listDokumenTerkait">
                        <div id="divRelated" class="upload-new">
                    <div class="upload-new-input" style="margin-bottom: 5px;">
                        <i class="fa fa-search"></i>
                        <input type="text" id="titleSearch" name="titleSearch" class="form-control input-sm" placeholder="Title, Code or Work Unit" style="font-size:15px;display:inline;width:90%;margin-left:0px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
<!--                         <i class="fa"></i>
                        <input type="text" name="titleSearch" value="" class="upload-new-title form-control" id="titleSearch" placeholder="Document Title" autofocus="1" style="border-bottom: 1px solid #dedede; margin-top: 50px;"> -->
                    </div>
                    <div class="upload-new-input" style="margin-bottom: 5px;">
                        <i class="fa fa-file"></i>
                        <?php 
                            $types =array(""=>NULL);
                            foreach ($variables['document_types'] as $k => $v_type) {
                                $types[$v_type['id']] = $v_type['type'];
                            }
                            echo form_dropdown('typeSearch', $types, null, 'id="typeSearch" class="form-control input-sm select2" style="font-size:15px;display:inline;width:90%;margin-left:0px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;" '); ?>                    
                    </div>
                    
                    <div class="upload-new-input hidden" style="margin-bottom: 10px;">
                        <i class="fa fa-user"></i>
                        <input type="text" id="pemilikSearch" name="pemilikSearch" class="form-control input-sm" placeholder="Document Creator" style="font-size:15px;display:inline;width:90%;margin-left:0px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
                    </div>
                    <div class="upload-new-input" style="margin-bottom: 10px;display: none">
                        <i class="fa fa-folder"></i>
                        <select id="lokasiSearch" name="lokasiSearch" class="form-control input-sm select2" style="width:90%;">
                            <option></option>
                            <?php
                                foreach($lokasi as $obj){
                                    echo "<option value='$obj'>".$obj."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="upload-new-input hidden" style="margin-bottom: 10px;">
                        <i class="fa fa-folder"></i>
                        <select id="tagsSearch" name="tagsSearch[]" class="form-control input-sm select2 tag" style="width:90%;" multiple="multiple">
                            <?php foreach($tags as $obj){
                                    echo "<option value='".$obj['name']."'>".$obj['name']."</option>";
                            }?>
                        </select>
                    </div>
                    <a href="javascript:void(0)" class="col-md-12 btn editedTombolagi" style="margin-top:5px; letter-spacing: 1px;" onClick="searchRelated()">Search Document</a>
                </div>
                    </div>
                    <div class="col-sm-9" id="listDokumenTerkait">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                        <br>
                        <h4 id="RelatedTitle" class="modal-title" style="margin:0px 0px 0px 300px;display:inline;color:#616161; text-align:center; font-weight: bold;padding: 20px 0px; letter-spacing: 1px;">Related Document</h4>

                        <div style="margin-top: 10px;overflow-y: scroll;max-height:280px;">
                            <table id="tabelRelated" style="width:100%;">
                                <tr>
                                    <th style="display:none;"></th>
                                    <th style="display:none;"></th>
                                    <th style="color: #9E9E9E; letter-spacing: 1px;">Title</th>
                                    <th style="color: #9E9E9E; letter-spacing: 1px;">Type</th>
                                    <th style="color: #9E9E9E; letter-spacing: 1px;">Work Unit</th>
                                    <th style="color: #9E9E9E; letter-spacing: 1px;">Preview</th>
                                    <th style="color: #9E9E9E; letter-spacing: 1px;">Related?</th>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
                
                <hr>
                
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <div class="col-md-7"></div>
                <button type="button" class="btn tombolBarui" data-dismiss="modal" style="letter-spacing: 1px; margin-right: 0px;">Cancel</button>
                <button id="simpanRelated" type="button" class="btn tombolBarui22" data-dismiss="modal" style="margin:0px;margin-right:20px; margin-top:15px; margin-bottom:15px; letter-spacing: 1px;">Save</button>
            </div>
        </div>
    </div>
</div>
<style>
#tabelRelated th{
    border-bottom: 3px solid #ddd;
}
#tabelRelated td{
    text-align:left;
}
</style>
<script>
function upload2(){
    document.getElementById("dropbox").click();
}

$(document).ready(function() {
    $("#typeSearch").select2({
        placeholder: 'Document type'
    });
});

var index = 0;
function addNewInput(){
    var parent = document.getElementById("listDokumenTerkait");
    var newInput = document.createElement("input");
    newInput.id = "related" + index;
    newInput.name = "related[]";
    newInput.type = "text";
    newInput.className = "form-control";
    newInput.style = "display: inline;font-size: 12px;margin-bottom:8px;";
    parent.appendChild(newInput);
    $( "#related" + index ).autocomplete({
        source: "<?php echo site_url('web/document/allDocument'); ?>",
        minLength: 1,
        select: function( event, ui ) {
        },
        open: function (event, ui) {
            $(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
            $(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
        },
        close: function (event, ui) {
            },
             messages: {
                    noResults: '',
                    results: function() {}
                }            
        })
     .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<div>" )
            .data( "ui-autocomplete-item", item )
            .append( "<a style='margin-left:5px;'>"+ item.label+ "</a>" )
            .appendTo( ul );
        };
    index++;
}

function searchRelated(){
    var title = document.getElementById('titleSearch').value;
    var type = document.getElementById('typeSearch').value;
    var id = document.getElementById('dokumen').textContent;
    var tags = $('#tagsSearch').select2("val");
    var titleR = [];
    var temp = 0
    var tableR = document.getElementById('tabelRelated');
    var lenRow = tableR.rows.length;
    for(var y=1;y<lenRow;y++){
        var checked = $('input', tableR.rows[y].cells[6]).is(':checked');
        var isAsli = tableR.rows[y].cells[1].innerHTML;
        if(!checked && isAsli != "true"){
            tableR.deleteRow(y);
            y--;
            lenRow--;
        }
    }
    var http = new XMLHttpRequest();
    http.open("POST", "<?php echo site_url('/web/document/searchAdvance');?>", true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    var param = "";
    param = "title=" + title;
    param = param + "&type="+type;
    param = param + "&tags=" + JSON.stringify(tags);
    param += "&id=" + document.getElementById('dokumen').textContent;
    http.send(param);
    http.onload = function() {
        console.log('related:'+related_terpilih);
        // alert(http.responseText);
        var doc = JSON.parse(http.responseText);
        console.log(doc);
        var size = doc.length;
        var inner = "";
        var tableR = document.getElementById('tabelRelated');
        var lenRow = tableR.rows.length;
        var ins = 0;
        $("#data").remove();
        // console.log('udah remove');
        inner += "<tbody id='data'>";
        for(var i=0;i<size;i++){
            var id = doc[i].document_id;
            for(var y=1;y<lenRow;y++){
                // var id2 = tableR.rows[y].cells[0].innerHTML;
                // if(id2 == id){
                //     ins = 1;
                //     break;
                // }
            }
            var title = doc[i].title;
            if(ins == 1)
                ins = 0;
            else{
                if (doc[i].type_name) {
                    jenis = doc[i].type_name;
                } else {
                    jenis = '-';
                }
                inner += "<tr>";
                inner += "<td style='display:none;'>" + doc[i].document_id+ "</td>";
                inner += "<td style='display:none;'>false</td>";
                inner += "<td id='titleRelated' name='titleRelated[]'>" + doc[i].title + "</td>";
                inner += "<td>" + jenis + "</td>";
                inner += "<td>" + doc[i].unitkerja_name + " - "+doc[i].company_name+"</td>";
                inner += "<td><button class='btn btn-success' onClick='viewDocument(" + id + ")'><i class='fa fa-eye' aria-hidden='true' style='float:middle;'></i></button></td>";
                
                console.log(doc[i].document_id);

                if(related_terpilih.length == 0){
                    console.log('kosong');
                    inner += "<td><input type='checkbox' name='vehicle' value='Terkait'></td>";
                }
                else if($.inArray(doc[i].document_id, related_terpilih) !== -1){
                    console.log('tidak kosong');
                    inner += "<td><input type='checkbox' name='vehicle' value='Terkait' checked></td>";
                }
                else{
                    inner += "<td><input type='checkbox' name='vehicle' value='Terkait'></td>";
                }
                // inner += "<td><input type='checkbox' name='vehicle' value='Terkait'></td>";
                inner += "</tr>";
            }
        }
        if(size <=0 || doc==false){
            inner += "<tr style='padding:5px'>";
            inner += "<td style='display:none;'>-2</td>";
            inner += "<td colspan=6 style='background-color:#FFECB3;color:#E65100;text-align:center;'><b>No Documents</b></td></tr>";
        }
        inner += "</tbody>";
        tableR.innerHTML += inner;
        // for(var y=1;y<lenRow;y++){
        //     $('input', tableR.rows[y].cells[6]).attr("checked", true);
        // }
    }
}

function searchRelated2(){
    var title = document.getElementById('titleSearch').value;
    var pemilik = document.getElementById('pemilikSearch').value;
    // var klasifikasi = document.getElementById('klasifikasiSearch').value;
    var id = document.getElementById('dokumen').textContent;
    var lokasi = document.getElementById('lokasiSearch').value;
    var tags = $('#tagsSearch').select2("val");
    var titleR = [];
    var temp = 0
    var tableR = document.getElementById('tabelRelated');
    var lenRow = tableR.rows.length;
    for(var y=1;y<lenRow;y++){
        var checked = $('input', tableR.rows[y].cells[6]).is(':checked');
        var isAsli = tableR.rows[y].cells[1].innerHTML;
        if(!checked && isAsli != "true"){
            tableR.deleteRow(y);
            y--;
            lenRow--;
        }
    }
    var http = new XMLHttpRequest();
    http.open("POST", "<?php echo site_url('/web/document/searchAdvance');?>", true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    var param = "";
    if(title != "")
        param = "title=" + title;
    else
        param = "title=_miss_";
    if(pemilik != "")
        param = param + "&pemilik=" + pemilik;
    else
        param = param + "&pemilik=a";
    if(lokasi != "")
        param = param + "&lokasi=" + lokasi;
    else
        param = param + "&lokasi=a";
    // if(klasifikasi != "")
    //     param = param + "&klasifikasi=" + klasifikasi;
    // else
    //     param = param + "&klasifikasi=a";
    param = param + "&tags=" + JSON.stringify(tags);
    param += "&id=" + document.getElementById('dokumen').textContent;
    http.send(param);
    http.onload = function() {
        // alert(http.responseText);
        var doc = JSON.parse(http.responseText);
        var size = doc.length;
        var inner = "";
        var tableR = document.getElementById('tabelRelated');
        var lenRow = tableR.rows.length;
        var ins = 0;
        for(var i=0;i<size;i++){
            var id = doc[i].document_id;
            for(var y=1;y<lenRow;y++){
                var id2 = tableR.rows[y].cells[0].innerHTML;
                if(id2 == id){
                    ins = 1;
                    break;
                }
            }
            var title = doc[i].title;
            if(ins == 1)
                ins = 0;
            else{
                if (doc[i].type_name) {
                    jenis = doc[i].type_name;
                } else {
                    jenis = '-';
                }
                inner += "<tr>";
                inner += "<td style='display:none;'>" + id+ "</td>";
                inner += "<td style='display:none;'>false</td>";
                inner += "<td id='titleRelated' name='titleRelated[]'>" + title + "</td>";
                inner += "<td>" + jenis + "</td>";
                inner += "<td>" + doc[i].username + "</td>";
                inner += "<td><button class='btn btn-success' onClick='viewDocument(" + id + ")'><i class='fa fa-eye' aria-hidden='true' style='float:middle;'></i></button></td>";
                inner += "<td><input type='checkbox' name='vehicle' value='Terkait'></td>";
                inner += "</tr>";
            }
        }
        if(size <=0){
            inner = "<tr style='padding:5px'>";
            inner += "<td style='display:none;'>-2</td>";
            inner += "<td colspan=6 style='background-color:#F00;color:#FFF'><b>Tidak ada dokumen </b></td></tr>";
        }
        tableR.innerHTML += inner;
        for(var y=1;y<lenRow;y++){
            $('input', tableR.rows[y].cells[6]).attr("checked", true);
        }
    }
}
function editRelated(){
    var related = [];
    console.log('masuk fungsi ini dong');
    var tableR = document.getElementById('tabelRelated');
    var lenRow = tableR.rows.length;
    var temp = 0;
    for(var y=1;y<lenRow;y++){
        var checked = $('input', tableR.rows[y].cells[6]).is(':checked');
        var id = tableR.rows[y].cells[0].innerHTML;
         if(checked){
            related[temp] = id;
            temp++;
        }
    }
    var param = "relatedEdit=" + JSON.stringify(related);
    param += "&id=" + document.getElementById('dokumen').textContent;
    var http = new XMLHttpRequest();
    http.open("POST", "<?php echo site_url('/web/document/editRelated');?>", true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    http.send(param);
    http.onload = function() {
        // alert(http.responseText);
        location.reload();  
    }
}
$("#pemilikSearch").autocomplete({
    source: "<?php echo site_url('web/document/allUser'); ?>",
    minLength: 1,
    select: function( event, ui ) {
    },
    open: function (event, ui) {
        $(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
        $(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
    },
    close: function (event, ui) {
    },
    messages: {
        noResults: '',
        results: function() {}
    }            
})
.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
    return $( "<div>" )
    .data( "ui-autocomplete-item", item )
    .append( "<a style='margin-left:5px;'>"+ item.label+ "</a>" )
    .appendTo( ul );
};
</script>

<style type="text/css">
   .ui-autocomplete {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 16777270;
    float: left;
    display: none;
    min-width: 160px;   
    padding: 4px 0;
    margin: 0 0 10px 25px;
    list-style: none;
    background-color: #ffffff;
    border-color: #ccc;
    border-color: rgba(0, 0, 0, 0.2);
    border-style: solid;
    border-width: 1px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    *border-right-width: 2px;
    *border-bottom-width: 2px;
    cursor: pointer;
}

.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;
    text-decoration: none;
}

.ui-state-hover, .ui-state-active {
    color: #ffffff;
    text-decoration: none;
    background-color: #0088cc;
    border-radius: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    background-image: none;
}
</style>

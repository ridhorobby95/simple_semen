<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . 'update/' . $data['id'], array('id' => 'form_data')) ?>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Document type</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'type', 'value' => $data['type'], 'class' => 'form-control input-sm', 'id' => 'type')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Document Code</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'code', 'value' => $data['code'], 'class' => 'form-control input-sm', 'id' => 'code')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Need Reviewer ? </label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('need_reviewer', array( '0' => 'No', '1' => 'Yes'), $data['need_reviewer'], 'id="need_reviewer" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="retension" class="col-sm-3">Need Record Retension Period ? </label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('retension', array( '0' => 'No', '1' => 'Yes'), $data['retension'], 'id="retension" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="klausul" class="col-sm-3">Need Clausul ? </label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('klausul', array( '0' => 'No', '1' => 'Yes'), $data['klausul'], 'id="klausul" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                        $eselon = array(
                            "0" => "Tidak Ada",
                            '50' => "Eselon 5",
                            '40' => "Eselon 4",
                            '30' => "Eselon 3",
                            '20' => "Eselon 2",
                            '10' => "Eselon 1"
                        );
                    ?>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="eselon_drafter" class="col-sm-3">Drafter Min. Eselon </label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('eselon_drafter', $eselon, $data['eselon_drafter'], 'id="eselon_drafter" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="eselon_reviewer" class="col-sm-3">Reviewer Min. Eselon </label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('eselon_reviewer', $eselon, $data['eselon_reviewer'], 'id="eselon_reviewer" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="eselon_creator" class="col-sm-3">Creator Min. Eselon </label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('eselon_creator', $eselon, $data['eselon_creator'], 'id="eselon_creator" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="eselon_approver" class="col-sm-3">Approver Min. Eselon </label>
                            <div class="col-sm-9">
                                <?php echo form_dropdown('eselon_approver', $eselon, $data['eselon_approver'], 'id="eselon_approver" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 text-right">
                        <a class="post-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var selected_color = "<?= $data['color'] ? $data['color'] : 0 ?>";
    var selected_logo = "<?= $data['logo'] ? $data['logo'] : 0 ?>";
    var name = "<?= $data['name'] ?>";
    $(function() {
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }
</script>
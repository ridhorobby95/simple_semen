<?php
	// fungsi membuat side bar
	function createSideBar($arrmenu,&$i, $need_action_count) {

		if(empty($arrmenu[$i]))
		return '';
		
		$menu = $arrmenu[$i];
		
		if($menu['namamenu'] != ':separator') {
			$level = $menu['levelmenu'];
			$j=$i+1;
			$nextlevel = $arrmenu[$j]['levelmenu'];
			
			if(!empty($menu['namafile'])){
				if($menu['class'] == 'blank'){
					$href = $menu['link'];
				}
				else{
					$href = site_url($menu['namafile']);
				}
				
			}
			else{
				$href = 'javascript:void(0)';
			}
			
			if(empty($menu['faicon']))
				$icon = 'desktop';
			else
				$icon = $menu['faicon'];
			
			if($nextlevel > $level)
				$class = 'treeview active';
			else
				$class = '';
			
			$nama = '<span>'.$menu['namamenu'].'</span>';
			if(/*$level == 0 or */$nextlevel <= $level)
				$nama = '<i class="icon-io-'.$icon.'"></i> '.$nama;
			else if($level == 1)
				$nama = '<i class="fa fa-angle-right"></i> '.$nama;
			/*else
				$nama = '<i class="fa fa-angle-double-right"></i> '.$nama;
			*/
			if($nextlevel > $level)
				$nama .= ' <i class="fa pull-right fa-angle-down"></i>';

			$ballon = '';
			if($menu['namamenu'] == 'Dashboard' && $need_action_count!=0){
				$ballon = '<span class="label label-danger" id="textkedip" style="border-radius:50% ">'.$need_action_count.'</span>';
			}
			if($menu['class'] == 'blank'){
				$href = $menu['namafile'];
			}
			$nama = '<a href="'.$href.' "  '.($menu['class'] == 'blank' ? 'target="_blank"' : '').'  class=" '. (($menu['is_active']) ? 'active' : '') .' ">'.$nama.' '.$ballon.'</a>';
			
			$str = '<li'.(empty($class) ? '' : ' class="'.$class.'"').'>'.$nama;
			if($nextlevel > $level) {
				$str .= "\n".'<ul class="treeview-menu">'."\n";
				$i++;
				$str .= createSideBar($arrmenu,$i, $need_action_count);
				$str .= '</ul>'."\n";
			}
			else
				$str .= '</li>'."\n";
		}
		$j=$i+1;
		$nextlevel = $arrmenu[$j]['levelmenu'];
		if($nextlevel < $level)
			return $str;
		else {
			$i++;
			return $str.createSideBar($arrmenu,$i, $need_action_count);
		}
	}
?>
<section class="sidebar">
    <div class="site-photo site-photo-48 site-photo-left" style="margin:10% 40%;">
			<a href="<?php echo site_url($path.'user/me') ?>">
			<img src="<?php echo SessionManagerWeb::getPhoto().($user['photo'] ? '/48' : '') ?>" />
			</a>
		</div>
	<div class="sidebar-title">
        <div style="text-align: center;"><?php echo SessionManagerWeb::getUserName() ?></div>
        <div style="text-align: center;"><?php echo Role::name(SessionManagerWeb::getRole()).' - '.SessionManagerWeb::getEselon() ?></div>
        <a href="<?php echo site_url($path . 'user/me') ?>" style="font-weight: bold; text-align: center;">My Profile</a>	</div>
	
	<ul class="sidebar-menu">
		<br>
		<?php echo form_dropdown('select_company', $company, str_pad(SessionManagerWeb::getCompany(),8,'0', STR_PAD_LEFT), 'id="select_company" class="form-control input-sm desdropdown" style="" onChange="changeCompany()"'); ?>
		<!-- <select class="form-control input-sm desdropdown" id="select_company" onchange="changeCompany()">
			<?php foreach ($company as $key => $comp) { ?>
				<option value="<?= $comp['id'] ?>"><?= $comp['initial'] ?></option>
			<?php } ?>
		</select> -->
		<?php
			$i = 0;
			echo createSideBar($arrmenu,$i, $need_action_count);
		?>
	</ul>
	<ul class="sidebar-menu hidden-md hidden-lg">
		<li>
			<a href="<?php echo site_url($path.'notification/me') ?>">
				<i class="icon-io-notifications"></i>
				<span>
					Notifikasi
					<?php if(!empty($jmlnotif)) { ?>
					<span style="color:red">(<?php echo ($jmlnotif > 99) ? '99+' : $jmlnotif ?>)</span>
					<?php } ?>
				</span>
			</a>
		</li>
		<li>
			<a href="<?php echo site_url($path.'user/logout') ?>">
				<i class="icon-io-logout"></i> <span>Logout</span>
			</a>
		</li>
	</ul>
</section>
<script>
	// var kedipan = 400; 
	// var dumet = setInterval(function () {
	//     var ele = document.getElementById('textkedip');
	//     ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden');
	// }, kedipan);
	function changeCompany(){
		var company = document.getElementById('select_company').value;
		$.ajax({
	        url : "<?= site_url('/web/unitkerja/ajaxSetCompany');?>",
	        type: 'post',
	        data: {'company':company},
	        success:function(respon){
	        	// window.location.replace("<?= site_url('web/document/dashboard') ?>/");
	        	location.reload();
	        } 
	    });
	}
</script>
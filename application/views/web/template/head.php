<meta charset="utf-8">
<title><?= 'Simple - ' . $title ?></title>
<meta name="description" content="Semen Indonesia">
<meta name="author" content="Sevima">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/font-awesome.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/ionicons.min.css') ?>">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/fontawesome-all.min.css') ?>"> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/style.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/horizontal-menu/style.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/icomoon.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/web/css/select2.css') ?>"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/custom.css') ?>?q=2">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/datepicker/datepicker3.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/timepicker/bootstrap-timepicker.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap-datetimepicker.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/colorbox/colorbox.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/datatables/dataTables.bootstrap.css') ?>">

<!-- <link rel="icon" type="image/png" href="<?php echo base_url('assets/web/img/favicon_semen.png') ?>"> -->
<link rel="icon" type="image/png" href="<?php echo base_url('assets/web/img/logo-simple.png') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/document.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/newTabs.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/dropzone.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/web/js/textboxio/textboxio.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/html-docx.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/FileSaver.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/dropzone.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/html5shiv.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/respond.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery-ui-1.10.3.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/AdminLTE/app.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/select2.multi-checkboxes.js') ?>?q=1"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/cookie/js.cookie.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/colorbox/jquery.colorbox-min.js') ?>"></script>
<script src="<?php echo base_url('assets/web/js/moment.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/timepicker/bootstrap-timepicker.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap-datetimepicker.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/custom.js') ?>?q=1"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/plugins/datatables/jquery.dataTables.js') ?>"></script>



<script type="text/javascript" src="<?php echo base_url('assets/web/js/select2.full.js') ?>"></script>

<!-- include chart -->
<script src="<?php echo base_url('assets/web/js/Chart.min.js') ?>"></script>

<!-- full calendar -->
<link rel='stylesheet' type="text/css" href="<?= base_url('assets/web/css/fullcalendar.css') ?>"/>
<script src="<?php echo base_url('assets/web/js/moment.min.js') ?>"></script>
<script src="<?php echo base_url('assets/web/js/fullcalendar.js') ?>"></script>

<!-- qtip -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/jquery.qtip.min.css') ?>">
<script src="<?php echo base_url('assets/web/js/jquery.qtip.min.js') ?>"></script>
<script src="<?php echo base_url('assets/web/js/locale-all.js') ?>"></script>

<!--mention-->
<link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url('assets/web/css/recommended-styles.css') ?>">
<script src="<?php echo base_url('assets/web/js/plugins/typeahead/bootstrap-typeahead.js') ?>"></script>
<script src="<?php echo base_url('assets/web/js/mention.js') ?>"></script>

<script src="<?php echo base_url('assets/web/js/bootstrap-treeview.js') ?>"></script>
<script src="<?php echo base_url('assets/web/js/jquery.mark.min.js') ?>"></script>
<!--RIGHT SIDEBAR YANG MUNCUL PADA SEMUA HALAMAN KECUALI DASHBOARD TUGAS, KALENDER, DAN REKAP LAPORAN-->
<div class="rsidebar-card">
<div class="rsidebar-title">
    <div class="main-title">TUGAS SAYA</div>
    <a class="pull-right" href="<?php echo site_url($path.'issue/') ?>">Lihat Semua</a>
</div>
<?php if(!$arrsidebar['issue']){ ?>
<div class="rsidebar-group-empty">
    <i class="icon-io-check rsidebar-group-empty-plus"></i>
    <div class="rsidebar-group-empty-text">Belum Ada Tugas</div>
</div>
<?php }else{ foreach ($arrsidebar['issue'] as $issue) { 
    $class = $issue['issue_category_id'] == '5' ? 'helpdesk' : 'issue';
    ?>
<div class="rsidebar-task-sm">
    <div class="rsidebar-task-title">
        <a href="<?= site_url($path.$class.'/detail/'.$issue['id']) ?>">
            <?= $issue['title'] ? $issue['title'] : 'No Title' ?>    
        </a>
    </div>
    <p class="rsidebar-task-date"><i  data-toggle="tooltip" data-placement="left" title="Deadline" class="icon-io-calendar rsidebar-task-icon"></i>
        <?php
            $deadline = FormatterWeb::dateToDiff($issue['deadline'], 'd');
            if(!strtotime(substr($deadline, -4)))
                echo '<span class="rsidebar-task-danger">'. ucwords($deadline) .'</span>';
            else
                echo $deadline;
        ?>

    </p>
</div>
<?php } ?>
<?php } ?>
</div>
<div class="rsidebar-break"></div>
<div class="rsidebar-card">
<div class="rsidebar-title">
    <div class="main-title">AGENDA TERDEKAT</div>
    <a class="pull-right" href="<?php echo site_url($path.'calendar') ?>">Lihat Semua</a>
</div>
<?php if(!$arrsidebar['calendar']){ ?>
<div class="rsidebar-group-empty">
    <i class="icon-io-check rsidebar-group-empty-plus"></i>
    <div class="rsidebar-group-empty-text">Belum Ada Agenda</div>
</div>
<?php }else{ 
    foreach ($arrsidebar['calendar'] as $calendar) { ?>
        <div class="rsidebar-task">
            <div class="rsidebar-task-date-big">
                <?php 
                    // cek format output berupa tanggal atau tulisan & sesuaikan output sesuai output tampilan
                    $date_started = FormatterWeb::dateToDiff($calendar['date_started'], 'd');
                    if(strtotime(substr($date_started, -4))){
                        //jika tanggal
                        $date_started = explode(' ', FormatterWeb::toIndoDate($calendar['date_started']));
                        echo $date_started[0] . ' ' . strtoupper(substr($date_started[1], 0, 4)) . '<div class="rsidebar-date-year">' . $date_started[2] . '</div>';
                    }else
                        //jika tulisan
                        echo '<span class="rsidebar-task-danger">'. ucwords($date_started) .'</span>';
                ?>    
            </div>
            <div class="rsidebar-task-title-big">
                <a href="<?= site_url($path.'calendar/detail/'.$calendar['id']) ?>">
                    <?= $calendar['title']; ?>
                </a>

                <p class="rsidebar-task-time"><i class="fa fa-clock-o"></i> <?= FormatterWeb::formatTime($calendar['date_started']) ?> - <?= FormatterWeb::formatTime($calendar['date_finished']) ?></p> 
            </div>
        </div>
    <?php } ?>
<?php } ?>
</div>
<div class="rsidebar-break"></div>
<div class="rsidebar-card">
<div class="rsidebar-title">
    <div class="main-title" style="width:60%">KNOWLEDGE POPULER</div>
    <a class="pull-right" href="<?php echo site_url($path.'knowledge_management') ?>">Lihat Semua</a>
</div>
<?php if(!$arrsidebar['knowledge']){ ?>
<div class="rsidebar-group-empty">
    <i class="icon-io-check rsidebar-group-empty-plus"></i>
    <div class="rsidebar-group-empty-text">Belum Ada Agenda</div>
</div>
<?php }else{ 
    foreach ($arrsidebar['knowledge'] as $knowledge) { ?>
        <div class="rsidebar-task-sm">
            <div class="rsidebar-task-title"><a href="<?php echo site_url($path.'knowledge_management/detail/'.$knowledge['id']) ?>"><?= $knowledge['title'] ? $knowledge['title'] : 'No Title' ?></a>                 
            </div>
            <div data-toggle="tooltip" title="Rating" style="width: 80px;">
                <?php for ($i=1;$i<=5;$i++) { 
                    if ($i<=$knowledge['rating']) {
                ?>
                    <i class="glyphicon glyphicon-star" style="color:#FFD700;text-shadow: 2px 2px 2px white"></i> 
                <?php } else { ?>
                    
                    <i class="glyphicon glyphicon-star" style="color:#C0C0C0"></i>
                <?php }
                } 
                ?>
            </div>
            <p class="rsidebar-task-date"><i data-toggle="tooltip" data-placement="left" title="Penulis" class="icon-io-file rsidebar-task-icon"></i>
                <?php
                    $total_author = count($knowledge['post_authors']);
                    $counter=1;
                    foreach ($knowledge['post_authors'] as $post_author) {
                        $name = xShortenNama($post_author['name']);
                        echo ($counter<$total_author) ?  substr($name, 0, strlen($name)-1).', ' : $name;
                        $counter++;
                    }
                    if ($counter==1){
                        echo 'Tidak ada penulis';
                    }
                    
                ?>
            </p>
        </div>
    <?php } ?>
<?php } ?>
</div>
<!-- <div class="rsidebar-task">
    <div class="rsidebar-task-date-big">11 JUL <div class="rsidebar-date-year">2017</div></div>
    <div class="rsidebar-task-title-big">
        UIX Meeting
        <p class="rsidebar-task-time"><i class="fa fa-clock-o"></i> 11.00 - 12.00</p>
    </div>
</div> --> 
<div class="rsidebar-break"></div>
<div class="rsidebar-card">
<div class="rsidebar-title">
    <div class="main-title">GRUP SAYA</div>
    <a class="pull-right" href="<?php echo site_url($path.'group/list_me') ?>">Lihat Semua</a>
</div>

<!-- IF NO GROUP ADDED -->
<?php if(!$arrsidebar['group']){ ?>
<a href="<?php echo site_url($path.'group/edit') ?>">
<div class="rsidebar-group-empty">
    <i class="icon-io-plus rsidebar-group-empty-plus"></i>
    <div class="rsidebar-group-empty-text">Anda belum memiliki grup, buat grup sekarang.</div>
</div>
</a>
<?php } else { 
        $i = 0;
        foreach ($arrsidebar['group'] as $group) {
            if($i < 5)
                $i++;
            else
                break;
            
    ?>
    <!-- IF GROUP FOUND -->
    <a href="<?php echo site_url($path . 'post/group/' . $group['id']) ?>">
    <div class="rsidebar-group">
<!--        <div class="rsidebar-group-category group-cat-<?php echo strtolower($a_type[$group['type']]) ?>"><?php echo strtolower($a_type[$group['type']]) ?></div>-->
        <div class="rsidebar-group-image" style="background-color:#00B65C">
            <center><img src="<?php echo isset($group['image']) ? $group['image']['thumb']['link'] : $noimggroup ?>" style="background-color:#00B65C;vertical-align:middle;<?php echo !isset($group['image']) ? 'margin-top:12px;width:80%;' : '' ?>"></center>
        </div>
        <div class="rsidebar-group-title"><?= $group['name'] ?></div>
        <div class="rsidebar-group-owner"><i class="fa fa-users"></i><?php echo count($group['group_members']) ?> Anggota</div>
    </div>
    </a>
    <?php } ?>
<?php } ?>
</div>
<!-- IF NO GROUP ADDED -->
<!-- IF GROUP FOUND -->
<!-- RIGHT SIDEBAR KHUSUS GRUP TIMELINE -->
<!--<div class="rsidebar-profile-picture">
    <img src="<?php echo base_url('assets/web/img/bg.jpg') ?>">
</div>
<div class="rsidebar-profile-name">
    Grup Designer <i class="icon-io-lock"></i>
</div>
<div class="rsidebar-profile-owner">
    <i class="fa fa-user"></i>Febria Retno Ramadhani
</div>
<div class="rsidebar-profile-description">
    Grup ini merupakan perkumpulan dari divisi desain untuk membahas segala hal terkait desain.
</div>
<div class="rsidebar-title">
    Anggota Grup (20)
    <a class="pull-right" href="#">Lihat Semua</a>
</div>
<div class="rsidebar-member">
    <div class="rsidebar-member-image"><img src="<?php echo base_url('assets/web/img/avatar.png') ?>"></div>
    <div class="rsidebar-member-name">Arya Bawanta</div>
</div>
<div class="rsidebar-member">
    <div class="rsidebar-member-image"><img src="<?php echo base_url('assets/web/img/avatar.png') ?>"></div>
    <div class="rsidebar-member-name">Grady Fernando</div>
</div>
<div class="rsidebar-member">
    <div class="rsidebar-member-image"><img src="<?php echo base_url('assets/web/img/avatar.png') ?>"></div>
    <div class="rsidebar-member-name">designer design</div>
</div>
<div class="rsidebar-member">
    <div class="rsidebar-member-image"><img src="<?php echo base_url('assets/web/img/avatar.png') ?>"></div>
    <div class="rsidebar-member-name">Eka Pramudita Purnomo</div>
</div>
<div class="rsidebar-member">
    <div class="rsidebar-member-image"><img src="<?php echo base_url('assets/web/img/avatar.png') ?>"></div>
    <div class="rsidebar-member-name">Setiawan Tjukipto</div>
</div>-->


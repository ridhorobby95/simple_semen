<!--<div class="<?php (!isset($container)) ? 'container' : $container; ?>">-->
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <div class="input-search" style="display: none;">
            <form action="<?= site_url('web/document/setSessionSearch/index') ?>" method='post'>
                <input name="keyword" id="keyword" class="" placeholder=" Document's title, code or work unit..." value="<?= $_SESSION['document_search']['keyword'] ?>">
            </form>
        </div>

        <aside class="left-side sidebar-offcanvas" style="overflow-y: overlay;max-height: 100px;"> 
            <?php echo $sidebar ?>
        </aside>
        <div class="thetop"></div>
        <aside class="right-side">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9" id="mainContent">
                        <div class="row">
                        <section class="content">
                            <?php if (isset($srvok)) { ?>
                                <div class="alert alert-<?php echo ($srvok ? 'success' : 'danger') ?> alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $srvmsg ?>
                                </div>
                            <?php } ?>
                            <div class="header-title row">
                                <div class="col-xs-12 btn-group">

                                    <h4><?php echo $title ?></h4>
                                </div>
                                <div class="col-xs-6 pull-right text-right hidden-xs">
                                    <?php
                                        if (!empty($buttons)) {
                                        foreach ($buttons as $btn) {
                                    ?>
                                    <button type="<?php echo empty($btn['submit']) ? 'button' : 'submit' ?>" class="btn btn-sm btn-<?php echo $btn['type'] ?>"
                                        <?php echo (empty($btn['click']) ? '' : ' onClick="' . $btn['click'] . '"') ?> <?php echo $btn['add'] ?>>
                                        <i class="fa fa-<?php echo $btn['icon'] ?>"></i> <?php echo $btn['label'] ?>
                                    </button>
                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="col-xs-5 pull-right text-right visible-xs">
                                    <?php
                                    if (!empty($buttons)) {
                                        foreach ($buttons as $btn) {
                                    ?>
                                    <button type="<?php echo empty($btn['submit']) ? 'button' : 'submit' ?>" class="btn btn-sm btn-<?php echo $btn['type'] ?>"
                                        <?php echo (empty($btn['click']) ? '' : ' onClick="' . $btn['click'] . '"') ?> <?php echo $btn['add'] ?>>
                                        <i class="fa fa-<?php echo $btn['icon'] ?>"></i>
                                    </button>
                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php echo $body ?>
                        </section>
                        </div>
                    </div>
                    <div class="col-sm-3 hidden-xs" id="rightSidebar" style="display:none!important">
                        <div class="row">
                        <section class="content">
                            <?php include (dirname ( __FILE__ ) . '/right_sidebar.php'); ?>
                        </section>
                        </div>
                    </div>
                </div>
            
            </div>
        </aside>
    </div>
<!--</div>-->

<div class="loadings" style="display: none;">Loading&#8230;</div>

<div class="footer">
    <div class="row">
        <div class="col-xs-6">
            <div class="footer-text">
                Copyright &copy; 2018 Document Management Semen Indonesia.
            </div>
        </div>
        <div class="col-xs-6 text-right">
            <div class="footer-text">
                Powered by
            </div>
            <img src="<?php echo base_url('assets/web/img/logo.png') ?>" class="footer-img" height="24">
        </div>
    </div>
</div>

<div class='scrolltop'>
    <div class='scroll icon'><i class="fa fa-4x fa-angle-up"></i></div>
</div>

<script>
    $(window).scroll(function() {
    if ($(this).scrollTop() > 100 ) {
        $('.scrolltop:hidden').stop(true, true).fadeIn();
    } else {
        $('.scrolltop').stop(true, true).fadeOut();
    }
    });
    $(document).ready(function() {
        $(".scroll").click(function(){
            $("html,body").animate({
                scrollTop:0
            },"1000");
            return false;
        });
    });

</script>

<?php 
    if ($_photoswipe)
        include (dirname ( __FILE__ ) . '/_photoswipe.php');
?>

<?php if (SessionManagerWeb::isAuthenticated()) { ?>
    <nav class="navbar navbar-default navbar-fixed-top">
        <header class="header">
            <div class="horizontal-menu menu-bar">
                    
                <div class="row">
                    <div class="col-xs-2 col-sm-3 col-md-3 menu-left">
                        <button type="button" class="btn btn-sm btn-sidemenu" data-toggle="offcanvas" style="width:20%">
                            <i data-toggle="tooltip" data-placement="bottom" title="Sembunyikan Sidebar" class="fa fa-bars" style="color: #000; font-size: 30px;"></i>
                        </button>
                        <a href="<?php echo site_url($path . 'document/dashboard') ?>" class="logo-simple hidden-xs">
                                    <!-- <img class="img-brand" src="<?= $config['logo'][strtolower(Image::IMAGE_ORIGINAL)]['link'] ?>" style="height:50%;width:17%"> -->
                            <img class="img-brand" src="<?= site_url('assets/web/img/logo-simple.png') ?>" style="height:30%;width:17%">
                        </a>
                    </div>
                    <div class="col-xs-8 d-logo visible-xs">
                        <a href="<?php echo site_url($path . 'document/dashboard') ?>">
                            <img class="img-brand" src="<?= site_url('assets/web/img/logo-simple.png') ?>" style="height:30%;width:30%">
                        </a>
                    </div>
                    <div class="col-xs-2 col-sm-5 col-md-5 row">
                            <!--<?php echo $menu ?>-->
                        <?php if (!SessionManagerWeb::isAdminUnit()){ ?>
                        <div class="col-xs-12">
                            <form action="<?= site_url('web/document/setSessionSearch/index') ?>" method='post'>
                                <div class="header-search">
                                    <i class="fa fa-search" style="color:#616161; font-size: 18px;"></i>
                                    <input name="keyword" id="keyword" class="header-search-input" placeholder="Document's title, code or work unit..." value="<?= $_SESSION['document_search']['keyword'] ?>">
                                </div>
                            </form>
                        </div>
                        <?php } ?>
                    </div>
                        
                    <div class="col-xs-4 menu-img hidden-xs" style="text-align:left;">
                        <div class="menu-button">
                            <?php if (!SessionManagerWeb::isAdminUnit()) { ?>
                            <a href="<?php echo site_url($path . 'notification/me') ?>">
                                <i data-toggle="tooltip" title="Notification" data-placement="bottom" class="icon-io-notifications" style="color: #000"></i>
                                <?php if (!empty($jmlnotif)) { ?>
                                    <span class="label label-danger"><?php echo ($jmlnotif > 99) ? '99+' : $jmlnotif ?></span>
                                <?php } ?>
                            </a>
                            <?php } ?>
                            <div class="dropdown" style="display:inline">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i  title="Download Help" data-placement="bottom" class="fa fa-question-circle" style="color: #000"></i>
                                </a>
                                <ul class="dropdown-menu" style="left:-50px;text-align: left">
                                    <li>
                                        <a href="<?= site_url('application/files/user_guide/User_Manual_Guide_Simple.pdf') ?>" target="_blank">User Guide</a>
                                    </li>
                                    <?php if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()) { ?>
                                        <li>
                                            <a href="<?= site_url('application/files/user_guide/User_Guide_Doc_Controller.pdf') ?>" target="_blank">Document Controller</a>
                                        </li>
                                        <?php if (SessionManagerWeb::isAdministrator()) { ?>
                                            <li>
                                                <a href="<?= site_url('application/files/user_guide/User_Guide_Administrator.pdf') ?>" target="_blank">Administrator</a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                            <a href="<?php echo site_url($path . 'setting/') ?>">
                                <i data-toggle="tooltip" title="Setting" data-placement="bottom" class="fa fa-cog" style="color: #000"></i>
                            </a>
                            
                            <a href="<?php echo site_url($path . 'user/logout') ?>">
                                <i data-toggle="tooltip" title="Logout" data-placement="bottom" class="fa fa-power-off" style="color: #000"></i>
                            </a>
                        </div>
                        <div class="menu-photo site-photo-36 site-photo-right">

                            <a href="<?php echo site_url($path . 'user/me') ?>"><img src="<?php echo SessionManagerWeb::getPhoto().($user['photo'] ? '/36' : '') ?>" /></a>
                        </div>
                        <div class="menu-name pull-right"><b>Halo, <?php echo ucfirst(strlen(SessionManagerWeb::getFirstName()) < 9 ? SessionManagerWeb::getFirstName() : substr(SessionManagerWeb::getFirstName(),0,8).'...' ) ?></b>
                        </div>
                    </div>
                </div>
            </div>
        </header>
            
    </nav>
    
    <script>
        $(document).ready(function(){
            $('.header-search .fa-search').click(function(){
               $(".input-search").toggle();
               // alert();
            });
        })
    </script>
    <!-- <div class="visible-xs" style="height:80px"></div> -->
    <div class="hidden-xs" style="height:70px"></div>
<?php } ?>

<style type="text/css">
    #rightSidebar {
        display: none!important;
    }
    #mainContent {
        margin-bottom: 50vh;
    }
</style>
<script>
	$("#mainContent").removeClass('col-sm-9');
	$("#mainContent").addClass('col-sm-12');
</script>
<?php
	include("_document_js.php");
	include('document_modal_viewdoc.php');
?>


<div id="reject_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title text-center" style="font-family: Source Sans Pro, sans-serif; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">
                    Rejection
                </h4>
                <p class="sub-one-revisi">Write Your Rejection Reason</p>
            </div>
            <div class="modal-body">
            	<form action="<?= site_url('web/document/setDownloadReason').'/'.$download['id'] ?>" method="post">
            		<h4 for="reason_textarea">Reason</h4>
                    <br>
					<textarea id="reason_textarea" placeholder="Write your rejection reason..." name="reason_textarea" style="height:100px;width:100%" required></textarea>
					<button type="submit" id="submit_reject" name="submit_reject" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
				</form>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1-1" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-sm btn-danger btn-rad" onClick="submit_rejection()"><i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Reject</span></button>
            </div>
        </div>
    </div>
</div>

<div id="approve_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <center>
        <div class="modal-content" style="background-color: #fff;width:50%">
            <div class="modal-header" style="background-color: #fff;border:0px">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Approval</h4>
                <p class="sub-one-revisi">Are you sure to approve this download request ?</p>
            </div>
            <div class="modal-footer" style="background-color: #fff;border:0px">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal" style="margin: 0px;padding: 6px 12px">Cancel</button>
                <button type="button" id="btn_approve_modal" class="btn btn-sm btn-success btn-rad" onClick="approve(<?= $download['id'] ?>)"> <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Approve</span>
                </button>
            </div>
        </div>
        </center>
    </div>
</div>

<!-- Variables -->
<div class="row" style="margin-left:5px">
	<div class="row" style="margin-bottom: 1%">
		<div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px; width: 96%; margin-left: 1.5%">
			<!-- Detail Requestor -->
			<table class="table" >
				<!-- START : Requestor Detail -->
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h3 style="border-bottom: 1px solid;padding:3px;margin-top:7px"><b style="color:#0288D1">Requestor Detail</b></h3></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Name</b></td>
					<td><?= ($requestor['name']==NULL) ? '-' : $requestor['name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Position</b></td>
					<td><?= ($requestor['jabatan_name']==NULL) ? '-' : $requestor['jabatan_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Workunit</b></td>
					<td><?= ($requestor['unitkerja_name']==NULL) ? '-' : $requestor['unitkerja_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b style="color:red">Download Reason</b></td>
					<td><b><?= ($download['reason']==NULL) ? '-' : $download['reason'] ?></b></td>
				</tr>
				<!-- ENDOF : Requestor Detail -->

				<tr>
					<td colspan=2 style="padding:0px"><h3 style="border-bottom: 1px solid;padding:3px;margin-top:7px"><b style="color:#0288D1">Status</b></h3></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Status</b></td>
					<td><?= ($download['is_allowed']==0) ? "<b style='color:#FFCA28'>Still waiting Approval from Document Controller</b>" : (($download['is_allowed']==1) ? "<b style='color:green'>Approved</b>" : "<b style='color:red'>Rejected</b>") ?></td>
				</tr>
				<?php if ($download['is_allowed']=='1'){ ?>
					<tr>
						<td class="e-left-field" width="150"><b>Valid Until</b></td>
						<td><b style="color:green"><?= $download['valid_until'] ?></b></td>
					</tr>
				<?php } ?>
				<?php if ($download['is_allowed']=='-1'){ ?>
					<tr>
						<td class="e-left-field" width="150"><b style="color:red">Reason</b></td>
						<td><b style="color:red"><?= $download['reject_reason'] ?></b></td>
					</tr>
				<?php } ?>

				<!-- START : User Access Detail -->
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h3 style="border-bottom:1px solid;padding:3px;margin-top:7px"><b style="color:#0288D1">Detail Dokumen</b></h3></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Title</b></td>
					<td><?= ($document['title']==NULL) ? '-' : $document['title'] ?></td>
				</tr>
		        <tr>
					<td class="e-left-field"><b>Drafter</b></td>
					<td><?= ($document['user_name']==NULL) ? '-' : $document['user_name'] ?></td>
				</tr>
		        <tr> 
					<td class="e-left-field"><b>Drafter Workunit</b></td>
					<td><?= ($document['user_workunit_name']==NULL) ? '-' : $document['user_workunit_name'] ?></td>
				</tr>
		        <tr>
					<td class="e-left-field"><b>Creator</b></td>
					<td><?= ($document['creator_name']==NULL) ? '-' : $document['creator_name'] ?></td>
				</tr>
		        <tr>
					<td class="e-left-field"><b>Approver</b></td>
					<td><?= ($document['approver_name']==NULL) ? '-' : $document['approver_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Type</b></td>
					<td><?= ($document['type_name']==NULL) ? '-' : $document['type_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Revision</b></td>
					<td><?= ($document['revision']==NULL) ? '-' : $document['revision'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Business Process</b></td>
					<td><?= ($document['prosesbisnis_name']==NULL) ? '-' : $document['prosesbisnis_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Code</b></td>
					<td><?= ($document['code']==NULL) ? '-' : $document['code'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Requirements</b></td>
					<td><?= ($document['requirement']==NULL) ? '-' : $document['requirement'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Clausuls</b></td>
					<td><?= ($document['clausul']==NULL) ? '-' : $document['clausul'] ?></td>
				</tr>
			</table>
			<div class="col-md-9" style="margin-bottom: 20px;margin-top: 10px">
           		<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad" onClick="viewDocument(<?= $document['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
				    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
				    <span>View Document</span>
				</button>
				<?php if ($download['is_allowed']==1){ ?>
					<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad" onClick="download_document(<?= $download['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
					    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
					    <span>Download</span>
					</button>
				<?php } ?>
				<?php if (SessionManagerWeb::isDocumentController() and $download['is_allowed']==0 and $is_allow_approve) { ?>
					<button id="btn_approve" class="btn btn-sm btn-success btn-rad pull-right" onClick="approve_document()" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
					    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
					    <span>Approve</span>
					</button>
					<button id="btn_reject" class="btn btn-sm btn-danger btn-rad pull-right" onClick="javascript:reject_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
					    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
					    <span>Reject</span>
					</button>
				<?php } ?>
			</div>

		</div>

	</div>
</div>
<script>
	function goBack(){
		location.href = "<?php echo site_url($path.$class.'/dashboard') ?>";
	}

	function approve_document(){
		$("#approve_modal").modal('show');
	}
	function reject_document(){
		$("#reject_modal").modal('show');
	}

	function submit_rejection(){
		$("#submit_reject").click();
	}

	function approve(id){
		$.ajax({
			url : "<?= site_url('web/document/approve_download/').'/' ?>/"+id,
			type: 'post',
			cache: false,
			data: {},
			success: function(respon){
				if (respon != 'ERROR') {
					window.location.replace("<?= site_url('web/document/download_detail') ?>/"+id);
				}
				else {
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
				}
			}
		});
	}

	function download_document(id){
		window.open("<?php echo base_url('/web/document/download_watermark') ?>/"+id);
		// window.location.replace("<?php// echo base_url('/web/document/download_watermark') ?>/"+id);
	}


</script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>

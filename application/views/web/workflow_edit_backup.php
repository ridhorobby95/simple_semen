<script src="<?= base_assets(); ?>js/go.js"></script>

<script id="code">
  function init() {
    //if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates
    var yellowgrad = $(go.Brush, "Linear", { 0: "rgb(254, 201, 0)", 1: "rgb(254, 162, 0)" });
    var greengrad = $(go.Brush, "Linear", { 0: "#98FB98", 1: "#9ACD32" });
    var bluegrad = $(go.Brush, "Linear", { 0: "#B0E0E6", 1: "#87CEEB" });
    var redgrad = $(go.Brush, "Linear", { 0: "#C45245", 1: "#871E1B" });
    var whitegrad = $(go.Brush, "Linear", { 0: "#F0F8FF", 1: "#E6E6FA" });
    var bigfont = "bold 13pt Helvetica, Arial, sans-serif";
    var smallfont = "bold 11pt Helvetica, Arial, sans-serif";
    // Common text styling
    function textStyle() {
      return {
        margin: 6,
        wrap: go.TextBlock.WrapFit,
        textAlign: "center",
        editable: true,
        font: bigfont
      }
    }
    myDiagram =
      $(go.Diagram, "myDiagramDiv",
        {
          // have mouse wheel events zoom in and out instead of scroll up and down
          "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
          allowDrop: true,  // support drag-and-drop from the Palette
          initialAutoScale: go.Diagram.Uniform,
          "linkingTool.direction": go.LinkingTool.ForwardsOnly,
          initialContentAlignment: go.Spot.Center,
          layout: $(go.LayeredDigraphLayout, { isInitial: false, isOngoing: false, layerSpacing: 50 }),
          "undoManager.isEnabled": true
        });
    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function(e) {
      var button = document.getElementById("SaveButton");
      if (button) button.disabled = !myDiagram.isModified;
      var idx = document.title.indexOf("*");
      if (myDiagram.isModified) {
        if (idx < 0) document.title += "*";
      } else {
        if (idx >= 0) document.title = document.title.substr(0, idx);
      }
    });
    var defaultAdornment =
      $(go.Adornment, "Spot",
        $(go.Panel, "Auto",
          $(go.Shape, { fill: null, stroke: "dodgerblue", strokeWidth: 4 }),
          $(go.Placeholder)),
        // the button to create a "next" node, at the top-right corner
        $("Button",
          { alignment: go.Spot.TopRight,
            click: addNodeAndLink },  // this function is defined below
          new go.Binding("visible", "", function(a) { return !a.diagram.isReadOnly; }).ofObject(),
          $(go.Shape, "PlusLine", { desiredSize: new go.Size(6, 6) })
        )
      );
    // define the Node template
    myDiagram.nodeTemplate =
      $(go.Node, "Auto",
        { selectionAdornmentTemplate: defaultAdornment },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        // define the node's outer shape, which will surround the TextBlock
        $(go.Shape, "Rectangle",
          { fill: yellowgrad, stroke: "black",
            portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer",
            toEndSegmentLength: 50, fromEndSegmentLength: 40 }),
        $(go.TextBlock, "Page",
          { margin: 6,
            font: bigfont,
            editable: true },
          new go.Binding("text", "text").makeTwoWay()));
    myDiagram.nodeTemplateMap.add("Source",
      $(go.Node, "Auto",
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "RoundedRectangle",
          { fill: bluegrad,
          portId: "", fromLinkable: true, cursor: "pointer", fromEndSegmentLength: 40}),
        $(go.TextBlock, "Source", textStyle(),
          new go.Binding("text", "text").makeTwoWay())
        ));
    myDiagram.nodeTemplateMap.add("DesiredEvent",
      $(go.Node, "Auto",
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "RoundedRectangle",
          { fill: greengrad, portId: "", toLinkable: true, toEndSegmentLength: 50 }),
        $(go.TextBlock, "Success!", textStyle(),
          new go.Binding("text", "text").makeTwoWay())
        ));
    // Undesired events have a special adornment that allows adding additional "reasons"
    var UndesiredEventAdornment =
      $(go.Adornment, "Spot",
        $(go.Panel, "Auto",
          $(go.Shape, { fill: null, stroke: "dodgerblue", strokeWidth: 4 }),
          $(go.Placeholder)),
        // the button to create a "next" node, at the top-right corner
        $("Button",
          { alignment: go.Spot.BottomRight,
            click: addReason },  // this function is defined below
          new go.Binding("visible", "", function(a) { return !a.diagram.isReadOnly; }).ofObject(),
          $(go.Shape, "TriangleDown", { desiredSize: new go.Size(10, 10) })
        )
      );
    var reasonTemplate = $(go.Panel, "Horizontal",
      $(go.TextBlock, "Reason",
        {
          margin: new go.Margin(4,0,0,0),
          maxSize: new go.Size(200, NaN),
          wrap: go.TextBlock.WrapFit,
          stroke: "whitesmoke",
          editable: true,
          font: smallfont
        },
        new go.Binding("text", "text").makeTwoWay())
      );
    myDiagram.nodeTemplateMap.add("UndesiredEvent",
      $(go.Node, "Auto",
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        { selectionAdornmentTemplate: UndesiredEventAdornment },
        $(go.Shape, "RoundedRectangle",
          { fill: redgrad, portId: "", toLinkable: true, toEndSegmentLength: 50 }),
        $(go.Panel, "Vertical", {defaultAlignment: go.Spot.TopLeft},
          $(go.TextBlock, "Drop", textStyle(),
            { stroke: "whitesmoke",
              minSize: new go.Size(80, NaN) },
            new go.Binding("text", "text").makeTwoWay()),
          $(go.Panel, "Vertical",
            { defaultAlignment: go.Spot.TopLeft,
              itemTemplate: reasonTemplate },
            new go.Binding("itemArray", "reasonsList").makeTwoWay()
          )
        )
        ));
    myDiagram.nodeTemplateMap.add("Comment",
      $(go.Node, "Auto",
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Shape, "Rectangle",
          { portId: "", fill: whitegrad, fromLinkable: true }),
        $(go.TextBlock, "A comment",
          { margin: 9,
            maxSize: new go.Size(200, NaN),
            wrap: go.TextBlock.WrapFit,
            editable: true,
            font: smallfont },
          new go.Binding("text", "text").makeTwoWay())
        // no ports, because no links are allowed to connect with a comment
        ));
    // clicking the button on an UndesiredEvent node inserts a new text object into the panel
    function addReason(e, obj) {
      var adorn = obj.part;
      if (adorn === null) return;
      e.handled = true;
      var arr = adorn.adornedPart.data.reasonsList;
      myDiagram.startTransaction("add reason");
      myDiagram.model.addArrayItem(arr, {});
      myDiagram.commitTransaction("add reason");
    }
    // clicking the button of a default node inserts a new node to the right of the selected node,
    // and adds a link to that new node
    function addNodeAndLink(e, obj) {
      var adorn = obj.part;
      if (adorn === null) return;
      e.handled = true;
      var diagram = adorn.diagram;
      diagram.startTransaction("Add State");
      // get the node data for which the user clicked the button
      var fromNode = adorn.adornedPart;
      var fromData = fromNode.data;
      // create a new "State" data object, positioned off to the right of the adorned Node
      var toData = { text: "new" };
      var p = fromNode.location;
      toData.loc = p.x + 200 + " " + p.y;  // the "loc" property is a string, not a Point object
      // add the new node data to the model
      var model = diagram.model;
      model.addNodeData(toData);
      // create a link data from the old node data to the new node data
      var linkdata = {};
      linkdata[model.linkFromKeyProperty] = model.getKeyForNodeData(fromData);
      linkdata[model.linkToKeyProperty] = model.getKeyForNodeData(toData);
      // and add the link data to the model
      model.addLinkData(linkdata);
      // select the new Node
      var newnode = diagram.findNodeForData(toData);
      diagram.select(newnode);
      diagram.commitTransaction("Add State");
    }
    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        new go.Binding("points").makeTwoWay(),
        { curve: go.Link.Orthogonal, corner: 1 },
        new go.Binding("curviness", "curviness"),
        $(go.Shape,  // the link shape
          { stroke: "#2F4F4F", strokeWidth: 2 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "Standard", fill: "#2F4F4F", stroke: null, scale: 1 })
    );
    myDiagram.linkTemplateMap.add("Comment",
      $(go.Link, { selectable: false },
        $(go.Shape, { strokeWidth: 2, stroke: "darkgreen" })));
    var palette =
      $(go.Palette, "palette",  // create a new Palette in the HTML DIV element "palette"
        {
          // share the template map with the Palette
          nodeTemplateMap: myDiagram.nodeTemplateMap,
          autoScale: go.Diagram.Uniform  // everything always fits in viewport
        });
    palette.model.nodeDataArray = [
      { category: "Source" },
      { }, // default node
      { category: "DesiredEvent" },
      { category: "UndesiredEvent", reasonsList: [{}] },
      { category: "Comment" }
    ];
    // read in the JSON-format data from the "mySavedModel" element
    load();
    layout();
  }
  function layout() {
    myDiagram.layoutDiagram(true);
  }
  // Show the diagram's model in JSON format
  function save() {
    $('#act').val('1');
    /*
    doc_json = document.getElementById("mySavedModel").value = myDiagram.model.toJson();
    $('#act').val(doc_json);
    myDiagram.isModified = false;
    */
  }
  function load() {
    myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
  }
</script>
<hr/>
<div id="sample">
  <div style="width:100%; white-space:nowrap;">
    <span style="display: none; vertical-align: top; padding: 5px; width:0px">
      <div id="palette" style="border: solid 1px black; height: 480px;display: none;"></div>
    </span>
    <span style="display: inline-block; vertical-align: top; padding: 1px; width:95%">
      <div id="myDiagramDiv" style="border: solid 1px #ccc; height: 280px"></div>
    </span>
  </div>

<div style="display: none">
  <button id="SaveButton" onclick="save()">Save</button>
  <button onclick="load()">Load</button>
  <button onclick="layout()">Layout</button>
  Diagram Model saved in JSON format:
  <br />
 </div>

<?php

$nodes = $workflow['rule']['model']['nodeDataArray'];
$linkDataArray = $workflow['rule']['model']['linkDataArray'];

?>

  <textarea id="mySavedModel" style="width:100%;height:300px;display: none;">
{ "copiesArrays": true,
  "copiesArrayObjects": true,
  "nodeDataArray": <?= json_encode($nodes) ?>,
  "linkDataArray": <?= json_encode($linkDataArray) ?>
}
</textarea></div>

<script>
    $(document).ready(function() {
        init();
    });
</script>

<br>

<table>
  <tr>
    <td width="150">Nama Aplikasi</td>
    <td><input type="text" name="nama" value="<?=$workflow['rule']['nama']?>" size="50"></td>
  </tr>
  <tr>
    <td>Tenggat waktu (days)</td>
    <td><input type="text" name="tenggat_hari" value="<?=$workflow['rule']['tenggat_hari']?$workflow['rule']['tenggat_hari']:0?>" size="5"></td>
  </tr>
  <tr>
    <td>Laporan</td>
    <td><select name="file_pdf">
<option value=""></option>
<?php foreach ($list_pdf as $pdf) { ?>
  <option value="<?= $pdf['id_pdf'] ?>" <?php if ($workflow['rule']['file_pdf'] == $pdf['id_pdf']) echo 'selected' ?>><?= $pdf['nama']  ?></option>
<?php } ?>
</select></td>
  </tr>
</table>

<hr>

<table>
<tr>
<?php

$links = array();
foreach ($nodes as $node) {
    $temp = explode('_', $node['key']);
    $i_node = $temp[1];

    $links['node_' . $i_node] = 'node_' . $i_node;
}

$approve = array();
foreach ($linkDataArray as $link) {
    $temp = explode('_', $link['from']);
    $i_from = $temp[1];
    $temp = explode('_', $link['to']);
    $i_to = $temp[1];

    if ($i_from < $i_to)
        $approve[$link['from']] = $link['to'];
}

$refuse = array();
foreach ($linkDataArray as $link) {
    $temp = explode('_', $link['from']);
    $i_from = $temp[1];
    $temp = explode('_', $link['to']);
    $i_to = $temp[1];

    if ($i_from > $i_to)
        $refuse[$link['from']] = $link['to'];
}

$i=0;
foreach ($nodes as $node) {
    $temp = explode('_', $node['key']);
    $i_node = $temp[1];

    $i++;
?>
<td>
<div class="thumb-configure">
<div style="text-align:right;padding:5px"><label class="label label-primary"><?= $node['key'] ?></label></div>
<table>
<tr><td>Nama</td><td><input type="text" name="<?= $node['key'] ?>" value="<?= $node['text2'] ?>" size="30"></td></tr>
<tr><td>Actor</td><td><select name="actor_<?= $i_node ?>" style="width:200px">
<option value=""></option>
<?php foreach ($actors as $k => $v) { ?>
    <option value="<?= $k ?>" <?php if ($node['actor'] == $k) echo 'selected' ?>><?= $v ?></option>
<?php } ?>
</select></td></tr>
<tr><td>Keterangan</td><td><textarea name="keterangan_<?= $i_node ?>" style="width:200px;height:50px"><?= $node['keterangan'] ?></textarea></td></tr>
<tr><td>Approved</td><td><select name="approve_<?= $i_node ?>" style="width:200px">
<option value=""></option>
<?php foreach ($links as $k => $v) { ?>
    <option value="<?= $k ?>" <?php if ($approve['node_' . $i_node] == $k) echo 'selected' ?>><?= $v  ?></option>
<?php } ?>
</select></td></tr>
<tr><td>Refused</td><td><select name="refuse_<?= $i_node ?>">
<option value=""></option>
<?php foreach ($links as $k => $v) { ?>
    <option value="<?= $k ?>" <?php if ($refuse['node_' . $i_node] == $k) echo 'selected' ?>><?= $v  ?></option>
<?php } ?>
</select></td></tr>
<tr><td>Form</td><td><select name="form_<?= $i_node ?>" style="width:200px">
<option value=""></option>
<?php foreach ($list_form as $form) { ?>
  <option value="<?= $form['id_form'] ?>" <?php if ($node['form'] == $form['id_form']) echo 'selected' ?>><?= $form['nama']  ?></option>
<?php } ?>
</select></td></tr>
</table>
</div>
<?php if ($i%4==0) echo '</td></tr><tr></td>' ?>

<?php
}

?>
</td>
</tr>
</table>


<button id="SaveButton" onclick="save()" class="btn btn-success"><small class="glyphicon glyphicon-floppy-disk"></small> Simpan</button>

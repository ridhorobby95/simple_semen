
<table class="table">
	<!-- START : Requestor Detail -->
		<tr>
			<td colspan=2><h3 style="background-color:#ccc;padding:3px">Requestor Detail</h3></td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">NIK</td>
			<td><?php if ($edit) { ?><input type="text" name="nik" size="50"> <?php } else { echo $nik; }?> </td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">Nama</td>
			<td><?php if ($edit) { ?><input type="text" name="nama" size="50"><?php } else { echo $nama; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">Departemen</td>
			<td><?php if ($edit) { ?><input class="bg-warning" type="text" name="departemen" size="50"><?php } else { echo $departemen; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">Posisi</td>
			<td><?php if ($edit) { ?><input class="bg-danger" type="text" name="posisi" size="50"><?php } else { echo $posisi; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field">Email</td>
			<td><?php if ($edit) { ?><input type="text" name="email" size="50"><?php } else { echo $email; } ?></td>
		</tr>
	<!-- ENDOF : Requestor Detail -->
	<!-- START : User Access Detail -->
		<tr>
<td colspan=2><h3 style="background-color:#ccc;padding:3px">User Access Detail</h3></td>
		</tr>
		<tr>
			<td class="e-left-field">Type of Request</td>
			<td><?php if ($edit) { ?>
				<table>
					<tr><td><input type="radio" name="jenis" value="creation" checked></td><td>Creation &nbsp; &nbsp;</td>
					<td><input type="radio" name="jenis" value="termination"></td><td>Termination</td>
					</tr>
				</table>
<?php } else { echo ucwords($jenis); } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field">Computer Name</td>
			<td><?php if ($edit) { ?><input type="text" name="computer_name" size="50"><?php } else { echo $computer_name; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field">Proposed Email Type</td>
			<td><?php if ($edit) { ?>
				<table>
					<tr><td><input type="radio" name="email_type" value="group"></td><td>Group &nbsp; &nbsp;</td>
					<td><input type="radio" name="email_type" value="personal" checked=""></td><td>Personal</td>
					</tr>
				</table>
<?php } else { echo ucwords($email_type); } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field">Proposed E-mail Name</td>
			<td><?php if ($edit) { ?>
				<input type="text" name="email_name" size="50">
				<select name="email_domain">
					<option value="@tds.com">@tds.com</option>
					<option value="@wonokoyo.co.id">@wonokoyo.co.id</option>
					<option value="@wonokoyo-group.com">@wonokoyo-group.com</option>
					<option value="@wonokoyojayacorporindo.com">@wonokoyojayacorporindo.com</option>
					<option value="@feedtech.wjc">@feedtech.wjc</option>
					<option value="@mitraunggassejati.com">@mitraunggassejati.com</option>
					<option value="@kda.com">@kda.com</option>
					<option value="@wjk.co.id">@wjk.co.id</option>
					<option value="@gna.com">@gna.com</option>
					<option value="@gna.com">@tdp.com</option>
				</select>
<?php } else { echo $email_name . $email_domain; } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field">Validity Period</td>
			<td><?php if ($edit) { ?>
				<table>
					<tr>
						<td><input type="radio" name="validity_type" value="continuous" onclick="change_validity_type(this)" checked></td><td>Continuous &nbsp; &nbsp;</td>
					</tr>
					<tr>
						<td>
							<input type="radio" name="validity_type" value="temporary" onclick="change_validity_type(this)" ></td>
						<td>Temporary</td>
						<td>
							<input type="text" name="validity_start" size="50" placeholder="DARI [dd/mm/yyyy]" readonly>
						</td>
						<td>
							<input type="text" name="validity_end" size="50" placeholder="SAMPAI [dd/mm/yyyy]" readonly>
						</td>
					</tr>
					</tr>
				</table>
<?php } else { 
echo ucwords($validity_type); 
if ($validity_type == 'temporary')
  echo ' (' . $validity_start . '-' . $validity_end . ')';
} ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field" style="vertical-align:top">Description</td>
			<td><?php if ($edit) { ?>
				<textarea name="description" style="width:500px;height:100px"></textarea>
<?php } else { echo nl2br($description); } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field" style="vertical-align:top">Reason</td>
			<td><?php if ($edit) { ?>
				<textarea name="reason" style="width:500px;height:100px"></textarea>
<?php } else { echo nl2br($reason); } ?>
			</td>
		</tr>
</table>

<script type="text/javascript">
	/**
	 * Fungsi change_validity_type
	 * @param  eval = objek radio button
	 * -> Disable / Enable Textbox tanggal bila validity type berubah
	 */
	function change_validity_type(eval){
		var validity_type = eval.value;
		if (validity_type =='continuous'){
			$("input[name='validity_start']").attr('readonly', 'false');
			$("input[name='validity_end']").attr('readonly', 'false');
		}else{
			$("input[name='validity_start']").removeAttr('readonly');
			$("input[name='validity_end']").removeAttr('readonly');
		}
	}
</script>


<style>
	.e-left-field{
		text-align: left;
		
	}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:5%">No</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">Code</th>
                                        <th class="text-center">Order</th>
                                        <th class="text-center" style="width:15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $urutan = $v['urutan'];
                                        if ($v['urutan']==NULL){
                                            $urutan = count($data);
                                        }
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td class="text-center">
                                                <?= $v['type'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $v['code'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($v['urutan']>1) { ?>
                                                    <a href="javascript:sort(<?= $v['id'].','.($urutan-1) ?>)" class="text-primary" title="Switch Up">
                                                        <i class="fa fa-sort-asc"></i>
                                                    </a> 
                                                <?php } ?>
                                                <?php if ($v['urutan']<count($data)) { ?>
                                                    <a href="javascript:sort(<?= $v['id'].','.($urutan+1) ?>)" class="text-primary" title="Switch Down" >
                                                        <i class="fa fa-sort-desc"></i>
                                                    </a> 
                                                <?php } ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                echo anchor('web/document_types/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                // echo anchor('web/document/edit/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip"  title="Hapus"');
                                                ?>
                                                <?php if (1==0) {?>
                                                <a href="javascript:hapus_jenis(<?php echo $v['id'] ?>,'<?php echo $v['type'] ?>')" class="text-danger" title="Delete Document Type">
                                                    <i class="fa fa-trash"></i>
                                                </a> 
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <?php if (1==0) { ?>
            <div class="row">
                <div class="col-sm-4 col-md-4" style="position:relative;left:1.5%">
                    <input type="text" name="type_name" id="type_name" placeholder="Type name" class="form-control" style="width:300px">
                </div>
                <div class="col-sm-4 col-md-4" style="position:relative;left:5%;bottom:1%;padding-bottom: 10px">
                    <span class="btn btn-sm btn-primary" id="btn_tambah_jenis">Add Document Type</span>
                </div>
            </div>
            <?php } ?>
        </div>

    </div>
</div>
<script>



    $('#btn_tambah_jenis').on('click', function () {
        var name = $('#type_name').val();
        location.href = "<?= site_url('web/document_types/add') ?>/" + name ;
    });    

    function hapus_jenis(id,name) {
        if (confirm('Delete "'+name+'" ?')){
            location.href = "<?= site_url('web/document_types/delete') ?>/"+id;
        }
    }

    function sort(id, urutan){
        $.ajax({
            url : "<?php echo site_url('/web/document_types/ajaxSetUrutan');?>",
            type: 'post',
            data: {'id':id, 'urutan':urutan},
            success:function(respon){
                // alert(respon);
                location.reload();
            } 
        }); 
    }

</script>
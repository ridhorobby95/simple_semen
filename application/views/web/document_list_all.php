<script>
    $("#mainContent").addClass('col-sm-12');
    $("#mainContent").removeClass('col-sm-9');
</script>

<br>
<!-- <form method="post" action="<?= site_url('web/document/list_all') ?>" class="search-form" autocomplete="off" style="position:relative ;width:100%;bottom:5px"> -->
    <div class="row form-group has-feedback">
        <!-- <div class="col-md-1">
            <label for="role" class="">Date : </label>
        </div> -->
        <div class="col-md-2 hidden">
            <input type="text" data-provide="datepicker" name="start_date" id="start_date" class="form-control input-md inputplus" value="<?= $filter['start_date'] ?>" placeholder="Start Date" onchange="javascript:dateChange()"/>
        </div>

        <div class="col-md-2 hidden">
            <input type="text" data-provide="datepicker" name="end_date" id="end_date" class="form-control input-md inputplus" value="<?= $filter['end_date'] ?>" placeholder="End Date" onchange="javascript:dateChange()"/>
        </div>
        <div class="col-md-1">
            <button type="button" id="submit_button" data-toggle="modal" data-target=".bs-example-modal-list-all-lg" class="btn btn-md mousedrag01" style="padding:6px 20px">
                Filter
            </button>
        </div>
        <div class="modal fade bs-example-modal-list-all-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
          <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding-left: 5%;">
              <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="row">
                        <div class="col-md-10">
                            <h2 style="margin: 0%;">
                                Filter
                            </h2>
                        </div>
                        <div class="col-md-2" style="text-align: center">
                            <i class='fa fa-times-circle onhovering hidden' aria-hidden='true' style='' data-dismiss="modal" aria-hidden="true">
                                
                            </i>
                        </div>
                    </div>
                </div>
              </div>
              <br>
            <form method="post" name="filter_form" id="filter_form" action="<?= site_url('web/document/setSessionSearchListAll') ?>" style="">
                <input type="hidden" id="numPage" name="numPage" value="<?php echo $_SESSION['page'];?>">

                <div class="row">
                
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Keyword
                                </h5>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input id="title" name="title" type="text" class="form-control input-md inputplus" id="title" placeholder="Write Document Title or Code" value="<?= $_SESSION['document_search_listall']['title'] ?>">
                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_title()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Document type
                                </h5>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                    <?php 
                                    foreach ($variables['document_types'] as $k => $type) {
                                        $types[$type['id']] = $type['type'];
                                    }
                                    unset($types[""]);
                                    echo form_dropdown('type[]', $types, $_SESSION['document_search_listall']['arr_type'], 'id="type" class="form-control input-sm select2 type" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" '); ?>
                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_type()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Status
                                </h5>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                    <?php
                                    $status = array(
                                        1   => 'Document Drafting',
                                        2   => 'Document Revising',
                                        3   => 'Draft Content Review',
                                        4   => 'Evaluation Approval',
                                        5   => 'Evaluation Form Input',
                                        6   => 'Evaluation Form Input Drafter',
                                        7   => 'Obsoleted',
                                        8   => 'Publish',
                                        9   => 'Review and Approval',
                                        10  => 'Verification',
                                        11  => 'Withdrawal Verification'
                                        // 1   => 'Publish',
                                        // 2   => 'Evaluation',
                                        // 3   => 'Revision',
                                        // 5   => 'Rejected',
                                        // 4   => 'Obsoleted'
                                    ); 
                                    // foreach ($variables['document_types'] as $k => $type) {
                                    //  $types[$type['id']] = $type['type'];
                                    // }
                                    // unset($types[""]);
                                    echo form_dropdown('status[]', $status, $_SESSION['document_search_listall']['status'], 'id="status" class="form-control input-sm select2 type" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" '); ?>
                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_type()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Work Unit
                                </h5>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                    <?php
                                        $unitkerja = $variables['unitkerja'];
                                        unset($unitkerja[0]);
                                        echo form_dropdown('unitkerja[]', $unitkerja, $_SESSION['document_search_listall']['arr_unitkerja'], 'id="unitkerja" class="form-control input-sm select2 unitkerja" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" ');
                                    ?>
                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_workunit()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Date Start
                                </h5>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                     
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type="text" name="datestart" id="datestart" class="form-control input-md inputplus" value="<?= $_SESSION['document_search_listall']['datestart'] ?>" placeholder="Date Start" onchange="javascript:dateChange()"/>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>

                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_datestart()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Date End
                                </h5>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                    
                                    <div class="input-group date" data-provide="datepicker">
                                        <input type='text' id='dateend' name="dateend"  class="form-control input-md inputplus" value="<?= $_SESSION['document_search_listall']['dateend'] ?>" placeholder="Date End"/ onchange="javascript:dateChange()">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-th"></span>
                                        </div>
                                    </div>

                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_dateend()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <button type="button" class="btn btn-filto" data-dismiss="modal">
                                    Cancel
                                </button>
                            </div>              
                            <div class="col-md-3 text-right">
                                <button type="submit" class="btn btn-filtu" onclick="unsetPage()">
                                    Apply Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              <br>
            </form>
            </div>
          </div>
        </div>
        <div class="col-md-5">
            <button type="button" class="btn btn-md btn-filtu" onClick="javascript:goExport()">
                Export
            </button>
        </div>
    </div>
    <div class="panel-group col-md-12 <?= ($isFiltered) ? '' : 'hidden' ?> " id="accordion"  style="padding-left: 0px;padding-right: 0px;">
        <br>
        <div class="panel panel-default" style="display: flex; padding-top: 1%;padding-left: 2%; flex-wrap: wrap;">
            <span style="margin-top: 1%;">
                Filtered By:
            </span>
            <?php if (isset($_SESSION['document_search_listall']['title'])) { ?>
                <span class="label label-primary filter-tag" style="">
                    <a href="<?= site_url('web/document/removeFilterListAll/title') ?>" style="color: #FF5722;">
                        <i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
                            
                        </i>
                    </a>
                    keyword: <b><?= $_SESSION['document_search_listall']['title'] ?></b>
                </span>
            <?php } ?>
            <?php if (isset($_SESSION['document_search_listall']['type'])) { ?>
                <?php foreach ($_SESSION['document_search_listall']['arr_type'] as $k => $type) { ?>
                    <span class="label label-primary filter-tag" style="">
                        <a href="<?= site_url('web/document/removeFilterListAll/type') ?>/<?= $type ?>" style="color: #FF5722;">
                            <i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
                                
                            </i>
                        </a>
                        type: <b><?= $types[$type] ?></b>
                    </span>
                <?php } ?>
            <?php } ?>
            <?php if (isset($_SESSION['document_search_listall']['status'])) { ?>
                <?php foreach ($_SESSION['document_search_listall']['arr_status'] as $k => $status) { ?>
                    <span class="label label-primary filter-tag" style="">
                        <a href="<?= site_url('web/document/removeFilterListAll/status') ?>/<?= $status ?>" style="color: #FF5722;">
                            <i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
                                
                            </i>
                        </a>
                        <?php
//                         1. Document Drafting (1-1)
// 2. Document Revising (3-1)
// 3. Draft Content Review (1-2,3-2)
// 4. Evaluation Approval (2-5)
// 5. Evaluation Form Input (2-1)
// 6. Evaluation Form Input Drafter (2-0)
// 7. Obsolete (is obsolete =1)
// 8. Publish (is published=1)
// 9. Review and Approval (1-5)
// 10. Verification(1-4, 3-4)
                            if($status == 1){
                                $status_text = "Document Drafting";
                            }
                            elseif($status == 2){
                                $status_text = "Document Revising";
                            }
                            elseif($status == 3){
                                $status_text = "Draft Content Review";
                            }
                            elseif($status == 4){
                                $status_text = "Evaluation Approval";
                            }
                            elseif($status == 5){
                                $status_text = "Evaluation Form Input";
                            }
                            elseif($status == 6){
                                $status_text = "Evaluation Form Input Drafter";
                            }
                            elseif($status == 7){
                                $status_text = "Obsolete";
                            }
                            elseif($status == 8){
                                $status_text = "Publish";
                            }
                            elseif($status == 9){
                                $status_text = "Review and Approval";
                            }
                            elseif($status == 10){
                                $status_text = "Verification";
                            }
                            elseif($status == 11){
                                $status_text = "Withdrawal Verification";
                            }
                         ?>
                        status: <b><?= $status_text ?></b>
                    </span>
                <?php } ?>
            <?php } ?>
            <?php if (isset($_SESSION['document_search_listall']['unitkerja'])) { ?>
                <?php foreach ($_SESSION['document_search_listall']['arr_unitkerja'] as $k => $unitkerja) { ?>
                    <span class="label label-primary filter-tag" style="">
                        <a href="<?= site_url('web/document/removeFilterListAll/workunit') ?>/<?= $unitkerja ?>" style="color: #FF5722;">
                            <i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
                                
                            </i>
                        </a>
                        workunit: <b><?= $variables['unitkerja'][$unitkerja] ?></b>
                    </span>
                <?php } ?>
            <?php } ?>
            <?php if (isset($_SESSION['document_search_listall']['datestart'])) { ?>
                <span class="label label-primary filter-tag" style="">
                    <a href="<?= site_url('web/document/removeFilterListAll/datestart') ?>" style="color: #FF5722;">
                        <i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
                            
                        </i>
                    </a>
                    date start: <b><?= $_SESSION['document_search_listall']['datestart'] ?></b>
                </span>
            <?php } ?>
            <?php if (isset($_SESSION['document_search_listall']['dateend'])) { ?>
                <span class="label label-primary filter-tag" style="">
                    <a href="<?= site_url('web/document/removeFilterListAll/dateend') ?>" style="color: #FF5722;">
                        <i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
                            
                        </i>
                    </a>
                    date end: <b><?= $_SESSION['document_search_listall']['dateend'] ?></b>
                </span>
            <?php } ?>


            <span class="label label-primary filter-tag long-title hidden" style="">
                <a href="#" style="color: #FF5722;">
                    <i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
                        
                    </i>
                </a>

            </span>
        </div>
        <div class="col-md-12 <?= ($isFiltered) ? '' : 'hidden' ?>" style="background-color: #fff;margin: 0px;">
            <div class="col-md-12" style="padding: 2%;">
                <a href="<?= site_url('web/document/removeDocumentFilterListAll').'/'.'list_all' ?>">
                    <button type="button" class="btn btn-filto">
                        Clear Filter
                    </button>
                </a>
            </div>
            <!-- <div class="row col-md-4 text-right hidden"  style="padding: 2%;">         
                <button type="submit" class="btn btn-filtu" onclick="unsetPage()">
                    Add Filter
                </button>
            </div> -->
        </div>
    </div>
    <div class="row" style="margin-top:10px">
        <div class="col-md-5">
            <i class="" style="font-size:10px">*Only show 20 datas, export to view all data </i>
        </div>
    </div>   
<!-- </form> -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive" style="overflow-x:auto;">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Last Modified</th>
                                        <th class="text-center">Document Type</th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Document Code</th>
                                        <th class="text-center">Title</th>
                                        <th class="text-center">Revision</th>
                                        <th class="text-center">Business Process</th>
                                        <th class="text-center">ISO Requirements</th>
                                        <th class="text-center">Clauses</th>
                                        <th class="text-center">Related Documents</th>
                                        <th class="text-center">Published Date</th>
                                        <th class="text-center">Evaluation Schedule</th>
                                        <th class="text-center">Drafter</th>
                                        <th class="text-center">Drafter Date</th>
                                        <th class="text-center">Creator</th>
                                        <th class="text-center">Created Date</th>
                                        <th class="text-center">Reviewer</th>
                                        <th class="text-center">Verified by Document Controller</th>
                                        <th class="text-center">Verified Date</th>
                                        <th class="text-center">Approver</th>
                                        <th class="text-center">Approved Date</th>
                                        <th class="text-center">Work Unit</th>
                                        <th class="text-center">Departement</th>
                                        <th class="text-center">Retention Time</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Change Log</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $v['updated_at'] ?></td>
                                            <td><?= $v['type_name'] ?></td>
                                            <td><?= $v['document_id'] ?></td>
                                            <td><?= $v['code'] ?></td>
                                            <td><?= $v['title'] ?></td>
                                            <td><?= $v['revision'] ?></td>
                                            <td><?= $v['prosesbisnis_name'] ?></td>
                                            <td><?= $v['requirements'] ?></td>
                                            <td><?= $v['clauses'] ?></td>
                                            <td><?= $v['related_documents'] ?></td>
                                            <td><?= $v['published_at'] ?></td>
                                            <td><?= $v['validity_date'] ?></td>
                                            <td><?= $v['name'] ?></td>
                                            <td><?= $v['drafter_date'] ?></td>
                                            <td><?= $v['creator_name'] ?></td>
                                            <td><?= $v['created_date'] ?></td>
                                            <td><?= $v['reviewers'] ?></td>
                                            <td><?= $v['document_controller'] ?></td>
                                            <td><?= $v['verified_date'] ?></td>
                                            <td><?= $v['approver_name'] ?></td>
                                            <td><?= $v['approved_date'] ?></td>
                                            <td><?= $v['unitkerja_name'] ?></td>
                                            <td><?= $v['department_name'] ?></td>
                                            <td><?= $v['retention'] ?></td>
                                            <td><?= $v['task'] ?></td>
                                            <td><?= $v['description'] ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function remove_title(){
        document.getElementById('title').value=null;
    }
    function remove_type(){
        $('#type').val(null).trigger("change");
    }
    function remove_workunit(){
        $('#unitkerja').val(null).trigger("change");
    }
    function remove_datestart(){
        document.getElementById('datestart').value=null;
    }
    function remove_dateend(){
        document.getElementById('dateend').value=null;
    }

    $("#type").select2({
        placeholder: 'Search type here...'
    });
     $("#status").select2({
        placeholder: 'Search status here...'
    });
    $("#unitkerja").select2({
        placeholder: 'Search workunit...'
    });
$.fn.datepicker.defaults.format = "yyyy-mm-dd";

function dateChange(){
    var fStart = $("[name='start_date'");
    var fEnd = $("[name='end_date'");

    var aStart = fStart.val().split('-');
    var aEnd = fEnd.val().split('-');

    var dStart = new Date(aStart[0], aStart[1], aStart[2]);
    var dEnd = new Date(aEnd[0], aEnd[1], aEnd[2]);

    if(dStart > dEnd){
        fEnd.val(fStart.val());
    }
}

function goFilter(){
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;
    if (start_date==null || end_date==null || start_date=='' || end_date==''){
        location.href = "<?php echo base_url('/web/document/list_all') ?>/";
    } else {
        location.href = "<?php echo base_url('/web/document/list_all') ?>/?start_date="+start_date+"&end_date="+end_date;
    }
    
}

function goExport() {
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;
    window.open("<?php echo base_url('/web/report/exportListAll') ?>/?start_date="+start_date+"&end_date="+end_date);
}

function removeFilter(){
    document.getElementById("start_date").value = null;
    document.getElementById("end_date").value = null;
    $("#submit_button").click();
}

function goSearch(){
    var search = document.getElementById('search').value;
    var company = document.getElementById('company').value;
    var role = document.getElementById('role').value;
    location.href = "<?= site_url('web/user') ?>?search="+search+"&company="+company+"&role="+role;
}

</script>
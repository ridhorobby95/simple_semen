<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:5%">No</th>
                                        <th class="text-center">Jenis</th>
                                        <th class="text-center">Kode</th>
                                        <th class="text-center" style="width:15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td class="text-center">
                                                <?= $v['type'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $v['code'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                echo anchor('web/document_types/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                // echo anchor('web/document/edit/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip"  title="Hapus"');
                                                ?>
                                                <a href="javascript:hapus_jenis(<?php echo $v['id'] ?>,'<?php echo $v['type'] ?>')" class="text-danger" title="Hapus Jenis Dokumen">
                                                    <i class="fa fa-trash"></i>
                                                </a> 
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-sm-4 col-md-4" style="position:relative;left:1.5%">
                    <input type="text" name="type_name" id="type_name" placeholder="Nama Jenis" class="form-control" style="width:300px">
                </div>
                <div class="col-sm-4 col-md-4" style="position:relative;left:5%;bottom:1%;padding-bottom: 10px">
                    <span class="btn btn-sm btn-primary" id="btn_tambah_jenis">Tambah Jenis Dokumen</span>
                </div>
            </div>
        </div>

    </div>
</div>
<script>



    $('#btn_tambah_jenis').on('click', function () {
        var name = $('#type_name').val();
        location.href = "<?= site_url('web/document_types/add') ?>/" + name ;
    });    

    function hapus_jenis(id,name) {
        if (confirm('Hapus jenis "'+name+'" ?')){
            location.href = "<?= site_url('web/document_types/delete') ?>/"+id;
        }
    }

</script>
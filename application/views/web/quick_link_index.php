<script>
    $("#mainContent").addClass('col-sm-12');
    $("#mainContent").removeClass('col-sm-9');
</script>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Link</th>
                                        <th class="text-center">Description</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Icon</th>
                                        <?php if (SessionManagerWeb::isAdministrator()){ ?>
                                            <th class="text-center">Action</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($quicklink as $v) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $v['name'] ?></td>
                                            <td class="text-center"><?= $v['link'] ?></td>
                                            <td class="text-center"><?= $v['description'] ?></td>
                                            <td class="text-center"><?= ( $v['status'] == 1 ? 'Active':'Nonactive' ) ?></td>
                                            <td class="text-center"><i class="icon-io-<?= $v['icon']  ?>"></i></td>
                                            <?php if (SessionManagerWeb::isAdministrator() or (SessionManagerWeb::isAdminUnit() and $v['company']==SessionManagerWeb::getVariablesIndex('mycompany'))){ ?>
                                                <td style="text-align: center">
                                                    <?php
                                                    echo anchor('web/quick_link/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                    echo anchor('web/quick_link/delete/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-success deletemenu" data-toggle="tooltip" onclick="return confirm(\'Are You Sure?\')"  title="Delete" ') . '&nbsp;&nbsp;';
                                                   // echo anchor('web/user/delete/' . $v['user_id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip" title="Hapus"');
                                                    ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function goExport() {
    location.href = "<?php echo site_url($path . $class . '/add_import') ?>";
}

function goAdd() {
    location.href = "<?php echo site_url($path . $class . '/add') ?>";
}

function goBack(){
    location.href = "<?php echo site_url($path . 'setting') ?>";
}

function goSearch(){
    var search = document.getElementById('search').value;
    var company = document.getElementById('company').value;
    location.href = "<?= site_url('web/karyawan') ?>?search="+search+"&company="+company;
}
</script>
<style type="text/css">
    #rightSidebar {
        display: none!important;
    }
    #mainContent {
        margin-bottom: 50vh;
    }
</style>

<?php include('document_modal_viewdoc.php'); ?>

<script>
	$("#mainContent").removeClass('col-sm-9');
	$("#mainContent").addClass('col-sm-12');
</script>

<div id="creator_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Choose Manually</h4>
                <p class="sub-one-revisi">Choose the Users to become the Creator of this document</p>
            </div>
            <div class="modal-body" style="padding:30px;padding-bottom: 150px">
                <div class="col-md-9" style="/*width:50%;*/height:100%;padding-right:0px;padding-left: 0px;float: left;">
                    <label class="col-md-2 control-label" for="user">Creator</label>
                    <select id="user" name="user" class="form-control input-sm select2" style="width:60%; " onchange="userChanged()" >
                    	<option></option>
                        <?php
                            foreach($users as $user) {
                                echo "<option value='".$user['user_id']."'>".$user['name']."</option>";
                            }
                        ?>
                    </select>
                    <br>
                </div>
                <div class="col-md-9" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Position</label>
                    <input type='text' id="position_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="position_id" style="width:60%;" disabled required>
                    <br>
                </div>
                <div id="div_creator_approver" class="col-md-9 hidden" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Approver</label>
                    <?php echo form_dropdown('input_creator_approver_id',array(''=>null), null, 'id="input_creator_approver_id" class="form-control input-sm select2" style="width:60%; font-size: 12px; letter-spacing: 0.5px;" onChange="javascript:approverChange()"'); ?>
                    <!-- <input type='text' id="creator_approver_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="creator_approver_id" style="width:60%;" disabled required> -->
                    <br>
                </div>
                <div id="div_creator_approver_position" class="col-md-9 hidden" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Approver Position</label>
                    <input type='text' id="creator_approver_position_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="creator_approver_position_id" style="width:60%;" disabled required>
                    <br>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger tombolShareFix1" data-dismiss="modal" onClick="changeUnitCreator(<?= $document['document_id']?> )" style="background-color:#00B65C !important;border:3px solid #00B65C !important">Change Now</button>
            </div>
        </div>
    </div>
</div>

<div id="approver_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Choose Manually</h4>
                <p class="sub-one-revisi">Choose the Users to become the Approver of this document</p>
            </div>
            <div class="modal-body" style="padding:30px;padding-bottom: 100px">
                <div class="col-md-9" style="/*width:50%;*/height:100%;padding-right:0px;padding-left: 0px;float: left;">
                    <label class="col-md-2 control-label" for="approver">Approver</label>
                    <select id="approver" name="approver" class="form-control input-sm select2" style="width:60%; " onchange="userChanged(1)" >
                    	<option></option>
                        <?php
                            foreach($approvers as $approver) {
                                echo "<option value='".$approver['id']."'>".$approver['name']."</option>";
                            }
                        ?>
                    </select>
                    <br>
                </div>
                <div class="col-md-9" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position_approver">Position</label>
                    <input type='text' id="approver_position_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="approver_position_id" style="width:60%;" disabled required>
                    <br>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger tombolShareFix1" data-dismiss="modal" onClick="changeUnitApprover(<?= $document['document_id']?> )" style="background-color:#00B65C !important;border:3px solid #00B65C !important">Change Now</button>
            </div>
        </div>
    </div>
</div>

<div id="company_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Choose Manually</h4>
                <p class="sub-one-revisi">Choose the Users to become the Creator of this document</p>
            </div>
            <div class="modal-body" style="padding:30px;padding-bottom: 150px">
                <div class="col-md-9" style="/*width:50%;*/height:100%;padding-right:0px;padding-left: 0px;float: left;">
                    <label class="col-md-2 control-label" for="user">Company</label>
                    <select id="company" name="company" class="form-control input-sm select2" style="width:60%; " onchange="companyChanged()" >
                    	<option></option>
                        <?php
                            foreach($company_choose as $comp) {
                                echo "<option value='".$comp['id']."'>".$comp['name']."</option>";
                            }
                        ?>
                    </select>
                    <br>
                </div>
                <div id="div_workunit" class="col-md-9 hidden" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Workunit</label>
                    <?php echo form_dropdown('input_workunit_id',array(''=>null), null, 'id="input_workunit_id" class="form-control input-sm select2" style="width:60%; font-size: 12px; letter-spacing: 0.5px;" onChange="javascript:workunitChanged()"'); ?>
                    <!-- <input type='text' id="creator_approver_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="creator_approver_id" style="width:60%;" disabled required> -->
                    <br>
                </div>
                <div id="div_creator" class="col-md-9 hidden" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Creator</label>
                    <?php echo form_dropdown('input_workunit_creator_id',array(''=>null), null, 'id="input_workunit_creator_id" class="form-control input-sm select2" style="width:60%; font-size: 12px; letter-spacing: 0.5px;" onChange="javascript:workunitCreatorChange()"'); ?>
                    <!-- <input type='text' id="creator_approver_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="creator_approver_id" style="width:60%;" disabled required> -->
                    <br>
                </div>
                <div id="div_creator_position" class="col-md-9 hidden" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Creator Position</label>
                    <input type='text' id="workunit_creator_position_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="workunit_creator_position_id" style="width:60%;" disabled required>
                    <br>
                </div>
                <div id="div_approver" class="col-md-9 hidden" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Approver</label>
                    <?php echo form_dropdown('input_workunit_approver_id',array(''=>null), null, 'id="input_workunit_approver_id" class="form-control input-sm select2" style="width:60%; font-size: 12px; letter-spacing: 0.5px;"'); ?>
                    <!-- <input type='text' id="creator_approver_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="creator_approver_id" style="width:60%;" disabled required> -->
                    <br>
                </div>
                <div id="div_approver_position" class="col-md-9 hidden" style="height:100%;padding-right:0px;padding-left: 0px;float: left;padding-top: 10px">
                    <label class="col-md-2 control-label" for="Position">Approver Position</label>
                    <input type='text' id="workunit_approver_position_name" value=" -" style="width:60%;" disabled required>
                    <input class="hidden" type='text' value="" id="workunit_approver_position_id" style="width:60%;" disabled required>
                    <br>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #fff;     margin-top: 110px;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger tombolShareFix1" data-dismiss="modal" onClick="changeCompany(<?= $document['document_id']?> )" style="background-color:#00B65C !important;border:3px solid #00B65C !important">Change Now</button>
            </div>
        </div>
    </div>
</div>

<div id="reject_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title text-center" style="font-family: Source Sans Pro, sans-serif; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">
                    Rejection
                </h4>
                <p class="sub-one-revisi">Write Your Rejection Reason</p>
            </div>
            <div class="modal-body">
            	<form action="<?= site_url('web/document/setChangeUnitReason').'/'.$document['document_id'] ?>" method="post">
            		<h4 for="reason_textarea">Reason</h4>
                    <br>
                    <input type="text" id="is_approver" name="is_approver" value="" class="hidden">
					<textarea id="reason_textarea" placeholder="Write Reason Here..." name="reason_textarea" style="height:100px;width:100%" required></textarea>
					<button type="submit" id="submit_reject" name="submit_reject" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
				</form>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1-1" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_reject_modal" class="btn btn-sm btn-danger btn-rad" onClick="setReason(<?= $document['document_id'] ?>)"><i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Reject</span></button>
            </div>
        </div>
    </div>
</div>

<!-- Variables -->
<div class="row" style="margin-left:5px">
	<div class="row" style="margin-bottom: 1%">
		<div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px; width: 96%; margin-left: 1.5%">
			<!-- Detail Requestor -->
			<table class="table" >
				<!-- START : Requestor Detail -->
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h3 style="border-bottom: 1px solid;padding:3px;margin-top:7px"><b style="color:#0288D1">Status</b></h3></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Workunit</b></td>
					<td><?= ($document['unitkerja_name']==NULL) ? '-' : '<b>'.$document['unitkerja_name'].'</b>' ?></td>
				</tr>
				<?php if ($document['changeunit'] == 'X'){ ?>
					<tr>
						<td class="e-left-field" width="150"><b>Status</b></td>
						<td><?= $document['workunit_status']; ?></td>
					</tr>
					<tr>	
						<td class="e-left-field" width="150">
							<button id="btn_choose" class="btn btn-sm btn-secondary btn-rad" onClick="choose_company()" data-toggle="tooltip" title="Agree with Solution" style="margin-left:0.5%;">
								<i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Choose Manually</span>
							</button>
						</td>
					</tr>
				<?php } else{?>
				<tr>
					<td class="e-left-field" width="150"><b></b></td>
					<td></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Creator</b></td>
					<td><?= ($document['creator_name']==NULL) ? '-' : $document['creator_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Position</b></td>
					<td><?= ($document['creator_jabatan_name']==NULL) ? '-' : $document['creator_jabatan_name'] ?></td>
				</tr>
				<?php if ($document['changeunit_verification']['creator']==0) { ?>
					<tr>
						<td class="e-left-field" width="150"><b>Status</b></td>
						<td><?= ($document['creator_reason']==NULL or $document['creator_reason']=='') ? $document['creator_status'] : $document['creator_reason'] ?></td>
					</tr>
					<?php 
					// if ($document['creator_solution']!=NULL and (SessionManagerWeb::isAdministrator() or SessionManagerWeb::isDocumentController())) { 
					if ($document['creator_solution']!=NULL and (SessionManagerWeb::isDocumentController())) { 
					?>
						<tr>
							<td class="e-left-field" width="150"><b style="color:green">Solution</b></td>
							<td><?= $document['creator_solution'] ?></td>
						</tr>
						<tr>
							<td class="e-left-field" width="150">
								<button id="btn_choose" class="btn btn-sm btn-secondary btn-rad" onClick="choose_creator()" data-toggle="tooltip" title="Agree with Solution" style="margin-left:0.5%;">
									<i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Choose Manually</span>
								</button>
							</td>
							<?php if ($btn['creator_agree']) { ?>
								<td>
									<a href="<?= site_url('web/document/agreeWithSolution/'.$document['document_id'].'/creator') ?>">
										<button id="btn_agree" class="btn btn-sm btn-success btn-rad" data-toggle="tooltip" title="Agree with Solution" style="margin-left:0.5%;">
											<i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
										    <span>Agree with Solution</span>
										</button>
									</a>
								</td>
							<?php } ?>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td class="e-left-field" width="150"><b>Status</b></td>
						<td>Waiting Verification from Selected Creator ( <b style="color:green"> <?= $new_users['creator'] ?> </b>)</td>
					</tr>
					<?php 
					// if ($new['creator_id']==SessionManagerWeb::getUserID() or SessionManagerWeb::isAdministrator()) { 
					if ($new['creator_id']==SessionManagerWeb::getUserID()) { 
					?>
						<tr>
							<td class="e-left-field" width="150">
								<button id="bnt_reject" class="btn btn-sm btn-danger btn-rad" data-toggle="tooltip" onclick="reject(0)" title="Its not my document" style="margin-left:0.5%;">
									<i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Reject</span>
								</button>
							</td>
							<td>
								<a href="<?= site_url('web/document/verifyChangeunit/'.$document['document_id'].'/1/creator') ?>">
									<button id="btn_agree" class="btn btn-sm btn-success btn-rad" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
										<i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
									    <span>Approve</span>
									</button>
								</a>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
				<tr>
					<td class="e-left-field" width="150"><b></b></td>
					<td></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Approver</b></td>
					<td><?= ($document['approver_name']==NULL) ? '-' : $document['approver_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field" width="150"><b>Position</b></td>
					<td><?= ($document['approver_jabatan_name']==NULL) ? '-' : $document['approver_jabatan_name'] ?></td>
				</tr>
				<?php if ($document['changeunit_verification']['approver']==0) { ?>
					<tr>
						<td class="e-left-field" width="150"><b>Status</b></td>
						<td><?= ($document['approver_reason']==NULL or $document['approver_reason']=='') ? $document['approver_status'] : $document['approver_reason'] ?></td>
					</tr>
					<?php 
					// if ($document['approver_solution']!=NULL and (SessionManagerWeb::isAdministrator() or SessionManagerWeb::isDocumentController())) { 
					if ($document['approver_solution']!=NULL and (SessionManagerWeb::isDocumentController())) { 
					?>
						<tr>
							<td class="e-left-field" width="150"><b style="color:green">Solution</b></td>
							<td><?= $document['approver_solution'] ?></td>
						</tr>
						<tr>
							<td class="e-left-field" width="150">
								<button id="btn_choose" class="btn btn-sm btn-secondary btn-rad" onClick="choose_approver()" data-toggle="tooltip" title="Agree with Solution" style="margin-left:0.5%;">
									<i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Choose Manually</span>
								</button>
							</td>
							<?php if ($btn['approver_agree']) { ?>
								<td>
									<a href="<?= site_url('web/document/agreeWithSolution/'.$document['document_id'].'/approver') ?>">
										<button id="btn_agree" class="btn btn-sm btn-success btn-rad" data-toggle="tooltip" title="Agree with Solution" style="margin-left:0.5%;">
											<i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
										    <span>Agree with Solution</span>
										</button>
									</a>
								</td>
							<?php } ?>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td class="e-left-field" width="150"><b>Status</b></td>
						<td>Waiting Verification from Selected Approver ( <b style="color:green"> <?= $new_users['approver'] ?> </b>)</td>
					</tr>
					<?php 
					// if ($new['approver_id']==SessionManagerWeb::getUserID() or SessionManagerWeb::isAdministrator()) { 
					if ($new['approver_id']==SessionManagerWeb::getUserID()) {
					?>
						<tr>
							<td class="e-left-field" width="150">
								<button id="bnt_reject" class="btn btn-sm btn-danger btn-rad" data-toggle="tooltip" onclick="reject(1)" title="Its not my document" style="margin-left:0.5%;">
									<i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Reject</span>
								</button>
							</td>
							<td>
								<a href="<?= site_url('web/document/verifyChangeunit/'.$document['document_id'].'/1/approver') ?>">
									<button id="btn_agree" class="btn btn-sm btn-success btn-rad" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
										<i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
									    <span>Approve</span>
									</button>
								</a>
							</td>
						</tr>
					<?php } ?>
				<?php } ?>
				<?php } // tutupnya status change work unit X ?>
				
				

				

				<?php if ($document['changeunit']=='O' and $document['changeunit_moved']==0) { ?>
					<tr>
						<td class="e-left-field" width="150"><b></b></td>
						<td></td>
					</tr>
					<tr>
						<td class="e-left-field" colspan="2">
						This Document <b>Creator and Approver's </b> Position is <b style="color:green">Correct.</b> 
						<br>Please Click the button <b>"Move Now"</b> to move the document to the new workunit (<b style="color:green"><?= $creator['unitkerja_name'] ?></b>) .</td>
					</tr>
					<tr>
						<td>
							<a href="<?= site_url('web/document/moveChangeunit/'.$document['document_id']) ?>">
								<button id="btn_move" class="btn btn-sm btn-secondary btn-rad" data-toggle="tooltip" title="Move Now" style="margin-left:0.5%;">
									<i class="fa fa-arrow" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Move Now</span>
								</button>
							</a>
						</td>
					</tr>
				<?php } ?>
				
				<!-- ENDOF : Requestor Detail -->

				<!-- START : User Access Detail -->
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h3 style="border-bottom:1px solid;padding:3px;margin-top:7px"><b style="color:#0288D1">Document Detail</b></h3></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Title</b></td>
					<td><?= ($document['title']==NULL) ? '-' : $document['title'] ?></td>
				</tr>
		        <tr>
					<td class="e-left-field"><b>Drafter</b></td>
					<td><?= ($document['user_name']==NULL) ? '-' : $document['user_name'] ?></td>
				</tr>
		        <tr> 
					<td class="e-left-field"><b>Drafter Workunit</b></td>
					<td><?= ($document['user_workunit_name']==NULL) ? '-' : $document['user_workunit_name'] ?></td>
				</tr>
		        <tr>
					<td class="e-left-field"><b>Creator</b></td>
					<td><?= ($document['creator_name']==NULL) ? '-' : $document['creator_name'] ?></td>
				</tr>
		        <tr>
					<td class="e-left-field"><b>Approver</b></td>
					<td><?= ($document['approver_name']==NULL) ? '-' : $document['approver_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Type</b></td>
					<td><?= ($document['type_name']==NULL) ? '-' : $document['type_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Revision</b></td>
					<td><?= ($document['revision']==NULL) ? '-' : $document['revision'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Business Process</b></td>
					<td><?= ($document['prosesbisnis_name']==NULL) ? '-' : $document['prosesbisnis_name'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Code</b></td>
					<td><?= ($document['code']==NULL) ? '-' : $document['code'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Requirements</b></td>
					<td><?= ($document['requirement']==NULL) ? '-' : $document['requirement'] ?></td>
				</tr>
				<tr>
					<td class="e-left-field"><b>Clausuls</b></td>
					<td><?= ($document['clausul']==NULL) ? '-' : $document['clausul'] ?></td>
				</tr>
			</table>
			<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
				<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad " onClick="viewDocument(<?= $document['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
				    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
				    <span>View Document</span>
				</button>
			</div>
		</div>

	</div>
</div>
<script>
	function goBack(){
		location.href = "<?php echo site_url($path.$class.'/index') ?>";
	}

	function choose_creator(){
		$("#creator_modal").modal('show');
	}
	function choose_approver(){
		$("#approver_modal").modal('show');
	}
	function choose_company(){
		// alert('asd');
		$("#company_modal").modal('show');
	}

	function submit_rejection(){
		$("#submit_reject").click();
	}

	function changeUnitCreator(id){
		var position = $("#position_id").val();
		var creator_id = document.getElementById("user").value;
		var approver_id = $("#input_creator_approver_id").val();
		var approver_jabatan_id = document.getElementById("creator_approver_position_id").value;
		if (creator_id==null){
			alert('Must choose creator!');
		} else {
			if (position==null){
				alert('Must choose creator that have a position!');
			} else {
				$.ajax({
			        url : "<?php echo site_url('/web/document/ajaxUpdateCreator');?>/"+id,
			        type: 'post',
			        dataType: 'JSON',
			        data: {'position':position, 'creator_id':creator_id, "approver_id":approver_id, "approver_jabatan_id":approver_jabatan_id},
			        success:function(respon){
			        	window.location.replace("<?= site_url('web/document/changeunit') ?>/"+respon);
			        } 
			    });
			}
		}
	}

	function changeCompany(id){
		var company = $("#company").val();
		var workunit = $("#input_workunit_id").val();
		var creator_id = $("#input_workunit_creator_id").val();
		var creator_jabatan_id = document.getElementById("workunit_creator_position_id").value;
		var approver_id = $("#input_workunit_approver_id").val();
		var approver_jabatan_id = document.getElementById("workunit_approver_position_id").value;
		// console.log(company);
		// console.log(workunit);
		// console.log(creator_id);
		// console.log(creator_jabatan_id);
		// console.log(approver_id);
		// console.log(approver_jabatan_id);
		if (company==null){
			alert('Must choose company!');
		} 
		else if(workunit == null) {
			alert('Must choose workunit!');
		}
		else if(creator_id == null) {
			alert('Must choose creator!');
		}
		else if(creator_jabatan_id == null) {
			alert('Must choose creator that have a position!');
		}
		else if(approver_id == null) {
			alert('Must choose approver!');
		}
		else if(approver_jabatan_id == null) {
			alert('Must choose approver that have a position!');
		}
		else{
            $('.loading').addClass('fa fa-spinner fa-spin');
            $('.loadings').show();
			// console.log('ye masuk');
			$.ajax({
		        url : "<?php echo site_url('/web/document/ajaxUpdateCompany');?>/"+id,
		        type: 'post',
		        dataType: 'JSON',
		        data: {'company':company,'workunit':workunit, 'creator_id':creator_id, 'creator_jabatan_id':creator_jabatan_id,"approver_id":approver_id, "approver_jabatan_id":approver_jabatan_id},
		        success:function(respon){
		        	window.location.replace("<?= site_url('web/document/changeunit') ?>/"+respon);
		        } 
		    });
		}
	}

	function changeUnitApprover(id){
		var position = $("#approver_position_id").val();
		var approver_id = document.getElementById("approver").value;
		if (approver_id==null){
			alert('Must choose approver!');
		} else {
			if (position==null){
				alert('Must choose approver that have a position!');
			} else {
				$.ajax({
			        url : "<?php echo site_url('/web/document/ajaxUpdateApprover');?>/"+id,
			        type: 'post',
			        dataType: 'JSON',
			        data: {'position':position, 'approver_id':approver_id},
			        success:function(respon){
			        	window.location.replace("<?= site_url('web/document/changeunit') ?>/"+respon);
			        } 
			    });
			}
		}
	}

	function approverChange(){
		var approver_id = $('#input_creator_approver_id').val();
        $.ajax({
	        url : "<?php echo site_url('/web/document/ajaxGetPosition');?>",
	        type: 'post',
	        dataType: 'JSON',
	        data: {'user':approver_id},
	        success:function(respon){
	        	$("#div_creator_approver_position").removeClass("hidden");
        		document.getElementById("creator_approver_position_id").value = respon.id;
	        	document.getElementById("creator_approver_position_name").value = ' '+respon.name;
	        } 
	    });
	}


	function companyChanged(){
		var company =$("#company").val();
		// console.log(company);
		$("#div_workunit").removeClass("hidden");
		$.ajax({
			url : '<?= site_url('web/unitkerja/ajaxGetListUnitkerja') ?>/',
	        type: 'post',
	        cache: false,
	        data: {"company_id":company},
	        success: function(respon){
	            response = JSON.parse(respon);
	            console.log(response);
	            $("#input_workunit_id").select2().html('');
	            $("#input_workunit_id").select2({
	                data:response,
	                placeholder : 'Choose Workunit'
	            });
	            console.log(response[0].id);
	            workunitChanged(response[0].id);
	        }
		});

		
	}
	function workunitChanged(workunit = null){
		if(workunit === null){
			console.log('null lh');
			var workunit = $("#input_workunit_id").val();
		}
		else{
			console.log('tidak null');
		}
		// console.log(workunit);
		var type_id = <?= $document['type_id'] ?>;
		$("#div_creator").removeClass("hidden");
		$("#div_creator_position").removeClass("hidden");
		$.ajax({
			url : '<?= site_url('web/unitkerja/ajaxGetUserCreator/') ?>/'+workunit,
	        type: 'post',
	        cache: false,
	        data: {"type_id":type_id},
	        success: function(respon){
	        	if(respon != "null"){
	        		console.log('ini loh');
	        		console.log(respon);
	        		response = JSON.parse(respon);
	        		console.log(response);
		            // console.log(response);
		            $("#input_workunit_creator_id").select2().html('');
		            $("#input_workunit_creator_id").select2({
		                data:response,
		                placeholder : 'Choose Creator'
		            });
	                // $('#input_creator_approver_id').val(response[0].id).trigger('change');
	                var idCreator = response[0].id;
	                var type_id = <?= $document['type_id'] ?>;
	                $.ajax({
				        url : "<?php echo site_url('/web/document/ajaxGetPosition');?>",
				        type: 'post',
				        dataType: 'JSON',
				        data: {'user':idCreator},
				        success:function(respon){
				        	// $("#div_creator_approver_position").removeClass("hidden");
			        		document.getElementById("workunit_creator_position_id").value = respon.id;
				        	document.getElementById("workunit_creator_position_name").value = ' '+respon.name;

				        	$("#div_approver").removeClass("hidden");
							$("#div_approver_position").removeClass("hidden");
							var company = $("#company").val();
							// console.log('sadasd'+puaskah);
				        	// $('#workunit_creator_position_id').val().trigger('change');
				        	$.ajax({
						        url : "<?php echo site_url('/web/unitkerja/ajaxGetApprover');?>/"+company,
						        type: 'post',
						        dataType: 'JSON',
						        data: {"creator_id":idCreator, "type_id":type_id},
						        success:function(respon){
						        	response = respon;
						        	// console.log(response.approver[0].id);
						        	$("#input_workunit_approver_id").select2().html('');
						            $("#input_workunit_approver_id").select2({
						                data:response.approver,
						                placeholder : 'Choose Approver'
						            });
						            $('#input_workunit_approver_id').val(response.approver[0].id).trigger('change');
						            $.ajax({
								        url : "<?php echo site_url('/web/document/ajaxGetPosition');?>",
								        type: 'post',
								        dataType: 'JSON',
								        data: {'user':response.approver[0].id},
								        success:function(respon){
								        	response = respon;
								        	console.log('ini loh');
								        	console.log(response);
								        	// $("#div_creator_approver_position").removeClass("hidden");
								        	// $("#approver_position_name").val(' '+response.name);
							        		document.getElementById("workunit_approver_position_id").value = response.id;
								        	document.getElementById("workunit_approver_position_name").value = ' '+response.name;
								        } 
								    });
						        	
						        } 
						    });
				        } 
				    });	
	        	}
	        	else{
	        		$("#input_workunit_creator_id").select2().html('');
		            $("#input_workunit_creator_id").select2({
		                data:null,
		                placeholder : 'Choose Creator'
		            });
	        	}
	            
	        }
		});
	}

	function workunitCreatorChange(){
		var idCreator = $("#input_workunit_creator_id").val();

		var type_id = <?= $document['type_id'] ?>;
		$.ajax({
		        url : "<?php echo site_url('/web/document/ajaxGetPosition');?>",
		        type: 'post',
		        dataType: 'JSON',
		        data: {'user':idCreator},
		        success:function(respon){
		        	document.getElementById("workunit_creator_position_id").value = respon.id;
			        document.getElementById("workunit_creator_position_name").value = ' '+respon.name;
			        var company = $("#company").val();
					// console.log('sadasd'+puaskah);
		        	// $('#workunit_creator_position_id').val().trigger('change');
		        	$.ajax({
				        url : "<?php echo site_url('/web/unitkerja/ajaxGetApprover');?>/"+company,
				        type: 'post',
				        dataType: 'JSON',
				        data: {"creator_id":idCreator, "type_id":type_id},
				        success:function(respon){
				        	response = respon;
				        	// console.log(response.approver[0].id);
				        	$("#input_workunit_approver_id").select2().html('');
				            $("#input_workunit_approver_id").select2({
				                data:response.approver,
				                placeholder : 'Choose Approver'
				            });
				            $('#input_workunit_approver_id').val(response.approver[0].id).trigger('change');
				            $.ajax({
						        url : "<?php echo site_url('/web/document/ajaxGetPosition');?>",
						        type: 'post',
						        dataType: 'JSON',
						        data: {'user':response.approver[0].id},
						        success:function(respon){
						        	response = respon;
						        	console.log('ini loh');
						        	console.log(response);
						        	// $("#div_creator_approver_position").removeClass("hidden");
						        	// $("#approver_position_name").val(' '+response.name);
					        		document.getElementById("workunit_approver_position_id").value = response.id;
						        	document.getElementById("workunit_approver_position_name").value = ' '+response.name;
						        } 
						    });
				        	
				        } 
				    });
		        }
		});
	}

	function userChanged(is_approver=0){
		if (is_approver==0){
			var user = $("#user").val();
			$("#div_creator_approver").removeClass("hidden");

			var type_id = <?= $document['type_id'] ?>;
			$.ajax({
		        url : '<?= site_url('web/unitkerja/ajaxGetApprover') ?>/',
		        type: 'post',
		        cache: false,
		        data: {"creator_id":user, "type_id":type_id},
		        success: function(respon){
		            response = JSON.parse(respon);
		            $("#input_creator_approver_id").select2().html('');
		            $("#input_creator_approver_id").select2({
		                data:response.approver,
		                placeholder : 'Choose Approver'
		            });
	                $('#input_creator_approver_id').val(response.approver[0].id).trigger('change');
	                $.ajax({
				        url : "<?php echo site_url('/web/document/ajaxGetPosition');?>",
				        type: 'post',
				        dataType: 'JSON',
				        data: {'user':response.approver[0].id},
				        success:function(respon){
				        	$("#div_creator_approver_position").removeClass("hidden");
			        		document.getElementById("creator_approver_position_id").value = respon.id;
				        	document.getElementById("creator_approver_position_name").value = ' '+respon.name;
				        } 
				    });
		        }
		    });
		}
		else 
			var user = $("#approver").val();
			$.ajax({
		        url : "<?php echo site_url('/web/document/ajaxGetPosition');?>",
		        type: 'post',
		        dataType: 'JSON',
		        data: {'user':user},
		        success:function(respon){
		        	if (is_approver==0){
		        		document.getElementById("position_id").value = respon.id;
			        	document.getElementById("position_name").value = ' '+respon.name;
		        	} else {
		        		document.getElementById("approver_position_id").value = respon.id;
			        	document.getElementById("approver_position_name").value = ' '+respon.name;
		        	}
		        	
		        } 
		    });
	}

	function approve(id){
		$.ajax({
			url : "<?= site_url('web/document/approve_download/').'/' ?>/"+id,
			type: 'post',
			cache: false,
			data: {},
			success: function(respon){
				if (respon != 'ERROR') {
					window.location.replace("<?= site_url('web/document/download_detail') ?>/"+id);
				}
				else {
					alert('Something wrong. Try again later or call the Administrator');
				}
			}
		});
	}

	function reject(is_approver=0){
		document.getElementById("is_approver").value=is_approver;
		$("#reject_modal").modal('show');
	}

	function setReason(){
		$("#submit_reject").click();
	}

	function download_document(id){
		window.location.replace("<?php echo base_url('/web/document/download_watermark') ?>/"+id);
	}

	$(document).ready(function() {
	  	$("#user").select2({
	  	    placeholder: 'Choose User...'
	    });

	    $("#approver").select2({
	  	    placeholder: 'Choose User...'
	    });
	    $("#company").select2({
	  	    placeholder: 'Choose Company...'
	    });
	});

	function openNav() {
	    document.getElementById("viewDoc").style.width = "100%";
	}
	function closeNav() {
	    document.getElementById("viewDoc").style.width = "0%";
	}
	function baseName(str){
		var base = new String(str).substring(str.lastIndexOf('/') + 1); 
	    if(base.lastIndexOf(".") != -1)       
	        base = base.substring(0, base.lastIndexOf("."));
	   	return base;
	}

	var menuDisplayed = false;
	var menuBox = null;   
	jQuery(document).ready(function() {
		$(".clickable-row").click(function() {
			closeEdit();
			rowClick($(this));
		});
		$("#t1").delegate("tr", "contextmenu", function(e) {
			if($(this).data("url")!=null){
		   		// alert($(this).data("url").ukuran);
		   		var data = $(this).data("url");
		   		var left = arguments[0].clientX;
				var top = arguments[0].clientY;
                
				menuBox = window.document.querySelector(".menu");
				menuBox.style.left = left + "px";
				menuBox.style.top = top + "px";
				menuBox.style.display = "block";
		        arguments[0].preventDefault();
        		menuDisplayed = true;
        		rowClick($(this));
		   	}
		   	return false;
		});

		jQuery('.tabs .tab-links a').on('click', function(e)  {
	        var currentAttrValue = jQuery(this).attr('href');
	 		jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();
	 		jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
	 
	        e.preventDefault();
    	});
	});
	window.addEventListener("click", function() {
        if(menuDisplayed == true){
			menuBox.style.display = "none"; 
        }
        closeNav();
	}, true);

	function viewDocument(id){
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/view_real_docx/');?>" + "/" + id, true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {
			var response = JSON.parse(http.responseText);
			var title = response['document']['title'];
		   	document.getElementById("titleDoc").textContent = title;
		   	var basename = baseName(response['file']['real_encryption']);
		   	var namaFile = response['file']['nama_file'];
		   	var exts = namaFile.split(".");
		    var ext = exts[exts.length-1];
		    var divContent = document.getElementById("viewDocContent");
		    var inner = "";
		    if(ext == "pdf"){
		    	// inner = "<iframe name='viewer' src ='<?php echo site_url("web/document/view/");?>" +  "/" + basename + ".pdf' width='100%' height='100%'allowfullscreen webkitallowfullscreen ></iframe>";
		    	inner = "<iframe name='viewer' src ='<?php echo site_url("web/document/view_watermark/");?>" +  "/" + basename + "' width='100%' height='100%'allowfullscreen webkitallowfullscreen ></iframe>";
		    }else if(ext == "jpg" || ext =="png" || ext == "jpeg" || ext=='gif' || ext=='bmp'){
		   		inner = "<img src='" + response['file']['link'] + "' alt='<?= $ext;?>' style='width:100%;height:100%;float:middle;'/>";
		   	}else if (ext=='mp4' || ext=='mpeg' || ext=='avi' || ext=='flv' || ext=='wmv' || ext=='mov'){
		   		mime = ext;
		   		if (ext=='wmv')
		   			mime = "x-ms-wmv";
		   		else if (ext=="flv")
		   			mime = "x-flv";
		   		inner = "<video width='100%' height='100%' controls controlsList='nodownload'><source src='"+response['file']['link']+"' type='video/"+mime+"'>Browser anda tidak support video.</video>";
		   		
		   	}else if(ext != "zip" || ext != "rar" || ext != "gz" || ext=='xmind'){
		   		inner = "<div style='width: 100%; height: 100%; position: inline;'>";
		   		// inner = inner + "<iframe src='https://docs.google.com/gview?url=<?php echo site_url("/web/thumb/files");?>"+'/' + basename + "." + ext + "&embedded=true' frameborder='0' scrolling='no' seamless style='width:100%;height:100%;float:middle;'></iframe>";
		   		inner = inner + "<iframe src='https://view.officeapps.live.com/op/view.aspx?src=<?php echo site_url("/web/thumb/files");?>"+'/' + basename + "&embedded=true' frameborder='0' scrolling='no' seamless style='width:100%;height:100%;float:middle;'></iframe>";
		   		
	    		// inner = inner + "<div style='width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;'>&nbsp;</div>";
	    		inner = inner + "<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>";
	    	}
	    	divContent.innerHTML = inner;
		    openNav();
		}
	}


</script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>

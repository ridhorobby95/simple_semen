<!-- Variables -->
<label id="dokumen" style="display:none;">0</label>
<label id="access_eval" style="display:none;">0</label>
<label id="drafter_eval" style="display:none;">0</label>

<!-- Pop Up -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Revision</h4>
                <p class="sub-one-revisi">Pilih file lama yang ingin dikembalikan</p>
            </div>
            <div class="modal-body">
                <table id="listfile" style="width:100%;padding:5px;">
                </table>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeRevision" type="button" class="btn tombolShareFix1" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<div id="ShareBox" class="modal fade">
    <div class="modal-dialog" class="share_modal" style="width:600px !important;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff; border-bottom: 1px solid #E0E0E0;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:#fff">&times;</button>
                <h4 id="LinkTitle" class="modal-title" style="color:#9E9E9E; font-weight: bold;text-align: center; margin: 15px;">Buat Link Share Document</h4>
            </div>
            <div class="modal-body" style="background-color: #fff;">
                <p id="KeteranganShare" style="color:#757575; font-weight: bold;margin: 15px; letter-spacing: 1px;">Apakah anda yakin untuk membuat link share document?</p>
            </div>
            <div class="modal-footer" style="background-color: #fff;padding: 25px 10px 25px 10px;">
            	<div class="col-md-5"></div>
            	<button type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal" onClick="createLink()">Buat</button>
            	<button id='closeShareBox' type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<div id="last_evaluation_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="last_evaluation_title" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Last Evaluation</h4>
                <p class="sub-one-revisi">The last evaluation for this document is shown below.</p>
            </div>
            <div class="modal-body">
                <table id="last_evaluation_list" style="width:100%;padding:5px;">
                </table>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="close_last_evaluation_modal" type="button" class="btn tombolShareFix1" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="DeleteBox" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="DeleteTitle" class="modal-title" style="margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Delete Document</h4>
            </div>
            <div class="modal-body">
                <p id="keteranganDelete">Apakah anda yakin untuk menghapus dokumen </p>
                <p style="font-weight: bold;font-size:18px;" id="namaDocDelete"></p>
            </div>
            <div class="modal-footer" style="background-color: #fff;padding: 25px 10px 25px 10px;">
                <div class="col-md-5"></div>
            	<button type="button" class="btn tombolShare2 col-md-3" data-dismiss="modal">Close</button>
                <button type="button" class="btn tombolShare1 col-md-3" data-dismiss="modal" onClick="deleteDocument()">Delete</button>
            </div>
        </div>
    </div>
</div>
<div id="AlertBox" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="PemberitahuanTitle " class="modal-title title-compare" style="">PEMBERITAHUAN</h4>
            </div>
            <div class="modal-body" style="margin:20px 20px 20px 20px;">
                <p id="Keterangan">Link share telah berhasil dibuat</p>
                <p style=""><a href="" id="Pesan"><large><?= site_url('web/document/share/'); ?> </large></a></p>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
            	<div class="col-md-10"></div>
                <button type="button" class="btn tombolShareFix1 col-md-6" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div id="UploadBox" class="modal fade">
   	<div class="modal-dialog">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header modal-baru">
            	<div class="row">
            		<div class="col-sm-12">
		            	<div class="row">
							<div class="col-sm-10">
		                		<h4 id="PerbaharuiTitle" class="modal-title modal-style">Perbaharui Dokumen</h4>
		                	</div>
		            		<div class="col-sm-2">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;
								</button>
							</div>

		                </div>
		            </div>
                </div>
            </div>
            <div class="modal-body row">
	            <div id="alertForPerbaharui"  style="display:none;">
		            <div class="alert alert-warning">
		  				<strong>Warning!</strong> <label id="alertMessagePerbaharui">File sudah pernah diunggah!</label>
					</div>
				</div>
				<div class="col-sm-12 row" style="margin:0px 30px; text-align: center;">
	                <form action="<?php echo site_url('web/document/documentform'); ?>" align="center" class="dropzone col-sm-6 upload-boxing" id="dropbox"  style="border: 3px dotted #27ae60; padding: 20px 50px 0px 50px">
	                	<div id="containerDropbox3" style="display:inline;">
	                        <div class="col-sm-12 dz-message" data-dz-message style=" margin: 0%;">
	                        	<img src="<?php echo site_url("assets/web/images/icon/Group255.png");?>" style="">
	                            <h4 align="center" style="color: #BDBDBD;letter-spacing: 1px;"><b>Geser File ke Sini</b></h4>
	                            <div class="row">
	                                <div id="files_preview" name="files_preview" class="col-md-12" align="center"></div>
	                            </div>
							</div>
	                    </div>
	                    <div id="scanContainer3" style="display:none;"><img id="dwtcontrolContainer3" src="" style="margin-top:5px;"></div>
	                </form>
	                <div id="containerRevisiButton" class="col-sm-6" style="text-align: center; margin-top:80px;">
		               	<span class="btn tombolBaru12" onClick="upload()">Pilih Dokumen</span> 
		               	<br>
		                <button type="button" class="btn tombolBarui2" onClick="openScan2()" style="display:none">Pindai Dokumen</button>
		            </div>
				</div>
	       	</div>
	       	<div class="modal-footer" style="background-color: #Fff;margin-top:15px;">
		        <div class="col-sm-12" >
					<?php echo form_textarea(array('name' => 'updateNote', 'value' => '', 'class' => 'form-control input-sm', 'id' => 'updateNote','placeholder' => "Catatan Perubahan Dokumen", 'rows' => 5, 'style' => 'height:80px;width:500px;margin-bottom:20px;margin-left:120px;')) ?>
				</div>
	           	<div class="col-md-3" style="margin-left:70px;"></div>
		        <button type="button" class="btn tombolBaruifix col-md-2" onClick="send()" style="margin: 0px 10px;">Unggah</button>
			    <button type="button" class="btn tombolBarui22 col-md-2" data-dismiss="modal">Batal</button>
	        </div>
	    </div>
    </div>
</div>

<div id="ask_download_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="downloadReason" class="modal-title text-center" style="font-family: Source Sans Pro, sans-serif; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">
                    Download
                </h4>
                <p class="sub-one-revisi">Write Your Download Reason</p>
            </div>
            <div class="modal-body">
                <h4 for="download_reason_textarea">Reason</h4>
                <br>
                <textarea id="download_reason_textarea" placeholder="Write Reason Here..." name="reason_textarea" style="height:100px;width:100%" required></textarea>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1-1" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_reject_modal" class="btn btn-sm btn-rad tombolShareFix1" onClick="javascript:askDownload()"><i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Ask Download</span></button>
            </div>
        </div>
    </div>
</div>

<div id="download_creator_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="downloadReason" class="modal-title text-center" style="font-family: Source Sans Pro, sans-serif; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">
                    Download
                </h4>
            </div>
            <div class="modal-body">
                <h4 for="download_reason_textarea">Choose Your File</h4>
                <br>
                <button  type="button" class="btn col-md-6 tombolShare1-1" data-dismiss="modal" onclick="Download_creator_proc(1)"><i class="fa fa-file"></i> Word File</button>
                <button  type="button" class="btn col-md-6 tombolShare1-1" data-dismiss="modal" onclick="Download_creator_proc(0)"><i class="fa fa-file"></i> PDF File</button>
                <br>
                <br>
                <br><br>
                <!-- <button type="button" id="btn_reject_modal" class="btn btn-sm btn-rad tombolShareFix1" onClick="javascript:askDownload()"><i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Ask Download</span></button> -->
            </div>
        </div>
    </div>
</div>


<div id="template_modal" class="modal fade" style="z-index: 1100">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="Template Title" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Download Template</h4>
                <p class="sub-one-revisi">Choose Templates that you want to download</p>
            </div>
            <div class="modal-body">
                <table id="listfile" style="width:100%;padding:5px;">
                    <tr>
                        <td>
                            <h5><b>TEMPLATE PEDOMAN</b></h5>
                            
                        </td>
                        <td>
                            <a href="<?= site_url('application/files/template/FP_SMI_SYM_002_R0_01012018%20TEMPLATE%20PEDOMAN.docx') ?>" target="_blank" class='btn btn-info' style='float:right;width:100%;'>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5><b>TEMPLATE PROSEDUR</b></h5>
                            
                        </td>
                        <td>
                            <a href="<?= site_url('application/files/template/FP_SMI_SYM_003_R0_01012018%20TEMPLATE%20PROSEDUR.docx') ?>" target="_blank" class='btn btn-info' style='float:right;width:100%;'>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5><b>TEMPLATE INSTRUKSI KERJA</b></h5>
                            
                        </td>
                        <td>
                            <a href="<?= site_url('application/files/template/FP_SMI_SYM_004_R0_01012018%20TEMPLATE%20INSTRUKSI%20KERJA.docx') ?>" target="_blank" class='btn btn-info' style='float:right;width:100%;'>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5><b>TEMPLATE FORM PEDOMAN</b></h5>
                            
                        </td>
                        <td>
                            <a href="<?= site_url('application/files/template/FP_SMI_SYM_005_R0_01012018%20TEMPLATE%20FORM%20PEDOMAN.docx') ?>" target="_blank" class='btn btn-info' style='float:right;width:100%;'>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5><b>TEMPLATE FORM PROSEDUR</b></h5>
                            
                        </td>
                        <td>
                            <a href="<?= site_url('application/files/template/FP_SMI_SYM_006_R0_01012018%20TEMPLATE%20FORM%20PROSEDUR.docx') ?>" target="_blank" class='btn btn-info' style='float:right;width:100%;'>Download</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5><b>TEMPLATE FORM INSTRUKSI KERJA</b></h5>
                            
                        </td>
                        <td>
                            <a href="<?= site_url('application/files/template/FP_SMI_SYM_007_R0_01012018%20TEMPLATE%20FORM%20INSTRUKSI%20KERJA.docx') ?>" target="_blank" class='btn btn-info' style='float:right;width:100%;'>Download</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="close_template_modal" type="button" class="btn tombolShareFix1" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
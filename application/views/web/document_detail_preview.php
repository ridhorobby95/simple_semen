<?php 
$i = 0;
if ($data['workflow_id']==1) {
	$role = $this->User_model->getBy($data['user_id'], 'role');
	$i++;
	if ($role==Role::ADMINISTRATOR) {
		$role=Role::DRAFTER;
	}
} ?>

<style>
	.popover.fade.bottom.in{
	    top: 143px;
	    left: -50%;
	    display: block;
	    width: 400%;
	}
</style>


<button class="btn btn-primary hidden" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
	Button with data-target
</button>
<div class="collapse hidden" id="collapseExample">
	<div class="well">
		<div class="col-xs-1" style="width: 13%;padding: 0px 5px;" >
			        <div class="panel panel-default <?= $shadow ?>" style="margin: 0px !important;width: 85%;<?= $panel_color ?>">
						<div class="panel-body" style="padding:10px;height:70px;">
							<p class="" style="font-size: 12px;">
								<b><?= $no.'. '. $node['name'] ?></b>
								
							</p>
						</div>
						<div class="panel-heading <?= $div_background ?> cobaneh" data-html="true" data-toggle="popover" data-placement="bottom" data-content="<?= $node['urutan']==1 ? $data['username'] : $node['actor_name'] ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;height: 70px;padding:10px;">
								<?php 
									if ($node['end']==1)
										// continue;
								?>
								<?= Role::name($node['user']) ?>
							<br>
							<h7 style="font-size:7px">
								<?= $node['urutan']==1 ? $data['username'] : $node['actor_name'] ?>
							</h7>
						</div>
						<div class="panel-heading hidden <?= $div_background ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;height: 70px;padding:10px;">
							<?php 
								if ($node['end']==1)
									// continue;
							?>
							<?= Role::name($node['user']) ?>
							<br>
							<h7 style="font-size:7px">
								<?= $node['urutan']==1 ? $data['username'] : $node['actor_name'] ?>
							</h7>
						</div>
						<i class="fa fa-caret-right position-arrow <?= $arrow_color ?>"></i>
					</div>
				</div>
				<div class="col-xs-1" style="width: 13%;padding: 0px 5px;" >
			        <div class="panel panel-default <?= $shadow ?>" style="margin: 0px !important;width: 85%;<?= $panel_color ?>">
						<div class="panel-body" style="padding:10px;height:70px;">
							<p class="" style="font-size: 12px;">
								<b><?= $no.'. '. $node['name'] ?></b>
								
							</p>
						</div>
						<div class="panel-heading <?= $div_background ?> cobaneh" data-html="true" data-toggle="popover" data-placement="bottom" data-content="<?= $node['urutan']==1 ? $data['username'] : $node['actor_name'] ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;height: 70px;padding:10px;">
								<?php 
									if ($node['end']==1)
										// continue;
								?>
								<?= Role::name($node['user']) ?>
							<br>
							<h7 style="font-size:7px">
								<?= $node['urutan']==1 ? $data['username'] : $node['actor_name'] ?>
							</h7>
						</div>
						<div class="panel-heading hidden <?= $div_background ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;height: 70px;padding:10px;">
							<?php 
								if ($node['end']==1)
									// continue;
							?>
							<?= Role::name($node['user']) ?>
							<br>
							<h7 style="font-size:7px">
								<?= $node['urutan']==1 ? $data['username'] : $node['actor_name'] ?>
							</h7>
						</div>
						<i class="fa fa-caret-right position-arrow <?= $arrow_color ?>"></i>
					</div>
				</div>
	</div>
</div>


<div class="row" >
	<div class="col-md-12">
		<div class="row" >
		<?php
			$no=1;
			foreach ($tasks as $node) {
				$arrow_color = "";
				$shadow = "";
				$extra_text = '';
				// $bgcolor = '#efefef';
				$color = '#333';
				$disable = '';
				$div_background = "flow-edit-before";
				$chevron_color="#efefef";
				$panel_color = "border-top: 3px solid #9E9E9E;";
				if ($node['urutan'] <= $data['workflow_urutan']) {
					$arrow_color = "arrow-done";
					$panel_color = "border-top: 3px solid #2ecc71;";
					$chevron_color="green";
					$div_background = "flow-edit-done";
					// $bgcolor = '#BE9005';
					// $bgcolor = 'blue-style';
					if ($node['urutan'] == 1 and 1==0) {
						if ($data['status']=='P') {
							if ($node['user']==Role::DRAFTER) {
								// continue;
							}
						} else
						if ($role!=$node['user']) {
							// continue;
						}
						// $bgcolor = 'red-style';

					}
					if ($node['urutan']==$data['workflow_urutan']) {
						if ($data['status']!='D'){
							$arrow_color = "arrow-progress";
							$panel_color = "border-top: 3px solid #FFB300;";
							$chevron_color="#efefef";
							$div_background = "flow-edit-progress";
							$shadow = "shadowplay";
							if ($data['status']=='C'){
								$shadow = "";
								$arrow_color = "";
								$panel_color = "border-top: 3px solid #d50000;";
								$div_background="flow-edit-reject";
							}
						}
						
						
						// $bgcolor = 'yellow-style';
					}
					$color = '#fff';
					if ($i ==  count($tasks)-1 ) {
						$bgcolor = 'green-style';
						$extra_text = 'Done';
					}
				} else {
					$disable = 'pointer-events:none;cursor: not-allowed';
				}

			?>
				<div class="col-xs-1" style="width: 13%;padding: 0px 5px;" >
			        <div class="panel panel-default <?= $shadow ?>" style="margin: 0px !important;width: 85%;<?= $panel_color ?>">
						<div class="panel-body" style="padding:10px; height:70px; background-image:url(<?= $node['background'] ?>)">
							<p class="" style="font-size: 12px;">
								<b><?= $no.'. '. $node['name'] ?></b>
								
							</p>
						</div>
						<div class="panel-heading <?= $div_background ?> cobaneh" data-html="true" data-toggle="popover" data-placement="bottom" data-content="<?= ($node['urutan']==1 and $data['workflow_id']=='1') ? $data['name'] : $node['actor_name'] ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;height: 70px;padding:10px;">
								<?php 
									if ($node['end']==1)
										continue;
								?>
								<?php
								$actor_role = $node['user'];
								// if ($node['urutan']==1 and in_array($data['workflow_id'], array(2,3))){
								// 	if ($data['creator_id']!=$data['user_id']){
								// 		$actor_role = 'D';
								// 	}
								// }
								echo Role::name($actor_role) 
								?>
							<br>
							<h7 style="font-size:10px">
								<?= ($node['urutan']==1 and $data['workflow_id']=='1') ? $data['name'] : $node['actor_name'] ?>
							</h7>
						</div>

						<div class="panel-heading hidden <?= $div_background ?>" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;height: 70px;padding:10px;">
							<?php 
								if ($node['end']==1)
									continue;
							?>
							<?= Role::name($node['user']) ?>
							<br>
							<h7 style="font-size:10px">
								<?= ($node['urutan']==1 and $data['workflow_id']=='1') ? $data['username'] : $node['actor_name'] ?>
							</h7>
						</div>
						<i class="fa fa-caret-right position-arrow <?= $arrow_color ?>"></i>
					</div>
            		<button type="button" class="btn btn-fix btn-circle btn-xl hidden" data-toggle="tooltip" data-placement="top" title="Validasi" style="    position: relative;margin: -230% 0% 0% 70%;z-index: 100;">
            			<i class="fa fa-check">
            			</i>                            				
            		</button>
				</div>
			<?php $i++;$no++; } ?>	
		</div>
	</div>
</div>






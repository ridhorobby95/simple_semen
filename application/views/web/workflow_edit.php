<script src="<?= base_assets(); ?>js/go.js"></script>

<script id="code">
  function init() {
    if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $ = go.GraphObject.make;  // for conciseness in defining templates

    myDiagram =
      $(go.Diagram, "myDiagramDiv",  // must name or refer to the DIV HTML element
        {
          initialContentAlignment: go.Spot.Center,
          "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
          initialAutoScale: go.Diagram.Uniform,
          allowDrop: true,  // must be true to accept drops from the Palette
          "LinkDrawn": showLinkLabel,  // this DiagramEvent listener is defined below
          "LinkRelinked": showLinkLabel,
          // scrollsPageOnFocus: false,
          // layout: $(go.LayeredDigraphLayout, { isInitial: false, isOngoing: false, layerSpacing: 50 }),
          "undoManager.isEnabled": true  // enable undo & redo
        });

    // when the document is modified, add a "*" to the title and enable the "Save" button
    myDiagram.addDiagramListener("Modified", function(e) {
      var button = document.getElementById("SaveButton");
      if (button) button.disabled = !myDiagram.isModified;
      var idx = document.title.indexOf("*");
      if (myDiagram.isModified) {
        if (idx < 0) document.title += "*";
      } else {
        if (idx >= 0) document.title = document.title.substr(0, idx);
      }
    });

    // helper definitions for node templates

    function nodeStyle() {
      return [
        // The Node.location comes from the "loc" property of the node data,
        // converted by the Point.parse static method.
        // If the Node.location is changed, it updates the "loc" property of the node data,
        // converting back using the Point.stringify static method.
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        {
          // the Node.location is at the center of each node
          locationSpot: go.Spot.Center,
          //isShadowed: true,
          //shadowColor: "#888",
          // handle mouse enter/leave events to show/hide the ports
          mouseEnter: function (e, obj) { showPorts(obj.part, true); },
          mouseLeave: function (e, obj) { showPorts(obj.part, false); }
        }
      ];
    }

    // Define a function for creating a "port" that is normally transparent.
    // The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
    // and where the port is positioned on the node, and the boolean "output" and "input" arguments
    // control whether the user can draw links from or to the port.
    function makePort(name, spot, output, input) {
      // the port is basically just a small circle that has a white stroke when it is made visible
      return $(go.Shape, "Circle",
               {
                  fill: "transparent",
                  stroke: null,  // this is changed to "white" in the showPorts function
                  desiredSize: new go.Size(8, 8),
                  alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
                  portId: name,  // declare this object to be a "port"
                  fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
                  fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
                  cursor: "pointer"  // show a different cursor to indicate potential link point
               });
    }

    // define the Node templates for regular nodes

    var lightText = 'whitesmoke';

    myDiagram.nodeTemplateMap.add("",  // the default category
      $(go.Node, "Spot", nodeStyle(),
        // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
        $(go.Panel, "Auto",
          $(go.Shape, "Rectangle",
            { fill: "#00A9C9", stroke: null },
            new go.Binding("figure", "figure")),
          $(go.TextBlock,
            {
              font: "bold 11pt Helvetica, Arial, sans-serif",
              stroke: lightText,
              margin: 8,
              maxSize: new go.Size(160, NaN),
              wrap: go.TextBlock.WrapFit,
              editable: true
            },
            new go.Binding("text").makeTwoWay())
        ),
        // four named ports, one on each side:
        makePort("T", go.Spot.Top, false, true),
        makePort("L", go.Spot.Left, true, true),
        makePort("R", go.Spot.Right, true, true),
        makePort("B", go.Spot.Bottom, true, false)
      ));

    myDiagram.nodeTemplateMap.add("Decision",  // the default category
      $(go.Node, "Spot", nodeStyle(),
        // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
        $(go.Panel, "Auto",
          $(go.Shape, "Diamond",
            { fill: "#79C900", stroke: null },
            new go.Binding("figure", "figure")),
          $(go.TextBlock,
            {
              font: "bold 11pt Helvetica, Arial, sans-serif",
              stroke: lightText,
              margin: 8,
              maxSize: new go.Size(160, NaN),
              wrap: go.TextBlock.WrapFit,
              editable: true
            },
            new go.Binding("text").makeTwoWay())
        ),
        // four named ports, one on each side:
        makePort("T", go.Spot.Top, false, true),
        makePort("L", go.Spot.Left, true, true),
        makePort("R", go.Spot.Right, true, true),
        makePort("B", go.Spot.Bottom, true, false)
      ));

    myDiagram.nodeTemplateMap.add("Start",
      $(go.Node, "Spot", nodeStyle(),
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(40, 40), fill: "#DC3C00", stroke: null }),
          $(go.TextBlock, "Start",
            { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
            new go.Binding("text"))
        ),
        // three named ports, one on each side except the top, all output only:
        makePort("L", go.Spot.Left, true, false),
        makePort("R", go.Spot.Right, true, false),
        makePort("B", go.Spot.Bottom, true, false)
      ));

    myDiagram.nodeTemplateMap.add("End",
      $(go.Node, "Spot", nodeStyle(),
        $(go.Panel, "Auto",
          $(go.Shape, "Circle",
            { minSize: new go.Size(20, 20),  fill: "#DC3C00", stroke: null }),
          $(go.TextBlock, "End",
            { font: "bold 11pt Helvetica, Arial, sans-serif", stroke: lightText },
            new go.Binding("text"))
        ),
        // three named ports, one on each side except the bottom, all input only:
        makePort("T", go.Spot.Top, false, true),
        makePort("L", go.Spot.Left, false, true),
        makePort("R", go.Spot.Right, false, true)
      ));

    myDiagram.nodeTemplateMap.add("Comment",
      $(go.Node, "Auto", nodeStyle(),
        $(go.Shape, "File",
          { fill: "#EFFAB4", stroke: null }),
        $(go.TextBlock,
          {
            margin: 5,
            maxSize: new go.Size(200, NaN),
            wrap: go.TextBlock.WrapFit,
            textAlign: "center",
            editable: true,
            font: "bold 12pt Helvetica, Arial, sans-serif",
            stroke: '#454545'
          },
          new go.Binding("text").makeTwoWay())
        // no ports, because no links are allowed to connect with a comment
      ));


    // replace the default Link template in the linkTemplateMap
    myDiagram.linkTemplate =
      $(go.Link,  // the whole link panel
        {
          routing: go.Link.AvoidsNodes,
          curve: go.Link.JumpOver,
          corner: 5, toShortLength: 4,
          relinkableFrom: true,
          relinkableTo: true,
          reshapable: true,
          resegmentable: true,
          // mouse-overs subtly highlight links:
          mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
          mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
        },
        new go.Binding("points").makeTwoWay(),
        $(go.Shape,  // the highlight shape, normally transparent
          { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
        $(go.Shape,  // the link path shape
          { isPanelMain: true, stroke: "gray", strokeWidth: 2 }),
        $(go.Shape,  // the arrowhead
          { toArrow: "standard", stroke: null, fill: "gray"}),
        $(go.Panel, "Auto",  // the link label, normally not visible
          { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5},
          new go.Binding("visible", "visible").makeTwoWay(),
          $(go.Shape, "RoundedRectangle",  // the label shape
            { fill: "#F8F8F8", stroke: null }),
          $(go.TextBlock, "Yes",  // the label
            {
              textAlign: "center",
              font: "10pt helvetica, arial, sans-serif",
              stroke: "#333333",
              editable: true
            },
            new go.Binding("text").makeTwoWay())
        )
      );

    // Make link labels visible if coming out of a "conditional" node.
    // This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
    function showLinkLabel(e) {
      var label = e.subject.findObject("LABEL");
      if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
    }

    // temporary links used by LinkingTool and RelinkingTool are also orthogonal:
    myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
    myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;

    load();  // load an initial diagram from some JSON text

    // initialize the Palette that is on the left side of the page
    myPalette =
      $(go.Palette, "palette",  // must name or refer to the DIV HTML element
        {
          // scrollsPageOnFocus: false,
          nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
          model: new go.GraphLinksModel([  // specify the contents of the Palette
            { category: "Start", text: "Start" },
            { text: "Step" },
            { text: "Decision", figure: "Diamond" },
            { category: "End", text: "End" },
            { category: "Comment", text: "Comment" }
          ])
        });
  } // end init

  // Make all ports on a node visible when the mouse is over the node
  function showPorts(node, show) {
    var diagram = node.diagram;
    if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
    node.ports.each(function(port) {
        port.stroke = (show ? "white" : null);
      });
  }


  // Show the diagram's model in JSON format that the user may edit
  function save() {
    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
    myDiagram.isModified = false;
  }
  function load() {
    myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
  }

  // add an SVG rendering of the diagram at the end of this page
  function makeSVG() {
    var svg = myDiagram.makeSvg({
        scale: 0.5
      });
    svg.style.border = "1px solid black";
    obj = document.getElementById("SVGArea");
    obj.appendChild(svg);
    if (obj.children.length > 0) {
      obj.replaceChild(svg, obj.children[0]);
    }
  }
</script>
<hr/>
<div id="sample">
  <div style="width:100%; white-space:nowrap;">
    <span style="display: none; vertical-align: top; padding: 5px; width:0px">
      <div id="palette" style="border: solid 1px black; height: 480px;display: none;"></div>
    </span>
    <span style="display: inline-block; vertical-align: top; padding: 1px; width:95%">
      <div id="myDiagramDiv" style="border: solid 1px #ccc; height: 500px;width:1000px" ></div>
    </span>
  </div>

<div style="display: none">
  <button id="SaveButton" onclick="save()">Save</button>
  <button onclick="load()">Load</button>
  <button onclick="layout()">Layout</button>
  Diagram Model saved in JSON format:
  <br />
 </div>

<?php

$nodes = $workflow['rule']['model']['nodeDataArray'];
$linkDataArray = $workflow['rule']['model']['linkDataArray'];

?>

  <textarea id="mySavedModel" style="width:100%;height:1000px;display: none;">
{ "copiesArrays": true,
  "copiesArrayObjects": true,
  "nodeDataArray": <?= json_encode($nodes) ?>,
  "linkDataArray": <?= json_encode($linkDataArray) ?>
}
</textarea></div>

<script>
    $(document).ready(function() {
        init();
    });
</script>

<br>

<table>
  <tr>
    <td width="150">Nama Aplikasi</td>
    <td><input type="text" name="nama" value="<?=$workflow['rule']['nama']?>" size="50"></td>
  </tr>
  <tr>
    <td>Tenggat waktu (days)</td>
    <td><input type="text" name="tenggat_hari" value="<?=$workflow['rule']['tenggat_hari']?$workflow['rule']['tenggat_hari']:0?>" size="5"></td>
  </tr>
  <tr>
    <td>Laporan</td>
    <td><select name="file_pdf">
<option value=""></option>
<?php foreach ($list_pdf as $pdf) { ?>
  <option value="<?= $pdf['id_pdf'] ?>" <?php if ($workflow['rule']['file_pdf'] == $pdf['id_pdf']) echo 'selected' ?>><?= $pdf['nama']  ?></option>
<?php } ?>
</select></td>
  </tr>
</table>

<hr>

<table>
<tr>
<?php

$links = array();
foreach ($nodes as $node) {
    $temp = explode('_', $node['key']);
    $i_node = $temp[1];

    $links['node_' . $i_node] = 'node_' . $i_node;
}

$approve = array();
foreach ($linkDataArray as $link) {
    $temp = explode('_', $link['from']);
    $i_from = $temp[1];
    $temp = explode('_', $link['to']);
    $i_to = $temp[1];

    if ($i_from < $i_to)
        $approve[$link['from']] = $link['to'];
}

$refuse = array();
foreach ($linkDataArray as $link) {
    $temp = explode('_', $link['from']);
    $i_from = $temp[1];
    $temp = explode('_', $link['to']);
    $i_to = $temp[1];

    if ($i_from > $i_to)
        $refuse[$link['from']] = $link['to'];
}

$i=0;
foreach ($nodes as $node) {
    $temp = explode('_', $node['key']);
    $i_node = $temp[1];

    $i++;
?>
<td>
<div class="thumb-configure">
<div style="text-align:right;padding:5px"><label class="label label-primary"><?= $node['key'] ?></label></div>
<table>
<tr><td>Nama</td><td><input type="text" name="<?= $node['key'] ?>" value="<?= $node['text2'] ?>" size="30"></td></tr>
<tr><td>Actor</td><td><select name="actor_<?= $i_node ?>" style="width:200px">
<option value=""></option>
<?php foreach ($actors as $k => $v) { ?>
    <option value="<?= $k ?>" <?php if ($node['actor'] == $k) echo 'selected' ?>><?= $v ?></option>
<?php } ?>
</select></td></tr>
<tr><td>Keterangan</td><td><textarea name="keterangan_<?= $i_node ?>" style="width:200px;height:50px"><?= $node['keterangan'] ?></textarea></td></tr>
<tr><td>Approved</td><td><select name="approve_<?= $i_node ?>" style="width:200px">
<option value=""></option>
<?php foreach ($links as $k => $v) { ?>
    <option value="<?= $k ?>" <?php if ($approve['node_' . $i_node] == $k) echo 'selected' ?>><?= $v  ?></option>
<?php } ?>
</select></td></tr>
<tr><td>Refused</td><td><select name="refuse_<?= $i_node ?>">
<option value=""></option>
<?php foreach ($links as $k => $v) { ?>
    <option value="<?= $k ?>" <?php if ($refuse['node_' . $i_node] == $k) echo 'selected' ?>><?= $v  ?></option>
<?php } ?>
</select></td></tr>
<tr><td>Form</td><td><select name="form_<?= $i_node ?>" style="width:200px">
<option value=""></option>
<?php foreach ($list_form as $form) { ?>
  <option value="<?= $form['id_form'] ?>" <?php if ($node['form'] == $form['id_form']) echo 'selected' ?>><?= $form['nama']  ?></option>
<?php } ?>
</select></td></tr>
</table>
</div>
<?php if ($i%4==0) echo '</td></tr><tr></td>' ?>

<?php
}

?>
</td>
</tr>
</table>


<button id="SaveButton" onclick="save()" class="btn btn-success"><small class="glyphicon glyphicon-floppy-disk"></small> Simpan</button>

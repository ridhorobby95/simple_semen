<script type="text/javascript">
    var related_terpilih = [];
</script>

<div class="col-md-4" style="background-color:#FFFFFF; padding-right: 0%;">
        <div class="tabs tabsEdit" >
    		<ul class="tab-links">
        		<li class="active" ><a href="#tab1">Detail</a></li>
        		<li ><a href="#tab2">Activity</a></li>
                <li ><a href="#tab3">History</a></li>
        	</ul>
	        <div class="tab-content" style="margin: 10px; padding: 0px;">
		        <div id="tab1" class="tab active">
                    <img id="image_file" src="<?php echo site_url("assets/web/images/spritemap1.png");?>" class="detailView" style="float:left; max-width:25% !important;margin: 20px 0px 20px 20px; ">
                    
                    <!-- <a onClick="View()" class="mousedrag2 NoDetailLihat" style=""><img src="<?php// echo site_url("/assets/web/images/icon/Group293.png");?>" style="margin: 0px 10px 0px -5px;">Lihat Doc
                    </a> -->
                    <a id="activeLihat" onClick="View()" class="mousedrag2 detailLihat" style="display:none;margin:10px 0px 0px 20px;"><i class="fa fa-eye" aria-hidden="true"></i>
                    <!-- <img src="<?php echo site_url("/assets/web/images/icon/Group291.png");?>" style="margin: 0px 10px 0px -5px;"> --> View Document 
                    </a>
                    <a id="nonActiveLihat" class="mousedrag2 NoDetailLihat" style="margin:15px 0px 0px 20px;display:inline;cursor: not-allowed;" disabled><i class="fa fa-eye" aria-hidden="true"></i><!-- <img src="<?php echo site_url("/assets/web/images/icon/Group293.png");?>" style="margin: 0px 10px 0px -5px;"> --> View Document 
                    </a>
                    <?php if (SessionManagerWeb::isDocumentController() || SessionManagerWeb::isAdministrator()): ?>
                    <a id="activeReconvert" onClick="reconvert()" class="mousedrag2 detailLihatrcv" style="margin:15px 0px 0px 20px;display:none;"><i class="fa fa-refresh" aria-hidden="true"></i>
                    <!-- <img src="<?php echo site_url("/assets/web/images/icon/Group291.png");?>" style="margin: 0px 10px 0px -5px;"> --> Reconvert 
                    </a>
                    <a id="nonActiveReconvert" class="mousedrag2 NoDetailLihatrcv" style="margin:15px 0px 0px 20px;display:inline;cursor: not-allowed;" disabled><i class="fa fa-refresh" aria-hidden="true"></i><!-- <img src="<?php echo site_url("/assets/web/images/icon/Group293.png");?>" style="margin: 0px 10px 0px -5px;"> --> Reconvert 
                    </a>    
                    <?php endif ?>
                    
                    <br>

               		<!-- <h3>Detail Dokumen</h3>
                	<hr> -->
                	<!-- <table id="tDetail" width="100%" style="table-layout: fixed;"> -->
                    <!-- <tr> -->
                        <!-- <td colspan="2" style="padding-left: 0px"> -->
                        <!-- <div id="parentButton" style="float:right;">
                            <button id="viewBtn" class="command btn btn-inverse btn-success" onClick="View()" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                            <button class="command btn btn-primary" onClick="Edit()" data-toggle="tooltip" title="Upload New Version"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                            <button class="command btn btn-info" onClick="Download()" data-toggle="tooltip" title="Download"><i class="fa fa-download" aria-hidden="true"></i></button>
                            <button class="command btn btn-warning" onClick="Revision()" data-toggle="tooltip" title="Manage Version" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-recycle" aria-hidden="true"></i></button>
							<button class="command btn btn-danger" onClick="openShareBox()" data-toggle="tooltip" title="Share" data-backdrop="static" data-keyboard="false"><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                            <button class="command btn btn-danger" onClick="openDeleteBox()" data-toggle="tooltip" title="Delete" data-backdrop="static" data-keyboard="false"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </div> -->
                        <!-- </td> -->
                        <!-- <label id="dokumen" style="display:none;">0</label> -->
                    <!-- </tr> -->
                    
                    <table id="tDetail" width="100%" style="table-layout: fixed;">
                        <tr>
                            <td colspan="4" style="padding-top:5px !important;padding-bottom:10px !important;">
                                <div id="parentButton" style="float:left;">
                                    <button id="activeAskDownload" class="tombolDowload" onClick="show_ask_download()" data-toggle="tooltip" title="Ask Download Document" style='display:none;'>
                                        <i class="fa fa-download" aria-hidden="true" style="color: #56baed;font-size: 15px;margin-right: 10px;"></i>
                                        <span style="font-weight: bold;color: #56baed;font-size: 14px;"> Ask Download</span>
                                    </button>
                                    <button id="nonActiveAskDownload" class="NoTombolDowload" data-toggle="tooltip" title="Ask Download Document" style="display:inline;cursor: not-allowed;" disabled>
                                        <i class="fa fa-download" aria-hidden="true" style="color: #e0e0e0;font-size: 15px;margin-right: 10px;"></i>
                                        <span style="font-weight: bold;color: #E0E0E0;font-size: 14px;"> Ask Download</span>
                                    </button>
                                    <button id="activeDownload" class="tombolDowload hidden" onclick="" data-toggle="tooltip" title="Download Document" style='display:none;'>
                                        <i class="fa fa-download" aria-hidden="true" style="color: #56baed;font-size: 15px;margin-right: 10px;"></i>
                                        <span style="font-weight: bold;color: #56baed;font-size: 14px;"> Download</span>
                                    </button>
                                    <button id="nonActiveDownload" class="NoTombolDowload hidden" data-toggle="tooltip" title="Download Document" style="display:inline;cursor: not-allowed;" disabled>
                                        <i class="fa fa-download" aria-hidden="true" style="color: #e0e0e0;font-size: 15px;margin-right: 10px;"></i>
                                        <span style="font-weight: bold;color: #E0E0E0;font-size: 14px;"> Download</span>
                                    </button>
                                    <!-- <button id="activeReconvert" class="tombolReconvert" onClick="reconvert()" data-toggle="tooltip" title="Ask Download Document" style='display:none;'>
                                        <i class="fa fa-refresh" aria-hidden="true" style="color: #56baed;font-size: 15px;margin-right: 10px;"></i>
                                        <span style="font-weight: bold;color: #56baed;font-size: 14px;"> Reconvert</span>
                                    </button>
                                    <button id="nonActiveReconvert" class="NoTombolReconvert" data-toggle="tooltip" title="Ask Download Document" style="display:inline;cursor: not-allowed;" disabled>
                                        <i class="fa fa-refresh" aria-hidden="true" style="color: #e0e0e0;font-size: 15px;margin-right: 10px;"></i>
                                        <span style="font-weight: bold;color: #E0E0E0;font-size: 14px;"> Reconvert</span>
                                    </button> -->
                                    <?php if (1==0) {?>
                                    <button id='activeEdit' class="tombolDowload cIMG1" onClick="Edit()" data-toggle="tooltip" title="Ganti ke Dokumen Baru" style="display:none;">
                                        <i class="fa fa-pencil" aria-hidden="true" style="margin-left: -5px;margin-right: 5px;font-size: 17px;color: #56baed;"></i>
                                        <span style="font-weight: bold;color: #56baed; font-size: 14px; margin-left: 5px;">Revise</span>
                                    </button>
                                    <button id='nonActiveEdit' class="NoTombolDowload cIMG1" data-toggle="tooltip" title="Ganti ke Dokumen Baru" style="display:inline;cursor: not-allowed;" disabled>
                                        <i class="fa fa-pencil" aria-hidden="true" style="margin-left: -5px;margin-right: 5px;font-size: 17px;color: #e0e0e0;"></i>
                                        <span style="font-weight: bold;color: #E0E0E0; font-size: 14px; margin-left: 5px;">Revise</span>
                                    </button>
                                    <br>
                                    <?php } ?>
                                    <button id='activeEvaluate' class="tombolDowload cIMG1" onClick="Evaluate()" data-toggle="tooltip" title="Evaluate this document" style="display:none;">
                                        <i class="fa fa-search" aria-hidden="true" style="margin-left: -5px;margin-right: 5px;font-size: 17px;color: #56baed;"></i>
                                        <span id="evaluateWord" style="font-weight: bold;color: #56baed; font-size: 14px; margin-left: 5px;">Evaluate</span>
                                    </button>
                                    <button id='nonactiveEvaluate' class="NoTombolDowload cIMG1" data-toggle="tooltip" title="Evaluate this document" style="display:inline;cursor: not-allowed;" disabled>
                                        <i class="fa fa-search" aria-hidden="true" style="margin-left: -5px;margin-right: 5px;font-size: 17px;color: #e0e0e0;"></i>
                                        <span style="font-weight: bold;color: #E0E0E0; font-size: 14px; margin-left: 5px;">Evaluate</span>
                                    </button>
                                    <br>
                                    <?php if (1==0) { ?>
                                    <button id='activeRevision' class="btn tombolDowload" onClick="Revision()" data-toggle="tooltip" title="Kelola Versi Dokumen" data-target="#myModal" data-backdrop="static" data-keyboard="false" style='display:none;'>
                                        <i class="fa fa-cogs" aria-hidden="true" style="margin-right: 10px; margin-left: -15px;color: #56baed;font-size: 17px;"></i>
                                       <span style="font-weight: bold;color: #56baed;font-size: 14px;">Manage</span>
                                    </button>
                                    <button id='nonActiveRevision' class="btn NoTombolDowload" data-toggle="tooltip" title="Kelola Versi Dokumen" data-target="#myModal" data-backdrop="static" data-keyboard="false" style="display:inline;cursor: not-allowed;" disabled>
                                        <i class="fa fa-cogs" aria-hidden="true" style="margin-right: 10px; margin-left: -15px;color: #e0e0e0;font-size: 17px;"></i>
                                       <span style="font-weight: bold;color: #e0e0e0;font-size: 14px;">Manage</span>
                                    </button>
                                    <?php } ?>
                                    <?php if (1==0) {?>
                                        <button id='activeShare' class="btn tombolDowload" onClick="openShareBox()" data-toggle="tooltip" title="Bandingkan Dokumen" data-backdrop="static" data-keyboard="false" style='display:none;'>
                                            <i class="fa fa-compress" aria-hidden="true" style="margin-right: 5px;margin-left: -5px;font-size: 15px;color: #56baed;"></i>
                                            <span style="font-weight: bold;margin:0px 0px 0px 5px;color: #56baed;font-size: 14px;">Share</span>
                                        </button>
                                        <button id='nonActiveShare' class="btn NoTombolDowload" data-toggle="tooltip" title="Bandingkan Dokumen" data-backdrop="static" data-keyboard="false" style="display:inline;cursor: not-allowed;" disabled>
                                            <i class="fa fa-compress" aria-hidden="true" style="margin-right: 5px;margin-left: -5px;font-size: 15px;color: #e0e0e0;"></i>
                                            <span style="font-weight: bold;margin:0px 0px 0px 5px;color: #e0e0e0;font-size: 14px;">Share</span>
                                        </button>
                                    <?php } ?>
                                    <?php 
                                    // if (SessionManagerWeb::isDocumentController() || SessionManagerWeb::isAdministrator()){
                                    if (SessionManagerWeb::isDocumentController()){

                                    ?>
                                        <button id='activeLastEvaluation' class="btn tombolDowload" onClick="openLastEvaluation()" data-toggle="tooltip" title="Last Evaluation" data-backdrop="static" data-keyboard="false" style='display:none;'>
                                            <i class="fa fa-compress" aria-hidden="true" style="margin-right: 5px;margin-left: -5px;font-size: 15px;color: #56baed;"></i>
                                            <span style="font-weight: bold;margin:0px 0px 0px 5px;color: #56baed;font-size: 14px;">Last Evaluation</span>
                                        </button>
                                        <button id='nonActiveLastEvaluation' class="btn NoTombolDowload" data-toggle="tooltip" title="Last Evaluation" data-backdrop="static" data-keyboard="false" style="display:inline;cursor: not-allowed;" disabled>
                                            <i class="fa fa-compress" aria-hidden="true" style="margin-right: 5px;margin-left: -5px;font-size: 15px;color: #e0e0e0;"></i>
                                            <span style="font-weight: bold;margin:0px 0px 0px 5px;color: #e0e0e0;font-size: 14px;">Last Evaluation</span>
                                        </button>
                                        <button id='activeChangeUnit' class="btn tombolDowload" onClick="changeUnit()" data-toggle="tooltip" title="Change Workunit" data-backdrop="static" data-keyboard="false" style='display:none;'>
                                            <i class="fa fa-arrows-alt" aria-hidden="true" style="margin-right: 5px;margin-left: -5px;font-size: 15px;color: #56baed;"></i>
                                            <span style="font-weight: bold;margin:0px 0px 0px 5px;color: #56baed;font-size: 14px;">Change Unit</span>
                                        </button>
                                        <button id='nonActiveChangeUnit' class="btn NoTombolDowload" data-toggle="tooltip" title="Change Workunit" data-backdrop="static" data-keyboard="false" style="display:none;cursor: not-allowed;" disabled>
                                            <i class="fa fa-arrows-alt" aria-hidden="true" style="margin-right: 5px;margin-left: -5px;font-size: 15px;color: #e0e0e0;"></i>
                                            <span style="font-weight: bold;margin:0px 0px 0px 5px;color: #e0e0e0;font-size: 14px;">Change Unit</span>
                                        </button>
                                    <?php } ?>
                                </div>
                            </td>

                            <label id="revisi" style="display:none;">0</label>
                        </tr>

                        <td id="OtherDetail" colspan=3>
                            <h7 style="float:left; margin-right: 16px; font-size: 20px; font-weight: bold; color: #424242;">Information</h7>
                                <a href="#" id='editBtn1' onClick="prepareEdit()" style="font-size: 14px;background-color::#2ecc71;display:none">Edit
                            </a>
                            <i class="fa fa-pencil inform-btn hidden" aria-hidden="true"></i>      
                        </td>
                    <tr>
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Title</td>
                        <td colspan=2><label id="judul_file" style="overflow:hidden;max-width:180px;font-size:12px;">     <?php if($viewdoc){echo $document[0]['title'];}else{echo "-";} ?></label>
                             <input type="text" id="judul2" style="display:none;"></td>
                    </tr>
                    <tr>
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Code</td>
                        <td colspan=2><label id="document_code" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['code'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr>
                        <td style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Type</td>
                        <td>
                            <label id="jenis_file" style="font-size:12px;"><?php if($viewdoc){echo $document[0]['type_name'];}else{echo "-";} ?></label>
                            <div id="divJenis" style="display:none"/>
                            <select id="jenis" name="jenis" class="form-control input-sm select2" style="width:100%;">
                            <option></option>
                            <?php
                                foreach($jenis as $obj){
                                    echo "<option value='$obj'>".$obj."</option>";
                                }
                            ?>
                            </select>
                            </div>
                            <!-- <button id="klasifikasibtn" class="dropdown-btns" type="button" style="display:none;"><span class="caret"></span></button> -->
                        </td>
                    </tr>
                    <tr>
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Revision</td>
                        <td colspan=2><label id="document_revision" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['code'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr>
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;" id="workunit_title">Workunit</td>
                        <td colspan=2><label id="document_workunit" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['unitkerja_name'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr id="drafter_line">
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Drafter</td>
                        <td colspan=2><label id="drafter_name" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['user_name'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr id="creator_line">
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Creator</td>
                        <td colspan=2><label id="creator_name" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['creator_name'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr id="approver_line">
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Approver</td>
                        <td colspan=2><label id="approver_name" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['approver_name'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr id="businessprocess_line">
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Business Process</td>
                        <td colspan=2><label id="document_businessprocess" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['prosesbisnis_name'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr id="requirements_line">
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Reqs.</td>
                        <td colspan=2><label id="document_requirements" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['requirements'];}else{echo "-";} ?></label></td>
                    </tr>
                    <tr id="clausuls_line">
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Clausuls</td>
                        <td colspan=2>
                                <label id="document_clausuls" style="overflow:hidden;max-width:180px;font-size:12px;">
                                    <?php if($viewdoc){echo $document[0]['clausul'];}else{echo "-";} ?>
                                    
                                </label>
                        </td>
                    </tr>
                    <tr id="record_line">
                        <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Record Retension Period</td>
                        <td colspan=2><label id="document_retension" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['retension'].' month';}else{echo "-";} ?></label></td>
                    </tr>
                    <?php if (1==0) { ?>
                        <tr>
                            <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Filename</td>
                            <td colspan=2><label id="nama_file" style="overflow:hidden;max-width:180px;font-size:12px;"><?php if($viewdoc){echo $document[0]['namafile'];}else{echo "-";} ?></label></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Size</td>
                        <td id="ukuran_file" colspan="2" style="font-size:12px;"><label >
                            <?php
                                if($viewdoc){
                                    $ukuran = $document[0]['ukuran'];
                                    $satuan = " bytes";
                                    $temp = 0;
                                    while($ukuran>=1000){
                                        $ukuran /= 1000;
                                        $temp++;
                                    }
                                    if($temp==1)
                                        $satuan = " KB";
                                    else if($temp==2)
                                        $satuan = " MB";
                                    else if($temp==3)
                                        $satuan = " GB";
                                    $ukuran = (int)$ukuran;
                                    $satuan = $ukuran . $satuan;
                                    echo $satuan;
                                }else{
                                    echo "-";
                                }
                            ?>
                        </label></td>
                    </tr>
                    <tr>
                        <td style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Change Log</td>
                        <td>
                        	<label id="deskripsi_file" style="display:inline;font-size:12px;">
                                <?php 
                                    if($viewdoc){
                                        echo $document[0]['description'];
                                    }else{
                                        echo "-";
                                    } ?>
                                        
                                    </label>
                        	<textarea type="text" id="deskripsi" style="display:none;"></textarea>
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td style="padding-left: 0px">Lokasi</td>
                        <td>
                        	<label id="lokasi_file" style="font-size:12px;"><?php if($viewdoc){echo $document[0]['kode_lokasi'];}else{echo "-";} ?></label>
                        	<div id="divLokasi" style="display:none"/>
                        	<select id="lokasi" name="lokasi" class="icon-edit form-control input-sm select2" style="width:100%;">
                            <option></option>
                            <?php
                                foreach($lokasi as $obj){
                                    echo "<option value='$obj'>".$obj."</option>";
                                }
                            ?>
                            </select>
                            </div>
                        </td>
                    </tr>
                    <?php $display="none"; ?>
                    <tr style="display:<?= $display ?>">
                        <td style="padding-left: 0px">Klasifikasi</td>
                        <td>
                        	<label id="klasifikasi_file" style="font-size:12px;"><?php if($viewdoc){echo $document[0]['nama_klasifikasi'];}else{echo "-";} ?></label>
                        	<div id="divKlasifikasi" style="display:none"/>
                            <select id="klasifikasi" name="klasifikasi" class="form-control input-sm select2" style="width:100%;">
                            <option></option>
                            <?php
                                foreach($klasifikasi as $obj){
                                    echo "<option value='$obj'>".$obj."</option>";
                                }
                            ?>
                            </select>
                            </div>
                        	<!-- <button id="klasifikasibtn" class="dropdown-btns" type="button" style="display:none;"><span class="caret"></span></button> -->
                        </td>
                    </tr>
                    <tr>
                        <tr style="display:none">
                            <td width="45%" style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Tags</td>
                            <td colspan=2>
                                <label id="tags_file" style="font-size:12px; white-space: pre-wrap;">-</label>
                                <div id="divTags" style="display:none;">
                                    <select id="tags" name="tags" class="form-control input-sm select2 tag" multiple="multiple" style="width:100%;">
                                    <?php foreach($tags as $obj){
                                            echo "<option value='".$obj['name']."'>".$obj['name']."</option>";
                                    }?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <td colspan=3>
                            <h7 style="float:left; margin-right: 16px; font-size: 20px; font-weight: bold; color: #424242;">Reference</h7>
                            <a id="editBtn2" href="#" onClick="openRelatedModal()" style="display:none">Edit
                            </a>      
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Related Document</td>
                        <td>
                            <div id="listDokumenTerkaitDetail">
                                <?php 
                                if($viewdoc){
                                    foreach($document[0]['docRelated'] as $obj){
                                        echo "<li><a href='" . site_url("web/document/viewdoc/") . "/" . $obj['IDDOCUMENT'];
                                        echo "'>" . $obj['title'] . "</a>";
                                        echo "</li>";
                                    //    $temp++;
                                    }
                                }else{
                                    echo "-";
                                }?>
                            </div>
                        </td>
                        <td style="display: none">
                        </td>
                    </tr>
                    <tr id='showListDokumenFormTerkaitDetail' class=''>
                        <td style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Related Form</td>
                        <td>
                            <div id="listDokumenFormTerkaitDetail">
                                <?php 
                                if($viewdoc){
                                    foreach($document[0]['docRelated'] as $obj){
                                        echo "<li><a href='" . site_url("web/document/viewdoc/") . "/" . $obj['IDDOCUMENT'];
                                        echo "'>" . $obj['title'] . "</a>";
                                        echo "</li>";
                                    //    $temp++;
                                    }
                                }else{
                                    echo "-";
                                }?>
                            </div>
                        </td>
                        <td style="display: none">
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Modified</td>
                        <td id="waktu_update" colspan="2" style="font-size:12px;"><?php if($viewdoc){echo $document[0]['updated_at'];}else{echo "-";} ?></td>
                    </tr>
                    <tr>
                        <td style="color:#757575; letter-spacing: 0.7px;font-weight: bold;">Created</td>
                        <td id="waktu_create" colspan="2" style="font-size:12px;"><?php if($viewdoc){echo $document[0]['created_at'];}else{echo "-";} ?></td>
                    </tr>
                    <tr style="display: none">
                        <td colspan = 3>
                          <a id="activeQR" href="#" onClick="showQRCode()" class="BtnQRCode" style="display:none;">Tampilkan Kode QR </a>
                          <a id="nonActiveQR" href="#" class="NoBtnQRCode" style="display:inline;">Tampilkan Kode QR </a>
                        </td>

                    </tr>
                    <!-- <tr>
                    	<td colspan = "3" style="padding-left: 0px">QR Code</td>
                    </tr>
                    <tr>
                        <td colspan = 3 align="center"><img id="qrcode" src="<?php // if($viewdoc){echo "https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=" . $document[0]['hash'] . "&choe=UTF-8";} ?>" title="" /></td>
                    </tr> -->
                </table>
	            </div>
	            <div id="tab2" class="tab">
	                <h3>Activity</h3>
	                <hr>
	                <table id="tAktifitas" width="100%" style="font-size:12px;">
					</table>
	            </div>

                <div id="tab3" class="tab">
                    <h3>History</h3>
                    <hr>
                    <table id="tHistory" width="100%" style="font-size:12px;">
                    </table>
                </div>
			</div>
            <br>
		</div>
</div>
<style>
#tDetail td{
  font-size:12px;
}
#parentButton button{
    margin-left:3px;
    margin-bottom:5px;
    padding:0px;
}
#listDokumenTerkaitDetail li{
    margin-bottom:5px;
    list-style-type: none;
    font-weight: bold;
}
#listRelatedDocument li:last-child {
  margin-bottom:0px;
}
</style>
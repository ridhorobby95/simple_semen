<?php if ($task['urutan']==1) {  ?>
	<div class="row" style="margin-bottom: 1%;margin-left:0.5%">
		<div class="col-md-11 shadowPanel" style="background-color: #fff;border-radius: 5px; width: 98%;">
            <div class="col col-xs-8" style="margin: 2% 0% 0% 0%;padding: 0%;">
				<b><span class="label label-primary" ><?= $task_urutan ?></span> <?= $task['name'] ?></b>
            </div>

			<div class="col-md-6" style="width:50%;height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;    margin-bottom: 3%;">
				<div class="row" style="margin-left:5px">
					<a id="node_<?= $task['urutan'] ?>" style="display:block;top:-80px;position:relative"></a>
					<!-- Detail Requestor -->
					<table class="table" >
						<!-- START : Requestor Detail -->
						<tr>
							<td colspan=2 style="padding:0px">
								<i class='fa fa-user' aria-hidden='true' style='font-size: 27px; color:#03A9F4; float:middle; float: left;margin: 2%;'></i>
								<h3 style="font-family: Source Sans Pro, sans-serif;padding: 3px 0px 10px 0px;width: 70%; margin-top:7px;border-bottom: 1.5px solid #03A9F4;">Detail Pengaju</h3>
							</td>
						</tr>
						<tr>
							<td class="e-left-field designtext" width="150">Nama</td>
							<td><?= $user['name'] ?></td>
						</tr>

						<tr>
							<td class="e-left-field designtext" width="150">Unit Kerja</td>
							<td><?= $user['unitkerja_name'] ?></td>
						</tr>
						<tr>
							<td class="e-left-field designtext" width="150">Jabatan</td>
							<td><?= $user['subgroup_name'].' - '.$user['jabatan_name'] ?></td>
						</tr>
						<tr>
							<td class="e-left-field designtext">Email</td>
							<td><?= $user['email'] ?></td>
						</tr>
					</table>
				</div>
	        </div>

			<div class="col-md-6" style="width:50%;height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;    margin-bottom: 3%;">
				<div class="row" style="margin-left:5px">
					<a id="node_<?= $task_urutan ?>" style="display:block;top:-80px;position:relative"></a>
					<!-- Detail Requestor -->
					<table class="table" >
						<tr>
							<i class='fa fa-file' aria-hidden='true' style='font-size: 27px; color:#03A9F4; float:middle; float: left;margin: 2%;'></i>
							<h3 style="font-family: Source Sans Pro, sans-serif;padding: 3px 0px 10px 0px; width: 70%; margin-top:7px;border-bottom: 1.5px solid #03A9F4;">Detail Dokumen</h3>
						</tr>
				        <tr>
							<td class="e-left-field designtext">Judul</td>
							<td><?= $data['title'] ?></td>
						</tr>
				        <tr>
							<td class="e-left-field designtext">Nama File</td>
							<td><?= $data['filename'] ?></td>
						</tr>
				        <tr>
							<td class="e-left-field designtext">Ukuran Dokumen</td>
							<td><?= $data['ukuran'] ?></td>
						</tr>
				        <tr>
							<td class="e-left-field designtext">Catatan Perubahan Dokumen</td>
							<td><?= $data['description'] ?></td>
						</tr>
				        <tr>
							<td class="e-left-field designtext">Jenis Dokumen</td>
							<td><?= $data['type_name'] ?></td>
						</tr>
						<?php if ($data['reason']!=NULL) { ?>
							<tr>
								<td class="e-left-field designtext">Perubahan</td>
								<td><?= $data['reason'] ?></td>
							</tr>
						<?php } ?>
					</table>
				</div>
	        </div>
		</div>
	</div>
<?php } else { ?>
	<div class="row" style="margin-bottom: 1%;margin-left:0.5%">
		<div class="col-md-11 shadowPanel" style="background-color: #fff;border-radius: 5px; width: 98%;">
            <div class="col col-xs-8" style="margin: 2% 0% 0% 0%;padding: 0%;">
				<b><span class="label label-primary" ><?= $task_urutan ?></span> <?= $task['name'] ?></b>
            </div>
			<div class="col-md-6" style="width:50%;height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;    margin-bottom: 3%;">
				<div class="row" style="margin-left:5px">
					<a id="node_<?= $task['urutan'] ?>" style="display:block;top:-80px;position:relative"></a>
					<!-- Detail Requestor -->
					<table class="table" >
						<!-- START : Requestor Detail -->
							<tr>
							<td colspan=2 style="padding:0px"><h3 style="background-color:#ccc;padding:3px;margin-top:7px">Detail <?= $task['name'] ?></h3></td>
						</tr>
						<?php
						if ($task['form']=='R'){
							foreach ($data['review'] as $key => $review) {
						?>
							<tr>
								<td class="e-left-field" width="150">Reviewer</td>
								<td><?= $review['reviewer_name'] ?></td>
							</tr>
							<tr >
								<td class="e-left-field" width="150">Status</td>
								<td><?= $review['is_agree'] ? "<b style='color:green'>Setuju</b>" : "<b style='color:red'>Tidak Setuju</b>" ?></td>
							</tr>
							<tr>
								<td class="e-left-field" width="150">Komentar</td>
								<td><?= $review['review'] ?></td>
							</tr>
							<tr style="border-bottom:1px solid">
								<td class="e-left-field" width="150">Tanggal Review</td>
								<td><b><?= strftime('%d %b %Y %H:%M:%S', strtotime($review['created_at']))  ?></b></td>
							</tr>
							
							<?php } ?>
						<?php } else if ($task['form']=='A') { ?>
							<tr>
								<td class="e-left-field" width="150">Approver</td>
								<td><?= $task['actor_name'] ?></td>
							</tr>
							<tr>
								<td class="e-left-field" width="150">Komentar</td>
								<td><?= ($data['reason']) ? $data['reason'] : '-'; ?></td>
							</tr>
						<?php } else if ($task['form']=='V') { ?>
							<tr>
								<td class="e-left-field" width="150">Verifikator</td>
								<td><?= $task['actor_name'] ?></td>
							</tr>
							<tr>
								<td class="e-left-field" width="150">Komentar</td>
								<td><?= ($data['reason']) ? $data['reason'] : '-'; ?></td>
							</tr>
						<?php } ?>
					</table>
				</div>
	        </div>
		</div>
	</div>
<?php } ?>
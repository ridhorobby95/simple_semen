<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . 'update/' . $data['id'], array('id' => 'form_data')) ?>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Business Process</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'prosesbisnis', 'value' => $data['prosesbisnis'], 'class' => 'form-control input-sm', 'id' => 'prosesbisnis')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Business Process Description</label>
                            <div class="col-sm-9">
                                <?php echo form_textarea(array('name' => 'description', 'id' => 'description', 'value' => ($data['description'] ? $data['description'] : ''), 'class' => 'form-control input-sm input string', 'cols' => 30, 'rows' =>5)) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Business Process Code</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'code', 'value' => $data['code'], 'class' => 'form-control input-sm', 'id' => 'code')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 text-right">
                        <a class="post-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var selected_color = "<?= $data['color'] ? $data['color'] : 0 ?>";
    var selected_logo = "<?= $data['logo'] ? $data['logo'] : 0 ?>";
    var name = "<?= $data['name'] ?>";
    $(function() {
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }
</script>
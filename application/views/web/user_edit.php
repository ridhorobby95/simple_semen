<?php
    if($data['role'] != Role::EKSTERNAL){
        $readonly = isset($id) ? 'readonly' : '';
        $disabled = isset($id) ? 'disabled' : '';
    }
    
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-8">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Employee Number</label>
                            <div class="col-sm-6">
                                <?php 
                                $input = array('name' => 'karyawan_id', 'value' => $data['karyawan_id'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'karyawan_id', 'placeholder' => 'Fill the Employee Number');
                                if(isset($id)){
                                    $input['disabled'] = 'disabled';
                                }
                                echo form_input($input) ?>
                                <label class="hidden" style="font-size:10px;color:red" id="label_exist">
                                    <b style="color:red">*</b>Username with this email prefix (in front of "@") already exist
                                </label>
                                <label class="hidden" style="font-size:10px;color:green" id="label_available">
                                    <b style="color:green">*</b>Available
                                </label>
                                <label class="hidden" style="font-size:10px;color:orange" id="label_warning">
                                    <b style="color:orange">*</b>Warning! this employee have same username like the existed one.
                                </label>
                            </div>
                            <?php if (!isset($id)): ?>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-xs btn-success pull-right " id="check_button" onClick="getKaryawan()">
                                    Check
                                </button>
                            </div>  
                            <?php endif ?>
                           
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'name', 'value' => $data['name'], 'required' => true, 'readonly'=> true, 'class' => 'form-control input-sm', 'id' => 'name', 'pattern' => '.*\S+.*')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="email" class="col-sm-4">Email</label>
                            <div class="col-sm-8">
                                <input name="email" type="email" value="<?= $data['email'] ?>" pattern=".*\S+.*" required class="form-control input-sm" id="email" readonly>
                            </div>
                        </div>
                        <div class="row bord-bottom required">
                            <label class="col-sm-4">Username</label>
                            <div class="col-sm-8">
                                <input name="username" value="<?= $data['username'] ?>" required class="form-control input-sm" id="username" pattern=".*\S+.*" readonly>
                            </div>
                        </div>
                        <?php if (!isset($id)) { ?>
                            <div class="row bord-bottom required">
                                <label class="col-sm-4">Password</label>
                                <div class="col-sm-8">
                                    <input name="password" value="" placeholder="Fill the password" required class="form-control input-sm" id="password">
                                </div>
                            </div>
                        <?php } ?>
                        
                        <div class="row bord-bottom" id="row_role">
                            <label for="account number" class="col-sm-4">Role</label>
                            <div class="col-sm-8">
                                <?php echo form_dropdown('role', $a_role, $data['role'], 'id="role" class="form-control input-sm" onchange="getRole()" ') ?>
                            </div>
                        </div>
                        
                        <div class="row bord-bottom hidden" id="begindate">
                            <label for="tgl_masuk" class="col-sm-4">Begin Date</label>
                            <div class="col-sm-8">
                                <input type="text" data-provide="datepicker" name="tgl_masuk" id="tgl_masuk" value="<?= $data['tgl_masuk'] ?>" class="form-control input-md inputplus" placeholder="01-JAN-2018" <?= $readonly ?> autocomplete="off">
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="enddate">
                            <label for="tgl_pensiun" class="col-sm-4">End Date</label>
                            <div class="col-sm-8">
                                <input type="text" data-provide="datepicker" name="tgl_pensiun" id="tgl_pensiun" value="<?= $data['tgl_pensiun'] ?>" class="form-control input-md inputplus" placeholder="31-DES-9999" <?= $readonly ?> autocomplete="off">
                                <label style="font-size:10px">
                                    <b style="color:red">*</b> Keep blank if there's no End Date
                                </label>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="row_company_access">
                            <label for="account number" class="col-sm-4">Company Access</label>
                            <div class="col-sm-8">
                                <select id="company_access" name="company_access[]" class="form-control input-sm select2 company_access" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;">
                                    <option></option>
                                    <?php
                                        foreach($company as $key => $name){
                                            $value = in_array($key, $data['company_access']);
                                            $selected = '';
                                            if($value == 1){
                                                $selected ='selected';
                                            }
                                            echo "<option value='".$key."' $selected>".$name."</option>";
                                        }
                                    ?>
                               </select>
                            </div>

                        </div>
                        <!-- <?php echo "<pre>";print_r($data['document_type_access']);echo "</pre>" ?> -->
                        <div class="row bord-bottom hidden" id="row_type_access">
                            <label for="account number" class="col-sm-4">Document Type Access</label>
                            <div class="col-sm-8">
                                <select id="type_access" name="type_access[]" class="form-control input-sm select2 type_access" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;">
                                    <option></option>
                                    <?php
                                        foreach($document_types as $key){
                                            $value = in_array($key['id'], $data['document_type_access']);
                                            $selected = '';
                                            if($value == 1){
                                                $selected ='selected';
                                            }
                                            echo "<option value='".$key['id']."' $selected>".$key['type']."</option>";
                                        }
                                    ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center">
                        <div class="row">
                            <label class="col-sm-12">Profil Picture</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="site-photo site-photo-128 site-photo-center" id="img_user">
                                    <?php /*
                                    <img id="img_user" src="<?php //echo isset($data['photo']) ? site_url($path . 'thumb/watermark/' . $data['id'] . '/128') : $nopic ?>" data-toggle="tooltip" title="Upload Foto" />
                                    */?>
                                    <img src="<?php echo isset($data['photo']) ? site_url($data['photo']) : $nopic ?>" data-toggle="tooltip" title="Upload Foto" />
                                </div>
                                <label id="label_user" class="label label-info hidden">Save Profile for upload picture</label>
                                <?php echo form_upload(array('name' => 'photo', 'id' => 'photo', 'class' => 'hidden')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <a class="post-right"><button type="submit" id="save" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                    </div>
                    
                    </form>
                    <!-- <div class="col-md-12 text-left">
                        <button onclick="goChangePass()" class="btn btn-success btn-sm post-footer-btn">Change Password</button>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo form_open_multipart($path . $class . '/change_password', array('id' => 'form_modal')) ?>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ganti Password</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label class="col-md-4">Sekarang</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'oldPassword', 'class' => 'form-control', 'placeholder' => 'Password Sekarang')) ?>
                    </div>
                </div>
                <div class="vspace"></div>
                <div class="row">
                    <label class="col-md-4">Baru</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'newPassword', 'class' => 'form-control', 'placeholder' => 'Password Baru', 'id' => 'newPassword')) ?>
                    </div>
                </div>
                <div class="vspace"></div>
                <div class="row">
                    <label class="col-md-4">Ulangi</label>
                    <div class="col-md-8">
                        <?php echo form_password(array('name' => 'confirmPassword', 'class' => 'form-control', 'placeholder' => 'Ulangi Password Baru', 'id' => 'confirmPassword')) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="center">
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Ganti</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        var role = '<?php echo $data['role'] ?>';
        if(role == 'X'){
            $("#karyawan_id").attr('readonly', true);
            $("#check_button").addClass('hidden');
            $("#name").attr('readonly',false);
            $("#email").attr('readonly',false);
            $("#username").attr('readonly',false);
            $('#begindate, #enddate, #row_company_access, #row_type_access').removeClass('hidden');
            $("#row_role").addClass('hidden');

        }
    });
    

    $("#company_access").select2({
        placeholder: 'Choose company'
    });
    $("#type_access").select2({
        placeholder: 'Choose types'
    });
    $(function () {





        var dateToday = new Date();
        $("#tgl_masuk, #tgl_pensiun").datepicker({
            minDate: dateToday,
            format: 'dd-M-yyyy'
        });
        $("#tgl_masuk, #tgl_pensiun").keydown(function(e){
            e.preventDefault();
        });
        $("[name='rank_date'],[name='birth_date']").datepicker({format: 'dd-mm-yyyy'});
        $("#img_user").css("cursor", "pointer").click(function () {
            $("#photo").click();
        });
        $("#photo").change(function () {
            $("#label_user").removeClass("hidden");
        });
        $("#form_modal").submit(function () {
            // password baru dan konfirmasinya harus sama
            if ($("#newPassword").val() != $("#confirmPassword").val()) {
                alert("Password Baru dan pengulangannya tidak sama");
                return false;
            }
        });
        $("#daily_report_users_filter").keyup(function () {
            var str = $(this).val();

            $("#div_users tr:hidden").show();
            if (str != "") {
                $(".labelinput").each(function () {
                    if ($(this).html().toLowerCase().search(str.toLowerCase()) == -1)
                        $(this).parents("tr:eq(0)").hide();
                });
            }
        });
        $("#div_users [type='checkbox']").change(function () {
            value = $(this).attr("value");
            label = $("#div_selected [data-id='" + value + "']");
            check = $(this).prop("checked");
            if (check) {
                if (label.length == 0)
                    $("#div_selected").append('<div data-id="' + value + '">' + $("label[for='" + $(this).attr("id") + "']").text() + '</div>');
            } else {
                if (label.length != 0)
                    label.remove();
            }
        });
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }


    function getRole(){
        var role = document.getElementById("role").value;
        if(role == 'X'){
            $('#name').prop("readonly", false);
            $('#username').prop("readonly", false);
            $('#email').prop("readonly", false);
            $('#begindate, #enddate, #row_company_access, #row_type_access').removeClass('hidden');
            // $('#enddate').removeClass('hidden');
            $("#karyawan_id").prop("disabled",true);
            $("#check_button").addClass('hidden');
            $("#tgl_masuk, #tgl_pensiun").attr('required', "true");
        }
        else{
            $('#name').prop("readonly", true);
            $('#username').prop("readonly", true);
            $('#email').prop("readonly", true);
            <?php if (isset($id)) {
            } 
            else{
                ?>
                document.getElementById("name").value = null;
                document.getElementById("email").value = null;
                document.getElementById("username").value = null;
                $("#check_button").removeClass('hidden');
                $("#karyawan_id").prop("disabled",false);
                <?php
            }
            ?>
            $('#begindate, #enddate, #row_company_access, #row_type_access').addClass('hidden');
            $("#tgl_masuk, #tgl_pensiun").attr('required', "false");
            // $('#enddate').addClass('hidden');
            
            
        }
        // var id = document.getElementById("karyawan_id").value;
    }
    function getKaryawan(){
        var id = document.getElementById("karyawan_id").value;
        
        $.ajax({
            url : "<?php echo site_url('/web/karyawan/ajaxGetKaryawan');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'id':id},
            success:function(respon){
                if (respon=='0'){
                    $("#label_exist").addClass("hidden");
                    $("#label_available").addClass("hidden");
                    $("#label_warning").addClass("hidden");
                    document.getElementById("name").value = "Not Found!";
                    document.getElementById("email").value = "Not Found!";
                    document.getElementById("username").value = "Not Found!";
                    $("#save").prop("disabled", true);
                } else if (respon=='1'){
                    $("#label_exist").removeClass("hidden");
                    $("#label_available").addClass("hidden");
                    $("#label_warning").addClass("hidden");
                    document.getElementById("name").value = "Already Exist!";
                    document.getElementById("email").value = "Already Exist!";
                    document.getElementById("username").value = "Already Exist!";
                    $("#save").prop("disabled", true);

                } else {
                    $("#label_exist").addClass("hidden");
                    $("#label_available").removeClass("hidden");
                    $("#label_warning").addClass("hidden");
                    document.getElementById("name").value = respon.name;
                    document.getElementById("email").value = respon.email;
                    document.getElementById("username").value = respon.username;
                    $("#save").prop("disabled", false);
                }
            } 
        });
    }

</script>
<div class="alert alert-info">Klik foto untuk mengirimkan post kepada teman</div>
<?php
	$n = ceil(count($data)/2);
	for($i=0;$i<$n;$i++) {
?>
<div class="row">
	<?php
		for($j=0;$j<2;$j++) {
			$v = $data[($i*2)+$j];
			if(empty($v))
				continue;
	?>
	<div class="col-sm-6">
		<div class="post-section user-section">
			<div data-toggle="tooltip" title="Kirim pesan" data-placement="top" class="site-photo site-photo-100 site-photo-left" data-id="<?php echo $v['id'] ?>">
				<?php /*
				<img src="<?php echo isset($v['photo']) ? site_url($path.'thumb/watermark/'.$v['id'].'/100') : $nopic ?>" />
				*/?>
				<img src="<?php echo isset($v['photo']) ? $v['photo']['thumb']['link'] : $nopic ?>" />
			</div>
			<div class="user-label"><strong><?php echo $v['name'] ?></strong></div>
			<div class="site-label user-label"><i class="fa fa-envelope" data-toggle="tooltip" title="Email" data-placement="left"></i> <?php echo $v['email'] ?></div>
			<div class="site-label user-label"><i class="fa fa-comments" data-toggle="tooltip" title="YM" data-placement="left"></i> <?php echo $v['ym'] ?></div>
			<div class="site-label user-label"><i class="fa fa-phone-square" data-toggle="tooltip" title="No HP" data-placement="left"></i> <?php echo $v['phone'] ?></div>
			<div class="user-label"><i class="fa fa-sign-in" data-toggle="tooltip" title="Status Login" data-placement="left"></i>
				<?php if(empty($presence[$v['id']])) { ?>
				<span class="text-danger" style="margin-top: 20px;">Tidak Masuk</span>
				<?php } else { ?>
				<span class="text-success" style="margin-top: 20px;">Masuk</span>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<?php } ?>

<script type="text/javascript">

function goExport() {
    location.href = "<?php echo site_url($path . $class . '/add_import') ?>";
}

var back = "<?php echo base_url(uri_string()) ?>";
sessionStorage.setItem("post.back",back);

$(function() {
	$("[data-id]").css("cursor","pointer").click(function() {
		location.href = "<?php echo site_url($path.'post/add') ?>/" + $(this).attr("data-id");
	});
})

</script>
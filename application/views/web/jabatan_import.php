<script>
    $("#mainContent").addClass('col-sm-12');
    $("#mainContent").removeClass('col-sm-9');
</script>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <label style="margin-left: 5px;font:10px;">
                        <i>
                            *This page is one time view, these data will removed once you refresh or leave this page
                        </i>
                    </label>
                    <table style="table-layout:fixed;overflow:hidden;margin-left: 5%" align="center" width="1000" height="300">
                        <tr></tr>
                        <tr><td colspan="2"><b>Import Jabatan</b></td></tr>
                        <tr><td>Di Import Tanggal</td><td><?= date("d M Y") ?></td></tr>
                        <tr><td>Di Import Oleh</td><td colspan=3><?= SessionManagerWeb::getName() ?></td></tr>
                        <tr><td></td></tr><tr><tr></tr></tr>

                        <tr>
                            <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>FAILED IMPORT</td>
                        </tr>
                        <tr></tr>
                        <tr>
                            <td style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Position Code</td>
                            <td colspan='2' style='border: 1px solid black;background-color:red;font-weight:bold;max-height:300px;overflow:hidden'>Reason</td>
                        </tr>
                        <?php 
                            foreach ($failed as $fail) {
                                echo "<tr>
                                    <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
                                        {$fail['id']}
                                    </td>
                                    <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;'>
                                        {$fail['reason']}
                                    </td>
                                </tr>";
                            }
                        ?>
                        <tr><td></td></tr><tr><td></td> </tr>
                        <tr><td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;overflow:hidden'>SUCCESS IMPORT</td></tr>
                        <tr> </tr>
                        <tr>
                            <td style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Position Code</td>
                            <td colspan='2' style='border: 1px solid black;background-color:#AED581;font-weight:bold;max-height:300px;'>Status</td>
                        </tr>
                        <?php 
                            foreach ($success as $s) {
                                echo "<tr>
                                    <td style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
                                        {$s['id']}
                                    </td>
                                    <td colspan='2' style='border: 1px solid black;text-overflow: ellipsis;white-space: nowrap;max-height:300px;overflow:hidden'>
                                        {$s['reason']}
                                    </td>
                                </tr>";
                            }
                        ?>
                        <tr><td></td></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>

<script type="text/javascript">

    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goDownload() {
        location.href = "<?php echo site_url($path . $class) ?>/downloadImportReport";
    }

    function goSave() {
        $("#form_data").submit();
    }

</script>
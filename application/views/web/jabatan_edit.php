<?php
    $readonly = isset($id) ? 'readonly' : '';
    $disabled = isset($id) ? 'disabled' : '';
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-11">
                        <div class="row bord-bottom">
                            <label for="id" class="col-sm-4">Position Code</label>
                            <div class="col-sm-8">
                                <input name="id" placeholder="fill the position code" value="<?= $data['id'] ?>" required class="form-control input-sm" id="id" <?= $readonly ?>>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="subgroup_id" class="col-sm-4">Position Group</label>
                            <div class="col-sm-8">
                                <?php echo form_dropdown('subgroup_id',$groups, $data['subgroup_id'], 'id="subgroup_id" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required '.$disabled); ?>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden">
                            <label for="subgroup_name" class="col-sm-4">Echelon Name</label>
                            <div class="col-sm-8">
                                <input name="subgroup_name" value="<?= $data['subgroup_name'] ?>" class="form-control input-sm" id="subgroup_name" <?= $readonly ?>>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <input name="name" placeholder="Position Name" value="<?= $data['name'] ?>" required class="form-control input-sm" id="name" <?= $readonly ?>>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="is_chief" class="col-sm-4">Head Of Position ?</label>
                            <div class="col-sm-8">
                                <?php if (!isset($id)) { ?>
                                    <select name="is_chief" id="is_chief" class="form-control input-sm"> 
                                        <option value=''>No</option>
                                        <option value='X'>Yes</option>
                                    </select>
                                <?php } else { ?>
                                    <input name="is_chief" value="<?= $data['is_chief'] ?>" required class="form-control input-sm" id="is_chief" <?= $readonly ?>>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="unitkerja_id" class="col-sm-4">Work Unit Code</label>
                            <div class="col-sm-6">
                                <input name="unitkerja_id" value="<?= $data['unitkerja_id'] ?>" required class="form-control input-sm" id="unitkerja_id" <?= $readonly ?>>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getWorkunitName()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="div_unitkerja_name">
                            <label for="unitkerja_name" class="col-sm-4">Work Unit Name</label>
                            <div class="col-sm-8">
                                <input name="unitkerja_name" value="" required class="form-control input-sm" id="unitkerja_name" disabled>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="end_date" class="col-sm-4">End Date</label>
                            <div class="col-sm-8">
                                <input type="text" data-provide="datepicker" name="end_date" id="end_date" value="<?= $data['end_date'] ?>" class="form-control input-md inputplus" placeholder="31/12/9999" <?= $readonly ?>>
                                <label style="font-size:10px">
                                    <b style="color:red">*</b> Keep blank if there's no End Date
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-11 text-right">
                        <a class="post-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $.fn.datepicker.defaults.format = "dd/mm/yyyy";

    $(document).ready(function() {
        $("#subgroup_id").select2({
            placeholder: 'Choose Group of Position...',
            width: '100%'
        });
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function getWorkunitName(){        
        var unitkerja_id = document.getElementById("unitkerja_id").value;
        
        $.ajax({
            url : "<?php echo site_url('/web/unitkerja/ajaxGetUnitkerjaName');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'id':unitkerja_id},
            success:function(respon){
                $("#div_unitkerja_name").removeClass("hidden");
                if (respon==null || respon==false){
                    document.getElementById("unitkerja_name").value = "Not Found!";
                } else {
                    document.getElementById("unitkerja_name").value = respon;    
                }
                
            } 
        });
    }

</script>
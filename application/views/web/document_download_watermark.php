<style type="text/css">
    #rightSidebar {
        display: none!important;
    }
    #mainContent {
        margin-bottom: 50vh;
    }
</style>
<script>
	$("#mainContent").removeClass('col-sm-9');
	$("#mainContent").addClass('col-sm-12');
</script>

<!-- Variables -->
<div class="row" style="margin-left:5px">
	<div class="row" style="margin-bottom: 1%">
		<div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px; width: 96%; margin-left: 1.5%">
			<!-- Detail Requestor -->
			<table class="table" >
				<!-- START : Requestor Detail -->
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h3><b style="color:#333">We're Sorry,</b></h3></td>

				</tr>
				<?php if ($access == 1){ ?>
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h4><b style="color:#333">
The <b style="color:#4FC3F7"><?= $title ?></b> that you want to download is still in the process of <b style="color:red">converting</b></b></h4></td>
				</tr>
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h4 style="color:#333">
Please Check after 15 minute</h4></td>
				</tr>
				<?php } else {?>
<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h4><b style="color:#333">
The <b style="color:#4FC3F7"><?= $title ?></b> can't be downloaded because you haven't <b style="color:red">ask download</b> yet</b></h4></td>
				</tr>
				<tr>
					<td colspan=2 style="padding:0px;padding-top: 20px"><h4 style="color:#333">
Please Ask Download This Document</h4></td>
				</tr>
				<?php } ?>
				
				<tr class="hidden">
					<td colspan=2 style="padding:0px;padding-top: 20px">
						<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
						    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>View Document</span>
						</button>
					</td>
				</tr>
				
				
			</table>
		</div>
	</div>
</div>

<script>
	function goBack() {
        location.href = "<?php echo site_url($path . $class.'/dashboard') ?>/";
    }
</script>
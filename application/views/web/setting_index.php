<div class="row">
    <div class="col-md-12">
        <?php foreach ($data as $key => $value) { ?>
        <?php if($value['item']){ ?> 
        <h4><?= $value['title'] ?></h4>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($value['title']=='Helpdesk' and isset($value['button'])){
                        ?>
                            <a class="pull-right" href="<?php echo site_url($path.$value['button']['link']) ?>"><button class="btn btn-primary btn-xs post-footer-btn"><i class="fa fa-<?= $value['button']['logo'] ?>"></i> <?= $value['button']['name'] ?></button></a>
                            <br>
                            <br>
                        <?php
                        } ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <tbody>
                                    <?php foreach ($value['item'] as $v) { ?>
                                        <tr>
                                            <!-- <td class="text-right"><?= $no ?></td> -->
                                            <td class="text-left">
                                                <i class="fa fa-<?= $v['logo'] ?>"></i> &nbsp
                                                <?= $v['name'] ?>
                                            </td>
                                            <td class="text-center" style="text-align: right">
                                                <button type="button" class="btn btn-xs btn-success" onclick="javascript:test('<?= $v['link'] ?>')" data-toggle="tooltip" title="<?= $v['name'] ?> Setting"><i class="fa fa-gears"></i></button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
</div>
<script>
    function test(val) {
        location.href = "<?php echo site_url($path) ?>" + '/' + val;
    }
    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    function goAddEvent(id) {
        location.href = "<?php echo site_url($path . 'calendar/add') ?>" + '/' + id;
    }
</script>
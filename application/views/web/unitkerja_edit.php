<?php
    $readonly = '';
    if (isset($id)){
        $readonly ='readonly';
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create').'/'.$_SESSION['is_company'], array('id' => 'form_data')) ?>
                        <div class="col-sm-8">
                            <div class="row bord-bottom">
                                <label for="name" class="col-sm-4">Code<b style="color:red">*</b></label>
                                <div class="col-sm-8">
                                    <input name="id" value="<?= $data['id'] ?>" required class="form-control input-sm" id="id" <?= $readonly ?>>
                                </div>
                            </div>
                            <div class="row bord-bottom">
                                <label for="name" class="col-sm-4">Name<b style="color:red">*</b></label>
                                <div class="col-sm-8">
                                    <?php echo form_input(array('name' => 'name', 'value' => $data['name'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'name')) ?>
                                </div>
                            </div>
                            <div class="row bord-bottom">
                                <label for="initial" class="col-sm-4">Initial</label>
                                <div class="col-sm-8">
                                    <input name="initial" value="<?= $data['initial'] ?>" class="form-control input-sm" id="initial" >
                                </div>
                            </div>
                            <?php if ($_SESSION['is_company']==0) { ?>
                                <div class="row bord-bottom">
                                    <label for="level" class="col-sm-4">Level<b style="color:red">*</b></label>
                                    <div class="col-sm-8">
                                        <?php echo form_dropdown('level',$levels, $data['level'], 'id="level" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required'); ?>
                                    </div>
                                </div>

                                <div class="row bord-bottom">
                                    <label for="initial" class="col-sm-4">Company<b style="color:red">*</b></label>
                                    <div class="col-sm-8">
                                        <?php echo form_dropdown('company',$companies, $data['company'], 'id="company" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required onChange="getWorkunits()"'); ?>
                                    </div>
                                </div>

                                <div class="row bord-bottom">
                                    <label for="parent" class="col-sm-4">Upper Work Unit<b style="color:red">*</b></label>
                                    <div class="col-sm-8">
                                        <?php echo form_dropdown('parent',$workunits, $data['parent'], 'id="parent" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required <?= (isset($id) ? "":"disabled") ?>'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row bord-bottom">
                                <label for="end_date" class="col-sm-4">End Date</label>
                                <div class="col-sm-8">
                                    <input type="text" data-provide="datepicker" name="end_date" id="end_date" value="<?= $data['end_date'] ?>" class="form-control input-md inputplus" placeholder="31/12/9999">
                                    <label style="font-size:10px">
                                        <b style="color:red">*</b> Keep blank if there's no End Date
                                    </label>
                                </div>
                            </div>
                        </div>
                        <?php if ($data['level']=='COMP' || $_SESSION['is_company']==1){ ?>
                            <div class="col-sm-4 text-center">
                                <div class="row">
                                    <label class="col-sm-12">Company Logo (.png)</label>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="site-photo site-photo-128 site-photo-center" id="img_user">
                                            <img src="<?php echo ($data['photo']!=NULL) ? site_url($data['photo']) : site_url('assets/web/img/logo-simple.jpeg') ?>" data-toggle="tooltip" title="Upload Foto" />
                                        </div>
                                        <label id="label_user" class="label label-info hidden">Save Profile for upload picture</label>
                                        <?php echo form_upload(array('name' => 'photo', 'id' => 'photo', 'class' => 'hidden')) ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-12 text-right" style="margin-top: 10px">
                            <a class="post-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $.fn.datepicker.defaults.format = "dd/mm/yyyy";

    $(document).ready(function() {
        $("#company_id").select2({
            placeholder: 'Choose Company...',
            width: '100%'
        });

        $("#parent").select2({
            placeholder: 'Choose Upper Work Unit...',
            width: '100%'
        });

        $("#level").select2({
            placeholder: 'Choose Level...',
            width: '100%'
        });

        $("#company").select2({
            placeholder: 'Choose Company...',
            width: '100%'
        });
    });

    $(function () {
        $("#img_user").css("cursor", "pointer").click(function () {
            $("#photo").click();
        });
        $("#photo").change(function () {
            $("#label_user").removeClass("hidden");
        });
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack(company='') {
        location.href = "<?php echo site_url($path . $class) ?>/"+company;
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

    function getWorkunits(){        
        var company_id = document.getElementById("company").value;

        $("#parent").select2().html('');
        document.getElementById("parent").removeAttribute('disabled');
        $.ajax({
            url : "<?php echo site_url('/web/unitkerja/ajaxGetListUnitkerja');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'company_id':company_id},
            success:function(respon){
                json_resp = respon;
                // json_resp = JSON.parse(respon);
                
                $("#parent").select2({
                    placeholder : 'Choose Work unit...',
                    data:json_resp 
                });
                $('#parent').val(null).trigger('change');
            } 
        });
    }

</script>
<div id="NewUploadBox" class="modal fade" style="overflow-y:scroll;">
    <div class="modal-dialog2">
        <!-- <form method="post" name="kirim_form" id="kirim_form" action="<?//= site_url('web/document/kirim') ?>"> -->


        <div class="modal-content row col-sm-12" style="padding: 0px; background-color: transparent;">
            <div class="row" style="margin: 0px;">
                <div class="col-sm-12" style="background-color: #FFC107;border-top-left-radius: 10px;border-top-right-radius: 10px;">
                    <div class="col-md-10" style="">
                        <h5>
                            To <b>Create document here,</b> change the method below into <b>"Create Here"</b>
                        </h5>
                    </div>
                    <div class="col-md-2 text-right" style="">
                        <h4>
                            <i class="fa fa-close new-close" data-dismiss='modal' ></i>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="row col-md-12" id="alertFileExceeds" style="display: none;width:103%;">
                <div class="row" style="margin: 0px;">
                    <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                        <div class="col-md-10" style="">
                        <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                            <strong style="color:#b71c1c;">
                                Warning!
                            </strong>
                            <label id="alertMessageNewDoc" style="color:#b71c1c;">
                                Maximum file limit of 20MB Exceeded, please upload file less than 20MB!
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2 text-right" id="hide_alertFileExceeds" style="">
                                <h4>
                                    <i class="fa fa-close new-close" ></i>
                                </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-12" style="background-color: #F0FFFF;height: 15em;">
                        <div class="col-md-12" style="margin-top:10px;">
                            <div class="row">
                                <div class="col-sm-1" style="padding: 1% 2%;">
                                    <label class="col-md-3 control-label" for="select_upload" style="padding-left: 0px">Method</label>

                                </div>
                                <div class="col-sm-3" style="padding: 1% 2%;">
                                    <select id="select_upload" name="select_upload" class="form-control input-sm " onchange="hideUpload()" style="width:100%;">
                                        <option value="0">Upload Dokumen</option>
                                        <option value="1">Create Here</option>
                                    </select>
                                </div>
                                <div class="col-sm-8">
                                    <button type="button" class="btn btn-sm btn-green pull-right" data-toggle="modal" data-target="#template_modal" style="padding-left:14px;padding-right:13px;font-weight: 1px; margin-bottom:8px;font-size: 11px; letter-spacing: 1px; margin-top: 0px;font-weight:bold;">Download Template</button>
                                </div>
                                <div class="row col-md-12 hidden" id="alertChosenType">
                                    <div class="alert alert-info" style="width:100%;margin-bottom: 0px;background-color: #03A9F4;">
                                        <label style="color: #fff;">Please fill the document detail, then click <b>"Create Document"</b> to create the document</label>
                                    </div>
                                </div>
                            </div>

                            <form class="col-md-12 dropzone" action="<?php echo site_url('web/document/documentform'); ?>" align="center"  id="dropbox2" style="border: 5px dotted #03A9F4;padding: 7px 25%;height:10em;">
                                 <div class="col-sm-12" style="">
                                    <div id="containerDropbox2" style="display:inline;">
                                        <div class="col-sm-12 dz-message" data-dz-message style="margin: 20px;">
                                            <i class='fa fa-copy' aria-hidden='true' style='font-size: 50px; color:#03A9F4; float:middle;'></i>
        <!--                                     <img src="<?php echo site_url("assets/web/images/icon/Group255.png");?>" style="">  -->
                                            <h5 align="center">
                                                <b>
                                                    Click to Choose or Drop File Here
                                                </b>
                                            </h5>
                                            <div class="row">
                                                <div id="files_preview" name="files_preview" class="col-md-12" align="center"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="scanContainer2" style="display:none;">
                                        <img id="dwtcontrolContainer2" src="" style="margin-top:5px;">
                                    </div>
                                </div>
                            </form>

                            <form class="col-md-6 dropzone hidden" action="<?php echo site_url('web/document/documentformbm'); ?>" align="center"  id="dropboxexcel" style="border: 5px dotted #03A9F4;padding: 7px 5%;height:10em;">
                                 <div class="col-sm-12" style="">
                                    <div id="containerDropboxexcel" style="display:inline;">
                                        <div class="col-sm-12 dz-message" data-dz-message style="margin: 20px;">
                                            <i class='fa fa-copy' aria-hidden='true' style='font-size: 50px; color:#03A9F4; float:middle;'></i>
        <!--                                     <img src="<?php echo site_url("assets/web/images/icon/Group255.png");?>" style="">  -->
                                            <h5 align="center">
                                                <b>
                                                    Click to Choose or Drop File Here
                                                </b>
                                            </h5>
                                            <div class="row">
                                                <div id="files_previewexcel" name="files_previewexcel" class="col-md-12" align="center"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="scanContainer2" style="display:none;">
                                        <img id="dwtcontrolContainer2" src="" style="margin-top:5px;">
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="col-md-6 hidden" style="width:50%;height:100%;margin-top:10px;">
                            <div id="containerUploadButton" class="col-md-12" style="margin: 3%;">
                                    <span onClick="upload()" class="btn btn-upload1" style="margin: 10px;">Choose Document</span>
                                    <span id="btnUpload1" class="btn btn-upload2" style="margin: 10px;display: none">Scan Dokumen</span><!--onClick="openScan()"-->
                            </div>
                        </div>

                </div>
                    
                </div>
            </div>
        </div>
        <div class="row col-md-12" id="alertForNewDoc" style="display: inline;width:100%;padding: 0px;">
            <div class="row" style="margin: 0px;">
                <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                    <div class="col-md-10" style="">
                    <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                        <strong style="color:#b71c1c;">
                            Warning!
                        </strong>
                        <label id="alertMessageNewDoc" style="color:#b71c1c;">
                            You have already uploaded this document!
                        </label>
                    </div>
                    </div>
                    <div class="col-md-2 text-right" id="hide_alertForNewDoc" style="">
                            <h4>
                                <i class="fa fa-close new-close" ></i>
                            </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-12" id="alertExcelNotPDF" style="display: inline;width:100%;padding: 0px;">
            <div class="row" style="margin: 0px;">
                <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                    <div class="col-md-10" style="">
                    <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                        <strong style="color:#b71c1c;">
                            Warning!
                        </strong>
                        <label id="alertMessageExcelNotPDF" style="color:#b71c1c;">
                            You must upload a PDF File!
                        </label>
                    </div>
                    </div>
                    <div class="col-md-2 text-right" id="hide_alertExcelNotPDF" style="">
                            <h4>
                                <i class="fa fa-close new-close" ></i>
                            </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-content row col-sm-12" style="padding: 0px; background-color: transparent;">
            <form method="post" name="kirim_form" id="kirim_form" action="<?= site_url('web/document/kirim') ?>">
                <div class="row" style="margin: 0px; background-color: #fff;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                    <div class="col-sm-6" style="background-color: #fff;">
                        <tr>
                            <br>
                            <i class="fa fa-file" style="font-size: 20px;padding-left: 3%; color: #03A9F4;">
                                <h4 class="modal-title" style="display:inline; color:#757575;font-size:18px;font-weight:bold; color:#424242;">
                                    Detail Document
                                </h4>
                            </i>
                        </tr>
                        <tr>
                            <div class="modal-body row" style="margin-bottom: 0%;margin-top: 0px;padding-bottom: 0px;">
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="judul">Title* </label>
                                        <?php echo form_input(array('name' => 'judul', 'value' => '', 'class' => 'form-control fcontrolsize', 'id' => 'judul', 'placeholder' => 'Title','required' => ''))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="kodeAdministratif">Document Code</label>
                                        <div style="width: 60%; float: right;" >
                                            <?php echo form_input(array('name' => 'kodeAdministratif', 'value' => '', 'class' => 'form-control input-sm', 'id' => 'kodeAdministratif', 'placeholder' => 'Document Code' , 'readonly'=>TRUE))?>
                                            <?php echo form_input(array('name' => 'is_create_later', 'value' => '0', 'class' => 'form-control input-sm hidden', 'id' => 'is_create_later', 'placeholder' => 'Document Code' , 'readonly'=>TRUE))?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;"> 
                                        <label for="jenisIns" >Document Type*</label>
                                        <div style="width: 60%; float: right;">
                                            <select id="jenisIns" name="jenisIns" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required>
                                                <option></option>
                                                <?php
                                                    foreach($variables['jenis'] as $obj){
                                                        echo "<option value='".$obj['id']."'>".$obj['type']."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden" id="div_form_type" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;"> 
                                        <label for="form_type" >Form Type*</label>
                                        <div style="width: 60%; float: right;">
                                            <select id="form_type" name="form_type" placeholder="Choose Form Type" class="form-control input-sm select2 form_type" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" onChange="changeCode()">
                                                <option></option>
                                                <?php
                                                    foreach($variables['jenis'] as $obj){
                                                        if (strtolower($obj['type'])=='form'){
                                                            continue;
                                                        }
                                                        echo "<option value='".$obj['id']."'>".$obj['type']."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="myunitkerja" >Work Unit*</label>
                                        <div style="width: 60%; float: right;">
                                            <select id="myunitkerja" name="myunitkerja" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" disabled required>
                                                <option></option>
                                                <?php
                                                    foreach($variables['myunitkerja'] as $key => $obj){
                                                        echo "<option value='".$key."'>".$obj."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="prosesbisnis" >Business Process*</label>
                                        <div style="width: 60%; float: right;">
                                            <select id="prosesbisnis" name="prosesbisnis" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" onChange="changeCode()" required>
                                                <option></option>
                                                <?php
                                                    foreach($variables['prosesbisnis'] as $key => $obj){
                                                        echo "<option value='".$key."'>".$obj."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="input_iso" >Requirement*</label>
                                        <div style="width: 60%; float: right;">
                                            <?php
                                                // foreach($variables['document_iso'] as $key => $obj){
                                                //     echo "<input type='checkbox' name='input_iso[]' id='input_iso' onClick='javascript:getKlausul()' value='".$obj['id']."'>".$obj['iso']."<br>";
                                                // }
                                            ?>
                                            <?php if (1==1) {?>
                                            <select id="input_iso" name="input_iso[]" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" onChange="javascript:getKlausul()" required>
                                                <?php
                                                    foreach($variables['document_iso'] as $key => $obj){
                                                        echo "<option value=\"".$obj['id']."\"'>".$obj['iso']."</option>";
                                                    }
                                                ?>
                                           </select>
                                           <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group hidden" id="div_klausul" style="padding: 0px 20px;">
                                        <label for="input_iso_klausul" >Klausul</label>
                                        <div style="width: 60%;float: right;">
                                            <select id="input_iso_klausul[]" name="input_iso_klausul[]" class="form-control input-sm select2 input_iso_klausul" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;">
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">  
                                    <div class="form-group hidden" id="div_retension" style="padding: 0px 20px;">
                                        <label for="retension_input" >Retention Period*</label>
                                        <div style="width: 60%;float: right;">
                                            <?php echo form_input(array('name' => 'retension_input', 'value' => '', 'class' => 'form-control input-sm', 'id' => 'retension_input', 'placeholder' => 'Retention Period', 'style' => 'width:55%;display:inline' ))?>
                                            <select id="retension_select" name="retension_select" class="form-control input-sm" style="width:40%; font-size: 12px; letter-spacing: 0.5px;display:inline">
                                                <option value="0">Bulan</option>
                                                <option value="1">Tahun</option>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <?php if (1==0) { ?>
                                    <div class="form-group hidden" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="judul">Title </label>
                                        <?php echo form_input(array('name' => 'judul', 'value' => '', 'class' => 'form-control fcontrolsize', 'id' => 'judul', 'placeholder' => 'Title','required' => ''))?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="deskripsi" >Change Log</label>
                                        <div style="width: 60%;float: right;">
                                           <?php echo form_textarea(array('id' => 'descriptionIns', 'name' => 'descriptionIns', 'value' => '', 'class' => 'form-control input-sm', 'id' => 'descriptionIns','placeholder' => "Document Change Log", 'rows' => 5, 'style' => 'height:70px;margin-bottom:10px;max-width: 100%;')) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>                    
                    </div>

                    <div class="col-sm-6" >
                        <tr>
                            <br>
                            <i class="fa fa-user" style="font-size: 20px;padding-left: 3%; color: #03A9F4;">
                                <h4 class="modal-title" style="display:inline; color:#757575;font-size:18px;font-weight:bold; color:#424242;">
                                    Related Parties
                                </h4>
                            </i>
                        </tr>
                        <tr>
                            <div class="modal-body row" style="margin-bottom: 0%;margin-top: 0px;">
                                <div class="row">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="reviewer" >Reviewers*</label>
                                        <div style="width: 60%; float: right;">
                                            <select id="reviewer" name="reviewer[]" class="form-control input-sm select2 reviewer" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required>
                                                <option></option>
                                                <?php
                                                    foreach($variables['reviewer'] as $key => $obj){
                                                        echo "<option value='".$key."'>".$obj."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:5px">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="creator_id" >Creator</label>
                                        <div style="width: 60%;float: right;">
                                            <?php echo form_dropdown('creator_id',array(''=>null), null, 'id="creator_id" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="approver_id">Approver</label>
                                        <div style="width: 60%;float: right;" >
                                            <?php echo form_dropdown('approver_id',array(''=>null), null, 'id="approver_id" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;"'); ?>
                                            <?php if (1==0) {?>
                                                <?php echo form_input(array('name' => 'approver_id2', 'value' => '', 'class' => 'form-control input-sm hidden', 'id' => 'approver_id2', 'placeholder' => 'Approver Name' , 'readonly'=>TRUE))?>
                                                <?php echo form_input(array('name' => 'approver_name', 'value' => '', 'class' => 'form-control input-sm hidden', 'id' => 'approver_name', 'placeholder' => 'Approver Name' , 'readonly'=>TRUE))?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="unit_kerja_terkait" >Related Work Unit</label>
                                        <div style="width: 60%;float:right;">
                                            <select id="unit_kerja_terkait" name="unit_kerja_terkait[]" class="form-control input-sm select2 unit_kerja_terkait" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;">
                                                <option></option>
                                                <?php
                                                    foreach($variables['unitkerja'] as $key => $obj){
                                                        echo "<option value='".$key."'>".$obj."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 5px">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="DokumenTerkait" >Related Document</label>
                                        <div id="listDokumenTerkait" style="width: 60%;float: right;">
                                            <button type="button" class="btn tombolBarui" onClick="openModalRelatedForUpload()" style="padding-left:14px;padding-right:13px;font-weight: 1px; margin-bottom:8px;font-size: 12px; letter-spacing: 1px; margin-top: 0px;">Add Related Document</button>
                                        </div>
                                    </div>
                                </div>
                                <?php if (SessionManagerWeb::getRole() == Role::DOCUMENT_CONTROLLER && SessionManagerWeb::getCompanyUserLogin() == '2000'): ?>
                                <div class="row" style="margin-top: 5px">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="AskChangeLogo" >Change Logo Watermark?</label>
                                        <div style="width: 60%;float:right;">
                                            <select id="AskChange" name="AskChange[]" class="form-control input-sm select2 AskChange">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden" id="row_company_header">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="company_header" >Logo Header Watermark</label>
                                        <div style="width: 60%;float:right;">
                                            <select id="company_header" name="company_header" class="form-control input-sm select2 company_header" style="width:100%; font-size: 12px; letter-spacing: 0.5px;">
                                                <option></option>
                                                <?php
                                                    foreach($company as $key => $name){
                                                        echo "<option value='".$key."'>".$name."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <?php endif ?>
                                
                            </div>
                        </tr>


                    </div>
                    <div class="row col-sm-12" style="margin: 0px;padding: 10px 10px; background-color: transparent;">
                        <div class="row" style="margin: 0px;">
                                <div class="row col-sm-12" style="padding: 0px;margin: 0px;">
                                    <button type="button" id="btn_simpan" class="btn btn-sm btn-success btn-rad pull-right tombolBarui22" style="padding-left:25px;padding-right:25px; margin-right: 10px; letter-spacing: 1px; " onClick="submitDocument()">Submit Document
                                    </button>
                                    <button type="button" id="btn_edit_document" class="btn btn-sm btn-success btn-rad pull-right tombolBarui22 hidden" style="padding-left:25px;padding-right:25px; margin-right: 10px; letter-spacing: 1px;" onClick="submitDocument('0')">Create Document</button>
                                    <button type="button" class="btn btn-sm btn-danger btn-rad pull-right tombolBarui" data-dismiss="modal" style="margin: 0px; margin-right: 10px; letter-spacing: 1px;">Cancel
                                    </button>&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                <button id="btnSubmit" class="btn btn-info col-sm-12" style="display:none;"></button>
            </form>
        </div>
    </div>
</div>

<div id="modal_upload_board_manual" class="modal fade" style="overflow-y:scroll;">
    <div class="modal-dialog2">
        <div class="modal-content row col-sm-12" style="padding: 0px; background-color: transparent;">
            <div class="row" style="margin: 0px;">
                <div class="col-sm-12" style="background-color: #FFC107;border-top-left-radius: 10px;border-top-right-radius: 10px;">
                    <div class="col-md-12 text-right" style="">
                        <h4>
                            <i class="fa fa-close new-close" data-dismiss='modal' ></i>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="row col-md-12" id="alertFileExceeds_bm" style="display: none;width:103%;">
                <div class="row" style="margin: 0px;">
                    <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                        <div class="col-md-10" style="">
                        <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                            <strong style="color:#b71c1c;">
                                Warning!
                            </strong>
                            <label id="alertMessageNewDoc_bm" style="color:#b71c1c;">
                                Maximum file limit of 20MB Exceeded, please upload file less than 20MB!
                            </label>
                        </div>
                        </div>
                        <div class="col-md-2 text-right" id="hide_alertFileExceeds_bm" style="">
                                <h4>
                                    <i class="fa fa-close new-close" ></i>
                                </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12" style="background-color: #F0FFFF;height: 15em;">
                        <div class="col-md-12" style="margin-top:10px;">

                            <form class="col-md-12 dropzone" action="<?php echo site_url('web/document/documentformbm'); ?>" align="center"  id="dropboxbm" style="border: 5px dotted #03A9F4;padding: 7px 25%;height:10em;">
                                <div class="col-sm-12" style="">
                                    <div id="containerDropbox_bm" style="display:inline;">
                                        <div class="col-sm-12 dz-message" data-dz-message style="margin: 20px;">
                                            <i class='fa fa-copy' aria-hidden='true' style='font-size: 50px; color:#03A9F4; float:middle;'></i>
                                            <h5 align="center">
                                                <b>
                                                    Click to Choose or Drop File Here
                                                </b>
                                            </h5>
                                            <div class="row">
                                                <div id="files_preview_bm" name="files_preview_bm" class="col-md-12" align="center">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="row col-md-12" id="alertForNewDoc_bm" style="display: inline;width:100%;padding: 0px;">
            <div class="row" style="margin: 0px;">
                <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                    <div class="col-md-10" style="">
                        <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                            <strong style="color:#b71c1c;">
                                Warning!
                            </strong>
                            <label id="alertMessageNewDoc_bm" style="color:#b71c1c;">
                                You have already uploaded this document or the document is not PDF!
                            </label>
                        </div>
                    </div>
                    <div class="col-md-2 text-right" id="hide_alertForNewDoc_bm" style="">
                        <h4>
                            <i class="fa fa-close new-close" ></i>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-content row col-sm-12" style="padding: 0px; background-color: transparent;">
            <form method="post" name="kirim_form_bm" id="kirim_form_bm" action="<?= site_url('web/document/kirimBoardManual') ?>">
                <div class="row" style="margin: 0px; background-color: #fff;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                    <div class="col-sm-6" style="background-color: #fff;">
                        <tr>
                            <br>
                            <i class="fa fa-file" style="font-size: 20px;padding-left: 3%; color: #03A9F4;">
                                <h4 class="modal-title" style="display:inline; color:#757575;font-size:18px;font-weight:bold; color:#424242;">
                                    Detail Document
                                </h4>
                            </i>
                        </tr>
                        <tr>
                            <div class="modal-body row" style="margin-bottom: 0%;margin-top: 0px;padding-bottom: 0px;">
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="judul_bm">Title* </label>
                                        <?php echo form_input(array('name' => 'judul_bm', 'value' => '', 'class' => 'form-control fcontrolsize', 'id' => 'judul_bm', 'placeholder' => 'Title','required' => ''))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="kodeAdministratif_bm">Document Code*</label>
                                        <div style="width: 60%; float: right;" >
                                            <?php echo form_input(array('name' => 'kodeAdministratif_bm', 'value' => '', 'class' => 'form-control input-sm', 'id' => 'kodeAdministratif_bm', 'placeholder' => 'Document Code', 'required'=>true))?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="revision_bm">Revision* </label>
                                        <?php echo form_input(array('name' => 'revision_bm', 'value' => '0', 'class' => 'form-control fcontrolsize', 'id' => 'revision_bm', 'placeholder' => 'Revision','required' => ''))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="publish_date_bm" >Publish Date*</label>
                                        <?php echo form_input(array('name' => 'publish_date_bm', 'class' => 'form-control fcontrolsize', 'id' => 'publish_date_bm', 'placeholder' => 'dd-mm-yyyy','required' => ''))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;;">
                                        <label for="validity_date_bm" >Validity Date*</label>
                                        <?php echo form_input(array('type'=>'text', 'name' => 'validity_date_bm', 'value' => '', 'class' => 'form-control fcontrolsize', 'id' => 'validity_date_bm', 'readonly'=>''))?>
                                    </div>
                                </div>
                                <?php if (1==0) { ?>
                                    <div class="row" style="margin-top: 1%">
                                        <div class="form-group" style="padding: 0px 20px;"> 
                                            <label for="jenisIns" >Document Type*</label>
                                            <?php echo form_input(array('value' => 'Board Manual', 'class' => 'form-control fcontrolsize', 'placeholder' => 'Document Type','required' => ''))?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="company_bm" >Company*</label>
                                        <div style="width: 60%; float: right;">
                                            <select id="company_bm" name="company_bm" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required>
                                                <option></option>
                                                <?php
                                                    foreach($company as $key => $obj){
                                                        echo "<option value='".$key."'>".$obj."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <?php if (1==0) { ?>
                                    <div class="row" style="margin-top: 1%">
                                        <div class="form-group" style="padding: 0px 20px;">
                                            <label for="prosesbisnis_bm" >Business Process*</label>
                                            <div style="width: 60%; float: right;">
                                                <select id="prosesbisnis_bm" name="prosesbisnis_bm" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required>
                                                    <option></option>
                                                    <?php
                                                        foreach($variables['prosesbisnis'] as $key => $obj){
                                                            echo "<option value='".$key."'>".$obj."</option>";
                                                        }
                                                    ?>
                                               </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 1%">
                                        <div class="form-group" style="padding: 0px 20px;">
                                            <label for="input_iso" >Requirement*</label>
                                            <div style="width: 60%; float: right;">
                                                <?php
                                                    // foreach($variables['document_iso'] as $key => $obj){
                                                    //     echo "<input type='checkbox' name='input_iso[]' id='input_iso' onClick='javascript:getKlausul()' value='".$obj['id']."'>".$obj['iso']."<br>";
                                                    // }
                                                ?>
                                                <?php if (1==1) {?>
                                                <select id="input_iso" name="input_iso[]" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" onChange="javascript:getKlausul()" required>
                                                    <?php
                                                        foreach($variables['document_iso'] as $key => $obj){
                                                            echo "<option value=\"".$obj['id']."\"'>".$obj['iso']."</option>";
                                                        }
                                                    ?>
                                               </select>
                                               <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 1%">
                                        <div class="form-group hidden" id="div_klausul" style="padding: 0px 20px;">
                                            <label for="input_iso_klausul" >Klausul</label>
                                            <div style="width: 60%;float: right;">
                                                <select id="input_iso_klausul[]" name="input_iso_klausul[]" class="form-control input-sm select2 input_iso_klausul" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;">
                                               </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 1%">  
                                        <div class="form-group hidden" id="div_retension" style="padding: 0px 20px;">
                                            <label for="retension_input" >Retention Period*</label>
                                            <div style="width: 60%;float: right;">
                                                <?php echo form_input(array('name' => 'retension_input', 'value' => '', 'class' => 'form-control input-sm', 'id' => 'retension_input', 'placeholder' => 'Retention Period', 'style' => 'width:55%;display:inline' ))?>
                                                <select id="retension_select" name="retension_select" class="form-control input-sm" style="width:40%; font-size: 12px; letter-spacing: 0.5px;display:inline">
                                                    <option value="0">Bulan</option>
                                                    <option value="1">Tahun</option>
                                               </select>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="deskripsi_bm" >Change Log</label>
                                        <div style="width: 60%;float: right;">
                                           <?php echo form_textarea(array('id' => 'descriptionIns_bm', 'name' => 'descriptionIns_bm', 'value' => '', 'class' => 'form-control input-sm','placeholder' => "Document Change Log", 'rows' => 5, 'style' => 'height:70px;margin-bottom:10px;max-width: 100%;')) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>                    
                    </div>

                    <div class="col-sm-6" >
                        <tr>
                            <br>
                            <i class="fa fa-user" style="font-size: 20px;padding-left: 3%; color: #03A9F4;">
                                <h4 class="modal-title" style="display:inline; color:#757575;font-size:18px;font-weight:bold; color:#424242;">
                                    Related Parties
                                </h4>
                            </i>
                        </tr>
                        <tr>
                            <div class="modal-body row" style="margin-bottom: 0%;margin-top: 0px;">
                                <?php if (1==0) { ?>
                                    <div class="row">
                                        <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                            <label for="reviewer" >Reviewers*</label>
                                            <div style="width: 60%; float: right;">
                                                <select id="reviewer" name="reviewer[]" class="form-control input-sm select2 reviewer" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required>
                                                    <option></option>
                                                    <?php
                                                        foreach($variables['reviewer'] as $key => $obj){
                                                            echo "<option value='".$key."'>".$obj."</option>";
                                                        }
                                                    ?>
                                               </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:5px">
                                        <div class="form-group" style="padding: 0px 20px;">
                                            <label for="creator_id" >Creator</label>
                                            <div style="width: 60%;float: right;">
                                                <?php echo form_dropdown('creator_id',array(''=>null), null, 'id="creator_id" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group" style="padding: 0px 20px;">
                                            <label for="approver_id">Approver</label>
                                            <div style="width: 60%;float: right;" >
                                                <?php echo form_dropdown('approver_id',array(''=>null), null, 'id="approver_id" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;"'); ?>
                                                <?php if (1==0) {?>
                                                    <?php echo form_input(array('name' => 'approver_id2', 'value' => '', 'class' => 'form-control input-sm hidden', 'id' => 'approver_id2', 'placeholder' => 'Approver Name' , 'readonly'=>TRUE))?>
                                                    <?php echo form_input(array('name' => 'approver_name', 'value' => '', 'class' => 'form-control input-sm hidden', 'id' => 'approver_name', 'placeholder' => 'Approver Name' , 'readonly'=>TRUE))?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="unit_kerja_terkait_bm" >Related Work Unit</label>
                                        <div style="width: 60%;float:right;">
                                            <select id="unit_kerja_terkait_bm" name="unit_kerja_terkait_bm[]" class="form-control input-sm select2 unit_kerja_terkait" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;">
                                                <option></option>
                                                <?php
                                                    foreach($variables['unitkerja'] as $key => $obj){
                                                        echo "<option value='".$key."'>".$obj."</option>";
                                                    }
                                                ?>
                                           </select>
                                        </div>
                                    </div>
                                </div>
                                <?php if (1==0) { ?>
                                    <div class="row" style="margin-top: 5px">
                                        <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                            <label for="DokumenTerkait_bm" >Related Document</label>
                                            <div id="listDokumenTerkait_bm" style="width: 60%;float: right;">
                                                <button type="button" class="btn tombolBarui" onClick="openModalRelatedForUploadBm()" style="padding-left:14px;padding-right:13px;font-weight: 1px; margin-bottom:8px;font-size: 12px; letter-spacing: 1px; margin-top: 0px;">Add Related Document</button>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </tr>
                    </div>
                    <div class="row col-sm-12" style="margin: 0px;padding: 10px 10px; background-color: transparent;">
                        <div class="row" style="margin: 0px;">
                            <div class="row col-sm-12" style="padding: 0px;margin: 0px;">
                                <button type="button" id="btn_simpan_bm" class="btn btn-sm btn-success btn-rad pull-right tombolBarui22" style="padding-left:25px;padding-right:25px; margin-right: 10px; letter-spacing: 1px; " onClick="submitDocumentBm()">Upload Document
                                </button>
                                <button type="button" class="btn btn-sm btn-danger btn-rad pull-right tombolBarui" data-dismiss="modal" style="margin: 0px; margin-right: 10px; letter-spacing: 1px;">Cancel
                                </button>&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                <button id="btnSubmitBm" class="btn btn-info col-sm-12" style="display:none;"></button>
            </form>
        </div>
    </div>
</div>

<script>


var related_terpilih = [];
// Setting + Load
$("[name='publish_date_bm']").datepicker({
    format: 'dd-mm-yyyy',
    endDate: new Date()
});


$("#publish_date_bm").on('change', function(){
    var d = $("#publish_date_bm").datepicker("getDate");
    var year = d.getFullYear()+1;
    var month = ('0'+(d.getMonth()+1)).slice(-2);
    var day = ('0'+d.getDate()).slice(-2);
    $("#validity_date_bm").attr('value',day+'-'+month+'-'+year);
});

function hideUpload(){
    var val = document.getElementById("select_upload").value;
    $("#dropbox2").removeClass("col-md-6");
    $("#dropbox2").addClass("col-md-12");
    $("#dropboxexcel").addClass("hidden");
    document.getElementById("dropbox2").style.padding = "7px 25%";
    if (val=='0'){
        // $('.dropzone').removeClass('hidden');
        $('#dropbox2').removeClass('hidden');
        $('#containerUploadButton').removeClass('hidden');
        $('#btn_edit_document').addClass('hidden');
        $('#btn_simpan').removeClass('hidden');
        $('#alertChosenType').addClass('hidden');
        document.getElementById('is_create_later').value='0';
    } else {
        // $('.dropzone').addClass('hidden');
        $('#dropbox2').addClass('hidden');
        $('#containerUploadButton').addClass('hidden');
        $('#btn_edit_document').removeClass('hidden');
        $('#btn_simpan').addClass('hidden');
        $('#alertChosenType').removeClass('hidden');
        document.getElementById('is_create_later').value='1';
    }
}

$("#AskChange").on('change',function(){
   var a= document.getElementById('AskChange').value;
   if(a == 1){
        $("#row_company_header").removeClass('hidden');
        $("#row_company_header").attr("required", "true");
   }
   else{
        $("#row_company_header").addClass('hidden');
        $("#row_company_header").attr("required", "false");
   }
});
$("#myunitkerja").on('change',function(){
    changeCode();
    var unitkerja_id = document.getElementById('myunitkerja').value;
    var type = document.getElementById('jenisIns').value;
    $.ajax({
        url : '<?= site_url('web/unitkerja/ajaxGetCreator') ?>/'+unitkerja_id,
        type: 'post',
        cache: false,
        data: {'type':type},
        success: function(respon){
            response = JSON.parse(respon);
            // if (response.creator.user_id!=null){
                $("#creator_id").select2().html('');
            // }
            $.ajax({
                url : '<?= site_url('web/unitkerja/ajaxGetListCreator') ?>/'+unitkerja_id,
                type: 'post',
                cache: false,
                data: {'type':type},
                success: function(resp){
                    json_resp = JSON.parse(resp);
                    $("#creator_id").select2({
                        placeholder : 'Choose creator',
                        data:json_resp 
                    });

                    if (response.creator.user_id!=null){
                        $('#creator_id').val(response.creator.user_id).trigger('change');
                    } else {
                        if (json_resp[0].id!=null){
                            $('#creator_id').val(json_resp[0].id).trigger('change');
                        } else {
                            $("#creator_id").select2().html('');
                        }
                    }
                    
                }
            });
        }
    });
});

$("#creator_id").on('change',function(){
    var types_id = document.getElementById("jenisIns").value;
    // var unitkerja_id = document.getElementById('myunitkerja').value;
    var creator_id = document.getElementById('creator_id').value;
    $.ajax({
        url : '<?= site_url('web/unitkerja/ajaxGetApprover') ?>/',
        type: 'post',
        cache: false,
        data: {"creator_id":creator_id, "type_id":types_id},
        success: function(respon){
            response = JSON.parse(respon);
            $("#approver_id").select2().html('');
            $("#approver_id").select2({
                data:response.approver,
                placeholder : 'Choose Approver'
            });
            // if (response.approver[0].id!='null'){
                $('#approver_id').val(response.approver[0].id).trigger('change');
            // } 
            // else {
            //     $('#approver_id').val('').trigger('change');
            // }
            
            //     document.getElementById('approver_id').value=response.approver.user_id;
            //     document.getElementById('approver_name').value=response.approver.name;
        }
    });
});

function changeCode(){
    var types_id = document.getElementById("jenisIns").value;
    var prosesbisnis_id = document.getElementById("prosesbisnis").value;
    var unitkerja_id = document.getElementById("myunitkerja").value;
    var form_type = document.getElementById("form_type").value;
    $.ajax({
        url : '<?= site_url('web/document/ajaxgetcode') ?>',
        type: 'post',
        cache: false,
        data: {"type_id":types_id, "prosesbisnis_id":prosesbisnis_id, "unitkerja_id":unitkerja_id, "form_type":form_type},
        success: function(respon){
                document.getElementById("kodeAdministratif").value = respon;
            }
    });
}

$("#jenisIns").on('change', function(){
    var types_id = document.getElementById("jenisIns").value;
    $.ajax({
        url : '<?= site_url('web/document_types/ajaxGetDetail') ?>',
        type: 'post',
        cache: false,
        data: {"id":types_id},
        success: function(respon){
            response = JSON.parse(respon);

            if (response.need_reviewer==0){
                $("#reviewer").val(null).trigger("change"); 
                document.getElementById("reviewer").setAttribute('disabled', true);
            } else {
                document.getElementById("reviewer").removeAttribute('disabled');
            }

            if (response.retension=='0'){
                $("#div_retension").addClass('hidden');
                document.getElementById("retension_input").value = '';
                $("#retension_input").removeAttr('required');
            } else {
                $("#div_retension").removeClass('hidden');
                $("#retension_input").attr('required', true);
            }

            if (response.klausul==0){
                $("#input_iso_klausul").val(null).trigger("change"); 
                $("#div_klausul").addClass('hidden');
            } else {
                $("#div_klausul").removeClass('hidden');
            }

            // kalo form pilih jenisnya 
            if (response.code!='F'){
                $("#form_type").val(null).trigger("change"); 
                $("#form_type").removeAttr("required");
                $("#div_form_type").addClass('hidden');
            } else {
                $("#div_form_type").removeClass('hidden');
                $("#form_type").attr("required", true);
            }

            document.getElementById("myunitkerja").removeAttribute('disabled');
            var unitkerja_id = document.getElementById('myunitkerja').value;
            if (unitkerja_id!=null && unitkerja_id!=''){
                var type = document.getElementById('jenisIns').value;
                $("#creator_id").select2().html('');
                $.ajax({
                    url : '<?= site_url('web/unitkerja/ajaxGetCreator') ?>/'+unitkerja_id,
                    type: 'post',
                    cache: false,
                    data: {'type': type},
                    success: function(respon){
                        response = JSON.parse(respon);
                        var type = document.getElementById('jenisIns').value;
                        $("#creator_id").select2().html('');
                        $.ajax({
                            url : '<?= site_url('web/unitkerja/ajaxGetListCreator') ?>/'+unitkerja_id,
                            type: 'post',
                            cache: false,
                            data: {'type':type},
                            success: function(resp){
                                json_resp = JSON.parse(resp);
                                
                                $("#creator_id").select2({
                                    placeholder : 'Choose creator',
                                    data:json_resp 
                                });
                                $('#creator_id').val(response.creator.user_id).trigger('change');
                            }
                        });
                    }
                });
            } 
        }
    });
    changeCode();
});




function upload2(){
    document.getElementById("dropbox2").click();
}

$("#retension_input").on('change', function(){
    if ($("#retension_input").val()<=0){
        alert("Value must bigger than zero!");
        $("#retension_input").val(null);
    }
});

function submitDocument(create='1'){
    if((!isUploading2 && dropzone2.files.length>0) && create=='1'){
        var filename = dropzone2.files[0].name;
        var exts = filename.split(".");
        var ext = exts[exts.length-1];
        if (ext=='xls' || ext=='xlsx'){
            if((!isUploadingexcel && dropzoneexcel.files.length>0)){
                document.getElementById('btnSubmit').click();
            } else {
                document.getElementById('alertExcelNotPDF').style.display = "inline";
            }
        } else {
            document.getElementById('btnSubmit').click();
        }
        // document.getElementById('btnSubmit').click();
        // alert("Uploaded");
    } else if (create=='0'){
        document.getElementById('btnSubmit').click();
    } else {
        if(dropzone2.files.length>0){
            document.getElementById('alertForNewDoc').style.display = "inline";
            document.getElementById('alertMessageNewDoc').textContent = "Proses upload belum selesai";
        }else{
            document.getElementById('alertForNewDoc').style.display = "inline";
            document.getElementById('alertMessageNewDoc').textContent = "Anda belum mengupload file dokumen";
        }

    }
}

function submitDocumentBm(){
    if((!isUploading_bm && dropzone_bm.files.length>0)){
        document.getElementById('btnSubmitBm').click();
        // alert("Uploaded");
    }
    else{
        if(dropzone_bm.files.length>0){
            document.getElementById('alertForNewDoc_bm').style.display = "inline";
            // document.getElementById('alertMessageNewDoc_bm').textContent = "Upload is not done yet!";
            $('#alertMessageNewDoc_bm').text("Upload is not done yet!");
        }else{
            document.getElementById('alertForNewDoc_bm').style.display = "inline";
            // document.getElementById('alertMessageNewDoc_bm').textContent = "You not upload a document yet!";
            $('#alertMessageNewDoc_bm').text("You not upload a document yet!");
        }

    }
}

var index = 0;
var isUpload = false;
function addNewInput(){
    var parent = document.getElementById("listDokumenTerkait");
    var newInput = document.createElement("input");
    newInput.id = "related" + index;
    newInput.name = "related[]";
    newInput.type = "text";
    newInput.className = "form-control";
    newInput.style = "display: inline;font-size: 12px;margin-bottom:8px;";
    parent.appendChild(newInput);
    $( "#related" + index ).autocomplete({
        source: "<?php echo site_url('web/document/allDocument'); ?>",
        minLength: 1,
        select: function( event, ui ) {
        },
        open: function (event, ui) {
            $(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
            $(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
        },
        close: function (event, ui) {
            },
             messages: {
                    noResults: '',
                    results: function() {}
                }            
        })
     .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<div>" )
            .data( "ui-autocomplete-item", item )
            .append( "<a style='margin-left:5px;'>"+ item.label+ "</a>" )
            .appendTo( ul );
        };
    index++;
}

function openModalRelatedForUpload(){
    // console.log('masuk fungsi ini');
    var listID = [];
    var temp = 0;
    isUpload = true;
    $("#RelatedBox").modal('show');
    document.getElementById('RelatedTitle').textContent = "Related Document";
    $("#listDokumenTerkait > input").each(function() {
      listID[temp]= $(this).attr("value") ;
      temp++;
    });
    var http = new XMLHttpRequest();
    http.open("POST", "<?php echo site_url('/web/document/getChoosenDocument/');?>", true);
    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    var param = "listid=" + JSON.stringify(listID);
    http.send(param);
    http.onload = function() {
        var doc = JSON.parse(http.responseText);
        // console.log(doc);
        var size = doc.length;
        var inner = "";
        var tableR = document.getElementById('tabelRelated');
        var lenRow = tableR.rows.length;
        var ins = 0;
        for(var i=1;i<lenRow;i++){
            tableR.deleteRow(1);
        }
        for(var i=0;i<size;i++){
            var id = doc[0][i].document_id;
            var title = doc[0][i].title;
            inner += "<tr>";
            inner += "<td style='display:none;'>" + id+ "</td>";
            inner += "<td style='display:none;'>true</td>";
            inner += "<td id='titleRelated' name='titleRelated[]'>" + title + "</td>";
            inner += "<td>" + doc[i][0].type_name + "</td>";
            inner += "<td>" + doc[0][i].username + "</td>";
            inner += "<td><button class='btn btn-success' onClick='viewDocument(" + id +"," + title + ")'><i class='fa fa-eye' aria-hidden='true' style='float:middle;'></i></button></td>";
            inner += "<td><input type='checkbox' name='vehicle' value='Terkait' checked></td>";
            inner += "</tr>";
        }
        tableR.innerHTML += inner;
    }
    document.getElementById('simpanRelated').onclick = pilihRelated;
}

function pilihRelated(){
    related_terpilih=[];
    var tableR = document.getElementById('tabelRelated');
    var lenRow = tableR.rows.length;
    var parent = document.getElementById("listDokumenTerkait");
    while (parent.childElementCount>1) {
        parent.removeChild(parent.lastChild);
    }
    for(var y=1;y<lenRow;y++){
        var checked = $('input', tableR.rows[y].cells[6]).is(':checked');
        var id = tableR.rows[y].cells[0].innerHTML;
        var title = tableR.rows[y].cells[2].innerHTML;
         if(checked){
            var newInput = document.createElement("input");
            newInput.id = "related";
            newInput.name = "related[]";
            newInput.type = "hidden";
            newInput.value = id;
            related_terpilih.push(id);
            newInput.className = "pilihRelated";
            var newLabel = document.createElement("label");
            newLabel.innerHTML = title;
            newLabel.className = "form-control";
            newLabel.style = "border-style:ridge;font-size: 12px;margin-bottom:8px;padding-left:3px;";
            parent.appendChild(newInput);
            parent.appendChild(newLabel);
        }
    }
    // console.log(related_terpilih);
    $("#RelatedBox").modal('show'); 
}

</script>
<style type="text/css">
   .ui-autocomplete {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 16777270;
    float: left;
    display: none;
    min-width: 160px;   
    padding: 4px 0;
    margin: 0 0 10px 25px;
    list-style: none;
    background-color: #ffffff;
    border-color: #ccc;
    border-color: rgba(0, 0, 0, 0.2);
    border-style: solid;
    border-width: 1px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    *border-right-width: 2px;
    *border-bottom-width: 2px;
    cursor: pointer;
}

.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;
    text-decoration: none;
}

.ui-state-hover, .ui-state-active {
    color: #ffffff;
    text-decoration: none;
    background-color: #0088cc;
    border-radius: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    background-image: none;
}
</style>
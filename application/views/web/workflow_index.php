<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:5%">No</th>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center" style="width:15%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td class="text-center">
                                                <?= $v['rule']['nama'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                echo anchor('web/workflow/configure/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                // echo anchor('web/document/edit/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip"  title="Hapus"');
                                                ?>
                                                <a href="javascript:hapus_workflow(<?php echo $v['id'] ?>,'<?php echo $v['rule']['nama'] ?>')" class="text-danger" title="Hapus Workflow">
                                                    <i class="fa fa-trash"></i>
                                                </a> 
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function hapus_workflow(id,name) {
        if (confirm('Hapus workflow "'+name+'" ?')){
            location.href = "<?= site_url('web/workflow/delete') ?>/"+id;
        }
    }

</script>
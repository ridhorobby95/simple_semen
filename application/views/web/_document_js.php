
<script>

	$('#hide_alertForNewDoc').on('click', function (){
		document.getElementById('alertForNewDoc').style.display = "none";
	});

	$('#hide_alertExcelNotPDF').on('click', function (){
		document.getElementById('alertExcelNotPDF').style.display = "none";
	});

	$('#hide_alertForNewDoc_bm').on('click', function (){
		document.getElementById('alertForNewDoc_bm').style.display = "none";
	});

	$('#hide_alertFileExceeds').on('click', function (){
		document.getElementById('alertFileExceeds').style.display = "none";
	});
	$('#hide_alertFileExceeds_bm').on('click', function (){
		document.getElementById('alertFileExceeds_bm').style.display = "none";
	});

	function viewDocumentLess(){
		var page = parseInt("<?= $page ?>")-1;
		window.location.href = "<?php echo site_url('web/document/dashboard') ?>/"+page;
	}

	function viewDocumentMore(){
		var page = parseInt("<?= $page ?>")+1;
		window.location.href = "<?php echo site_url('web/document/dashboard') ?>/"+page;
	}

	// $('#input_iso').on('click', getKlausul());

	function getKlausul(is_reset_klausul=1){
		var iso_id = $('#input_iso').val();
		$.ajax({
	        url : "<?php echo site_url('/web/document_iso_klausul/ajaxGetKlausul');?>",
	        type: 'post',
	        dataType: 'JSON',
	        data: {'iso_id':iso_id},
	        success:function(respon){
	        	var klausul = $.map(respon, function (obj) {
                    obj.id = obj.id || obj.id; // replace id_kab with your identifier
                    obj.text = obj.text || obj.klausul; // replace nama with your identifier
                    return obj;
                });
                if (is_reset_klausul==1){
                	$(".input_iso_klausul").html('');
                }
                
        	  	$(".input_iso_klausul").select2({
        	  		placeholder: "Klausul",
        	  		data : klausul,
        	  		minimumResultsForSearch: Infinity
        	  	});
	        } 
	    });
	}

	$(document).ready(function() {
	    $(".header-title.row").addClass('hidden');
	});

	function filterJenis(jenis){
		document.getElementById('type').value = jenis;
		// document.getElementById('lokasiFilter').value = "";
		// document.getElementById('numPage').value = "1";
		document.getElementById('filter_form').submit();
	}

	function filterShow(show){
		document.getElementById('show_by').value = show;
		document.getElementById('filter_form').submit();
	}

	function filterLokasi(lokasi){
		document.getElementById('lokasiFilter').value = lokasi;
		document.getElementById('jenisFilter').value = "";
		document.getElementById('numPage').value = "1";
		document.getElementById('filter_form').submit();
	}

	function filterTag(tag){
		document.getElementById('lokasiFilter').value = "";
		document.getElementById('jenisFilter').value = "";
		document.getElementById('tagFilter').value = tag;
		document.getElementById('numPage').value = "1";
		document.getElementById('filter_form').submit();
	}

	function changePage(page){
		document.getElementById('numPage').value = page;
		document.getElementById('filter_form').submit();
	}

	function unsetPage(){
		document.getElementById('numPage').value = 1;
	}

	var isSearch = false;
	function changeIconAdvanceSearch(){
		// isSearch = !isSearch;
		// if(isSearch){
		// 	document.getElementById('iconAdvanceSearch').className = "fa fa-minus-square";
		// }else{
		// 	document.getElementById('iconAdvanceSearch').className = "fa fa-plus-square";
		// }
	}
	var relatedDocument = [];
	function openNewUploadBox(){
		document.getElementById('alertForPerbaharui').style.display = "none";
		document.getElementById('alertForNewDoc').style.display = "none";
		document.getElementById('alertExcelNotPDF').style.display = "none";
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/removeFileTemp');?>", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {

		  	if(currentFile!=null && dropzone!=null){
		    	dropzone.removeFile(currentFile);	
		    	currentFile = null;
			}
			if(currentFile2!=null && dropzone!=null){
		    	dropzone2.removeFile(currentFile2);	
		    	currentFile2 = null;
			}
			$("#NewUploadBox").modal('show');
		}
	}

	function openNewUploadBoxBoardManual(){
		document.getElementById('alertForNewDoc_bm').style.display = "none";
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/removeFileTemp');?>", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {

		  	if(currentFile_bm!=null && dropzone_bm!=null){
		    	dropzone_bm.removeFile(currentFile_bm);	
		    	currentFile_bm = null;
			}
			$("#modal_upload_board_manual").modal('show');
		}
	}

	function Download() {
		var id = document.getElementById('dokumen').textContent;
		if(id == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			window.open("<?php echo base_url('/web/document/download_watermark/') ?>/"+id);
			// document.getElementById('frame_download').src = "<?php //echo base_url('/web/document/download_watermark') ?>?file="+id;
		}
	}

	function checklistDownload(value){
		if(value == 1){
			$('.form_checklist_download').removeClass('hidden');
			$('.th_checklist_download').removeClass('hidden');
			$('.td_checklist_download').removeClass('hidden');
			$('.submit_checklist_download').removeClass('hidden');
			$('.button_checklist_download').addClass('hidden');
			$('.button_unchecklist_download').removeClass('hidden');
		}
		else{
			// $('.form_checklist_download').addClass('hidden');
			$('.th_checklist_download').addClass('hidden');
			$('.td_checklist_download').addClass('hidden');
			$('.submit_checklist_download').addClass('hidden');
			$('.button_checklist_download').removeClass('hidden');
			$('.button_unchecklist_download').addClass('hidden');
		}
		
	}
	function Download_creator() {
		$("#download_creator_modal").modal('show');
	};

	function Download_creator_proc(word){
		var id = document.getElementById('dokumen').textContent;
		if(id == "0"){
			alert("Anda belum memilih dokumen");
		}else{

			window.open("<?php echo base_url('/web/document/download_watermark/') ?>/"+id+"/"+word);
			// document.getElementById('frame_download').src = "<?php //echo base_url('/web/document/download_watermark') ?>?file="+id;
		}
	}
	function Evaluate() {
		if (confirm("Are you sure to evaluate this document?")){
			var id = document.getElementById('dokumen').textContent;
			var access_eval = document.getElementById('access_eval').textContent;
			var drafter_eval = document.getElementById('drafter_eval').textContent;
			if(id == "0"){
				alert("You not choose any document yet.");
			}else{
				$.ajax({
			        url : "<?php echo site_url('/web/document/ajaxEvaluateDocument');?>/",
			        type: 'post',
			        data: {'id':id, 'access_eval':access_eval, 'drafter_eval':drafter_eval},
			        success:function(respon){
			        	window.location.href = "<?php echo site_url('web/document/detail') ?>/"+respon;
			        } 
			    }); 
			}
		}
		
	};

	function reconvert() {
		if (confirm("Are you sure to reconvert this document? (Will be waiting for 15 minutes to view this document)")){
			var id = document.getElementById('dokumen').textContent;
			if(id == "0"){
				alert("You not choose any document yet.");
			}else{
				$.ajax({
			        url : "<?php echo site_url('/web/document/reconvert');?>/",
			        type: 'post',
			        data: {'id':id},
			        success:function(respon){
			        	if(respon == 'excel'){
			        		alert('documents with excel extensions, cannot be reconverted');
			        	}
			        	else{
			        		window.location.href = "<?php echo site_url('web/document') ?>";
			        	}
			        	
			        } 
			    }); 
			}
		}
		
	};

	function changeUnit() {
		var id = document.getElementById('dokumen').textContent;
		window.open("<?php echo site_url('web/document/changeunit') ?>/"+id);
	};

	function pageEvaluation(page){
		$.ajax({
	        url : "<?php echo site_url('/web/document/ajaxSetSessionPage');?>/",
	        type: 'post',
	        data: {'page':page},
	        success:function(respon){
	        	window.location.href = "<?php echo site_url('web/document/dashboard') ?>";
	        } 
	    }); 
	}

	function show_ask_download(){
		var id = document.getElementById('dokumen').textContent;
		// alert(id);
		$.ajax({
	        url : "<?php echo site_url('/web/document/checkAskDownloadExist');?>/",
	        type: 'post',
	        data: {'id':id},
	        success:function(respon){
	        	if(respon == 0){
	        		$("#ask_download_modal").modal('show');
	        	}
	        	else{
	        		alert("You already request download this document");
	        	}
	        	// window.location.href = "<?php echo site_url('web/document/dashboard') ?>";
	        } 
	    }); 
		// $("#ask_download_modal").modal('show');
	}
	function show_reconvert(){
		$("#reconvert_modal").modal('show');
	}

	function askDownload() {
		var id = document.getElementById('dokumen').textContent;
		var reason = document.getElementById('download_reason_textarea').value;
		if(id == "0"){
			alert("You not choose any document yet.");
		}else{
			if (reason.trim()!='' && reason.trim()!=null){
				$.ajax({
			        url : "<?php echo site_url('/web/document/ajaxAskDownload');?>/",
			        type: 'post',
			        data: {'id':id, 'reason':reason},
			        success:function(respon){
			        	// window.location.href = "<?php echo site_url('web/document/detail') ?>/"+respon;
			        	window.location.href = "<?php echo site_url('web/document/download_detail') ?>/"+respon;
			        } 
			    }); 
			} else {
				alert('Reason cant be empty!');
			}
			
		}
	};

	function DownloadRevision(id){
		if(id == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			document.getElementById('frame_download').src = "<?php echo base_url('/web/document/download') ?>?file="+id;
		}
	}
	
	window.onclick = function(event) {
		var modal = document.getElementById('myModal');
		if (event.target == modal) {
			modal.style.display = "none";
	}};
	
	function Edit() {
		var id = document.getElementById('dokumen').textContent;
		var title = document.getElementById('judul_file').textContent;
		document.getElementById('PerbaharuiTitle').textContent = "Perbaharui Dokumen - " + title;
		if(id == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			removeUploadedFile(true)
		}
	};

	function send(){
		var id = document.getElementById('dokumen').textContent;
		var note = document.getElementById('updateNote').value;
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/editDocument');?>", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "id_dokumen=" + id;
		param += "&note=" + note;
		http.send(param);
		http.onload = function() {
			// alert(http.responseText);
		 	location.reload();
		}
	}

	var indexRelated = 0;
	function addRelated(){
		var parent = document.getElementById("listDokumenTerkaitDetail");
		var newInput = document.createElement("input");
		newInput.id = "relatedEdit" + indexRelated;
		newInput.name = "relatedEdit[]";
		newInput.type = "text";
		newInput.className = "form-control";
		newInput.style = "display: inline;font-size: 12px;margin-bottom:8px;";
		parent.appendChild(newInput);
		$( "#relatedEdit" + indexRelated ).autocomplete({
		    source: "<?php echo site_url('web/document/allDocument'); ?>",
		    minLength: 1,
		    select: function( event, ui ) {
		    },
		    open: function (event, ui) {
		        $(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
		        $(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
		    },
		    close: function (event, ui) {
		        },
			        messages: {
		                noResults: '',
		                results: function() {}
		            }            
		    })
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		    return $( "<div>" )
		    .data( "ui-autocomplete-item", item )
		    .append( "<a style='margin-left:5px;'>"+ item.label+ "</a>" )
		    .appendTo( ul );
		};
		indexRelated++;    
	}

	function prepareEdit(){
		// document.getElementById('editBtn1').onclick = sendEdit;
		// document.getElementById('editBtn1').textContent = "Save";
		document.getElementById('judul_file').style.display = "none";
		document.getElementById('judul2').style.display = "inline";
		document.getElementById('deskripsi_file').style.display = "none";
		document.getElementById('deskripsi').style.display = "inline";
		document.getElementById('lokasi_file').style.display = "none";
		document.getElementById('divLokasi').style.display = "inline";
		document.getElementById('klasifikasi_file').style.display = "none";
		document.getElementById('divKlasifikasi').style.display = "inline";
		document.getElementById('jenis_file').style.display = "none";
		document.getElementById('divJenis').style.display = "inline";
		document.getElementById('tags_file').style.display = "none";
		document.getElementById('divTags').style.display = "inline";
	}

	function closeEdit(){
		// document.getElementById('editBtn1').onclick = prepareEdit;
		// document.getElementById('editBtn1').textContent = "Edit";
		document.getElementById('judul_file').style.display = "inline";
		document.getElementById('judul2').style.display = "none";
		document.getElementById('deskripsi_file').style.display = "inline";
		document.getElementById('deskripsi').style.display = "none";
		document.getElementById('lokasi_file').style.display = "inline";
		document.getElementById('divLokasi').style.display = "none";
		document.getElementById('klasifikasi_file').style.display = "inline";
		document.getElementById('divKlasifikasi').style.display = "none";
		document.getElementById('jenis_file').style.display = "inline";
		document.getElementById('divJenis').style.display = "none";
		document.getElementById('tags_file').style.display = "inline";
		document.getElementById('divTags').style.display = "none";
	}

	function sendEdit(){
		closeEdit();

		var idDokumen = document.getElementById('dokumen').textContent;
		var title = document.getElementById('judul2').value;
		var deskripsi = document.getElementById('deskripsi').value;
		var lokasi = document.getElementById('lokasi').value;
		// var klasifikasi = document.getElementById('klasifikasi').value;
		var jenis = document.getElementById('jenis').value;
		var tags = $('#tags').select2("val");
						
		var http = new XMLHttpRequest();
	    http.open("POST", "<?php echo site_url('/web/document/editDeskripsi');?>", true);
	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    var param = "id_dokumen=" + idDokumen;
	    param = param + "&title=" + title;
	    param = param + "&deskripsi=" + deskripsi;
	    param = param + "&lokasi=" + lokasi;
	    // param = param + "&klasifikasi=" + klasifikasi;
	    param = param + "&jenis=" + jenis;
	    param = param + "&tags=" + JSON.stringify(tags);
	    http.send(param);
	    http.onload = function() {
	    	var title = document.getElementById('judul2').value;
			document.getElementById('judul_file').textContent = title;
	    	var deskripsi = document.getElementById('deskripsi').value;
			document.getElementById('deskripsi_file').textContent = deskripsi;
			var lokasi = document.getElementById('lokasi').value;
			document.getElementById('lokasi_file').textContent = lokasi;
			// var klasifikasi = document.getElementById('klasifikasi').value;
			document.getElementById('klasifikasi_file').textContent = klasifikasi;
			var jenis = document.getElementById('jenis').value;
			document.getElementById('jenis_file').textContent = jenis;
			document.getElementById('tags_file').textContent = tags;
		}
	}
	
	function openNav() {
	    document.getElementById("viewDoc").style.width = "100%";
	    $('html').css({'overflow-y':'hidden'});
	    $('body').css({'overflow-y':'hidden'});
	    var target = $("#viewerContainer");
		  $("#viewDoc").scroll(function() {
		    target.prop("scrollTop", this.scrollTop)
		          .prop("scrollLeft", this.scrollLeft);
		  });
	}
	function closeNav() {
    	document.getElementById("viewDoc").style.width = "0%";
	    $('html').css({'overflow-y':'scroll'});
	    // $('body').css({'overflow-y':'scroll'});

	}

	function deleteDocument(){
		var id = document.getElementById('dokumen').textContent;
		var http = new XMLHttpRequest();
		$.ajax({
	        url : "<?php echo site_url('/web/document/deleteDocument');?>",
	        type: 'post',
	        data: {id_dokumen:id},
	        success:function(respon){
	            location.reload();
	        } 
	    }); 
	 //    http.open("POST", "<?php// echo site_url('/web/document/deleteDocument');?>", true);
	 //    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	 //    var param = "id_dokumen=" + id;
	 //    http.onload = function() {
	 //    	alert(http.responseText);
	 //    	location.reload();
		// }
	}

	function openDeleteBox(){
		var id = document.getElementById('dokumen').textContent;
		var title = document.getElementById('judul_file').textContent;
		document.getElementById('DeleteTitle').textContent = "Delete Dokumen - " + title;
		if(id == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			$("#DeleteBox").modal('show');
			document.getElementById('namaDocDelete').textContent = document.getElementById('judul_file').textContent
		}
	}

	function openRelatedModal(){
		$("#RelatedBox").modal('show');
		document.getElementById('RelatedTitle').textContent = "Related Document - " + document.getElementById('judul_file').textContent;
		document.getElementById('simpanRelated').onclick = editRelated;
		var idDokumen = document.getElementById('dokumen').textContent;
		var http = new XMLHttpRequest();
	    http.open("POST", "<?php echo site_url('/web/document/getRelatedDocument');?>", true);
	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    var param = "id_dokumen=" + idDokumen;
	    http.send(param);
	    http.onload = function() {
	    	var response = http.responseText;
	    	// console.log(response);
	    	var related = JSON.parse(response);
	    	// console.log(related);
	    	var size = related.length;
	    	document.getElementById('tabelRelated').innerHTML = "";
	    	var inner = "<tr><th>Title</th><th>Type</th><th>Workunit</th><th>Preview</th><th>Related?</th></tr>";
	    	$("#data").remove();
    		inner += "<tbody id='data'>";
	    	for(var i=0;i<size;i++){
	    		// console.log(related[$i].DOCUMENT1+' dan '+idDokumen);
	    		if(related[i].DOCUMENT1 == idDokumen){
		    		var id = related[i].DOCUMENT2;
		    		related_terpilih.push(id);
		    		var title = related[i].TITLE;
		    		inner += "<tr>";
		    		inner += "<td style='display:none;'>" + id+ "</td>";
		    		inner += "<td style='display:none;'>true</td>";
		    		inner += "<td id='titleRelated' name='titleRelated[]'>" + title + "</td>";
		    		inner += "<td>" + related[i].TYPE_NAME + "</td>";
		    		inner += "<td>" + related[i].WORKUNIT + "</td>";
		    		inner += "<td><button class='btn btn-success' onClick='viewDocument(" + id +")'><i class='fa fa-eye' aria-hidden='true' style='float:middle;'></i></button></td>";
		    		inner += "<td><input type='checkbox' name='vehicle' value='Terkait' checked></td>";
		    		inner += "</tr>";
	    		}
	    	}
	    	inner += "</tbody>";
	    	document.getElementById('tabelRelated').innerHTML = inner;
		}
	}

	function baseName(str){
		var base = new String(str).substring(str.lastIndexOf('/') + 1); 
	    if(base.lastIndexOf(".") != -1)       
	        base = base.substring(0, base.lastIndexOf("."));
	   	return base;
	}

	function viewDocument(id){
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/viewdoc/');?>" + "/" + id, true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {
			var response = JSON.parse(http.responseText);
			// console.log(response);
			if (response['have_header']==1){
				var title = response['document']['title'];
			   	document.getElementById("titleDoc").textContent = title;
			   	var basename = baseName(response['file']['link']);
			   	var namaFile = response['file']['nama_file'];
			   	var username = response['username'];
			   	var date = response['date'];
			   	var ip 		 = response['ip'];
			   	var exts = namaFile.split(".");
			    var ext = exts[exts.length-1];
			    var divContent = document.getElementById("viewDocContent");
			    var inner = "";
			    if(ext == "pdf" || 1==1){
			    	inner = "<div style=\"z-index: 10; margin:15% 35%; opacity:0.2; position:absolute; text-align:center;  \"> <h1> Diakses oleh "+username+"</h1><br><h1>("+ip+")</h1><br><h1>Tanggal "+date+"</h1> </div>";
			    	// inner = "<iframe name='viewer' src ='<?php echo site_url("web/document/view/");?>" +  "/" + basename + ".pdf' width='100%' height='100%'allowfullscreen webkitallowfullscreen ></iframe>";
			    	inner += "<iframe name='viewer' src ='<?php echo site_url("web/document/view_watermark/");?>" +  "/" + basename + "' width='100%' height='100%'allowfullscreen webkitallowfullscreen ></iframe>";

			    }else if(ext == "jpg" || ext =="png" || ext == "jpeg" || ext=='gif' || ext=='bmp'){
			   		inner = "<img src='" + response['file']['link'] + "' alt='<?= $ext;?>' style='width:100%;height:100%;float:middle;'/>";
			   	}else if (ext=='mp4' || ext=='mpeg' || ext=='avi' || ext=='flv' || ext=='wmv' || ext=='mov'){
			   		mime = ext;
			   		if (ext=='wmv')
			   			mime = "x-ms-wmv";
			   		else if (ext=="flv")
			   			mime = "x-flv";
			   		inner = "<video width='100%' height='100%' controls controlsList='nodownload'><source src='"+response['file']['link']+"' type='video/"+mime+"'>Browser anda tidak support video.</video>";
			   		
			   	}else if(ext != "zip" || ext != "rar" || ext != "gz" || ext=='xmind'){
			   		inner = "<div style='width: 100%; height: 100%; position: inline;'>";
			   		// inner = inner + "<iframe src='https://docs.google.com/gview?url=<?php echo site_url("/web/thumb/files");?>"+'/' + basename + "." + ext + "&embedded=true' frameborder='0' scrolling='no' seamless style='width:100%;height:100%;float:middle;'></iframe>";
			   		inner = inner + "<iframe src='https://view.officeapps.live.com/op/view.aspx?src=<?php echo site_url("/web/thumb/files");?>"+'/' + basename + "." + ext + "&embedded=true' frameborder='0' scrolling='no' seamless style='width:100%;height:100%;float:middle;'></iframe>";
		    		// inner = inner + "<div style='width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;'>&nbsp;</div>";
		    		inner = inner + "<div style='width: 98%;height: 6%;position: absolute;right: 1%;left:1%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>";
		    		
		    	}
		    	divContent.innerHTML = inner;
			    openNav();
			} else {
				alert('Sorry, this document is still on converting process, please refresh in 15 mins.');
			}
		}
	}
	
	function View() {
		var id = document.getElementById('dokumen').textContent;
		if(id == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			//window.location.href = "<?php //echo base_url('/web/document/viewdoc') ?>/"+id;
			viewDocument(id);
		}
	};
	
	function Revision(){
		var idDokumen = document.getElementById('dokumen').textContent;
		var title = document.getElementById('judul_file').textContent;
		document.getElementById('RevisionTitle').textContent = "Revision - " + title;
		if(idDokumen == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			$("#myModal").modal('show');
			var table = document.getElementById('listfile');
			var rowSize = table.rows.length;
			for(var i=0;i<rowSize;i++){
				table.deleteRow(0);
			}
			var http = new XMLHttpRequest();
		    http.open("POST", "<?php echo site_url('/web/document/getPrevVersion');?>", true);
		    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    var param = "idDokumen=" + idDokumen;
		    http.send(param);
		    http.onload = function() {
		    	// alert(http.responseText);
		    	var documents = JSON.parse(http.responseText);
		    	if(documents[0].namafile == "kosong"){
		    		str = "<tr style='padding:5px'><td colspan=2 style='background-color:#D3D3D3;color:#000'><b>No Documents</b></td></tr>";
					table.innerHTML += str;
		    	}else{
		    		var str = "";
		    		for(var obj in documents){
		    			var note = documents[obj].update_note;
		    			if(note  == null)
		    				note = "-";
						str += "<tr><td><h5><b>" + documents[obj].document + "</b> (Versi "+ documents[obj].revision +")</h5>Note: " + note +"</td>";
						str += "<td><button class='btn btn-primary' style='float:right;width:100%;' onClick='rollback("+ documents[obj].id + ")'>Rollback</button><br><br>";
						str += "<button class='btn btn-info' style='float:right;width:100%;' onClick='DownloadRevision("+ documents[obj].id + ")'>Download</button></td></tr>";
						str += "<tr><td colspan=2>Last updated : <b>" + documents[obj].updated +"</b></td></tr>";
		    		}
		    		table.innerHTML += str;
		    	}
		    }
		}
	}

	function rollback(id){
		var idDokumen = document.getElementById('dokumen').textContent;
		var http = new XMLHttpRequest();
	    http.open("POST", "<?php echo site_url('/web/document/rollbackDocument');?>", true);
	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    var param = "idDokumen=" + id + "&asalid=" + idDokumen;
	    http.send(param);
	    http.onload = function() {
	    	document.getElementById('closeRevision').click();
	    	location.reload();
	    }
	}

	function openShareBox(){
		var id = document.getElementById('dokumen').textContent;
		var title = document.getElementById('judul_file').textContent;
		document.getElementById('LinkTitle').textContent = "Buat Link Share Document - " + title;
		if(id == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			var http = new XMLHttpRequest();
			http.open("POST", "<?php echo site_url('/web/share/checkLink');?>", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			var param = "idDocument=" + id;
			http.send(param);
			http.onload = function() {
				var response = http.responseText;
				if(response == "true"){ // Jika sudah ada link langsung dimunculkan pop up link
					createLink();
				}else{
					$("#ShareBox").modal('show');
				}
			}
		}
	}

	function openLastEvaluation(){
		var idDokumen = document.getElementById('dokumen').textContent;
		var title = document.getElementById('judul_file').textContent;
		if(idDokumen == "0"){
			alert("Anda belum memilih dokumen");
		}else{
			$("#last_evaluation_modal").modal('show');
			var table = document.getElementById('last_evaluation_list');
			var rowSize = table.rows.length;
			for(var i=0;i<rowSize;i++){
				table.deleteRow(0);
			}
			var http = new XMLHttpRequest();
		    http.open("POST", "<?php echo site_url('/web/document/ajaxGetLastEvaluation');?>", true);
		    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    var param = "idDokumen=" + idDokumen;
		    http.send(param);
		    http.onload = function() {
		    	// alert(http.responseText);
		    	var evaluation = JSON.parse(http.responseText);
		    	if(evaluation == null){
		    		str = "<tr style='padding:5px'><td colspan=2 style='background-color:#D3D3D3;color:#000'><b>This Document is not yet evaluated.</b></td></tr>";
					table.innerHTML += str;
		    	}else{
		    		var str = "";
	    			var comment = evaluation.comment;
	    			if(comment  == null)
	    				comment = "-";
	    			if (evaluation.status=='R'){
	    				status = "<b style='color: #FFB300'>Revise</b>";
	    			} else if (evaluation.status=='C') {
	    				status = "<b style='color:red'>Withdraw</b>";
	    			} else if (evaluation.status=='D'){
	    				status = "<b style='color: #00B65C'>Relevant</b>";
	    			} else {
	    				status = '<b>Belum Selesai Evaluasi</b>';
	    			}
	    			str +="<tr><td style='width:10%'>Drafter</td><td><b>"+evaluation.drafter_name+"</b></td></tr>";
	    			str +="<tr><td style='width:10%'>Creator</td><td><b>"+evaluation.creator_name+"</b></td></tr>";
	    			str +="<tr><td style='width:10%'>Approved by Creator</td><td><b>"+evaluation.creator_date+"</b></td></tr>";
	    			if (evaluation.approver_name!=null){
	    				str +="<tr><td style='width:10%'>Approver</td><td><b>"+evaluation.approver_name+"</b></td></tr>";
	    				str +="<tr><td style='width:10%'>Approved by Approver</td><td><b>"+evaluation.approver_date+"</b></td></tr>";
	    			}

			        // var evaluation_no = 0;   
	    			for (no in evaluation.text){
	    				var is_ok = '';
	    				if (evaluation.text[no]==1){
	    					is_ok = "<b style='color:green'>YES</b>";
	    				} else {	
	    					is_ok = "<b style='color:red'>NO</b>";
	    				}
	    				str += "<tr><td style='border:1px solid;width:5%'><label>"+no+"</label></td>";
	    				str += "<td style='border:1px solid'><label>"+evaluation.forms[no-1].text+"</label></td>";
	    				str += "<td style='border:1px solid;width:5%'><label>"+is_ok+"</label></td></tr>";
	    			}
	    			str +="<tr><td style='width:10%'>Result</td><td><b>"+status+"</b></td></tr>";
	    			str +="<tr><td style='width:10%'>Comment</td><td><b>"+comment+"</b></td></tr>";
					// str += "<tr><td><h5>" + status + "</h5>Comment: " + comment +"</td></tr>";
					// str += "<tr><td><h5>" + status + "</h5>Comment: " + comment +"</td></tr>";
					// str += "<td><button class='btn btn-primary' style='float:right;width:100%;' onClick='rollback("+ documents[obj].id + ")'>Rollback</button><br><br>";
					// str += "<button class='btn btn-info' style='float:right;width:100%;' onClick='DownloadRevision("+ documents[obj].id + ")'>Download</button></td></tr>";
					// str += "<tr><td colspan=2>Last updated : <b>" + documents[obj].updated +"</b></td></tr>";
		    		table.innerHTML += str;
		    	}
		    }
		}
	}

	//Membuat share link untuk dokumen yang telah dipilih
	function createLink(){
		var modal = document.getElementById('myModal');
		if (event.target == modal) {
			modal.style.display = "none";
		}
		var id = document.getElementById('dokumen').textContent;
		Share(id);
		$("#AlertBox").modal('show');
	}

	function Share(param){
		postShare("<?php echo site_url('/web/share/createShareLink');?>",param);
	}

	function postShare(path, params, method) {
		var title = document.getElementById('judul_file').textContent;
	    var http = new XMLHttpRequest();
	    http.open("POST", path, true);
	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    var param = "idDocument=" + params; 
	    http.send(param);
	    http.onload = function() {
	    	var obj = JSON.parse(http.responseText);
			var kode = obj.kode;
			var isNew = obj.isNew;
			var expired = obj.expired;
			if(isNew){
				document.getElementById('Keterangan').innerHTML = "Link untuk dokumen <b>" + title + "</b> berhasil dibuat dan berlaku hingga <b>" + expired + "<b>";
				document.getElementById('Pesan').innerHTML = "<p style='word-break: break-all;'><?php echo site_url('web/share/doc');?>/"+ kode + "</p>";
				document.getElementById('Pesan').href = "<?php echo site_url('web/share/doc');?>/"  + kode;				
				document.getElementById('Pesan').target = "_blank";
			}else{
				document.getElementById('Keterangan').innerHTML = "Link untuk dokumen <b>"+ title + "</b> masih berlaku hingga <b>" + expired + "</b>";
				document.getElementById('Pesan').innerHTML = "<p style='word-break: break-all;'><?php echo site_url('web/share/doc');?>/"+ kode + "</p>";
				document.getElementById('Pesan').href = "<?php echo site_url('web/share/doc');?>/"+ kode;
				document.getElementById('Pesan').target = "_blank";
			}
	    }
	}

	var menuDisplayed = false;
	var menuBox = null;   
	jQuery(document).ready(function() {
		$(".clickable-row").click(function() {
			closeEdit();
			rowClick($(this));
		});
		// $(".check_all").change(function(){
		// 	var a= $(".check_all").val();
		// 	alert(a);
		// });
		$("#t1").delegate("tr", "contextmenu", function(e) {
			if($(this).data("url")!=null){
		   		// alert($(this).data("url").ukuran);
		   		var data = $(this).data("url");
		   		var left = arguments[0].clientX;
				var top = arguments[0].clientY;
                
				menuBox = window.document.querySelector(".menu");
				menuBox.style.left = left + "px";
				menuBox.style.top = top + "px";
				menuBox.style.display = "block";
		        arguments[0].preventDefault();
        		menuDisplayed = true;
        		rowClick($(this));
		   	}
		   	return false;
		});

		jQuery('.tabs .tab-links a').on('click', function(e)  {
	        var currentAttrValue = jQuery(this).attr('href');
	 		jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();
	 		jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
	 
	        e.preventDefault();
    	});
	});
	// window.addEventListener("click", function() {
 //        if(menuDisplayed == true){
	// 		menuBox.style.display = "none"; 
 //        }
 //        closeNav();
	// }, true);

	function rowClick(elements){
    	var val = elements.data("url");
    	console.log(val);
    	// var myunitkerja = <?php echo json_encode($variables['myunitkerja']) ?>;
    	// var testArray = val.unitkerja_id in myunitkerja;
    	// console.log(testArray);
    	// console.log(myunitkerja[50000026]);
    	// console.log();
    	var satuan = val.ukuran;
		// var ukuran = val.ukuran;
		// var ukuran =0;
		// var satuan = " bytes";
		// var temp = 0;
		// while(ukuran >= 1000){
		// 	ukuran= ukuran/1000;
		// 	temp++;
		// }
		// if(temp == 1){
		// 	satuan = " KB";
		// }else if(temp == 2){
		// 	satuan = " MB";
		// }else if(temp == 3){
		// 	satuan = " GB";
		// }
		// var intUkuran = parseInt(ukuran);
		// satuan = intUkuran + satuan;
		// document.getElementById('dokumen').textContent = val.id;
		// document.getElementById('judul_file').textContent = val.title;
		// document.getElementById('judul2').value = val.title;
		// document.getElementById('nama_file').textContent = val.namafile;
		// document.getElementById('ukuran_file').textContent = satuan;
		if (val.description==null)
			val.description = '-';
		var retension = val.retension;
		if (retension==null)
			retension='-';
		else
			retension=val.retension+' month';
		if (val.clausul==null)
			val.clausul='-';
		var type = ""; 
		if(val.filename!=null){
			if(val.filename.lastIndexOf(".") != -1)       
	        type = val.filename.substring(val.filename.lastIndexOf(".") + 1,val.filename.length);
		    if(type == "pdf")
				type = "pdf.png";
			else if(type == "doc" || type == "docx")
				type = "docx.png";
			else if(type == "ppt" || type == "pptx")
				type = "ppt.png";
			else if(type == "xls" || type == "xlsx")
				type = "xls.png";
			else if(type == "zip")
				type = "zip.png";
			else if(type == "rar")
				type = "rar.png";
			else if(type == "xmind")
				type = "xmind.png";
			else if(type == "mp4" || type == "wmv" || type == "mov" || type == "avi" || type == "flv" || type == "mpeg")
				type = "video.png";
			else
				type = "image.png";	
			document.getElementById('image_file').src = "<?php echo site_url("assets/web/images/");?>/" + type;
		}
		document.getElementById('dokumen').textContent = val.document_id;
		document.getElementById('access_eval').textContent = val.access_eval;
		document.getElementById('drafter_eval').textContent = <?php echo SessionManagerWeb::getUserID() ?>;

		document.getElementById('judul_file').textContent = val.title;
		document.getElementById('judul2').value = val.description;
		// document.getElementById('nama_file').textContent = val.filename;
		document.getElementById('document_workunit').textContent = val.unitkerja_name;
		if(val.name != null && val.name != ''){
			document.getElementById('drafter_name').textContent = val.name;	
		}
		else{
			document.getElementById('drafter_name').textContent = '-';	
		}
		if(val.creator_name != null && val.creator_name != ''){
			document.getElementById('creator_name').textContent = val.creator_name;
		}
		else{
			document.getElementById('creator_name').textContent = '-';
		}
		if(val.approver_name != null && val.approver_name != ''){
			document.getElementById('approver_name').textContent = val.approver_name;
		}
		else{
			document.getElementById('approver_name').textContent = '-';
		}
		document.getElementById('document_code').textContent = val.code;
		document.getElementById('document_revision').textContent = val.revision;
		if(val.prosesbisnis_name != null && val.prosesbisnis_name != ''){
			document.getElementById('document_businessprocess').textContent = val.prosesbisnis_name;
		}
		else{
			document.getElementById('document_businessprocess').textContent = '-';
		}
		
		document.getElementById('document_retension').textContent = retension;
		if(val.clausul != null && val.clausul != ''){
			document.getElementById('document_clausuls').textContent = val.clausul;	
		}
		else{
			document.getElementById('document_clausuls').textContent = '-';
		}
		
		if(val.requirement != null && val.requirement != ''){
			document.getElementById('document_requirements').textContent =  val.requirement;	
		}
		else{
			document.getElementById('document_requirements').textContent =  '-';
		}
		
		document.getElementById('ukuran_file').textContent = satuan;
		
		// document.getElementById('lokasi_file').textContent = val.kode_lokasi;
		document.getElementById('lokasi_file').textContent = 1;
		var data = [];
		// data.push(val.kode_lokasi);
		data.push(1);
		$("#lokasi").select2("val",data);
		
		// document.getElementById('klasifikasi_file').textContent = val.nama_klasifikasi;
		document.getElementById('klasifikasi_file').textContent = 'dummy_klasifikasi';
		// data = [];
		// data.push(val.nama_klasifikasi);
		// data.push('test');
		// $("#klasifikasi").select2("val",data);

		if (val.type_name==null)
			val.type_name='-';
		document.getElementById('jenis_file').textContent = val.type_name;
		data = [];
		data.push(val.type_name);
		$("#jenis").select2("val",data);

		var strTag = "";
		var len = val.tags.length;
		data = new Array();
		for(var i=0;i<len;i++){
			if(i>0)
				strTag += ", ";
			strTag += val.tags[i];
			data.push(val.tags[i]);
		}
		if (strTag=="")
			strTag = '-';
		document.getElementById('tags_file').textContent = strTag;
		$("#tags").select2("val",[data]);
		document.getElementById('deskripsi_file').textContent = val.description;
		document.getElementById('deskripsi').textContent = val.description;

		document.getElementById('waktu_update').textContent = val.updated_at;
		document.getElementById('waktu_create').textContent = val.created_at;

		var divDokumenTerkait = document.getElementById('listDokumenTerkaitDetail');
		while (divDokumenTerkait.firstChild) {
				divDokumenTerkait.removeChild(divDokumenTerkait.firstChild);
		    
		}

		var divDokumenFormTerkait = document.getElementById('listDokumenFormTerkaitDetail');
		while (divDokumenFormTerkait.firstChild) {
			divDokumenFormTerkait.removeChild(divDokumenFormTerkait.firstChild);
		    
		}

		if (val.type_id==5){
			$('#showListDokumenFormTerkaitDetail').addClass('hidden');
		} else {
			$('#showListDokumenFormTerkaitDetail').removeClass('hidden');
		}

		len = val.docRelated.length;
		relatedDocument = val.docRelated;
		for(var i=0;i<len;i++){
			var node = document.createElement("li");
			var a = document.createElement('a');
		    var textnode = document.createTextNode(val.docRelated[i].TITLE);
		    a.appendChild(textnode);
		    a.setAttribute('onclick',"viewDocument(" + val.docRelated[i].IDDOCUMENT + ")");
		    a.href = "#";
		    // "viewDocument('" + val.docRelated[i].iddocument + "')"
		    // a.href="viewDocument('" + val.docRelated[i].iddocument + "')";
		    //"<?php// echo site_url("web/document/viewdoc/");?>/" + val.document_id;
		    node.appendChild(a);
		    if (val.docRelated[i].IS_FORM==1 && val.type_id!=5){
				divDokumenFormTerkait.appendChild(node);
			} else {
				divDokumenTerkait.appendChild(node);
			}
		}
		if(val.docRelatedNonForm === false && val.type_id!=5){
			divDokumenTerkait.innerHTML+="-";
		}
		if(val.docRelatedForm === false && val.type_id!=5){
			divDokumenFormTerkait.innerHTML+="-";
		}
		// document.getElementById('qrcode').src = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=" + val.hash + "&choe=UTF-8";
		
		var role = "<?php echo $role;?>";
		// document.getElementById('iconPencil').style.display = "inline";
		// document.getElementById('iconPencil1').style.display = "inline";
		// document.getElementById('iconPencil2').style.display = "inline";
		// document.getElementById('iconPencil3').style.display = "inline";
		// document.getElementById('iconPencil4').style.display = "inline";
		// // document.getElementById('iconPencil5').style.display = "inline";
		// document.getElementById('iconPencil6').style.display = "inline";
	
		var table = document.getElementById('t1');
		var size = table.rows.length;
		for(var i=0;i<size;i++){
			if(i%2==0){
				$("#tr"+i).css('background-color','#fff');	
			}else{
				$("#tr"+i).css('background-color','#fff');	
			}				
		}
		elements.css('background-color','#c0d9f9');	
		var table = document.getElementById('tAktifitas');
		var rowSize = table.rows.length;
		for(var i=0;i<rowSize;i++){
			table.deleteRow(0);
		}
		var aktifitas = val.activity;
		// var aktifitas = 'test aktifitas';
		var tempRow = 0;
		// alert(JSON.stringify(aktifitas));
		for(var key in aktifitas){
			var row1 = table.insertRow(tempRow);
		    var cell1 = row1.insertCell(0);
		    // cell1.innerHTML = "<div class='site-photo site-photo-left'> <img class='img-circle' style='height:30px;width:30px; margin-top:5px;' src='" + "<?php// echo site_url('web/thumb/profile'); ?>/" + aktifitas[key].user_id + "'/><b> " + val.name + "</b></div>";

		    // class='site-photo site-photo-left'
		    cell1.style = "margin:0px;padding: 0px 0px 0px 8px;height: 60px;width: 50px;";
		    cell1.innerHTML = "<div class='site-photo site-photo-left'> <img style='height:30px;' src='" + aktifitas[key].photo + "'/>" + "</div>";
		    var cell2 = row1.insertCell(1);
		    cell2.style = "padding-left:0px";
		    cell2.innerHTML = "<div> <img style='height:30px;'/>" +'<b>'+  aktifitas[key].name +'</b>'+ '<span style="font-size:10px;display:block;position:relative;top:-8px">' + aktifitas[key].created_at + '</span>' + "</div>";

			tempRow++;
			var row2 = table.insertRow(tempRow);
			row2.style.borderBottom = "1px solid #eee";
			var cell3 = row2.insertCell(0);
			cell3.colSpan = 2;
			cell3.innerHTML = "<label style='font-size:12px;'>" + aktifitas[key].activity + "</label>";
			tempRow++;
		}

		var table = document.getElementById('tHistory');
		var rowSize = table.rows.length;
		for(var i=0;i<rowSize;i++){
			table.deleteRow(0);
		}
		var history = val.history;
		var tempRow = 0;
		for(var key in history){
			var row1 = table.insertRow(tempRow);
		    var cell1 = row1.insertCell(0);
		    cell1.style = "margin:0px;padding: 0px 0px 0px 8px;height: 60px;width: 50px;";
		    cell1.innerHTML = "<div class='site-photo site-photo-left'> <img style='height:30px;' src='" + history[key].photo + "'/>" + "</div>";
		    var cell2 = row1.insertCell(1);
		    cell2.style = "padding-left:0px";
		    cell2.innerHTML = "<div> <img style='height:30px;'/>" +'<b>'+  history[key].name +'</b>'+ '<span style="font-size:10px;display:block;position:relative;top:-8px">' + history[key].created_at + '</span>' + "</div>";

			tempRow++;
			var row2 = table.insertRow(tempRow);
			row2.style.borderBottom = "1px solid #eee";
			var cell3 = row2.insertCell(0);
			cell3.colSpan = 2;
			cell3.innerHTML = "<label style='font-size:12px;'>" + history[key].activity + "</label>";
			tempRow++;
		}

		document.getElementById('activeLihat').style.display="inline";
		document.getElementById('nonActiveLihat').style.display="none";
		var is_doccon_holding = 0;
		<?php
			if(SessionManagerWeb::isDocumentController() == true){
				?>
				if (<?= SessionManagerWeb::getCompanyUserLogin()  ?> == val.company) {
					document.getElementById('editBtn2').style.display="inline";
				}
				else{
					document.getElementById('editBtn2').style.display="none";
				}
				<?php
			}
			else{
				?>
				document.getElementById('editBtn2').style.display="none";
				<?php
			}
			if(SessionManagerWeb::isDocumentController() == true && SessionManagerWeb::getCompanyUserLogin() == '2000'){
			?>
				is_doccon_holding = 1;
			<?php
			}
			?>
		// document.getElementById('activeDownload').onclick='';

		if (val.is_allow_download == 1 || val.creator_id == <?php echo SessionManagerWeb::getUserID() ?> || is_doccon_holding == 1){
			document.getElementById('activeAskDownload').style.display="none";
			document.getElementById('nonActiveAskDownload').style.display="none";
			$('#activeDownload').removeClass("hidden");
			document.getElementById('activeDownload').style.display="inline";
			document.getElementById('nonActiveDownload').style.display="none";
			if(val.creator_id == <?php echo SessionManagerWeb::getUserID() ?> && is_doccon_holding == 1){
				document.getElementById('activeDownload').onclick='';
				$('#activeDownload').attr('onclick', 'Download_creator()');
				// $('#activeDownload').on("click", function(){ 
				// 	Download_creator();
				//  });
			}
			else if(val.creator_id == <?php echo SessionManagerWeb::getUserID() ?> && is_doccon_holding == 0){
				document.getElementById('activeDownload').onclick='';
				$('#activeDownload').attr('onclick', 'Download_creator()');
				// $('#activeDownload').on("click", function(){ 
				// 	Download_creator();
				//  });
			}
			else if(val.creator_id != <?php echo SessionManagerWeb::getUserID() ?> && is_doccon_holding == 1){
				document.getElementById('activeDownload').onclick='';
				$('#activeDownload').attr('onclick', 'Download()');
				// $('#activeDownload').on("click", function(){ 
				// 	Download();
				//  });
			}
			else{
				document.getElementById('activeDownload').onclick='';
				$('#activeDownload').attr('onclick', 'Download()');
				// $('#activeDownload').on("click", function(){ 
				// 	Download();
				//  });
			}
			// if(val.creator_id == <?php echo SessionManagerWeb::getUserID() ?>){
			// 	document.getElementById('activeDownload').onclick='Download_creator()';
			// 	// $('#activeDownload').on("click", function(){ 
			// 	// 	Download_creator();
			// 	//  });
			// }
			// else{
			// 	// document.getElementById('activeDownload').onclick='Download()';
			// 	// $('#activeDownload').on("click", function(){ 
			// 	// 	Download();
			// 	//  });
			// }	
		} else {
			document.getElementById('activeAskDownload').style.display="inline";
			document.getElementById('nonActiveAskDownload').style.display="none";
			document.getElementById('activeDownload').style.display="none";
			document.getElementById('nonActiveDownload').style.display="none";
			document.getElementById('activeDownload').onclick='';
			$('#activeDownload').attr('onclick', 'Download()');
			// $('#activeDownload').on("click", function(){ 
			// 	Download();
			//  });
			// document.getElementById('activeDownload').onclick='Download()';
		}

		if (val.reconvert==1){
			if(type == "docx.png" || type == "doc.png" || type == "ppt.png" ||type == "pptx.png"){
				document.getElementById('activeReconvert').style.display="inline";
				document.getElementById('nonActiveReconvert').style.display="none";
			}
			else{
				document.getElementById('activeReconvert').style.display="none";
				document.getElementById('nonActiveReconvert').style.display="inline";
			}
			
		}

		
		// document.getElementById('activeEdit').style.display="inline";
		// document.getElementById('nonActiveEdit').style.display="none";
		// document.getElementById('activeRevision').style.display="inline";
		// document.getElementById('nonActiveRevision').style.display="none";
		// document.getElementById('activeShare').style.display="inline";
		// document.getElementById('nonActiveShare').style.display="none";
		// document.getElementById('activeDelete').style.display="inline";
		// document.getElementById('nonActiveDelete').style.display="none";
		// document.getElementById('activeQR').style.display="inline";
		// document.getElementById('nonActiveQR').style.display="none";
		var is_allow = false;
		<?php 
		// if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()){ 
		if (SessionManagerWeb::isDocumentController()){ 
		?>
			is_allow = true;
		<?php } else { ?>
			is_allow = false;
		<?php } ?>
		document.getElementById('activeEvaluate').style.display="none";
		document.getElementById('nonactiveEvaluate').style.display="inline";
		
		if(val.type_id==1){
			document.getElementById('activeEvaluate').style.display="none";
			document.getElementById('nonactiveEvaluate').style.display="none";
		}
		else{
			if (val.creator_id==<?=SessionManagerWeb::getUserID()?> || is_allow || val.access_eval == 1){
				console.log('izinkan eval');
				if (val.status=='D' || (val.workflow_id!='2' || val.hasil_evaluasi=='D')){
					document.getElementById('activeEvaluate').style.display="inline";
					document.getElementById('nonactiveEvaluate').style.display="none";
				}
			}
		}
		
		<?php 
			// if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()){ 
		if (SessionManagerWeb::isDocumentController()){ 
		?>
			document.getElementById('activeLastEvaluation').style.display="inline";
			document.getElementById('nonActiveLastEvaluation').style.display="none";
			
			if(val.type_id==1){
				document.getElementById('activeLastEvaluation').style.display="none";
				document.getElementById('nonActiveLastEvaluation').style.display="none";
			}
			else{
				if (val.change_workunit!='O'){
					document.getElementById('activeChangeUnit').style.display="inline";
					document.getElementById('nonActiveChangeUnit').style.display="none";
				} else {
					document.getElementById('activeChangeUnit').style.display="none";
					document.getElementById('nonActiveChangeUnit').style.display="none";
				}
			}
			
		<?php } ?>

		if (val.type_id == 1) {
			document.getElementById('workunit_title').textContent = 'Company';
			document.getElementById('drafter_line').style.display="none";
			document.getElementById('creator_line').style.display="none";
			document.getElementById('approver_line').style.display="none";
			document.getElementById('businessprocess_line').style.display="none";
			document.getElementById('requirements_line').style.display="none";
			document.getElementById('clausuls_line').style.display="none";
			document.getElementById('record_line').style.display="none";
		}
		else{
			document.getElementById('workunit_title').textContent = 'Workunit';	
			document.getElementById('drafter_line').style.display="table-row";
			document.getElementById('creator_line').style.display="table-row";
			document.getElementById('approver_line').style.display="table-row";
			document.getElementById('businessprocess_line').style.display="table-row";
			document.getElementById('requirements_line').style.display="table-row";
			document.getElementById('clausuls_line').style.display="table-row";
			document.getElementById('record_line').style.display="table-row";
		}
    }

    $('#gambar').on('change', function() {
        $('#image_form').ajaxForm({
            target: '#images_preview',
            error: function(e) {
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    $('#file_upload').on('change', function() {
        $('#file_form').ajaxForm({
            target: '#files_preview',
            error: function(e) {
                //target: '#images_preview';
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });
	
    function kirim_pesan(){
        if ($('#kirim_status').attr('value')=='1') {
            if ($('#sticky').is(':checked')) {
                $('#stick_kirim').val('1');
            }
            $('#kirim_form').submit();
        }
    }

function upload(){
	document.getElementById("dropbox2").click();
}

function removeUploadedFile(showBox){
	// document.getElementById('alertForPerbaharui').style.display = "none";
	// document.getElementById('alertForNewDoc').style.display = "none";
	var http = new XMLHttpRequest();
	http.open("POST", "<?php echo site_url('/web/document/removeFileTemp');?>", true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	var param = "";
	http.send(param);
	http.onload = function() {
		if(currentFile!=null && dropzone2!=null){
			dropzone.removeFile(currentFile);	
			currentFile = null;
		}
		if(currentFile2!=null && dropzone2!=null){
		   	dropzone2.removeFile(currentFile2);	
		   	currentFile2 = null;
		}
		if(showBox)
			$("#UploadBox").modal('show');
	}	
}

function removeUploadedFileBm(showBox){
	// document.getElementById('alertForPerbaharui').style.display = "none";
	// document.getElementById('alertForNewDoc').style.display = "none";
	var http = new XMLHttpRequest();
	http.open("POST", "<?php echo site_url('/web/document/removeFileTemp');?>", true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	var param = "";
	http.send(param);
	http.onload = function() {
		if(currentFile_bm!=null && dropzone_bm!=null){
			dropzone_bm.removeFile(currentFile_bm);	
			currentFile_bm = null;
		}
		// if(showBox)
		// 	$("#UploadBox").modal('show');
	}	
}


function removeUploadedFileexcel(showBox){
	var http = new XMLHttpRequest();
	http.open("POST", "<?php echo site_url('/web/document/removeFileTempPDF');?>", true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	var param = "";
	http.send(param);
	http.onload = function() {
		if(currentFileexcel!=null && dropzoneexcel!=null){
			dropzoneexcel.removeFile(currentFileexcel);	
			currentFileexcel = null;
		}
		// if(showBox)
		// 	$("#UploadBox").modal('show');
	}	
}

var currentFile = null;
var currentFile2 = null;
var dropzone = null;
var isUploading = false;
var dropzone2 = null;
var isUploading2 = false;
Dropzone.options.dropbox = {
  addRemoveLinks: true,
  timeout:120000,
  maxFiles:1,
  maxFileSize:20,
  init: function() {
    this.on("addedfile", function(file) {
    	document.getElementById('alertForPerbaharui').style.display = "none";
    	document.getElementById('alertFileExceeds').style.display = "none";
    	console.log("Added");
		if (file.size>1024*1024*20){
			document.getElementById('alertFileExceeds').style.display = "inline";
			currentFile = null;
	    	removeUploadedFile(false);
	    	isUploading = false;
	    	dropzone.removeFile(file);
		} else {
			isUploading = true;
		}
    	
    });
    this.on("removedfile", function(file) {
    	if(file==currentFile){
    		currentFile=null;
    		removeUploadedFile(false);
    	}
	});
	this.on("canceled", function () {
		currentFile = null;
    	removeUploadedFile(false);
    });
    this.on("maxfilesexceeded", function(file) {
        // this.addFile(file);
        dropzone.removeFile(file);
        document.getElementById('alertForPerbaharui').style.display = "inline";
        document.getElementById('alertMessagePerbaharui').textContent = "Hanya boleh mengupload 1 file saja";
    });
    this.on("complete", function (file) {
      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        if(currentFile==null)
        	currentFile = file;
        var http = new XMLHttpRequest();
	  	http.open("POST", "<?php echo site_url('/web/document/checkSameDoc');?>", true);
	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    var param = "";
	    http.send(param);
	    http.onload = function() {
	    	isUploading = false;
      		if(http.responseText == "false"){
	    		document.getElementById('alertForPerbaharui').style.display = "inline";
	    		document.getElementById('alertMessagePerbaharui').textContent = "File sudah pernah diunggah!";
	    		dropzone.removeFile(currentFile);
	    		currentFile = null;
	    	}
	    }
      }
    });
    dropzone = this;
  }   
};
Dropzone.options.dropbox2 = {
  addRemoveLinks: true,
  timeout:120000,
  maxFiles:1,
  maxFileSize:20,
  init: function() {
    this.on("addedfile", function(file) {
      document.getElementById('alertForNewDoc').style.display = "none";
      document.getElementById('alertFileExceeds').style.display = "none";
      if (file.size>1024*1024*20){
	      	document.getElementById('alertFileExceeds').style.display = "inline";
	      	currentFile2 = null;
	    	removeUploadedFile(false);
	    	isUploading2 = false;
	    	dropzone2.removeFile(file);
      } else {
      	isUploading2 = true;
      }
    });
    this.on("removedfile", function(file) {
    	if(file==currentFile2){
    		currentFile2=null;
	    	removeUploadedFile(false);
    	}
	});
	this.on("canceled", function () {
		currentFile2 = null;
    	removeUploadedFile(false);
    });
    this.on("maxfilesexceeded", function(file) {
        // this.addFile(file);
        dropzone2.removeFile(file);
        document.getElementById('alertForNewDoc').style.display = "inline";
        document.getElementById('alertMessageNewDoc').textContent = "Hanya boleh mengupload 1 file saja";
    });
    this.on("complete", function (file) {
      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        if(currentFile2==null)
        	currentFile2 = file;
      	var http = new XMLHttpRequest();
	  	http.open("POST", "<?php echo site_url('/web/document/checkSameDoc');?>", true);
	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    var param = "";
	    http.send(param);
	    http.onload = function() {
	    	isUploading2 = false;
	    	if(http.responseText == "false"){
	    		document.getElementById('alertForNewDoc').style.display = "inline";
	    		document.getElementById('alertMessageNewDoc').textContent = "File sudah pernah diunggah!";
	    		dropzone2.removeFile(currentFile2);
	    		currentFile2 = null;
	    	} else {
	    		var filename = dropzone2.files[0].name;
	    		var exts = filename.split(".");
			    var ext = exts[exts.length-1];
			    if (ext=='xls' || ext=='xlsx'){
			    	$("#dropbox2").removeClass("col-md-12");
			    	$("#dropbox2").addClass("col-md-6");
			    	$("#dropboxexcel").removeClass("hidden");
			    	document.getElementById("dropbox2").style.padding = "7px 5%";
			    } else {
			    	$("#dropbox2").removeClass("col-md-6");
			    	$("#dropbox2").addClass("col-md-12");
			    	$("#dropboxexcel").addClass("hidden");
			    	document.getElementById("dropbox2").style.padding = "7px 25%";
			    }
	    	}
	    }
      }
    });
    dropzone2 = this;
  }   
};

// excel
var currentFileexcel = null;
var dropzoneexcel = null;
var isUploadingexcel = false;
Dropzone.options.dropboxexcel = {
  addRemoveLinks: true,
  timeout:120000,
  maxFiles:1,
  maxFileSize:20,
  init: function() {
    this.on("addedfile", function(file) {
      document.getElementById('alertExcelNotPDF').style.display = "none";
      document.getElementById('alertFileExceeds').style.display = "none";
      if (file.size>1024*1024*20){
	      	document.getElementById('alertFileExceeds').style.display = "inline";
	      	currentFileexcel = null;
	    	removeUploadedFileexcel(false);
	    	isUploadingexcel = false;
	    	dropzoneexcel.removeFile(file);
      } else {
      	isUploadingexcel = true;
      }
    });
    this.on("removedfile", function(file) {
    	if(file==currentFileexcel){
    		currentFileexcel=null;
	    	removeUploadedFileexcel(false);
    	}
	});
	this.on("canceled", function () {
		currentFileexcel = null;
    	removeUploadedFileexcel(false);
    });
    this.on("maxfilesexceeded", function(file) {
        // this.addFile(file);
        dropzoneexcel.removeFile(file);
        document.getElementById('alertExcelNotPDF').style.display = "inline";
    });
    this.on("complete", function (file) {
      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        if(currentFileexcel==null)
        	currentFileexcel = file;
      	isUploadingexcel = false;
		var filename = dropzoneexcel.files[0].name;
		var exts = filename.split(".");
	    var ext = exts[exts.length-1];
	    if (ext!='pdf'){
	    	document.getElementById('alertExcelNotPDF').style.display = "inline";
    		dropzoneexcel.removeFile(currentFileexcel);
    		currentFileexcel = null;
	    }

      	if (1==0){
      		var http = new XMLHttpRequest();
      		http.open("POST", "<?php echo site_url('/web/document/checkSameDoc');?>", true);
		    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    var param = "";
		    http.send(param);
		    http.onload = function() {
		    	isUploadingexcel = false;
		    	if(http.responseText == "false"){
		    		document.getElementById('alertForNewDoc').style.display = "inline";
		    		dropzoneexcel.removeFile(currentFileexcel);
		    		currentFileexcel = null;
		    	} else {
		    		var filename = dropzoneexcel.files[0].name;
		    		var exts = filename.split(".");
				    var ext = exts[exts.length-1];
				    if (ext!='pdf'){
				    	document.getElementById('alertExcelNotPDF').style.display = "inline";
			    		dropzoneexcel.removeFile(currentFileexcel);
			    		currentFileexcel = null;
				    }
		    	}
		    }
      	}
	  	
      }
    });
    dropzoneexcel = this;
  }   
};



// untuk board manual
var currentFile_bm = null;
var dropzone_bm = null;
var isUploading_bm = false;
Dropzone.options.dropboxbm = {
  addRemoveLinks: true,
  timeout:120000,
  maxFiles:1,
  maxFileSize:20,
  init: function() {
    this.on("addedfile", function(file) {
    	// document.getElementById('alertForPerbaharui_bm').style.display = "none";
    	document.getElementById('alertFileExceeds_bm').style.display = "none";
    	console.log("Added");
		if (file.size>1024*1024*20){
			document.getElementById('alertFileExceeds_bm').style.display = "inline";
			currentFile_bm = null;
	    	removeUploadedFileBm(false);
	    	isUploading_bm = false;
	    	dropzone_bm.removeFile(file);
		} else {
			isUploading_bm = true;
		}
    	
    });
    this.on("removedfile", function(file) {
    	if(file==currentFile_bm){
    		currentFile_bm=null;
    		removeUploadedFileBm(false);
    	}
	});
	this.on("canceled", function () {
		currentFile_bm = null;
    	removeUploadedFileBm(false);
    });
    this.on("maxfilesexceeded", function(file) {
        // this.addFile(file);
        dropzone_bm.removeFile(file);
        document.getElementById('alertForNewDoc_bm').style.display = "inline";
        document.getElementById('alertMessageNewDoc_bm').textContent = "Only 1 file allowed!";
    });
    this.on("complete", function (file) {
      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
        if(currentFile_bm==null)
        	currentFile_bm = file;
        var http = new XMLHttpRequest();
	  	http.open("POST", "<?php echo site_url('/web/document/checkSameDoc');?>", true);
	    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    var param = "";
	    http.send(param);
	    http.onload = function() {
	    	isUploading_bm = false;
      		if(http.responseText == "false"){
	    		document.getElementById('alertForNewDoc_bm').style.display = "inline";
	    		document.getElementById('alertMessageNewDoc_bm').textContent = "Already Uploaded this document or Document is not PDF!";
	    		dropzone_bm.removeFile(currentFile_bm);
	    		currentFile_bm = null;
	    	}
	    }
      }
    });
    dropzone_bm = this;
  }   
};

$("#mainContent").addClass('col-sm-12');
$("#mainContent").removeClass('col-sm-9');


$(document).ready(function() {
	$('#check_all').on('ifChecked', function(event){
		$('.checklist_download').iCheck('check'); 
	});
	$('#check_all').on('ifUnchecked', function(event){
		$('.checklist_download').iCheck('uncheck');
	});

	// $().on('ifChanged', function() {
	// 	alert($(this).val());
	// });

    $(".tag").select2({
  	    placeholder: 'Tag 1,Tag 2',
        minimumResultsForSearch: Infinity,
        tags: true
    });
   //  $("#jenisFilter").select2({
  	//     placeholder: 'Jenis Dokumen'
  	// });
  	// $("#lokasiFilter").select2({
  	//     placeholder: 'Lokasi Dokumen'
  	// });
  	$("#lokasiSearch").select2({
  	    placeholder: 'Lokasi Dokumen'
  	});
  	$("#lokasiIns").select2({
  	    placeholder: 'Lokasi Dokumen'
  	});
  	$("#proses_bisnis").select2({
  	    placeholder: 'Choose Business Process'
  	});
  	$("#jenisIns").select2({
  	    placeholder: 'Document Type'
  	});
  	$("#form_type").select2({
  	    placeholder: 'Choose Form Type'
  	});

  	$('#input_iso').select2MultiCheckboxes({
  		placeholder:'Requirement',
  		// multiple:true,
	   // templateSelection: function(selected, total) {
  		// 	if (selected.length==0){
  		// 		return 'Select Requirement';
  		// 	}
	   //  	return "Selected " + selected.length + " of " + total;
	   //  }
    });

  	// $("#input_iso").select2({
  	//     placeholder: 'Requirement',
  	//     closeOnSelect:false
  	// });
  	$(".input_iso_klausul").select2({
  	    placeholder: 'Klausul',
        minimumResultsForSearch: Infinity
    });
  	$("#myunitkerja").select2({
  	    placeholder: 'Choose your work unit'
  	});
  	$("#prosesbisnis").select2({
  	    placeholder: 'Choose Business Process'
  	});
  	$("#reviewer").select2({
  	    placeholder: 'Write Reviewer Name'
  	});
  	$("#type").select2({
  	    placeholder: 'Search type here...'
  	});
  	$("#status").select2({
  	    placeholder: 'Search status here...'
  	});
  	$("#unitkerja").select2({
  	    placeholder: 'Search workunit...'
  	});
  	$("#company_bm").select2({
  	    placeholder: 'Search Company...'
  	});
  	$("#unit_kerja_terkait").select2({
  	    placeholder: 'Write related work unit'
  	});
  	$("#company_header").select2({
	    placeholder: 'Choose company'
  	});
  	$("#company_access").select2({
	    placeholder: 'Choose company'
  	});
  	$("#unit_kerja_terkait_bm").select2({
  	    placeholder: 'Write related work unit'
  	});
  	$("#input_unitkerjaterkait").select2({
  	    placeholder: 'Write related work unit'
  	});
  	$("#creator_id").select2({
  	    placeholder: 'Choose creator'
  	});
  	$("#approver_id").select2({
  	    placeholder: 'Choose Approver'
  	});
  	// $("#creator_id").select2({
  	//     placeholder: 'Pilih Creator'
  	// });
  	$("#lokasi").select2({
  	    placeholder: 'Document Location'
  	});
  	$("#jenis").select2({
  	    placeholder: 'Document type'
  	});
  	if(<?php if($viewdoc)echo "true";else echo "false";?>){
    	var val = JSON.parse(<?php echo json_encode($document[0]);?>);
    	var len = val.length;
		var data = [];
		for(var i=0;i<len;i++){
			data.push(val.tags[i]);
		}
		$("#tagIns").select2("val",[data]);
    }
});

$("#pemilikFilter").autocomplete({
	source: "<?php echo site_url('web/document/allUser'); ?>",
	minLength: 1,
	select: function( event, ui ) {
	},
	open: function (event, ui) {
		$(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
		$(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
	},
	close: function (event, ui) {
	},
	messages: {
		noResults: '',
		results: function() {}
	}            
})
// .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
// 	return $( "<div>" )
// 	.data( "ui-autocomplete-item", item )
// 	.append( "<a style='margin-left:5px;'>"+ item.label+ "</a>" )
// 	.appendTo( ul );
// };
</script>
<style>
.select2-results__option .wrap:before{
    font-family:fontAwesome;
    color:#999;
    content:"\f096";
    width:25px;
    height:25px;
    padding-right: 10px;
    
}
.select2-results__option[aria-selected=true] .wrap:before{
    content:"\f14a";
}

</style>
<div class="row">
		<div class="col-md-12">
			<a href="<?= site_url('home/listtask/' . $workflow['id']) ?>" class="btn btn-success pull-right"><small class="glyphicon glyphicon-chevron-left"></small> Kembali</a>
		</div>
</div>

<hr>
		<i class="glyphicon glyphicon-time"></i> <?= xFormatDateInd($task['header']['created'], false, false,'-', true) ?> &nbsp; 
		<a href="<?= site_url('home/attachment/'. $task['id_task']) ?>" target="_blank"><img src="<?= base_assets() . 'images/noimage_pdf.jpg'?>" width="22"></a> &nbsp;<a href="<?= site_url('home/task/' . $task['id_task'])  ?>" class="btn btn-xs btn-primary" style="color:white">Detail</a>
		<?php if ($task['is_active']) { ?>
		<label class="label label-danger">Request Rejected</label>
		<?php } ?>
<br/>

<?php include (dirname ( __FILE__ ) . '/workflow_task_preview.php'); ?>

<div class="row"></div>
<br>
<span class="label label-primary">1</span> <?= $actors[$task['header']['id_user']] ?>
<?= $task['isi'] ?>

<!-- Detail Requestor -->
<table class="table">
	<!-- START : Requestor Detail -->
		<tr>
			<td colspan=2><h3 style="background-color:#ccc;padding:3px">Requestor Detail</h3></td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">NIK</td>
			<td><?php if ($edit) { ?><input type="text" name="nik" size="50"> <?php } else { echo $nik; }?> </td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">Nama</td>
			<td><?php if ($edit) { ?><input type="text" name="nama" size="50"><?php } else { echo $nama; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">Departemen</td>
			<td><?php if ($edit) { ?><input class="bg-warning" type="text" name="departemen" size="50"><?php } else { echo $departemen; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field" width="150">Posisi</td>
			<td><?php if ($edit) { ?><input class="bg-danger" type="text" name="posisi" size="50"><?php } else { echo $posisi; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field">Email</td>
			<td><?php if ($edit) { ?><input type="text" name="email" size="50"><?php } else { echo $email; } ?></td>
		</tr>
	<!-- ENDOF : Requestor Detail -->
	<!-- START : User Access Detail -->
		<tr>
<td colspan=2><h3 style="background-color:#ccc;padding:3px">User Access Detail</h3></td>
		</tr>
		<tr>
			<td class="e-left-field">Type of Request</td>
			<td><?php if ($edit) { ?>
				<table>
					<tr><td><input type="radio" name="jenis" value="creation" checked></td><td>Creation &nbsp; &nbsp;</td>
					<td><input type="radio" name="jenis" value="termination"></td><td>Termination</td>
					</tr>
				</table>
<?php } else { echo ucwords($jenis); } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field">Computer Name</td>
			<td><?php if ($edit) { ?><input type="text" name="computer_name" size="50"><?php } else { echo $computer_name; } ?></td>
		</tr>
		<tr>
			<td class="e-left-field">Proposed Email Type</td>
			<td><?php if ($edit) { ?>
				<table>
					<tr><td><input type="radio" name="email_type" value="group"></td><td>Group &nbsp; &nbsp;</td>
					<td><input type="radio" name="email_type" value="personal" checked=""></td><td>Personal</td>
					</tr>
				</table>
<?php } else { echo ucwords($email_type); } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field">Proposed E-mail Name</td>
			<td><?php if ($edit) { ?>
				<input type="text" name="email_name" size="50">
				<select name="email_domain">
					<option value="@tds.com">@tds.com</option>
					<option value="@wonokoyo.co.id">@wonokoyo.co.id</option>
					<option value="@wonokoyo-group.com">@wonokoyo-group.com</option>
					<option value="@wonokoyojayacorporindo.com">@wonokoyojayacorporindo.com</option>
					<option value="@feedtech.wjc">@feedtech.wjc</option>
					<option value="@mitraunggassejati.com">@mitraunggassejati.com</option>
					<option value="@kda.com">@kda.com</option>
					<option value="@wjk.co.id">@wjk.co.id</option>
					<option value="@gna.com">@gna.com</option>
					<option value="@gna.com">@tdp.com</option>
				</select>
<?php } else { echo $email_name . $email_domain; } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field">Validity Period</td>
			<td><?php if ($edit) { ?>
				<table>
					<tr>
						<td><input type="radio" name="validity_type" value="continuous" onclick="change_validity_type(this)" checked></td><td>Continuous &nbsp; &nbsp;</td>
					</tr>
					<tr>
						<td>
							<input type="radio" name="validity_type" value="temporary" onclick="change_validity_type(this)" ></td>
						<td>Temporary</td>
						<td>
							<input type="text" name="validity_start" size="50" placeholder="DARI [dd/mm/yyyy]" readonly>
						</td>
						<td>
							<input type="text" name="validity_end" size="50" placeholder="SAMPAI [dd/mm/yyyy]" readonly>
						</td>
					</tr>
					</tr>
				</table>
<?php } else { 
echo ucwords($validity_type); 
if ($validity_type == 'temporary')
  echo ' (' . $validity_start . '-' . $validity_end . ')';
} ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field" style="vertical-align:top">Description</td>
			<td><?php if ($edit) { ?>
				<textarea name="description" style="width:500px;height:100px"></textarea>
<?php } else { echo nl2br($description); } ?>
			</td>
		</tr>
		<tr>
			<td class="e-left-field" style="vertical-align:top">Reason</td>
			<td><?php if ($edit) { ?>
				<textarea name="reason" style="width:500px;height:100px"></textarea>
<?php } else { echo nl2br($reason); } ?>
			</td>
		</tr>
</table>


<hr>

<?php 
	$i=0;
	foreach ($list_task as $task) { 
		$i++;
	if ($task['id_node'] == 'node_1')
		continue;

	if ($task['is_active']) {
		foreach ($workflow['rule']['model']['nodeDataArray'] as $node)	{
			if ($node['key'] == $task['node_id']) {
				$bg = '#e0d328';
				$is_end = 0;
				if ($task['node_id'] == 'node_' . count($workflow['rule']['model']['nodeDataArray'])) {
					$str = '<h2 style="color:white">DONE</h2>';
					$bg = '#38a02c';
					$is_end = 1;
				}

				echo '<a id="' . $task['node_id'] . '"></a>';
				if (!$is_end)
					echo '<h1><i class="glyphicon glyphicon-arrow-down"></i></h1><br>	';
				echo '<div style="border:1px solid #999;padding:5px;background-color:'.$bg.'">';
				if (!$is_end)
					echo '<span class="label label-primary">'. $i .'</span> ' .$actors[$node['actor']];
				echo '<hr style="margin-top:5px;margin-bottom:5px">';
				echo $node['keterangan'];
				echo $str;
				echo $node['form'];
				if ($_SESSION[G_SESSION]['actor'] == $node['actor'] && $node['form']) {
					echo '<input type="hidden" name="id_task" value="'.$task['id'].'">';
					echo '<button class="btn btn-primary">Submit</button>';
				}
				else {
					echo '<script>
					$(document).ready(function() {
						$(\'input\').attr(\'disabled\', true);
						$(\'textarea\').attr(\'disabled\', true);
					});
					</script>';
				}
				echo '</div>';
			}
		}
	}
	elseif ($task['content']) { ?>
		<a id="<?= $task['id_node'] ?>"></a>
		<h1><i class="glyphicon glyphicon-arrow-down"></i></h1>
		<div style="border: 1px solid #ccc;background-color:#efefef;padding:5px">
		<span class="label label-primary"><?= $i ?></span>  <?= $actors[$task['header']['id_user']]  ?> <br>

		<br><br>
		<table class="table table-condensed" style="width:600px">
			<?php foreach ($task['content'] as $k=>$v) { ?>
			<tr>
				<td width="200"><?= ucwords($k) ?></td><td><?= $v ?></td>
			</tr>
			<?php } ?>
			<tr><td colspan="2"><i class="glyphicon glyphicon-time"></i> <?= xFormatDateInd($task['header']['created'], false, false,'-', true) ?></td></tr>
		</table>
		</div>
<?php }

} ?>

<br>
<a href="<?= site_url('home/listtask/' . $workflow['id']) ?>" class="btn btn-success"><small class="glyphicon glyphicon-chevron-left"></small> Kembali</a>


<script>			

	$(function () {
	  $('[data-toggle="popover"]').popover()
	})

var openPhotoSwipe = function(items) {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    var options = {
      	history: false,
      	focus: false,
		bgOpacity:0.7,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
		maxSpreadZoom : 2,
		getDoubleTapZoom: function(isMouseClick, item) {
			// isMouseClick          - true if mouse, false if double-tap
			// item                  - slide object that is zoomed, usually current
			// item.initialZoomLevel - initial scale ratio of image
			//                         e.g. if viewport is 700px and image is 1400px,
			//                              initialZoomLevel will be 0.5
			if(isMouseClick) {
				// is mouse click on image or zoom icon
				// zoom to original
				return 1.5;
				// e.g. for 1400px image:
				// 0.5 - zooms to 700px
				// 2   - zooms to 2800px

			} else {
				// is double-tap
				// zoom to original if initial zoom is less than 0.7x,
				// otherwise to 1.5x, to make sure that double-tap gesture always zooms image
				return item.initialZoomLevel < 0.7 ? 1 : 1.5;
			}
		}		
    };
    
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};
</script>

<link rel="stylesheet" href="<?= base_assets() ?>css/photoswipe.css"> 
<link rel="stylesheet" href="<?= base_assets() ?>css/photoswipeskin/default-skin.css"> 
<script src="<?= base_assets() ?>js/photoswipe.min.js"></script> 
<script src="<?= base_assets() ?>js/photoswipe-ui-default.min.js"></script> 

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>

    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Tutup (Esc)"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
	/**
	 * Fungsi change_validity_type
	 * @param  eval = objek radio button
	 * -> Disable / Enable Textbox tanggal bila validity type berubah
	 */
	function change_validity_type(eval){
		var validity_type = eval.value;
		if (validity_type =='continuous'){
			$("input[name='validity_start']").attr('readonly', 'false');
			$("input[name='validity_end']").attr('readonly', 'false');
		}else{
			$("input[name='validity_start']").removeAttr('readonly');
			$("input[name='validity_end']").removeAttr('readonly');
		}
	}
</script>


<style>
	.e-left-field{
		text-align: left;
		
	}
</style>
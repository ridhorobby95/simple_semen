<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">Clause</th>
                                        <th class="text-center" style="width:15%">Code</th>
                                        <th class="text-center" style="width:15%">Action</th>
                                    </tr>
                                </thead>
                               <tbody>
                                    <?php
                                    foreach ($data['klausul'] as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                           
                                            <td class="text-center">
                                                <?= $v['klausul'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $v['code'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                echo anchor('web/document_iso/formEditClause/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                // echo anchor('web/document/edit/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip"  title="Hapus"');
                                                ?>
                                                <a href="javascript:hapus_clause(<?php echo $v['id'] ?>,'<?php echo $v['klausul'] ?>',<?php echo $data['id'] ?>)" class="text-danger" title="Delete Document ISO">
                                                    <i class="fa fa-trash"></i>
                                                </a> 
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
           <!--  <div class="row">
                <div class="col-sm-4 col-md-4" style="position:relative;left:1.5%">
                    <input type="text" name="iso_name" id="iso_name" placeholder="ISO Name" class="form-control" style="width:300px" required>
                </div>
                <div class="col-sm-2 col-md-2" style="position:relative;left:1.5%">
                    <input type="text" name="iso_code" id="iso_code" placeholder="ISO Code" class="form-control" style="width:150px;display:inline" required>
                </div>
                <div class="col-sm-4 col-md-4" style="position:relative;left:5%;bottom:1%;padding-bottom: 10px">
                    <span class="btn btn-sm btn-primary" id="btn_tambah_iso">Add Document ISO</span>
                </div>
            </div> -->
        </div>

    </div>
</div>
<script>

    function hapus_clause(id,name,id_iso) {
        if (confirm('are you sure want to delete clause "'+name+'" ?')){
            location.href = "<?= site_url('web/document_iso/deleteClause') ?>/"+id+"/"+id_iso;
        }
    }

    function goBack(id) {
        location.href = "<?php echo site_url($path . $class) ?>/edit/"+id;
    }

</script>
<style type="text/css">
    #rightSidebar {
        display: none!important;
    }
    #mainContent {
        margin-bottom: 50vh;
    }
    .WACRibbonPanel {
    	display: none!important;
    }
</style>
<script>
	var related_terpilih = [];
	var get_data_related = [];
	$("#mainContent").removeClass('col-sm-9');
	$("#mainContent").addClass('col-sm-12');
	$("#WACRibbonPanel").addClass('hidden');
</script>
<?php include (dirname ( __FILE__ ) . '/document_detail_preview.php'); ?>
<?php include('document_modal_viewdoc.php'); ?>

<!-- Variables -->
<label id="dokumen" style="display:none;">0</label>

<div class="row">
	<!-- 	<a id="activeLihat" onClick="viewDocument(<?= $data['document_id'] ?>)" class="btn btn-sm btn-secondary" style="margin:0px"><i class="fa fa-eye" aria-hidden="true"></i> Lihat Dokumen 
		</a> -->
	<iframe id="frame_download" style="display:none;"></iframe>
	<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad hidden" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
	    <i class="fa fa-eye" aria-hidden="true"></i>
	    <!-- <img src="<?php echo site_url("assets/web/images/icon/Group2661.png");?>" style="padding: 0px 10px 0px 0px"> -->
	    <span>Lihat Dokumen</span>
	</button>
	<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad hidden" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
	    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
	    <!-- <img src="<?php echo site_url("assets/web/images/icon/Group2661.png");?>" style="padding: 0px 10px 0px 0px"> -->
	    <span>Unduh</span>
	</button>

</div>
<div class="row"></div>
<br>
<div class="row col-md-9">
	<!-- <form>
		<textarea id="mytextarea"></textarea>
		<button type="submit">Submit</button>
	</form>
	<script type="text/javascript">
		var editor = textboxio.replace('#mytextarea');
	</script> -->
	<?php 
	$details = array();
	if ($data['workflow_urutan']!=1){
		// $details[1]=1;
		// include(dirname ( __FILE__ ) . '/document_detail_task.php');
	}
	$task_urutan = count($tasks_ordered);
	foreach ($tasks_ordered as $key => $task) {
		// Check delegasi
		$allowed_users = array();
		$allowed_users[] = $delegated['delegate'];
		if($task['urutan']<$data['workflow_urutan'] ){
			// include(dirname ( __FILE__ ) . '/document_detail_task.php'); 
	?>
			
		<?php } else
		if ($task['urutan']==$data['workflow_urutan'] and 
				// (SessionManagerWeb::isAdministrator() or 
					(
					($task['user']==Role::DOCUMENT_CONTROLLER and ($is_chief or SessionManagerWeb::getUserID()==$delegated['delegate']))
					or ($task['user']==Role::REVIEWER and in_array(SessionManagerWeb::getUserID(),$data['reviewer']))
					or ($task['user']==Role::DRAFTER and SessionManagerWeb::getUserID()==$data['user_id'] )
					or (($task['user']==Role::CREATOR or $data['status']=='R') and SessionManagerWeb::getUserID()==$data['creator_id'] && $task['urutan']!=0)
					or ($task['user']==Role::APPROVER and SessionManagerWeb::getUserID()==$data['approver_id'])
					or ($task['user']==Role::DRAFTER_REVISION and SessionManagerWeb::getUserID()==$drafter_evaluation)
					or ($task['urutan']==1 and in_array(SessionManagerWeb::getUserID(), $allowed_users))
				)
			) {
			$urutan = $data['workflow_urutan']+1;
			if ($data['workflow_urutan']==1 and $type['need_reviewer']=='0'){
				$urutan = $data['workflow_urutan']+2;
				if ($data['creator_id']==$data['user_id']){
					$urutan++;
				}
			}
			// echo "<pre>";print_r($urutan);die();
			$approved_status = "'".'P';
			if($data['workflow_id'] == 2 && $evaluation['result'] == 9){ // jika hasilnya relevan langsung ke
				$urutan = 6;
				$approved_status = "'".'D';

			}
			elseif($data['workflow_id'] == 2 && $data['workflow_urutan'] != 3 && $evaluation['result'] != 9 && $evaluation['result'] != 0 && $drafter_evaluation!=null){
				$urutan = 5;
			}
			elseif($data['workflow_id'] == 2 && $data['workflow_urutan'] != 3 && $evaluation['result'] != 9 && $evaluation['result'] != 0 && $drafter_evaluation==null){
				$urutan = 6;
				$approved_status = "'".'D';
			}
			elseif($data['workflow_id'] == 2 && $data['workflow_urutan'] == 3 && $evaluation['result'] != 9 && $evaluation['result'] != 0 && $drafter_evaluation==null){
				$approved_status = "'".'D';
				$urutan = 6;
			}
			elseif($data['workflow_id'] == 3  && $data['workflow_urutan'] == 1 && $drafter_evaluation != null && $type['need_reviewer'] == '0'){
				$approved_status = "'".'P';
				$urutan = 3;
			}
			elseif($data['workflow_id'] == 3  && $data['workflow_urutan'] == 1 && $drafter_evaluation != null && $type['need_reviewer'] != '0'){
				$approved_status = "'".'P';
				$urutan = 2;
			}
			if ($task['form']=='R') {
				// echo "masuk sini";die();
				$next=0;
				$reviewed = $counter['reviewed']+1;
				if ($reviewed == $counter['reviewer']){
					$next=1;
				}
			}
			
			if ($task['next_end']==1)
				$approved_status="'".'D';
			$approved_status .="'";

			include(dirname ( __FILE__ ) . '/document_detail_form.php');
		?>
			
		<?php 
		}

		$details[$task['urutan']]=1;
		$task_urutan--;
		 ?>
	<?php 
		
	} 
	if ($data['status']=='D' and $data['workflow_id']==2 and $already_show==0){
		$task_urutan = count($tasks_ordered);
		$task = array(
            'name' => 'Published',
            'form' => 'P'
        );
		include(dirname ( __FILE__ ) . '/document_detail_form.php');
	}
	if($data['workflow_urutan'] == 0 && $data['workflow_id'] == 2 && $drafter_evaluation == SessionManagerWeb::getUserID()){
		$task_urutan = $urutan_drafter;
		$urutan = $urutan_drafter;
		$approved_status="'".'P';
		$approved_status .="'";
		// $task = array(
  //           'name' => 'Evaluation Form',
  //           'form' => 'E',
  //           'user' => 'DE'
  //       );
        include(dirname ( __FILE__ ) . '/document_detail_form.php'); // line 2091
	}
	?>

</div>
<div class="row col-md-3 shadowPanel" style="margin-left: 2.5%;background-color: #fff;padding: 0px;">
    <div class="tabs " >
		<ul class="tab-links" style="border-bottom: 3px solid #E0E0E0">
    		<li class="active" ><a href="#tab1">Detail</a></li>
            <li ><a href="#tab3">History</a></li>
    	</ul>
        <div class="tab-content" style="margin: 10px; padding: 0px;">
	        <div id="tab1" class="tab active">
                <table width="100%">
                	<td colspan=3>
                        <h7 style="float:left; margin-right: 16px; font-size: 20px; font-weight: bold; color: #424242; ">Detail Actor</h7>  
                    </td>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Drafter</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">     <?php echo $user['name'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;"> Drafter Position</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden; padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">     <?php echo $user['subgroup_name'].' - '.$user['jabatan_name'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Drafter Work Unit</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">     <?php echo $user['unitkerja_name'] ?></label>
	                </tr>
	                <?php if (1==0) {?>
		                <tr>
		                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Company</td>
		                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden; padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">     <?php echo $user['company_name'] ?></label>
		                </tr>

		                <tr>
		                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Email</td>
		                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;word-wrap:break-word;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $user['email'] ?></label>
		                </tr>
	                <?php } ?>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Creator</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">     <?php echo $data['creator_name'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Approver</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">     <?php echo $data['approver_name'] ?></label>
	                </tr>


	                <td colspan=3>
                        <h7 style="float:left; margin-right: 16px; font-size: 20px; font-weight: bold; color: #424242;">Detail Document</h7> 
                    </td>
                    <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Title</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['title'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Code</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['code'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Document Type</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['type_name'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Revision</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['revision'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Work Unit</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['unitkerja_name'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Business Process</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['prosesbisnis_name'] ?></label>
	                </tr>
                    <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Requirements</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['requirement'] ?></label>
	                </tr>
                    <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Clausul</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo ($data['clausul']=="") ? '-' : $data['clausul'] ?></label>
	                </tr>
                    <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;"> Record Retension Period</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo ($data['retension']==NULL) ? '-': $data['retension'] ?></label>
	                </tr>
	                <?php if (1==0) { ?>
		                <tr>
		                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">File Name</td>
		                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['filename'] ?></label>
		                </tr>
	                <?php } ?>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Size</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo $data['ukuran'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Change Log</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;"><?php echo ($data['description']==NULL) ? '-' : $data['description'] ?></label>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Related Document</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">
	                    	<?php 
	                    		$val = 0;
	                    		foreach ($data['docRelated'] as $k_related => $v_related) {
	                    			if ($v_related['IS_FORM']==0 || ($v_related['IS_FORM']==1 && $data['type_id'] == 5  ) ){
	                    				echo '- '.$v_related['TITLE'].'<br>';
	                    				$val++;
	                    			}
	                    			
	                    		}
	                    		if ($val==0){
	                    			echo '-';
	                    		}
	                    	?></label>
	                    </td>
	                </tr>
	                <tr>
	                    <td width="45%" style="color:#424242; letter-spacing: 0.7px;font-size:11px;font-weight: bold;">Related Form</td>
	                    <td colspan=2 style="display: block;width: 90%;text-overflow: ellipsis;overflow: hidden;padding-bottom: 0px;"><label style="overflow:hidden;max-width:180px;font-size:12px;">
	                    	<?php 
		                    	$val = 0;
	                    		foreach ($data['docRelated'] as $k_related => $v_related) {
	                    			if ($v_related['IS_FORM']==1 && $data['type_id'] != 5){
	                    				echo '- '.$v_related['TITLE'].'<br />';
	                    				$val++;
	                    			}
	                    			
	                    		}
	                    		if ($val==0){
	                    			echo '-';
	                    		}
	                    	?></label>
	                    </td>
	                </tr>
	            </table>
            </div>
            <div id="tab3" class="tab">
                <h3>History</h3>
                <hr>
                <table id="tHistory" width="100%" style="font-size:12px;">
                	<?php foreach ($data['history'] as $history) {
                	?>
                		<tbody>
                			<tr>
                				<td style="margin: 0px; padding: 0px 0px 0px 8px; height: 60px; width: 50px;">
                					<div class="site-photo site-photo-left"> 
                						<img style="height:30px;" src="<?= $history['photo'] ?>">
                					</div>
                				</td>
                				<td style="padding-left: 0px;">
                					<div> 
                						<img style="height:30px;">
            							<b><?= $history['username'] ?></b>
            							<span style="font-size:10px;display:block;position:relative;top:-8px"><?= $history['created_at'] ?>
            							</span>
                					</div>
                				</td>
                			</tr>
                			<tr style="border-bottom: 1px solid rgb(238, 238, 238);">
                				<td colspan="2">
                					<label style="font-size:12px;"><?= nl2br($history['activity']) ?></label>
                				</td>
                			</tr>
                		</tbody>
                	<?php
                	} ?>
                </table>
            </div>
		</div>
        <br>
	</div>
</div>

<!-- NEW SCRIPT -->
<script type="text/javascript">
	
	function blobToFile(theBlob, fileName){
	    //A Blob() is almost a File() - it's just missing the two properties below which we will add
	    theBlob.lastModifiedDate = new Date();
	    theBlob.name = fileName;
	    return theBlob;
	}

  //   document.getElementById('btn_save_edited_doc').addEventListener('click', function(e) {
  //   	e.preventDefault();
  //   	var editor = textboxio.replace('#document_edit_textarea');
		// var content = '<!DOCTYPE html>' + editor.content.get();
		// var converted = htmlDocx.asBlob(content);
		// var converted_file = blobToFile(converted, "<?php//= $data['filename'] ?>");
		// dropzone.addFile(converted_file);
  //   });

    function save_doc_to_dropzone(){
    	var editor = textboxio.replace('#document_edit_textarea');
		var content = '<!DOCTYPE html>' + editor.content.get();
		var converted = htmlDocx.asBlob(content);
		var converted_file = blobToFile(converted, "<?= $data['filename'] ?>");
		dropzone.addFile(converted_file);
		return true;
    }

	function delegate(document_id, urutan, workflow_id){
		var user_id = document.getElementById('delegate').value;
		if (user_id=='' || user_id==null){
			alert('You Must choose someone to delegate');
		} else {
			$.ajax({
				url : '<?= site_url('web/document/ajaxSetDelegates') ?>',
				type: 'post',
				cache: false,
				data: {"document_id":document_id, "workflow_id":workflow_id,"workflow_urutan":urutan, "delegate":user_id},
				success: function(respon){
					if (respon != 'ERROR') {
						// $('#approve').click();
						window.location.replace("<?= site_url('web/document/detail') ?>/"+document_id);
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		}
	}

		function subordinate_delegate(document_id, urutan, workflow_id){
		var user_id = document.getElementById('subordinate_delegate').value;
		if (user_id=='' || user_id==null){
			alert('You Must choose someone to delegate');
		} else {
			$.ajax({
				url : '<?= site_url('web/document/ajaxSetDelegates') ?>',
				type: 'post',
				cache: false,
				data: {"document_id":document_id, "workflow_id":workflow_id,"workflow_urutan":urutan, "delegate":user_id},
				success: function(respon){
					if (respon != 'ERROR') {
						// $('#approve').click();
						window.location.replace("<?= site_url('web/document/detail') ?>/"+document_id);
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		}
	}

	function review(document_id, user_id, is_agree, next=0) {
		var comment = document.getElementById('comment_textarea').value;
		if (is_agree==0 && comment==''){
			alert("comment must be filled!");
		} else {
			$(".loadings").show();
			$.ajax({
				url : '<?= site_url('web/document/ajaxSetReview') ?>',
				type: 'post',
				cache: false,
				data: {"document_id":document_id,"review":comment, "document_reviewer":user_id, "is_agree":is_agree},
				success: function(respon){
					if (respon != 'ERROR') {
						if (next==1)
							$('#btn_next_stepper').click();
						else if (next==0)
							window.location.replace("<?= site_url('web/document/detail') ?>/"+document_id);
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		}
		
		
	}

</script>

<script>
	// untuk view
	function openNav() {
	    document.getElementById("viewDoc").style.width = "100%";
	}
	function closeNav() {
	    document.getElementById("viewDoc").style.width = "0%";
	}
	function baseName(str){
		var base = new String(str).substring(str.lastIndexOf('/') + 1); 
	    if(base.lastIndexOf(".") != -1)       
	        base = base.substring(0, base.lastIndexOf("."));
	   	return base;
	}
	var menuDisplayed = false;
	var menuBox = null;   
	jQuery(document).ready(function() {
		$(".clickable-row").click(function() {
			closeEdit();
			rowClick($(this));
		});
		$("#t1").delegate("tr", "contextmenu", function(e) {
			if($(this).data("url")!=null){
		   		// alert($(this).data("url").ukuran);
		   		var data = $(this).data("url");
		   		var left = arguments[0].clientX;
				var top = arguments[0].clientY;
                
				menuBox = window.document.querySelector(".menu");
				menuBox.style.left = left + "px";
				menuBox.style.top = top + "px";
				menuBox.style.display = "block";
		        arguments[0].preventDefault();
        		menuDisplayed = true;
        		rowClick($(this));
		   	}
		   	return false;
		});

		jQuery('.tabs .tab-links a').on('click', function(e)  {
	        var currentAttrValue = jQuery(this).attr('href');
	 		jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();
	 		jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
	 
	        e.preventDefault();
    	});
	});
	window.addEventListener("click", function() {
        if(menuDisplayed == true){
			menuBox.style.display = "none"; 
        }
        closeNav();
	}, true);

	function viewDocument(id){
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/view_real_docx/');?>" + "/" + id, true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {
			var response = JSON.parse(http.responseText);
			// if (response['have_header']==1){
				var title = response['document']['title'];
			   	document.getElementById("titleDoc").textContent = title;
			   	var basename = baseName(response['file']['real_encryption']);
			   	var namaFile = response['file']['nama_file'];
			   	var exts = namaFile.split(".");
			    var ext = exts[exts.length-1];
			    var divContent = document.getElementById("viewDocContent");
			    var inner = "";
			    if(ext == "pdf"){
			    	// inner = "<iframe name='viewer' src ='<?php echo site_url("web/document/view/");?>" +  "/" + basename + ".pdf' width='100%' height='100%'allowfullscreen webkitallowfullscreen ></iframe>";
			    	inner = "<iframe name='viewer' src ='<?php echo site_url("web/document/view_watermark/");?>" +  "/" + basename + "' width='100%' height='100%'allowfullscreen webkitallowfullscreen ></iframe>";
			    }else if(ext == "jpg" || ext =="png" || ext == "jpeg" || ext=='gif' || ext=='bmp'){
			   		inner = "<img src='" + response['file']['link'] + "' alt='<?= $ext;?>' style='width:100%;height:100%;float:middle;'/>";
			   	}else if (ext=='mp4' || ext=='mpeg' || ext=='avi' || ext=='flv' || ext=='wmv' || ext=='mov'){
			   		mime = ext;
			   		if (ext=='wmv')
			   			mime = "x-ms-wmv";
			   		else if (ext=="flv")
			   			mime = "x-flv";
			   		inner = "<video width='100%' height='100%' controls controlsList='nodownload'><source src='"+response['file']['link']+"' type='video/"+mime+"'>Browser anda tidak support video.</video>";
			   		
			   	}else if(ext != "zip" || ext != "rar" || ext != "gz" || ext=='xmind'){
			   		inner = "<div style='width: 100%; height: 100%; position: inline;'>";
			   		// inner = inner + "<iframe src='https://docs.google.com/gview?url=<?php echo site_url("/web/thumb/files");?>"+'/' + basename + "." + ext + "&embedded=true' frameborder='0' scrolling='no' seamless style='width:100%;height:100%;float:middle;'></iframe>";
			   		inner = inner + "<iframe src='https://view.officeapps.live.com/op/view.aspx?src=<?php echo site_url("/web/thumb/files");?>"+'/' + basename + "&embedded=true' frameborder='0' scrolling='no' seamless style='width:100%;height:100%;float:middle;'></iframe>";
			   		
		    		// inner = inner + "<div style='width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;'>&nbsp;</div>";
		    		inner = inner + "<div style='width: 98%;height: 6%;position: absolute;right: 1%;left:1%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>";
		    		
		    	}
		    	divContent.innerHTML = inner;
			    openNav();
			// } else {
			// 	alert('Sorry, this document is still on converting process, please refresh in 5 mins.');
			// }
		}
	}

	// Download 
	function Download(id) {
		document.getElementById('frame_download').src = "<?php echo base_url('/web/document/download') ?>?file="+id;
	};

	function delegasikan(id, urutan){
		var user_id = document.getElementById('delegate').value;
		$('#btn_delegate').addClass('hidden');
		reason = 'delegate_to';
		$.ajax({
			url : '<?= site_url('web/document/ajaxinsertdocumentform') ?>',
			type: 'post',
			cache: false,
			data: {"document_id":id,"reason":reason, "workflow_task":urutan, "delegated_user_id":user_id},
			success: function(respon){
				if (respon != 'ERROR') {
					// $('#approve').click();
					window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
				}
				else {
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
				}
			}
		});

	}

	// function set_urutan(id, urutan, status){
	// 	$.ajax({
	// 		url : '<?= site_url('web/document/ajaxseturutan') ?>',
	// 		type: 'post',
	// 		cache: false,
	// 		data: {"document_id":id,"workflow_urutan":urutan, 'status':status},
	// 		success: function(respon){
	// 			if (respon != 'ERROR') {
	// 				// if (urutan==-1)
	// 				if (status=='R'){
	// 					$('#form_review_atribut').submit();
	// 				}
	// 				if (status=='C'){
	// 					// window.location.replace("<?//= site_url('web/document/dashboard') ?>");
	// 					$("#submit_reject").click();
	// 				} else {
	// 					window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
	// 				}
					
	// 			}
	// 			else {
	// 				alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
	// 			}
	// 		}
	// 	});
	// }

	// function reject_document(){
	// 	$("#reject_modal").modal('show');
	// }

	// function revise_document(){
	// 	$("#revise_modal").modal('show');
	// }

	function update_documentbackup(id){
		// if (save_doc_to_dropzone()){
			// if(!isUploading && dropzone.files.length>0){
				// var doc_editor = document.getElementById('document_edit_textarea').value;
		    	var editor = textboxio.replace('#document_edit_textarea');
				var doc_editor =editor.content.get();
				var converted = htmlDocx.asBlob(doc_editor);
				var converted_file = blobToFile(converted, "<?= $data['filename'] ?>");
				dropzone.addFile(converted_file);
				dropzone.on('complete',function(){
					$.ajax({
						url : '<?= site_url('web/document/updateDocumentFiles') ?>',
						type: 'post',
						cache: false,
						data: {"document_id":id, 'document_text':doc_editor},
						success: function(respon){
							if (respon != 'ERROR') {
								
								$('#btn_next_stepper').click();
							}
							else {
								alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
							}
						}
					});
				});
				
				
		    // }
		    // else{
		    //     alert("Not Uploaded");
		    // }
		// }
	}


	function update_document(id){
		if (document.getElementById("form_create_document").className!='hidden'){
	    	var editor = textboxio.replace('#document_edit_textarea');
			var doc_editor =editor.content.get();
			if (doc_editor.length<4000) {
				var converted = htmlDocx.asBlob(doc_editor);
				var converted_file = blobToFile(converted, "<?= $data['filename'] ?>");
				dropzone.addFile(converted_file);
				dropzone.on('complete',function(){
					$.ajax({
						url : '<?= site_url('web/document/updateDocumentFiles') ?>',
						type: 'post',
						cache: false,
						data: {"document_id":id, 'document_text':doc_editor},
						success: function(respon){
							if (respon != 'ERROR') {
								
								$('#btn_next_stepper').click();
							}
							else {
								alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
							}
						}
					});
				});
			} else {
				alert('Ukuran file anda melebihi batas yang telah ditentukan. Silahkan kurangi isi file ini.');
			}
			
		} else {
			$.ajax({
				url : '<?= site_url('web/document/updateDocumentFiles') ?>',
				type: 'post',
				cache: false,
				data: {"document_id":id},
				success: function(respon){
					if (respon != 'ERROR') {
						$('#btn_next_stepper').click();
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		}
	}

	// function update_document(id){
	// 	var reason = document.getElementById('reason').value;
	// 	$.ajax({
	// 		url : '<?= site_url('web/document/ajaxsetreason') ?>',
	// 		type: 'post',
	// 		cache: false,
	// 		data: {"document_id":id,"reason":reason},
	// 		success: function(respon){
	// 			if (respon != 'ERROR') {
	// 				$('#approve').click();
	// 			}
	// 			else {
	// 				alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
	// 			}
	// 		}
	// 	});
	// }

	$(function () {
	  $('[data-toggle="popover"]').popover()
	});

var openPhotoSwipe = function(items) {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    var options = {
      	history: false,
      	focus: false,
		bgOpacity:0.7,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
		maxSpreadZoom : 2,
		getDoubleTapZoom: function(isMouseClick, item) {
			// isMouseClick          - true if mouse, false if double-tap
			// item                  - slide object that is zoomed, usually current
			// item.initialZoomLevel - initial scale ratio of image
			//                         e.g. if viewport is 700px and image is 1400px,
			//                              initialZoomLevel will be 0.5
			if(isMouseClick) {
				// is mouse click on image or zoom icon
				// zoom to original
				return 1.5;
				// e.g. for 1400px image:
				// 0.5 - zooms to 700px
				// 2   - zooms to 2800px

			} else {
				// is double-tap
				// zoom to original if initial zoom is less than 0.7x,
				// otherwise to 1.5x, to make sure that double-tap gesture always zooms image
				return item.initialZoomLevel < 0.7 ? 1 : 1.5;
			}
		}		
    };
    
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
};
</script>

<link rel="stylesheet" href="<?= base_assets() ?>css/photoswipe.css">
<link rel="stylesheet" href="<?= base_assets() ?>css/photoswipeskin/default-skin.css"> 
<script src="<?= base_assets() ?>js/photoswipe.min.js"></script> 
<script src="<?= base_assets() ?>js/photoswipe-ui-default.min.js"></script> 

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>

    <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Tutup (Esc)"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

<script type="text/javascript">
	/**
	 * Fungsi change_validity_type
	 * @param  eval = objek radio button
	 * -> Disable / Enable Textbox tanggal bila validity type berubah
	 */
	function change_validity_type(eval){
		var validity_type = eval.value;
		if (validity_type =='continuous'){
			$("input[name='validity_start']").attr('readonly', 'false');
			$("input[name='validity_end']").attr('readonly', 'false');
		}else{
			$("input[name='validity_start']").removeAttr('readonly');
			$("input[name='validity_end']").removeAttr('readonly');
		}
	}

	function goBack(){
		location.href = "<?php echo site_url($path.$class.'/dashboard') ?>";
	}
</script>


<style>
	.e-left-field{
		text-align: left;
		
	}
</style>


<style type="text/css">
   .ui-autocomplete {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 16777270;
    float: left;
    display: none;
    min-width: 160px;   
    padding: 4px 0;
    margin: 0 0 10px 25px;
    list-style: none;
    background-color: #ffffff;
    border-color: #ccc;
    border-color: rgba(0, 0, 0, 0.2);
    border-style: solid;
    border-width: 1px;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    *border-right-width: 2px;
    *border-bottom-width: 2px;
    cursor: pointer;
}

.ui-menu-item > a.ui-corner-all {
    display: block;
    padding: 3px 15px;
    clear: both;
    font-weight: normal;
    line-height: 18px;
    color: #555555;
    white-space: nowrap;
    text-decoration: none;
}

.ui-state-hover, .ui-state-active {
    color: #ffffff;
    text-decoration: none;
    background-color: #0088cc;
    border-radius: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    background-image: none;
}
</style>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>

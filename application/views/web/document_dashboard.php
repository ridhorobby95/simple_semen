<style type="text/css">
    #rightSidebar {
        display: none!important;
    }
    #mainContent {
/*        margin-bottom: 50vh;*/
    }
</style>

<?php include('_document_modal.php'); ?>
<?php include('document_modal_upload.php'); ?>
<?php include('document_modal_related.php'); ?>
<?php include('document_modal_viewdoc.php'); ?>

<?php include('_document_js.php'); ?>


<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="row">
			<div class="col-xs-6 col-sm-6" style="padding: 15px 10px 0;">
				<h3 class="titleDashboard">Dashboard</h3>
			</div>
			<div class="col-xs-6 col-sm-6 text-right hidden">
				<button style="text-align: right;" type="button" class="btn btn-sm btn-barudetail-question">
					<i class="fa fa-question Q_edit"></i>
					Help
				</button>
				<button  type="button" class="btn btn-sm btn-barudetail">
					<i class="fa fa-plus P_edit"></i>
					Tambah Dokumen
				</button>
			</div>
		</div>
	</div>
</div>
<div class="row	hidden">
	<div class="col-xs-12 col-md-8">
        	<div class="panel shadowPanel2" style="background-color: #f5f5f5;box-shadow: 0 0px 0px rgba(0,0,0,0.00);">
  				<!-- Default panel contents -->

			  	<table class="table tabedit" id="table2">
			    	<thead class="head-size bg_edit">
	                    <tr>
	                        <th class="text-center" style="width: 20%;">
	                        	<a href="#" class="thumbnail2">
      								<img src="<?php echo site_url("assets/web/img/Logo_Semen.png");?>" alt="...">
   								</a>
	                        </th>
	                        <th class="text-center" style="width: 20%;">
	                        	<a href="#" class="thumbnail2 deactivate-img">
      								<img src="<?php echo site_url("assets/web/img/SG_Logo.png");?>" alt="...">
   								</a>
	                        </th>
	                        <th class="text-center" style="width: 20%;">
	                        	<a href="#" class="thumbnail2 deactivate-img">
      								<img src="<?php echo site_url("assets/web/img/Padang_Semen.png");?>" class="img-circle" alt="...">
   								</a>
	                        </th>	                       
	                        <th class="text-center" style="width: 20%;">
	                        	<a href="#" class="thumbnail2 deactivate-img">
      								<img src="<?php echo site_url("assets/web/img/Tonas_Semen.png");?>" class="img-circle" alt="...">
   								</a>
	                        </th>
	                        <th class="text-center" style="width: 20%;">
	                        	<a href="#" class="thumbnail2 deactivate-img">
      								<img src="<?php echo site_url("assets/web/img/ThangLong_Semen.png");?>" class="img-circle" alt="...">
   								</a>
	                        </th>
	                        </tr> 
	                </thead>
	               
			  	</table>			
			</div>
	</div>
	<div class="col-xs-12 col-sm-4 col-md-4">
        	<div class="panel shadowPanel">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-none">
					<div class="row">
<!-- 		                <div class="col col-xs-8">
		                    <h3 class="panel-title titlePanel">Pilih Tombol</h3>
		                </div> -->
<!-- 		                <div class="col col-xs-4 text-right">
		                	<a class="btn btn-detailainnya2">
		                    	<i class=" fa fa-ellipsis-h" style="color: #9C27B0;"></i>
		                	</a>
		                </div> -->
		            </div>
				</div>
				<div class="panel-body">
					<div class="description-section">
					    <div class="container">
						 	<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
								<a class="mouse-btn-green" style="margin-right: 1%">
									<i class="fa fa-download" aria-hidden="true" style="font-size: 25px;">
										<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">Download
										</div>
									</i>
								</a>
								<a class="mouse-btn-green" style="margin-right: 1%">
									<i class="fa fa-download" aria-hidden="true" style="font-size: 25px;">
										<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">Challenge
										</div>
									</i>
								</a>							
								<button type="button" class="btn tambahan_btn hidden">Download</button>
								<button type="button" class="btn tambahan_btn hidden">Challenger</button>

					   		</div>
						</div>
					</div>
				</div>
			</div>

			  	<table class="table">
			  	</table>
	</div>
</div>


<!-- 	<div class="spacing-button"> -->
<!-- 		<button style="text-align: right;" type="button" class="btn btn-sm btn-barudetail-question">
			<i class="fa fa-question Q_edit"></i>
			Bantuan
		</button> -->
		<!-- 		<button  type="button" class="btn btn-sm btn-barudetail">
			<i class="fa fa-plus P_edit"></i>
			Tambah Dokumen
		</button> -->
		<div class="row">
			<div class="col-md-8" style="padding: 0px;">
				<div id="filterDiv" style="background-color:#ff3232;">
					<?php if (SessionManagerWeb::getRole() != Role::EKSTERNAL): ?>
					<a class="mousedrag01" style="" onClick="openNewUploadBox()">
						<i class="fa fa-file" aria-hidden="true" style="">
						</i><div class="d-title" style=" ">
								New Document
							</div>
					</a>
					<?php endif ?>
					
					<?php if (SessionManagerWeb::isDocumentController() and $myworkunit_company=='2000') { ?>
						<a class="mousedrag02" onClick="openNewUploadBoxBoardManual()" style="margin-left: 10px;">
							<i class="fa fa-file" aria-hidden="true" style="font-size: 25px;">
								<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">
									Add Pedoman Tata Laksana
								</div>
							</i>
						</a>
					<?php } ?>
					<!-- <a class="btn-outline-green" style="" onClick="openNewUploadBox()">
						<i class="fa fa-file-word-o" aria-hidden="true" style="font-size: 25px;">
							<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">
								Template Document 
							</div>
						</i>
					</a> -->
					<?php
					$isFilter = true;
					if (isset($_SESSION['document_search']))
						$isFiltered = 1;

					 if($isFilter and 1==0){ ?>
						<a data-toggle="collapse" class="mousedrag013" data-parent="#accordion" href="#collapse1" style="<?php if($isFiltered==0)echo "color:#56baed; margin-top: 0%;";else echo "color:#ff9b9b;"; ?>">
							<i class="fa fa-filter" aria-hidden="true" style="font-size: 25px;">
								<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">
									<?php if($isFiltered==0) echo "Filter Off";else echo "Filter On "?>
											
								</div>
							</i>
						</a>
					<?php } ?>
				</div>

	<div class="panel-group col-md-12" id="accordion"	style="padding-left: 0px;padding-right: 10px">
	    <div class="panel panel-default">	
	     <div id="collapse1" class="panel-collapse <?= (isset($_SESSION['document_search'])) ? 'in' : 'collapse' ?>" style="/*border: 1px solid #27ae60;*/ border-radius: 3px; box-shadow: 0px 5px 10px rgba(0,0,0,0.2);">
	        <div class="panel-body" style="padding: 0px 0px 0px 0px;">     
	        	<div class="tabs">
		    		<ul class="tab-links"  style="border-bottom: 2px solid #E0E0E0;">
		    			<li class="active"><a href="#tabShowBy">Show</a></li>
		        		<li><a href="#tabJenis">Type</a></li>
		        		<li <?php if(strcmp($lokasiFilter,"")!=0) echo "class='active'";?> style="display:none"><a href="#tabLokasi">Lokasi</a></li>
		        		<li class="hidden" <?php if(strcmp($tagFilter,"")!=0) echo "class='active'";?>><a href="#tabTag">Tags</a></li>
		        	</ul>
			        <div class="tab-content" style="max-height:250px;overflow-y: auto;">
			        	<div id="tabShowBy" class="tab active">
				        	<table style="width:100%;">
				        		<?php
				        			$ShowingJenis = 0;
				        			$showBy = array(
				        				"All" => "All",
				        				"Me" => "My Document",
				        				"Need Action" => "Need Action"
				        			);
				        			foreach($showBy as $key => $obj){
				        				if($ShowingJenis%2==0){
					        				echo "<tr>";
					        			}
					        			if(strcmp($key,$_SESSION['document_search']['show_by'])==0)
					        				echo "<td style='background-color:#E1F5FE;'><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterShow(".'"'.$key.'"'. ")'><b> ". $obj . "</b></a></td>";
					        			else
					        				echo "<td><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterShow(".'"'.$key.'"'. ")'><b> ". $obj . "</b></a></td>";
					        			if($ShowingJenis%2==1){
					        				echo "</tr>";
					        			}
					        			$ShowingJenis++;
					        		}
				        		?>
				        	</table>
				        </div>
				        <div id="tabJenis" class="tab">
				        	<table style="width:100%;">
				        		<?php
				        			$ShowingJenis = 0;
				        			foreach($variables['jenis'] as $obj){
				        				if($ShowingJenis%2==0){
					        				echo "<tr>";
					        			}
					        			if(strcmp($obj['id'],$_SESSION['document_search']['type'])==0)
					        				echo "<td style='background-color:#E1F5FE;'><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterJenis(".'"'.$obj['id'].'"'. ")'><b> ". $obj['type'] . "</b></a></td>";
					        			else
					        				echo "<td><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterJenis(".'"'.$obj['id'].'"'. ")'><b> ". $obj['type'] . "</b></a></td>";
					        			if($ShowingJenis%2==1){
					        				echo "</tr>";
					        			}
					        			$ShowingJenis++;
					        		}
				        		?>
				        	</table>
				        </div>
				    </div>
				</div>
				<div class="col-md-12" style="    background-color: #E1F5FE;" >
					<form method="post" name="filter_form" id="filter_form" action="<?= site_url('web/document/setSessionSearch').'/'.$method ?>" style="">
		        		<div class="upload-new" style="background-color:#E1F5FE;">
			        		<div style="margin-left:10px;margin-top:10px">
								<!-- <input type="hidden" id="numPage" name="numPage" value="<?php echo $page;?>">
					        	<input type="hidden" id="idOrganisasi" name="idOrganisasi" value="<?php echo $idOrganisasi;?>">
					        	<input type="hidden" id="lokasiFilter" name="lokasiFilter" value="<?php echo $lokasiFilter;?>">
					        	
					        	<input type="hidden" id="tagFilter" name="tagFilter" value="<?php echo $tagFilter;?>"> -->
					        	<input type="hidden" id="show_by" name="show_by" value="<?php echo $_SESSION['document_search']['show_by'];?>">
					        	<input type="hidden" id="type" name="type" value="<?php echo $_SESSION['document_search']['type'];?>">
					        	<div>
							        <div class="upload-new-input" style="background-color:#E1F5FE;">
				                        <i class="fa fa-search" ></i>
				                        <input type="text" id="title" name="title" value="<?php echo $_SESSION['document_search']['title'];?>" class="form-control input-sm" placeholder="Document Title" style="font-size:15px;display:inline;width:41%;margin-left:5px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
				                        <i class="fa fa-user" style="padding-left: 30px;"></i>
				                        <input type="text" id="user" name="user" value="<?php echo $_SESSION['document_search']['user'];?>" class="form-control input-sm" placeholder="Document Author" style="font-size:15px;display:inline;width:41%;margin-left:5px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
									</div>
								</div>
				            </div>
				        </div>
				        
				        <button type='submit' value='Filter' class="btn pull-right btn-filt">
				        	Search Document
				        	<!-- <label style="color:#fff;letter-spacing: 0.7px;">Cari Dokumen</label> -->
				        </button>
				        <?php if ($isFiltered) { ?>
				        	<a href="<?= site_url('web/document/removeDocumentFilter').'/'.$method; ?>" class="btn btn-clear-s pull-right" style="margin:0px 0px 15px;">Clear Filter
					        </a>
				        <?php } ?>
				    </form>
				</div>
	        </div>
	      </div>
	    </div>
	</div>

<div>
	<br>
	<br>
	<br>
	<div class="hidden-xs">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist" style="border: 0px solid #fff;">
			<li role="presentation" class=" tab-pane active">
				<a href="#need_action" aria-controls="need_action" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
					Need Action <span class="label label-danger"><?= $need_action ?></span>
				</a>
			</li>
			<li role="presentation">
				<a href="#my_document" aria-controls="my_document" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
					In Progress <span class="label label-danger"><?= $in_process ?></span>
				</a>
			</li>
			<li role="presentation">
				<a href="#download_permission" aria-controls="download_permission" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
					Download Req. <span class="label label-danger"><?= $download_counter ?></span>
				</a>
			</li>
			<li role="presentation" class="hidden">
				<a href="#workunit_change" aria-controls="workunit_change" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
					Unit Change <span class="label label-danger"><?= $workunit_change_counter ?></span>
				</a>
			</li>
			<li role="presentation">
				<a href="#rejected_document" aria-controls="rejected_document" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
					Rejected Doc <span class="label label-danger"><?= $rejected_document_counter ?></span>
				</a>
			</li>
		</ul>

	<!-- Tab panes -->
		<div class="tab-content" style="padding: 0px;">
			<div role="tabpanel" class="tab-pane active" id="need_action">
				<div class="col-xs-12 col-md-12" style="padding:0%;">
		        	<div class="panel shadowPanel" style="padding:0%;">
		  				<!-- Default panel contents -->
		  				<!-- untuk notifikasi pakai class ini "panel-color-blues-notif" -->
						<div class="panel-heading panel-color-blues" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
							<div class="row">
				                <div class="col-xs-5 col-sm-5">
				                    <h3 class="panel-title titlePanel">Document Need Action</h3>
				                </div>
				                <div class="col-xs-7 col-sm-7 text-right">
								<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <b style="color:red"><?= $need_action ?></b> documents that need to be checked</h7>	
									<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
									<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
				                </div>
				            </div>
						</div>
					  	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>
			                        <th style="width: 6%;"></th>
			                        <th style="width: 15%;">Title</th>
			                        <th style="width: 15%;">Status</th>
			                        <th style="width: 50%; padding-left: 0px;">Progress</th>
		            	            <th style="width: 10%;">Action</th>
			                    </tr> 
			                </thead>

							<tbody class=" text-decor ">
			                	<?php
		                		foreach ($data as $key => $value) {
		                			if ($value['is_mine']==1)
	                				continue;
			                	?>		
		                			<tr>
			                            <td align="center">
		                              	<?php 
		                              		switch ($value['workflow_id']) {
		                              			case 1:
		                              				$type = "submit-01.png";
		                              				$title = "Submission";
		                              				break;
		                              			
		                              			case 2:
		                              				$type = "evaluasi-01.png";
		                              				$title = "Evaluation";
		                              				break;
		                              			case 3:
		                              				$type = "revisi-01.png";
		                              				$title = "Revision";
		                              			break;
		                              			// default:
		                              			// 	$type = "submit-01.png";
		                              			// 	$title = "Submission";
		                              			// 	break;
		                              		}
		                              	?>
			                              	<img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" title="<?= $title ?>">
			                            </td>
			                            <?php 
			                            	$color="background-color:#FFEB3B;";
			                            	if ($value['status']=='C'){
			                            		$color = "background-color:#FF7043;";
			                            	}
			                            	$color="";
			                            ?>
			                            <td style="display: block;
			                            	width:135px;
										    overflow: hidden;
										    white-space: nowrap;
										    text-overflow: ellipsis;">
			                            	<div class="label" style="<?= $color ?> white-space: pre-wrap;padding-left: 0px;"><?= $value['title'] ?></div>
			                            </td>
			                            <td>
			                            	<div style="white-space: pre-wrap;padding-left: 0px;"><?= $value['user_action'] ?></div>
			                            </td>
			                            <td class="pad-style">
			                            	<div class="" style="position: absolute;width: 50%;">
			                            		<?= $value['status_step'] ?>             		
			                            	</div>
			                            	<div class="progress fhdisplay" style="width:<?=$value['total_progress'].'%'?>">
											  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $value['progress_percentage'].'%' ?> ">
											    <span class="sr-only"><?= $value['progress_percentage'].'%' ?> Complete</span>
											  </div>
											</div>
			                            </td>		                            
			                            <!-- <td>Tersisa 3 Hari Lagi</td> -->
			                            <td style="text-align: center;padding-left: 0px;">
			                            	<?php if ($value['ext']!='docx' && $value['ext']!='doc' and 1==0) { ?>
			                            		<a class="btnTable btn-lihatview" href="javascript:View(<?= $value['document_id'] ?>)" data-toggle="tooltip" data-placement="top" title="Preview">
			                            			View
			                            		</a>
			                            	<? } ?>
			                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
			                            			View
			                            		</a>
			                        		<a class="btnTable btn-detailainnya hidden" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>">
			                        			<!-- <i class="fa fa-ellipsis-h"></i> -->
			                        			Cek
			                        		</a>
			                        	</td>
			                      	</tr>
			                	<?php	}
			                	?>
			                	<?php 
			                	if (1==0){
		                		foreach ($downloads as $k_download => $v_download) {
		                			$download_title = " <a style='color:#424242' href='".site_url('web/document/download_detail').'/'.$v_download['document_id']."'>".$v_download['type_name']." <b style=''>\"".$v_download['name']."\"</b> </a> ";
			                	?>		
		                			<tr>
			                            <td align="center">
			                              	<?php 
	                              				$type = "delight-01.png";
	                              				$title = "Ask To Download";
			                              	?>
			                              	<img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" title="<?= $title ?>">
			                            </td>

			                            <?php 
			                            	// $color="background-color:#FFEB3B;";
			                            	$color="";
			                            ?>
			                            <td style="display: block;
			                            	width:135px;
										    overflow: hidden;
										    white-space: nowrap;
										    text-overflow: ellipsis;">
			                            	<div class="label" style="<?= $color ?> white-space: pre-wrap;padding-left: 0px;"><?= $download_title ?></div>
			                            </td>
			                            <td>
			                            	<div style="white-space: pre-wrap;padding-left: 0px;">Need Your Approval</div>
			                            </td>
			                            <td class="pad-style">
			                            	<div class="" style="position: absolute;width: 50%;">
			                            		<button type="button" class='btn btn-fix btn-circle btn-xl' data-toggle='tooltip' data-placement='top' title='Asking For Download'>
										        	<i class='fa fa-file'></i>
									            </button>  
									            <button type="button" class='btn btn-warning-prog btn-circle btn-xl' data-toggle='tooltip' data-placement='top' title='Approval Document Controller'>
										        	<i class='fa fa-thumbs-up'></i>
									            </button>    
									            <button type="button" class='btn btn-default-none btn-circle btn-xl' data-toggle='tooltip' data-placement='top' title='Allowed Download'>
										        	<i class='fa fa-download'></i>
									            </button>     		
			                            	</div>
			                            	<div class="progress fhdisplay" style="width:34.5%">
											  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:33.333333333333%">
											    <span class="sr-only"><?= $value['progress_percentage'].'%' ?> Complete</span>
											  </div>
											</div>
			                            </td>		                            
			                            <!-- <td>Tersisa 3 Hari Lagi</td> -->
			                            <td style="text-align: center;padding-left: 0px;">
			                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
		                            			View
		                            		</a>
			                        	</td>
			                      	</tr>
			                	<?php	} }
			                	?>
		                    </tbody>
					  	</table>
					  	<hr>
			        	<div class="panel-footer plusedit" style="padding-top: 0px;">
							<div class="row">
								<?php
									$hidden_before = 'hidden'; 
									$hidden_next = 'hidden'; 
									if ($page>0) { 
										$hidden_before = '';
									} 
									if ($next==1) {
										$hidden_next = '';
									}
								?>
				                <div class="col col-xs-6">
				                    <button type="button" onclick="viewDocumentLess()" class="btn btn-sm btn-barubefore <?= $hidden_before ?>"><b>Previous</b></button>
				                </div>
				                <div class="col col-xs-6 text-right">
				                    <button type="button" onclick="viewDocumentMore()" class="btn btn-sm btn-barunext <?= $hidden_next ?>"><b>Next</b></button>
				                </div>
				            </div>
				        </div>			
					</div>

				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="my_document">
				<div class="col-md-12" style="padding:0%;">
		        	<div class="panel shadowPanel" style="padding:0%;">
		  				<!-- Default panel contents -->
		  				<!-- untuk notifikasi pakai class ini "panel-color-blues-notif" -->
						<div class="panel-heading panel-color-blues" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
							<div class="row">
				                <div class="col col-xs-5">
				                    <h3 class="panel-title titlePanel">In Progress Documents</h3>
				                </div>
				                <div class="col col-xs-7 text-right">
								<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <b style="color:red"><?= $in_process ?></b> documents that still in progress</h7>	
									<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
									<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
				                </div>
				            </div>
						</div>
					  	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>
			                        <th style="width: 6%;"></th>
			                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
			                        <th style="width: 15%;">Title</th>
			                        <th style="width: 15%;">Status</th>
			                        <th style="width: 50%; padding-left: 0px;">Progress</th>
		            	            <th style="width: 10%;">Action</th>
			                    </tr> 
			                </thead>

							<tbody class=" text-decor ">
			                	<?php
			                		foreach ($data as $key => $value) {
			                			// echo '<pre>';
			                			// vaR_dump($data);
			                			// die();
			                			if ($value['is_mine']==0)
			                				continue;
			                	?>		
			                			<tr>
				                            <td align="center">

				                              	<!-- <i class="glyphicon glyphicon-file file-doc"></i> -->
				                              	<?php 
				         //                      		if($value['ext'] == "pdf")
													// 	$type = "pdf.png";
													// else if($value['ext'] == "doc" || $value['ext'] == "docx")
													// 	$type = "docx.png";
													// else if($value['ext'] == "ppt" || $value['ext'] == "pptx")
													// 	$type = "ppt.png";
													// else if($value['ext'] == "xls" || $value['ext'] == "xlsx")
													// 	$type = "xls.png";
					                              	switch ($value['workflow_id']) {
				                              			case 1:
				                              				$type = "submit-01.png";
				                              				$title = "Submission";
				                              				break;
				                              			
				                              			case 2:
				                              				$type = "evaluasi-01.png";
				                              				$title = "Evaluation";
				                              				break;
			                              				case 3:
				                              				$type = "revisi-01.png";
				                              				$title = "Revision";
				                              			break;
				                              			// default:
				                              			// 	$type = "submit-01.png";
				                              			// 	$title = "Submission";
				                              			// 	break;
				                              		}
				                              	?>
				                              	<img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" title="<?= $title ?>">
				                            </td>
				                            <!-- <td class="nowrapping"><?//= $value['code'] ?></td> -->
				                            <?php 
				                            	$color="background-color:#FFEB3B;";
				                            	if ($value['status']=='C'){
				                            		$color = "background-color:#FF7043;";
				                            	}
				                            	$color="";
				                            ?>
				                            <td style="display: block;
											    overflow: hidden;
											    width:135px;
											    white-space: nowrap;
											    text-overflow: ellipsis;">
				                            	<div class="label" style="<?= $color ?> white-space: pre-wrap;padding-left: 0px;"><?= $value['title'] ?></div>
				                            </td>
				                            <td>
				                            	<div style="white-space: pre-wrap;padding-left: 0px;"><?= $value['user_action'] ?></div>
				                            </td>
				                            <td class="pad-style">
				                            	<div class="" style="position: absolute;width: 50%;">
				                            		<?= $value['status_step'] ?>             		
				                            	</div>
				                            	<div class="progress fhdisplay" style="width:<?=$value['total_progress'].'%'?>">
												  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $value['progress_percentage'].'%' ?> ">
												    <span class="sr-only"><?= $value['progress_percentage'].'%' ?> Complete</span>
												  </div>
												</div>
				                            </td>		                            
				                            <!-- <td>Tersisa 3 Hari Lagi</td> -->
				                            <td style="text-align: center;padding-left: 0px;">
				                            	<?php if ($value['ext']!='docx' && $value['ext']!='doc' and 1==0) { ?>
				                            		<a class="btnTable btn-lihatview" href="javascript:View(<?= $value['document_id'] ?>)" data-toggle="tooltip" data-placement="top" title="Preview">
				                            			View
				                            		</a>
				                            	<? } ?>
				                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
				                            			View
				                            		</a>
				                        		<a class="btnTable btn-detailainnya hidden" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>">
				                        			<!-- <i class="fa fa-ellipsis-h"></i> -->
				                        			Cek
				                        		</a>
				                        	</td>
				                      	</tr>
			                	<?php	}
			                	?>
		                    </tbody>
					  	</table>
					  	<hr>
			        	<div class="panel-footer plusedit" style="padding-top: 0px;">
							<div class="row">
								<?php
									$hidden_before = 'hidden'; 
									$hidden_next = 'hidden'; 
									if ($page>0) { 
										$hidden_before = '';
									} 
									if ($next==1) {
										$hidden_next = '';
									}
								?>
				                <div class="col col-xs-6">
				                    <button type="button" onclick="viewDocumentLess()" class="btn btn-sm btn-barubefore <?= $hidden_before ?>"><b>Previous</b></button>
				                </div>
				                <div class="col col-xs-6 text-right">
				                    <button type="button" onclick="viewDocumentMore()" class="btn btn-sm btn-barunext <?= $hidden_next ?>"><b>Next</b></button>
				                </div>
				            </div>
				        </div>			
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="download_permission">
				<div class="col-md-12" style="padding:0%;">
		        	<div class="panel shadowPanel" style="padding:0%;">
		  				<!-- Default panel contents -->
		  				<!-- untuk notifikasi pakai class ini "panel-color-blues-notif" -->
						<div class="panel-heading panel-color-blues" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
							<div class="row">
				                <div class="col col-xs-5">
				                    <h3 class="panel-title titlePanel">Download Permissions</h3>
				                </div>
				                <!-- <div class="col col-xs-7 text-right">
								<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <b style="color:red"><?= $need_action ?></b> documents that need to be checked</h7>	
									<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
									<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
				                </div> -->
				            </div>
						</div>
					  	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>
									<th style="width: 10%;">
			                        	<div style="color: #fff">Icon</div>
			                        </th>
			                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
			                        <th style="width: 80%;">Title</th>
									<!--<th style="width: 15%;">Status</th>
			                        <th style="width: 50%; padding-left: 0px;">Progress</th> -->
		            	            <th style="width: 10%;">Action</th>
			                    </tr> 
			                </thead>

							<tbody class=" text-decor ">
			                	<?php 
			                		foreach ($downloads as $k_download => $v_download) {
			                			$download_title = "<a style='color:#424242' href='".site_url('web/document/download_detail').'/'.$v_download['document_id']."'>".$v_download['type_name']." <b style='color:#64DD17'>\"".$v_download['name']."\"</b> from <b style='color:#0091EA'>".$v_download['document_workunit']."</b> </a> "; ?>		
			                			<tr>
				                            <td align="center">
				                              	<?php 
		                              				$type = "delight-01.png";
		                              				$title = "Ask To Download";
				                              	?>
				                              	<img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" title="<?= $title ?>">
				                            </td>
				                            <td>
				                            	<div><?= $v_download['requestor'] ?></div>
				                            	<div><?= $v_download['requestor_workunit'] ?></div>
				                            	<div class="label" style="white-space: pre-wrap;padding-left: 0px;"><?= $download_title ?></div>
				                            	<div style="padding-left: 0px;">
				                            		<?php if ($v_download['is_allowed']==0) { 
				                            			$is_allowed = 0;
					                            		// if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()) { 
				                            			if (SessionManagerWeb::isDocumentController()) { 
							                            	$text = "Need Your Approval";
							                            	$is_allowed = 2;
						                            	} else { 
						                            		$text =	"Waiting Approval";
						                            	}  
						                            } else if ($v_download['is_allowed']==-1) {  
					                            		$text = "<b style='color:red'>Rejected</b> because \"".$v_download['reject_reason']."\"";
					                            		$is_allowed = -1;
					                            	} else {
					                            		$text = "Approved, Valid Until <b style='color:#64DD17'>".$v_download['valid_until'].'</b>';
					                            		$is_allowed = 1;
					                            	} ?>
					                            	<?= $text ?>
					                            </div>
				                            </td>
	<!-- 			                            <td>
				                            	<div><?= $v_download['requestor_workunit'] ?></div>
				                            </td> -->
	<!-- 			                            <td style="display: block;
				                            	width:135px;
											    overflow: hidden;
											    white-space: nowrap;
											    text-overflow: ellipsis;">
				                            	<div class="label" style="white-space: pre-wrap;padding-left: 0px;"><?= $download_title ?></div>
				                            </td> -->
				                            <!-- <td>
				                            	<div style="padding-left: 0px;">
				                            		<?php if ($v_download['is_allowed']==0) { 
				                            			$is_allowed = 0;
					                            		// if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()) { 
				                            			if (SessionManagerWeb::isDocumentController()) { 
							                            	$text = "Need Your Approval";
							                            	$is_allowed = 2;
						                            	} else { 
						                            		$text =	"Waiting Approval";
						                            	}  
						                            } else if ($v_download['is_allowed']==-1) {  
					                            		$text = "<b style='color:red'>Rejected</b> because \"".$v_download['reason']."\"";
					                            		$is_allowed = -1;
					                            	} else {
					                            		$text = "Approved";
					                            		$is_allowed = 1;
					                            	} ?>
					                            	<?= $text ?>
					                            </div>
				                            </td> -->	                            
				                            <td style="padding-left: 0px;">
				                            	
			                            		<?php if ($is_allowed==2) { ?>
				                            		<a class="btnTable btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$v_download['id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank" style="margin-right:10px">
				                            			View
				                            		</a>
					                            	<a class="btnTable btn-dashboard-success hidden" href="<?= site_url('web/document/approve_download/').'/'.$v_download['id'] ?>" data-toggle="tooltip" data-placement="top" title="Approve Download" style="background-color: #00B65C">
				                            			Approve
				                            		</a>
			                            		<?php } else if ($is_allowed==1) { ?>
			                            			<a class="btnTable btn-lihatview" href="<?= base_url('/web/document/download_watermark').'/'.$v_download['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="Download Document" target="_blank" >
				                            			Download
				                            		</a>
				                            	<?php } else if ($is_allowed==0) { ?>
					                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$v_download['id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank" style="margin-right:10px">
				                            			View
				                            		</a>
				                            	<?php } ?>	
				                        	</td>
				                      	</tr>
			                			<?php	
			                		} ?>
		                    </tbody>
					  	</table>
					  	<hr>		
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="workunit_change">
				<div class="col-md-12" style="padding:0%;">
		        	<div class="panel shadowPanel" style="padding:0%;">
		  				<!-- Default panel contents -->
						<div class="panel-heading panel-color-blues" id="notificationx" style="background-color:<?= ($workunit_change_counter>0) ? "#FFF" : "white"; ?>">
							<div class="row">
				                <div class="col col-xs-5">
				                    <h3 class="panel-title titlePanel">Workunit Change</h3>
				                </div>
				            </div>
						</div>
					  	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>
			                        <th style="width: 50%;">Document</th>
			                        <th style="width: 15%;">Creator</th>
			                        <th style="width: 15%;">Approver</th>
		            	            <th style="width: 15%;">Action</th>
			                    </tr> 
			                </thead>

							<tbody class=" text-decor ">
			                	<?php 
			                		foreach ($workunit_change as $k_workunit => $v_workunit) {
			                	?>		
			                			<tr>
				                            <td>
				                            	<div><?= $v_workunit['title'] ?></div>
				                            </td>
				                            <td>
				                            	<div><?= ($v_workunit['creator']) ? "<b style='color:red'>Need Action</b>" : "<b style='color:green'></b>" ?></div>
				                            </td>
				                            <td>
				                            	<div><?= ($v_workunit['approver']) ? "<b style='color:red'>Need Action</b>" : "<b style='color:green'>OK</b>" ?></div>
				                            </td>	                            
				                            <td style="padding-left: 0px;">
				                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/changeunit/').'/'.$v_workunit['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Changeunit Document" target="_blank" style="margin-right:10px">
			                            			View
			                            		</a>
				                        	</td>
				                      	</tr>
			                			<?php	
			                		} ?>
		                    </tbody>
					  	</table>
					  	<hr>		
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="rejected_document">
				<div class="col-md-12" style="padding:0%;">
		        	<div class="panel shadowPanel" style="padding:0%;">
		  				<!-- Default panel contents -->
		  				<!-- untuk notifikasi pakai class ini "panel-color-blues-notif" -->
						<div class="panel-heading panel-color-blues" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
							<div class="row">
				                <div class="col col-xs-5">
				                    <h3 class="panel-title titlePanel">Rejected Documents</h3>
				                </div>
				                <div class="col col-xs-7 text-right">
								<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <b style="color:red"><?= $rejected_document_workunit ?></b> documents that rejected</h7>	
									<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
				                </div>
				            </div>
						</div>
					  	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>
			                        <th style="width: 6%;"></th>
			                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
			                        <th style="width: 15%;">Title</th>
			                        <th style="width: 15%;">Status</th>
			                        <th style="width: 50%; padding-left: 0px;">Progress</th>
		            	            <th style="width: 10%;">Action</th>
			                    </tr> 
			                </thead>

							<tbody class=" text-decor ">
			                	<?php
			                		foreach ($rejected_documents as $key => $value) {
			                	?>		
			                			<tr>
				                            <td align="center">
				                              	<?php 
					                              	switch ($value['workflow_id']) {
				                              			case 1:
				                              				$type = "submit-01.png";
				                              				$title = "Submission";
				                              				break;
				                              			
				                              			case 2:
				                              				$type = "evaluasi-01.png";
				                              				$title = "Evaluation";
				                              				break;
			                              				case 3:
				                              				$type = "revisi-01.png";
				                              				$title = "Revision";
				                              			break;
				                              		}
				                              	?>
				                              	<img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" title="<?= $title ?>">
				                            </td>
				                            <?php 
				                            	$color="background-color:#FFEB3B;";
				                            	if ($value['status']=='C'){
				                            		$color = "background-color:#FF7043;";
				                            	}
				                            	$color="";
				                            ?>
				                            <td style="display: block;
											    overflow: hidden;
											    width:135px;
											    white-space: nowrap;
											    text-overflow: ellipsis;">
				                            	<div class="label" style="<?= $color ?> white-space: pre-wrap;padding-left: 0px;"><?= $value['title'] ?></div>
				                            </td>
				                            <td>
				                            	<div style="white-space: pre-wrap;padding-left: 0px;"><?= $value['user_action'] ?></div>
				                            </td>
				                            <td class="pad-style">
				                            	<div class="" style="position: absolute;width: 50%;">
				                            		<?= $value['status_step'] ?>             		
				                            	</div>
				                            	<div class="progress fhdisplay" style="width:<?=$value['total_progress'].'%'?>">
												  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $value['progress_percentage'].'%' ?> ">
												    <span class="sr-only"><?= $value['progress_percentage'].'%' ?> Complete</span>
												  </div>
												</div>
				                            </td>		                            
				                            <td style="text-align: center;padding-left: 0px;">
				                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
			                            			View
			                            		</a>
				                        	</td>
				                      	</tr>
			                	<?php	}
			                	?>
		                    </tbody>
					  	</table>
					  	<hr>			
					</div>
				</div>
			</div>
		</div>
	</div>

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

	<div class="col-xs-12 visible-xs">
		<div class="sub-header">
				<div class="swipe-tabs">
				    <div class="swipe-tab">
						<a href="#need_action" aria-controls="need_action" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
							Need Action <span class="label label-danger"><?= $need_action ?></span>
						</a>
				    </div>
				    <div class="swipe-tab">
						<a href="#my_document" aria-controls="my_document" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
							In Progress <span class="label label-danger"><?= $in_process ?></span>
						</a>
				    </div>
				    <div class="swipe-tab">
						<a href="#download_permission" aria-controls="download_permission" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
							Download Req. <span class="label label-danger"><?= $download_counter ?></span>
						</a>
				    </div>
				    <div class="swipe-tab">
						<a href="#workunit_change" aria-controls="workunit_change" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
							Unit Change <span class="label label-danger"><?= $workunit_change_counter ?></span>
						</a>
				    </div>
				    <div class="swipe-tab">
						<a href="#rejected_document" aria-controls="rejected_document" role="tab" data-toggle="tab" style="border: 0px solid #fff;">
							Rejected Doc <span class="label label-danger"><?= $rejected_document_counter ?></span>
						</a>
				    </div>
			 	</div>
			</div>
			<div class="main-container" style="box-shadow: 0px 2px 7px rgba(0,0,0,0.1);background-color: #fff;border-radius: 5px;">
				<div class="swipe-tabs-container ">
				    <div class="swipe-tab-content">
						<div class="panel-heading panel-color-blues-mbl" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
							<div class="row hidden">
				                <div class="col col-xs-12 text-right">
								<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <b style="color:red"><?= $need_action ?></b> documents that need to be checked</h7>	
									<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
									<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
				                </div>
				            </div>
							<div class="row">
				                <div class="col col-xs-9">
				                    <h3 class="panel-title titlePanel">Document Need Action</h3>
				                </div>
				                <div class="col col-xs-3 text-right">
								<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;"><b style="color:red"><?= $need_action ?></b> </h7>	
									<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
									<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
				                </div>
				            </div>
						</div>
					  	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>
			                        <th style="width: 6%;">
			                        	<div style="color: #fff">Icon</div>
			                        </th>
			                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
			                        <th style="width: 24%;">Title</th>
<!-- 			                    <th style="width: 15%;">Status</th>
			                        <th style="width: 50%; padding-left: 0px;">Progress</th> -->
		            	            <th style="width: 10%;">Action</th>
			                    </tr> 
			                </thead>

							<tbody class=" text-decor ">
								<?php 
									foreach ($data as $key => $value) {
			                			if ($value['is_mine']==1)
			                				continue;

			                		?>
			                	<tr>
									<td>
										<?php 
				         //                      		if($value['ext'] == "pdf")
													// 	$type = "pdf.png";
													// else if($value['ext'] == "doc" || $value['ext'] == "docx")
													// 	$type = "docx.png";
													// else if($value['ext'] == "ppt" || $value['ext'] == "pptx")
													// 	$type = "ppt.png";
													// else if($value['ext'] == "xls" || $value['ext'] == "xlsx")
													// 	$type = "xls.png";
				                              		switch ($value['workflow_id']) {
				                              			case 1:
				                              				$type = "submit-01.png";
				                              				$title = "Submission";
				                              				break;
				                              			
				                              			case 2:
				                              				$type = "evaluasi-01.png";
				                              				$title = "Evaluation";
				                              				break;
				                              			case 3:
				                              				$type = "revisi-01.png";
				                              				$title = "Revision";
				                              			break;
				                              			// default:
				                              			// 	$type = "submit-01.png";
				                              			// 	$title = "Submission";
				                              			// 	break;
				                              		}
				                        ?>
										<img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" style="">
										<!-- <img src="C:/www/semen_indonesia/assets/web/images/icondoc/mod1/submit-01.png"> -->
									</td>
									<td>
										<ul class="list-group" style="margin-bottom:0px;">
											<li class="list-group-item overflow ellipsis"><?= $value['title'] ?></li>
											<li class="list-group-item"><?= $value['user_action'] ?></li>
										</ul>
									</td>
									<td>
		                            	<a class="btnTable-mbl btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
	                            			View
	                            		</a>
									</td>
								</tr>
			                	<?php
			                		}
								?>
								

												
		                    </tbody>
					  	</table>
					  	<div class="panel-footer" style="padding: 0px 15px;background-color: #fff;">
						<nav>
						  <ul class="pager">
						  	<?php 
						  		$prev = '';
						  		$next = '';
						  		if($_SESSION['page_evaluation'] == 1){
						  			$prev = 'disabled';
						  		}
						  		if($PendingDocEvaluation['nextpage']==0){
						  			$next= 'disabled';
						  		}
						  	 ?>
						    <li class="previous <?php echo $prev ?>" onclick="pageEvaluation(<?php echo ($_SESSION['page_evaluation']-1) ?>)">
						    	<a >
						    		<span aria-hidden="true">&larr;</span> 
						    		Previous 
						    	</a>
						    </li>
						    <li class="next <?php echo $next ?>" onclick="pageEvaluation(<?php echo ($_SESSION['page_evaluation']+1) ?>)">
						    	<a>
						    		Next 
						    		<span aria-hidden="true">&rarr;</span>
						    	</a>
						    </li>
						  </ul>
						</nav>
					</div>

			        	<div class="panel-footer plusedit hidden" style="padding-top: 0px;">
							<div class="row">
								<?php
									$hidden_before = ''; 
									$hidden_next = ''; 
									if ($page>0) { 
										$hidden_before = '';
									} 
									if ($next==1) {
										$hidden_next = '';
									}
								?>
				                <div class="col col-xs-6">
				                    <button type="button" onclick="viewDocumentLess()" class="btn btn-sm btn-barubefore <?= $hidden_before ?>"><b><span class="glyphicon glyphicon-backward" aria-hidden="true"></span></b></button>
				                </div>
				                <div class="col col-xs-6 text-right">
				                    <button type="button" onclick="viewDocumentMore()" class="btn btn-sm btn-barunext <?= $hidden_next ?>"><b><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></b></button>
				                </div>
				            </div>
				        </div>			

				    </div>
				    <div class="swipe-tab-content">
				    	<div class="panel-heading panel-color-blues-mbl" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
						<div class="row">
			                <div class="col col-xs-9">
			                    <h3 class="panel-title titlePanel">In Progress Documents</h3>
			                </div>
			                <div class="col col-xs-3 text-right">
							<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;"><b style="color:red"><?= $in_process ?></b></h7>
								<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
								<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
			                </div>
			            </div>
					</div>
				  	<table class="table tabedit">
				    	<thead class="head-size">
			                    <tr>
			                        <th style="width: 6%;">
			                        	<div style="color: #fff">Icon</div>
			                        </th>
			                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
			                        <th style="width: 24%;">Title</th>
									<!--<th style="width: 15%;">Status</th>
			                        <th style="width: 50%; padding-left: 0px;">Progress</th> -->
		            	            <th style="width: 10%;">Action</th>
			                    </tr> 
		                </thead>

						<tbody class=" text-decor ">
							<?php 
								foreach ($data as $key => $value) {
			                			// echo '<pre>';
			                			// vaR_dump($data);
			                			// die();
			                			if ($value['is_mine']==0)
			                				continue;

			                	?>
			                	<tr>
									<td>
										<?php 
				         //                      		if($value['ext'] == "pdf")
													// 	$type = "pdf.png";
													// else if($value['ext'] == "doc" || $value['ext'] == "docx")
													// 	$type = "docx.png";
													// else if($value['ext'] == "ppt" || $value['ext'] == "pptx")
													// 	$type = "ppt.png";
													// else if($value['ext'] == "xls" || $value['ext'] == "xlsx")
													// 	$type = "xls.png";
					                              	switch ($value['workflow_id']) {
				                              			case 1:
				                              				$type = "submit-01.png";
				                              				$title = "Submission";
				                              				break;
				                              			
				                              			case 2:
				                              				$type = "evaluasi-01.png";
				                              				$title = "Evaluation";
				                              				break;
			                              				case 3:
				                              				$type = "revisi-01.png";
				                              				$title = "Revision";
				                              			break;
				                              			// default:
				                              			// 	$type = "submit-01.png";
				                              			// 	$title = "Submission";
				                              			// 	break;
				                              		}
				                              	?>
										<img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" style="">
										<!-- <img src="C:/www/semen_indonesia/assets/web/images/icondoc/mod1/submit-01.png"> -->
									</td>
									<td>
										<ul class="list-group" style="margin-bottom:0px;">
											<li class="list-group-item overflow ellipsis"><?= $value['title'] ?></li>
											<li class="list-group-item"><?= $value['user_action'] ?></li>
										</ul>
									</td>
									<td>
		                            	<a class="btnTable-mbl btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
	                            			View
	                            		</a>
									</td>
								</tr>
			                	<?php
			                	}
							 ?>
								

											
		                    </tbody>
				  	</table>
				    </div>

				    <div class="swipe-tab-content">
				    	<div class="panel-heading panel-color-blues-mbl" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
						<div class="row">
			                <div class="col col-xs-12">
			                    <h3 class="panel-title titlePanel">Download Permissions</h3>
			                </div>
			                <!-- <div class="col col-xs-7 text-right">
							<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <b style="color:red"><?= $need_action ?></b> documents that need to be checked</h7>	
								<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
								<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
			                </div> -->
			            </div>
					</div>
				  	<table class="table tabedit">
				    	<thead class="head-size">
		                    <tr>
		                        <th style="width: 6%;">
		                        	<div style="color: #fff">Icon</div>
		                        </th>
		                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
		                        <th style="width: 24%;">Title</th>
								<!--<th style="width: 15%;">Status</th>
		                        <th style="width: 50%; padding-left: 0px;">Progress</th> -->
	            	            <th style="width: 10%;">Action</th>
		                    </tr> 
<!-- 		                    <tr>
		                        <th style="width: 6%;"></th>
		                        <th style="width: 15%;">Requestor</th>
		                        <th style="width: 15%;">Workunit</th>
		                        <th style="width: 25%;">Document</th>
		                        <th style="width: 15%; padding-left: 0px;">Status</th>
	            	            <th style="width: 25%;">Action</th>
		                    </tr>  -->
		                </thead>
		                <tbody class=" text-decor ">
		                	<?php 
		                		foreach ($downloads as $k_download => $v_download) {
		                			$download_title = "<a style='color:#424242' href='".site_url('web/document/download_detail').'/'.$v_download['document_id']."'>".$v_download['type_name']." <b style='color:#64DD17'>\"".$v_download['name']."\"</b> from <b style='color:#0091EA'>".$v_download['document_workunit']."</b> </a> ";
		                	?>
		                		<tr>
									<td>
										<?php 
                              				$type = "delight-01.png";
                              				$title = "Ask To Download";
		                              	?>
				                        <img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" title="<?= $title ?>">
									</td>
									<td>
				                            	<div class="label" style="white-space: pre-wrap;padding-left: 0px;"></div>
				                            	<div style="padding-left: 0px;">
				                          
					                            </div>
										<ul class="list-group" style="margin-bottom:0px;">
											<li class="list-group-item overflow ellipsis"><?= $v_download['requestor'] ?></li>
											<li class="list-group-item overflow ellipsis"><?= $v_download['requestor_workunit'] ?></li>
											<li class="list-group-item overflow ellipsis"><?= $download_title ?></li>
											<li class="list-group-item overflow ellipsis">
												<?php if ($v_download['is_allowed']==0) { 
				                            			$is_allowed = 0;
					                            		// if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isDocumentController()) { 
				                            			if (SessionManagerWeb::isDocumentController()) { 
							                            	$text = "Need Your Approval";
							                            	$is_allowed = 2;
						                            	} else { 
						                            		$text =	"Waiting Approval";
						                            	}  
						                            } else if ($v_download['is_allowed']==-1) {  
					                            		$text = "<b style='color:red'>Rejected</b> because \"".$v_download['reject_reason']."\"";
					                            		$is_allowed = -1;
					                            	} else {
					                            		$text = "Approved, Valid Until <b style='color:#64DD17'>".$v_download['valid_until'].'</b>';
					                            		$is_allowed = 1;
					                            	} ?>
					                            	<?= $text ?>
											</li>
										</ul>
									</td>
									<td>
		                            	<?php if ($is_allowed==2) { ?>
	                            		<a class="btnTable btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$v_download['id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank" style="margin-right:10px">
	                            			View
	                            		</a>
		                            	<a class="btnTable btn-dashboard-success hidden" href="<?= site_url('web/document/approve_download/').'/'.$v_download['id'] ?>" data-toggle="tooltip" data-placement="top" title="Approve Download" style="background-color: #00B65C">
	                            			Approve
	                            		</a>
                            		<?php } else if ($is_allowed==1) { ?>
                            			<a class="btnTable btn-lihatview" href="<?= base_url('/web/document/download_watermark').'/'.$v_download['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="Download Document" target="_blank" >
	                            			Download
	                            		</a>
	                            	<?php } else if ($is_allowed==0) { ?>
		                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$v_download['id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank" style="margin-right:10px">
	                            			View
	                            		</a>
	                            	<?php } ?>
									</td>
								</tr>
		                	<?php
		                		}
		                	 ?>	
		                    </tbody>
						
				  	</table>
				    </div>
				    <div class="swipe-tab-content">
				    	<div class="panel-heading panel-color-blues-mbl" id="notificationx" style="background-color:<?= ($workunit_change_counter>0) ? "#FFF" : "white"; ?>">
						<div class="row">
			                <div class="col col-xs-12">
			                    <h3 class="panel-title titlePanel">Workunit Change</h3>
			                </div>
			            </div>
					</div>
				  	<table class="table tabedit">
				    	<thead class="head-size">
			                    <tr>
<!-- 			                        <th style="width: 6%;">
			                        	<div style="color: #fff">Icon</div>
			                        </th> -->
			                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
			                        <th style="width: 24%;">Document</th>
									<!--<th style="width: 15%;">Status</th>
			                        <th style="width: 50%; padding-left: 0px;">Progress</th> -->
		            	            <th style="width: 10%;">Action</th>
			                    </tr> 
<!-- 		                    <tr>
		                        <th style="width: 50%;">Document</th>
		                        <th style="width: 15%;">Creator</th>
		                        <th style="width: 15%;">Approver</th>
	            	            <th style="width: 15%;">Action</th>
		                    </tr>  -->
		                </thead>
		                 <tbody class=" text-decor ">
								<tr>
									<td>
										<ul class="list-group" style="margin-bottom:0px;">
											<li class="list-group-item overflow ellipsis">GALIH KURDINAR PURDANIYANTO, S.Kom.</li>
											<li class="list-group-item overflow ellipsis">Department of Strategic ICT</li>
											<li class="list-group-item overflow ellipsis">Instruksi Kerja "PENERIMAAN TAMU DI KANTOR JAKARTA" from Bureau of Jakarta Office  </li>
											<li class="list-group-item pad-style hidden">
				                            	<div class="" style="position: absolute; width: 100%;">
				                            		<?= $value['status_step'] ?>             		
				                            	</div>
				                            	<div class="progress fhdisplay-mbl" style="width:<?=$value['total_progress'].'%'?>">
													<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $value['progress_percentage'].'%' ?> ">
												    	<span class="sr-only"><?= $value['progress_percentage'].'%' ?> Complete</span>
													</div>
												</div>										
											</li>
										</ul>
									</td>
									<td>
		                            	<a class="btnTable-mbl btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
	                            			View
	                            		</a>
									</td>
								</tr>

								<tr>

									<td>
										<ul class="list-group" style="margin-bottom:0px;">
											<li class="list-group-item overflow ellipsis">Cras justo odio, Dapibus ac facilisis in,Dapibus ac facilisis in</li>
											<li class="list-group-item">Waiting Approval</li>
											<li class="list-group-item hidden">Morbi leo risus</li>
										</ul>
									</td>
									<td>
		                            	<a class="btnTable-mbl btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
	                            			View
	                            		</a>
									</td>
								</tr>
								<tr>

									<td>
										<ul class="list-group" style="margin-bottom:0px;">
											<li class="list-group-item overflow ellipsis">Cras justo odio, Dapibus ac facilisis in,Dapibus ac facilisis in</li>
											<li class="list-group-item">Waiting Validation</li>
											<li class="list-group-item hidden">Morbi leo risus</li>
										</ul>
									</td>
									<td>
		                            	<a class="btnTable-mbl btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
	                            			View
	                            		</a>
									</td>
								</tr>						
		                    </tbody>

						<!-- <tbody class=" text-decor ">
		                	<?php 
		                		foreach ($workunit_change as $k_workunit => $v_workunit) {
		                	?>		
		                			<tr>
			                            <td>
			                            	<div><?= $v_workunit['title'] ?></div>
			                            </td>
			                            <td>
			                            	<div><?= ($v_workunit['creator']) ? "<b style='color:red'>Need Action</b>" : "<b style='color:green'></b>" ?></div>
			                            </td>
			                            <td>
			                            	<div><?= ($v_workunit['approver']) ? "<b style='color:red'>Need Action</b>" : "<b style='color:green'>OK</b>" ?></div>
			                            </td>	                            
			                            <td style="padding-left: 0px;">
			                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/changeunit/').'/'.$v_workunit['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Changeunit Document" target="_blank" style="margin-right:10px">
		                            			View
		                            		</a>
			                        	</td>
			                      	</tr>
		                			<?php	
		                		} ?>
	                    </tbody> -->
				  	</table>
				    </div>
				    <div class="swipe-tab-content">
				    	<div class="panel-heading panel-color-blues-mbl" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
						<div class="row">
			                <div class="col col-xs-9">
			                    <h3 class="panel-title titlePanel">Rejected Documents</h3>
			                </div>
			                <div class="col col-xs-3 text-right">
							<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">
								<b style="color:red"><?= $rejected_document_workunit ?></b>
							</h7>	
								<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
								<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
			                </div>
			            </div>
					</div>
				  	<table class="table tabedit">
				    	<thead class="head-size">
		                    <tr>
		                        <th style="width: 6%;">
		                        	<div style="color: #fff">Icon</div>
		                        </th>
		                        <!-- <th style="width: 20%;">Kode Dokumen</th> -->
		                        <th style="width: 24%;">Title</th>
<!-- 			                    <th style="width: 15%;">Status</th>
		                        <th style="width: 50%; padding-left: 0px;">Progress</th> -->
	            	            <th style="width: 10%;">Action</th>
		                    </tr> 
		                </thead>

						<tbody class=" text-decor ">
							<?php
		                		foreach ($rejected_documents as $key => $value) {
		                	?>
		                		<tr>
									<td>
		                              	<?php 
			                              	switch ($value['workflow_id']) {
		                              			case 1:
		                              				$type = "submit-01.png";
		                              				$title = "Submission";
		                              				break;
		                              			
		                              			case 2:
		                              				$type = "evaluasi-01.png";
		                              				$title = "Evaluation";
		                              				break;
	                              				case 3:
		                              				$type = "revisi-01.png";
		                              				$title = "Revision";
		                              			break;
		                              		}
		                              	?>
				                        <img src="<?php echo site_url("assets/web/images/icondoc/mod1");?>/<?= $type ?>" title="<?= $title ?>" style="">
									</td>
									<?php 
		                            	$color="background-color:#FFEB3B;";
		                            	if ($value['status']=='C'){
		                            		$color = "background-color:#FF7043;";
		                            	}
		                            	$color="";
		                            ?>
									<td>
										<ul class="list-group" style="margin-bottom:0px;">
											<li class="list-group-item overflow ellipsis"><?= $value['title'] ?></li>
											<li class="list-group-item"><?= $value['user_action'] ?></li>
										</ul>
									</td>
									<td>
		                            	<a class="btnTable-mbl btn-lihatview" href="<?= site_url('web/document/download_detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail" target="_blank">
	                            			View
	                            		</a>
									</td>
								</tr>
		                	<?php 
		                		}
		                	 ?>
														
		                    </tbody>
				  	</table>
				    </div>
			  	</div>
		</div>
	</div>

	<div class="col-xs-12 col-md-12" style="padding:0%;">
		<div class="panel shadowPanel hidden">
			<!-- Default panel contents -->
		<div class="panel-heading panel-color-purple">
			<div class="row">
                <div class="col col-xs-8">
                    <h3 class="panel-title titlePanel">List Document</h3>
                </div>
            </div>
		</div>
		<div class="panel-body-2">
			<div class="description-section">
		    	<table class="table tabedit">
			    	<thead class="head-size">
	                    <tr>

	                        <th style="width: 0%;"></th>
	                        <th style="width: 50%;">Company name</th>
            	            <th  class="text-center" style="width: 30%;">Amount</th>
	                    </tr> 
	                </thead>
	                <tbody class=" text-decor ">
            			<tr>
                            <td class="nowrapping2 text-center" style="padding: 0px">
								<img src="<?php echo site_url("assets/web/img/SG.png");?>">
                            </td>
                            <td class="mod-jd-text" style="padding-top: 0px;">
                            	Semen Gresik
                            </td>
                            <td class="text-center mod-jd-text">
                            	150
                            </td>
                      	</tr>
                      	<tr>
                            <td class="nowrapping2 text-center" style="padding: 0px">
								<img src="<?php echo site_url("assets/web/img/SP.png");?>">
                            </td>
                            <td class="mod-jd-text" style="padding-top: 0px;">
                            	Semen Padang
                            </td>
                            <td class="text-center mod-jd-text">
                            	50
                            </td>
                      	</tr>
                      	<tr>
                            <td class="nowrapping2 text-center" style="padding: 0px">
								<img src="<?php echo site_url("assets/web/img/ST.png");?>">
                            </td>
                            <td class="mod-jd-text" style="padding-top: 0px;">
                            	Semen Tonasa
                            </td>
                            <td class="text-center mod-jd-text">
                            	200
                            </td>
                      	</tr>
                      	<tr>
                            <td class="nowrapping2 text-center" style="padding: 0px">
								<img src="<?php echo site_url("assets/web/img/TL.png");?>">
                            </td>
                            <td class="mod-jd-text" style="padding-top: 0px;">
                            	Semen Thang Long
                            </td>
                            <td class="text-center mod-jd-text">
                            	70
                            </td>
                      	</tr>
                    	<tr>
                            <td class="nowrapping2 text-center" style="padding: 0px">

                            </td>
                            <td class="mod-jd-text">
                            	Document Total
                            </td>
                            <td class="text-center mod-jd-text">
                            	470
                            </td>
                      	</tr>
                    </tbody>
                </table>
				</div>
			</div>
		</div>

		<div class="visible-xs">	
		<br>
		</div>
		<div class="col-xs-12 col-sm-12 total-document">
		<div class="panel shadowPanel">
			<!-- Default panel contents -->
			<div class="panel-heading panel-color-none">
				<div class="row">
	                <div class="col-xs-12 col-sm-12">
	                    <h3 class="panel-title titlePanel">Total Documents per Type</h3>
	                </div>
	            </div>
			</div>
			<div class="panel-body">
				<div class="description-section">
			    	<table class="table tabedit">
				    	<thead class="head-size">
		                </thead>
    	                <tbody class=" text-decor ">
<!-- 					<canvas id="densityChart" width="600" height="400"></canvas> -->
							<canvas id="bar-chart" width="600" height="200"></canvas>
							<script>
								var text = 'Total documents in ' + "<?= $total_type['name'] ?>" ;
								var BM = "<?= $total_type['total'][1] ?>";
								var M = "<?= $total_type['total'][2] ?>";
								var P = "<?= $total_type['total'][3] ?>";
								var IK = "<?= $total_type['total'][4] ?>";
								var F = "<?= $total_type['total'][5] ?>";

								var windowsize = $(window).width();
								if (windowsize < 768) {
									var ctx_1 = document.getElementById('bar-chart');
									ctx_1.height = 150;
									ctx_1.width = 150;
								}

								new Chart(document.getElementById("bar-chart"), {
								    type: 'bar',

								    data: {
								      labels: ["Pedoman Tata Laksana ("+BM+")","Pedoman ("+M+")", "Prosedur ("+P+")","Instruksi Kerja ("+IK+")",  "Form ("+F+")"],
								      datasets: [
								        {
								          label: "Documents",
								          backgroundColor: ["#FFB5B5", "#FFBBFF","#BBBBFF","#CAFEB8","#FFE099"],
								          data: [BM, M, P, IK, F]
								        }
								      ]
								    },
								    options: {
								    	legend: { display: false },
								    	title: {
								    		display: true,
								        	text: text
								      }
								    }
								});

								var densityCanvas = document.getElementById("densityChart");
									Chart.defaults.global.defaultFontFamily = "Lato";
									Chart.defaults.global.defaultFontSize = 12;

									var densityData = {
									  label: 'Amount',
									  data: [200, 75, 400, 570, 730, 810, 980, 900],
									  backgroundColor: 'rgba(255,193,7 ,1)',
									  borderWidth: 0,
									  yAxisID: "y-axis-density"
									};

									var gravityData = {
									  label: 'Document',
									  data: [100.0, 370.0, 327.0, 410.0, 573.0, 500.0, 742.0, 990.0],
									  backgroundColor: 'rgba(255,87,34 ,1)',
									  borderWidth: 0,
									  yAxisID: "y-axis-gravity"
									};

									var planetData = {
									  labels: ["Form", "Pedo.", "Pros.", "I.K.", "BoarMan"],
									  datasets: [densityData, gravityData]
									};

									var chartOptions = {
									  scales: {
									    xAxes: [{
									      barPercentage: 1,
									      categoryPercentage: 0.6
									    }],
									    yAxes: [{
									      id: "y-axis-density"
									    }, {
									      id: "y-axis-gravity"
									    }]
									  }
									};

									var barChart = new Chart(densityCanvas, {
									  type: 'bar',
									  data: planetData,
									  options: chartOptions
									});
							</script>
	                    </tbody>
	                </table>
					</div>
			</div>
			  <!-- Table -->
		</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-6" style="padding-right: 0px;">
		<div class="panel shadowPanel hidden">
			<!-- Default panel contents -->
		<div class="panel-heading panel-color-purple" style="padding: 8px 5%;">
			<div class="row">
                <div class="col col-xs-7">
                    <h3 class="panel-title titlePanel" style="margin-top: 5%;">Document Status</h3>
                </div>
                <div class="col col-xs-5" style="padding: 0px;">
					<div class="dropup">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #fff;border-bottom: 1px solid #000;padding: 5px 10px 5px 5px;">
					    Change Status
					    <span class="caret" style="margin-left: 5%;"></span>
					  </button>
					  <ul class="dropdown-menu pluset" aria-labelledby="dropdownMenu2" style="border:0px solid;">
					    <li><a href="#">Draft</a></li>
					    <li><a href="#">Publish</a></li>
					  </ul>
					</div>
                </div>
            </div>
		</div>
		<div class="panel-body" style="padding-top: 1%;padding-bottom: 1%;">
			<div class="description-section">

			    	<table class="table tabedit">
			    	<thead class="head-size">
	                    <tr>

	                        <th class="" style="width: 0%;">No.</th>
	                        <th style="width: 60%;">Company Name</th>
            	            <th  class="text-center" style="width: 50%;">Amount</th>
	                    </tr> 
	                </thead>
	                <tbody class=" text-decor ">
            			<tr>
                            <td class="nowrapping3 text-center">
								1
                            </td>
                            <td class="mod-jd-text">
                            	Semen Gresik
                            </td>
                            <td class="text-center mod-jd-text">
                            	30
                            </td>
                      	</tr>
                      	<tr>
                            <td class="nowrapping3 text-center">
								2
                            </td>
                            <td class="mod-jd-text">
                            	Semen Padang
                            </td>
                            <td class="text-center mod-jd-text">
                            	70
                            </td>
                      	</tr>
                      	<tr>
                            <td class="nowrapping3 text-center">
								3
                            </td>
                            <td class="mod-jd-text">
                            	Semen Tonasa
                            </td>
                            <td class="text-center mod-jd-text">
                            	150
                            </td>
                      	</tr>
                      	<tr>
                            <td class="nowrapping3 text-center">
								4
                            </td>
                            <td class="mod-jd-text">
                            	Semen Thang Long
                            </td>
                            <td class="text-center mod-jd-text">
                            	30
                            </td>
                      	</tr>
                    	<tr>
                            <td class="nowrapping3">

                            </td>
                            <td class="mod-jd-text">
                            	Document Total
                            </td>
                            <td class="text-center mod-jd-text">
                            	280
                            </td>
                      	</tr>
                    </tbody>
                </table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6" style="padding-right: 0px;"></div>
	<div class="clearfix"></div>
</div>

	  	<div class="col-xs-12 col-md-12 hidden" style="padding:0%;">
        	<div class="panel shadowPanel">
  				<!-- Default panel contents -->
  				<!-- untuk notifikasi pakai class ini "panel-color-blues-notif" -->
				<div class="panel-heading panel-color-blues" id="notificationx" style="background-color:<?= ($total_document>0) ? "#FFF" : "white"; ?>">
					<div class="row">
		                <div class="col col-xs-4">
		                    <h3 class="panel-title titlePanel">Document List</h3>
		                </div>
		                <div class="col col-xs-8 text-right">
						<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <b style="color:red"><?= $total_all_document ?></b> documents that need to be checked</h7>	
							<i style="margin-right: 5%; color: #FFFF00;" class="fa fa-bell notifcolr hidden"></i>
							<i style="margin-right: 5%; color: #F4511E;" class="fa fa-bell notifcolr"></i>
		                </div>
		            </div>
				</div>
			  	<table class="table tabedit">
			    	<thead class="head-size">
	                    <tr>
	                        <th style="width: 8%;"></th>
	                        <th style="width: 25%;">Title</th>
	                        <th style="width: 25%;">Title</th>
	                        <th style="width: 45%; padding-left: 0px;">Status</th>
            	            <th style="width: 10%;">Action</th>
	                    </tr> 
	                </thead>

					<tbody class=" text-decor ">
	                	<?php
	                		foreach ($data as $key => $value) {
	                	?>		
	                			<tr>
		                            <td align="center">

		                              	<!-- <i class="glyphicon glyphicon-file file-doc"></i> -->
		                              	<?php 
		                              		if($value['ext'] == "pdf")
												$type = "pdf.png";
											else if($value['ext'] == "doc" || $value['ext'] == "docx")
												$type = "docx.png";
											else if($value['ext'] == "ppt" || $value['ext'] == "pptx")
												$type = "ppt.png";
											else if($value['ext'] == "xls" || $value['ext'] == "xlsx")
												$type = "xls.png";
		                              	?>
		                              	<img src="<?php echo site_url("assets/web/images/");?>/<?= $type ?>">
		                            </td>
		                            <!-- <td class="nowrapping"><?//= $value['code'] ?></td> -->
		                            <?php 
		                            	$color="background-color:#FFEB3B;";
		                            	if ($value['status']=='C'){
		                            		$color = "background-color:#FF7043;";
		                            	}
		                            	$color="";
		                            ?>
		                            <td style="display: block;
									    width: 210px;
									    overflow: hidden;
									    white-space: nowrap;
									    text-overflow: ellipsis;">
		                            	<div class="label" style="<?= $color ?> white-space: pre-wrap;padding-left: 0px;"><?= $value['title'] ?></div>
		                            </td>
		                            <td class="pad-style">
		                            	<div class="" style="position: absolute;width: 50%;">
		                            		<?= $value['status_step'] ?>      		
		                            	</div>
		                            	<div class="progress fhdisplay" style="width:<?=$value['total_progress'].'%'?>">
										  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?= $value['progress_percentage'].'%' ?> ">
										    <span class="sr-only"><?= $value['progress_percentage'].'%' ?> Complete</span>
										  </div>
										</div>
		                            </td>		                            
		                            <!-- <td>Tersisa 3 Hari Lagi</td> -->
		                            <td style="text-align: center;padding-left: 0px;">
		                            	<?php if ($value['ext']!='docx' && $value['ext']!='doc' and 1==0) { ?>
		                            		<a class="btnTable btn-lihatview" href="javascript:View(<?= $value['document_id'] ?>)" data-toggle="tooltip" data-placement="top" title="Preview">
		                            			View
		                            		</a>
		                            	<? } ?>
		                            	<a class="btnTable btn-lihatview" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="View Document Detail">
		                            			View
		                            		</a>
		                        		<a class="btnTable btn-detailainnya hidden" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>">
		                        			<!-- <i class="fa fa-ellipsis-h"></i> -->
		                        			Cek
		                        		</a>
		                        	</td>
		                      	</tr>
	                	<?php	}
	                	?>
                    </tbody>
			  	</table>
			  	<hr>
	        	<div class="panel-footer plusedit" style="padding-top: 0px;">
					<div class="row">
						<?php
							$hidden_before = 'hidden'; 
							$hidden_next = 'hidden'; 
							if ($page>0) { 
								$hidden_before = '';
							} 
							if ($next==1) {
								$hidden_next = '';
							}
						?>
		                <div class="col col-xs-6">
		                    <button type="button" onclick="viewDocumentLess()" class="btn btn-sm btn-barubefore <?= $hidden_before ?>"><b>Previous</b></button>
		                </div>
		                <div class="col col-xs-6 text-right">
		                    <button type="button" onclick="viewDocumentMore()" class="btn btn-sm btn-barunext <?= $hidden_next ?>"><b>Next</b></button>
		                </div>
		            </div>
		        </div>			
			</div>
	</div>
</div>

<div class="col-xs-12 col-sm-4 new-publish" style="">
    	<div class="panel shadowPanel">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-purple">
					<div class="row">
		                <div class="col col-xs-12">
		                    <h3 class="panel-title titlePanel">New Published Document</h3>
		                </div>
		            </div>
				</div>
				<div class="panel-body-2" style="padding: 5px 7px;">
					<div class="description-section">
					    	<table class="table tabedit">
		    	                <tbody class=" text-decor ">
		    	                	<?php foreach ($newest_doc as $k => $v) { ?>
			                			<tr>
				                            <td class="mod-jd-text" style="padding-top: 0px;">
				                            	<a href="<?= site_url('web/document/popularDocument').'/'.$v['document_id'] ?>" class="hovnewsdoc">
					                            	<h5 class="distanh5">
					                            		<b>
					                            			<?= $v['title'] ?>
					                            		</b>
					                            	</h5>
					                            </a>
				                            	<i class="fa fa-user userfaedit"></i>
				                            		<span class="fontuser">
				                            			<?= $v['unitkerja_name'] ?>
				                            		</span> 
				                            	<br>
				                            	<i class="fa fa-calendar calfaedit"></i>
				                            		<span class="fontcal">
				                            			<?= $v['updated_at'] ?>
				                            		</span>
				                            </td>
				                      	</tr>
			                      	<?php } ?>
			                    </tbody>
			                </table>
						</div>
					</div>
			</div>

    	<div class="panel shadowPanel">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-purple">
					<div class="row">
		                <div class="col col-xs-12">
		                    <h3 class="panel-title titlePanel">Pending Evaluated Documents</h3>
		                </div>
		            </div>
		            <br>
					<div class="row">
		                <div class="col col-xs-12">
							<tr>
								<td>
									<span class="label label-primary" style="color: #BBBBFF;background-color: #BBBBFF;">B</span>
									<h7 style="font-size: 14px;font-weight: 500;"> Documents that have not been Approve</h7>
								</td>
							</tr>
		                </div>
		                <div class="col col-xs-12">
							<tr>
								<td>
									<span class="label label-danger" style="color: #FFB5B5;background-color: #FFB5B5;">B</span>
									<h7 style="font-size: 14px;font-weight: 500;"> Documents that have not been Evaluation</h7>
								</td>
							</tr>
		                </div>
	            	</div>
				</div>

				<div class="panel-body">
					<div class="description-section">

					    	<table class="table tabedit">
					    	<thead class="head-size" style="">
			                    <tr>
			                        <th style="width: 40%;">Name</th>
		            	            <th  class="" style="width: 60%;">Amount</th>
			                    </tr> 
			                </thead>
	    	                <tbody class=" text-decor ">
	    	               <?php foreach ($PendingDocEvaluation['row'] as $pendingEvaluation): ?>
	    	               		<?php 
	    	               			$widthEv = ($pendingEvaluation['PENDING_EVALUATION']/$_SESSION['max_evaluation']) * 100;
	    	               			$widthAp = ($pendingEvaluation['PENDING_APPROVE']/$_SESSION['max_evaluation']) * 100;
	    	               		 ?>
	    	                	<tr>
		                            <td class="mod-jd-text">
		                            	<?php echo $pendingEvaluation['NAME'] ?>
		                            </td>
		                            <td class="mod-jd-text">
		                            	<div class="progress no-bg-pending">
		                            		<?php if ($pendingEvaluation['PENDING_EVALUATION']!= 0): ?>
		                            			<div class="progress-bar progress-bar-info red-progs" style="width: <?php echo $widthEv ?>%"><?php echo $pendingEvaluation['PENDING_EVALUATION'] ?>
													<span class="sr-only">35% Complete (success)</span>
												</div>
		                            		<?php endif ?>
		                            		<?php if ($pendingEvaluation['PENDING_APPROVE']!= 0): ?>
		                            			<div class="progress-bar progress-bar-danger blue-progs" style="width:<?php echo $widthAp ?>%"><?php echo $pendingEvaluation['PENDING_APPROVE'] ?>
													<span class="sr-only">10% Complete (danger)</span>
												</div>
		                            		<?php endif ?>
											
											
										</div>
		                            </td>
		                      	</tr>
	    	                <?php endforeach ?>
	                			
		                    </tbody>
		                </table>
						</div>
					</div>
					<div class="panel-footer" style="padding: 0px 15px;background-color: #fff;">
						<nav>
						  <ul class="pager">
						  	<?php 
						  		$prev = '';
						  		$next = '';
						  		if($_SESSION['page_evaluation'] == 1){
						  			$prev = 'disabled';
						  		}
						  		if($PendingDocEvaluation['nextpage']==0){
						  			$next= 'disabled';
						  		}
						  	 ?>
						    <li class="previous <?php echo $prev ?>" onclick="pageEvaluation(<?php echo ($_SESSION['page_evaluation']-1) ?>)">
						    	<a >
						    		<span aria-hidden="true">&larr;</span> 

						    		Previous 
						    	</a>
						    </li>
						    <li class="next <?php echo $next ?>" onclick="pageEvaluation(<?php echo ($_SESSION['page_evaluation']+1) ?>)">
						    	<a >
						    		Next 
						    		<span aria-hidden="true">&rarr;</span>
						    	</a>
						    </li>
						  </ul>
						</nav>
					</div>
			</div>

        	<div class="panel shadowPanel hidden">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-purple">
					<div class="row">
		                <div class="col col-xs-8">
		                    <h3 class="panel-title titlePanel">List Document</h3>
		                </div>
<!-- 		                <div class="col col-xs-4 text-right">
		                	<a class="btn btn-detailainnya2">
		                    	<i class=" fa fa-ellipsis-h" style="color: #9C27B0;"></i>
		                	</a>
		                </div> -->
		            </div>
				</div>
				<div class="panel-body-2">
					<div class="description-section">

					    	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>

			                        <th style="width: 0%;"></th>
			                        <th style="width: 50%;">Company name</th>
		            	            <th  class="text-center" style="width: 30%;">Amount</th>
			                    </tr> 
			                </thead>
	    	                <tbody class=" text-decor ">
	                			<tr>
		                            <td class="nowrapping2 text-center" style="padding: 0px">
										<img src="<?php echo site_url("assets/web/img/SG.png");?>">
		                            </td>
		                            <td class="mod-jd-text" style="padding-top: 0px;">
		                            	Semen Gresik
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	150
		                            </td>
		                      	</tr>
		                      	<tr>
		                            <td class="nowrapping2 text-center" style="padding: 0px">
										<img src="<?php echo site_url("assets/web/img/SP.png");?>">
		                            </td>
		                            <td class="mod-jd-text" style="padding-top: 0px;">
		                            	Semen Padang
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	50
		                            </td>
		                      	</tr>
		                      	<tr>
		                            <td class="nowrapping2 text-center" style="padding: 0px">
										<img src="<?php echo site_url("assets/web/img/ST.png");?>">
		                            </td>
		                            <td class="mod-jd-text" style="padding-top: 0px;">
		                            	Semen Tonasa
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	200
		                            </td>
		                      	</tr>
		                      	<tr>
		                            <td class="nowrapping2 text-center" style="padding: 0px">
										<img src="<?php echo site_url("assets/web/img/TL.png");?>">
		                            </td>
		                            <td class="mod-jd-text" style="padding-top: 0px;">
		                            	Semen Thang Long
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	70
		                            </td>
		                      	</tr>
		                    	<tr>
		                            <td class="nowrapping2 text-center" style="padding: 0px">

		                            </td>
		                            <td class="mod-jd-text">
		                            	Document Total
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	470
		                            </td>
		                      	</tr>
		                    </tbody>
		                </table>
						</div>
					</div>
				</div>

				<div class="panel shadowPanel hidden">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-purple">
					<div class="row">
		                <div class="col col-xs-7">
		                    <h3 class="panel-title titlePanel" style="margin-top: 5%;">Document Status</h3>
		                </div>
		                <div class="col col-xs-5" style="padding: 0px;">
							<div class="dropup">
							  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #fff;border-bottom: 1px solid #000;padding: 5px 10px 5px 5px;">
							    Change Status
							    <span class="caret" style="margin-left: 5%;"></span>
							  </button>
							  <ul class="dropdown-menu pluset" aria-labelledby="dropdownMenu2" style="border:0px solid;">
							    <li><a href="#">Draft</a></li>
							    <li><a href="#">Publish</a></li>
							  </ul>
							</div>
		                </div>
		            </div>
				</div>
				<div class="panel-body">
					<div class="description-section">

					    	<table class="table tabedit">
					    	<thead class="head-size">
			                    <tr>

			                        <th class="" style="width: 0%;">No.</th>
			                        <th style="width: 60%;">Company Name</th>
		            	            <th  class="text-center" style="width: 50%;">Amount</th>
			                    </tr> 
			                </thead>
	    	                <tbody class=" text-decor ">
	                			<tr>
		                            <td class="nowrapping3 text-center">
										1
		                            </td>
		                            <td class="mod-jd-text">
		                            	Semen Gresik
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	30
		                            </td>
		                      	</tr>
		                      	<tr>
		                            <td class="nowrapping3 text-center">
										2
		                            </td>
		                            <td class="mod-jd-text">
		                            	Semen Padang
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	70
		                            </td>
		                      	</tr>
		                      	<tr>
		                            <td class="nowrapping3 text-center">
										3
		                            </td>
		                            <td class="mod-jd-text">
		                            	Semen Tonasa
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	150
		                            </td>
		                      	</tr>
		                      	<tr>
		                            <td class="nowrapping3 text-center">
										4
		                            </td>
		                            <td class="mod-jd-text">
		                            	Semen Thang Long
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	30
		                            </td>
		                      	</tr>
		                    	<tr>
		                            <td class="nowrapping3">

		                            </td>
		                            <td class="mod-jd-text">
		                            	Document Total
		                            </td>
		                            <td class="text-center mod-jd-text">
		                            	280
		                            </td>
		                      	</tr>
		                    </tbody>
		                </table>
						</div>
					</div>
				</div>

				<div class="panel shadowPanel">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-purple">
					<div class="row">
		                <div class="col col-xs-7">
		                    <h3 class="panel-title titlePanel" style="margin-top: 5%;">Document Popular</h3>
		                </div>
		            </div>
				</div>
				<div class="panel-body" style="padding: 5px 7px;">
					<div class="description-section">
					    	<table class="table tabedi">
					    	<thead class="head-size">
			                </thead>
	    	                <tbody class=" text-decor ">
	    	                	<?php foreach ($document_popular as $popular): ?>
	    	                	<tr>
		                            <td>
		                            	<a href="<?= site_url('web/document/popularDocument').'/'.$popular['ID'] ?>">
		                            		<h5 class="title-pop"><?php echo $popular['TITLE'] ?></h5>
		                            	</a>
		                            	<i class="fa fa-user userfaedit"></i>
	                            		<span class="fontuser">
	                            			<?php echo $popular['NAME'] ?>
	                            		</span>
	                            		<br>		                            	
		                            	<button class="btn btn-pop-view " type="button">
			                            	<i class="fa fa-eye" style="padding-right: 10%;"></i>
		                            		<span> <?php echo $popular['TOTAL_VIEW'] ?> </span>
										</button>
		                            </td>
		                      	</tr>
	    	                	<?php endforeach ?>
		                    </tbody>
		                </table>
						</div>
					</div>
				</div>

			<div class="panel shadowPanel hidden">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-none">
					<div class="row">
		                <div class="col col-xs-12">
		                    <h3 class="panel-title titlePanel">Jumlah Document per Opco</h3>
		                </div>
		            </div>
				</div>
				<div class="panel-body">
					<div class="description-section">

					    	<table class="table tabedit">

	    	                <tbody class=" text-decor ">
								<!-- canvas id="densityChart" width="600" height="400"></canvas> -->
								<canvas id="bar-chart1" width="600" height="400"></canvas>
								<script>
									new Chart(document.getElementById("bar-chart1"), {
									    type: 'bar',
									    data: {
									      labels: ["Gresik", "Padang", "Tonasa", "ThangLong"],
									      datasets: [
									        {
									          label: "Document",
									          backgroundColor: ["#E91E63", "#9C27B0","#2196F3","#4CAF50","#FFC107"],
									          data: [570, 730, 810, 980, 900]
									        }
									      ]
									    },
									    options: {
									      legend: { display: false },
									      title: {
									        display: true,
									        text: 'Jumlah Document per Opco'
									      }
									    }
									});

									// var densityCanvas = document.getElementById("densityChart");
									// 	Chart.defaults.global.defaultFontFamily = "Lato";
									// 	Chart.defaults.global.defaultFontSize = 12;

									// 	var densityData = {
									// 	  label: 'Amount',
									// 	  data: [200, 75, 400, 570, 730, 810, 980, 900],
									// 	  backgroundColor: 'rgba(255,193,7 ,1)',
									// 	  borderWidth: 0,
									// 	  yAxisID: "y-axis-density"
									// 	};

									// 	var gravityData = {
									// 	  label: 'Document',
									// 	  data: [100.0, 370.0, 327.0, 410.0, 573.0, 500.0, 742.0, 990.0],
									// 	  backgroundColor: 'rgba(255,87,34 ,1)',
									// 	  borderWidth: 0,
									// 	  yAxisID: "y-axis-gravity"
									// 	};

									// 	var planetData = {
									// 	  labels: ["Form", "Pedo.", "Pros.", "I.K.", "BoarMan"],
									// 	  datasets: [densityData, gravityData]
									// 	};

									// 	var chartOptions = {
									// 	  scales: {
									// 	    xAxes: [{
									// 	      barPercentage: 1,
									// 	      categoryPercentage: 0.6
									// 	    }],
									// 	    yAxes: [{
									// 	      id: "y-axis-density"
									// 	    }, {
									// 	      id: "y-axis-gravity"
									// 	    }]
									// 	  }
									// 	};

									// 	var barChart1 = new Chart(densityCanvas, {
									// 	  type: 'bar',
									// 	  data: planetData,
									// 	  options: chartOptions
									// 	});
								</script>
		                    </tbody>
		                </table>
						</div>
					</div>
			  <!-- Table -->
			</div>

			<div class="panel shadowPanel">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-none">
					<div class="row">
		                <div class="col col-xs-12">
		                    <h3 class="panel-title titlePanel">Visitor Amount</h3>
		                </div>
<!-- 		                <div class="col col-xs-5">
							<div class="dropup">
							  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #fff;border-bottom: 1px solid #000;padding: 5px 10px 5px 5px;">
							    Pilih Status
							    <span class="caret" style="margin-left: 5%;"></span>
							  </button>
							  <ul class="dropdown-menu pluset" aria-labelledby="dropdownMenu2" style="">
							    <li><a href="#">Active</a></li>
							    <li><a href="#">Smart</a></li>
							    <li><a href="#">Phone</a></li>
							  </ul>
							</div>
		                </div> -->
		            </div>
				</div>
				<div class="panel-body ">
					<div class="description-section">

					    	<table class="table tabedit">
<!-- 					    	<thead class="head-size">
			                    <tr>
			                        <th class="" style="width: 0%;">No.</th>
			                        <th style="width: 60%;">Nama Perusahaan</th>
		            	            <th  class="text-center" style="width: 50%;">Jumlah</th>
			                    </tr> 
			                </thead> -->
	    	                <tbody class=" text-decor ">
								<!-- canvas id="densityChart" width="600" height="400"></canvas> -->
								<canvas id="bar-chart-grouped" width="600" height="400"></canvas>
								<script>
								
								var M1 = "<?= $dataVisitor[4]['bulan'] ?>";
								var T1 = "<?= $dataVisitor[4]['hasil']['TOTAL'] ?>";
								var M2 = "<?= $dataVisitor[3]['bulan'] ?>";
								var T2 = "<?= $dataVisitor[3]['hasil']['TOTAL'] ?>";
								var M3 = "<?= $dataVisitor[2]['bulan'] ?>";
								var T3 = "<?= $dataVisitor[2]['hasil']['TOTAL'] ?>";
								var M4 = "<?= $dataVisitor[1]['bulan'] ?>";
								var T4 = "<?= $dataVisitor[1]['hasil']['TOTAL'] ?>";
								var M5 = "<?= $dataVisitor[0]['bulan'] ?>";
								var T5 = "<?= $dataVisitor[0]['hasil']['TOTAL'] ?>";
								

								new Chart(document.getElementById("bar-chart-grouped"), {
								    type: 'bar',
								    data: {
								      labels: [M1, M2, M3, M4, M5],
								      datasets: [
								        {
								          label: "Visitor",
								          backgroundColor: "#FFE099",
								          data: [T1,T2,T3,T4,T5]
								        }
								      ]
								    },
								    options: {
								      title: {
								        display: true,
								        text: 'Visitor Amount (people)'
								      }
								    }
								});
									// new Chart(document.getElementById("bar-chart1"), {
									//     type: 'bar',
									//     data: {
									//       labels: ["Gresik", "Padang", "Tonasa", "ThangLong"],
									//       datasets: [
									//         {
									//           label: "Document",
									//           backgroundColor: ["#E91E63", "#9C27B0","#2196F3","#4CAF50","#FFC107"],
									//           data: [570, 730, 810, 980, 900]
									//         }
									//       ]
									//     },
									//     options: {
									//       legend: { display: false },
									//       title: {
									//         display: true,
									//         text: 'Jumlah Document per Opco'
									//       }
									//     }
									// });

									// var densityCanvas = document.getElementById("densityChart");
									// 	Chart.defaults.global.defaultFontFamily = "Lato";
									// 	Chart.defaults.global.defaultFontSize = 12;

									// 	var densityData = {
									// 	  label: 'Amount',
									// 	  data: [200, 75, 400, 570, 730, 810, 980, 900],
									// 	  backgroundColor: 'rgba(255,193,7 ,1)',
									// 	  borderWidth: 0,
									// 	  yAxisID: "y-axis-density"
									// 	};

									// 	var gravityData = {
									// 	  label: 'Document',
									// 	  data: [100.0, 370.0, 327.0, 410.0, 573.0, 500.0, 742.0, 990.0],
									// 	  backgroundColor: 'rgba(255,87,34 ,1)',
									// 	  borderWidth: 0,
									// 	  yAxisID: "y-axis-gravity"
									// 	};

									// 	var planetData = {
									// 	  labels: ["Form", "Pedo.", "Pros.", "I.K.", "BoarMan"],
									// 	  datasets: [densityData, gravityData]
									// 	};

									// 	var chartOptions = {
									// 	  scales: {
									// 	    xAxes: [{
									// 	      barPercentage: 1,
									// 	      categoryPercentage: 0.6
									// 	    }],
									// 	    yAxes: [{
									// 	      id: "y-axis-density"
									// 	    }, {
									// 	      id: "y-axis-gravity"
									// 	    }]
									// 	  }
									// 	};

									// 	var barChart1 = new Chart(densityCanvas, {
									// 	  type: 'bar',
									// 	  data: planetData,
									// 	  options: chartOptions
									// 	});
								</script>
		                    </tbody>
		                </table>
						</div>
					</div>
			  <!-- Table -->
			</div>



			<div class="panel shadowPanel hidden">
  				<!-- Default panel contents -->
				<div class="panel-heading panel-color-none">
					<div class="row">
		                <div class="col col-xs-7">
		                    <h3 class="panel-title titlePanel">Document Access</h3>
		                </div>
		                <div class="col col-xs-5">
<!-- 							<div class="dropup">
							  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color: #fff;border-bottom: 1px solid #000;padding: 5px 10px 5px 5px;">
							    Pilih Status
							    <span class="caret" style="margin-left: 5%;"></span>
							  </button>
							  <ul class="dropdown-menu pluset" aria-labelledby="dropdownMenu2" style="">
							    <li><a href="#">Active</a></li>
							    <li><a href="#">Smart</a></li>
							    <li><a href="#">Phone</a></li>
							  </ul>
							</div> -->
		                </div>
		            </div>
				</div>
				<div class="panel-body">
					<div class="description-section">

					    	<table class="table tabedit">
								<div class="text-center">
									<h2 style="margin: 0px">5380</h2>
									<p>Employees <br>Access Document</p>
								</div>
								<div>
									<div class="progress">
									  <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%"> 70% (5380) from 7000
									    <span class="sr-only">40% Complete</span>
									  </div>
									</div>
								</div>
								<div class="">
									<h5 style=""><span class="label label-default label-colr hidden">Employess Access</span></h5>
									<h5 style=""><span class="label label-default label-colr-2 hidden">Total Employess</span></h5>
									
								</div>
			                </table>
						</div>
					</div>
			  <!-- Table -->
			</div>


		</div>
		</div>

	<div class="panel-group col-md-8" id="accordion" style="margin-bottom:25%;padding-left: 0px;padding-right: 10px">
	    <div class="panel panel-default">	
	    <div id="collapse1" class="panel-collapse collapse" style="border: 1px solid #27ae60; border-radius: 3px; box-shadow: 0px 5px 10px rgba(0,0,0,0.2);">
	        <div class="panel-body" style="padding: 0px 0px 0px 0px;">     
	        	<div class="tabs">
		    		<ul class="tab-links"  style="border-bottom: 2px solid #E0E0E0;">
		        		<li <?php if( (strcmp($jenisFilter,"")!=0) || (strcmp($lokasiFilter,"")==0 && (strcmp($tagFilter,"")==0))) echo "class='active'";?>><a href="#tabJenis">Jenis</a></li>
		        		<li <?php if(strcmp($lokasiFilter,"")!=0) echo "class='active'";?> style="display:none"><a href="#tabLokasi">Lokasi</a></li>
		        		<li class="hidden" <?php if(strcmp($tagFilter,"")!=0) echo "class='active'";?>><a href="#tabTag">Tags</a></li>
		        	</ul>
			        <div class="tab-content" style="max-height:250px;overflow-y: auto;">
				        <div id="tabJenis" class="tab <?php if( (strcmp($jenisFilter,"")!=0) || (strcmp($lokasiFilter,"")==0 && (strcmp($tagFilter,"")==0))) echo "active";?>">
				        	<table style="width:100%;">
				        		<?php
				        			$ShowingJenis = 0;
				        			// echo '<pre>';
				        			// var_dump($jenis);
				        			// die();
				        			foreach($jenis as $obj){
				        				if($ShowingJenis%2==0){
					        				echo "<tr>";
					        			}
					        			if(strcmp($obj['type'],$jenisFilter)==0)
					        				echo "<td style='background-color:#eee;'><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterJenis(".'"'.$obj['type'].'"'. ")'><b> ". $obj['type'] . "</b></a></td>";
					        			else
					        				echo "<td><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterJenis(".'"'.$obj['type'].'"'. ")'><b> ". $obj['type'] . "</b></a></td>";
					        			if($ShowingJenis%2==1){
					        				echo "</tr>";
					        			}
					        			$ShowingJenis++;
					        		}
				        		?>
				        	</table>
				        </div>
				        <div id="tabLokasi" class="tab <?php if(strcmp($lokasiFilter,"")!=0) echo "active";?>">
				        	<table style="width:100%;">
				        		<?php
				        			$ShowingLokasi = 0;
				        			foreach($lokasi as $obj){
				        				if($ShowingLokasi%2==0){
					        				echo "<tr>";
					        			}
					       				if(strcmp($obj['name'],$lokasiFilter)==0)
					        				echo "<td style='background-color:#eee;'><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterLokasi(".'"'.$obj['name'].'"'. ")'><b> ". $obj['name']. "</b></a></td>";
					        			else
					        				echo "<td><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterLokasi(".'"'.$obj['name'].'"'. ")'><b> ". $obj['name']. "</b></a></td>";
					        			if($ShowingLokasi%2==1){
					        				echo "</tr>";
					        			}
					        			$ShowingLokasi++;
					        		}
				        		?>
				        	</table>
				        </div>
				    <div id="tabTag" class="tab <?php if(strcmp($tagFilter,"")!=0) echo "active";?>">
				        	<table style="width:100%;">
				        		<?php
				        			$ShowingTag = 0;
				        			foreach($tags as $obj){
					        			if($ShowingTag%2==0){
					        				echo "<tr>";
					        			}
					        			if(strcmp($obj['name'],$tagFilter)==0)
					        				echo "<td style='background-color:#eee;'><i class='fa fa-hashtag' style='color:#27ae60;'></i><a href='#' style='color:#000;background-color:#eee;' onClick='filterTag(".'"'.$obj['name'].'"'. ")'><b> ". $obj['name'] . "</b></a></td>";
					        			else
					        				echo "<td><i class='fa fa-hashtag' style='color:#27ae60;'></i><a href='#' style='color:#000;' onClick='filterTag(".'"'.$obj['name'].'"'. ")'><b> ". $obj['name'] . "</b></a></td>";
					        			if($ShowingTag%2==1){
					        				echo "</tr>";
					        			}
					        			$ShowingTag++;
					        		}
				        		?>
				        	</table>
				        </div>
				    </div>
				</div>
			<div class="col-md-12" style="background-color:#F1F8E9;" >
				<form method="post" name="filter_form" id="filter_form" action="<?= site_url('web/document/setSessionSearch') ?>" style="">
	        		<div class="upload-new" style="background-color:#F1F8E9;">
		        		<div style="margin-left:10px;margin-top:10px">
							<!-- <input type="hidden" id="numPage" name="numPage" value="<?php echo $page;?>">
				        	<input type="hidden" id="idOrganisasi" name="idOrganisasi" value="<?php echo $idOrganisasi;?>">
				        	<input type="hidden" id="lokasiFilter" name="lokasiFilter" value="<?php echo $lokasiFilter;?>">
				        	<input type="hidden" id="jenisFilter" name="jenisFilter" value="<?php echo $jenisFilter;?>">
				        	<input type="hidden" id="tagFilter" name="tagFilter" value="<?php echo $tagFilter;?>"> -->
				        	<div>
						        <div class="upload-new-input" style="background-color:#F1F8E9;">
			                        <i class="fa fa-search" ></i>
			                        <input type="text" id="title" name="title" value="<?php echo $_SESSION['document_search']['title'];?>" class="form-control input-sm" placeholder="Document Title" style="font-size:15px;display:inline;width:41%;margin-left:5px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
			                        <i class="fa fa-user-circle-o" style="padding-left: 30px;"></i>
			                        <input type="text" id="user" name="user" value="<?php echo $_SESSION['document_search']['user'];?>" class="form-control input-sm" placeholder="Document Author" style="font-size:15px;display:inline;width:41%;margin-left:5px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
								</div>
							</div>
			            </div>
			        </div>
			        
			        <button type='submit' value='Filter' class="btn pull-right btn-filt">
			        	Cari Dokumen
			        	<!-- <label style="color:#fff;letter-spacing: 0.7px;">Cari Dokumen</label> -->
			        </button>
			        <?php if(strcmp($jenisFilter,"")!=0 || strcmp($lokasiFilter,"")!=0 || strcmp($tagFilter,"")!=0 || strcmp($pemilikFilter,"")!=0 || strcmp($titleFilter,"")!=0 || $isFiltered==2 and 1==0){ ?>
				        <a href="<?= site_url('web/document/removeDocumentFilter'); ?>" class="btn btn-danger pull-right" style="margin:0px 30px 15px;">Hilangkan Filter
				        </a>
			        <?php } ?>
			        <?php if ($isFiltered) { ?>
			        	<a href="<?= site_url('web/document/removeDocumentFilter'); ?>" class="btn btn-danger pull-right" style="margin:0px 30px 15px;">Hilangkan Filter
				        </a>
			        <?php } ?>
			    </form>
			</div>
	        </div>
	      </div>
	    </div>
	  </div> 
	  <div style="display:none">
	  	<p style="color:#424242; font-weight: bold; font-size: 15px;letter-spacing: .5px;">
	  		Pencarian Berdasarkan Jenis Dokumen 
	  	</p>
	  </div>
	  <br>
	<div style="margin-bottom: 6%;display:none">
		<a class="mousebtn01 hidden" style="margin-right: 1%" onClick="openNewUploadBox()">
			<i class="fa fa-plus" aria-hidden="true" style="font-size: 25px;">
				<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">Submit Document
				</div>
			</i>
		</a>
		<a class="mousebtn01 hidden" style="margin-right: 1%" onClick="openNewUploadBox()">
			<i class="fa fa-filter" aria-hidden="true" style="font-size: 25px;">
				<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">Filter
				</div>
			</i>
		</a>
		<a class="mousebtn01green" style="margin-right: 1%">
			<i class="fa fa-question" aria-hidden="true" style="font-size: 25px;">
				<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">Help
				</div>
			</i>
		</a>
		
		
	</div>
<!-- 	</div> -->

<div class="row hidden">
	<div class="col-md-8">
        	<div class="panel shadowPanel">
  				<!-- Default panel contents -->
  				<!-- untuk notifikasi pakai class ini "panel-color-blues-notif" -->
				<div class="panel-heading panel-color-blues" id="notificationx" style="background-color:<?= ($total_document<0) ? "#FF685F" : "white"; ?>">
					<div class="row">
		                <div class="col col-xs-4">
		                    <h3 class="panel-title titlePanel">List Document</h3>
		                </div>
		                <div class="col col-xs-8 text-right">
						<h7 class="" style="font-size: 13px; letter-spacing: 0.5px;">You have <?= $total_document ?> document to be checked</h7>	
							<i style="margin-right: 5%;" class="fa fa-bell notifcolr"></i>
		                </div>
		            </div>
				</div>
			  	<table class="table tabedit">
			    	<thead class="head-size">
	                    <tr>
	                        <th style="width: 4%;"></th>
	                        <th style="width: 20%;">Nama Dokumen</th>
	                        <th style="width: 30%;">Status</th>
            	            <th style="width: 20%;">Aksi</th>
	                    </tr> 
	                </thead>
	                <tbody class=" text-decor ">
	                	<?php
	                		foreach ($data as $key => $value) {
	                	?>		
	                			<tr>
		                            <td align="center">
		                            	<img src="<?php echo site_url("assets/web/images/doc.png");?>">
		                            </td>
		                            <td class="nowrapping"><?= $value['title'] ?></td>
		                            <td><span class="label label-notif"><?= $value['status'] ?></span></td>
		                            <td>
		                            	<a class="btnTable btn-lihatview" href="javascript:View(<?= $value['document_id'] ?>)"><i class="fa fa-eye"></i></a>
		                        		<a class="btnTable btn-detailainnya" href="<?= site_url('web/document/detail/').'/'.$value['document_id'] ?>"><i class="fa fa-ellipsis-h"></i></a>
		                        	</td>
		                      	</tr>

	                	<?php	}
	                	?>

                    </tbody>
			  	</table>			
			</div>
	</div>

	
	</div>

			        </button>
			        <?php if(strcmp($jenisFilter,"")!=0 || strcmp($lokasiFilter,"")!=0 || strcmp($tagFilter,"")!=0 || strcmp($pemilikFilter,"")!=0 || strcmp($titleFilter,"")!=0 || $isFiltered==2){ ?>
				        <a href="<?= site_url('web/document/removeDocumentFilter'); ?>" class="btn btn-danger pull-right" style="margin:0px 30px 15px;">Hilangkan Filter
				        </a>
			        <?php } ?>
			    </form>
			</div>
	        </div>
	      </div>
	    </div>
	  </div>
<iframe id="frame_download" style="display:none;"></iframe>
<div class="menu" style="padding:5px;z-index:1000;">
    <div id="view" class="menu-item" onClick="View()">
    	<p class="menu-item-detail">View<i class="fa fa-eye" aria-hidden="true" style="float:right;"></i></p>
    </div>
    <div class="menu-item" onClick="Download()">
    	<p class="menu-item-detail">Download <i class="fa fa-download" aria-hidden="true" style="float:right;"></i></p>
    </div>
    <div></div>
    <div class="menu-item" onClick="Edit()">
    	<p class="menu-item-detail">Upload New Version <i class="fa fa-pencil-square-o" aria-hidden="true" style="float:right;"></i></p>
    </div>
    <div class="menu-item" onClick="Revision()">
    	<p class="menu-item-detail">Manage Version <i class="fa fa-recycle" aria-hidden="true" style="float:right;"></i></p>
    	</div>
    <div class="menu-item" onClick="openShareBox()">
    	<p class="menu-item-detail">Share <i class="fa fa-share-alt" aria-hidden="true" style="float:right;"></i></p>
    </div>
</div>
<style>
	
</style>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>

<style>
				.slick-initialized .swipe-tab-content {
					  position: relative;
					  min-height: 365px;
					}
					@media screen and (min-width: 767px) {
					  .slick-initialized .swipe-tab-content {
					    min-height: 500px;
					  }
					}
					.slick-initialized .swipe-tab {
					  display: -webkit-box;
					  display: -webkit-flex;
					  display: -ms-flexbox;
					  display: flex;
					  -webkit-box-align: center;
					  -webkit-align-items: center;
					      -ms-flex-align: center;
					          align-items: center;
					  -webkit-box-pack: center;
					  -webkit-justify-content: center;
					      -ms-flex-pack: center;
					          justify-content: center;
					  height: 50px;
					  background: none;
					  border: 0;
					  color: #757575;
					  cursor: pointer;
					  text-align: center;
					  border-bottom: 2px solid transparent;
					  -webkit-transition: all 0.5s;
					  transition: all 0.5s;
					}
					.slick-initialized .swipe-tab:hover {
					  color: #000;
					}
					.slick-initialized .swipe-tab.active-tab {
				      box-shadow: 0px 2px 7px rgba(0,0,0,0.1);
			          background-color: #fff;
					  color: #000;
					  font-weight: bold;
					}

					.main-container {
					  padding: 15px;
					  background: #f1f1f1;
					}
			</style>

	<script>
				$(function () {
	'use strict';

	var $swipeTabsContainer = $('.swipe-tabs'),
		$swipeTabs = $('.swipe-tab'),
		$swipeTabsContentContainer = $('.swipe-tabs-container'),
		currentIndex = 0,
		activeTabClassName = 'active-tab';

	$swipeTabsContainer.on('init', function(event, slick) {
		$swipeTabsContentContainer.removeClass('invisible');
		$swipeTabsContainer.removeClass('invisible');

		currentIndex = slick.getCurrent();
		$swipeTabs.removeClass(activeTabClassName);
       	$('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
	});

	$swipeTabsContainer.slick({
		//slidesToShow: 3.25,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		infinite: false,
		swipeToSlide: true,
		touchThreshold: 10
	});

	$swipeTabsContentContainer.slick({
		asNavFor: $swipeTabsContainer,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		infinite: false,
		swipeToSlide: true,
    draggable: false,
		touchThreshold: 10
	});


	$swipeTabs.on('click', function(event) {
        // gets index of clicked tab
        currentIndex = $(this).data('slick-index');
        $swipeTabs.removeClass(activeTabClassName);
        $('.swipe-tab[data-slick-index=' + currentIndex +']').addClass(activeTabClassName);
        $swipeTabsContainer.slick('slickGoTo', currentIndex);
        $swipeTabsContentContainer.slick('slickGoTo', currentIndex);
    });

    //initializes slick navigation tabs swipe handler
    $swipeTabsContentContainer.on('swipe', function(event, slick, direction) {
    	currentIndex = $(this).slick('slickCurrentSlide');
		$swipeTabs.removeClass(activeTabClassName);
		$('.swipe-tab[data-slick-index=' + currentIndex + ']').addClass(activeTabClassName);
	});
});
	</script>

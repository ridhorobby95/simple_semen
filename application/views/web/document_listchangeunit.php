<style type="text/css">
    #rightSidebar {
        display: none!important;
    }
    #mainContent {
        margin-bottom: 50vh;
    }
</style>
<script>
    $("#mainContent").removeClass('col-sm-9');
    $("#mainContent").addClass('col-sm-12');
</script>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/bulkChangeunit', array('id' => 'form_data')) ?>
                        <div class="col-sm-12">
                            <a class="pull-right" href="<?= site_url('web/document/listchangeunit') ?>/<?= ($show_all) ? '0' : '1' ?>" style="margin-bottom: 10px">
                                <button type="button" class="btn btn-sm btn-<?= ($show_all) ? 'danger' : 'secondary' ?> btn-rad" style="margin-bottom: 10px">
                                    <?= ($show_all) ? 'Hide' : 'Show' ?> Problematic Positions
                                </button>
                            </a>
                             <button type="button" class="btn mousedrag01" style="margin-bottom: 10px" data-toggle="modal" data-target=".bs-example-modal-lg">
                                <i class="fa fa-filter" aria-hidden="true">
                                </i>
                                Filter
                                   
                                </button>
                            <?php if (isset($_SESSION['filter_changeunit'])){ ?>
                                 
                                        <a href="<?= site_url('web/document/removeChangeUnitFilter')?>">
                                            <button type="button" class="btn mousedrag01"  style="margin-bottom: 10px; background-color: #FFB300; color:#FFFFFF;     border: 1.5px solid #FFFFFF; ">
                                                Clear Filter
                                            </button>
                                        </a>
                                <div class="col-md-12" style="margin-bottom: 10px">
                                    <b>Filtered by
                                        <?php
                                            $counter = 1; 
                                            $total = count($_SESSION['filter_changeunit']);
                                        ?>
                                        <?php if (isset($_SESSION['filter_changeunit']['keyword'])): ?>
                                            Keyword
                                            <?php if ($total>$counter) { ?>
                                                ,
                                            <?php 
                                                $counter++;
                                            } ?>
                                        <?php endif ?>
                                        <?php if (isset($_SESSION['filter_changeunit']['type'])): ?>
                                            Document Type
                                            <?php 
                                            $total--;
                                            if ($total>$counter) { ?>
                                                ,
                                            <?php 
                                                $counter++;
                                            } ?>
                                        <?php endif ?>
                                        <?php if (isset($_SESSION['filter_changeunit']['unitkerja'])): ?>
                                            Work Unit
                                            <?php 
                                            $total--;
                                            if ($total>$counter) { ?>
                                                ,
                                            <?php 
                                                $counter++;
                                            } ?>
                                        <?php endif ?>
                                    </b>
                                </div>
                                
                            <?php }?>

                           

                            <table class="table tabedit">
                                <thead class="head-size">
                                    <tr style="color:blue">
                                        <th style="width: 15%;border:1px solid;text-align: center">Code</th>
                                        <th style="width: 15%;border:1px solid;text-align: center">Document</th>
                                        <th style="width: 20%;border:1px solid;text-align: center">Workunit</th>
                                        <th style="width: 10%;border:1px solid;text-align: center">Old Creator</th>
                                        <th style="width: 10%;border:1px solid;text-align: center">New Creator</th>
                                        <th style="width: 10%; padding-left: 0px;border:1px solid;text-align: center">Old Approver</th>
                                        <th style="width: 10%; padding-left: 0px;border:1px solid;text-align: center">New Approver</th>
                                        <th style="width: 10%;border:1px solid;text-align: center">OK ? </th>
                                    </tr> 
                                </thead>
                                <tbody class=" text-decor ">
                                    <?php foreach ($data as $k_data => $v_data) {
                                    ?>
                                        <tr>
                                            <td style="border:1px solid;text-align: center">
                                                <?= $v_data['code']?>
                                            </td>
                                            <td style="border:1px solid;text-align: center">
                                                <?= $v_data['title']?>
                                            </td>
                                            <td style="border:1px solid;text-align: center">
                                                <?= $v_data['unitkerja_name']?>
                                            </td>
                                            <td style="border:1px solid;text-align: center">
                                                <?= $v_data['old']['creator_name']?>
                                            </td>
                                            <td style="border:1px solid;text-align: center">
                                                <?= $v_data['new']['creator_name']?>
                                            </td>
                                            <td style="border:1px solid;text-align: center">
                                                <?= $v_data['old']['approver_name']?>
                                            </td>
                                            <td style="border:1px solid;text-align: center">
                                                <?= $v_data['new']['approver_name']?>
                                            </td>
                                            <td style="border:1px solid;text-align: center">
                                                <?php if ($v_data['is_problematic']!=1) { ?>
                                                    <input type="checkbox" name="approve[]" value="<?= $v_data['id'] ?>">
                                                <?php } else { ?>
                                                    <a class="btnTable btn-lihatview" href="<?= site_url('web/document/changeunit').'/'.$v_data['document_id'] ?>" data-toggle="tooltip" data-placement="top" title="Detail Problematic Position" target="_blank">
                                                        Detail
                                                    </a>
                                                <?php } ?>
                                            </td>  
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <div class="modal-footer" align="center">
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> Submit</button>
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
          <div class="modal-dialog modal-lg">
            <div class="modal-content" style="padding-left: 5%;">
              <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="row">
                        <div class="col-md-10">
                            <h2 style="margin: 0%;">
                                Filter
                            </h2>
                        </div>
                        <div class="col-md-2" style="text-align: center">
                            <i class='fa fa-times-circle onhovering hidden' aria-hidden='true' style='' data-dismiss="modal" aria-hidden="true">
                                
                            </i>
                        </div>
                    </div>
                </div>
              </div>
              <br>
            <form method="post" name="filter_form" id="filter_form" action="<?= site_url('web/document/setFilterChangeUnit').'/'.$show_all ?>" style="">
                <input type="hidden" id="numPage" name="numPage" value="<?php echo $_SESSION['page'];?>">
                <div class="row">
                
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Keyword
                                </h5>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input id="title" name="keyword" type="text" class="form-control input-md inputplus" id="title" placeholder="Write Document Title or Code" value="<?= $_SESSION['filter_changeunit']['keyword'] ?>">
                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_title()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Document type
                                </h5>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                    <?php 
                                    foreach ($variables['document_types'] as $k => $type) {
                                        $types[$type['id']] = $type['type'];
                                    }
                                    unset($types[""]);
                                    echo form_dropdown('type[]', $types, $_SESSION['filter_changeunit']['arr_type'], 'id="type" class="form-control input-sm select2 type" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" '); ?>
                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_type()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <h5>
                                    Work Unit
                                </h5>
                            </div>
                            <div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                    <?php
                                        $unitkerja = $variables['unitkerja'];
                                        unset($unitkerja[0]);
                                        echo form_dropdown('unitkerja[]', $unitkerja,$_SESSION['filter_changeunit']['arr_unitkerja'], 'id="unitkerja" class="form-control input-sm select2 unitkerja" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" ');
                                    ?>
                                    <div class="input-group-addon no-bg-addon" onclick="javascript:remove_workunit()">
                                        <i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
                            
                                        </i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <button type="button" class="btn btn-filto" data-dismiss="modal">
                                    Cancel
                                </button>
                            </div>              
                            <div class="col-md-3 text-right">
                                <button type="submit" class="btn btn-filtu">
                                    Apply Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              <br>
            </form>
            </div>
          </div>
        </div>
<?php include('_document_js.php'); ?>
<script type="text/javascript">
    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

    function remove_title(){
        document.getElementById('title').value=null;
    }
    function remove_type(){
        $('#type').val(null).trigger("change");
    }
    function remove_workunit(){
        $('#unitkerja').val(null).trigger("change");
    }

</script>

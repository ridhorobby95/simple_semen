
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . 'updateClause/' . $data['code'], array('id' => 'form_data')) ?>
                    <input type="hidden" name="id_clause" value="<?php echo $data['id'] ?>">
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Clause</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'clause', 'value' => $data['klausul'], 'class' => 'form-control input-sm', 'id' => 'clause')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-3">Clause Code</label>
                            <div class="col-sm-9">
                                <?php echo form_input(array('name' => 'code', 'value' => $data['code'], 'class' => 'form-control input-sm', 'id' => 'code')) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button id="btn_simpan" type="submit" class="btn btn-sm btn-success btn-rad pull-right" title="Save" style="margin-left:0.5%;">
                            <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                            <span>Save</span>
                        </button>
                        <!-- <a class="post-right pull-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a> -->
                   

                    </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var selected_color = "<?= $data['color'] ? $data['color'] : 0 ?>";
    var selected_logo = "<?= $data['logo'] ? $data['logo'] : 0 ?>";
    var name = "<?= $data['name'] ?>";
    $(function() {
    });

    function goBack(id) {
        location.href = "<?php echo site_url($path . $class) ?>/editclause/"+id;
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

    function upload_excel(){
        $("#upload_modal").modal('show');
    }

    $(".klausul_iso").select2({
        placeholder: 'Input Klausul Iso...',
        minimumResultsForSearch: Infinity,
        tags: true,
        width: '100%',
    });
    $(".klausul_iso").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);

        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });
</script>
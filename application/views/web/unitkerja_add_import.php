<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/import/'.$type, array('id' => 'form_data')) ?>               
                        <div class="col-sm-8">
                            <div class="row bord-bottom">
                            <label for="migration data" class="col-sm-4">Metadata</label>
                                <div class="col-sm-8">
                                    <div class="btn btn-default btn-sm" onclick="$('#file_metadata_upload').click()"><span class="fa fa-file" style="cursor:pointer;color:#777;"></span> &nbsp;Upload Here</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="row bord-bottom">
                            <label for="migration data" class="col-sm-4">Metadata file</label>
                                <div class="col-sm-8" style="padding-left: 0px">
                                    <div id="metadata_preview" name="metadata_preview" class="col-md-12"><?= ($metadata=='') ? '-' : $metadata ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right" style="margin-top: 10px">
                            <a class="post-right">
                                <button type="submit" class="btn btn-success post-footer-btn btn-md">Start Import</button>
                            </a>
                        </div>                     
                    </form>

                    <form method="post" name="file_metadata_form" id="file_metadata_form" enctype="multipart/form-data" action="<?= site_url('web/unitkerja/uploadMetadata') ?>" class="hidden">
                        <input type="file" id="file_metadata_upload" name="file_metadata_upload" class="hidden">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.form.js') ?>"></script>

<script type="text/javascript">

    $('#file_metadata_upload').on('change', function() {
        $('#file_metadata_form').ajaxForm({
            target: '#metadata_preview',
            error: function(e) {
                //target: '#images_preview';
                alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>/";
    }

    function goSave() {
        $("#form_data").submit();
    }

</script>
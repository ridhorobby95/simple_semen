<?php
	// echo '<pre>';
 //                            vaR_dump($data['reviewer']);
 //                            die();
	$hide_btn_lihat_dokumen= "";
	$hide_upload = '';
	$hide_editor='hidden';
	if ($data['is_upload']=='0'){
		$hide_editor = '';
		$hide_upload = 'hidden';
		$hide_btn_lihat_dokumen= "hidden";
	}
	$hide_btn_approve = 'hidden';
	include('document_detail_form_modal.php');
	include('document_modal_related.php');
?>

<script type="text/javascript">
    $(document).ready(function() {
        $(".select2").select2({
            // width: '60%'
        });
        $(".select2").on("select2:select", function (evt) {
            var element = evt.params.data.element;
            var $element = $(element);
            $element.detach();
            $(this).append($element);
            $(this).trigger("change");
        });
        $(".input_reviewer").select2({
        	placeholder: 'Write Reviewer Name Here',
        	width: '80%'
        });
        $(".input_reviewer_review").select2({
        	placeholder: 'Write Reviewer Name Here',
        	width: '80%'
        });
        $(".input_reviewer_reviewed").select2({
        	placeholder: 'There no one reviewed yet',
        	width: '80%'
        });
        $(".input_reviewer2").select2({
        	placeholder: 'Write Reviewer Name Here',
        	width: '100%'
        });
        $(".input_reviewer_reviewed2").select2({
        	placeholder: 'There no one reviewed yet',
        	width: '100%'
        });
        $(".attribute_requirement .attribute_clause").select2({
        	placeholder: 'Choose...',
        	width: '100%'
        });
        $("#typeSearch").select2({
        	placeholder: 'Choose Document Type',
        	width: '90%'
        });
    });
</script>

<script>

	var saveCommand = {
	    id : 'saveCommand',
	    text : 'Save', 
	    icon : "<?= site_url('assets/web/images/save-new.png') ?>",
	    action : function () {
	        save_as_draft("<?= $data['document_id'] ?>");
	        // write custom functionality to occur when the button is clicked here.
	    }
	};

	var textboxio_config = {    
			ui: {
	        toolbar: {
	            // groups can be defined inline as part of the toolbar items
	            items: [
	            		{
	                        label: 'Custom commands group',
	                        items: [saveCommand]
	                    },
	                    'undo','insert', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools', // other toolbar items
	                    
	            ]
	        }
	    }
	};

	// langsung next tanpa save
	function continueToNext(id, urutan, status){
		if (confirm("Are you sure to continue ?\nAny of your changes will not be saved (remember to save then continue)")){
			set_urutan(id, urutan, status);
		}
	}

	function set_urutan(id, urutan, status){
		if (status=='R'){
			var text = document.getElementById('revise_reason_textarea').value;
		}else if (status=='C'){
			var text = document.getElementById('reason_textarea').value;
		}
		if (text!='') {
			$('.loading').addClass('fa fa-spinner fa-spin');
	        $('.loadings').show();
			$.ajax({
				url : '<?= site_url('web/document/ajaxseturutan') ?>',
				type: 'post',
				cache: false,
				data: {"document_id":id,"workflow_urutan":urutan, 'status':status, 'text': text},
				success: function(respon){
					if (respon != 'ERROR') {
						// if (urutan==-1)
						if (status=='R'){
							$('#form_review_atribut').submit();
						}
						// if (workflow_id=='2' && workflow_urutan=='1'){
						// 	$("#btn_submit_evaluation").click();
						// }
						if (status=='C'){
							// window.location.replace("<?//= site_url('web/document/dashboard') ?>");
							$("#submit_reject").click();
						} else {
							window.location.replace("<?= site_url('web/document/detail') ?>/"+respon);
						}
						
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		} else {
			alert('Reason must be filled!');
		}
		
	}

	$("#btn_cancel_ok").on('click', function() {
	    window.location.replace("<?= site_url('web/document') ?>");
	});
	function set_cancel(id, status){
		var text = document.getElementById('cancel_reason_textarea').value;
		if (text!='') {
			$('.loading').addClass('fa fa-spinner fa-spin');
	        $('.loadings').show();
			$.ajax({
				url : '<?= site_url('web/document/ajaxCancel') ?>',
				type: 'post',
				cache: false,
				data: {"document_id":id,'status':status, 'text': text},
				success: function(respon){
					if (respon != 'ERROR') {
						// document.location.reload();
						$('.loading').removeClass('fa fa-spinner fa-spin');
						$('.loadings').hide();
						$("#cancel_modal").modal('hide');
						// $("#cancel_box_modal").modal('show');
						$("#cancel_box_modal").modal({
							backdrop: 'static',
							keyboard: true,
							show: true
						});
						// console.log(respon);
						$("#cancel_text").text("The Document is canceled because "+respon+"");
							// window.location.replace("<?= site_url('web/document/detail') ?>/"+respon);
						
						
					}
					else {
						alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
					}
				}
			});
		} else {
			alert('Reason must be filled!');
		}
		
	}

	function reject_document(){
		$("#reject_modal").modal('show');
	}
	function cancel_document(){
		$("#cancel_modal").modal('show');
	}

	function add_reviewer_document(is_continue_reviewer = 1){
		document.getElementById('is_continue_reviewer').value=is_continue_reviewer;
		$("#add_reviewer_modal").modal('show');
	}

	function revise_document(){
		$("#revise_modal").modal('show');
	}

	function upload_document(){
		$("#upload_modal").modal('show');
	}

	function revise_create_here(id){
		// $("#upload_modal").modal('show');
		// alert('created here');
		window.location.replace("<?= site_url('web/document/changeUploadToCreate') ?>/"+id);
	}

	function compare_document(prev_document){
		// alert($("#viewer").attr('src'));
		// var iFrameBody= document.getElementById('viewer').contentDocument || document.getElementById('viewer').contentWindow.document;
		// head = iFrameBody.getElementsByTagName('head')[0].innerHTML;;
		// content= iFrameBody.getElementsByTagName('body')[0].innerHTML;
		// alert($("#viewerjs").attr('src'));
		// var newiFrameBody = document.getElementById('viewerNew').contentDocument || document.getElementById('viewerNew').contentWindow.document;
		// newiFrameBody.getElementsByTagName('head')[0].innerHTML = head;
		$("#viewerOld").attr('src', prev_document);
		if ("<?= $data['is_upload'] ?>"==0){
			if (document.getElementById('document_edit_textarea')) {
				var saveCommand_compare = {
				    id : 'saveCommand_compare',
				    text : 'Save', 
				    icon : "<?= site_url('assets/web/images/save-new.png') ?>",
				    action : function () {
				        save_as_draft_compare("<?= $data['document_id'] ?>");
				        // write custom functionality to occur when the button is clicked here.
				    }
				};

				var textboxio_config_compare = {    
						ui: {
				        toolbar: {
				            // groups can be defined inline as part of the toolbar items
				            items: [
				            		{
				                        label: 'Custom commands group',
				                        items: [saveCommand_compare]
				                    },
				                    'undo','insert', 'style', 'emphasis', 'align', 'listindent', 'format', 'tools', // other toolbar items
				                    
				            ]
				        }
				    }
				};
				document.getElementById('document_edit_textarea_compare').value =document.getElementById('document_edit_textarea').value;
				if ("<?= $data['edit'] ?>"=='1'){
					var editor = textboxio.replace('#document_edit_textarea_compare',textboxio_config_compare);
				} else {
					var editor = textboxio.replace('#document_edit_textarea_compare');
				}
			}
		} else {
			$("#viewerNew").attr('src', $("#viewer").attr('src'));
			
		}
		
		// newiFrameBody.getElementsByTagName('head')[0].innerHTML = head;
		// newiFrameBody.getElementsByTagName('body')[0].innerHTML = content;
		// var view = $('#viewerjs').html();
		// alert(view);
		// document.getElementById('viewerNew').innerHTML = view;
		$("#compare_modal").modal('show');
	}

	function approve_document(){
		$("#approve_modal").modal('show');
	}

	function save_evaluation_form(){
		var conf = confirm('Are you sure to save the Verification form?');
		if(conf == true){
			if (document.getElementById('select_evaluation_1').value==0){
				if (document.getElementById('input_evaluation_comment').value==''){
					alert('You must fill the comment.');
				} else {
					$("#btn_submit_evaluation_form").click();
				}
			} else {
				$("#btn_submit_evaluation_form").click();
			}
		}
		
	}

	function evaluate_document(){
		$("#evaluation_modal").modal('show');	
	}

	function submit_document(){
		$("#submit_modal").modal('show');
	}

	function set_attribute_document(is_submission=true){
		var doc = <?php echo json_encode($data['docRelated']) ?>;
		get_data_related = doc;
		var parent = document.getElementById("listDokumenTerkait");
		var size = doc.length;
		for(var i=0;i<size;i++){
            var id = doc[i].DOCUMENT2;
            var title = doc[i].TITLE;
            var newInput = document.createElement("input");
            newInput.id = "related";
            newInput.name = "related[]";
            newInput.type = "hidden";
            newInput.value = id;
            related_terpilih.push(id);
            newInput.className = "pilihRelated";
            var newLabel = document.createElement("label");
            newLabel.innerHTML = title;
            newLabel.className = "form-control";
            newLabel.style = "border-style:ridge;font-size: 12px;margin-bottom:8px;padding-left:3px;";
            parent.appendChild(newInput);
            parent.appendChild(newLabel);
        }
		// var docs = JSON.parse(doc);
		console.log(doc);
		// related_terpilih=[];
		// var document_id = <?php echo $data['document_id'] ?>;
		// var http = new XMLHttpRequest();
	 //    http.open("POST", "<?php echo site_url('/web/document/getChoosenDocument/');?>", true);
	 //    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  //   	var param = "listid=" + JSON.stringify(document_id);
  //   	http.send(param);
  //   	http.onload = function() {
  //   		var parent = document.getElementById("listDokumenTerkait");
  //   		var doc = JSON.parse(http.responseText);
  //   		get_data_related = doc;
  //   		var size = doc.length;
  //   		for(var i=0;i<size;i++){
	 //            var id = doc[i].DOCUMENT2;
	 //            var title = doc[i].TITLE;
	 //            var newInput = document.createElement("input");
	 //            newInput.id = "related";
	 //            newInput.name = "related[]";
	 //            newInput.type = "hidden";
	 //            newInput.value = id;
	 //            related_terpilih.push(id);
	 //            newInput.className = "pilihRelated";
	 //            var newLabel = document.createElement("label");
	 //            newLabel.innerHTML = title;
	 //            newLabel.className = "form-control";
	 //            newLabel.style = "border-style:ridge;font-size: 12px;margin-bottom:8px;padding-left:3px;";
	 //            parent.appendChild(newInput);
	 //            parent.appendChild(newLabel);
	 //        }
  //   		// var tableR = document.getElementById('tabelRelated');
    		
  //   	 }
		$("#set_attribute_modal").modal('show');
		if (!is_submission){
			$("input[name='title']").prop('readonly', true);
		}
	}

	function continue_form(id, is_upload='1'){
		if (is_upload=='0'){
			var editor = textboxio.replace('#document_edit_textarea',textboxio_config);
			var doc_editor =editor.content.get();
			if (doc_editor.length<20000000) {
				var converted = htmlDocx.asBlob(doc_editor);
				var converted_file = blobToFile(converted, "<?= $data['filename'] ?>");
				dropzone.addFile(converted_file);
				dropzone.on('complete',function(){
					$.ajax({
						url : '<?= site_url('web/document/updateDocumentFiles') ?>',
						type: 'post',
						cache: false,
						data: {"document_id":id, 'document_text':doc_editor},
						success: function(respon){
							// window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
							$('#btn_next_stepper').click();
						}
					});
				});
			} else {
				alert('File limit of 20MB Exceeds. Please reduce the content of your document.');
			}
		} 
		if (is_upload=='1')
			$('#btn_next_stepper').click();
		
	}

	function delegate_user(){
		var user_id = document.getElementById('delegate').value;
		if (user_id=='0') {
			$('#btn_delegate').addClass('hidden');
			$('#form_review_atribut').removeClass('hidden');
			$('#btn_simpan').removeClass('hidden');
		} else {
			$('#btn_delegate').removeClass('hidden');
			$('#form_review_atribut').addClass('hidden');
			$('#btn_simpan').addClass('hidden');
		}
	}

	function verification_delegate(){
		var user_id = document.getElementById('delegate').value;
		if (user_id=='0') {
			$('#btn_delegate').addClass('hidden');
			// $('#form_review_atribut').removeClass('hidden');
			$('#btn_revise').removeClass('hidden');
			$('#btn_simpan').removeClass('hidden');
			$('#div_edit_document').removeClass('hidden');
		} else {
			$('#btn_delegate').removeClass('hidden');
			// $('#form_review_atribut').addClass('hidden');
			$('#btn_revise').addClass('hidden');
			$('#btn_simpan').addClass('hidden');
			$('#div_edit_document').addClass('hidden');
		}
	}

	function change_select_upload(){
		var select_id = document.getElementById('select_upload').value;
		if (select_id=='0') {
			$('#div_upload_document').addClass('hidden');
			$('#form_create_document').addClass('hidden');
			// Creation
			$('#btn_simpan').addClass('hidden');

			// Approve
			// $('#btn_approve').addClass('hidden');
		} else if (select_id=='1'){
			$('#div_upload_document').removeClass('hidden');
			$('#form_create_document').addClass('hidden');
			// Creation
			$('#btn_simpan').removeClass('hidden');
			$('#btn_simpan').removeClass('pull-right');
		} else if (select_id=='2'){
			$('#div_upload_document').addClass('hidden');
			$('#form_create_document').removeClass('hidden');
			// Creation
			$('#btn_simpan').removeClass('hidden');
			$('#btn_simpan').addClass('pull-right');
		}
	}

	function submit_evaluation(id, urutan, status){
		if (document.getElementById('select_evaluation_1').value==0){
			if (document.getElementById('input_evaluation_comment').value==''){
				alert('You must fill the comment.');
			} else {
				$("#btn_submit_evaluation_form").click();
				set_urutan(id, urutan, status);
			}
		} else {
			$("#btn_submit_evaluation_form").click();
			set_urutan(id, urutan, status);
		}
	}

	function add_reviewer(id, workflow_id=1){
		// workflow_id = 1;
		var reviewer = $('#input_reviewer').val();
		
		if (reviewer==null || reviewer==''){
			alert("Reviewer empty! Failed to add Reviewer!");
			window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
		} else {
			$.ajax({
				url : '<?= site_url('web/document/ajaxAddReviewer') ?>',
				type: 'post',
				cache: false,
				data: {"document_id":id,"reviewer":reviewer},
				success: function(respon1){
					$.ajax({
						url : '<?= site_url('web/document/ajaxsetreason') ?>',
						type: 'post',
						cache: false,
						data: {"document_id":id,"reason":'Adding Reviewer'},
						success: function(respon1){
							if (document.getElementById('is_continue_reviewer').value==1){
								$.ajax({
									url : '<?= site_url('web/document/ajaxSetUrutan') ?>',
									type: 'post',
									cache: false,
									data: {"document_id":id,"workflow_id":workflow_id,'workflow_urutan':1, 'status':'P', 'text': 'Adding Reviewer'},
									success: function(respon2){
										$('#submit_add_reviewer').click();
										window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
									}
								});
							} else {
								window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
							}
							
						}
					});
				}
			});
		}
		
		
	}

	function update_doc(id){
		$.ajax({
			url : '<?= site_url('web/document/updateDocumentFiles') ?>',
			type: 'post',
			cache: false,
			data: {"document_id":id},
			success: function(respon){
				if (respon != 'ERROR') {
					// alert(respon);
					// $('#btn_next_stepper').click();
					window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
				}
				else {
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
				}
			}
		});
	}

	function update_upload_document(id){
		if((!isUploading && dropzone.files.length>0)){
	        var filename = dropzone.files[0].name;
	        var exts = filename.split(".");
	        var ext = exts[exts.length-1];
	        if (ext=='xls' || ext=='xlsx'){
	            if((!isUploadingexcel && dropzoneexcel.files.length>0)){
	                update_doc(id);
	            } else {
	                document.getElementById('alertExcelNotPDF').style.display = "inline";
	            }
	        } else {
	            update_doc(id);
	        }
	        // document.getElementById('btnSubmit').click();
	        // alert("Uploaded");
	    } else {
	        if(dropzone.files.length>0){
	            document.getElementById('alertForNewDoc').style.display = "inline";
	            document.getElementById('alertMessageNewDoc').textContent = "Proses upload belum selesai";
	        }else{
	            document.getElementById('alertForNewDoc').style.display = "inline";
	            document.getElementById('alertMessageNewDoc').textContent = "Anda belum mengupload file dokumen";
	        }
	    }
	}

	function setRevise(id) {
		$.ajax({
	        url : "<?php echo site_url('/web/document/revisenow');?>/",
	        type: 'post',
	        data: {'id':id},
	        success:function(respon){
	        	window.location.href = "<?php echo site_url('web/document/detail') ?>/"+respon;
	        } 
	    }); 
	};

	function save_as_draft(id){
    	var editor = textboxio.replace('#document_edit_textarea',textboxio_config);
		var doc_editor =editor.content.get();
		if (doc_editor.length<20000000) {
			$('.loading').addClass('fa fa-spinner fa-spin');
	        $('.loadings').show();
			var converted = htmlDocx.asBlob(doc_editor);
			var converted_file = blobToFile(converted, "<?= $data['filename'] ?>");
			dropzone.addFile(converted_file);
			dropzone.on('complete',function(){
				$.ajax({
					url : '<?= site_url('web/document/updateDocumentFiles') ?>',
					type: 'post',
					cache: false,
					data: {"document_id":id, 'document_text':doc_editor},
					success: function(respon){
						window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
					}
				});
			});
		} else {
			alert('Ukuran file anda melebihi batas yang telah ditentukan. Silahkan kurangi isi file ini.');
		}
	}

	function save_as_draft_compare(id){
    	var editor = textboxio.replace('#document_edit_textarea_compare',textboxio_config);
		var doc_editor =editor.content.get();
		if (doc_editor.length<20000000) {
			$('.loading').addClass('fa fa-spinner fa-spin');
	        $('.loadings').show();
			var converted = htmlDocx.asBlob(doc_editor);
			var converted_file = blobToFile(converted, "<?= $data['filename'] ?>");
			dropzone.addFile(converted_file);
			dropzone.on('complete',function(){
				$.ajax({
					url : '<?= site_url('web/document/updateDocumentFiles') ?>',
					type: 'post',
					cache: false,
					data: {"document_id":id, 'document_text':doc_editor},
					success: function(respon){
						window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
					}
				});
			});
		} else {
			alert('Ukuran file anda melebihi batas yang telah ditentukan. Silahkan kurangi isi file ini.');
		}
	}

	function add_column_perubahan(){
		var id = document.getElementById('id_tr').value;
		// $('#table_perubahan').append("<tr><td><input type='text' name='bab[]'></td><td><input type='text' name='old_condition[]'></td><td><input type='text' name='new_condition[]'></td></tr>");
		$('#table_perubahan').append("<tr id='tr_"+id+"'>"+
										"<td>"+
											"<textarea name='bab[]'></textarea>"+
										"</td>"+
										"<td>"+
											"<textarea name='old[]'></textarea>"+
										"</td>"+
										"<td>"+
										"<textarea name='new[]'></textarea>"+
										"</td>"+
										"<td>"+
										"<button type='button' class='btn btn-xs btn-danger' onclick='delete_column_perubahan("+id+")'>"+
										"<i class='fa fa-times'></i></button></td>"+
										"</tr>");
		id++;
		document.getElementById('id_tr').value= id;
	}

	function edit_column_perubahan($id){

	}

	function delete_column_perubahan(id){
		if ($('#tr_' + id).length) {
	      $('#tr_' + id).remove();
	    }
	}

	function verify(id){
		var json = {
			template : {
				is_appropriate:document.getElementById('select_verification_template').value, 
				text:document.getElementById('input_verification_template').value
			},
			tugas : {
				is_appropriate:document.getElementById('select_verification_tugas').value, 
				text:document.getElementById('input_verification_tugas').value
			},
			penulisan : {
				is_appropriate:document.getElementById('select_verification_penulisan').value, 
				text:document.getElementById('input_verification_penulisan').value
			}
			//,

			// perubahan :{
			// 	bab:document.getElementById('bab').value,
			// 	old_condition:document.getElementById('old_condition').value,
			// 	new_condition:document.getElementById('new_condition').value
			// }
		};
		$.ajax({
			url : '<?= site_url('web/document/ajaxSetDocumentVerification') ?>',
			type: 'post',
			cache: false,
			data: {"document_id":id, "json":json},
			success: function(respon){
				if (respon != 'ERROR') {
					// window.location.replace("<?= site_url('web/document/detail') ?>/"+id);
					$('#submit_verify').click();
				}
				else {
					alert('Terjadi kesalahan. Silakan coba lagi atau hubungi Administrator');
				}
			}
		});
	}

	// function add_reviewer(){
	// 	var select_id = document.getElementById('select_add_reviewer').value;
	// 	if (select_id=='0'){
	// 		$('#form_review_atribut').addClass('hidden');
	// 		$('#div_edit_document').removeClass('hidden');
	// 		$('#btn_approve').removeClass('disabled');
	// 		$('#btn_simpan').removeClass('disabled');
	// 		$('#btn_revise').addClass('disabled');
	// 		$('#btn_reject').removeClass('disabled');
	// 		$('#div_is_revise').removeClass('hidden');
	// 	} else {
	// 		$('#form_review_atribut').removeClass('hidden');
	// 		$('#div_edit_document').addClass('hidden');
	// 		$('#btn_approve').addClass('disabled');
	// 		$('#btn_simpan').addClass('disabled');
	// 		$('#btn_revise').removeClass('disabled');
	// 		$('#btn_reject').addClass('disabled');
	// 		$('#div_is_revise').addClass('hidden');
	// 		document.getElementById('select_is_revise').value='0';
	// 	}
	// }

	function change_revise(){
		var select_id = document.getElementById('select_is_revise').value;
		if (select_id=='0'){
			$('#btn_approve').removeClass('disabled');
			$('#btn_revise').addClass('disabled');
			$('#btn_simpan').removeClass('disabled');
			$('#btn_reject').removeClass('disabled');
			$('#div_edit_document').removeClass('hidden');
		} else {
			$('#btn_approve').addClass('disabled');
			$('#btn_revise').removeClass('disabled');
			$('#btn_simpan').addClass('disabled');
			$('#btn_reject').addClass('disabled');
			$('#div_edit_document').addClass('hidden');
		}
	}

	$(document).ready(function(){
		removeTemp();
		if (document.getElementById('document_edit_textarea')) {
			document.getElementById('document_edit_textarea').value ="<?= $data['document_text'] ?>";
			// if ("<?= $data['edit'] ?>"=='1' && "<?= $data['is_published'] ?>"=='0'){
			if ("<?= $data['edit'] ?>"=='1'){
				var editor = textboxio.replace('#document_edit_textarea',textboxio_config);
			} else {
				var editor = textboxio.replace('#document_edit_textarea');
			}
			// $(".ephox-candy-mountain.ephox-candy-mountain-active.ephox-platform-desktop").attr("contenteditable", 'false');
			// var editable_elements = document.querySelectorAll("[contenteditable=true]");
			// alert(editable_elements.length);
			// for(var i=0; i<editable_elements.length; i++)
			//     editable_elements[i].setAttribute("contenteditable", false);
			
		}
		// var filename = "<?php//= $data['document_name_encrypted'] ?>";
		// var http = new XMLHttpRequest();
		// http.open("POST", "<?php// echo site_url('/web/document/read_docx');?>" + "/<?php//= $data['document_id'] ?>", true);
		// http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		// var param = "";
		// http.send(param);
		// http.onload = function() {
		// 	if (document.getElementById('document_edit_textarea')) {
		// 		document.getElementById('document_edit_textarea').value = http.responseText;
		// 		var editor = textboxio.replace('#document_edit_textarea');
		// 	}
		// }
	});

	function getRawDocument(id){
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/viewdoc/');?>" + "/" + id, true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {
			var response = JSON.parse(http.responseText);
		   	var title = response['document'][0]['title'];
		   	document.getElementById("titleDoc").textContent = title;
		   	var basename = baseName(response['file']['link']);
		   	var namaFile = response['file']['nama_file'];
		   	var exts = namaFile.split(".");
		    var ext = exts[exts.length-1];
		    var divContent = document.getElementById("viewDocContent");
		    var inner = "";
		    if(ext == "pdf"){
		    	inner = "<iframe name='viewer' src ='<?php echo site_url("web/document/view/");?>" +  "/" + basename + ".pdf' width='100%' height='100%'allowfullscreen webkitallowfullscreen ></iframe>";
		    }else if(ext == "jpg" || ext =="png" || ext == "jpeg" || ext=='gif' || ext=='bmp'){
		   		inner = "<img src='" + response['file']['link'] + "' alt='<?= $ext;?>' style='width:100%;height:100%;float:middle;'/>";
		   	}else if (ext=='mp4' || ext=='mpeg' || ext=='avi' || ext=='flv' || ext=='wmv' || ext=='mov'){
		   		mime = ext;
		   		if (ext=='wmv')
		   			mime = "x-ms-wmv";
		   		else if (ext=="flv")
		   			mime = "x-flv";
		   		inner = "<video width='100%' height='100%' controls controlsList='nodownload'><source src='"+response['file']['link']+"' type='video/"+mime+"'>Browser anda tidak support video.</video>";
		   		
		   	}else if(ext != "zip" || ext != "rar" || ext != "gz" || ext=='xmind'){
		   		inner = "<div style='width: 100%; height: 100%; position: inline;'>";
		   		inner = inner + "<iframe src='https://view.officeapps.live.com/op/view.aspx?src=<?php echo site_url("/web/thumb/files");?>"+'/' + basename + "." + ext + "&embedded=true' frameborder='0' scrolling='no' seamless style='width:100%;height:100%;float:middle;'></iframe>";
	    		// inner = inner + "<div style='width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;'>&nbsp;</div>";
	    		inner = inner + "<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:12px;color:red'>*NB : do not fill the header and footer</i></div>";
	    		
	    	}
	    	document.getElementById('document_edit_textarea').value=inner;
		}
	}

	function removeTemp(){
		// document.getElementById('alertForPerbaharui').style.display = "none";
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/removeFileTemp');?>", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {
		  	if(currentFile!=null && dropzone!=null){
		    	dropzone.removeFile(currentFile);	
		    	currentFile = null;
			}
		}
	}

	function submitDocument(){
	    if(!isUploading2 && dropzone2.files.length>0){
	        document.getElementById('btnSubmit').click();
	        // alert("Uploaded");
	    }
	    else{
	        alert("Not Uploaded");
	        if(dropzone2.files.length>0){
	            document.getElementById('alertForNewDoc').style.display = "inline";
	            document.getElementById('alertMessageNewDoc').textContent = "Proses upload belum selesai";
	        }else{
	            document.getElementById('alertForNewDoc').style.display = "inline";
	            document.getElementById('alertMessageNewDoc').textContent = "Anda belum mengupload file dokumen";
	        }

	    }
	}

	$('#file_upload').on('change', function() {
        $('#file_form').ajaxForm({
            target: '#files_preview',
            error: function(e) {
                //target: '#images_preview';
                 alert('Terjadi kesalahan. Silakan diulangi lagi');
            }
        }).submit();
    });

    function removeUploadedFile(showBox){
		// document.getElementById('alertForPerbaharui').style.display = "none";
		// document.getElementById('alertForNewDoc').style.display = "none";
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/removeFileTemp');?>", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {
			if(currentFile!=null && dropzone!=null){
				dropzone.removeFile(currentFile);	
				currentFile = null;
			}
			if(currentFile!=null && dropzone!=null){
			   	dropzone.removeFile(currentFile);	
			   	currentFile = null;
			}
			if(showBox)
				$("#UploadBox").modal('show');
		}	
	}

	function removeUploadedFileexcel(showBox){
		var http = new XMLHttpRequest();
		http.open("POST", "<?php echo site_url('/web/document/removeFileTempPDF');?>", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var param = "";
		http.send(param);
		http.onload = function() {
			if(currentFileexcel!=null && dropzoneexcel!=null){
				dropzoneexcel.removeFile(currentFileexcel);	
				currentFileexcel = null;
			}
			// if(showBox)
			// 	$("#UploadBox").modal('show');
		}	
	}

	$('#hide_alertForNewDoc').on('click', function (){
		document.getElementById('alertForNewDoc').style.display = "none";
	});

	$('#hide_alertExcelNotPDF').on('click', function (){
		document.getElementById('alertExcelNotPDF').style.display = "none";
	});

	$('#hide_alertForNewDoc_bm').on('click', function (){
		document.getElementById('alertForNewDoc_bm').style.display = "none";
	});

	$('#hide_alertFileExceeds').on('click', function (){
		document.getElementById('alertFileExceeds').style.display = "none";
	});
	$('#hide_alertFileExceeds_bm').on('click', function (){
		document.getElementById('alertFileExceeds_bm').style.display = "none";
	});

	var currentFile = null;
	var dropzone = null;
	var isUploading = false;
	Dropzone.options.dropbox = {
		addRemoveLinks: true,
		timeout:120000,
		maxFiles:1,
		maxFileSize:20,
		init: function() {
		this.on("addedfile", function(file) {
		  document.getElementById('alertForNewDoc').style.display = "none";
		  document.getElementById('alertFileExceeds').style.display = "none";
		  if (file.size>1024*1024*20){
		      	document.getElementById('alertFileExceeds').style.display = "inline";
		      	currentFile = null;
		    	removeUploadedFile(false);
		    	isUploading = false;
		    	dropzone.removeFile(file);
		  } else {
		  	isUploading = true;
		  }
		});
		this.on("removedfile", function(file) {
			if(file==currentFile){
				currentFile=null;
		    	removeUploadedFile(false);
			}
		});
		this.on("canceled", function () {
			currentFile = null;
			removeUploadedFile(false);
		});
		this.on("maxfilesexceeded", function(file) {
		    // this.addFile(file);
		    dropzone.removeFile(file);
		    document.getElementById('alertForNewDoc').style.display = "inline";
		    document.getElementById('alertMessageNewDoc').textContent = "Hanya boleh mengupload 1 file saja";
		});
		this.on("complete", function (file) {
		  if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
		    if(currentFile==null)
		    	currentFile = file;
		  	var http = new XMLHttpRequest();
		  	http.open("POST", "<?php echo site_url('/web/document/checkSameDoc');?>", true);
		    http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		    var param = "";
		    http.send(param);
		    http.onload = function() {
		    	isUploading = false;
		    	if(http.responseText == "false"){
		    		document.getElementById('alertForNewDoc').style.display = "inline";
		    		document.getElementById('alertMessageNewDoc').textContent = "File sudah pernah diunggah!";
		    		dropzone.removeFile(currentFile);
		    		currentFile = null;
		    	} else {
		    		var filename = dropzone.files[0].name;
		    		var exts = filename.split(".");
				    var ext = exts[exts.length-1];
				    if (ext=='xls' || ext=='xlsx'){
				    	$("#dropbox").removeClass("col-md-12");
				    	$("#dropbox").addClass("col-md-6");
				    	$("#dropboxexcel").removeClass("hidden");
				    	document.getElementById("dropbox").style.padding = "7px 5%";
				    } else {
				    	$("#dropbox").removeClass("col-md-6");
				    	$("#dropbox").addClass("col-md-12");
				    	$("#dropboxexcel").addClass("hidden");
				    	document.getElementById("dropbox").style.padding = "7px 25%";
				    }
		    	}
		    }
		  }
		});
		dropzone = this;
		}   
	};

	// excel
	var currentFileexcel = null;
	var dropzoneexcel = null;
	var isUploadingexcel = false;
	Dropzone.options.dropboxexcel = {
	  addRemoveLinks: true,
	  timeout:120000,
	  maxFiles:1,
	  maxFileSize:20,
	  init: function() {
	    this.on("addedfile", function(file) {
	      document.getElementById('alertExcelNotPDF').style.display = "none";
	      document.getElementById('alertFileExceeds').style.display = "none";
	      if (file.size>1024*1024*20){
		      	document.getElementById('alertFileExceeds').style.display = "inline";
		      	currentFileexcel = null;
		    	removeUploadedFileexcel(false);
		    	isUploadingexcel = false;
		    	dropzoneexcel.removeFile(file);
	      } else {
	      	isUploadingexcel = true;
	      }
	    });
	    this.on("removedfile", function(file) {
	    	if(file==currentFileexcel){
	    		currentFileexcel=null;
		    	removeUploadedFileexcel(false);
	    	}
		});
		this.on("canceled", function () {
			currentFileexcel = null;
	    	removeUploadedFileexcel(false);
	    });
	    this.on("maxfilesexceeded", function(file) {
	        // this.addFile(file);
	        dropzoneexcel.removeFile(file);
	        document.getElementById('alertExcelNotPDF').style.display = "inline";
	    });
	    this.on("complete", function (file) {
	      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
	        if(currentFileexcel==null)
	        	currentFileexcel = file;
	      	isUploadingexcel = false;
			var filename = dropzoneexcel.files[0].name;
			var exts = filename.split(".");
		    var ext = exts[exts.length-1];
		    if (ext!='pdf'){
		    	document.getElementById('alertExcelNotPDF').style.display = "inline";
	    		dropzoneexcel.removeFile(currentFileexcel);
	    		currentFileexcel = null;
		    }
	      }
	    });
	    dropzoneexcel = this;
	  }   
	};

	function checkEvaluationForm(){
		if (document.getElementById('select_evaluation_1').value==1){
			$('#evaluation_tr_2').removeClass('hidden');
			$('#evaluation_tr_3').removeClass('hidden');
			$('#evaluation_tr_4').removeClass('hidden');
			$('#evaluation_tr_5').removeClass('hidden');
			$('#evaluation_tr_6').removeClass('hidden');
			$('#evaluation_tr_7').removeClass('hidden');
			$('#evaluation_tr_8').removeClass('hidden');
			$('#evaluation_tr_9').removeClass('hidden');
		} else {
			document.getElementById('select_evaluation_2').value = 0;
			document.getElementById('select_evaluation_3').value = 0;
			document.getElementById('select_evaluation_4').value = 0;
			document.getElementById('select_evaluation_5').value = 0;
			document.getElementById('select_evaluation_6').value = 0;
			document.getElementById('select_evaluation_7').value = 0;
			document.getElementById('select_evaluation_8').value = 0;
			document.getElementById('select_evaluation_9').value = 0;
			$('#evaluation_tr_2').addClass('hidden');
			$('#evaluation_tr_3').addClass('hidden');
			$('#evaluation_tr_4').addClass('hidden');
			$('#evaluation_tr_5').addClass('hidden');
			$('#evaluation_tr_6').addClass('hidden');
			$('#evaluation_tr_7').addClass('hidden');
			$('#evaluation_tr_8').addClass('hidden');
			$('#evaluation_tr_9').addClass('hidden');
		}
	}

	function getKlausul(is_reset_klausul=1){
		var iso_id = $('#input_iso').val();
		$.ajax({
	        url : "<?php echo site_url('/web/document_iso_klausul/ajaxGetKlausul');?>",
	        type: 'post',
	        dataType: 'JSON',
	        data: {'iso_id':iso_id},
	        success:function(respon){
	        	var klausul = $.map(respon, function (obj) {
                    obj.id = obj.id || obj.id; // replace id_kab with your identifier
                    obj.text = obj.text || obj.klausul; // replace nama with your identifier
                    return obj;
                });
                if (is_reset_klausul==1){
                	$(".input_iso_klausul").html('');
                }
                
        	  	$(".input_iso_klausul").select2({
        	  		placeholder: "Klausul",
        	  		data : klausul,
        	  		minimumResultsForSearch: Infinity
        	  	});
	        } 
	    });
	}
</script>
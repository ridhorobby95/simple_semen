		<table>
			<tr>
				<td style="vertical-align: top">
					<table class="data_grid" style="margin:3px;background-color:white">
						<tr>
							<td width="120">Nama</td><td width="200"><?= $task['content']['nama'] ?></td>
							<td width="140">Start</td><td width="300"><?= xFormatDateInd($task['header']['created'], false, false,'-', true) ?></td>
						</tr>
						<tr>
							<td width="120">Email</td><td><?= $task['content']['email'] ?></td>
							<td width="140">Batas waktu</td><td>
							<?php if ($workflow['rule']['tenggat_hari']) { ?>
							<?php 
							$time = strtotime($task['header']['created']) + (60*60*24*$workflow['rule']['tenggat_hari']);
							echo xFormatDateInd(date('Y-m-d H:i:s', $time), false, false,'-', true);
							?>					
							&nbsp; (<?= $workflow['rule']['tenggat_hari'] ?> day)
							<?php } ?>
							</td>
						</tr>
						<tr>
							<td width="120">Departemen</td><td><?= $task['content']['departemen'] ?></td>
							<td width="140">Selesai</td><td>
							<?php

							if ($list_task[0]['header']['finished']) {
								$lebih_tenggat = 0;
								$color = '#5790ed';
								if ($workflow['rule']['tenggat_hari']) {
									$t = xGetDiffDate(strtotime($task['header']['created']), strtotime($list_task[0]['header']['finished']));
									if ($t > $workflow['rule']['tenggat_hari']) {
										$lebih_tenggat = 1;
										$color = '#ff3300';
									}
								}
								
								$t2 = strtotime($list_task[0]['header']['finished']) - strtotime($task['header']['created']);
								echo xFormatDateInd($list_task[0]['header']['finished'], false, false,'-', true);
								echo " &nbsp; <span style=\"color:{$color}\">(".xTimeLapse($t2) . ")</span>";

								if ($lebih_tenggat)
									echo '<br><label class="label label-danger">Melebihi tenggat waktu</label>';
							}
							else {
								$t = xGetDiffDate(time(), strtotime($task['header']['created']));
								if ($workflow['rule']['tenggat_hari']) {
									if ($t > $workflow['rule']['tenggat_hari'])
										echo '<label class="label label-danger">Melebihi tenggat waktu</label>';
								}
							}

		?>
										</td>
						</tr>
					</table>					
				</td>
				<td style="vertical-align: top;display:none">
					<div class="col-xs-3 col-sm-2 col-md-2 thumbnail" style="width:100px;margin:2px;height:90px;padding: 0;cursor: pointer;">
						<div id="popup-gallery<?=$task['id_submit']?>" onclick="openPhotoSwipe(items_<?= $task['id_submit'] ?>);" class="grid-item img_hover" title="File PDF"  style="padding:2px">
							<div class="imagelay" data-content="<?= ellipsize($lampiran['namafile'],20,0.5) ?>" >							
								<img src="<?= site_url('home/thumb/'.$task['id_submit'])  ?>" class="img-thumbnail" style="width:90px;height:80px;">							
								<div class="caption">
								</div>
							</div>
						</div>
						<div class="large"></div>
					</div>
						<script>
							ajaxurl = '<?= site_url('login/plampiran2/'.$task['id_submit']) ?>';
							$.ajax({
								type: "POST",
								url: ajaxurl,
								data: {"id_task":<?= $task['id_task']?>},
								success: function(ret){
									eval(ret);
								}
							});
						</script>
					</td>
			</tr>
		</table>

				<?php 
				$i = 0;
				foreach ($workflow['rule']['model']['nodeDataArray'] as $node) { 
					$extra_text = '';
					$bgcolor = '#efefef';
					$color = '#333';
					if ($node['key'] == $task['id_node_active']) {
						$bgcolor = '#BE9005';
						if ($node['key'] == 'node_1')
							$bgcolor = '#ee3300';
						$color = '#fff';
						if ($node['key'] == 'node_' . count($workflow['rule']['model']['nodeDataArray']) ) {
							$bgcolor = '#38a02c';
							$extra_text = 'Done';
						}
					}

				?>
	<div class="col-xs-3 col-md-1" style="padding:3px;margin:0">
	<a href="<?= site_url('home/task/' . $task['id_task'])  ?>#<?= $node['key'] ?>" class="thumbnail" style="padding:2px;height:60px;font-size:10px;background-color:<?= $bgcolor ?>;color:<?= $color ?>"><?= $actors[$node['actor']] ?>	<?= $extra_text ?></a>
		<?php
		$lapse = '';
		if ($list_task[$i]['header']['created'] && $list_task[$i-1]['header']['created']) {
			$t = strtotime($list_task[$i]['header']['created']) - strtotime($list_task[$i-1]['header']['created']);
			$lapse = xTimeLapse($t);
		}
		if (!$i)
			echo '<div style="font-size:10px">' . xFormatDateInd($list_task[$i]['header']['created'], false, false,'-', true) . '</div>';
		echo '<div style="font-size:10px" title="'.xFormatDateInd($list_task[$i]['header']['created'], false, false,'-', true).'">'.$lapse .'</div>';
		?>
	</div>
				<?php $i++; } ?>

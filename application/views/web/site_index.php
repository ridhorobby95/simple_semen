<!DOCTYPE html>
<html class="login-page">
	<head>
		<meta charset="UTF-8">
		<title>Document Management Semen Indonesia</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/ionicons.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/style.css') ?>">
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/login.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/custom.css') ?>">
		<!-- <link rel="icon" type="image/png" href="<?php echo base_url('assets/web/img/favicon_semen.png') ?>"> -->
		<link rel="icon" type="image/png" href="<?php echo base_url('assets/web/img/logo-simple.png') ?>">
		<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/html5shiv.js') ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/respond.min.js') ?>"></script>
	</head>
	<body>

	<div class="hidden-xs hidden-style" id="main_cont" style="">
		<div class="row">
			<div class="col-md-12">
        		<div class="row">
					<div class="col-md-4 styling-new" style="">

       					<div class="row" style="margin-bottom: 3%">
							<div class="col-md-12 text-center">
								<img src="<?php echo site_url("assets/web/img/logo-simple.png");?>" style="width: 75%;margin-top: 10%;">
<!-- 								<h3 style="    font-family: Lato,sans-serif;
    font-weight: 600;
    letter-spacing: 1px;">LOGIN</h3> -->

        					</div>
        				</div>
       					<div class="row" style="margin-bottom: 10%">
							<div class="col-md-12">
								<?php echo form_open(); ?>
			                        <?php if(!empty($errmsg)) { ?>
			                        <div class="login-message" style="margin-bottom: 3%">
			                            <?php echo implode('<br>',$errmsg) ?>
			                        </div>
			                        <?php } ?>
<!-- 			                        <div class="login-intro">Login untuk masuk aplikasi.</div> -->
			                        <label class="login-label">Username</label>
			                        <?php echo form_input(array('name' => 'username','class' => 'form-control login-form','placeholder' => 'Username', 'id'=>'username')) ?>
			                        <label class="login-label">Password</label>
			                        <?php echo form_password(array('name' => 'password','class' => 'form-control login-form','placeholder' => 'Password')) ?>
			                        <a href="<?php echo base_url() ?>">Lupa Password</a>
			                        <div class="login-bottom">
			                            <?php echo form_submit(array('value' => 'Login','class' => 'btn login-bg', 'style' => 'background-color:#2196F3', 'name' => 'submitLogin')) ?>
			                            <!-- <button type="submit" class="btn login-bg">Login</button> -->
			                        </div>
			                    </form>
			                    
								<!-- <form id="loginForm" method="POST" action="/login/" novalidate="novalidate">
								<div class="form-group">
								<h5 style="font-weight: 600;">Masukkan Username dan Password</h5>
								  <label for="username" class="control-label">Username</label>
								  <input type="text" class="form-control" id="username" name="username" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
								  <span class="help-block"></span>
								</div>
								<div class="form-group">
								  <label for="password" class="control-label">Password</label>
								  <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password">
								  <span class="help-block"></span>
								</div>
								<div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
								<button type="submit" class="btn login-bg">Login</button>
								</form> -->
        					</div>
        				</div>

       					<div class="row">
							<div class="col-md-12">
								<h6>Copyright &copy; 2018 Document Management Semen Indonesia.</h6>

        					</div>
        				</div>
					</div>


					<div class="col-md-8 carousel-new" style="">
						<div style="background: url(../assets/web/img/news-2.jpg);background-position: right;background-size:100% 100%;background-repeat: no-repeat;  height: 100%;width: 100%;">
							<div class="row" style="height:70%;">
								<div class="col-md-12">
									
								</div>
							</div>
							<div class="row" style="height:30%;background: linear-gradient(to bottom, rgba(255,255,255,0), rgba(79, 195, 247, 0.9), rgba(3, 169, 244, 0.9));margin: 0px;">
								<div class="col-md-12" style="margin-top: 2%;">
								    <h3 style="color: #FAFAFA;">Meningkatkan Efektifitas Pengelolaan Proses Bisnis di Semen Indonesia dengan Aplikasi “SIMPLE” </h3>
								    <h5 style="color: #FAFAFA;">Dalam rangka peningkatan efektifitas Sistem Manajemen Semen Indonesia (SMSI),...
								    </h5>
								    <a href="https://smile.semenindonesia.com/index.php?r=home#" target="_blank">
										<button type="button" class="btn btn-login-new" style="">
										  View More
										</button>
									</a>
								</div>								
							</div>
							
						</div>
						<div id="carousel-example-generic" class="carousel slide hidden" data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
							    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
							    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
							  </ol>

							  <!-- Wrapper for slides -->
							  <div class="carousel-inner" role="listbox">
								<div class="item active">
									<img src="<?php echo base_url('assets/web/img/gbr-1.jpeg') ?>">
								  <div class="carousel-caption" style="left: 10%;right: 10%;">
								    <h3>Asosiasi Semen Indonesia Desak Moratorium Pabrik Baru</h3>
								    <a href="https://smile.semenindonesia.com/index.php?r=home#">
										<button type="button" class="btn btn-login-new" style="">
										  View More
										</button>
									</a>
								  </div>
								</div>
								<div class="item">
								  <img src="<?php echo base_url('assets/web/img/gbr-2.jpg') ?>">
								  <div class="carousel-caption" style="left: 10%;right: 10%;">
								    <h3>Lagi, Semen Indonesia Airi Tiga Desa Kering di Sekitar Pabrik Rembang</h3>
								    <a href="https://smile.semenindonesia.com/index.php?r=home#">
										<button type="button" class="btn btn-login-new" style="">
										  View More
										</button>
									</a>
								  </div>
								</div>
								<div class="item">
								  <img src="<?php echo base_url('assets/web/img/gbr-3.jpg') ?>">
								  <div class="carousel-caption" style="left: 10%;right: 10%;">
								    <h3>2018, Kementerian PUPR Targetkan 25 Ruas Tol Beroperasi Komersial</h3>
								    <a href="https://smile.semenindonesia.com/index.php?r=home#">
										<button type="button" class="btn btn-login-new" style="">
										  View More
										</button>
									</a>
								  </div>
								</div>
							  </div>

							  <!-- Controls -->
							  <a class="left carousel-control lefting" href="#carousel-example-generic" role="button" data-slide="prev" style="">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control righting" href="#carousel-example-generic" role="button" data-slide="next" style="">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>
					</div>
        		</div>
    		</div>
		</div>
    </div>


<!-- 	<div class="hidden-xs hidden" id="main_cont" style="margin-top:80px;">
        <div class="main-cover theme-default">
            <div class="row"> -->
				<!-- Kalo maintenance uncomment section ini: -->
				<!-- ########################################################################################## -->
                <!-- <div class="col-sm-5 login-box">
                    <div class="brand-img">
                        <img src="<?php echo base_url('assets/web/img/logo1.png') ?>">
                    </div>
                    <div class="brand-motto">
                        Site Under Maintenance
                    </div>
                </div>
                <div class="col-sm-5 login-box">
                        <img src="<?php echo base_url('assets/web/img/warning.png') ?>">
                </div> -->
				<!-- ########################################################################################## -->
				
				<!-- Kalo maintenance comment section ini: -->
				<!-- ########################################################################################## -->
                <!-- <div class="col-sm-5 login-box">
                    <div class="brand-img" style="text-align:center">
                        <img src="<?= $config['logo'][strtolower(Image::IMAGE_LARGE)]['link'] ?>" style="width:150px;text-align: center">
                    </div>
                    <div class="brand-motto">
                    	<?= $config['title'] ?>
                    </div>  -->
                    <!--
                    <div class="brand-description">
                    	<?//= $config['description'] ?>
                    </div> -->
                <!-- </div>
                <div class="col-sm-1">
                    <div class="vertical-divider">
                    </div>
                </div>
                <div class="col-sm-6 login-box">
                    <?php echo form_open(); ?>
                        <?php if(!empty($errmsg)) { ?>
                        <div class="login-message">
                            <?php echo implode('<br>',$errmsg) ?>
                        </div>
                        <?php } ?>
                        <div class="login-intro">Login untuk masuk aplikasi.</div>
                        <label class="login-label">Username</label>
                        <?php echo form_input(array('name' => 'username','class' => 'form-control login-form','placeholder' => 'Username')) ?>
                        <label class="login-label">Password</label>
                        <?php echo form_password(array('name' => 'password','class' => 'form-control login-form','placeholder' => 'Password')) ?>
                        <div class="login-bottom">
                            <?php echo form_submit(array('value' => 'Masuk Aplikasi','class' => 'btn btn-warning btn-login', 'style' => 'background-color:#2196F3')) ?>
                        </div>
                    </form>
                </div> -->
				<!-- ########################################################################################## -->
<!--             </div>
        </div>
	</div> -->
    






    <div class="login-mobile visible-xs">
        <div class="main-cover theme-default">
            <div class="brand-img">
                <img src="<?php echo base_url('assets/web/img/logo1.png') ?>">
            </div>
            <?php echo form_open(); ?>
                <?php if(!empty($errmsg)) { ?>
                <div class="login-message">
                    <?php echo implode('<br>',$errmsg) ?>
                </div>
                <?php } ?>
                <div class="login-intro">Login untuk masuk aplikasi.</div>
                <label class="login-label">Username</label>
                <?php echo form_input(array('name' => 'username','class' => 'form-control login-form','placeholder' => 'Username')) ?>
                <label class="login-label">Password</label>
                <?php echo form_password(array('name' => 'password','class' => 'form-control login-form','placeholder' => 'Password')) ?>
                <div class="login-bottom">
                    <?php echo form_submit(array('value' => 'Masuk Aplikasi','class' => 'btn btn-warning btn-login', 'style' => 'background-color:#2196F3')) ?>
                    <a class="pull-right login-link" href="javascript:goLupa()<?php // echo site_url($path.'user/lupapw') ?>">Lupa Password?</a>
                </div>
            </form>
        </div>
    </div>

	<div class="modal fade" id="modal_user" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Pilih User</h4>
				</div>
				<div class="modal-body" style="overflow-y:auto">
					<?php
						// kolom: 4
						$n = ceil(count($data)/4);
						for($i=0;$i<$n;$i++) {
					?>
					<div class="row">
						<?php
							for($j=0;$j<4;$j++) {
								$row = $data[($i*4)+$j];
						?>
						<div class="col-sm-3 col-xs-6">
							<div class="login-pick" data-username="<?php echo $row['username'] ?>">
								<!-- <img src="<?php //echo (isset($row['photo']) ? site_url($path.'thumb/watermark/'.$row['id'].'/112') : $nopic) ?>" /> -->
								<img src="<?php echo isset($row['photo']) ? $row['photo']['thumb']['link'] : $nopic ?>"/>
								<div class="login-pick-text"><?php echo $row['name'] ?></div>
							</div>
						</div>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
    
    <div class="footer footer-bottom-fixed hidden">
    <div class="row">
        <div class="col-xs-6">
            <div class="footer-text">
                Copyright &copy; 2017 Document Management Semen Indonesia.
            </div>
        </div>
        <div class="col-xs-6 text-right">
            <div class="footer-text">
                Powered by
            </div>
            <img src="<?php echo base_url('assets/web/img/logo.png') ?>" class="footer-img" height="24">
        </div>
    </div>
</div>

	<script type="text/javascript" src="<?php echo base_url('assets/web/js/external/jquery.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/web/js/bootstrap.min.js') ?>"></script>
	<script type="text/javascript">

		$("#username").keypress(function(e) {
			var ascii = [13, 32, 33, 34, 39, 40, 41, 96];
			if (ascii.indexOf(e.which)!='-1'){
				return false;	
			}
		});	

		$(document).ready(function() {
			// ukuran modal
			$(".modal-body").css("max-height",viewport-((viewport-content)/3));

			// aktivasi tooltip dan onclick
			$("[data-toggle='tooltip']").css("cursor","pointer").tooltip().click(function() {
				$("#modal_user").modal();
			});
			$(".login-pilih").click(function() {
				$("#modal_user").modal();
			});

			// foto
			$(".login-pick").hover(
				function() {
					$(this).find(".login-pick-text").animate({minHeight:"100%"},100);
				},
				function() {
					$(this).find(".login-pick-text").animate({minHeight:0},100);
				}
			).click(function() {
				$(".login-photo img").attr("src",$(this).find("img").attr("src"));
				$(".login-name").html($(this).find(".login-pick-text").text());
				$("[name='username']").val($(this).attr("data-username"));
				$(".footer").removeClass("hidden");

				$(".login-pilih").addClass("hidden");
				$(".login-body").removeClass("hidden");

				$("#modal_user").modal("hide");
				$("[name='password']").focus();
			});

			// fokus ke password
			$("[name='password']").focus();
		});

		function goLupa() {
			alert("Fitur sedang dalam pengerjaan. Untuk sekarang coba ingat ingat password anda atau hubungi admin bila anda merasa tidak mampu :D");
		}

	</script>
	</body>
</html>

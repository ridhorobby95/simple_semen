<?php
    $readonly = isset($id) ? 'readonly' : '';
    $disabled = isset($id) ? 'disabled' : '';
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-2">Employee Number</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'k_nopeg', 'value' => $data['k_nopeg'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'k_nopeg')) ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getEmployee()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="jabatan_id" class="col-sm-2">Position Code</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'jabatan_id', 'value' => $data['jabatan_id'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'jabatan_id')) ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getEmployee()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="k_unitkerja_id" class="col-sm-2">Work Unit Code</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'k_unitkerja_id', 'value' => $data['k_unitkerja_id'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'k_unitkerja_id')) ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getEmployee()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="div_employee_detail">
                            <label for="name" class="col-sm-2">Employee Detail</label>
                            <div class="col-sm-10">
                                <textarea class="form-control input-sm" rows=1 id="employee_detail" name="employee_detail" readonly style="resize:none;width:100%">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan1_nopeg" class="col-sm-2">1st Superior</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'atasan1_nopeg', 'value' => $data['atasan1_nopeg'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'atasan1_nopeg')) ?>
                                <?php if (1==0) { ?>
                                    <?php echo form_dropdown('atasan1_nopeg', $variables['karyawan'], $data['atasan1_nopeg'], "class='form-control input-sm select2' id='atasan1_nopeg' placeholder='First Superior of this Employee' required style='width:100%'") ?>
                                <?php } ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getEmployee(1)">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="div_atasan1_detail">
                            <label for="atasan1_detail" class="col-sm-2">1st Superior Detail</label>
                            <div class="col-sm-10">
                                <textarea class="form-control input-sm" rows=3 id="atasan1_detail" name="atasan1_detail" readonly style="width:100%;resize:none">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan1_jabatan" class="col-sm-2">1st Superior's Position</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'atasan1_jabatan', 'value' => $data['atasan1_jabatan'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'atasan1_jabatan')) ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getPosition()">
                                    Check
                                </button>
                            </div>
                        </div>
                        
                        <div class="row bord-bottom hidden" id="div_atasan1_jabatan_detail">
                            <label for="atasan1_jabatan_detail" class="col-sm-2">1st Superior Position Detail</label>
                            <div class="col-sm-10">
                                <textarea class="form-control input-sm" rows=3 id="atasan1_jabatan_detail" name="atasan1_jabatan_detail" readonly style="width:100%;resize:none">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="jabatan_id" class="col-sm-2">1st Superior's Work Unit</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'atasan1_unitkerja', 'value' => $data['atasan1_unitkerja'], 'required' => true, 'class' => 'form-control input-sm', 'id' => 'atasan1_unitkerja')) ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getEmployee()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan1_level" class="col-sm-2">Level </label>
                            <div class="col-sm-5">
                                <?php echo form_dropdown('atasan1_level', $levels, $data['atasan1_level'], 'id="atasan1_level" class="form-control input-sm select2"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan1_pgs" class="col-sm-2">PGS? </label>
                            <div class="col-sm-5">
                                <?php echo form_dropdown('atasan1_pgs', $is_pgs, $data['atasan1_pgs'], 'id="atasan1_pgs" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan1_nopeg" class="col-sm-2">2nd Superior</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'atasan2_nopeg', 'value' => $data['atasan2_nopeg'], 'class' => 'form-control input-sm', 'id' => 'atasan2_nopeg')) ?>
                            </div>
                        </div>
                        <div class="row bord-bottom hidden" id="div_atasan2_detail">
                            <label for="atasan1_detail" class="col-sm-2">2nd Superior Detail</label>
                            <div class="col-sm-10">
                                <textarea class="form-control input-sm" rows=3 id="atasan1_detail" name="atasan1_detail" readonly style="width:100%;resize:none">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan2_jabatan" class="col-sm-2">2nd Superior's Position</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'atasan2_jabatan', 'value' => $data['atasan2_jabat'], 'class' => 'form-control input-sm', 'id' => 'atasan2_jabatan')) ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getPosition()">
                                    Check
                                </button>
                            </div>
                        </div>
                        
                        <div class="row bord-bottom hidden" id="div_atasan2_jabatan_detail">
                            <label for="atasan2_jabatan_detail" class="col-sm-2">2nd Superior Position Detail</label>
                            <div class="col-sm-10">
                                <textarea class="form-control input-sm" rows=3 id="atasan2_jabatan_detail" name="atasan2_jabatan_detail" readonly style="width:100%;resize:none">
                                    
                                </textarea>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="jabatan_id" class="col-sm-2">2nd Superior's Work Unit</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'atasan2_unitkerja', 'value' => $data['atasan2_unitkerja'], 'class' => 'form-control input-sm', 'id' => 'atasan2_unitkerja')) ?>
                            </div>
                            <div class="col-sm-1 hidden">
                                <button type="button" class="btn btn-xs btn-success pull-right " onClick="getEmployee()">
                                    Check
                                </button>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan2_level" class="col-sm-2">Level </label>
                            <div class="col-sm-5">
                                <?php echo form_dropdown('atasan2_level', $levels, $data['atasan2_level'], 'id="atasan2_level" class="form-control input-sm select2"') ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="atasan2_pgs" class="col-sm-2">PGS? </label>
                            <div class="col-sm-5">
                                <?php echo form_dropdown('atasan2_pgs', $is_pgs, $data['atasan2_pgs'], 'id="atasan2_pgs" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="col-md-12 text-right">
                        <a class="post-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#atasan1_level").select2({
            placeholder:'Choose Level'
        });
        $("#atasan2_level").select2({
            placeholder:'Choose Level'
        });
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function getEmployee($atasan=0){
        if ($atasan==0){
            var id = document.getElementById("k_nopeg").value;
        } else if ($atasan==1) {
            var id = document.getElementById("atasan1_nopeg").value;
        } else if ($atasan==2) {
            var id = document.getElementById("atasan2_nopeg").value;
        }
        
        $.ajax({
            url : "<?php echo site_url('/web/karyawan/ajaxGetEmployee');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'id':id},
            success:function(respon){
                if ($atasan==0){
                    $("#div_employee_detail").removeClass("hidden");
                    if (respon=='0'){
                        document.getElementById("employee_detail").value = "Not Found!";
                    } else {
                        document.getElementById("employee_detail").value = 
                            "Name : " + respon.name + "\nPosition :  " + respon.jabatan_name+"\nWork Unit :  "+respon.unitkerja_name;
                    }
                } else if ($atasan==1) {
                    $("#div_atasan1_detail").removeClass("hidden");
                    if (respon=='0'){
                        document.getElementById("atasan1_detail").value = "Not Found!";
                    } else {
                        document.getElementById("atasan1_detail").value = "Name : " + respon.name + "\nPosition :  " + respon.jabatan_name+"\nWork Unit :  "+respon.unitkerja_name;
                    }
                } else if ($atasan==2){
                    $("#div_atasan2_detail").removeClass("hidden");
                    if (respon=='0'){
                        document.getElementById("atasan2_detail").value = "Not Found!";
                    } else {
                        document.getElementById("atasan2_detail").value = "Name : " + respon.name + "\nPosition :  " + respon.jabatan_name+"\nWork Unit :  "+respon.unitkerja_name;
                    }
                }
                
            } 
        });
    }

    function getPosition(atasan=1){
        if (atasan==1){
            var id = document.getElementById("atasan1_jabatan").value;
        } else if (atasan==2){
            var id = document.getElementById("atasan2_jabatan").value;
        }


        $.ajax({
            url : "<?php echo site_url('/web/jabatan/ajaxGetJabatan');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'id':id},
            success:function(respon){ 
                if (atasan==1) {
                    $("#div_atasan1_jabatan_detail").removeClass("hidden");
                    if (respon=='0'){
                        document.getElementById("atasan1_jabatan_detail").value = "Not Found!";
                    } else {
                        document.getElementById("atasan1_jabatan_detail").value = "Position : " + respon.name +"\nWork Unit :  "+respon.unitkerja_name;
                    }
                } else if (atasan==2){
                    $("#div_atasan2_jabatan_detail").removeClass("hidden");
                    if (respon=='0'){
                        document.getElementById("atasan2_jabatan_detail").value = "Not Found!";
                    } else {
                        document.getElementById("atasan2_jabatan_detail").value = "Position : " + respon.name + "\nWork Unit :  "+respon.unitkerja_name;
                    }
                }
                
            }
        }); 

    }

</script>
<script>
    $("#mainContent").addClass('col-sm-12');
    $("#mainContent").removeClass('col-sm-9');
</script>
<br>
<form action="<?= site_url('web/unitkerja').'/'.$method ?>" class="search-form" style="position:relative ;width:100%;bottom:5px">
    <div class="form-group has-feedback">
        <?php if ($method!='company'){?>
            <label for="company" class="sr-only">Company</label>
            <?php echo form_dropdown('company', $company, $_GET['company'], 'id="company" class="form-control input-sm" style="width:20%; font-size: 12px; letter-spacing: 0.5px;display:inline;margin-bottom:10px" onchange="goSearch()" '); ?>
        <?php } ?>
        <label for="search" class="sr-only">Search</label>
        <input type="text" class="form-control" name="search" id="search" placeholder="Search" value="<?= $_GET['search'] ?>">
        <a href="javascript:goSearch()"><i class="glyphicon glyphicon-search form-control-feedback" style="position:absolute;left:97%;top:<?= ($method!='company') ? '70%' : '25%'?>"></i></a>
    </div>       
</form>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center"><?= ($method=='company') ? 'Company Code' : 'Work Unit Code' ?></th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Initials</th>
                                        <?php if (SessionManagerWeb::isAdministrator()){?>
                                            <th class="text-center">Action</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $v) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $v['id'] ?></td>
                                            <td class="text-center"><?= $v['name'] ?></td>
                                            <td class="text-center"><?= $v['initial'] ?></td>
                                            <?php if (SessionManagerWeb::isAdministrator() or (SessionManagerWeb::isAdminUnit() and $v['company']==SessionManagerWeb::getVariablesIndex('mycompany'))){?>
                                                <td style="text-align: center">
                                                    <?php
                                                    if ($v['parent']==0){
                                                        echo anchor('web/unitkerja/edit/' . $v['id'].'/1', '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                    } else {
                                                        echo anchor('web/unitkerja/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                    }
                                                    
                                                   // echo anchor('web/user/delete/' . $v['user_id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip" title="Hapus"');
                                                    ?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function goExport() {
    location.href = "<?php echo site_url($path . $class . '/add_import') ?>";
}

function goAdd(company=0) {
    location.href = "<?php echo site_url($path . $class . '/add') ?>/"+company;
}

function goBack(method){
    location.href = "<?= site_url('web/setting') ?>";
}

function goSearch(){
    var search = document.getElementById('search').value;
    var company = document.getElementById('company').value;
    location.href = "<?= site_url('web/unitkerja') ?>?search="+search+"&company="+company;
}

</script>
<?php
	// include('document_detail_form_modal.php');
	include('document_detail_form_js.php');
?>

<a id="node_<?= $task_urutan ?>" style="display:block;top:-80px;position:relative"></a>
<div class="col-md-12">

	<div class="row" style="margin-left:5px">

	
	<?php
	// Rejected
	if ($data['status']=='C'){ ?>
		<div class="row" style="margin-bottom: 1%">
			<div class="col-md-11 shadowPanel" style="background-color: #fff;border-radius: 5px; width: 96%; margin-left: 1.5%">
	            <div class="col col-xs-8" style="margin: 2% 0% 0% 0%;padding: 0%;">
					<b style="color:red"><span class="label label-primary" ><?= $task_urutan ?></span> <?= $task['name'] ?> (REJECTED)</b>
	            </div>
				<div class="col-md-12" style="height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;    margin-bottom: 3%;">
					<div class="row" style="margin-left:5px">
						<a id="node_<?= $task_urutan ?>" style="display:block;top:-80px;position:relative"></a>
						<table class="table" >
							<tr>
							</tr>
							<tr style="border-bottom:1px solid">
							</tr>
							<tr>
								<td style="padding: 10px 0px 0px 0px;">
									<button id="btn_download" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download" style="margin-left:0.5%;">
					    				<i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
									    <span>Download</span>
									</button>
									<?php if (1==0) {?>
									<a href="<?= site_url('web/document/delete').'/'.$data['document_id'] ?>">
										<button id="btn_download" class="btn btn-sm btn-delsecond btn-rad" data-toggle="tooltip" title="Delete" style="margin-left:0.5%;">
						    				<i class="fa fa-trash" aria-hidden="true" style="margin-right: 10px;"></i>
										    <span>Remove Document</span>
										</button>			
									</a>
									<?php } ?>						
								</td>

							</tr>
						</table>
					</div>
		        </div>
			</div>
		</div>
	<?php }else 
	if ($task['form']=='A') { 
		switch ($data['workflow_id']) {
			case '1':
	?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
						</b>
	                </div>
	                <?php if ($type['need_reviewer']=='1' and $task['user']==Role::CREATOR) { ?>
		                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
		                	<?php 
		                	foreach ($data['review'] as $k_review => $v_review) { ?>
		                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
		                	<?php } ?>
							
		                </div>
	                <?php } ?>
		            <div class="col col-xs-12" style="margin:0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
					<!-- <div class="col-md-9" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
						<form>
							<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
							<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
						</form>
			        </div> -->
					
			        <?php
			        	// Kalau Approvernya Creator, maka bisa edit doc dan tambah reviewer
			        	// $pull_right = "";
			        	$pull_right="pull-right";
			        	if ($task['user']==Role::CREATOR){
			        		$pull_right="pull-right";
			        		$is_allow_edit = TRUE;
			        	}
			        ?>
				        <div class="col-md-12" id="div_edit_document" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
				        	<?php
							if ($data['is_upload']=='0') {
							?>
					    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
									<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
									<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
								</form>
								<?php// include('_document_js.php') ?>
				    		<?php } else { ?>
				    			<?php if ($data['ext']=='pdf') {  ?>
				    				<iframe name="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
					    		<?php } else { ?>
					    			<div id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
						    		    <iframe src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>

						    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
						    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
						    		</div>
					    		<?php } ?>
							<?php } ?>
			               	<br>
				        </div>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
		           		<?php if ($is_allow_edit) { ?>
							<button id="btn_download" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
							    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Download</span>
							</button>

							<button id="btn_simpan" class="btn btn-sm btn-success btn-rad pull-right <?= $pull_right ?> hidden" onClick="javascript:update_document(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Save</span>
							</button>
							<?php if ($data['need_reviewer']=='1') { ?>
								<button id="btn_add_reviewer" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:add_reviewer_document()" data-toggle="tooltip" title="Add Reviewer" style="margin-left:0.5%;">
								    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span> Reviewer</span>
								</button>
							<?php } ?>
							<? if ($data['is_upload']=='1') { ?>
								<button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_document()" data-toggle="tooltip" title="Update Document" style="margin-left:0.5%;">
								    <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Update Document</span>
								</button>
							<?php } else { ?>
								<!-- <button id="btn_save_as_draft" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:save_as_draft(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
								    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Save</span>
								</button> -->
							<?php } ?>
						<?php $hide_btn_approve ='';
							} ?>
						<button id="btn_approve" class="btn btn-sm btn-success btn-rad pull-right" onClick="approve_document()" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
						    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Approve</span>
						</button>
						<button id="btn_revise" class="btn btn-sm btn-warning btn-rad <?= $pull_right ?>" onClick="javascript:revise_document()" data-toggle="tooltip" title="Revise" style="margin-left:0.5%;">
						    <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Revise</span>
						</button>
						<button id="btn_reject" class="btn btn-sm btn-danger btn-rad <?= $pull_right ?>" onClick="javascript:reject_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
						    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Reject</span>
						</button>

		            </div>
			    </div>

			</div>

		<?php 
			break;
			case '2':
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #FFF;border-radius: 5px;">
	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
						</b>
	                </div>
	                <?php if ($type['need_reviewer']=='1') { ?>
		                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
		                	<?php 
		                	foreach ($data['review'] as $k_review => $v_review) { ?>
		                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
		                	<?php } ?>
							
		                </div>
	                <?php } ?>
		            <div class="col col-xs-12" style="margin:0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
					
			        <?php
			        	// Kalau Approvernya Creator, maka bisa edit doc dan tambah reviewer
			        	$pull_right="pull-right";
			        	if ($task['user']==Role::CREATOR){
			        		$pull_right="pull-right";
			        		$is_allow_edit = TRUE;
			        	}
			        ?>
			        <div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
			        	<?php if ($_SESSION['edit_evaluation']==1) { ?>
	           			<form action="<?= site_url('web/document/setDocumentEvaluation').'/'.$data['document_id'] ?>" method="post" id="form_evaluation">
		           			<table id="evaluation_table" style="width:100%" toolbar="#toolbar" >
		                        <thead>
		                            <tr style="border:1px solid">
		                            	<th field="form_no" width="5%" style="text-align: center;border:1px solid" >No. </th>
		                                <th field="verification_name" width="80%" style="text-align: center;border:1px solid" >Document Evaluation</th>
		                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Y/N? </th>
		                            </tr>
		                        </thead>
		                        <?php 
		                        $evaluation_no = 0;
		                        $hide_tr = '';
		                        foreach ($evaluation_forms as $k_form => $v_form) { 
		                        	$evaluation_no++;
		                        	if ($evaluation_no>1 and $evaluation['text'][1]==0){
		                        		$hide_tr = 'hidden';
		                        	}
		                        	?>
		                        	<tr class="<?= $hide_tr ?>" style="border:1px solid;" id="evaluation_tr_<?= $v_form['id'] ?>" onchange="checkEvaluationForm()">
		                        		<td style="border:1px solid">
			                                <label><?= $evaluation_no ?></label>
			                            </td>
			                            <td style="border:1px solid">
			                                <label for="input_evaluation_<?= $v_form['id'] ?>"><?= $v_form['text']?></label>
			                            </td>
			                            <td style="border:1px solid">
			                                <?php 
			                                	$form_id = $v_form['id'];
			                                    echo form_dropdown("select_evaluation_".$v_form['id'], array(0=>"NO", 1=>"YES"), $evaluation['text'][$evaluation_no], ' class="form-control input-sm" style="margin-bottom: 5px" id="select_evaluation_'.$form_id.'"'); ?>
			                            </td>
			                        </tr>
		                        <?php } ?>
		                    </table>      
		           			<table id="evaluation_comment" style="width:100%;margin-top: 10px" toolbar="#toolbar" >
	                        	<tr style="border:1px solid">
		                            <td style="border:1px solid;width:10%">
		                                <label for="input_evaluation_comment">Comment</label>
		                            </td>
		                            <td>
		                                <textarea id="input_evaluation_comment" name="input_evaluation_comment" value="<?= $evaluation['comment']  ?>"  style="padding:5px;width:100%;max-width: 100%"><?= $evaluation['comment']  ?></textarea>
		                            </td>
		                        </tr> 
		                    </table>
		                    <input type='submit' id='btn_submit_evaluation_form' class="hidden">
		                </form>
		                <?php } else{ ?>
		                <table id="evaluation_table" style="width:100%" toolbar="#toolbar" >
	                        <thead>
	                            <tr style="border:1px solid">
	                            	<th field="form_no" width="5%" style="text-align: center;border:1px solid" >No. </th>
	                                <th field="verification_name" width="80%" style="text-align: center;border:1px solid" >Document Evaluation</th>
	                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Y/N? </th>
	                            </tr>
	                        </thead>
	                        <?php 
	                        $evaluation_no = 0;
	                        $hide_tr = '';
	                        foreach ($evaluation_forms as $k_form => $v_form) { 
	                        	$evaluation_no++;
	                        	?>
	                        	<tr style="border:1px solid;" id="evaluation_tr_<?= $v_form['id'] ?>" >
	                        		<td style="border:1px solid">
		                                <label><?= $evaluation_no ?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <label for="input_evaluation_<?= $v_form['id'] ?>"><?= $v_form['text']?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <?php 
		                                	$form_id = $v_form['id'];
		                                	if ($evaluation['text'][$evaluation_no]==0){
		                                		echo '<b style="color:red">NO</b>';
		                                	} else if ($evaluation['text'][$evaluation_no]==1){
		                                		echo '<b style="color:green">YES</b>';
		                                	} 
		                                ?>
		                            </td>
		                        </tr>
	                        <?php } ?>
	                    </table>      
	           			<table id="evaluation_comment" style="width:100%;margin-top: 10px" toolbar="#toolbar" >
                        	<tr style="border:1px solid">
	                            <td style="border:1px solid;width:10%">
	                                <label for="input_evaluation_comment">Comment</label>
	                            </td>
	                            <td>
	                            	<label>
	                            		<b>
		                            		<?= $evaluation['comment']  ?>
		                            	</b>
	                            	</label>
	                            </td>
	                        </tr> 
	                    </table>
		                <?php } ?>
		            </div>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
						<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad " onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
						    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>View Document</span>
						</button>
						<?php if ($_SESSION['edit_evaluation']==0) { ?>
							<?php if ($is_allow_edit) { ?>
								<?php if ($data['need_reviewer']=='1') { ?>
									<button id="btn_add_reviewer" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:add_reviewer_document()" data-toggle="tooltip" title="Add Reviewer" style="margin-left:0.5%;">
									    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
									    <span> Reviewer</span>
									</button>
								<?php }  if($drafter_evaluation == null) { ?>
								
								<a href="<?= site_url('web/document/detail').'/'.$data['document_id'].'?edit_evaluation=1' ?>">
									<button id="activeEdit" class="btn btn-sm btn-secondary btn-rad"  data-toggle="tooltip" title="Edit Form" style="margin-left:0.5%;">
									    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
									    <span>Edit Form</span>
									</button>
								</a>
								<?php } ?>
							<?php } ?>
							<button id="btn_approve" class="btn btn-sm btn-success btn-rad pull-right" onClick="approve_document()" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
							    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Approve</span>
							</button>
							<button id="btn_reject" class="btn btn-sm btn-warning btn-rad <?= $pull_right ?>" onClick="javascript:revise_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
							    <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Revise</span>
							</button>
							<button id="btn_cancel" class="btn btn-sm btn-danger btn-rad pull-right" onClick="javascript:cancel_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
							    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Cancel</span>
							</button>
							<?php if ($is_allow_edit and 1==0) { ?>
								<button id="btn_reject" class="btn btn-sm btn-danger btn-rad <?= $pull_right ?>" onClick="javascript:reject_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
								    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Reject</span>
								</button>
							<?php } ?>
						<?php } else if ($_SESSION['edit_evaluation']==1) {?>
							<button id="btn_save_form" class="btn btn-sm btn-success btn-rad pull-right"  data-toggle="tooltip" title="Save Form" style="margin-left:0.5%;" onclick="save_evaluation_form()">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Save Form</span>
							</button>
							<a href="<?= site_url('web/document/detail').'/'.$data['document_id'].'?edit_evaluation=0' ?>">
								<button id="activeEdit" class="btn btn-sm btn-secondary btn-rad pull-right"  data-toggle="tooltip" title="Edit Form" style="margin-left:0.5%;">
								    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Cancel Edit</span>
								</button>
							</a>
						<?php } ?>

		            </div>
			    </div>

			</div>
		<?php
			break;
			case 3:
		 ?>
				<div class="row" style="margin-bottom: 3%;">
			        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
		                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
		                    <b>
		                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							</b>
		                </div>
		                <?php if ($type['need_reviewer']=='1' and $task['user']==Role::CREATOR) { ?>
			                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
			                	<?php 
			                	foreach ($data['review'] as $k_review => $v_review) { ?>
			                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
			                	<?php } ?>
								
			                </div>
		                <?php } ?>
			            <div class="col col-xs-12" style="margin:0%;padding: 0%;">
		                    <b>
		                    	<h4 class="text-center">
									<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
								</h4>
							</b>
		                </div>
						
				        <?php
				        	// Kalau Approvernya Creator, maka bisa edit doc dan tambah reviewer
				        	// $pull_right = "";
				        	$pull_right="pull-right";
				        	if ($task['user']==Role::CREATOR){
				        		$pull_right="pull-right";
				        		$is_allow_edit = TRUE;
				        	}
				        ?>
				        
				        <div class="col-md-12" id="div_edit_document" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
				        	<?php
							if ($data['is_upload']=='0') {
							?>
					    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
									<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
									<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
								</form>
				    		<?php } else { ?>
				    			<?php if ($data['ext']=='pdf') {  ?>
				    				<iframe name="viewer" id="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
					    		<?php } else { ?>
					    			<div id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
						    		    <iframe name="viewer" id="viewer" src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" tes='1' seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
						    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
						    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
						    		</div>
					    		<?php } ?>
							<?php } ?>
			               	<br>
				        </div>
			           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
			           		<?php if ($is_allow_edit) { ?>
				           		<div style="margin-bottom:10px">
					           		<button id="btn_compare" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:compare_document('<?= site_url('web/document/view_watermark/').'/'.$prev_document ?>')" data-toggle="tooltip" title="Compare Document with Previous Version" style="margin-left:0.5%;">
									    <i class="fa fa-clone" aria-hidden="true" style="margin-right: 10px;"></i>
									    <span>Compare</span>
									</button>
									<? if ($data['is_upload']=='1') { ?>
										<button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_document()" data-toggle="tooltip" title="Update Document" style="margin-left:0.5%;">
										    <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
										    <span>Update Document</span>
										</button>
									<?php } ?>
								</div>
								<button id="btn_download" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
								    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Download</span>
								</button>

								<button id="btn_simpan" class="btn btn-sm btn-success btn-rad pull-right <?= $pull_right ?> hidden" onClick="javascript:update_document(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
								    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Save</span>
								</button>
								<?php if ($data['need_reviewer']=='1') { ?>
									<button id="btn_add_reviewer" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:add_reviewer_document()" data-toggle="tooltip" title="Add Reviewer" style="margin-left:0.5%;">
									    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
									    <span> Reviewer</span>
									</button>
								<?php } ?>
								
							<?php $hide_btn_approve ='';
								} ?>
							<button id="btn_approve" class="btn btn-sm btn-success btn-rad pull-right" onClick="approve_document()" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
							    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Approve</span>
							</button>
							<button id="btn_revise" class="btn btn-sm btn-warning btn-rad <?= $pull_right ?>" onClick="javascript:revise_document()" data-toggle="tooltip" title="Revise" style="margin-left:0.5%;">
							    <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Revise</span>
							</button>
							<button id="btn_reject" class="btn btn-sm btn-danger btn-rad <?= $pull_right ?>" onClick="javascript:reject_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
							    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Reject</span>
							</button>

			            </div>
				    </div>

				</div>
		<?php
			break;
		?>
	<?php } ?>
	<?php } else if ($task['form']=='R') { 
		switch($data['workflow_id']){
			case '1':
	?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							}?>
						</b>
	                </div>
	                 <?php if ($type['need_reviewer']=='1') { ?>
		                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
		                	<?php 
		                	foreach ($data['review'] as $k_review => $v_review) { ?>
		                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
		                	<?php } ?>
							
		                </div>
	                <?php } ?>
	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
	                <div class="col-md-12" id="div_edit_document" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
		                <?php
							if ($data['is_upload']=='0') {
						?>
				    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
								<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
								<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
							</form>
			    		<?php } else { ?>
			    			<?php if ($data['ext']=='pdf') {  ?>
			    				<iframe name="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
				    		<?php } else { ?>
				    			<div id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
					    		    <iframe src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
					    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
					    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
					    		</div>
				    		<?php } ?>
						<?php } ?>
					</div>
	                <?php 
	                $allowed = "disabled";
	                if (in_array(SessionManagerWeb::getUserID(), $data['reviewer']) and !in_array(SessionManagerWeb::getUserID(), $data['reviewed'])) { 
	                	$allowed = ""; }
	                ?>
	                <div class="col-md-12" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
						<!-- FORM REVIEW-->
						<form action="" <?= $allowed ?> method="post">
							<label class="col-md-2 control-label" for="comment">Comment</label>
							<textarea id="comment_textarea" placeholder="Write Comment" style="margin-right:10px;width:40%;height:100px"></textarea>
						</form>
			        </div>
		           	<div class="col col-xs-5" style="margin: 2% 0%;padding: 0% 0px 0% 0%;right:-1%">
						<button id="btn_agree" class="btn btn-sm btn-success btn-secondary btn-rad pull-right" <?= $allowed ?> onClick="javascript:review(<?= $data['document_id'].','.SessionManagerWeb::getUserID().',1'.','.$next ?>)" data-toggle="tooltip" title="Setuju" style="margin-left:0.5%;">
						    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Agree</span>
						</button>
						<button id="btn_disagree" class="btn btn-sm btn-danger btn-secondary btn-rad pull-right" <?= $allowed ?> onClick="javascript:review(<?= $data['document_id'].','.SessionManagerWeb::getUserID().',0'.','.$next ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
						    <i class="fa fa-close" aria-hidden="true" style="margin-right: 10px;"></i>
						    <span>Disagree</span>
						</button>					
		            </div>
		            <?php if ($counter['reviewed']>0 and 1==0) { ?>
			            <div class="col-md-12" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">

							<table class="table" >
								<tr>
									<td colspan=2 style="padding:0px"><h3 style="background-color:#ccc;padding:3px;margin-top:7px">Review</h3></td>
								</tr>
								<?php foreach ($data['review'] as $key=>$rev) { ?>
									<tr>
										<td class="e-left-field" width="150">Reviewer</td>
										<td><?= $rev['reviewer_name'] ?></td>
									</tr>
									<tr>
										<td class="e-left-field" width="150">Status</td>
										<td><?= $rev['is_agree'] ? "<b style='color:green'>Setuju</b>" : "<b style='color:red'>Tidak Setuju</b>" ?></td>
									</tr>
									<tr>
										<td class="e-left-field">Comment</td>
										<td><?= $rev['review'] ?></td>
									</tr>
									<tr style="border-bottom:1px solid">
										<td class="e-left-field" width="150">Review Date</td>
										<td><b><?= strftime('%d %b %Y %H:%M:%S', strtotime($rev['created_at']))  ?></b></td>
									</tr>
								<?php } ?>
							</table>
				        </div>
			        <?php } ?>
			    </div>
			</div>
		<?php 
			break;
			case '2':
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #FFF;border-radius: 5px;">
	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							}?>
						</b>
	                </div>
	                 <?php if ($type['need_reviewer']=='1') { ?>
		                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
		                	<?php 
		                	foreach ($data['review'] as $k_review => $v_review) { ?>
		                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
		                	<?php } ?>
							
		                </div>
	                <?php } ?>
	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
    	           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
	           			<table id="evaluation_table" style="width:100%" toolbar="#toolbar" >
	                        <thead>
	                            <tr style="border:1px solid">
	                            	<th field="form_no" width="5%" style="text-align: center;border:1px solid" >No. </th>
	                                <th field="verification_name" width="80%" style="text-align: center;border:1px solid" >Document Evaluation</th>
	                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Y/N? </th>
	                            </tr>
	                        </thead>
	                        <?php 
	                        $evaluation_no = 0;
	                        $hide_tr = '';
	                        foreach ($evaluation_forms as $k_form => $v_form) { 
	                        	$evaluation_no++;
	                        	?>
	                        	<tr style="border:1px solid;" id="evaluation_tr_<?= $v_form['id'] ?>" >
	                        		<td style="border:1px solid">
		                                <label><?= $evaluation_no ?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <label for="input_evaluation_<?= $v_form['id'] ?>"><?= $v_form['text']?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <?php 
		                                	$form_id = $v_form['id'];
		                                	if ($evaluation['text'][$evaluation_no]==0){
		                                		echo '<b style="color:red">NO</b>';
		                                	} else if ($evaluation['text'][$evaluation_no]==1){
		                                		echo '<b style="color:green">YES</b>';
		                                	} 
		                                ?>
		                            </td>
		                        </tr>
	                        <?php } ?>
	                    </table>      
	           			<table id="evaluation_comment" style="width:100%;margin-top: 10px" toolbar="#toolbar" >
                        	<tr style="border:1px solid">
	                            <td style="border:1px solid;width:10%">
	                                <label for="input_evaluation_comment">Comment</label>
	                            </td>
	                            <td>
	                            	<label>
	                            		<b>
		                            		<?= $evaluation['comment']  ?>
		                            	</b>
	                            	</label>
	                            </td>
	                        </tr> 
	                    </table>
	                    <input type='submit' id='btn_submit_evaluation_form' class="hidden">
		            </div>
	                <?php 
	                $allowed = "disabled";
	                if (in_array(SessionManagerWeb::getUserID(), $data['reviewer']) and !in_array(SessionManagerWeb::getUserID(), $data['reviewed'])) { 
	                	$allowed = ""; }
	                ?>
	                <div class="col-md-12" style="background-color: #F0FFFF;border-top:1px solid">
		                <div class="col-md-12" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
							<!-- FORM REVIEW-->
							<form action="" <?= $allowed ?> method="post">
								<label class="col-md-2 control-label" for="comment">Comment</label>
								<textarea id="comment_textarea" placeholder="Write Comment" style="margin-right:10px;width:80%;height:100px"></textarea>
							</form>
				        </div>

			           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0% 0px 0% 0%;right:-1%">
			           		<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad " onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
							    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>View Document</span>
							</button>
							<button id="btn_agree" class="btn btn-sm btn-success btn-secondary btn-rad pull-right" <?= $allowed ?> onClick="javascript:review(<?= $data['document_id'].','.SessionManagerWeb::getUserID().',1'.','.$next ?>)" data-toggle="tooltip" title="Setuju" style="margin-left:0.5%;">
							    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Agree</span>
							</button>
							<button id="btn_disagree" class="btn btn-sm btn-danger btn-secondary btn-rad pull-right" <?= $allowed ?> onClick="javascript:review(<?= $data['document_id'].','.SessionManagerWeb::getUserID().',0'.','.$next ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
							    <i class="fa fa-close" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Disagree</span>
							</button>					
			            </div>
			        </div>
		            <?php if ($counter['reviewed']>0 and 1==0) { ?>
			            <div class="col-md-12" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">

							<table class="table" >
								<tr>
									<td colspan=2 style="padding:0px"><h3 style="background-color:#ccc;padding:3px;margin-top:7px">Review</h3></td>
								</tr>
								<?php foreach ($data['review'] as $key=>$rev) { ?>
									<tr>
										<td class="e-left-field" width="150">Reviewer</td>
										<td><?= $rev['reviewer_name'] ?></td>
									</tr>
									<tr>
										<td class="e-left-field" width="150">Status</td>
										<td><?= $rev['is_agree'] ? "<b style='color:green'>Setuju</b>" : "<b style='color:red'>Tidak Setuju</b>" ?></td>
									</tr>
									<tr>
										<td class="e-left-field">Comment</td>
										<td><?= $rev['review'] ?></td>
									</tr>
									<tr style="border-bottom:1px solid">
										<td class="e-left-field" width="150">Review Date</td>
										<td><b><?= strftime('%d %b %Y %H:%M:%S', strtotime($rev['created_at']))  ?></b></td>
									</tr>
								<?php } ?>
							</table>
				        </div>
			        <?php } ?>
			    </div>
			</div>
		<?php	
			break;
			case '3':
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							}?>
						</b>
	                </div>
	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
	                <div class="col-md-12" id="div_edit_document" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
		                <?php
							if ($data['is_upload']=='0') {
						?>
				    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
								<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
								<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
							</form>
			    		<?php } else { ?>
			    			<?php if ($data['ext']=='pdf') {  ?>
			    				<iframe name="viewer" id="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
				    		<?php } else { ?>
				    			<div id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
					    		    <iframe name="viewer" id="viewer" src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
					    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
					    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
					    		</div>
				    		<?php } ?>
						<?php } ?>
					</div>
	                <?php 
	                $allowed = "disabled";
	                if (in_array(SessionManagerWeb::getUserID(), $data['reviewer']) and !in_array(SessionManagerWeb::getUserID(), $data['reviewed'])) { 
	                	$allowed = ""; }
	                ?>
	                <div class="col-md-12" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
						<!-- FORM REVIEW-->
						<form action="" <?= $allowed ?> method="post">
							<label class="col-md-2 control-label" for="comment">Comment</label>
							<textarea id="comment_textarea" placeholder="Write Comment" style="margin-right:10px;width:40%;height:100px"></textarea>
						</form>
			        </div>
		           	<div class="col col-xs-7" style="margin: 2% 0%;padding: 0% 0px 0% 0%;right:-1%">
		           		<button id="btn_compare" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:compare_document('<?= site_url('web/document/view_watermark/').'/'.$prev_document ?>')" data-toggle="tooltip" title="Compare Document with Previous Version" style="margin-left:0.5%;">
						    <i class="fa fa-clone" aria-hidden="true" style="margin-right: 10px;"></i>
						    <span>Compare</span>
						</button>
						<button id="btn_agree" class="btn btn-sm btn-success btn-secondary btn-rad pull-right" <?= $allowed ?> onClick="javascript:review(<?= $data['document_id'].','.SessionManagerWeb::getUserID().',1'.','.$next ?>)" data-toggle="tooltip" title="Setuju" style="margin-left:0.5%;">
						    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Agree</span>
						</button>
						<button id="btn_disagree" class="btn btn-sm btn-danger btn-secondary btn-rad pull-right" <?= $allowed ?> onClick="javascript:review(<?= $data['document_id'].','.SessionManagerWeb::getUserID().',0'.','.$next ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
						    <i class="fa fa-close" aria-hidden="true" style="margin-right: 10px;"></i>
						    <span>Disagree</span>
						</button>					
		            </div>
		            <?php if ($counter['reviewed']>0 and 1==0) { ?>
			            <div class="col-md-12" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">

							<table class="table" >
								<tr>
									<td colspan=2 style="padding:0px"><h3 style="background-color:#ccc;padding:3px;margin-top:7px">Review</h3></td>
								</tr>
								<?php foreach ($data['review'] as $key=>$rev) { ?>
									<tr>
										<td class="e-left-field" width="150">Reviewer</td>
										<td><?= $rev['reviewer_name'] ?></td>
									</tr>
									<tr>
										<td class="e-left-field" width="150">Status</td>
										<td><?= $rev['is_agree'] ? "<b style='color:green'>Setuju</b>" : "<b style='color:red'>Tidak Setuju</b>" ?></td>
									</tr>
									<tr>
										<td class="e-left-field">Comment</td>
										<td><?= $rev['review'] ?></td>
									</tr>
									<tr style="border-bottom:1px solid">
										<td class="e-left-field" width="150">Review Date</td>
										<td><b><?= strftime('%d %b %Y %H:%M:%S', strtotime($rev['created_at']))  ?></b></td>
									</tr>
								<?php } ?>
							</table>
				        </div>
			        <?php } ?>
			    </div>
			</div>
		<?php break; ?>
		<?php } ?>
	<?php } else if ($task['form']=='V') { 
		switch($data['workflow_id']){
			case '1':
	?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
						</b>
	                </div>        

	                <?php
	                	if ($data['approver_id']==NULL or $data['approver_id']==''){
	                ?>
	                		<div class="row col-md-12">
			                    <div class="alert alert-warning" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #FFB300 ;">
			                        <label style="color: #fff;"><b>Warning : There's no Approver on this document.<br>Please choose approver by clicking the <b style="color:blue">"Set Attribute"</b> button</b></label>
			                    </div>
			                </div>
	                <?php		
	                	}
	                ?>

	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>

			        <div class="col-md-12" id="div_edit_document" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
			        	<?php
							if ($data['is_upload']=='0') {
						?>
				    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
								<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
								<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
							</form>
			    		<?php } else { ?>
			    			<?php if ($data['ext']=='pdf') {  ?>
			    				<iframe name="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
				    		<?php } else { ?>
				    			<div id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
					    		    <iframe src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
					    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
					    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
					    		</div>
				    		<?php } ?>
						<?php } ?>
						<br>
			        </div>
			        <?php
	                	if (($data['approver_id']==NULL or $data['approver_id']=='') and $data['verification'] and $data['is_verification_appropriate']==0){
	                ?>
	                		<div class="row col-md-12">
			                    <div class="alert alert-danger" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #e57373 ;text-align: right">
			                        <label style="color: #fff;"><b>Cannot continue to the step because there's no <b style="color:green">Approver</b> on this document.<br>Please choose approver by clicking the <b style="color:blue">"Set Attribute"</b> button</b></label>
			                    </div>
			                </div>
	                <?php		
	                	}
	                ?>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
						<button id="btn_lihat" class="btn btn-sm btn-secondary btn-rad hidden <?= $hide_btn_lihat_dokumen ?>" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Lihat Dokumen" style="margin-left:0.5%;">
						    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Lihat Dokumen</span>
						</button>
						<div style="margin-bottom: 10px">
							<button id="btn_download" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
							    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Download</span>
							</button>
							<? if ($data['is_upload']=='1') { ?>
								<button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_document()" data-toggle="tooltip" title="Update Document" style="margin-left:0.5%;">
								    <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Update Document</span>
								</button>
							<?php } else { ?>
								<!-- <button id="btn_save_as_draft" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:save_as_draft(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
								    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Save</span>
								</button> -->
							<?php } ?>
						</div>
		                <?php if ($is_chief) { ?>
			                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#delegate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
							    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Delegate</span>
							</button>
				        <? } ?> 
						<?php if ($data['verification']){
						        if ($data['is_verification_appropriate']==0) { ?>
							        <?php if ($data['approver_id']!=NULL and $data['approver_id']!=''){ ?>
										<button id="btn_approve" class="btn btn-sm btn-success btn-rad pull-right"  onClick="approve_document()" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
										    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
										    <span>Approve</span>
										</button>
									<?php } ?>
								<?php } else { ?>
									<button id="btn_revise" class="btn btn-sm btn-warning btn-secondary btn-rad pull-right" onClick="javascript:revise_document()" data-toggle="tooltip" title="Revise" style="margin-left:0.5%;">
									    <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
									    <span>Revise</span>
									</button>
								<?php } ?>
						<?php } ?>
						<button id="btn_verificate" class="btn btn-sm btn-secondary btn-rad pull-right" data-target="#verification_modal" data-toggle="modal" title="Fill the Verification Form to Approve" style="margin-left:0.5%;">
						    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Verification Form</span>
						</button>
						<?php// include('_document_js.php') ?>
						<button id="btn_set_attribute" class="btn btn-sm btn-secondary btn-rad pull-right" onClick="set_attribute_document()" data-toggle="tooltip" title="Set Attribute" style="margin-left:0.5%;">
						    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Set Attribute</span>
						</button>

						<button id="btn_reject" class="btn btn-sm btn-danger btn-rad pull-right" onClick="javascript:reject_document()" data-toggle="tooltip" title="Set Attribute" style="margin-left:0.5%;">
						    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Reject</span>
						</button>
						<button id="btn_simpan" class="btn btn-sm btn-success btn-rad pull-right hidden" onClick="javascript:update_document(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Simpan" style="margin-left:0.5%;">
						    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Save</span>
						</button>
						<!-- <button id="btn_revise" class="btn btn-sm btn-warning btn-secondary btn-rad pull-right" onClick="javascript:revise_document()" data-toggle="tooltip" title="Revise" style="margin-left:0.5%;">
						    <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Revise</span>
						</button> -->
		            </div>
			    </div>
			</div>
		<?php 
			break;
			case '2':
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #FFF;border-radius: 5px;">
	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
						</b>
	                </div>
	                <?php if ($type['need_reviewer']=='1') { ?>
		                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
		                	<?php 
		                	foreach ($data['review'] as $k_review => $v_review) { ?>
		                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
		                	<?php } ?>
							
		                </div>
	                <?php } ?>
		            <div class="col col-xs-12" style="margin:0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
					<!-- <div class="col-md-9" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
						<form>
							<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
							<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
						</form>
			        </div> -->
					
			        <?php
			        	// Kalau Approvernya Creator, maka bisa edit doc dan tambah reviewer
			        	// $pull_right = "";
			        	$pull_right="pull-right";
			        	if ($task['user']==Role::CREATOR){
			        		$pull_right="pull-right";
			        		$is_allow_edit = TRUE;
			        	}
			        ?>
			        <div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
			        	<?php if ($_SESSION['edit_evaluation']==1) { ?>
	           			<form action="<?= site_url('web/document/setDocumentEvaluation').'/'.$data['document_id'] ?>" method="post" id="form_evaluation">
		           			<table id="evaluation_table" style="width:100%" toolbar="#toolbar" >
		                        <thead>
		                            <tr style="border:1px solid">
		                            	<th field="form_no" width="5%" style="text-align: center;border:1px solid" >No. </th>
		                                <th field="verification_name" width="80%" style="text-align: center;border:1px solid" >Document Evaluation</th>
		                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Y/N? </th>
		                            </tr>
		                        </thead>
		                        <?php 
		                        $evaluation_no = 0;
		                        $hide_tr = '';
		                        foreach ($evaluation_forms as $k_form => $v_form) { 
		                        	$evaluation_no++;
		                        	if ($evaluation_no>1 and $evaluation['text'][1]==0){
		                        		$hide_tr = 'hidden';
		                        	}
		                        	?>
		                        	<tr class="<?= $hide_tr ?>" style="border:1px solid;" id="evaluation_tr_<?= $v_form['id'] ?>" onchange="checkEvaluationForm()">
		                        		<td style="border:1px solid">
			                                <label><?= $evaluation_no ?></label>
			                            </td>
			                            <td style="border:1px solid">
			                                <label for="input_evaluation_<?= $v_form['id'] ?>"><?= $v_form['text']?></label>
			                            </td>
			                            <td style="border:1px solid">
			                                <?php 
			                                	$form_id = $v_form['id'];
			                                    echo form_dropdown("select_evaluation_".$v_form['id'], array(0=>"NO", 1=>"YES"), $evaluation['text'][$evaluation_no], ' class="form-control input-sm" style="margin-bottom: 5px" id="select_evaluation_'.$form_id.'"'); ?>
			                            </td>
			                        </tr>
		                        <?php } ?>
		                    </table>      
		           			<table id="evaluation_comment" style="width:100%;margin-top: 10px" toolbar="#toolbar" >
	                        	<tr style="border:1px solid">
		                            <td style="border:1px solid;width:10%">
		                                <label for="input_evaluation_comment">Comment</label>
		                            </td>
		                            <td>
		                                <textarea id="input_evaluation_comment" name="input_evaluation_comment" value="<?= $evaluation['comment']  ?>"  style="padding:5px;width:100%;max-width: 100%"><?= $evaluation['comment']  ?></textarea>
		                            </td>
		                        </tr> 
		                    </table>
		                    <input type='submit' id='btn_submit_evaluation_form' class="hidden">
		                </form>
		                <?php } else{ ?>
		                <table id="evaluation_table" style="width:100%" toolbar="#toolbar" >
	                        <thead>
	                            <tr style="border:1px solid">
	                            	<th field="form_no" width="5%" style="text-align: center;border:1px solid" >No. </th>
	                                <th field="verification_name" width="80%" style="text-align: center;border:1px solid" >Document Evaluation</th>
	                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Y/N? </th>
	                            </tr>
	                        </thead>
	                        <?php 
	                        $evaluation_no = 0;
	                        $hide_tr = '';
	                        foreach ($evaluation_forms as $k_form => $v_form) { 
	                        	$evaluation_no++;
	                        	?>
	                        	<tr style="border:1px solid;" id="evaluation_tr_<?= $v_form['id'] ?>" >
	                        		<td style="border:1px solid">
		                                <label><?= $evaluation_no ?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <label for="input_evaluation_<?= $v_form['id'] ?>"><?= $v_form['text']?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <?php 
		                                	$form_id = $v_form['id'];
		                                	if ($evaluation['text'][$evaluation_no]==0){
		                                		echo '<b style="color:red">NO</b>';
		                                	} else if ($evaluation['text'][$evaluation_no]==1){
		                                		echo '<b style="color:green">YES</b>';
		                                	} 
		                                ?>
		                            </td>
		                        </tr>
	                        <?php } ?>
	                    </table>      
	           			<table id="evaluation_comment" style="width:100%;margin-top: 10px" toolbar="#toolbar" >
                        	<tr style="border:1px solid">
	                            <td style="border:1px solid;width:10%">
	                                <label for="input_evaluation_comment">Comment</label>
	                            </td>
	                            <td>
	                            	<label>
	                            		<b>
		                            		<?= $evaluation['comment']  ?>
		                            	</b>
	                            	</label>
	                            </td>
	                        </tr> 
	                    </table>
		                <?php } ?>
		            </div>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
						<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad " onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
						    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>View Document</span>
						</button>
						<?php if ($is_chief) { ?>
			                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#delegate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
							    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Delegate</span>
							</button>
				        <? } ?> 
						<button id="btn_approve" class="btn btn-sm btn-success btn-rad pull-right" onClick="approve_document()" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
						    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Approve Withdrawal</span>
						</button>
						<button id="btn_reject" class="btn btn-sm btn-warning btn-rad <?= $pull_right ?>" onClick="javascript:revise_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
						    <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Revise Evaluation Form</span>
						</button>
		            </div>
			    </div>

			</div>	
		<?php
			break;
			case 3:
		?>
				<div class="row" style="margin-bottom: 3%;">
			        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
		                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
		                    <b>
		                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							</b>
		                </div>
		                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
		                    <b>
		                    	<h4 class="text-center">
									<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
								</h4>
							</b>
		                </div> 	

				        <div class="col-md-12" id="div_edit_document" style="/*width:50%;*/height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
				        	<?php
								if ($data['is_upload']=='0') {
							?>
					    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
									<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
									<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
								</form>
				    		<?php } else { ?>
				    			<?php if ($data['ext']=='pdf') {  ?>
				    				<iframe name="viewer" id="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
					    		<?php } else { ?>
					    			<div  id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
						    		    <iframe name="viewer" id="viewer" src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
						    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
						    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
						    		</div>
					    		<?php } ?>
							<?php } ?>
							<br>
				        </div>
			           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
							<button id="btn_lihat" class="btn btn-sm btn-secondary btn-rad hidden <?= $hide_btn_lihat_dokumen ?>" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Lihat Dokumen" style="margin-left:0.5%;">
							    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Lihat Dokumen</span>
							</button>
							<div style="margin-bottom: 10px">
								<button id="btn_compare" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:compare_document('<?= site_url('web/document/view_watermark/').'/'.$prev_document ?>')" data-toggle="tooltip" title="Compare Document with Previous Version" style="margin-left:0.5%;">
								    <i class="fa fa-clone" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Compare</span>
								</button>
				                <? if ($data['is_upload']=='1') { ?>
									<button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_document()" data-toggle="tooltip" title="Update Document" style="margin-left:0.5%;">
									    <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
									    <span>Update Document</span>
									</button>
								<?php } else { ?>
									<!-- <button id="btn_save_as_draft" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:save_as_draft(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
									    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;"></i>
									    <span>Save</span>
									</button> -->
								<?php } ?> 
							</div>
							<?php if ($is_chief or 1==1) { ?>
				                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#delegate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
								    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Delegate</span>
								</button>
					        <? } ?> 
							<button id="btn_download" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
							    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Download</span>
							</button>
							
					        <?php if ($data['verification']){
							        if ($data['is_verification_appropriate']==0) { ?>
										<button id="btn_approve" class="btn btn-sm btn-success btn-rad pull-right"  onClick="approve_document()" data-toggle="tooltip" title="Approve" style="margin-left:0.5%;">
										    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
										    <span>Approve</span>
										</button>
									<?php } else { ?>
										<button id="btn_revise" class="btn btn-sm btn-warning btn-secondary btn-rad pull-right" onClick="javascript:revise_document()" data-toggle="tooltip" title="Revise" style="margin-left:0.5%;">
										    <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
										    <span>Revise</span>
										</button>
									<?php } ?>
							<?php } ?>
							<button id="btn_verificate" class="btn btn-sm btn-secondary btn-rad pull-right" data-target="#verification_modal" data-toggle="modal" title="Fill the Verification Form to Approve" style="margin-left:0.5%;">
							    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Verification Form</span>
							</button>
							<?php// include('_document_js.php') ?>
							<button id="btn_set_attribute" class="btn btn-sm btn-secondary btn-rad pull-right" onClick="set_attribute_document(false)" data-toggle="tooltip" title="Set Attribute" style="margin-left:0.5%;">
							    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Set Attribute</span>
							</button>
							<button id="btn_reject" class="btn btn-sm btn-danger btn-rad pull-right" onClick="javascript:reject_document()" data-toggle="tooltip" title="Set Attribute" style="margin-left:0.5%;">
							    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Reject</span>
							</button>
							<button id="btn_simpan" class="btn btn-sm btn-success btn-rad pull-right hidden" onClick="javascript:update_document(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Simpan" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Save</span>
							</button>
			            </div>
				    </div>
				</div>
			<?php break; ?>
		<?php } ?>
	<?php } else if ($task['form']=='C') {  ?>
		<?php switch($data['workflow_id']) {
			case 1:
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
	                <div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							}?>
						</b>					
	                </div>
	                <?php if ($data['status']=='R') { ?>
	                	<div class="col col-xs-12" style="margin-bottom:10px;margin-top:10px;padding-bottom:5px;border-bottom: 1px solid;">
	                		<b>- <?= $revise_requestor['name'] ?></b> ask you to revise on Evaluation Form for Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because <b style="color:red"><?= $revise_requestor['reason'] ?></b>.<br>
							
		                </div>
		                <?php if ($revise_verification!=false) { ?>
			                <div class="row col-md-12">
			                	<table id="verification_table" style="width:100%;border:1px solid;background-color: white" toolbar="#toolbar" >
			                        <thead>
			                            <tr>
			                                <th field="verification_name" width="35%" style="text-align: center;border:1px solid" >Document Verification</th>
			                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Appropriate? </th>
			                                <th field="comment" width="55%" style="text-align: center;border:1px solid">Comment</th>
			                            </tr>
			                        </thead>
			                        <tr style="border:1px solid">
			                            <td style="border:1px solid">
			                                <label for="input_verification_template">Standar Penulisan Sesuai Template</label>
			                            </td>
			                            <td>
			                            	<label class='form-control input-sm'>
			                            		<?= ($revise_verification['template']['is_appropriate']) ? 'YES':'NO' ?>
			                            	</label>
			                            </td>
			                            <td style="border:1px solid">
			                            	<label>
			                            		<?= $revise_verification['template']['text']  ?>
			                            	</label>
			                            </td>
			                        </tr>
			                        <tr style="border:1px solid">
			                            <td style="border:1px solid">
			                                <label for="select_verification_tugas">Kesesuaian Terhadap Tugas Pokok Fungsi</label>
			                            </td>
			                            <td>
			                            	<label class='form-control input-sm'>
			                            		<?= ($revise_verification['tugas']['is_appropriate']) ? 'YES':'NO' ?>
			                            	</label>
			                            </td>
			                            <td style="border:1px solid">
			                            	<label>
			                            		<?= $revise_verification['tugas']['text']  ?>
			                            	</label>
			                            </td>
			                        </tr>
			                        <tr style="border:1px solid">
			                            <td style="border:1px solid">
			                                <label for="input_verification_penulisan">Keselarasan Penulisan Pada Dokumen</label>
			                            </td>
			                            <td>
			                                <label class='form-control input-sm'>
			                            		<?= ($revise_verification['penulisan']['is_appropriate']) ? 'YES':'NO' ?>
			                            	</label>
			                            </td>
			                            <td style="border:1px solid">
			                            	<label>
			                            		<?= $revise_verification['penulisan']['text']  ?>
			                            	</label>
			                            </td>
			                        </tr>
			                        <tr>
			                            <td style="border:1px solid">
			                                <label for="input_verification_perubahan">Rincian Perubahan Dokumen</label>
			                            </td>
			                            <td colspan="2">
			                                <table id="table_perubahan" class="" cellspacing="0" width="100%" border="1">
			                                    <thead>
			                                        <tr>
			                                            <th>BAB</th>
			                                            <th>KONDISI LAMA</th>
			                                            <th>KONDISI BARU</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        <?php 
			                                        $x=0;
			                                        foreach($revise_verification['perubahan']['text'] as $value) {
			                                        ?>
			                                            <tr>
			                                                <td>
			                                                	<label>
								                            		<?= $value['bab']  ?>
								                            	</label>
			                                                </td>
			                                                <td>
			                                                	<label>
								                            		<?= $value['old']  ?>
								                            	</label>
			                                                </td>
			                                                <td>
			                                                	<label>
								                            		<?= $value['new']  ?>
								                            	</label>
			                                                </td>
			                                            </tr>
			                                        <?php 
			                                            $x++;
			                                        } 
			                                        ?>
			                                    </tbody>

			                                </table>
			                            </td>
			                        </tr>
			                    </table>
			                </div>
		                <?php } ?>
	                <?php } ?>
	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
					<div class="col-md-12" style="height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
			        	<?php
							if ($data['is_upload']=='0') {
						?>
				    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
								<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
								<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
							</form>
			    		<?php } else { ?>
			    			<?php if ($data['ext']=='pdf') {  ?>
			    				<iframe name="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
				    		<?php } else { ?>
				    			<div id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
					    		    <iframe src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
					    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
					    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
					    		</div>
				    		<?php } ?>
						<?php } ?>
		               	<br>
			        </div>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
						<?php if ($data['status']=='R') { ?>
							<button id="btn_unduh" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Dokumen" style="margin-left:0.5%;">
							    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Download</span>
							</button>
						<? } ?>
						<? if ($data['is_upload']=='1') { ?>
							<button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_document()" data-toggle="tooltip" title="Update Document" style="margin-left:0.5%;">
							    <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Update Document</span>
							</button>
						<?php } else { ?>
							<!-- <button id="btn_save_as_draft" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:save_as_draft(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Save</span>
							</button> -->
						<?php } ?>
						<button id="btn_submit" class="btn btn-sm btn-success btn-rad pull-right" onClick="submit_document()" data-toggle="tooltip" title="Submit" style="margin-left:0.5%;">
						    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Submit</span>
						</button>
						<?php// include('_document_js.php') ?>
						<button id="btn_set_attribute" class="btn btn-sm btn-secondary btn-rad pull-right" onClick="set_attribute_document()" data-toggle="tooltip" title="Set Attribute" style="margin-left:0.5%;">
						    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Set Attribute</span>
						</button>
		            </div>
			    </div>
			</div>
		<?php 
		break; 
		case 3 : 
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #F0FFFF;border-radius: 5px;">
	                <div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<?php 
		                    	$actor_role = $task['user'];
								if ($task['urutan']==1 and in_array($data['workflow_id'], array(2,3))){
									if ($data['creator_id']!=$data['user_id']){
										$actor_role = 'D';
									}
								}
	                    	?>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($actor_role) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							}?>
						</b>					
	                </div>
	                <?php if ($data['status']=='R') { ?>
	                	<div class="col col-xs-12" style="margin-bottom:10px;margin-top:10px;padding-bottom:5px;border-bottom: 1px solid;">
	                		<b>- <?= $revise_requestor['name'] ?></b> ask you to revise on Evaluation Form for Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because <b style="color:red"><?= $revise_requestor['reason'] ?></b>.<br>
							
		                </div>
	                <?php } ?>
	                <?php if ($is_file_exist) { ?>
		                <div class="row col-md-12">
		                    <div class="alert alert-warning" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #FFB300 ;">
		                        <label style="color: #fff;">To Change the attributes(requirements, clausuls, reviewers, change log, etc.) of this document, you can Click the <b>"Set Attribute"</b> button</label>
		                    </div>
		                </div>
		                <?php if ($data['status']=='R') { ?>
		                	<?php if (1==0){ ?>
		                	<?php } ?>
			                <?php if ($revise_verification!=false) { ?>
				                <div class="row col-md-12">
				                	<table id="verification_table" style="width:100%;border:1px solid;background-color: white" toolbar="#toolbar" >
				                        <thead>
				                            <tr>
				                                <th field="verification_name" width="35%" style="text-align: center;border:1px solid" >Document Verification</th>
				                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Appropriate? </th>
				                                <th field="comment" width="55%" style="text-align: center;border:1px solid">Comment</th>
				                            </tr>
				                        </thead>
				                        <tr style="border:1px solid">
				                            <td style="border:1px solid">
				                                <label for="input_verification_template">Standar Penulisan Sesuai Template</label>
				                            </td>
				                            <td>
				                            	<label class='form-control input-sm'>
				                            		<?= ($revise_verification['template']['is_appropriate']) ? 'YES':'NO' ?>
				                            	</label>
				                            </td>
				                            <td style="border:1px solid">
				                            	<label>
				                            		<?= $revise_verification['template']['text']  ?>
				                            	</label>
				                            </td>
				                        </tr>
				                        <tr style="border:1px solid">
				                            <td style="border:1px solid">
				                                <label for="select_verification_tugas">Kesesuaian Terhadap Tugas Pokok Fungsi</label>
				                            </td>
				                            <td>
				                            	<label class='form-control input-sm'>
				                            		<?= ($revise_verification['tugas']['is_appropriate']) ? 'YES':'NO' ?>
				                            	</label>
				                            </td>
				                            <td style="border:1px solid">
				                            	<label>
				                            		<?= $revise_verification['tugas']['text']  ?>
				                            	</label>
				                            </td>
				                        </tr>
				                        <tr style="border:1px solid">
				                            <td style="border:1px solid">
				                                <label for="input_verification_penulisan">Keselarasan Penulisan Pada Dokumen</label>
				                            </td>
				                            <td>
				                                <label class='form-control input-sm'>
				                            		<?= ($revise_verification['penulisan']['is_appropriate']) ? 'YES':'NO' ?>
				                            	</label>
				                            </td>
				                            <td style="border:1px solid">
				                            	<label>
				                            		<?= $revise_verification['penulisan']['text']  ?>
				                            	</label>
				                            </td>
				                        </tr>
				                        <tr>
				                            <td style="border:1px solid">
				                                <label for="input_verification_perubahan">Rincian Perubahan Dokumen</label>
				                            </td>
				                            <td colspan="2">
				                                <table id="table_perubahan" class="" cellspacing="0" width="100%" border="1">
				                                    <thead>
				                                        <tr>
				                                            <th>BAB</th>
				                                            <th>KONDISI LAMA</th>
				                                            <th>KONDISI BARU</th>
				                                        </tr>
				                                    </thead>
				                                    <tbody>
				                                        <?php 
				                                        $x=0;
				                                        foreach($revise_verification['perubahan']['text'] as $value) {
				                                        ?>
				                                            <tr>
				                                                <td>
				                                                	<label>
									                            		<?= $value['bab']  ?>
									                            	</label>
				                                                </td>
				                                                <td>
				                                                	<label>
									                            		<?= $value['old']  ?>
									                            	</label>
				                                                </td>
				                                                <td>
				                                                	<label>
									                            		<?= $value['new']  ?>
									                            	</label>
				                                                </td>
				                                            </tr>
				                                        <?php 
				                                            $x++;
				                                        } 
				                                        ?>
				                                    </tbody>

				                                </table>
				                            </td>
				                        </tr>
				                    </table>
				                </div>
			                <?php } ?>
		                <?php } ?>
		                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
		                    <b>
		                    	<h4 class="text-center">
									<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
								</h4>
							</b>
		                </div>
						<div class="col-md-12" style="height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;">
				        	<?php
								if ($data['is_upload']=='0') {
							?>
					    		<form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
									<textarea id="document_edit_textarea" style="margin-right:10px;height:400px"></textarea>
									<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
								</form>

				    		<?php } else { ?>
				    			<?php if ($data['ext']=='pdf') {  ?>
				    				<iframe name="viewer" id="viewer" id="viewer" src ="<?= site_url('web/document/view/');?>/<?= $data['document_name_encrypted'] ?>" width='100%' height='100%' allowfullscreen webkitallowfullscreen ></iframe>
					    		<?php } else { ?>
					    			<div id="viewDocContent" class="overlay-content" style="left:0%;width: 800px;height:600px; margin:0px;  position: inline-block;">
						    		    <iframe name="viewer" id="viewer" src="https://view.officeapps.live.com/op/view.aspx?src=<?= site_url("/web/thumb/files");?>/<?= $data['document_name_encrypted'] ?>&embedded=true" frameborder="0" scrolling="no" seamless style="min-width:750px;min-height:600px;max-width:750px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
						    			<!-- <div style="width: 120px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div> -->
						    			<div style='width: 90%;height: 6%;position: absolute;right: 7%;top: 9%;background-color: white;'>&nbsp;<i style='font-size:10px;color:red'>*NB : do not fill the header and footer</i></div>
						    		</div>
					    		<?php } ?>
							<?php } ?>
			               	<br>
				        </div>
			           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
			           		<div style="margin-bottom: 10px">
				           		<button id="btn_compare" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:compare_document('<?= site_url('web/document/view_watermark/').'/'.$prev_document ?>')" data-toggle="tooltip" title="Compare Document with Previous Version" style="margin-left:0.5%;">
								    <i class="fa fa-clone" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Compare</span>
								</button>
								<? if ($data['is_upload']=='1') { ?>
									<button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_document()" data-toggle="tooltip" title="Update Document" style="margin-left:0.5%;">
									    <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
									    <span>Update Document</span>
									</button>
								<?php } ?>
							</div>
							<?php 
							// if (SessionManagerWeb::getUserID()==$data['creator_id'] || SessionManagerWeb::isAdministrator()) {
							if (SessionManagerWeb::getUserID()==$data['creator_id']) { 
							?>
	    		                <!-- <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#delegate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
								    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Delegate</span>
								</button> -->
								<button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#subordinate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
								    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Delegate</span>
								</button>
					        <? } ?>
							<button id="btn_unduh" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Download Document" style="margin-left:0.5%;">
							    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
							    <span>Download</span>
							</button>
							<?php if ($type['need_reviewer']=='0' or ($type['need_reviewer']=='1' and $data['reviewer']!=NULL)){ ?>
							<button id="btn_submit" class="btn btn-sm btn-success btn-rad pull-right" onClick="submit_document()" data-toggle="tooltip" title="Submit" style="margin-left:0.5%;">
							    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Submit</span>
							</button>
							<?php } ?>
							<?php// include('_document_js.php') ?>
							<button id="btn_set_attribute" class="btn btn-sm btn-secondary btn-rad pull-right" onClick="set_attribute_document(false)" data-toggle="tooltip" title="Set Attribute" style="margin-left:0.5%;">
							    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Set Attribute</span>
							</button>
							<button id="btn_cancel" class="btn btn-sm btn-danger btn-rad pull-right" onClick="javascript:cancel_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
								    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Cancel Revision</span>
								</button>
			            </div>
		            <?php } else { ?>
		            	<div class="row col-md-12">
		                    <div class="alert alert-warning" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #FFB300 ;">
		                        <label style="color: #fff;">You have to upload document to revise. You can edit the document by pressing <b>"Download"</b> button then press <b>"Upload Document"</b> to upload document</label>
		                    </div>
		                </div>
			           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
			           		<center>
			           			<button id="btn_cancel" class="btn btn-sm btn-danger btn-rad" onClick="javascript:cancel_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
								    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Cancel Revision</span>
								</button>
								<button id="btn_unduh" class="btn btn-sm btn-secondary btn-rad" onClick="Download(<?= $data['parent'] ?>)" data-toggle="tooltip" title="Download Document" style="margin-left:0.5%;">
								    <i class="fa fa-download" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Download Previous Version</span>
								</button>
								<button id="btn_upload" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:upload_document()" data-toggle="tooltip" title="Upload Document" style="margin-left:0.5%;">
								    <i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
								    <span>Upload Document</span>
								</button>
								<?php if ($data['is_upload']==2) {?>
									<button id="btn_create_here" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:revise_create_here(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Create Document Document" style="margin-left:0.5%;">
									    <i class="fa fa-file" aria-hidden="true" style="margin-right: 10px;"></i>
									    <span>Create Here</span>
									</button>
								<?php } ?>
							</center>
							<?php// include('_document_js.php') ?>
			            </div>
		            <?php } ?>
			    </div>
			</div>
		<?php 
		break; ?>
	<?php } ?>
	<?php } else if ($task['form']=='T') { 
		switch($data['workflow_id']){
			case '1':
	?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px;">

	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							} else {
								echo ' ('.$task['actor_name'].' ) ';
							}?>
						</b>
	                </div>
	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
	                <div class="row col-md-12">
	                    <div class="alert alert-info" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #03A9F4;">
	                        <label style="color: #fff;">You only can change the reviewer, Requirement, Clausuls on this document. Please check the needed reviewers for this documents and add if needed.</label>
	                    </div>
	                </div>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
		           		<!-- <form id="form_create_document2" class="<?= $hide_editor ?>" style="margin-bottom: 20px">
							<textarea id="document_edit_textarea2" style="margin-right:10px;height:400px"></textarea>
							<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
						</form> -->
		           		<!-- Detail Dokumen -->
		           		<form action="<?= site_url('web/document/update').'/'.$data['document_id'] ?>" method="post" id="form_review_atribut">

							<!-- Reviewer -->
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_reviewer"><b>Reviewer</b></label>
								<?php echo form_dropdown('input_reviewer[]', $variables['reviewer'], $data['reviewer'], 'id="input_reviewer" class="form-control input-sm select2 input_reviewer" style="width:100%;margin-bottom: 5px" multiple'); ?>
		                   </div>

		                   <!-- Already Review  -->
		                   <div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_reviewer"><b>Reviewer (already review)</b></label>
								<?php echo form_dropdown('input_reviewer_reviewed[]', $variables['reviewed'], $data['reviewed'], 'id="input_reviewer_reviewed" class="form-control input-sm select2 input_reviewer_reviewed" style="width:100%;margin-bottom: 5px" multiple disabled'); ?>
		                   </div>

		                   <!-- Persyaratan Aplikasi -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_iso"><b>Requirement</b></label>
								<?php echo form_dropdown('input_iso[]', $variables['document_iso'], $data['iso'], 'id="input_iso" class="form-control input-sm select2" style="width:80%;margin-bottom: 5px" multiple'); ?>
							</div>

							<!-- klausul -->
							<?php if ($type['klausul']=='1') { ?>
								<br>
								<div style="margin-bottom: 5px">
									<label class="col-md-2 control-label" for="input_iso"><b>Clausul</b></label>
									<?php echo form_dropdown('input_iso_klausul[]', $variables['klausul'], $data['klausul'], 'id="input_iso_klausul" class="form-control input-sm select2" style="width:80%;margin-bottom: 5px" multiple'); ?>
								</div>
							<?php } ?>

		           			<!-- Judul -->
		           			<br>
							<label class="col-md-2 control-label" for="input_judul">Title</label>
							<input type="text" id="input_judul" name="input_judul" class="form-control" value="<?= $data['title'] ?>" style="width:60%" readonly>

		           			<!-- Kode Dokumen -->
		           			<br>
							<label class="col-md-2 control-label" for="input_kode">Document Code</label>
							<input type="text" id="input_kode" name="input_kode" class="form-control" value="<?= $data['code'] ?>" style="width:60%" readonly>

							<!-- Jenis Dokumen -->
							<br>
							<label class="col-md-2 control-label" for="input_jenisdokumen">Document type</label>
							<input type="text" id="input_jenisdokumen" name="input_jenisdokumen" readonly class="form-control" value="<?= $data['type_name'] ?>" style="width:60%;">

							<!-- Unit Kerja -->
							<br>
							<label class="col-md-2 control-label" for="input_unitkerja">Work Unit</label>
							<input type="text" id="input_unitkerja" name="input_unitkerja" readonly class="form-control" value="<?= $data['unitkerja_name'] ?>" style="width:60%;">

							<!-- Proses Bisnis -->
							<br>
							<label class="col-md-2 control-label" for="input_prosesbisnis">Business Process</label>
							<input type="text" id="input_prosesbisnis" name="input_prosesbisnis" readonly class="form-control" value="<?= $data['prosesbisnis_name'] ?>" style="width:60%;">

							<!-- Proses Bisnis -->
							<?php if  ($type['retension']=='1') { ?>
								<br>
								<label class="col-md-2 control-label" for="retension_input">Record Retension Period</label>
								<?php echo form_input(array('name' => 'retension_input', 'value' => $data['retension'], 'class' => 'form-control input-sm', 'id' => 'retension_input', 'placeholder' => 'Masa Retensi', 'style' => 'width:40%;display:inline' ))?>
		                        <select id="retension_select" name="retension_select" class="form-control input-sm" style="width:20%; font-size: 12px; letter-spacing: 0.5px;display:inline">
		                            <option value="0">Bulan</option>
		                            <option value="1">Tahun</option>
		                       </select>
		                       <br>
	                       <?php } ?>
	                       

							<!-- Deskripsi -->
							<br>
							<label class="col-md-2 control-label" for="input_deskripsi">Change Log</label>
							<textarea type="text" id="input_deskripsi" name="input_deskripsi" rows='5' class="form-control" value="" style="width:60%" readonly><?= $data['description'] ?></textarea>

							<!-- Creator -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_creator">Creator</label>
								<?php// echo form_dropdown('input_creator', $creator, $data['creator_id'], 'id="input_creator" class="form-control input-sm select2" style="width:40%;margin-bottom: 5px"'); ?>
								<?php echo form_input(array('name' => 'creator_id', 'value' =>  $data['creator_id'], 'class' => 'form-control input-sm hidden', 'id' => 'creator_id', 'placeholder' => 'Nama Creator ', 'style' => 'width:60%;margin-bottom: 5px' , 'readonly'=>TRUE))?>
	                            <?php echo form_input(array('name' => 'creator_name', 'value' =>$data['creator_name'], 'class' => 'form-control input-sm', 'id' => 'creator_name', 'placeholder' => 'Nama Creator ', 'style' => 'width:60%;margin-bottom: 5px' , 'readonly'=>TRUE))?>
							</div>

							<!-- Approver -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_approver">Approver</label>
								<?php// echo form_dropdown('input_approver', $approver, $data['approver_id'], 'id="input_approver" class="form-control input-sm select2" style="width:40%;margin-bottom: 5px"'); ?>
								<?php echo form_input(array('name' => 'approver_id', 'value' => $data['approver_id'], 'class' => 'form-control input-sm hidden', 'id' => 'approver_id', 'placeholder' => 'Nama Approver', 'style' => 'width:60%;margin-bottom: 5px' , 'readonly'=>TRUE))?>
	                            <?php echo form_input(array('name' => 'approver_name', 'value' => $data['approver_name'], 'class' => 'form-control input-sm', 'id' => 'approver_name', 'placeholder' => 'Nama Approver', 'style' => 'width:60%;margin-bottom: 5px' , 'readonly'=>TRUE))?>
							</div>

							<!-- Unit Kerja Terkait -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_unitkerjaterkait">Related Work Unit</label>
								<?php echo form_dropdown('input_unitkerjaterkait[]', $variables['unitkerja'], $data['unitkerja'], 'id="input_unitkerjaterkait" class="form-control input-sm select2" style="width:60%;margin-bottom: 5px" multiple disabled'); ?>
		                   	</div>

							<!-- Dokumen Terkait -->
							<br>
							<label class="col-md-2 control-label" for="input_dokumenterkait">Related Document</label>
							<?php foreach ($data['docRelated'] as $key => $value) { ?>
								<input type="text" id="input_dokumenterkait" name="input_dokumenterkait[]" disabled value="<?= $value['TITLE'] ?>" class="form-control" style="width:60%;margin-bottom:5px">
							<?php }
								if (count($data['docRelated'])<1){ ?>
									<input type="text" disabled value="-" class="form-control" style="width:60%;margin-bottom:5px">
							<?php	}
							?>

							<div class="form-group hidden" style="margin-bottom: 15px;">
		                        <div id="listDokumenTerkait" style="width: 60%;">
		                            <button type="button" class="btn tombolBarui" onClick="openModalRelatedForUpload()" style="padding-left:14px;padding-right:13px;font-weight: 1px; margin-bottom:8px;font-size: 12px; letter-spacing: 1px; margin-top: 0px;">Add Related Document</button>
		                            <input id="related" name="related[]" type="hidden" value="624" class="pilihRelated">
		                            <label class="form-control" style="border-style: ridge; font-size: 12px; margin-bottom: 8px; padding-left: 3px;">tes related document</label>
		                        </div>
		                    </div>
						</form>
			            <br>
			            <div class="col-md-12">
							<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad <?= $hide_btn_lihat_dokumen ?>" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
							    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>View Document</span>
							</button>
							<?php if ($is_chief) { ?>
				                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#delegate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
								    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Delegate</span>
								</button>
					        <? } ?> 
					        <?php if ($document_controller['allow_next']) {?>
						        <button id="btn_continue" type="button" class="btn btn-sm btn-success btn-rad pull-right" onClick="javascript:continueToNext(<?= $data['document_id'].','.$urutan.','.$approved_status ?>)" data-toggle="tooltip" title="Continue to Creator" style="margin-left:0.5%;">
								    <i class="fa fa-arrow-right" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Continue to Creator</span>
								</button>
							<?php } ?>
							
							<button id="btn_simpan" type="submit" class="btn btn-sm btn-success btn-rad pull-right tombolBarui22" form="form_review_atribut" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Save</span>
							</button>
							
							<!-- <button id="btn_delegate" class="btn btn-sm btn-success btn-rad hidden pull-right" onClick="javascript:delegate(<?= $data['document_id'].','.$data['workflow_urutan'].','.$data['workflow_id'] ?>)" data-toggle="tooltip" title="Delegasikan" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Delegasikan</span>
							</button> -->
						</div>
		            </div>
			    </div>
			</div>
		<?php
			break;
			case '2':
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px;">

	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							} else {
								echo ' ('.$task['actor_name'].' ) ';
							}?>
						</b>
	                </div>
	                 <?php if ($type['need_reviewer']=='1') { ?>
		                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
		                	<?php 
		                	foreach ($data['review'] as $k_review => $v_review) { ?>
		                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
		                	<?php } ?>
							
		                </div>
	                <?php } ?>
	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
	                <div class="row col-md-12">
	                    <div class="alert alert-info" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #03A9F4;">
	                        <label style="color: #fff;">You only can change the reviewer on this document. Please check the needed reviewers for this documents and add if needed.</label>
	                    </div>
	                </div>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
		           		<!-- <form id="form_create_document2" class="<?= $hide_editor ?>" style="margin-bottom: 20px">
							<textarea id="document_edit_textarea2" style="margin-right:10px;height:400px"></textarea>
							<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
						</form> -->
		           		<!-- Detail Dokumen -->
		           		<form action="<?= site_url('web/document/update').'/'.$data['document_id'] ?>" method="post" id="form_review_atribut">

							<!-- Reviewer -->
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_reviewer"><b>Reviewer</b></label>
								<?php echo form_dropdown('input_reviewer[]', $variables['reviewer'], $data['reviewer'], 'id="input_reviewer" class="form-control input-sm select2 input_reviewer" style="width:100%;margin-bottom: 5px" multiple'); ?>
		                   </div>	

		                   <!-- Already Review  -->
		                   <div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_reviewer"><b>Reviewer (already review)</b></label>
								<?php echo form_dropdown('input_reviewer_reviewed[]', $variables['reviewed'], $data['reviewed'], 'id="input_reviewer_reviewed" class="form-control input-sm select2 input_reviewer_reviewed" style="width:100%;margin-bottom: 5px" multiple disabled'); ?>
		                   </div>                       
						</form>
			            <br>
			            <div class="col-md-12">
							<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad " onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
							    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>View Document</span>
							</button>
							<?php if ($is_chief) { ?>
				                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#delegate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
								    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Delegate</span>
								</button>
					        <? } ?> 
					        <?php if ($document_controller['allow_next']) {?>
						        <button id="btn_continue" type="button" class="btn btn-sm btn-success btn-rad pull-right" onClick="javascript:continueToNext(<?= $data['document_id'].','.$urutan.','.$approved_status ?>)" data-toggle="tooltip" title="Continue to Creator" style="margin-left:0.5%;">
								    <i class="fa fa-arrow-right" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Continue to Creator</span>
								</button>
							<?php } ?>
							<button id="btn_simpan" type="submit" class="btn btn-sm btn-success btn-rad pull-right tombolBarui22" form="form_review_atribut" data-toggle="tooltip" title="Simpan" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Save</span>
							</button>
						</div>
		            </div>
			    </div>
			</div>	
		<?php 
			break;
			case '3':
		?>
			<div class="row" style="margin-bottom: 3%;">
		        <div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px;">

	                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
	                    <b>
	                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
							<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
								echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
							} else {
								echo ' ('.$task['actor_name'].' ) ';
							}?>
						</b>
	                </div>
	                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
	                    <b>
	                    	<h4 class="text-center">
								<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
							</h4>
						</b>
	                </div>
	                <div class="row col-md-12">
	                    <div class="alert alert-info" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #03A9F4;">
	                        <label style="color: #fff;">You only can change the reviewer, Clausuls and Requirement on this document. Please check the needed reviewers for this documents and add if needed.</label>
	                    </div>
	                </div>
		           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
		           		<!-- <form id="form_create_document2" class="<?= $hide_editor ?>" style="margin-bottom: 20px">
							<textarea id="document_edit_textarea2" style="margin-right:10px;height:400px"></textarea>
							<button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
						</form> -->
		           		<!-- Detail Dokumen -->
		           		<form action="<?= site_url('web/document/update').'/'.$data['document_id'] ?>" method="post" id="form_review_atribut">

							<!-- Reviewer -->
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_reviewer"><b>Reviewer</b></label>
								<?php echo form_dropdown('input_reviewer[]', $variables['reviewer'], $data['reviewer'], 'id="input_reviewer" class="form-control input-sm select2 input_reviewer" style="width:100%;margin-bottom: 5px" multiple'); ?>
		                   </div>

		                   <!-- Already Review  -->
		                   <div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_reviewer"><b>Reviewer (already review)</b></label>
								<?php echo form_dropdown('input_reviewer_reviewed[]', $variables['reviewed'], $data['reviewed'], 'id="input_reviewer_reviewed" class="form-control input-sm select2 input_reviewer_reviewed" style="width:100%;margin-bottom: 5px" multiple disabled'); ?>
		                   </div>

		                   <!-- Persyaratan Aplikasi -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_iso"><b>Requirement</b></label>
								<?php echo form_dropdown('input_iso[]', $variables['document_iso'], $data['iso'], 'id="input_iso" class="form-control input-sm select2" style="width:40%;margin-bottom: 5px" multiple'); ?>
							</div>

							<!-- klausul -->
							<?php if ($type['klausul']=='1') { ?>
								<br>
								<div style="margin-bottom: 5px">
									<label class="col-md-2 control-label" for="input_iso"><b>Clausul</b></label>
									<?php echo form_dropdown('input_iso_klausul[]', $variables['klausul'], $data['klausul'], 'id="input_iso_klausul" class="form-control input-sm select2" style="width:40%;margin-bottom: 5px" multiple'); ?>
								</div>
							<?php } ?>

		           			<!-- Judul -->
		           			<br>
							<label class="col-md-2 control-label" for="input_judul">Title</label>
							<input type="text" id="input_judul" name="input_judul" class="form-control" value="<?= $data['title'] ?>" style="width:60%" readonly>

		           			<!-- Kode Dokumen -->
		           			<br>
							<label class="col-md-2 control-label" for="input_kode">Document Code</label>
							<input type="text" id="input_kode" name="input_kode" class="form-control" value="<?= $data['code'] ?>" style="width:60%" readonly>

							<!-- Jenis Dokumen -->
							<br>
							<label class="col-md-2 control-label" for="input_jenisdokumen">Document type</label>
							<input type="text" id="input_jenisdokumen" name="input_jenisdokumen" readonly class="form-control" value="<?= $data['type_name'] ?>" style="width:60%;">

							<!-- Unit Kerja -->
							<br>
							<label class="col-md-2 control-label" for="input_unitkerja">Work Unit</label>
							<input type="text" id="input_unitkerja" name="input_unitkerja" readonly class="form-control" value="<?= $data['unitkerja_name'] ?>" style="width:60%;">

							<!-- Proses Bisnis -->
							<br>
							<label class="col-md-2 control-label" for="input_prosesbisnis">Business Process</label>
							<input type="text" id="input_prosesbisnis" name="input_prosesbisnis" readonly class="form-control" value="<?= $data['prosesbisnis_name'] ?>" style="width:60%;">

							<!-- Proses Bisnis -->
							<?php if  ($type['retension']=='1') { ?>
								<br>
								<label class="col-md-2 control-label" for="retension_input">Record Retension Period</label>
								<?php echo form_input(array('name' => 'retension_input', 'value' => $data['retension'], 'class' => 'form-control input-sm', 'id' => 'retension_input', 'placeholder' => 'Masa Retensi', 'style' => 'width:40%;display:inline' ))?>
		                        <select id="retension_select" name="retension_select" class="form-control input-sm" style="width:20%; font-size: 12px; letter-spacing: 0.5px;display:inline">
		                            <option value="0">Bulan</option>
		                            <option value="1">Tahun</option>
		                       </select>
		                       <br>
	                       <?php } ?>
	                       

							<!-- Deskripsi -->
							<br>
							<label class="col-md-2 control-label" for="input_deskripsi">Change Log</label>
							<textarea type="text" id="input_deskripsi" name="input_deskripsi" rows='5' class="form-control" value="" style="width:60%" readonly><?= $data['description'] ?></textarea>

							<!-- Creator -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_creator">Creator</label>
								<?php// echo form_dropdown('input_creator', $creator, $data['creator_id'], 'id="input_creator" class="form-control input-sm select2" style="width:40%;margin-bottom: 5px"'); ?>
								<?php echo form_input(array('name' => 'creator_id', 'value' =>  $data['creator_id'], 'class' => 'form-control input-sm hidden', 'id' => 'creator_id', 'placeholder' => 'Nama Creator ', 'style' => 'width:60%;margin-bottom: 5px' , 'readonly'=>TRUE))?>
	                            <?php echo form_input(array('name' => 'creator_name', 'value' =>$data['creator_name'], 'class' => 'form-control input-sm', 'id' => 'creator_name', 'placeholder' => 'Nama Creator ', 'style' => 'width:60%;margin-bottom: 5px' , 'readonly'=>TRUE))?>
							</div>

							<!-- Approver -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_approver">Approver</label>
								<?php// echo form_dropdown('input_approver', $approver, $data['approver_id'], 'id="input_approver" class="form-control input-sm select2" style="width:40%;margin-bottom: 5px"'); ?>
								<?php echo form_input(array('name' => 'approver_id', 'value' => $data['approver_id'], 'class' => 'form-control input-sm hidden', 'id' => 'approver_id', 'placeholder' => 'Nama Approver', 'style' => 'width:60%;margin-bottom: 5px' , 'readonly'=>TRUE))?>
	                            <?php 
	                            echo form_dropdown('approver_name',null, null, 'id="approver_name" class="form-control input-sm select2" style="width:60%;margin-bottom: 5px"');
	                            // echo form_dropdown('approver_name', $approvers, $data['approvers'], 'class="form-control input-sm select2" style="width:60%;margin-bottom: 5px" multiple'); 
	                            ?>
							</div>

							<!-- Unit Kerja Terkait -->
							<br>
							<div style="margin-bottom: 5px">
								<label class="col-md-2 control-label" for="input_unitkerjaterkait">Related Work Unit</label>
								<?php echo form_dropdown('input_unitkerjaterkait[]', $variables['unitkerja'], $data['unitkerja'], 'id="input_unitkerjaterkait" class="form-control input-sm select2" style="width:60%;margin-bottom: 5px" multiple disabled'); ?>
		                   	</div>

							<!-- Dokumen Terkait -->
							<br>
							<label class="col-md-2 control-label" for="input_dokumenterkait">Related Document</label>
							<?php foreach ($data['docRelated'] as $key => $value) { ?>
								<input type="text" id="input_dokumenterkait" name="input_dokumenterkait[]" disabled value="<?= $value['TITLE'] ?>" class="form-control" style="width:60%;margin-bottom:5px">
							<?php }
								if (count($data['docRelated'])<1){ ?>
									<input type="text" disabled value="-" class="form-control" style="width:60%;margin-bottom:5px">
							<?php	}
							?>

							<div class="form-group hidden" style="margin-bottom: 15px;">
		                        <div id="listDokumenTerkait" style="width: 60%;">
		                            <button type="button" class="btn tombolBarui" onClick="openModalRelatedForUpload()" style="padding-left:14px;padding-right:13px;font-weight: 1px; margin-bottom:8px;font-size: 12px; letter-spacing: 1px; margin-top: 0px;">Add Related Document</button>
		                            <input id="related" name="related[]" type="hidden" value="624" class="pilihRelated">
		                            <label class="form-control" style="border-style: ridge; font-size: 12px; margin-bottom: 8px; padding-left: 3px;">tes related document</label>
		                        </div>
		                    </div>
						</form>
			            <br>
			            <div class="col-md-12">
							<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad <?= $hide_btn_lihat_dokumen ?>" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
							    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>View Document</span>
							</button>
							<?php if ($is_chief) { ?>
				                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#delegate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
								    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Delegate</span>
								</button>
					        <? } ?> 
					        <?php if ($document_controller['allow_next']) {?>
						        <button id="btn_continue" type="button" class="btn btn-sm btn-success btn-rad pull-right" onClick="javascript:continueToNext(<?= $data['document_id'].','.$urutan.','.$approved_status ?>)" data-toggle="tooltip" title="Continue to Creator" style="margin-left:0.5%;">
								    <i class="fa fa-arrow-right" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
								    <span>Continue to Creator</span>
								</button>
							<?php } ?>
							<button id="btn_simpan" type="submit" class="btn btn-sm btn-success btn-rad pull-right tombolBarui22" form="form_review_atribut" data-toggle="tooltip" title="Save" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Save</span>
							</button>
							<!-- <button id="btn_delegate" class="btn btn-sm btn-success btn-rad hidden pull-right" onClick="javascript:delegate(<?= $data['document_id'].','.$data['workflow_urutan'].','.$data['workflow_id'] ?>)" data-toggle="tooltip" title="Delegasikan" style="margin-left:0.5%;">
							    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span>Delegasikan</span>
							</button> -->
						</div>
		            </div>
			    </div>
			</div>
		<?php break;?>
		<?php } ?>
	<?php } elseif ($task['form']=='E' && $task['user'] == Role::DRAFTER_EVALUATION){?>
		<div class="row" style="margin-bottom: 3%;">
	        <div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px;">

                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
                    <b>
                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
						<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
							echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
						} else {
							echo ' ('.$task['actor_name'].' ) ';
						}?>
					</b>
                </div>
                <?php if ($data['status']=='B') { ?>
	                <div class="row col-md-12">
	                    <div class="alert alert-warning" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #FFB300 ;">
	                        <label style="color: #fff;">To Submit the form, you need :<br>- Click <b>"Save"</b> button to Save/Revise the evaluation form  <?= ($type['need_reviewer']=='1') ? '<br>- Click <b>"Add Reviewer"</b> button to add the Reviewers' :'' ?></label>
	                    </div>
	                </div>
                <?php } ?>
                <?php if ($data['status']=='R') { ?>
                	<div class="col col-xs-12" style="margin-bottom:10px;margin-top:10px;padding-bottom:5px;border-bottom: 1px solid;">
                		<b>- <?= $revise_requestor['name'] ?></b> ask you to revise on Evaluation Form for Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because <b style="color:red"><?= $revise_requestor['reason'] ?></b>.<br>
						
	                </div>
                <?php } ?>
                <?php if ($type['need_reviewer']=='1' and 1==0) { ?>
	                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
	                	<?php 
	                	foreach ($data['review'] as $k_review => $v_review) { ?>
	                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
	                	<?php } ?>
						
	                </div>
                <?php } ?>
                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
                    <b>
                    	<h4 class="text-center">
							<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
						</h4>
					</b>
                </div>
	           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
	           		<form action="<?= site_url('web/document/setDocumentEvaluation').'/'.$data['document_id'] ?>" method="post" id="form_evaluation">
	           			<table id="evaluation_table" style="width:100%" toolbar="#toolbar" >
	                        <thead>
	                            <tr style="border:1px solid">
	                            	<th field="form_no" width="5%" style="text-align: center;border:1px solid" >No. </th>
	                                <th field="verification_name" width="80%" style="text-align: center;border:1px solid" >Document Evaluation</th>
	                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Y/N? </th>
	                            </tr>
	                        </thead>
	                        <?php 
	                        $evaluation_no = 0;
	                        $hide_tr = '';
	                        foreach ($evaluation_forms as $k_form => $v_form) { 
	                        	$evaluation_no++;
	                        	if ($evaluation_no>1 and $evaluation['text'][1]==0){
	                        		$hide_tr = 'hidden';
	                        	}
	                        	?>
	                        	<tr class="<?= $hide_tr ?>" style="border:1px solid;" id="evaluation_tr_<?= $v_form['id'] ?>" onchange="checkEvaluationForm()">
	                        		<td style="border:1px solid">
		                                <label><?= $evaluation_no ?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <label for="input_evaluation_<?= $v_form['id'] ?>"><?= $v_form['text']?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <?php 
		                                	$form_id = $v_form['id'];
		                                	$choice = array(0=>"NO", 1=>"YES");
		                                	if ($must_revise and $evaluation_no==9){
		                                		$choice=array(0=>"NO");
		                                	}
		                                	$need_revise = '';
		                                	if ($v_form['need_revise']){
		                                		$need_revise = 'readonly';
		                                	}
		                                    echo form_dropdown("select_evaluation_".$v_form['id'], $choice, $evaluation['text'][$evaluation_no], ' class="form-control input-sm" '.$need_revise.' style="margin-bottom: 5px" id="select_evaluation_'.$form_id.'"'); ?>
		                            </td>
		                        </tr>
	                        <?php } ?>
	                    </table>      
	           			<table id="evaluation_comment" style="width:100%;margin-top: 10px" toolbar="#toolbar" >
                        	<tr style="border:1px solid">
	                            <td style="border:1px solid;width:10%">
	                                <label for="input_evaluation_comment">Comment</label>
	                            </td>
	                            <td>
	                                <textarea id="input_evaluation_comment" name="input_evaluation_comment" value="<?= $evaluation['comment']  ?>"  style="padding:5px;width:100%;max-width: 100%"><?= $evaluation['comment']  ?></textarea>
	                            </td>
	                        </tr> 
	                    </table>
	                    <input type='submit' id='btn_submit_evaluation_form' class="hidden">
	                </form>
		            <br>
		            <div class="col-md-12">
						<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
						    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>View Document</span>
						</button>
						<?php 
						// if (SessionManagerWeb::getUserID()==$data['creator_id'] || SessionManagerWeb::isAdministrator()){ 
						if (SessionManagerWeb::getUserID()==$data['creator_id']){ 
						?>
		                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#subordinate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
						    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Delegate</span>
						</button>
						<?php } ?>
						<?php 
						$can_submit = false;
						if ($type['need_reviewer']=='0'){
							$can_submit = true;
						} else {
							if ($data['reviewer']!=NULL){
								$can_submit = true;
							}
						}
						if ($evaluation and $can_submit) {?>
						<button id="btn_submit" class="btn btn-sm btn-success btn-rad pull-right" onclick="evaluate_document()" data-toggle="tooltip" title="Submit" style="margin-left:0.5%;">
						    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Submit</span>
						</button>
						<?php } ?>
						<button id="btn_save_evaluation" class="btn btn-sm btn-secondary btn-rad pull-right" onclick="save_evaluation_form()" style="margin-left:0.5%;">
						    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Save</span>
						</button>
						<button id="btn_cancel" class="btn btn-sm btn-danger btn-rad pull-right" onClick="javascript:cancel_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
						    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Cancel</span>
						</button>
						<?php if ($type['need_reviewer']=='1') { ?>
							<button id="btn_set_reviewer" class="btn btn-sm btn-secondary btn-rad pull-right" onClick="javascript:add_reviewer_document(0)" data-toggle="tooltip" title="Set Reviewer" style="margin-left:0.5%;">
							    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span> Reviewer</span>
							</button>
						<?php } ?>
					</div>
	            </div>
		    </div>
		</div>
	<?php } 
		elseif($task['form']=='E' && $task['user'] == Role::CREATOR){
		?>
		<div class="row" style="margin-bottom: 3%;">
	        <div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px;">

                <div class="col col-xs-9" style="margin: 2% 0%;padding: 0%;">
                    <b>
                    	<span class="label label-primary"><?= $task_urutan ?></span> <?= $task['name'] ?> by <?= Role::name($task['user']) ?>
						<?php if ($task['comment'][0]['delegated_user_id']!=NULL) {
							echo ' ( didelegasikan ke '.$task['comment'][0]['delegated_user_name'].' ) ';
						} else {
							echo ' ('.$task['actor_name'].' ) ';
						}?>
					</b>
                </div>
                <?php if ($data['status']=='B') { ?>
	                <div class="row col-md-12">
	                    <div class="alert alert-warning" style="width:100%;margin-bottom: 10px;margin-top: 15px;background-color: #FFB300 ;">
	                        <label style="color: #fff;">To Submit the form, you need :<br>- Click <b>"Save"</b> button to Save/Revise the evaluation form  <?= ($type['need_reviewer']=='1') ? '<br>- Click <b>"Add Reviewer"</b> button to add the Reviewers' :'' ?></label>
	                    </div>
	                </div>
                <?php } ?>
                <?php if ($data['status']=='R') { ?>
                	<div class="col col-xs-12" style="margin-bottom:10px;margin-top:10px;padding-bottom:5px;border-bottom: 1px solid;">
                		<b>- <?= $revise_requestor['name'] ?></b> ask you to revise on Evaluation Form for Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because <b style="color:red"><?= $revise_requestor['reason'] ?></b>.<br>
						
	                </div>
                <?php } ?>
                <?php if ($type['need_reviewer']=='1' and 1==0) { ?>
	                <div class="col col-xs-12" style="margin-bottom:10px;padding-bottom:5px;border-bottom: 1px solid">
	                	<?php 
	                	foreach ($data['review'] as $k_review => $v_review) { ?>
	                		<b>- <?= $v_review['reviewer_name'] ?></b> has been <?= ($v_review['is_agree']==1) ? "<b style='color:green'>Agree</b>" : "<b style='color:red'>Disagree</b>" ?> on Document <span style="color: #212121;font-weight: 900;"><?= $data['type_name'] ?>  "<?= $data['title']?>"</span> because '<?= $v_review['review'] ?>'.<br>
	                	<?php } ?>
						
	                </div>
                <?php } ?>
                <div class="col col-xs-12" style="margin: 0%;padding: 0%;">
                    <b>
                    	<h4 class="text-center">
							<?= $data['type_name'].' <b>"'.$data['title'].'"</b>' ?>
						</h4>
					</b>
                </div>
	           	<div class="col col-xs-12" style="margin: 2% 0%;padding: 0%;">
	           		<form action="<?= site_url('web/document/setDocumentEvaluation').'/'.$data['document_id'] ?>" method="post" id="form_evaluation">
	           			<table id="evaluation_table" style="width:100%" toolbar="#toolbar" >
	                        <thead>
	                            <tr style="border:1px solid">
	                            	<th field="form_no" width="5%" style="text-align: center;border:1px solid" >No. </th>
	                                <th field="verification_name" width="80%" style="text-align: center;border:1px solid" >Document Evaluation</th>
	                                <th field="is_appropriate" width="10%" style="text-align: center;border:1px solid" >Y/N? </th>
	                            </tr>
	                        </thead>
	                        <?php 
	                        $evaluation_no = 0;
	                        $hide_tr = '';
	                        foreach ($evaluation_forms as $k_form => $v_form) { 
	                        	$evaluation_no++;
	                        	if ($evaluation_no>1 and $evaluation['text'][1]==0){
	                        		$hide_tr = 'hidden';
	                        	}
	                        	?>
	                        	<tr class="<?= $hide_tr ?>" style="border:1px solid;" id="evaluation_tr_<?= $v_form['id'] ?>" onchange="checkEvaluationForm()">
	                        		<td style="border:1px solid">
		                                <label><?= $evaluation_no ?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <label for="input_evaluation_<?= $v_form['id'] ?>"><?= $v_form['text']?></label>
		                            </td>
		                            <td style="border:1px solid">
		                                <?php 
		                                	$form_id = $v_form['id'];
		                                	$choice = array(0=>"NO", 1=>"YES");
		                                	if ($must_revise and $evaluation_no==9){
		                                		$choice=array(0=>"NO");
		                                	}
		                                	$need_revise = '';
		                                	if ($v_form['need_revise']){
		                                		$need_revise = 'readonly';
		                                	}
		                                    echo form_dropdown("select_evaluation_".$v_form['id'], $choice, $evaluation['text'][$evaluation_no], ' class="form-control input-sm" '.$need_revise.' style="margin-bottom: 5px" id="select_evaluation_'.$form_id.'"'); ?>
		                            </td>
		                        </tr>
	                        <?php } ?>
	                    </table>      
	           			<table id="evaluation_comment" style="width:100%;margin-top: 10px" toolbar="#toolbar" >
                        	<tr style="border:1px solid">
	                            <td style="border:1px solid;width:10%">
	                                <label for="input_evaluation_comment">Comment</label>
	                            </td>
	                            <td>
	                                <textarea id="input_evaluation_comment" name="input_evaluation_comment" value="<?= $evaluation['comment']  ?>"  style="padding:5px;width:100%;max-width: 100%"><?= $evaluation['comment']  ?></textarea>
	                            </td>
	                        </tr> 
	                    </table>
	                    <input type='submit' id='btn_submit_evaluation_form' class="hidden">
	                </form>
		            <br>
		            <div class="col-md-12">
						<button id="activeDownload" class="btn btn-sm btn-secondary btn-rad" onClick="viewDocument(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="View Document" style="margin-left:0.5%;">
						    <i class="fa fa-eye" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>View Document</span>
						</button>
						<?php 
						// if (SessionManagerWeb::getUserID()==$data['creator_id'] || SessionManagerWeb::isAdministrator()){ 
						if (SessionManagerWeb::getUserID()==$data['creator_id']){ 
						?>
		                <button id="btn_delegate" class="btn btn-sm btn-secondary btn-rad" data-target="#subordinate_modal" data-toggle="modal" title="Delegasikan" style="margin-left:0.5%;">
						    <i class="fa fa-user" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Delegate</span>
						</button>
						<?php } ?>
						<?php 
						$can_submit = false;
						if ($type['need_reviewer']=='0'){
							$can_submit = true;
						} else {
							if ($data['reviewer']!=NULL){
								$can_submit = true;
							}
						}
						if ($evaluation and $can_submit) {?>
						<button id="btn_submit" class="btn btn-sm btn-success btn-rad pull-right" onclick="evaluate_document()" data-toggle="tooltip" title="Submit" style="margin-left:0.5%;">
						    <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Submit</span>
						</button>
						<?php } ?>
						<button id="btn_save_evaluation" class="btn btn-sm btn-secondary btn-rad pull-right" onclick="save_evaluation_form()" style="margin-left:0.5%;">
						    <i class="fa fa-hdd-o" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Save</span>
						</button>
						<button id="btn_cancel" class="btn btn-sm btn-danger btn-rad pull-right" onClick="javascript:cancel_document()" data-toggle="tooltip" title="Reject" style="margin-left:0.5%;">
						    <i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
						    <span>Cancel</span>
						</button>
						<?php if ($type['need_reviewer']=='1') { ?>
							<button id="btn_set_reviewer" class="btn btn-sm btn-secondary btn-rad pull-right" onClick="javascript:add_reviewer_document(0)" data-toggle="tooltip" title="Set Reviewer" style="margin-left:0.5%;">
							    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
							    <span> Reviewer</span>
							</button>
						<?php } ?>
					</div>
	            </div>
		    </div>
		</div>
		<?php
		}


		else if($task['form']=='P'){ ?>	
		<div class="row" style="margin-bottom: 3%;">
	        <div class="col-md-12 shadowPanel" style="background-color: #fff;border-radius: 5px;">
                <div class="col col-xs-8" style="margin: 2% 0% 0% 0%;padding: 0%;">
					<b><span class="label label-primary" ><?= $task_urutan ?></span> <?= $task['name'] ?> </b>
	            </div>
				<div class="col-md-12" style="height:100%;margin-top:10px;padding-right:0px;padding-left: 0px;float: left;    margin-bottom: 3%;">
					<div class="row" style="margin-left:5px">
						<a id="node_<?= $task_urutan ?>" style="display:block;top:-80px;position:relative"></a>
						<table class="table" >
							<tr>
								<td>
									Document 
									<span style="color: #212121;font-weight: 900;">
										<?= $data['type_name'] ?>  "<?= $data['title']?>"
									</span> has been evaluated and 
									<?php 
										$is_revise = false;
										switch ($evaluation['status']) {
											// Revisi
											case 'R':
												echo " need to be <span style='color: #FFB300;font-weight: 900;'> REVISED </span> because '<b>".$evaluation['comment']."</b>'";
												$is_revise = true;
											break;
											// Cabut
											case 'C':
												echo " will be <span style='color: red;font-weight: 900;'> WITHDRAWN </span> because '<b>".$evaluation['comment']."</b>'";
											break;
											// Relevan
											case 'D':
												echo " still <span style='color: #00B65C;font-weight: 900;'> RELEVANT</span>.";
											break;
										}
									?>
								</td>
							</tr>
							<?php if ($is_revise) { ?>
								<tr>
									<td style="padding: 10px 0px 0px 0px;">
										<button id="btn_evaluation_revise" class="btn btn-sm btn-warning btn-rad" onClick="setRevise(<?= $data['document_id'] ?>)" data-toggle="tooltip" title="Revise Now" style="margin-left:0.5%;">
						    				<i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i>
										    <span>Revise Now</span>
										</button>					
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>
		        </div>
		    </div>
		</div>
	<?php } ?>
	</div>	
	<button class="btn btn-success hidden" id="btn_next_stepper" name="btn_next_stepper" style="float:right;margin-left:10px" onclick="javascript:set_urutan(<?= $data['document_id'].','.$urutan.','.$approved_status ?>)">
		Approve
	</button>
	<button class="btn btn-success hidden" id="btn_save_edited_doc" name="btn_save_edited_doc" style="float:right;margin-left:10px" >
		Convert
	</button>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var creator_id = <?php echo $data['creator_id'] ?>;
		var types_id = <?php echo $data['type_id'] ?>;
		var approver_id = <?php echo $data['approver_id'] ?>;
		$.ajax({
	        url : '<?= site_url('web/unitkerja/ajaxGetApprover') ?>/',
	        type: 'post',
	        cache: false,
	        data: {"creator_id":creator_id, "type_id":types_id},
	        success: function(respon){
	            response = JSON.parse(respon);
	            // console.log(response);
	            $("#approver_name").select2().html('');
	            $("#approver_name").select2({
	                data:response.approver,
	                placeholder : 'Choose Approver'
	            });
	            $('#approver_name').val(approver_id).trigger('change');

	            $("#approver_name_modal").select2().html('');
	            $("#approver_name_modal").select2({
	                data:response.approver,
	                placeholder : 'Choose Approver'
	            });
	            $('#approver_name_modal').val(approver_id).trigger('change');
	        }
	    });
	    $(".header-title.row").removeClass('hidden');
	});
</script>
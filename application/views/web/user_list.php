<script>
    $("#mainContent").addClass('col-sm-12');
    $("#mainContent").removeClass('col-sm-9');
</script>

<br>
<form action="<?= site_url('web/user') ?>" class="search-form" style="position:relative ;width:100%;bottom:5px">
    <div class="form-group has-feedback">
        <label for="role" class="sr-only">Role</label>
        <?php echo form_dropdown('role', $role, $_GET['role'], 'id="role" class="form-control input-sm" style="width:20%; font-size: 12px; letter-spacing: 0.5px;display:inline;margin-bottom:10px" onchange="goSearch()" '); ?>
        <label for="company" class="sr-only">Company</label>
        <?php echo form_dropdown('company', $company, $_GET['company'], 'id="company" class="form-control input-sm" style="width:20%; font-size: 12px; letter-spacing: 0.5px;display:inline;margin-bottom:10px" onchange="goSearch()" '); ?>
        <label for="search" class="sr-only">Search</label>
        <input type="text" class="form-control" name="search" id="search" placeholder="Search" value="<?= $_GET['search'] ?>">
        <a href="javascript:goSearch()"><i class="glyphicon glyphicon-search form-control-feedback" style="position:absolute;left:97%;top:70%"></i></a>
    </div>       
</form>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Username</th>
                                        <th class="text-center">Echelon</th>
                                        <th class="text-center">Company</th>
                                        <th class="text-center">Role</th>
                                        
                                        <?php if ($_GET['role']==Role::DOCUMENT_CONTROLLER) { ?>
                                            <th class="text-center">Chief?</th>
                                        <?php } ?>
                                        <th class="text-center">Active?</th>
                                        <?php if (SessionManagerWeb::isAdministrator()) { ?>
                                            <th class="text-center">Action</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td><?= $v['name'] ?></td>
                                            <td><?= $v['username'] ?></td>
                                            <td><?= $v['eselon_name'] ?></td>
                                            <td><?= $v['company_name'] ?></td>
                                            <td class="text-center"><?php
                                                echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                // switch ($v['role']['id']) {
                                                //     case Role::ADMINISTRATOR:
                                                //         echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                //         break;
                                                //     case Role::DRAFTER:
                                                //         echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                //         break;
                                                //     case Role::SMSI:
                                                //         echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                //         break;
                                                //     case Role::REVIEWER:
                                                //         echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                //         break;
                                                //     case Role::APPROVER:
                                                //         echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                //         break;
                                                //     case Role::CREATOR:
                                                //         echo '<span class="label label-success">' . $v['role']['name'] . '</span>';
                                                //         break;

                                                // }
                                                ?></td>
                                            <?php if ($_GET['role']==Role::DOCUMENT_CONTROLLER) { ?>
                                                <td><?= $v['is_chief']=='X' ? 'YES' : '' ?></td>
                                            <?php } ?>
                                            <td>
                                                <?= $v['is_active']==1 ? 'YES' : 'NO'  ?>
                                            </td>
                                            <?php if (SessionManagerWeb::isAdministrator() or (SessionManagerWeb::isAdminUnit() and $v['company']==SessionManagerWeb::getVariablesIndex('mycompany')) ) { ?>
                                            <td style="text-align: center">
                                                <?php
                                                echo anchor('web/user/edit/' . $v['user_id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                echo anchor('web/user/reset/' . $v['user_id'], '<i class="fa fa-refresh"></i>', 'class="text-warning" data-toggle="tooltip" title="Reset Password"') . '&nbsp;&nbsp;';
                                               // echo anchor('web/user/delete/' . $v['user_id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip" title="Hapus"');
                                                ?>
                                            </td>
                                            <?php } ?>
                                            
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

function goExport() {
    location.href = "<?php echo site_url($path . $class . '/add_import') ?>";
}

function goAdd() {
    location.href = "<?php echo site_url($path . $class . '/add') ?>";
}

function goBack(){
    location.href = "<?php echo site_url($path . 'setting') ?>";
}

function goSearch(){
    var search = document.getElementById('search').value;
    var company = document.getElementById('company').value;
    var role = document.getElementById('role').value;
    location.href = "<?= site_url('web/user') ?>?search="+search+"&company="+company+"&role="+role;
}

</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/newTabs.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/document.css');?>">
<div class="row">
	<div class="col-md-8">
	<?php

        if($error==0){
		  $res = explode(".",basename($file['nama_file']));
          $exts = explode('.',basename($file['link']));
		  $ext = end($res);
          $filename = '';
          foreach ($exts as $key => $value) {
            if ($key<count($exts)-1) {
                $filename.=$value;
            }
          }
		  if(strcmp($ext,"pdf")==0){
	?>
    		<iframe name="viewer" src ="<?php echo site_url("web/share/view/".basename($file['link']));?>" width='800px' height='800px' style="padding-top:15px;padding-bottom: 10px;" allowfullscreen webkitallowfullscreen ></iframe>
    		</div>
    	<?php }else if(strcmp($ext,"jpg")==0 || strcmp($ext,"png")==0 || strcmp($ext,"jpeg")==0 || strcmp($ext,"gif")==0 || strcmp($ext,"bmp")==0){ ?>
    		<img src="<?php echo $file['link'];?>" alt="<?= $ext;?>" style="width:800px;height:800px;float:middle;padding-top:15px;padding-bottom: 10px;"/>
    	<?php }else if (strcmp($ext,"mp4")==0 || strcmp($ext,"mpeg")==0 || strcmp($ext,"avi")==0 || strcmp($ext,"flv")==0 || strcmp($ext,"wmv")==0 || strcmp($ext,"mov")==0 ){ ?>
            <video width='100%' height='100%' controls controlsList='nodownload'><source src="<?= $file['link'] ?>" type='video/<?= $ext ?>'>Browser anda tidak support video.</video>
        <?php }else if(strcmp($ext,"zip")!=0 || strcmp($ext,"rar")!=0 || strcmp($ext,"gz")!=0 || strcmp($ext,"xmind")!=0){?>
    		<div id="viewDocContent" class="overlay-content" style="width: 800px; height: 800px; position: inline;">
    		    <iframe src="https://docs.google.com/gview?url=<?= site_url("/web/thumb/files");?>/<?= $filename.'.' ?><?= $ext ?>&embedded=true" frameborder="0" scrolling="no" seamless style="width:750px;height:800px;float:middle;padding-top:15px;padding-bottom: 10px;"></iframe>
    			<div style="width: 80px; height: 80px; position: absolute; opacity: 0; right: 0px; top: 0px;">&nbsp;</div>
    		</div>
    		<!--<div style="height:100%; width:100%; border:0px; padding:0px; margin:0px">    -->
    <?php   }
        }else if($error>=1){?>
            <div class="alert alert-warning" style="margin-top:15px;">
                <strong>Warning!</strong> <label>Link sudah expired atau link tidak terdaftar</label>
            </div>
    <?php } ?>
	</div>
</div>
<script>
</script>
<style>
.overlay {
    /* Height & width depends on how you want to reveal the overlay (see JS below) */    
    height: 100%;
    width: 0;
    position: fixed; /* Stay in place */
    z-index: 5000; /* Sit on top */
    left: 0;
    top: 0;
    background-color: rgb(0,0,0); /* Black fallback color */
    background-color: rgba(0,0,0, 0.9); /* Black w/opacity */
    overflow-x: hidden; /* Disable horizontal scroll */
    transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
}

/* Position the content inside the overlay */
.overlay-content {
    position: relative;
    height: 90%;
    width: 90%; 
    top: 2%;
    left:5%;
}

/* The navigation links inside the overlay */
.overlay a {
    padding: 8px;
    text-decoration: none;
    font-size: 36px;
    color: #818181;
    display: block; /* Display block instead of inline */
    transition: 0.3s; /* Transition effects on hover (color) */
}

/* When you mouse over the navigation links, change their color */
.overlay a:hover, .overlay a:focus {
    color: #f1f1f1;
}

/* Position the close button (top right corner) */
.overlay .closebtn {
    position: absolute;
    top: 20px;
    right: 45px;
    font-size: 60px;
}

/* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
@media screen and (max-height: 450px) {
    .overlay a {font-size: 20px}
    .overlay .closebtn {
        font-size: 40px;
        top: 15px;
        right: 35px;
    }
}	
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-12">
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <?php if(!isset($id)){ ?>
                                <?php echo form_input(array('name' => 'name', 'value' => $data['name'], 'class' => 'form-control input-sm', 'id' => 'name')); ?>
                                <?php } else { 
                                    echo $data['name']; 
                                    echo form_hidden('name', $data['name']);
                                } ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="type number" class="col-sm-4">Type</label>
                            <div class="col-sm-8">
                                <?php echo form_dropdown('type', $a_type, $data['type'], 'id="type" class="form-control input-sm"'); ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="value" class="col-sm-4">Content</label>
                            <div class="col-sm-8">
                                <?php echo form_input(array('name' => 'integer', 'value' => $data['value'], 'class' => 'form-control input-sm input integer', 'style' => 'display:none;')) ?>

                                <div class="image input">
                                    <?php echo form_upload(array('name' => 'upload', 'id' => 'upload'), $data['value'], '', 'upload', TRUE) ?>
                                    <span class="text-upload"><?= $data['value'] ?></span>
                                </div>

                                <?php echo form_textarea(array('name' => 'string', 'value' => ($data['value'] ? $data['value'] : ''), 'class' => 'form-control input-sm input string', 'style' => 'display:none;')) ?>

                                <?php echo form_hidden('value', $data['value']); ?>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <div class="col-md-12 post-new-footer-item-right text-right">
                                <a class="post-right" href="#"><button class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                            </div>
                        </div>
                    </div>
                    <?php echo form_hidden('referer') ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function goChangePass() {
        $("#modal_form").modal();
    }

    $(function(){
        $("[name='type']").trigger("change");
    });

    $('.input').change(function(){
        $("[name='value']").val(this.value);
    });

    $("[name='type']").change(function(){
        $('.input').hide();
        $("."+this.value).show();
    });

</script>
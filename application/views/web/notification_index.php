<?php
	if (empty($data)) {
?>
	<div class="alert alert-info">There's no <?php echo $title ?> yet</div>
<?php
	}
	else {
?>
<table class="notification-table">
	<?php
		foreach($data as $v) {
			// memilih link
			switch($v['reference_type']) {
				case Notification_model::TYPE_DOCUMENT_DETAIL: 
					$link = site_url('web/document/detail/'.$v['reference_id']); 
				break;
				case Notification_model::TYPE_DOWNLOAD_DETAIL:
					$link = site_url('web/document/download_detail/'.$v['reference_id']); 
				break;
				case Notification_model::TYPE_DOWNLOAD_DOCUMENT:
					$link = site_url('web/document/download_document/'.$v['reference_id']);
				break;
				case Notification_model::TYPE_CHANGEUNIT_DETAIL:
					$link = site_url('web/document/changeunit/'.$v['reference_id']);
				break;
				case Notification_model::TYPE_DOCUMENT_FILTER:
					$link = site_url('web/document/searchDocumentById/'.$v['reference_id']);
				break;
				default: unset($link); break;
			}
	?>
	<tr>
		<td>
			<a data-id="<?php echo $v['id'] ?>" href="<?php echo empty($link) ? 'javascript:void(0)' : $link ?>">
				<table class="comment-table<?php echo $v['users']['status'] == 'R' ? '' : ' notification-unread' ?>">
					<tr>
						<td>
							<div class="site-photo site-photo-48">
								<!-- <img src="<?php //echo isset($v['sender']['photo']) ? site_url($path.'thumb/watermark/'.$v['sender']['id'].'/48') : $nopic ?>"> -->
								<img src="<?php echo isset($v['sender']['photo']) ? $v['sender']['photo']['thumb']['link'] : $nopic ?>"/>
							</div>
						</td>
						<td class="comment-info">
							<strong><?php echo $v['sender']['name'] ?></strong>
							<?php echo $v['message'] ?>
							<div class="post-header-info">
								<?php echo str_replace('.', ':', $v['created_at'])?>
							</div>
						</td>
					</tr>
				</table>
			</a>
		</td>
	</tr>
	<?php } ?>
</table>
<nav>
    <ul class="pager">
        <?php
        
        if ($this->router->method=='group'){
            $group = $this->uri->segment(4);
            $page = $this->uri->segment(5);
            if (empty($page) || $page == 0) {
                echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . $group . '/'. ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
            }
            if (empty($data) || count($data) < 10) {
                echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . $group . '/'. ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
            }
        } else {
            $page = $this->uri->segment(4);
            if (empty($page) || $page == 0) {
                echo '<li class="disabled"><a href = "#">Sebelumnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page - 1) . Util::paramsToUrlGet($_GET), 'Sebelumnya') . '</li>';
            }
            if (empty($data) || count($data) < 10) {
                echo '<li class="disabled"><a href = "#">Selanjutnya</a></li>';
            } else {
                echo '<li>' . anchor('web/' . $this->router->class . '/' . $this->router->method . '/' . ($page + 1) . Util::paramsToUrlGet($_GET), 'Selanjutnya') . '</li>';
            }
        }
        
        
        ?>
    </ul>
</nav>
<?php echo form_open(null, array('id' => 'form_delete')) ?>
<?php echo form_hidden('referer') ?>
</form>
<?php
	}
?>

<script type="text/javascript">

$(function() {
	$(".notification-table a").mousedown(function() {
		$.get("<?php echo site_url($path.'notification/read') ?>" + "/" + $(this).attr("data-id"));
	});
});

function goReadAll() {
	location.href = "<?php echo site_url($path.'notification/read_all') ?>";
}

</script>
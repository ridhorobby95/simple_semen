<style type="text/css">
    #rightSidebar {
        display: none!important;
    }
    #mainContent {
        margin-bottom: 50vh;
    }
</style>
<?php include('_document_modal.php'); ?>
<?php include('document_modal_upload.php'); ?>
<?php include('document_modal_related.php'); ?>
<?php include('document_modal_viewdoc.php'); ?>
<div class="row ">

	<div class="col-md-8" style="padding: 0px;">
        <h3 id='h3_title' style='font-weight: bold;margin: 0px;'><?php echo $title;?>
	<!-- <a><button class="btn btn-primary pull-right" id="btn_upload" style="margin-top: 0px;margin-right: 5px" onClick="openNewUploadBox()">Upload Dokumen</button></a> --></h3><br>
	<!-- <div id="filterDiv" style="background-color:#ff3232;display:none" >
		<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="float:right; 
			<?php// if($isFiltered==0) echo "background-color:#ff3232;";
				 // else echo "background-color:#00cd00;"?> color:#fff;padding:5px;border-radius: 5px;"><?php// if($isFiltered==0) echo "Filter OFF ";
				  //else echo "Filter ON "?><i id="iconAdvanceSearch" class="fa fa-filter"></i></a>
	</div> -->
	<div id="filterDiv" style="/*background-color:#ff3232;*/">
		<?php if (SessionManagerWeb::getRole() != Role::EKSTERNAL): ?>
		<a class="mousedrag01" style="" onClick="openNewUploadBox()">
			<i class="fa fa-file" aria-hidden="true" style="font-size: 25px;">
				<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">
					New Document
				</div>
			</i>
		<?php endif ?>

		</a>
		<?php if (SessionManagerWeb::isDocumentController() and $myworkunit_company=='2000') { ?>
			<a class="mousedrag02" onClick="openNewUploadBoxBoardManual()" style="margin-left: 10px;">
				<i class="fa fa-file" aria-hidden="true" style="font-size: 25px;">
					<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">
						Add Pedoman Tata Laksana
					</div>
				</i>
			</a>
		<?php } ?>
		<?php
		if (1==0) {
		$isFilter = true;
		if(strcmp($jenisFilter,"")!=0 || strcmp($lokasiFilter,"")!=0 || strcmp($tagFilter,"")!=0 || strcmp($pemilikFilter,"")!=0 || strcmp($titleFilter,"")!=0)
			$isFilter = true;

		 if($isFilter || $isFiltered>0){ ?>
		<a data-toggle="collapse" class="mousedrag013" data-parent="#accordion" href="#collapse1" style="<?php if($isFiltered==0)echo "color:#56baed; margin-top: 0%;";else echo "color:#ff9b9b;"; ?>">
			<i class="fa fa-filter" aria-hidden="true" style="font-size: 25px;">
				<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato, sans-serif;font-size: 14px;font-weight:700;">
					<?php if($isFiltered==0) echo "Filter Off";else echo "Filter On"?>
							<?php } ?>
				</div>
			</i>
		</a>
		<?php } ?>

		<?php
		$isFilter = true;
		if (isset($_SESSION['document_search']) && count($_SESSION['document_search']) != 0)
			$isFiltered = 1;

		// cek document search sama atau ga 
		if (count($_SESSION['document_search'])==1){
			$mn = explode('_', $_SESSION['selected_menu']);
            $selected_menu = $mn[0];
			foreach ($_SESSION['document_search'] as $key => $value) {
				switch($key){
					case 'user':
                        if ($selected_menu=='me'){
                        	$isFiltered = 0;
                        }
                    break;
                    case 'type':
                        if ($selected_menu=='type'){
                        	$isFiltered = 0;
                        }
                    break;
                    case 'status':
                        if ($selected_menu=='status'){
                        	$isFiltered = 0;
                        }
                    break;
                    case 'unitkerja':
                        if ($selected_menu=='unitkerja'){
                        	$isFiltered = 0;
                        }
                    break;

                    case 'related':
                        if ($selected_menu=='related'){
                        	$isFiltered = 0;
                        }
                    break;

                    case 'obsolete':
                    	if ($selected_menu=='obsolete'){
                        	$isFiltered = 0;
                        }
                    break;
				}
			}
		}
		

		 if($isFilter){ ?>
			<a data-toggle="collapse" class="mousedrag013 hidden" data-parent="#accordion" href="#collapse1" style="<?php if($isFiltered==0)echo "color:#56baed; margin-top: 0%;";else echo "color:#ff9b9b;"; ?>">
				<i class="fa fa-filter" aria-hidden="true" style="font-size: 25px;">
					<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato,sans-serif;font-size: 14px;font-weight:700;">
							<?php if($isFiltered==0) echo "Filter Off";else echo "Filter On "?>
									
					</div>
				</i>
			</a>
		<?php } ?>

		<button type="button" class="btn mousedrag01" data-toggle="modal" data-target=".bs-example-modal-lg" style="margin-left: 2%;">
			<i class="fa fa-filter" aria-hidden="true" style="font-size: 25px;">
				<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 10px;float:right; font-family: Muli, Lato,sans-serif;font-size: 14px;font-weight:700;">
					<?php if($isFiltered==0) echo "Filter";else echo "Filter "?>			
				</div>
			</i>
		</button>
	</div>


		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content" style="padding-left: 5%;">
		      <div class="row">
		      	<div class="col-md-12">
		      		<br>
		      		<div class="row">
		      			<div class="col-md-10">
		      				<h2 style="margin: 0%;">
		      					Filter
		      				</h2>
		      			</div>
		      			<div class="col-md-2" style="text-align: center">
	      					<i class='fa fa-times-circle onhovering hidden' aria-hidden='true' style='' data-dismiss="modal" aria-hidden="true">
	      						
	      					</i>
		      			</div>
		      		</div>
		      	</div>
		      </div>
		      <br>
		    <form method="post" name="filter_form" id="filter_form" action="<?= site_url('web/document/setSessionSearch').'/'.$method ?>" style="">
		    	<input type="hidden" id="numPage" name="numPage" value="<?php echo $_SESSION['page'];?>">

			    <div class="row">
		      	
			      	<div class="col-md-12">
			      		<div class="row">
			      			<div class="col-md-2">
			      				<h5>
			      					Keyword
			      				</h5>
			      			</div>
			      			<div class="col-md-9">
								<div class="input-group">
									<input id="title" name="title" type="text" class="form-control input-md inputplus" id="title" placeholder="Write Document Title or Code" value="<?= $_SESSION['document_search']['title'] ?>">
									<div class="input-group-addon no-bg-addon" onclick="javascript:remove_title()">
										<i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
							
										</i>
									</div>
								</div>
			      			</div>
			      		</div>
			      		<br>
			      		<div class="row">
			      			<div class="col-md-2">
			      				<h5>
			      					Document type
			      				</h5>
			      			</div>
			      			<div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                   	<?php 
                                   	foreach ($variables['document_types'] as $k => $type) {
                                   		$types[$type['id']] = $type['type'];
                                   	}
                                   	unset($types[""]);
                                   	echo form_dropdown('type[]', $types, $_SESSION['document_search']['arr_type'], 'id="type" class="form-control input-sm select2 type" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" '); ?>
                                   	<div class="input-group-addon no-bg-addon" onclick="javascript:remove_type()">
										<i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
							
										</i>
									</div>
                                </div>
                            </div>
			      		</div>
			      		<div class="row">
			      			<div class="col-md-2">
			      				<h5>
			      					Status
			      				</h5>
			      			</div>
			      			<div class="form-group col-md-9">
                                <div class="input-group" style="width: 100%;">
                                   	<?php
                                   	$status = array(
                                   		1 	=> 'Publish',
                                   		2	=> 'Evaluation',
                                   		3	=> 'Revision'
                                   	); 
                                   	// foreach ($variables['document_types'] as $k => $type) {
                                   	// 	$types[$type['id']] = $type['type'];
                                   	// }
                                   	// unset($types[""]);
                                   	echo form_dropdown('status[]', $status, $_SESSION['document_search']['status'], 'id="status" class="form-control input-sm select2 type" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" '); ?>
                                   	<div class="input-group-addon no-bg-addon" onclick="javascript:remove_type()">
										<i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
							
										</i>
									</div>
                                </div>
                            </div>
			      		</div>
			      		<div class="row">
			      			<div class="col-md-2">
			      				<h5>
			      					Work Unit
			      				</h5>
			      			</div>
			      			<div class="form-group col-md-9">
			      				<div class="input-group" style="width: 100%;">
									<?php
										// $unitkerja = $variables['unitkerja'];
										// unset($unitkerja[0]);
										echo form_dropdown('unitkerja[]', $unitkerja, $_SESSION['document_search']['arr_unitkerja'], 'id="unitkerja" class="form-control input-sm select2 unitkerja" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" ');
									?>
									<div class="input-group-addon no-bg-addon" onclick="javascript:remove_workunit()">
										<i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
							
										</i>
									</div>
								</div>
			      			</div>
			      		</div>
			      		<div class="row">
			      			<div class="col-md-2">
			      				<h5>
			      					Date Start
			      				</h5>
			      			</div>
			      			<div class="form-group col-md-9">
			      				<div class="input-group" style="width: 100%;">
									 
            						<div class="input-group date" data-provide="datepicker">
									    <input type="text" name="datestart" id="datestart" class="form-control input-md inputplus" value="<?= $_SESSION['document_search']['dateend'] ?>" placeholder="Date Start" onchange="javascript:dateChange()"/>
									    <div class="input-group-addon">
									        <span class="glyphicon glyphicon-th"></span>
									    </div>
									</div>

									<div class="input-group-addon no-bg-addon" onclick="javascript:remove_datestart()">
										<i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
							
										</i>
									</div>
								</div>
			      			</div>
			      		</div>
			      		<div class="row">
			      			<div class="col-md-2">
			      				<h5>
			      					Date End
			      				</h5>
			      			</div>
			      			<div class="form-group col-md-9">
			      				<div class="input-group" style="width: 100%;">
									
									<div class="input-group date" data-provide="datepicker">
									    <input type='text' id='dateend' name="dateend"  class="form-control input-md inputplus" value="<?= $_SESSION['document_search']['dateend'] ?>" placeholder="Date End"/ onchange="javascript:dateChange()">
									    <div class="input-group-addon">
									        <span class="glyphicon glyphicon-th"></span>
									    </div>
									</div>

									<div class="input-group-addon no-bg-addon" onclick="javascript:remove_dateend()">
										<i class="fa fa-close" aria-hidden="true" style="font-size: 18px; color: #FF5722;">
							
										</i>
									</div>
								</div>
			      			</div>
			      		</div>
			      	</div>
		      	</div>
		      	<br>
		      	<div class="row">
		      		<div class="col-md-12">
		      			<div class="row">
		      				<div class="col-md-8">
      							<button type="button" class="btn btn-filto" data-dismiss="modal">
									Cancel
								</button>
			      			</div>     			
			      			<div class="col-md-3 text-right">
	      						<button type="submit" class="btn btn-filtu" onclick="unsetPage()">
									Apply Filter
								</button>
			      			</div>
			      		</div>
			      	</div>
		        </div>
		      <br>
			</form>
		    </div>
		  </div>
		</div>
	
	<br><br>
	<div class="panel-group col-md-12 " id="accordion"	style="padding-left: 0px;padding-right: 0px;">
		<br>
		<div class="panel panel-default <?= ($isFiltered) ? '' : 'hidden' ?>" style="display: flex; padding: 3% 2%; flex-wrap: wrap;">
			<span style="margin-top: 1%;">
				Filtered By:
			</span>
			<?php if (isset($_SESSION['document_search']['title'])) { ?>
				<span class="label label-primary filter-tag" style="">
					<a href="<?= site_url('web/document/removeFilter/title') ?>" style="color: #FF5722;">
						<i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
							
						</i>
					</a>
					keyword: <b><?= $_SESSION['document_search']['title'] ?></b>
				</span>
			<?php } ?>
			<?php if (isset($_SESSION['document_search']['type'])) { ?>
				<?php foreach ($_SESSION['document_search']['arr_type'] as $k => $type) { ?>
					<span class="label label-primary filter-tag" style="">
						<a href="<?= site_url('web/document/removeFilter/type') ?>/<?= $type ?>" style="color: #FF5722;">
							<i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
								
							</i>
						</a>
						type: <b><?= $types[$type] ?></b>
					</span>
				<?php } ?>
			<?php } ?>
			<?php if (isset($_SESSION['document_search']['status'])) { ?>
				<?php foreach ($_SESSION['document_search']['arr_status'] as $k => $status) { ?>
					<span class="label label-primary filter-tag" style="">
						<a href="<?= site_url('web/document/removeFilter/status') ?>/<?= $status ?>" style="color: #FF5722;">
							<i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
								
							</i>
						</a>
						<?php 
							if($status == 1){
								$status_text = "Publish";
							}
							elseif($status == 2){
								$status_text = "Evaluation";
							}
							elseif($status == 3){
								$status_text = "Revision";
							}
						 ?>
						status: <b><?= $status_text ?></b>
					</span>
				<?php } ?>
			<?php } ?>
			<?php if (isset($_SESSION['document_search']['unitkerja'])) { ?>
				<?php foreach ($_SESSION['document_search']['arr_unitkerja'] as $k => $unit) { ?>
					<span class="label label-primary filter-tag" style="">
						<a href="<?= site_url('web/document/removeFilter/workunit') ?>/<?= $unitkerja ?>" style="color: #FF5722;">
							<i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
								
							</i>
						</a>
						workunit: <b><?= $unitkerja[$unit] ?></b>
					</span>
				<?php } ?>
			<?php } ?>
			<?php if (isset($_SESSION['document_search']['datestart'])) { ?>
				<span class="label label-primary filter-tag" style="">
					<a href="<?= site_url('web/document/removeFilter/datestart') ?>" style="color: #FF5722;">
						<i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
							
						</i>
					</a>
					date start: <b><?= $_SESSION['document_search']['datestart'] ?></b>
				</span>
			<?php } ?>
			<?php if (isset($_SESSION['document_search']['dateend'])) { ?>
				<span class="label label-primary filter-tag" style="">
					<a href="<?= site_url('web/document/removeFilter/dateend') ?>" style="color: #FF5722;">
						<i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
							
						</i>
					</a>
					date end: <b><?= $_SESSION['document_search']['dateend'] ?></b>
				</span>
			<?php } ?>


			<span class="label label-primary filter-tag long-title hidden" style="">
				<a href="#" style="color: #FF5722;">
					<i class="fa fa-close" aria-hidden="true" style="font-size: 13px;">
						
					</i>
				</a>

			</span>
		</div>
		<div class="col-md-6 <?= ($isFiltered) ? '' : 'hidden' ?>" style="background-color: #fff;margin: 0px;">
			<div class="col-md-8" style="padding: 2%;">
				<a href="<?= site_url('web/document/removeDocumentFilter').'/'.'index' ?>">
					<button type="button" class="btn btn-filto">
						Clear Filter
					</button>
				</a>
			</div>
			<!-- <div class="row col-md-4 text-right hidden"  style="padding: 2%;">			
				<button type="submit" class="btn btn-filtu" onclick="unsetPage()">
					Add Filter
				</button>
			</div> -->
		</div>
		<?php if (SessionManagerWeb::isAdministrator() or SessionManagerWeb::isDocumentController()) { ?>
			<div class="col-md-<?= ($isFiltered) ? '6' : '12' ?>" style="display:inline;background-color: #fff;margin:0px;">
				<div class="col-md-<?= ($isFiltered) ? '6' : '7' ?> dropdown" style="padding: <?= ($isFiltered) ? '3.9%' : '4%'; ?>;text-align: right;vertical-align: bottom">
					<label >
						There are <b style="color:blue"><?= $total_document ?></b> documents found!
					</label>
				</div>
				<div class="col-md-<?= ($isFiltered) ? '3' : '3' ?> dropdown button_checklist_download" style="margin-top: 23px;">
					<button type="submit" class="btn btn-filtu" onclick="checklistDownload(1)">
					Mark For Download
				</button>
				</div>
				<div class="col-md-<?= ($isFiltered) ? '3' : '3' ?> dropdown button_unchecklist_download hidden" style="margin-top: 23px;">
					<button type="submit" class="btn btn-filtu" onclick="checklistDownload(0)">
					Unmark Download
				</button>
				</div>
				<div class="col-md-<?= ($isFiltered) ? '3' : '2' ?> dropdown" style="padding:<?= ($isFiltered) ? '0.5%':'2%' ?>"">
					<button type="button" class="btn btn-filtu" data-toggle="dropdown" style="margin-left: 2%;">
						<div class="" style=" letter-spacing: 0px; margin: 5px 0px 5px 5px;float:right; font-family: Muli, Lato,sans-serif;font-size: 14px;font-weight:700;">
							Export <span class="caret"></span>
						</div>
					</button>
		            <ul class="dropdown-menu" style="text-align: left;">
		            	<?php if ($isFiltered) { ?>
			                <li>
			                    <a href="<?= site_url('web/report/exportDID/these') ?>" target="_blank">Export <b style="color:blue">These <?= ($isFiltered) ? 'Filtered' : '' ?></b>  Documents</a>
			                </li>
			                <li class="divider"></li>
		                <?php } ?>
		                <li>
		                    <a href="<?= site_url('web/report/exportDID').'/'.SessionManagerWeb::getCompany().'/0' ?>" target="_blank">Export <b style="color:blue"><?= $company[SessionManagerWeb::getCompany()] ?></b> Documents (All)</a>
		                </li>
		            </ul>
				</div>
	        </div>
        <?php } ?>
	    <div class="panel panel-default hidden">	
	     <div id="collapse1" class="panel-collapse in" style="/*border: 1px solid #27ae60;*/ border-radius: 3px; box-shadow: 0px 5px 10px rgba(0,0,0,0.2);">
	        <div class="panel-body" style="padding: 0px 0px 0px 0px;">     
	        	<div class="tabs">
		    		<ul class="tab-links"  style="border-bottom: 2px solid #E0E0E0;">
		    			<?php 
		    				$menus = explode('_', $_SESSION['selected_menu']);
			                $selected_menu = $menus[0];
		    			?>
		        		<li class="active" <?= ($selected_menu=='type') ? "style='display:none'" :'' ?> ><a href="#tabJenis">Type</a></li>
		        		<li <?php if(strcmp($lokasiFilter,"")!=0) echo "class='active'";?> style="display:none"><a href="#tabLokasi">Lokasi</a></li>
		        		<li class="hidden" <?php if(strcmp($tagFilter,"")!=0) echo "class='active'";?>><a href="#tabTag">Tags</a></li>
		        	</ul>
			        <div class="tab-content" style="max-height:250px;overflow-y: auto;<?= ($selected_menu=='type') ? 'display:none' : ''?> ">
				        <div id="tabJenis" class="tab active">
				        	<table style="width:100%;">
				        		<?php
				        			$ShowingJenis = 0;
				        			foreach($variables['document_types'] as $obj){
				        				if($ShowingJenis%2==0){
					        				echo "<tr>";
					        			}
					        			if(strcmp($obj['id'],$_SESSION['document_search']['type'])==0)
					        				echo "<td style='background-color:#E1F5FE;'><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterJenis(".'"'.$obj['id'].'"'. ")'><b> ". $obj['type'] . "</b></a></td>";
					        			else
					        				echo "<td><i class='fa fa-folder' style='color:#56baed;'></i><a href='#' style='color:#000;' onClick='filterJenis(".'"'.$obj['id'].'"'. ")'><b> ". $obj['type'] . "</b></a></td>";
					        			if($ShowingJenis%2==1){
					        				echo "</tr>";
					        			}
					        			$ShowingJenis++;
					        		}
				        		?>
				        	</table>
				        </div>
				    </div>
				</div>
				<div class="col-md-12" style="    background-color: #E1F5FE" >
					<form method="post" name="filter_form" id="filter_form" action="<?= site_url('web/document/setSessionSearch').'/'.$method ?>" style="">
		        		<div class="upload-new" style="background-color:#E1F5FE;">
			        		<div style="margin-left:10px;margin-top:10px">
								<!-- <input type="hidden" id="numPage" name="numPage" value="<?php echo $page;?>">
					        	<input type="hidden" id="idOrganisasi" name="idOrganisasi" value="<?php echo $idOrganisasi;?>">
					        	<input type="hidden" id="lokasiFilter" name="lokasiFilter" value="<?php echo $lokasiFilter;?>">
					        	
					        	<input type="hidden" id="tagFilter" name="tagFilter" value="<?php echo $tagFilter;?>"> -->
					        	<input type="hidden" id="show_by" name="show_by" value="<?php echo $_SESSION['document_search']['show_by'];?>">
					        	<input type="hidden" id="type" name="type" value="<?php echo $_SESSION['document_search']['type'];?>">
					        	<div>
							        <div class="upload-new-input" style="background-color:#E1F5FE;">
				                        <i class="fa fa-search" ></i>
				                        <input type="text" id="title" name="title" value="<?php echo $_SESSION['document_search']['title'];?>" class="form-control input-sm" placeholder="Write Document Title" style="font-size:15px;display:inline;width:41%;margin-left:5px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
				                        <?php if ($_SESSION['selected_menu']!='me') { ?>
					                        <i class="fa fa-user" style="padding-left: 30px;"></i>
					                        <input type="text" id="user" name="user" value="<?php echo $_SESSION['document_search']['user'];?>" class="form-control input-sm" placeholder="Document Author" style="font-size:15px;display:inline;width:41%;margin-left:5px;border-radius: 5px;-moz-border-radius: 5px;-webkit-border-radius: 5px; border: 1px solid #aaa;">
				                        <?php } ?>
									</div>
								</div>
				            </div>
				        </div>
				        
				        <button type='submit' value='Filter' class="btn pull-right btn-filt">
				        	Search Document
				        	<!-- <label style="color:#fff;letter-spacing: 0.7px;">Cari Dokumen</label> -->
				        </button>
				        <?php if ($isFiltered) { ?>
				        	<a href="<?= site_url('web/document/removeDocumentFilter').'/'.$method; ?>" class="btn btn-clear-s pull-right" style="margin:0px 0px 15px 30px;">Clear Filter
					        </a>
				        <?php } ?>
				    </form>
				</div>
	        </div>
	      </div>
	    </div>
	</div>

	  <div style="display:none">
	  	<p style="color:#424242; font-weight: bold; font-size: 15px;letter-spacing: .5px;">
	  		Pencarian Berdasarkan Jenis Dokumen 
	  	</p>
	  </div>
	<br>
	<form method="POST" class="form_checklist_download" action="<?php echo base_url('web/document/download_checklist') ?>">
	<table id="t1" style="width:100%;table-layout:fixed;word-wrap:break-word">
		<tr>
			<!-- <th width="35%">Kode</th> -->
			<th width="10%" class="th_checklist_download hidden" title="Mark All"><input type="checkbox" name="check_all" class="form-control check_all" id="check_all"></th>
			<th width="30%">Code Doc.</th>
			<!-- <th width="15%">Post ID</th> -->
			<th width="23%">Title</th>
			<th width="22%">Workunit</th>
			<th width="10%">Rev No.</th>
			<th width="10%">Publish Date</th>

			<th width="10%">Reassesment Date</th>
		</tr>
		<?php
			setlocale(LC_ALL, 'IND');
			$temp = 0;
			$page=1;
			if(empty($documents)){
				echo "<tr>";
				echo "<td colspan=6 style='background-color:#ffebee;color:#f44336; letter-spacing:1px; text-align:center;'><b>No Documents</b></td>";
				echo "</tr>";
				$min=0;
			}else{
				$min = ($page-1)*10;
				// echo '<pre>';
				// var_dump($documents);
				// die();
				foreach($documents as $obj){
					if($temp<$min){
						$temp++;
						continue;
					}
					$row = $temp-$min;
					$arr = explode('.', $obj['filename']);
					$extension = end($arr);
					echo "<tr id='tr$row' class='clickable-row' data-url='". json_encode($obj) . "' style='background-color:#fff;border-bottom:1px solid #d9dadc'>";
					?>
					<td class="td_checklist_download hidden"><input type="checkbox" name="checklist_download[]" value="<?php echo $obj['document_id'] ?>" class="form-control <?= ($obj['have_header'] == 1) ? 'checklist_download' : 'checklist_download_disabled' ?>" <?= ($obj['have_header'] == 1) ? '' : 'disabled' ?>>
					</td>
					<?php
					echo "<td style='overflow:hidden; font-size:12px;'>";
					?>
					<?php
					echo "<div style='float:left;'><img src='";
					switch ($obj['workflow_id']) {
              			case 1:
              				$type = "submit-01.png";
              				$title = "Submission";
		              		if ($obj['status']=='D'){
		              			$type= "pub-green-01.png";
		              		}
              				break;
              			
              			case 2:
              				$type = "evaluasi-01.png";
              				$title = "Evaluation";
              				if ($obj['hasil_evaluasi']=='R'){
              					$type = "revisi-01.png";
	              				$title = "Revision";
              				} else {
              					if ($obj['status']=='D'){
			              			$type= "pub-green-01.png";
			              		}
              				}
              				break;
              			case 3:
              				$type = "revisi-01.png";
              				$title = "Revision";
		              		if ($obj['status']=='D'){
		              			$type= "pub-green-01.png";
		              		}
              			break;
              		}
              		if ($obj['show']==0){
              			$type= "obsolete-01.png";
              		}
              		echo site_url("assets/web/images/icondoc/mod1").'/'.$type;
					// if(strcmp($extension,"pdf")==0)
					// 	echo site_url('/assets/web/images/pdf.png');
					// else if(strcmp($extension,"doc")==0 || strcmp($extension,"docx")==0)
					// 	echo site_url('/assets/web/images/docx.png');
					// else if(strcmp($extension,"ppt")==0 || strcmp($extension,"pptx")==0)
					// 	echo site_url('/assets/web/images/ppt.png');
					// else if(strcmp($extension,"xls")==0 || strcmp($extension,"xlsx")==0)
					// 	echo site_url('/assets/web/images/xls.png');
					// else if(strcmp($extension,"zip")==0)
					// 	echo site_url('/assets/web/images/zip.png');
					// else if(strcmp($extension,"rar")==0)
					// 	echo site_url('/assets/web/images/rar.png');
					// else if (strcmp($extension,"xmind")==0)
					// 	echo site_url('/assets/web/images/xmind.png');
					// else if (strcmp($extension,"mp4")==0 || strcmp($extension,"wmv")==0 || strcmp($extension,"mov")==0 || strcmp($extension,"avi")==0 || strcmp($extension,"flv")==0 || strcmp($extension,"mpeg")==0)
					// 	echo site_url('/assets/web/images/video.png');
					// else
					// 	echo site_url('/assets/web/images/image.png');
					echo "' alt='Icon' style='width:30px;height:30px;vertical-align:middle;padding-right:5px'/></div> ";
					echo "<b>  ". $obj['code']. "</b></td>";
					// echo "<td><h5 style='vertical-align:middle;overflow:hidden;line-height:30px;margin:0px'><b>  ". $obj['title']. "</b></h5></td>";
					// echo "<td>". $obj['post_id'] ."</td>";
					echo "<td style='font-size:12px'><b>". strtoupper($obj['title']) ."</b></td>";
					echo "<td style='font-size:12px'><b>". strtoupper($obj['unitkerja_name']) ."</b></td>";
					echo "<td style='font-size:12px'>" . $obj['revision'] . "</h5></td>";
					// echo "<td>". $obj['unitkerja_name']." ".($obj['change_workunit']!='O') ? "<i class='fa fa-exclamation-circle' style='color:red' aria-hidden='true'></i>" : "ok ". "</td>";
					if($obj['hasil_evaluasi'] == 'D'){
						$publish = strftime('%d %b %Y', strtotime($obj['publish_date']));
					}
					else{
						$publish = strftime('%d %b %Y', strtotime('-1 year' , strtotime($obj['validity_date'])));
					}
					echo "<td style='font-size:12px'>".$publish ." ";
					// if ($obj['change_workunit']!='O' and (SessionManagerWeb::isDocumentController() or SessionManagerWeb::isAdministrator())){
					if ($obj['change_workunit']!='O' and (SessionManagerWeb::isDocumentController())){
						echo "<i class='fa fa-exclamation-circle' style='color:red' aria-hidden='true'></i>";
					}
					echo "</td>";
					echo "<td style='font-size:12px'>" . strftime('%d %b %Y', strtotime($obj['validity_date'])) . "</td>";
					echo "</tr>";
					$temp++;
					// if($temp >= $page*10)
					// 	break;
				}
			}
		?>
	</table>
	<br>
	<button type="submit" class="btn btn-primary submit_checklist_download hidden">Download</button>
	<!-- <input type="submit" name="submit" class=""> -->
	</form>
	<center id="divPaging" style="margin-bottom:15px;"><ul class="pagination col-md-9">
		<?php
			$maxPage = $total_document/10;
			// $maxPage = $numDoc/10;
			if($total_document%10 !=0)
				$maxPage++;
			$temp = 0;
			$pageNow = $_SESSION['page'];
			$y = $pageNow-5;
			if($y<1)
				$y=1;
			for($i=$y;$i<=$pageNow;$i++){
				echo "<li";
				if($min_data == ($i-1)*10+1){
					echo " class='active'";
				}
				echo "><a href='#' onClick='changePage($i)'>$i</a></li>";
			}
			for($i=$pageNow+1;$i<=$maxPage;$i++){
				echo "<li";
				if($min_data == ($i-1)*10+1){
					echo " class='active'";
				}
				echo "><a href='#' onClick='changePage($i)'>$i</a></li>";
				$temp++;
				if($temp>4)
					break;
			}
		?>
	</ul></center>
	<div class="col-md-3 pull-right">
		<label class="pull-right">
			<b><?= $min_data ?>..<?= $max_data ?></b> from <b><?= $total_document ?></b> Documents
		</label>
	</div>
	</div>
	<?php include('_document_detail.php'); ?>
</div>
<iframe id="frame_download" style="display:none;"></iframe>
<div class="menu" style="padding:5px;z-index:1000;">
    <div id="view" class="menu-item" onClick="View()">
    	<p class="menu-item-detail">View<i class="fa fa-eye" aria-hidden="true" style="float:right;"></i></p>
    </div>
    <div class="menu-item" onClick="show_ask_download()">
    	<p class="menu-item-detail">Ask Download <i class="fa fa-download" aria-hidden="true" style="float:right;"></i></p>
    </div>
    <div class="menu-item" onClick="openLastEvaluation()">
    	<p class="menu-item-detail">Last Evaluation <i class="fa fa-compress" aria-hidden="true" style="float:right;"></i></p>
    </div>
    
    <div></div>
    <div class="menu-item hidden" onClick="Edit()">
    	<p class="menu-item-detail">Upload New Version <i class="fa fa-pencil-square-o" aria-hidden="true" style="float:right;"></i></p>
    </div>
    <div class="menu-item hidden" onClick="Revision()">
    	<p class="menu-item-detail">Manage Version <i class="fa fa-recycle" aria-hidden="true" style="float:right;"></i></p>
    	</div>
    <div class="menu-item hidden" onClick="openShareBox()">
    	<p class="menu-item-detail">Share <i class="fa fa-share-alt" aria-hidden="true" style="float:right;"></i></p>
    </div>
</div>
<style>
	.ui-datepicker {    width: 216px;
    height: auto;
    font: 9pt Arial, sans-serif;
    border: thick solid red;
}
</style>

<?php include('_document_js.php'); ?>
<script>
	function remove_title(){
		document.getElementById('title').value=null;
	}
	function remove_type(){
		$('#type').val(null).trigger("change");
	}
	function remove_workunit(){
		$('#unitkerja').val(null).trigger("change");
	}
	function remove_datestart(){
		document.getElementById('datestart').value=null;
	}
	function remove_dateend(){
		document.getElementById('dateend').value=null;
	}
</script>
<script type="text/javascript">
	$.fn.datepicker.defaults.format = "yyyy-mm-dd";

	function dateChange(){
        var fStart = $("[name='datestart'");
        var fEnd = $("[name='dateend'");

        var aStart = fStart.val().split('-');
        var aEnd = fEnd.val().split('-');

        var dStart = new Date(aStart[0], aStart[1], aStart[2]);
        var dEnd = new Date(aEnd[0], aEnd[1], aEnd[2]);

        if(dStart > dEnd){
            fEnd.val(fStart.val());
        }
    }
</script>
 <!-- <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker(
                	{
           
            format: 'YYYY-MM-DD',


        });
            });
            $(function () {
                $('#datetimepicker2').datetimepicker(
                	{
           
            format: 'YYYY-MM-DD',
           
        });
            });
 </script> -->


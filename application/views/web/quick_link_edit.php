<?php
    $readonly = isset($id) ? '' : '';
    $disabled = isset($id) ? '' : '';
?>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <?php echo form_open_multipart($path . $class . '/' . (isset($id) ? 'update/' . $id : 'create'), array('id' => 'form_data')) ?>
                    <div class="col-sm-11">
                        <div class="row bord-bottom">
                            <label for="id" class="col-sm-4">Name</label>
                            <div class="col-sm-8">
                                <input name="name" value="<?= $data['name'] ?>" required=""  pattern=".*\S+.*" class="form-control input-sm" placeholder="fill the name of menu" id="id" <?= $readonly ?>>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Link</label>
                            <div class="col-sm-8">
                                <input name="link" type="url"  pattern=".*\S+.*" placeholder="Fill the link of menu" value="<?= $data['link'] ?>" required="" class="form-control input-sm" id="name" <?= $readonly ?>>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" name="description" pattern=".*\S+.*" placeholder="Describe menu here..."><?= $data['description'] ?></textarea>
                            </div>
                        </div>
                        <div class="row bord-bottom">
                            <label for="name" class="col-sm-4">Icon</label>
                            <div class="col-sm-8">
                                 <?= form_dropdown('icon', $a_icon, $data['icon'] ? $data['icon'] : '', 'id="icon" class="form-control input-sm select2 icon" style="border: 1px solid; width:100%;"'); ?>
                            </div>
                            
                        </div>
                        
                        <?php if (isset($id)) { ?>
                            <div class="row bord-bottom">
                                <label for="account number" class="col-sm-4">Status</label>
                                <div class="col-sm-8">
                                    <select class="form-control select2" name="status">
                                        <option <?php if($data['status'] == 1){echo "selected";} ?> value="1">Active</option>
                                        <option <?php if($data['status'] == 0){echo "selected";} ?> value="0">Nonactive</option>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-md-11 text-right">
                        <a class="post-right"><button type="submit" class="btn btn-success btn-sm post-footer-btn">Save</button></a>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $.fn.datepicker.defaults.format = "dd-M-yyyy";

    function formatIcon(icon) {
        var originalOption = icon.element;
        console.log(icon);
        return $("<span><i class='icon-io-" + icon.text + "'></i>&ensp;"+icon.text+"</span>");
    }

    $(".icon").select2({
        placeholder: 'Pilih icon...',
        minimumResultsForSearch: Infinity,
        templateSelection: formatIcon,
        templateResult: formatIcon,
    });



    $(document).ready(function() {
        $("#eselon_code").select2({
            placeholder: 'Choose Group of Position...',
            width: '100%'
        });
    });

    function goAdd() {
        location.href = "<?php echo site_url($path . $class . '/add') ?>";
    }
    
    function goBack() {
        location.href = "<?php echo site_url($path . $class) ?>";
    }

    function goSave() {
        $("#form_data").submit();
    }

    function getPositionName(){        
        var jabatan_id = document.getElementById("jabatan_id").value;
        
        $.ajax({
            url : "<?php echo site_url('/web/jabatan/ajaxGetPosition');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'id':jabatan_id},
            success:function(respon){
                $("#div_position_name").removeClass("hidden");
                if (respon.position==null || respon.position==false){
                    document.getElementById("jabatan_name").value = "Not Found!";
                } else {
                    document.getElementById("jabatan_name").value = respon.position;    
                }

                $("#div_unitkerja_name").removeClass("hidden");
                if (respon.unitkerja==null || respon.unitkerja==false){
                    document.getElementById("unitkerja_name").value = "Not Found!";
                } else {
                    document.getElementById("unitkerja_name").value = respon.unitkerja;    
                }

                $("#div_company_name").removeClass("hidden");
                if (respon.company==null || respon.company==false){
                    document.getElementById("company_name").value = "Not Found!";
                } else {
                    document.getElementById("company_name").value = respon.company;    
                }

            } 
        });
    }

    function checkEmail(){        
        var email = document.getElementById("email").value;
        
        $.ajax({
            url : "<?php echo site_url('/web/karyawan/ajaxCheckEmail');?>",
            type: 'post',
            dataType: 'JSON',
            data: {'email':email},
            success:function(respon){
                if (respon=='0'){
                    $("#label_email_available").removeClass("hidden");
                    $("#label_email_exist").addClass("hidden");
                    $("#label_email_warning").addClass("hidden");
                } else if (respon=='1'){
                    $("#label_email_available").addClass("hidden");
                    $("#label_email_exist").removeClass("hidden");
                    $("#label_email_warning").addClass("hidden");
                } else {
                    $("#label_email_available").addClass("hidden");
                    $("#label_email_exist").addClass("hidden");
                    $("#label_email_warning").removeClass("hidden");
                }
            } 
        });
    }



    

</script>
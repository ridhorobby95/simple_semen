<div id="reject_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title text-center" style="font-family: Source Sans Pro, sans-serif; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">
                    Rejection
                </h4>
                <p class="sub-one-revisi">Write Your Rejection Reason</p>
            </div>
            <div class="modal-body">
            	<form action="<?= site_url('web/document/setReason').'/'.$data['document_id'] ?>" method="post">
            		<h4 for="reason_textarea">Reason</h4>
                    <br>
					<textarea id="reason_textarea" placeholder="Write Reason Here..." name="reason_textarea" style="height:100px;width:100%" required></textarea>
					<button type="submit" id="submit_reject" name="submit_reject" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
				</form>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1-1" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_reject_modal" class="btn btn-sm btn-danger btn-rad" onClick="javascript:set_urutan(<?= $data['document_id'].',' ?><?= $data['workflow_urutan'].',' ?> 'C')"><i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Reject</span></button>
            </div>
        </div>
    </div>
</div>

<div id="cancel_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title text-center" style="font-family: Source Sans Pro, sans-serif; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">
                    Canceletion
                </h4>
                <p class="sub-one-revisi">Write Your Cancel Reason</p>
            </div>
            <div class="modal-body">
                <form action="<?= site_url('web/document/setReason').'/'.$data['document_id'] ?>" method="post">
                    <h4 for="reason_textarea">Reason</h4>
                    <br>
                    <textarea id="cancel_reason_textarea" placeholder="Write Reason Here..." name="reason_textarea" style="height:100px;width:100%" required></textarea>
                    <button type="submit" id="submit_reject" name="submit_reject" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
                </form>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1-1" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_cancel_modal" class="btn btn-sm btn-danger btn-rad" onClick="javascript:set_cancel(<?= $data['document_id'].',' ?> 'S')"><i class="fa fa-times" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Cancel Evaluation</span></button>
            </div>
        </div>
    </div>
</div>

<div id="cancel_box_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <h4 id="RevisionTitle" class="modal-title text-center" style="font-family: Source Sans Pro, sans-serif; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">
                    Canceletion
                </h4>
                <p class="sub-one-revisi">Cancel Reason</p>
            </div>
            <div class="modal-body">
                <center><b><p id="cancel_text"></p></b></center>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button type="button" id="btn_cancel_ok" class="btn btn-sm btn-success btn-rad"><i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Ok</span></button>
            </div>
        </div>
    </div>
</div>

<div id="delegate_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Delegate</h4>
                <p class="sub-one-revisi">Choose the person to delegate</p>
            </div>
            <div class="modal-body">
                <div class="col-md-9" style="/*width:50%;*/height:100%;padding-right:0px;padding-left: 0px;float: left;">
                    <label class="col-md-2 control-label" for="delegate">Delegate</label>
                    <select id="delegate" name="delegate" class="form-control input-sm" style="width:60%; " onchange="" >
                        <?php
                            foreach($delegates as $delegate) {
                                echo "<option value='".$delegate['user_id']."'>".$delegate['name']."</option>";
                            }
                        ?>
                    </select>
                    <br>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger tombolShareFix1" data-dismiss="modal" onClick="javascript:delegate(<?= $data['document_id'].','.$data['workflow_urutan'].','.$data['workflow_id'] ?>)">Delegate</button>
            </div>
        </div>
    </div>
</div>

<div id="subordinate_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Delegate</h4>
                <p class="sub-one-revisi">Choose the person to delegate</p>
            </div>
            <div class="modal-body">
                <div class="col-md-9" style="/*width:50%;*/height:100%;padding-right:0px;padding-left: 0px;float: left;">
                    <label class="col-md-2 control-label" for="subordinate_delegate">Delegate</label>
                    <select id="subordinate_delegate" name="subordinate_delegate" class="form-control input-sm select2" style="width:80%; " onchange="" >
                        <?php
                            foreach($subordinates as $key => $subordinate) {
                                echo "<option value='".$subordinate['user_id']."'>".$subordinate['name']."</option>";
                            }
                        ?>
                    </select>
                    <br>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger tombolShareFix1" data-dismiss="modal" onClick="javascript:subordinate_delegate(<?= $data['document_id'].','.$data['workflow_urutan'].','.$data['workflow_id'] ?>)">Delegate</button>
            </div>
        </div>
    </div>
</div>

<div id="add_reviewer_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="add_reviewer_title" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Add Reviewer</h4>
                <p class="sub-one-revisi">Write reviewers name to add</p>
            </div>
            <div class="modal-body">
                <form action="<?= site_url('web/document/update').'/'.$data['document_id'] ?>" class="" method="post" id="form_add_reviewer">
                    <input type="hidden" id="is_continue_reviewer" value='1'>
                    <!-- Reviewer -->
                    <div style="margin-bottom: 5px">
                        <label class="col-md-2 control-label" for="input_reviewer">Reviewer</label>
                        <?php echo form_dropdown('input_reviewer[]', $variables['reviewer'], $data['reviewer'], 'id="input_reviewer" class="form-control input-sm select2 input_reviewer" style="width:100%;margin-bottom: 5px" multiple'); ?>
                        <button type="submit" id="submit_add_reviewer" name="submit_add_reviewer" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
                   </div>
                   <!-- Already Review  -->
                   <div style="margin-bottom: 5px">
                        <label class="col-md-2 control-label" for="input_reviewer"><b>Reviewer (already review)</b></label>
                        <?php echo form_dropdown('input_reviewer_reviewed[]', $variables['reviewed'], $data['reviewed'], 'id="input_reviewer_reviewed" class="form-control input-sm select2 input_reviewer_reviewed" style="width:100%;margin-bottom: 5px" multiple disabled'); ?>
                   </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_add_reviewer_modal" class="btn btn-danger tombolShareFix1" data-dismiss="modal" onClick="javascript:add_reviewer(<?= $data['document_id'].','.$data['workflow_id'] ?>)">Add Reviewer</button>
            </div>
        </div>
    </div>
</div>

<div id="revise_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Ask for Revision</h4>
                <p class="sub-one-revisi">You're Asking the drafter to revise, please write Revise Reasons below</p>
            </div>

            <div class="modal-body">
            	<form action="<?= site_url('web/document/setReason').'/'.$data['document_id'] ?>" method="post">

                    <h4 for="revise_reason_textarea">Reason</h4>
                    <br>
					<textarea id="revise_reason_textarea" placeholder="Write Reason Here..." name="revise_reason_textarea" style="height:100px;width:100%" required></textarea>
					<button type="submit" id="submit_revise" name="submit_revise" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
				</form>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal" style="margin: 0px;">Cancel</button>
                <?php $back = 1;
                    if($drafter_evaluation != null && $data['workflow_id'] == 2){
                        $back = 0;
                    } ?>
            	<button type="button" id="btn_revise_modal" class="btn btn-sm btn-warning btn-rad" onClick="javascript:set_urutan(<?= $data['document_id'].','.$back.',' ?>'R')"> <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                        <span>Revise</span></button>
            </div>
        </div>
    </div>
</div>

<div id="approve_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <center>
        <div class="modal-content" style="background-color: #fff;width:50%">
            <div class="modal-header" style="background-color: #fff;border:0px">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Approval</h4>
                <p class="sub-one-revisi">Are you sure to approve this document ?</p>
            </div>
            <div class="modal-footer" style="background-color: #fff;border:0px">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal" style="margin: 0px;padding: 6px 12px">Cancel</button>
                <button type="button" id="btn_approve_modal" class="btn btn-sm btn-success btn-rad" onClick="javascript:set_urutan(<?= $data['document_id'].','.$urutan.','.$approved_status ?>)"> <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Approve</span>
                </button>
            </div>
        </div>
        </center>
    </div>
</div>

<div id="evaluation_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <center>
        <div class="modal-content" style="background-color: #fff;width:50%">
            <div class="modal-header" style="background-color: #fff;border:0px">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="EvaluationTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Approval</h4>
                <p class="sub-one-revisi">Are you sure to submit this evaluation form ?</p>
            </div>
            <div class="modal-footer" style="background-color: #fff;border:0px">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal" style="margin: 0px;padding: 6px 12px">Cancel</button>
                <button type="button" id="btn_submit_evaluation" class="btn btn-sm btn-success btn-rad" onClick="javascript:submit_evaluation(<?= $data['document_id'].','.$urutan.','.$approved_status ?>)"> <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Submit</span>
                </button>
            </div>
        </div>
        </center>
    </div>
</div>


<div id="submit_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <center>
        <div class="modal-content" style="background-color: #fff;width:50%">
            <div class="modal-header" style="background-color: #fff;border:0px">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Submission</h4>
                <p class="sub-one-revisi">Are you sure to submit this document ?</p>
            </div>
            <div class="modal-footer" style="background-color: #fff;border:0px">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal" style="margin: 0px;padding: 6px 12px">Cancel</button>
                <button type="button" id="btn_submit_modal" class="btn btn-sm btn-success btn-rad" onClick="javascript:continue_form(<?= $data['document_id'].','.$data['is_upload'] ?>)"> <i class="fa fa-check" aria-hidden="true" style="margin-right: 10px;font-size: 16px;"></i>
                    <span>Submit</span>
                </button>
            </div>
        </div>
        </center>
    </div>
</div>

<div id="upload_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="RevisionTitle" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Update Document</h4>
                <p class="sub-one-revisi">Update Document</p>
            </div>
            <div class="modal-body row">
                <?php if (SessionManagerWeb::getUserID()!=$data['user_id']){
                    $not_me = $data['user_id'];
                } else {
                    $not_me = NULL;
                }?>
                <div class="row col-md-12" id="alertFileExceeds" style="display: none;width:103%;margin-left: 0%;margin-bottom: 0.5%">
                    <div class="row" style="margin: 0px;">
                        <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                            <div class="col-md-10" style="">
                            <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                                <strong style="color:#b71c1c;">
                                    Warning!
                                </strong>
                                <label id="alertMessageNewDoc" style="color:#b71c1c;">
                                    Maximum file limit of 20MB Exceeded, please upload file less than 20MB!
                                </label>
                            </div>
                            </div>
                            <div class="col-md-2 text-right" id="hide_alertFileExceeds" style="">
                                    <h4>
                                        <i class="fa fa-close new-close" ></i>
                                    </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-md-12" id="alertForNewDoc" style="display: none;width:100%;padding: 0px;margin-left: 0%;margin-bottom: 0.5%">
                    <div class="row" style="margin: 0px;">
                        <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                            <div class="col-md-10" style="">
                            <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                                <strong style="color:#b71c1c;">
                                    Warning!
                                </strong>
                                <label id="alertMessageNewDoc" style="color:#b71c1c;">
                                    You have already uploaded this document!
                                </label>
                            </div>
                            </div>
                            <div class="col-md-2 text-right" id="hide_alertForNewDoc" style="">
                                    <h4>
                                        <i class="fa fa-close new-close" ></i>
                                    </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row col-md-12" id="alertExcelNotPDF" style="display: none;width:100%;padding: 0px;margin-left: 0%;margin-bottom: 0.5%">
                    <div class="row" style="margin: 0px;">
                        <div class="col-sm-12" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                            <div class="col-md-10" style="">
                            <div class="alert" style="width: 100%;background-color: #ffcdd2;margin-bottom: 0px;">
                                <strong style="color:#b71c1c;">
                                    Warning!
                                </strong>
                                <label id="alertMessageExcelNotPDF" style="color:#b71c1c;">
                                    You must upload a PDF File!
                                </label>
                            </div>
                            </div>
                            <div class="col-md-2 text-right" id="hide_alertExcelNotPDF" style="">
                                    <h4>
                                        <i class="fa fa-close new-close" ></i>
                                    </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <form class="col-md-12 dropzone" action="<?php echo site_url('web/document/documentform'); ?>" align="center"  id="dropbox" style="border: 5px dotted #03A9F4;padding: 7px 25%;height:10em;">
                     <div class="col-sm-12" style="">
                        <div id="containerDropbox" style="display:inline;">
                            <div class="col-sm-12 dz-message" data-dz-message style="margin: 20px;">
                                <i class='fa fa-copy' aria-hidden='true' style='font-size: 50px; color:#03A9F4; float:middle;'></i>
                                <h5 align="center">
                                    <b>
                                        Click to Choose or Drop File Here
                                    </b>
                                </h5>
                                <div class="row">
                                    <div id="files_preview" name="files_preview" class="col-md-12" align="center"></div>
                                </div>
                            </div>
                        </div>
                        <div id="scanContainer2" style="display:none;">
                            <img id="dwtcontrolContainer2" src="" style="margin-top:5px;">
                        </div>
                    </div>
                </form>
                <form class="col-md-6 dropzone hidden" action="<?php echo site_url('web/document/documentformbm'); ?>" align="center"  id="dropboxexcel" style="border: 5px dotted #03A9F4;padding: 7px 5%;height:10em;">
                     <div class="col-sm-12" style="">
                        <div id="containerDropboxexcel" style="display:inline;">
                            <div class="col-sm-12 dz-message" data-dz-message style="margin: 20px;">
                                <i class='fa fa-copy' aria-hidden='true' style='font-size: 50px; color:#03A9F4; float:middle;'></i>
                                <h5 align="center">
                                    <b>
                                        Click to Choose or Drop File Here
                                    </b>
                                </h5>
                                <div class="row">
                                    <div id="files_previewexcel" name="files_previewexcel" class="col-md-12" align="center"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="background-color: #fff;margin-top: 10px">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_upload_modal" class="btn btn-sm btn-secondary btn-rad" onClick="javascript:update_upload_document(<?= $data['document_id'] ?>)"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>
                    <span>Update Document</span></button>
            </div>
        </div>
    </div>
</div>

<div id="verification_modal" class="modal fade">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content" style="background-color: #fff;">
            <div class="modal-header" style="background-color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                <h4 id="VerificationDocument" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Document Verification Form</h4>
                <p class="sub-one-revisi">Document Verification Form</p>
            </div>
            <div class="modal-body">
                <form action="<?= site_url('web/document/SetDocumentVerification').'/'.$data['document_id'] ?>" method="post" id="form_verification">
                    <table id="verification_table" style="width:100%" toolbar="#toolbar" >
                        <thead>
                            <tr>
                                <th field="verification_name" width="35%" style="text-align: center" >Document Verification</th>
                                <th field="is_appropriate" width="10%" style="text-align: center" >Appropriate? </th>
                                <th field="comment" width="55%" style="text-align: center">Comment</th>
                            </tr>
                        </thead>
                        <?php 
                            $is_form = '';
                            $value = $data['verification']['template']['is_appropriate'];
                            if ($type['id']==$type['type_form']){
                                $value=1;
                                $is_form = 'disabled';
                            }
                        ?>
                        <tr>
                            <td>
                                <label for="input_verification_template">Standar Penulisan Sesuai Template</label>
                            </td>
                            <td>
                                <?php 
                                    echo form_dropdown('select_verification_template', array(0=>"NO", 1=>"YES"), $value, 'id="select_verification_template" class="form-control input-sm" style="margin-bottom: 5px" '.$is_form); ?>
                            </td>
                            <td>
                                <textarea id="input_verification_template" name="input_verification_tugas" value="<?= $data['verification']['template']['text']  ?>" style="margin-left:10px;width:100%" <?= $is_form ?>><?= $data['verification']['template']['text']  ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="select_verification_tugas">Kesesuaian Terhadap Tugas Pokok Fungsi</label>
                            </td>
                            <td>
                                <?php 
                                    echo form_dropdown('select_verification_tugas', array(0=>"NO", 1=>"YES"), $data['verification']['tugas']['is_appropriate'], 'id="select_verification_tugas" class="form-control input-sm" style="margin-bottom: 5px"'); ?>
                            </td>
                            <td>
                                <textarea id="input_verification_tugas" name="input_verification_tugas" value="<?= $data['verification']['tugas']['text']  ?>"  style="margin-left:10px;width:100%"><?= $data['verification']['tugas']['text']  ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="input_verification_penulisan">Keselarasan Penulisan Pada Dokumen</label>
                            </td>
                            <td>
                                <?php 
                                    echo form_dropdown('select_verification_penulisan',array(0=>"NO", 1=>"YES"), $data['verification']['penulisan']['is_appropriate'], 'id="select_verification_penulisan" class="form-control input-sm" style="margin-bottom: 5px"'); ?>
                            </td>
                            <td>
                                <textarea id="input_verification_penulisan" name="input_verification_penulisan" value="<?= $data['verification']['penulisan']['text']  ?>"  style="margin-left:10px;width:100%"><?= $data['verification']['penulisan']['text']  ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="input_verification_perubahan">Rincian Perubahan Dokumen</label>
                            </td>
                            <td colspan="2">
                                <table id="table_perubahan" class="" cellspacing="0" width="100%" border="1">
                                    <thead>
                                        <tr>
                                            <th>BAB</th>
                                            <th>KONDISI LAMA</th>
                                            <th>KONDISI BARU</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $x=0;
                                        foreach($data['verification']['perubahan']['text'] as $value) {
                                        ?>
                                            <tr id="tr_<?= $x ?>">
                                                <td><textarea name='bab[]' value="<?= $value['bab'] ?>"><?= $value['bab'] ?></textarea></td>
                                                <td><textarea name='old[]' value="<?= $value['old'] ?>"><?= $value['old'] ?></textarea></td>
                                                <td><textarea name='new[]' value="<?= $value['new'] ?>"><?= $value['new'] ?></textarea></td>
                                                <td>
                                                    <button type="button" class="btn btn-xs btn-danger" onclick="delete_column_perubahan(<?= $x ?>)">
                                                        <i class="fa fa-times">
                                                        </i>
                                                    </button>
                                                </td>
                                            </tr>
                                        <?php 
                                            $x++;
                                        } 
                                        ?>
                                    </tbody>

                                </table>
                                <br>
                                <input id="id_tr" type="text" class="hidden" value="<?= count($data['verification']['perubahan']['text']) ?>">
                                <button type="button" id="add_row_perubahan" onclick="add_column_perubahan()" class="btn btn-xs btn-success pull-right" style="margin-bottom: 5px"><i class="fa fa-plus"></i> Add Data</button>
                            </td>
                        </tr>
                    </table>                    
                    <button type="submit" id="submit_verify" name="submit_verify" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
                </form>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1" data-dismiss="modal">Cancel</button>
                <button type="button" id="btn_save_modal" class="btn btn-danger tombolShareFix1" data-dismiss="modal" onClick="javascript:verify(<?= $data['document_id'] ?>)">Save Form</button>
            </div>
        </div>
    </div>
</div>

<?php if ($is_allow_compare) {?>
    <div id="compare_modal" class="modal fade">
        <div class="modal-dialog" style="width:1250px !important">
            <div class="modal-content" style="background-color: #fff;">
                <div class="modal-header" style="background-color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="display:inline; float:right;color:#000;font-size:20px;margin-right:20px;">&times;</button>
                    <h4 id="VerificationDocument" class="modal-title" style="font-family: Nunito Sans, Muli, Lato, Roboto, sans-serif; margin:10px 0px 10px 0px; letter-spacing:1px; color:#9E9E9E;text-align: center; font-weight: bold;">Compare Documents</h4>
                    <p class="sub-one-revisi">Compare Document with previous version</p>
                </div>
                <div class="modal-body">
                    <div id="viewDocContent" class="overlay-content" style="left:0%;width: 600px;height:600px; margin:0px;margin-bottom: 30px;  display: inline;float: left" scrolling="no">
                        <center>
                            <label style="text-align: center;font-weight: bold">Previous Document</label>
                        </center>
                        <iframe name="viewerOld" id="viewerOld" width='100%' height='100%' allowfullscreen webkitallowfullscreen style="min-width:600px;min-height:600px;max-width:600px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
                    </div>
                    <div id="viewDocContentNew" class="overlay-content" style="left:0%;width: 600px;height:600px; margin:0px; margin-bottom: 30px;  display: inline;float: right" scrolling="no">
                        <center>
                            <label style="text-align: center;font-weight: bold;color:blue;">Current Document</label>
                        </center>
                        <?php
                        if ($data['is_upload']=='0') {
                        ?>
                            <form id="form_create_document" class="<?= $hide_editor ?>" style="margin-bottom: 10px">
                                <textarea id="document_edit_textarea_compare" style="margin-right:10px;height:600px"></textarea>
                                <button type="submit" class="btn btn-success hidden" style="margin-top:10px">Submit</button>
                            </form>
                        <?php } else { ?>
                            <?php if ($data['ext']=='pdf') { ?>
                                <iframe name="viewerNew" id="viewerNew" width='100%' height='100px%' allowfullscreen webkitallowfullscreen style="min-width:600px;min-height:600px;max-width:600px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
                            <?php } else { ?>
                                <iframe name="viewerNew" id="viewerNew" frameborder="0" scrolling="no" seamless style="min-width:600px;min-height:600px;max-width:600px;max-height:600px;float:middle;padding-top:0px;padding-bottom: 15px;"></iframe>
                                <!-- <div style="width: 120px; height: 80px; position: absolute; right: 0px; top: 0px;">&nbsp;</div> -->
                                <div style="width: 90%;height: 6%;position: absolute;right: 0%;top: 13%;background-color: white;">&nbsp;<i style="font-size:10px;color:red">*NB : do not fill the header and footer</i></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: #fff;">
                    <button id="closeShareBox" type="button" class="btn col-md-3 tombolShare1 pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div id="set_attribute_modal" class="modal fade" style="overflow-y:scroll;">
    <div class="modal-dialog2">
        <div class="modal-content row col-sm-12" style="padding: 0px; background-color: transparent;">
            <form method="post" name="set_attibute_form" id="set_attribute_form" action="<?= site_url('web/document/update').'/'.$data['document_id'] ?>">
                <div class="row" style="margin: 0px; background-color: #fff;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                    <div class="col-sm-6" style="background-color: #fff;">
                        <tr>
                            <br>
                            <i class="fa fa-file" style="font-size: 20px;padding-left: 3%; color: #03A9F4;">
                                <h4 class="modal-title" style="display:inline; color:#757575;font-size:18px;font-weight:bold; color:#424242;">
                                    Detail Document
                                </h4>
                            </i>
                        </tr>
                        <?php 
                            $is_required = '';
                            if ($is_revise){
                                $is_required = 'required';
                            }
                        ?>
                        <tr>
                            <div class="modal-body row" style="margin-bottom: 0%;margin-top: 0px;padding-bottom: 0px;">
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="judul">Title </label>
                                        <?php echo form_input(array('name'=>'title', 'value' => $data['title'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Title'))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="kodeAdministratif">Code </label>
                                        <?php echo form_input(array('id'=>'kodeAdministratif','name'=>'kodeAdministratif','value' => $data['code'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Code', 'readonly'=>''))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;"> 
                                        <label for="jenisIns" >Document Type</label>
                                        <input type="hidden" value="<?= $data['type_id'] ?>" id="types_id">
                                        <?php echo form_input(array('jenisIns', 'value' => $data['type_name'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Type', 'disabled'=>''))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="myunitkerja" >Work Unit</label>
                                        <input type="hidden" value="<?= $data['unitkerja_id'] ?>" id="myunitkerja_id">
                                        <?php echo form_input(array( 'value' => $data['unitkerja_name'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Work Unit', 'disabled'=>''))?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="prosesbisnis" >Business Process</label>
                                        <div style="width: 60%; float: right;">
                                            <?php if ($is_revise or 1==1){ ?>
                                                <?php echo form_input(array( 'value' => $data['prosesbisnis_name'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Work Unit', 'disabled'=>'', 'style' => 'width:100%'))?>    
                                            <?php } else { ?>
                                                
                                                    <!-- <select id="prosesbisnis" name="prosesbisnis" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" onChange="changeCode()" required>
                                                        <option></option>
                                                        <?php
                                                            // foreach($variables['prosesbisnis'] as $key => $obj){
                                                            //     echo "<option value='".$key."'>".$obj."</option>";
                                                            // }
                                                        ?>
                                                   </select> -->
                                                   <?php echo form_dropdown('prosesbisnis', $variables['prosesbisnis'], $data['prosesbisnis_id'], 'id="prosesbisnis" class="form-control input-sm select2 prosesbisnis" onChange="changeCode()" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required'); ?>
                                                
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="input_iso" >Requirement</label>
                                        <div style="width: 60%; float: right;">
                                            <?php echo form_dropdown('input_iso[]', $variables['document_iso'], $data['iso'], 'id="input_iso" class="form-control input-sm select2 attribute_requirement" onChange="javascript:getKlausul(0)" style="margin-bottom: 5px;width:100%" multiple'); ?>
                                            
                                            <?php 
                                            if (1==0){ ?>
                                            <select id="input_iso" name="input_iso[]" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" onChange="javascript:getKlausul()" required>
                                                <?php
                                                    foreach($variables['isos'] as $key => $obj){
                                                        echo "<option value=\"".$obj['id']."\"'>".$obj['iso']."</option>";
                                                    }
                                                ?>
                                           </select>
                                           <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" id="div_klausul" style="padding: 0px 20px;">
                                        <label for="input_iso_klausul" >Clausuls</label>
                                        <?php if ($type['klausul']) {?>
                                            <div style="width: 60%;float: right;">
                                               <?php echo form_dropdown('input_iso_klausul[]', $variables['klausul'], $data['klausul'], 'id="input_iso_klausul" class="form-control input-sm select2 attribute_clause" style="width:100%;margin-bottom: 5px" multiple'); ?>
                                            </div>
                                        <?php } else { ?>
                                            <?php echo form_input(array( 'value' => 'Clausuls not needed', 'class' => 'form-control fcontrolsize', 'placeholder' => 'Clausuls', 'disabled'=>''))?>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 1%">  
                                    <div class="form-group" id="div_retension" style="padding: 0px 20px;">
                                        <label for="retension_input" >Retention Period</label>
                                        <?php if ($type['retension']=='1'){ ?>
                                        <!-- <div style="width: 100%;float: right;"> -->
                                            <?php// echo form_input(array('name' => 'retension_input', 'value' => '', 'class' => 'form-control input-sm', 'id' => 'retension_input', 'placeholder' => 'Retention Period', 'style' => 'width:55%;display:inline' ))?>
                                            <!-- <select id="retension_select" name="retension_select" class="form-control input-sm" style="width:40%; font-size: 12px; letter-spacing: 0.5px;display:inline">
                                                <option value="0">Bulan</option>
                                                <option value="1">Tahun</option>
                                           </select> -->
                                           <?php echo form_input(array( 'value' => $data['retension'].' Bulan', 'class' => 'form-control fcontrolsize', 'placeholder' => 'Retention Period', 'disabled'=>''))?>
                                        <!-- </div> -->
                                        <?php } else { ?>
                                            <?php echo form_input(array( 'value' => 'Retention Period not needed', 'class' => 'form-control fcontrolsize', 'placeholder' => 'Retention Period', 'disabled'=>''))?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 1%">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="deskripsi" >Change Log</label>
                                        <div style="width: 60%;float: right;">
                                            <textarea class="form-control input-sm" placeholder="Document Change Log" id='input_deskripsi' name='input_deskripsi' value="<?= $data['description'] ?>" rows='5' style='height:70px;margin-bottom:10px;max-width: 100%;' <?= $is_required ?> ><?= $value['description'] ?></textarea>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </tr>                    
                    </div>

                    <div class="col-sm-6" >
                        <tr>
                            <br>
                            <i class="fa fa-user" style="font-size: 20px;padding-left: 3%; color: #03A9F4;">
                                <h4 class="modal-title" style="display:inline; color:#757575;font-size:18px;font-weight:bold; color:#424242;">
                                    Related Parties
                                </h4>
                            </i>
                        </tr>
                        <tr>
                            <div class="modal-body row" style="margin-bottom: 0%;margin-top: 0px;">
                                <div class="row">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="reviewer" >Reviewers</label>
                                        <?php if ($type['need_reviewer']=='1'){?>
                                            <div style="width: 60%; float: right;">
                                                <?php if (1==0){?>
                                                <select id="reviewer" name="reviewer[]" class="form-control input-sm select2 reviewer" multiple="multiple" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required>
                                                    <option></option>
                                                    <?php
                                                        foreach($variables['reviewer'] as $key => $obj){
                                                            echo "<option value='".$key."'>".$obj."</option>";
                                                        }
                                                    ?>
                                               </select>
                                               <?php } ?>
                                               <?php echo form_dropdown('input_reviewer[]', $variables['reviewer'], $data['reviewer'], 'id="input_reviewer" class="form-control input-sm select2 input_reviewer2" style="width:100%;margin-bottom: 5px" multiple'); ?>
                                            </div>
                                        <?php } else { ?>
                                            <?php echo form_input(array( 'value' => 'Reviewer not needed', 'class' => 'form-control fcontrolsize', 'placeholder' => 'Work Unit', 'disabled'=>''))?>
                                        <?php } ?>
                                    </div>
                                    
                                </div>
                                
                                <div class="row" style="margin-top:5px">
                                    <?php if ($type['need_reviewer']=='1') {?>
                                        <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                            <label for="reviewed" >Reviewers <br>(already review)</label>
                                            <div style="width: 60%; float: right;">
                                               <?php echo form_dropdown('input_reviewer_reviewed[]', $variables['reviewed'], $data['reviewed'], 'id="input_reviewer_reviewed" required class="form-control input-sm select2 input_reviewer_reviewed2" style="width:100%;margin-bottom: 5px" multiple disabled'); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="row" style="margin-top:5px">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="creator_id" >Creator</label>
                                        <div style="width:60%;float: right;">
                                            <?php if (1==0){ ?>
                                            <?php echo form_dropdown('creator_id',$creators, $data['creator_id'], 'id="creator_id" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;" required'); ?>
                                            <?php } ?>
                                            <?php echo form_input(array( 'value' => $data['creator_name'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Creator Name', 'disabled'=>'', 'style'=>"width:100%"))?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="approver_id">Approver</label>
                                        <div style="width: 60%;float: right;" >
                                            <?php 
                                                if ($data['workflow_id']==1 and $data['workflow_urutan']==4){
                                                    echo form_dropdown('approver_id', $approvers, $data['approver_id'], 'id="approver_id" class="form-control input-sm select2 show_approver_id attribute_approver" style="width:100%; font-size: 12px; letter-spacing: 0.5px;"');
                                                } else {
                                                    echo form_dropdown('approver_id',null, null, 'id="approver_name_modal" class="form-control input-sm select2 show_approver_id attribute_approver" style="width:100%; font-size: 12px; letter-spacing: 0.5px;"');
                                                    // echo form_dropdown('approver_id', );
                                                    // echo form_input(array('approver_id', 'value' => $data['approver_id'], 'class' => 'form-control fcontrolsize hidden', 'placeholder' => 'Approver Name', 'style'=>"width:100%"));
                                                    // echo form_input(array( 'value' => $data['approver_name'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Approver Name', 'style'=>"width:100%"));
                                                }
                                            ?>

                                            <?php 
                                                // backup
                                                if (1==0){?>
                                                <?php echo form_dropdown('approver_id',$approvers, $data['approver_id'], 'id="approver_id" class="form-control input-sm select2" style="width:100%; font-size: 12px; letter-spacing: 0.5px;"'); ?>
                                                <?php echo form_input(array( 'value' => $data['approver_name'], 'class' => 'form-control fcontrolsize', 'placeholder' => 'Approver Name', 'disabled'=>'', 'style'=>"width:100%"))?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group" style="padding: 0px 20px;">
                                        <label for="unit_kerja_terkait" >Related Work Unit</label>
                                        <div style="width: 60%;float:right;">
                                            <?php echo form_dropdown('input_unitkerjaterkait[]',$variables['unitkerja'], $data['related_workunit'], 'id="input_unitkerjaterkait" class="form-control input-sm select2 input_unitkerjaterkait attribute_related_workunit" multiple style="width:100%; font-size: 12px; letter-spacing: 0.5px;"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 5px">
                                    <div class="form-group" style="margin-bottom: 15px;padding: 0px 20px;">
                                        <label for="DokumenTerkait" >Related Document</label>
                                        <div id="listDokumenTerkait" style="width: 60%;float: right;">
                                            <button type="button" class="btn tombolBarui" onClick="openModalRelatedForUpload()" style="padding-left:14px;padding-right:13px;font-weight: 1px; margin-bottom:8px;font-size: 12px; letter-spacing: 1px; margin-top: 0px;">Add Related Document</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>


                    </div>
                    <div class="row col-sm-12" style="margin: 0px;padding: 10px 10px; background-color: transparent;">
                        <div class="row" style="margin: 0px;">
                                <div class="row col-sm-12" style="padding: 0px;margin: 0px;">
                                    <button type="button" id="btn_save_attribute" class="btn btn-sm btn-success btn-rad pull-right tombolBarui22" style="padding-left:25px;padding-right:25px; margin-right: 10px; letter-spacing: 1px; ">Save Attribute
                                    </button>
                                    <button type="button" class="btn btn-sm btn-danger btn-rad pull-right tombolBarui" data-dismiss="modal" style="margin: 0px; margin-right: 10px; letter-spacing: 1px;">Cancel
                                    </button>&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                <button id="btn_submit_form_attribute" class="btn btn-info col-sm-12" style="display:none;"></button>
            </form>
        </div>
    </div>
</div>

<script>
var related_terpilih = [];
$(window).load(function() {
    $('#input_iso').select2({
        placeholder:'Requirement',
        width:"100%"
    });
    $("#input_iso_klausul").select2({
        placeholder: 'Klausul',
        minimumResultsForSearch: Infinity
    });
    $("#reviewer").select2({
        placeholder: 'Write Reviewer Name'
    });
    $("#input_unitkerjaterkait").select2({
        placeholder: 'Write related work unit'
    });
    $(".show_approver_id").select2({
        placeholder: 'Choose Approver'
    });
    $(".attribute_clause").select2({
        placeholder: 'Choose Clause',
        width: '100%'
    });
});

$("#btn_save_attribute").on('click', function(){
    $("#btn_submit_form_attribute").click();
});

$("#myunitkerja").on('change',function(){
    var unitkerja_id = document.getElementById('myunitkerja').value;
    $.ajax({
        url : '<?= site_url('web/unitkerja/ajaxGetCreator') ?>/'+unitkerja_id,
        type: 'post',
        cache: false,
        data: {},
        success: function(respon){
            response = JSON.parse(respon);
            var type = document.getElementById('jenisIns').value;
            $("#creator_id").select2().html('');
            $.ajax({
                url : '<?= site_url('web/unitkerja/ajaxGetListCreator') ?>/'+unitkerja_id,
                type: 'post',
                cache: false,
                data: {'type':type},
                success: function(resp){
                    json_resp = JSON.parse(resp);
                    $("#creator_id").select2({
                        placeholder : 'Choose creator',
                        data:json_resp 
                    });
                    $('#creator_id').val(response.creator.user_id).trigger('change');
                }
            });
        }
    });
});

$("#creator_id").on('change',function(){
    var types_id = document.getElementById("jenisIns").value;
    // var unitkerja_id = document.getElementById('myunitkerja').value;
    var creator_id = document.getElementById('creator_id').value;
    $.ajax({
        url : '<?= site_url('web/unitkerja/ajaxGetApprover') ?>/',
        type: 'post',
        cache: false,
        data: {"creator_id":creator_id, "type_id":types_id},
        success: function(respon){
            response = JSON.parse(respon);
            $("#approver_id").select2().html('');
            $("#approver_id").select2({
                data:response.approver,
                placeholder : 'Choose Approver'
            });
            $('#approver_id').val(response.approver[0].id).trigger('change');
        }
    });
});

$("#jenisIns").on('change', function(){
    var types_id = document.getElementById("jenisIns").value;
    $.ajax({
        url : '<?= site_url('web/document_types/ajaxGetDetail') ?>',
        type: 'post',
        cache: false,
        data: {"id":types_id},
        success: function(respon){
            response = JSON.parse(respon);

            if (response.need_reviewer==0){
                $("#reviewer").val(null).trigger("change"); 
                document.getElementById("reviewer").setAttribute('disabled', true);
            } else {
                document.getElementById("reviewer").removeAttribute('disabled');
            }

            if (response.retension=='0'){
                $("#div_retension").addClass('hidden');
                document.getElementById("retension_input").value = '';
            } else {
                $("#div_retension").removeClass('hidden');
            }

            if (response.klausul==0){
                $("#input_iso_klausul").val(null).trigger("change"); 
                $("#div_klausul").addClass('hidden');
            } else {
                $("#div_klausul").removeClass('hidden');
            }
            document.getElementById("myunitkerja").removeAttribute('disabled');
            var unitkerja_id = document.getElementById('myunitkerja').value;
            if (unitkerja_id!=null && unitkerja_id!=''){
                var type = document.getElementById('jenisIns').value;
                $("#creator_id").select2().html('');
                $.ajax({
                    url : '<?= site_url('web/unitkerja/ajaxGetCreator') ?>/'+unitkerja_id,
                    type: 'post',
                    cache: false,
                    data: {},
                    success: function(respon){
                        response = JSON.parse(respon);
                        var type = document.getElementById('jenisIns').value;
                        $("#creator_id").select2().html('');
                        $.ajax({
                            url : '<?= site_url('web/unitkerja/ajaxGetListCreator') ?>/'+unitkerja_id,
                            type: 'post',
                            cache: false,
                            data: {'type':type},
                            success: function(resp){
                                json_resp = JSON.parse(resp);
                                
                                $("#creator_id").select2({
                                    placeholder : 'Choose creator',
                                    data:json_resp 
                                });
                                $('#creator_id').val(response.creator.user_id).trigger('change');
                            }
                        });
                    }
                });
            } 
        }
    });
});

var index = 0;
var isUpload = false;
function addNewInput(){
    var parent = document.getElementById("listDokumenTerkait");
    var newInput = document.createElement("input");
    newInput.id = "related" + index;
    newInput.name = "related[]";
    newInput.type = "text";
    newInput.className = "form-control";
    newInput.style = "display: inline;font-size: 12px;margin-bottom:8px;";
    parent.appendChild(newInput);
    $( "#related" + index ).autocomplete({
        source: "<?php echo site_url('web/document/allDocument'); ?>",
        minLength: 1,
        select: function( event, ui ) {
        },
        open: function (event, ui) {
            $(".ui-autocomplete li.ui-menu-item:odd a").addClass("ui-menu-item-alternate");
            $(".ui-autocomplete li.ui-menu-item:hover a").addClass("ui-state-hover");
        },
        close: function (event, ui) {
            },
             messages: {
                    noResults: '',
                    results: function() {}
                }            
        })
     .data( "ui-autocomplete" )._renderItem = function( ul, item ) {
            return $( "<div>" )
            .data( "ui-autocomplete-item", item )
            .append( "<a style='margin-left:5px;'>"+ item.label+ "</a>" )
            .appendTo( ul );
        };
    index++;
}

function openModalRelatedForUpload(){
    var document_id = <?php echo $data['document_id'] ?>;
    console.log(related_terpilih);
    var listID = [];
    var temp = 0;
    isUpload = true;
    $("#RelatedBox").modal('show');
    document.getElementById('RelatedTitle').textContent = "Related Document";
    $("#listDokumenTerkait > input").each(function() {
      listID[temp]= $(this).attr("value") ;
      temp++;
    });
    var doc = get_data_related;
    var size = doc.length;
    var inner = "";
    var tableR = document.getElementById('tabelRelated');
    // tableR.innerHTML = ""
    $("#data").remove();
    inner += "<tbody id='data'>";
    for(var i=0;i<size;i++){
        var id = doc[i].DOCUMENT2;
        // related_terpilih.push(id);
        var title = doc[i].TITLE;
        var type = doc[i].TYPE_NAME;
        var unitkerja = doc[i].UNITKERJA;
        inner += "<tr>";
        inner += "<td style='display:none;'>" + id+ "</td>";
        inner += "<td style='display:none;'>true</td>";
        inner += "<td id='titleRelated' name='titleRelated[]'>" + title + "</td>";
        inner += "<td>" + type  + "</td>";
        inner += "<td>" + unitkerja + "</td>";
        inner += "<td><button class='btn btn-success' onClick='viewDocument(" + id +"," + title + ")'><i class='fa fa-eye' aria-hidden='true' style='float:middle;'></i></button></td>";
        if($.inArray(id, related_terpilih) !== -1){
            inner += "<td><input type='checkbox' name='vehicle' value='Terkait' checked></td>";
        }
        else{
            inner += "<td><input type='checkbox' name='vehicle' value='Terkait'></td>";
        }
        // inner += "<td><input type='checkbox' name='vehicle' value='Terkait' checked></td>";
        inner += "</tr>";
    }
    inner+= "</tbody>";
    tableR.innerHTML += inner;
    // var http = new XMLHttpRequest();
    // http.open("POST", "<?php echo site_url('/web/document/getChoosenDocument/');?>", true);
    // http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    // var param = "listid=" + JSON.stringify(document_id);
    // http.send(param);
    // http.onload = function() {
    //     var doc = JSON.parse(http.responseText);
    //     var size = doc.length;
    //     var inner = "";
    //     var tableR = document.getElementById('tabelRelated');
    //     var lenRow = tableR.rows.length;
    //     var ins = 0;
    //     for(var i=1;i<lenRow;i++){
    //         tableR.deleteRow(1);
    //     }
    //     for(var i=0;i<size;i++){
    //         var id = doc[i].DOCUMENT2;
    //         // related_terpilih.push(id);
    //         var title = doc[i].TITLE;
    //         var type = doc[i].TYPE_NAME;
    //         var unitkerja = doc[i].UNITKERJA;
    //         // inner += "<tr>";
    //         // inner += "<td style='display:none;'>" + id+ "</td>";
    //         // inner += "<td style='display:none;'>true</td>";
    //         // inner += "<td id='titleRelated' name='titleRelated[]'>" + title + "</td>";
    //         // inner += "<td>" + type  + "</td>";
    //         // inner += "<td>" + unitkerja + "</td>";
    //         // inner += "<td><button class='btn btn-success' onClick='viewDocument(" + id +"," + title + ")'><i class='fa fa-eye' aria-hidden='true' style='float:middle;'></i></button></td>";
    //         // inner += "<td><input type='checkbox' name='vehicle' value='Terkait' checked></td>";
    //         // inner += "</tr>";
    //     }
    //     tableR.innerHTML += inner;
    // }
    document.getElementById('simpanRelated').onclick = pilihRelated;
}

function pilihRelated(){
    related_terpilih=[];
    var tableR = document.getElementById('tabelRelated');
    var lenRow = tableR.rows.length;
    var parent = document.getElementById("listDokumenTerkait");
    while (parent.childElementCount>1) {
        parent.removeChild(parent.lastChild);
    }
    for(var y=1;y<lenRow;y++){
        var checked = $('input', tableR.rows[y].cells[6]).is(':checked');
        var id = tableR.rows[y].cells[0].innerHTML;
        var title = tableR.rows[y].cells[2].innerHTML;
         if(checked){
            var newInput = document.createElement("input");
            newInput.id = "related";
            newInput.name = "related[]";
            newInput.type = "hidden";
            newInput.value = id;
            related_terpilih.push(id);
            newInput.className = "pilihRelated";
            var newLabel = document.createElement("label");
            newLabel.innerHTML = title;
            newLabel.className = "form-control";
            newLabel.style = "border-style:ridge;font-size: 12px;margin-bottom:8px;padding-left:3px;";
            parent.appendChild(newInput);
            parent.appendChild(newLabel);
        }
    }
    // console.log(related_terpilih);
    $("#RelatedBox").modal('show'); 
}

function changeCode(){
    var types_id = document.getElementById("types_id").value;
    var prosesbisnis_id = document.getElementById("prosesbisnis").value;
    var unitkerja_id = document.getElementById("myunitkerja_id").value;
    var form_type = document.getElementById("form_type").value;
    $.ajax({
        url : '<?= site_url('web/document/ajaxgetcode') ?>',
        type: 'post',
        cache: false,
        data: {"type_id":types_id, "prosesbisnis_id":prosesbisnis_id, "unitkerja_id":unitkerja_id, "form_type": form_type},
        success: function(respon){
            document.getElementById("kodeAdministratif").value = respon;
            
        }
    });
}

function getKlausul(is_reset_klausul=1){
    var iso_id = $('#input_iso').val();
    $.ajax({
        url : "<?php echo site_url('/web/document_iso_klausul/ajaxGetKlausul');?>",
        type: 'post',
        dataType: 'JSON',
        data: {'iso_id':iso_id},
        success:function(respon){
            var klausul = $.map(respon, function (obj) {
                obj.id = obj.id || obj.id; // replace id_kab with your identifier
                obj.text = obj.text || obj.klausul; // replace nama with your identifier
                return obj;
            });
            if (is_reset_klausul==1){
                $(".input_iso_klausul").html('');
            }
            
            $(".input_iso_klausul").select2({
                placeholder: "Klausul",
                data : klausul,
                minimumResultsForSearch: Infinity
            });
        } 
    });
}

</script>

<script>
        // $(document).ready(function() {
        //     var t = $('#example').DataTable();
        //     var counter = 1;
         
        //     $('#addRow').on( 'click', function () {
        //         t.row.add( [
        //             counter +'.1',
        //             counter +'.2',
        //             counter +'.3',
        //             counter +'.4',
        //             counter +'.5'
        //         ] ).draw( false );
         
        //         counter++;
        //     } );
         
        //     // Automatically add a first row of data
        //     $('#addRow').click();
        // } );
</script>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:5%">Number</th>
                                        <th class="text-center">Business Process</th>
                                        <th class="text-center">Code</th>
                                        <th class="text-center" style="width:15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    foreach ($data as $v) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td class="text-right"><?= $no ?></td>
                                            <td class="text-center">
                                                <?= $v['prosesbisnis'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $v['code'] ?>
                                            </td>
                                            <td class="text-center">
                                                <?php
                                                echo anchor('web/document_prosesbisnis/edit/' . $v['id'], '<i class="fa fa-pencil"></i>', 'class="text-success" data-toggle="tooltip" title="Edit"') . '&nbsp;&nbsp;';
                                                // echo anchor('web/document/edit/' . $v['id'], '<i class="fa fa-trash"></i>', 'class="text-danger" data-toggle="tooltip"  title="Hapus"');
                                                ?>
                                                <a href="javascript:hapus(<?php echo $v['id'] ?>,'<?php echo $v['prosesbisnis'] ?>')" class="text-danger" title="Delete Business Process">
                                                    <i class="fa fa-trash"></i>
                                                </a> 
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <form action="<?= site_url('web/document_prosesbisnis/add') ?>" class="col-sm-12 col-md-12" style="position:relative;left:1.5%;bottom:1%;padding-bottom: 10px" method="post">
                    <input type="text" name="prosesbisnis" id="prosesbisnis" placeholder="Business Process Name" class="form-control" style="width:300px;display:inline" required>
                    <input type="text" name="code" id="code" placeholder="Business Process Code" class="form-control" style="width:150px;display:inline" required>
                    <input type="submit" class="btn btn-sm btn-primary" id="btn_tambah_jenis" style="display:inline" value="Add Business Process">
                </form>
            </div>
        </div>

    </div>
</div>
<script>

    function hapus(id,name) {
        if (confirm('Hapus proses bisnis "'+name+'" ?')){
            location.href = "<?= site_url('web/document_prosesbisnis/delete') ?>/"+id;
        }
    }

</script>
<?php echo xPageTitle($page_title) ?>

<div class="section-main">
	
	<div class="well">
		<a href="<?= site_url('web/document/dashboard')?>"><span class="btn btn-sm btn-info"><i class="glyphicon glyphicon-home"></i> Home</span></a>
		<span class="btn btn-sm btn-info" onclick="history.back()"><i class="glyphicon glyphicon-chevron-left"></i> Kembali</span>
		
	</div>
	
	<h3 class="alert alert-danger">


	<?php
		if ($message)
			echo $message;
		else
			echo 'Anda tidak mempunya hak mengakses halaman ini';
	?>

	
	</h3>
		
	<br/><br/>
</div>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Share_model extends AppModel {

    public function getShare($id){
        $sql = "select * from document_shares where id=$id";
        $res = dbGetRows($sql);
        return $res;
    }

    public function getShareDocument($idDocument){
    	$sql = "select * from document_shares where document_id=$idDocument";
    	$res = dbGetRows($sql);
    	return $res;
	}

    public function getAllShareDocument($idDocument){
        $sql = "select * from document_shares where document_id=$idDocument";
        $res = dbGetRows($sql);
        return $res;
    }

	public function getShareDocumentByKode($kode){
    	$sql = "select * from document_shares where kode='$kode'";
    	$res = dbGetRows($sql);
    	return $res;
	}

	public function notUniqueKode($kode){
		$sql = "select * from document_shares where kode='$kode'";
    	$res = dbGetRows($sql);
    	if($res)
    		return false;
    	else
    		return true;
	}

    public function updateShare($post_id,$data){
        $date = new DateTime();
        $data["updated_at"] =  $date->format('Y-m-d H:i:s');
        dbUpdate("document_shares",$data,"id='$post_id'");
    }

    public function create($data) {
        $date = new DateTime();
        $data['created_at'] =  $date->format('Y-m-d H:i:s');
        $data['updated_at'] =  $date->format('Y-m-d H:i:s');
        dbInsert('document_shares',$data);
    }

}

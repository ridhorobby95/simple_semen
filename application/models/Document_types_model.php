<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_types_model extends AppModel {

    // Eselon
    const ESELON_SATU = 10;
    const ESELON_DUA = 20;
    const ESELON_TIGA = 30;
    const ESELON_EMPAT = 40;
    const ESELON_LIMA = 50;

    const BOARD_MANUAL = 1;
    const PEDOMAN =2;
    const PROSEDUR = 3;
    const INSTRUKSI_KERJA = 4;    
    const FORM = 5;

    public function __construct() {
        parent::__construct();
    }

    protected $_table = "DOCUMENT_TYPES";
    protected $_order = 'urutan';
    protected $_filter = "";
    protected $_columns = array(
        "id" => "\"id\"",
        "type" => "\"type\"",
        "code" => "\"code\"",
        "need_reviewer" => "\"need_reviewer\"",
        "retension" => "\"retension\"",
        "klausul" => "\"klausul\""
    );
    protected $_with = "";

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }


    public function with($table, $condition){
        // $table_name = $table['name'];
        // $table_initial = $table['initital'];
        $table_with = $table['name'].' '.$table['initial'];
        $this->_with = " left join $table_with on $condition ";
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_columns = $column;
        return $this;
    }

    public function filter($filter=NULL){
        if ($filter!=NULL){
            $this->_filter = ' where '.$filter; 
        }
        return $this;
    }

    // Mengambil seluruh nilai klasifikasi
    public function getAllJenis($filterAccess=NULL){
    	$sql = "select id as \"id\", type as \"type\", code as \"code\", created_at as \"created_at\", updated_at as \"updated_at\", urutan as \"urutan\", eselon_drafter as \"eselon_drafter\", eselon_reviewer as \"eselon_reviewer\", eselon_creator as \"eselon_creator\", eselon_approver as \"eselon_approver\" from document_types where is_delete!=1 $filterAccess order by $this->_order";
        // die($sql);
    	$klas = dbGetRows($sql);
    	return $klas;
    }

    public function getBy($id, $column=NULL){
        $sql = "select $column as \"$column\" from document_types where id=$id";
        $column = dbGetOne($sql);
        return $column;
    }

    public function get($id){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table where id=$id";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($column){
        $sql = "select $column from $this->_table $this->_filter";
        $col = dbGetOne($sql);
        return $col;
    }

    public function getJenisBy($param=NULL){
        if ($param!=NULL) {
            $filter = " and id in (".$param.")";
        } else {
            return NULL;
        }
        $sql = "select id as \"id\", type as \"type\", created_at as \"created_at\", updated_at as \"updated_at\", code as \"code\", need_reviewer as \"need_reviewer\", retension as \"retension\", klausul as \"klausul\", urutan as \"urutan\", eselon_drafter as \"eselon_drafter\", eselon_reviewer as \"eselon_reviewer\", eselon_creator as \"eselon_creator\", eselon_approver as \"eselon_approver\" from document_types where is_delete!=1 $filter order by urutan";
        $klas = dbGetRows($sql);
        return $klas;
    }

    public function getDocumentType($id=null){
        if ($id==null)
            return false;
        $sql = "select id as \"id\", type as \"type\", created_at as \"created_at\", updated_at as \"updated_at\", code as \"code\", need_reviewer as \"need_reviewer\", retension as \"retension\", klausul as \"klausul\", urutan as \"urutan\", eselon_drafter as \"eselon_drafter\", eselon_reviewer as \"eselon_reviewer\", eselon_creator as \"eselon_creator\", eselon_approver as \"eselon_approver\" from document_types where is_delete!=1 and id=$id order by $this->_order";
        $klas = dbGetRow($sql);
        return $klas;
    }

    function add($name) {
        $record = array();
        $record['TYPE'] = $name;
        $sql = "select count(1) from document_types";
        $jml = dbGetOne($sql);
        $record['URUTAN'] = $jml+1;
        dbInsert('DOCUMENT_TYPES', $record);
    }

    public function insertJenis($nama){
        $date = new DateTime();
        $created =  $date->format('Y-m-d H:i:s');
        $updated =  $date->format('Y-m-d H:i:s');
        $sql = "insert into document_types(type,created_at,updated_at) values('$nama','$created','$updated')";
        dbQuery($sql);
    }

    public function updateJenis($type_id,$data){
        dbUpdate("document_types",$data,"id=$type_id");
    }

    public function cekSameName($id,$nama){
        $nama = strtolower($nama);
        $sql = "select * from document_types where lower(type) = '$nama' and id!=$id";
        $row = dbGetRows($sql);
        if($row){
            return true;
        }else{
            return false;
        }
    }

    public function getAllJenisAPI(){
        $sql = "select id as kode_jenis ,type from document_types where is_delete=0";
        $klas = dbGetRows($sql);
        return $klas;
    }
}

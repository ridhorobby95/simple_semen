<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends AppModel {
    
    //RANK
    const RANK_KOPRAL = 1;
    const RANK_SERSAN = 2;
    const RANK_LETNAN = 3;
    const RANK_KAPTEN = 4;
    const RANK_MAYOR = 5;
    const RANK_KOLONEL = 6;
    const RANK_LETJEN = 7;
    const RANK_JENDRAL = 8;

    private $_is_role = FALSE;
    private $_is_private = FALSE;

    /**
     * Digunakan untuk menampilkan data yang akan dikembalikan dari model 
     * Jika semua ditampilkan tidak perlu ditambahkan attribute $soft_data pada model
     * 
     * @var array 
     */
    protected $soft_data = array('id', 'name', 'username', 'photo', 'email', 'gender', 'ym', 'phone', 'address', 'account_number', 'role', 'status', 'created_at', 'updated_at');
    private $public_data = array('id', 'name', 'username', 'photo', 'email', 'gender', 'ym', 'phone', 'address');
    protected $_columns = '*';
    protected $_filter = '';
    protected $_order = " order by \"name\" ";
    protected $__table = 'users_view';
    protected $_show_sql =false;

    // protected $_columns = array();
    /**
     * Digunakan untuk relasi antar model
     * 
     * @var array 
     */
    protected $has_many = array(
        'Post',
        'Comment',
        'Group_member',
        'Post_user',
        'Post_tag_user',
        'Notification',
        'User_token',
        'Schedule',
        'Daily_report_user'
    );

    /**
     * Digunakan untuk mengcustom nama mapper
     * Jika penamaan standard (user_name => userName) tidak perlu ditambahkan pada attribute $mapper pada model
     * 
     * @var array 
     */
    protected $mapper = array(
        'password_reset_token' => 'resetToken',
    );

    /**
     * Digunakan untuk labeling jika label (user_name => User Name) hanya menyertakan nama fieldnya saja.
     * @var array 
     */
    protected $label = array();

    /**
     * Digunakan untuk validasi create dan update data
     * 
     * @var array 
     */
    protected $validation = array(
        'username' => 'required|min_length[3]|max_length[50]|max_length[50]|valid_email',
        'email' => 'required|max_length[50]|valid_email',
        'birth_place' => 'alpha_dash',
        'related_phone' => 'integer',
        'name' => 'required',
        'ym' => 'integer'
    );

    private function _getColumn(){
        if ($this->_columns=='*')
            return '*';
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function columns($columns=''){
        $this->_columns = $columns;
        return $this;
    }

    public function filter($filter=''){
        $this->_filter = $filter;
        return $this;
    }

    public function is_role($role = FALSE) {
        $this->_is_role = $role;
        return $this;
    }

    public function is_private($private = FALSE) {
        $this->_is_private = $private;
        return $this;
    }

    public function order($order=''){
        $this->_order = $order;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getRole($role) {
        $roleUser = array(
            'id' => $role,
            'name' => Role::name($role),
            'permissions' => Role::getPermissions($role)
        );
        return $roleUser;
    }

    public function getName($user_id){
        $sql = "select name from users u where u.id=$user_id";
        $name = dbGetRow($sql);
        return $name;
    }

    public function mapper($row) {
        $newRow = array();
        foreach ($row as $key => $val) {
            //MAPPER ATRIBUTE
            if (is_null($this->soft_data) || in_array($key, $this->soft_data) || $this->show_all || $this->_is_private) {
                $newKey = key_exists($key, $this->mapper) ? $this->mapper[$key] : camelize($key);
                $newRow[$newKey] = $val;
            }
        }
        return $newRow;
    }

    function getUserID($nama){
        $sql = "select * from users where name='$nama'";
        $rows = dbGetRows($sql);
        if($rows!=false)
            return $rows[0]['id'];
        else
            return "0";
    }

    protected function property($row) {
        if ($this->_is_role || $this->_is_private) {
            $row['role'] = $this->getRole($row['role']);
        } else {
            foreach ($row as $key => $val) {
                if (!in_array($key, $this->public_data) && !$this->show_all) {
                    unset($row[$key]);
                }
            }
        }
        if (isset($row['photo'])) {
            $row['photo'] = Image::getImage($row['id'], $row['photo'], 'users/photos');
        }
        return $row;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $this->_database->trans_begin();
        $create = parent::create($data, $skip_validation, $return);
        if ($create) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $upload = Image::upload('photo', $create, $_FILES['photo']['name'], 'users/photos');
            }
            if ($upload === TRUE) {
                $this->_database->trans_commit();
                if($return)
                    return $create;
                else
                    return true;
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        if (array_key_exists('password', $data) || array_key_exists('password_salt', $data)) {
            unset($data['password'], $data['password_salt']);
        }
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $upload = Image::upload('photo', $primary_value, $_FILES['photo']['name'], 'users/photos');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    // ACTIVE RECORD
    public function getListForAR($userId) {
        return $this->order_by('name', 'ASC')->get_many_by(array('id !=' => $userId, 'status' => Status::ACTIVE));
    }

    public function getListFor($user_id) {
        $status = Status::ACTIVE;
        $sql = "select * from users where id!=$user_id and status='$status' order by name";
        $rows = dbGetRows($sql);
        foreach ($rows as $key => $row) {
            $row['photo'] = Image::getimage($row['id'], $row['photo'], 'users/photos');

            $rows[$key]=$row;
        }
        return $rows;
    }

    public static function getRank() {
        return array(
            self::RANK_KOPRAL => 'Kopral',
            self::RANK_SERSAN => 'Sersan',
            self::RANK_LETNAN => 'Letnan',
            self::RANK_KAPTEN => 'Kapten',
            self::RANK_MAYOR => 'Mayor',
            self::RANK_KOLONEL => 'Kolonel',
            self::RANK_LETJEN => 'Letjen',
            self::RANK_JENDRAL => 'Jendral'
        );
    }
    
    public function getListPresence() {
        $date = date('Y-m-d H:i:s');
        
        $sql = "select distinct user_id from presence_log
                where scan_date::date = current_date";
        // $rows = $this->_database->query($sql)->result_array();
        $rows = dbGetRows($sql);
        
        $data = array();
        foreach($rows as $row)
            $data[$row['user_id']] = true;
        
        return $data;
    }

    public function getAll($filter=NULL){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->__table $filter $this->_order";
        if ($this->_show_sql){
            die($sql);
        }
        $rows = dbGetRows($sql);
        return $rows;        
    }

    public function getAllUsers(){
        $sql = "select u.id, u.username, u.name, u.email, u.ym, u.photo, u.gender, u.phone, u.address 
            from users u 
            order by name";
        $users = dbGetRows($sql);
        foreach ($users as $key => $user) {
            $user['photo'] = Image::getImage($user['id'], $user['photo'], 'users/photos');
            $users[$key] = $user;
        }
        return $users;
    }

    public function getAllUsersWithRole($role=FALSE, $condition=NULL, $limit=20, $offset=0){
        // $this->_is_role = $role;
        $sql = "select * from (select * from users_view u $condition order by \"name\")";
        if ($limit) {
            $sql .= " where rownum< $limit ";
        }
        $users = dbGetRows($sql);
        $this->load->model('unitkerja_model');
        foreach ($users as $key => $user) {
            $user['company_name'] = $this->unitkerja_model->getOne(str_pad($user['company'], 8, '0', STR_PAD_LEFT), 'name');
            $user['photo'] = Image::getImage($user['id'], $user['photo'], 'users/photos');
            if ($role==TRUE){
                $user['role'] = $this->getRole($user['role']);
            }
            
            $users[$key] = $user;
        }
        return $users;
    }

    public function getUser($id=NULL){
        if ($id==NULL)
            return NULL;
        $sql = "select u.id as \"id\", u.username as \"username\", u.name as \"name\", u.email as \"email\", u.photo as \"photo\"
                from users u where id=$id";
        $data = dbGetRow($sql);
        $data['photo'] = Image::getImage($id, $data['photo'], 'users/photos');
        return $data;
    }

    public function getSystemPhoto(){
        $photo = array(
            'original' => array(
                            'link' => site_url('assets/web/img/logo-simple.jpeg')
                            ),
            'small' => array(
                            'link' => site_url('assets/web/img/logo-simple.jpeg')
                        ),
            'medium' => array(
                            'link' => site_url('assets/web/img/logo-simple.jpeg')
                        ),
            'thumb' => array(
                            'link' => site_url('assets/web/img/logo-simple.jpeg')
                        )
        );
        return $photo;
    }

    public function getUserByUsername($username){
        $sql = "select u.id as \"id\", u.username as \"username\" , u.name  as \"name\", u.email as \"email\", u.ym as \"ym\", u.photo as \"photo\", u.gender as \"gender\", u.phone as \"phone\", u.address as \"address\"
                from users u where username='$username' order by name";
        $data = dbGetRow($sql);
        if ($data) {
            $data['photo'] = Image::getImage($data['id'], $data['photo'], 'users/photos');
        }
        
        return $data;
    }

    public function isAdministrator($user_id){
        $sql = "select 1 from users u where id='$user_id' and role in ('".Role::ADMINISTRATOR."')";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function isManagement($user_id){
        $sql = "select 1 from users u where id='$user_id' and role in ('".Role::MANAGEMENT."')";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function isEmployee($user_id){
        $sql = "select 1 from users u where id='$user_id' and role in ('".Role::EMPLOYEE."')";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function isLeader($user_id){
        $sql = "select 1 from users u where id='$user_id' and role in ('".Role::LEADER."')";
        if (dbGetOne($sql))
            return true;
        return false;
    }

    public function getJabatan($user_id=NULL){
        $sql = "select u.username as \"username\", u.name as \"name\", u.email as \"email\", u.role as \"role\",
                 j.name as \"jabatan\", j.subgroup_name as \"jabatan_group\" from users u 
                left join karyawan k on k.id=u.karyawan_id
                left join jabatan j on j.id=k.jabatan_id where u.id='$user_id'";
        $user = dbGetRow($sql);
        if ($user['jabatan']==NULL)
            $user['jabatan'] = 'Tanpa Jabatan';
        if ($user['jabatan_group']==NULL)
            $user['jabatan_group'] = 'Tidak Ada';
        return $user;
    }

    public function getBy($user_id=NULL, $column = NULL){
        // $sql = "select uk.* from users_view uv
        //         left join unitkerja uk on uk.id=uv.\"company\" where \"user_id\"=$user_id";
        $sql = "select \"$column\" from users_view where \"user_id\"='$user_id' ";
        if ($this->_show_sql){
            die($sql);
        }
        $company =dbGetOne($sql);
        return $company;
    }

    public function getOne($user_id, $col){
        $sql = "select \"$col\" from users_view where \"user_id\"='$user_id' $this->_filter";
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col){
        $sql = "select \"$col\" from users_view $this->_filter ";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }

    public function getSystemUser(){
        $sql = "select id from users where upper(username)='USER20' ";
        return dbGetOne($sql);
    }

    public function getByFilter($col){
        $sql = "select \"$col\" from $this->__table $this->_filter";
        $rows = dbGetOne($sql);
        return $rows;     
    }

    public function getByColumn($column, $value){
        $sql = "select * from users where $column='$value'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function isExistUsername($username=NULL){
        $sql = "select id as \"user_id\" from users where lower(username)=lower('$username') ";
        $id = dbGetOne($sql);
        return $id;
    }

    public function getByAll($user_id){
        $sql = "select * from users_view where  \"user_id\"='$user_id' ";
        $user = dbGetRow($sql);
        return $user;
    }

    public function getAllBy($filter=NULL){
        $sql = "select \"user_id\", \"name\" from users_view where $filter order by \"name\" ";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getJabatanGroup($user_id=NULL){
        $sql = "select j.subgroup_id as \"subgroup_id\", j.subgroup_name as \"subgroup_name\" from users u 
                left join karyawan k on k.id=u.karyawan_id 
                left join jabatan j on j.id=k.jabatan_id where u.id='$user_id'";
        $user = dbGetRow($sql);
        if ($user['subgroup_id']==NULL){
            $user['subgroup_id'] = '70';
            $user['subgroup_name'] = 'Non-Eselon';
        }
        return $user;
    }

    public function getEselon($user_id=NULL){
        $sql = "select k.eselon_code as \"subgroup_id\", k.eselon_name as \"subgroup_name\" from users u 
                left join karyawan k on k.id=u.karyawan_id 
                where u.id='$user_id'";
        $user = dbGetRow($sql);
        if ($user['subgroup_id']==NULL){
            $user['subgroup_id'] = '70';
            $user['subgroup_name'] = 'Non-Eselon';
        } else {
            $sql = "select subgroup_name from jabatan where subgroup_id='".$user['subgroup_id']."' ";
            $user['subgroup_name'] = dbGetOne($sql);
        }
        return $user;
    }



    public function getDelegateTo($unitkerja_ids){
        $sql ="select \"user_id\", \"name\" from users_view where \"unitkerja_id\" in $unitkerja_ids and \"is_chief\" is null";
        $delegates = dbGetRows($sql);
        // $data = array();
        // foreach ($delegates as $key => $delegate) {
        //     $data[$delegate['user_id']] = $delegate['name'];
        // }
        return $delegates;
    }

    public function getChiefByRole($role, $company='2000'){
        $sql = " select \"user_id\" from users_view where \"is_chief\"='X' and \"role\"='$role' and \"is_active\"=1 and \"company\"='$company' ";
        $row = dbGetOne($sql);
        return $row;

    }

    public function isChief($user_id){
        $is_chief = self::getBy($user_id, 'is_chief');
        $return_is_chief = false;
        // if ($is_chief=='X' or SessionManagerWeb::isAdministrator()) {
        if ($is_chief=='X') {
            $return_is_chief = true;
        }
        return $return_is_chief;
    }

    public function getDocumentController($company, $only_chief=0){
        $filter = '';
        if ($only_chief){
            $filter =" and \"is_chief\"='X' ";
        }
        $sql = " select \"user_id\" from users_view where \"company\"='$company' and \"role\"='".Role::DOCUMENT_CONTROLLER."' $filter";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getUserIDfromNopeg($nopeg){
        $sql = "select u.id from karyawan k left join users u on u.email=k.email where k.id='$nopeg'";
        return dbGetOne($sql);
    }

    public function getUnitKerjaFromUser($user_id){
        $sql = "select u.\"unitkerja_id\",k.name, k.\"LEVEL\", k.parent from users_view u left join unitkerja k on u.\"unitkerja_id\" = k.\"ID\" where u.\"user_id\"='$user_id' ";
        $column = dbGetRow($sql);
        return $column;
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_reviewer_model extends AppModel {

    protected $_table='DOCUMENT_REVIEWERS';
    protected $_order = 'order by document_reviewer';
    protected $_columns = array(
        "document_id" => "\"document_id\"",
        "document_reviewer" => "\"document_reviewer\""
    );
    protected $_filter = '';
    protected $_join = '';

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function order($order='order by document_reviewer'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function join($join){
        $this->_join=$join;
        return $this;
    }

    public function create($document_id, $workflow_id=1, $data) {
        foreach ($data as $key => $value) {
            // $sql = "select 1 from users_view where \"user_id\"=$value and \"role\"='R'";
            // if (dbGetOne($sql)){
                $dr['document_id'] = $document_id;
                $dr['document_reviewer'] = $value;
                $dr['workflow_id']=$workflow_id;
                dbInsert($this->_table,$dr);
            // }   
        }
    }

    public function add($document_id, $user_id){
        $dr['document_id'] = $document_id;
        $dr['document_reviewer'] = $user_id;
        dbInsert($this->_table,$dr);
    }

    public function deleteWith($document_id, $workflow_id=1){
        $sql ="delete from document_reviewers where document_id=$document_id and workflow_id=$workflow_id";
        return dbQuery($sql);
    }

    public function getAll($document_id, $workflow_id=1){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table where document_id=$document_id and workflow_id=$workflow_id $this->_order";
        $rows = dbGetRows($sql);
        $this->load->model('User_model');
        foreach ($rows as $key => $row) {
            $name = $this->User_model->getBy($row['document_reviewer'],'name');
            $actor_name .= '- '.$name;
            $actor_name .= '<br>';
        }
        return $actor_name;
    }

    public function getAllBy($document_id, $workflow_id=1){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table where document_id=$document_id and workflow_id=$workflow_id $this->_order";
        $rows = dbGetRows($sql);
        if ($rows){
            $reviewers = array();
            foreach ($rows as $key => $row) {
                $reviewers[] = $row['document_reviewer'];
            }
        }
        return $reviewers;
    }

    public function getAllFilter($show_sql=false){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table $this->_join $this->_filter $this->_order ";
        if ($show_sql){
            die($sql);
        }
        return dbGetRows($sql);
    }

    public function getBy(){

    }

    public function getOne($document_id, $document_reviewer){
        $sql = "select 1 from document_reviewer where document_id=$document_id and document_reviewer=$document_reviewer";
        return dbGetOne($sql);
    }

    public function getDocumentReviewers($document_id, $workflow){
         $sql = "select * from document_reviewers  where document_id=$document_id and workflow_id=$workflow";
        return dbGetRow($sql);
    }

}
?>
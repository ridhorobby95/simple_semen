<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Device_model extends AppModel {

    protected $has_many = array(
        'User_token'
    );
    protected $label = array(
        'secure_id' => 'ID Device',
        'name' => 'Device',
    );
    protected $validation = array(
        'secure_id' => 'required|is_unique[devices.secure_id]',
    );

    public function getDeviceUser($users) {
        $this->_database->select('devices.*');
        $this->_database->join('user_tokens', 'user_tokens.device_id=devices.id');
        $this->_database->join('users', 'user_tokens.user_id=users.id');
        $this->_database->group_by('devices.id');
        return $this->get_many_by(array(
                    'user_tokens.status' => AuthManager::STATUS_LOGIN,
//                    'user_tokens.token_expired_time >=' => Util::timeNow(),
                    'users.status' => 'A',
                    'users.id' => $users,
                    'devices.reg_id IS NOT NULL'
        ));
    }

}

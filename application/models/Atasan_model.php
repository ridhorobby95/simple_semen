<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Atasan_model extends AppModel {
    protected $_table = 'atasan';
    protected $_limit = 20;
    protected $_order = 'k_nopeg';

    protected $_columns = array(
        "K_NOPEG" => "\"k_nopeg\"",
        "K_UNITKERJA_ID" => "\"k_unitkerja_id\"",
        "UNITKERJA_ID" => "\"unitkerja_id\"",
        "JABATAN_ID" => "\"jabatan_id\"",
        "COMPANY" => "\"company\"",
        "ATASAN1_LEVEL" => "\"atasan1_level\"",
        "ATASAN1_UNITKERJA" => "\"atasan1_unitkerja\"",
        "ATASAN1_JABATAN" => "\"atasan1_jabatan\"",
        "ATASAN1_NOPEG" => "\"atasan1_nopeg\"",
        "ATASAN1_PGS" => "\"atasan1_pgs\"",
        "ATASAN2_LEVEL" => "\"atasan2_level\"",
        "ATASAN2_UNITKERJA" => "\"atasan2_unitkerja\"",
        "ATASAN2_JABAT" => "\"atasan2_jabatan\"",
        "ATASAN2_NOPEG" => "\"atasan2_nopeg\"",
        "ATASAN2_PGS" => "\"atasan2_pgs\"",
        "CHANGED_ON" => "\"changed_on\""
    );
    protected $_filter = '';

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getSubordinates($nopeg){
        if (in_array($nopeg, $_SESSION['all_bawahan'])){
            return NULL;
        }
        $_SESSION['all_bawahan'][] = $nopeg;
        $sql = "select k_nopeg as nopeg, atasan1_nopeg, atasan2_nopeg
                from atasan
                where atasan1_nopeg='$nopeg' or atasan2_nopeg='$nopeg'";
        $rows = dbGetRows($sql);
        $subordinates = array();
        foreach ($rows as $row) {
            $subordinates[$row['NOPEG']] = $row['NOPEG'];
            $sub = $this->getSubordinates($row['NOPEG']);
            if ($sub!=NULL){
                $subordinates += $sub;
            }
        }
        return $subordinates;
    }

    public function getDelegates($subordinates, $eselon){
        $delegates = array();
        foreach ($subordinates as $value) {
            $sql = " select * from users_view where \"karyawan_id\"='$value' and \"eselon_code\"<=$eselon ";
            $delegate = dbGetRow($sql);
            if ($delegate!=NULL and $delegate!=false){
                $delegates[] = $delegate;
            }
        }
        return $delegates;
    }   

    public function delete($id){
        $sql = "delete from atasan where k_nopeg='$id'";
        return dbQuery($sql);
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends AppModel {

	// protected $_table = 'documents';

    public $limit = 10;
    protected $_limit = 10;
    protected $_page = 0;
    protected $_filter = '';
    protected $_column = '*';
    protected $_with = '';
    protected $_join = '';
    protected $_order = 'order by id';
    protected $_show_sql = false;
    protected $_table = 'documents';

    //TYPE
    const TYPE_PRIVATE = 'V';
    const TYPE_PUBLIC = 'C';
    const TYPE_FRIEND = 'F';
    const TYPE_KNOWLEDGE = 'K';
    const TYPE_DISCLAIMER = 'D';
    const TYPE_INFORMATION = 'I';
    const TYPE_GROUP = 'G';
    const TYPE_TICKET = 'T';

    const STATUS_BARU = 'B';
    const STATUS_PROSES = 'P';
    const STATUS_SELESAI= 'D';
    const STATUS_REVISE= 'R';
    const STATUS_REJECT = 'C';
    const STATUS_EVALUATE = 'E';
    const STATUS_CANCEL = 'S';

    const BOARD_MANUAL = 1;
    const PEDOMAN = 2;
    const PROSEDUR = 3;
    const INSTRUKSI_KERJA = 4;
    const FORM = 5;

    protected $has_many = array(
        'Comment',
    );
    protected $belongs_to = array(
        'User',
        'Group',
        'Category',
        'Updated_by' => array('primary_key' => 'userIdUpdated', 'model' => 'User_model')
    );
    protected $many_to_many = array(
        'Post_tag' => array('Post', 'Tag')
    );
    protected $label = array(
        'category_id' => 'Kategori',
        'image' => 'Gambar',
        'file' => 'Berkas',
        'video' => 'Video'
    );
    protected $validation = array(
        'link' => 'valid_url',
        'category_id' => 'required|integer'
    );

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function column($column='*'){
    	$this->_column = $column;
    	return $this;
    }

    public function table($table='documents'){
    	$this->_table = $table;
    	return $this;
    }

    public function limit($limit=10){
    	$this->_limit = $limit;
    	return $this;
    }

    public function page($page=0){
    	$this->_page = $page;
    	return $this;
    }

    public function show_sql($show_sql=false){
    	$this->_show_sql = $show_sql;
    	return $this;
    }

    public function filter($filter){
    	$this->_filter = $filter;
    	return $this;
    }

    public function with($table, $on){
    	if ($table=='' or $on==''){
    		$this->_with = '';
    		return $this;
    	}
    	$this->_with = " left join $table on $on ";
    	return $this;
    }

    public function join($join=''){
    	$this->_join = $join;
    	return $this;
    }

    public function order($order='order by id'){
    	$this->_order = $order;
    	return $this;
    }

	public function createDataList($arr){
		// $val = "";
		$val = array();
		$temp = 0;
		// echo json_encode($arr);
		if($arr){
			foreach($arr as $idx=>$obj){
				// if($temp>0){
				// 	$val = $val . ",";
				// }
				// $val = $val . $obj['name'];
				$val[$temp] = $obj['name'];
				$temp++;
			}
		}
		return $val;
	}

	public function getRelatedDocument($id){
		$sql = "select dr.*,d.title,d.code,d.id iddocument, d.type_id,d.unitkerja_id from document_related dr inner join documents d ON (dr.document2 = d.id) where dr.document1=$id and d.is_delete=0";
		$docRelated1 = dbGetRows($sql);
		// $sql = "select dr.*,d.title,d.id iddocument from document_related dr inner join documents d ON (dr.document1 = d.id) where dr.document2=$id and d.is_delete=0";
		$sql = "select dr.*,d.title,d.code,d.id iddocument,  d.type_id,d.unitkerja_id from document_related dr inner join documents d ON (dr.document1 = d.id) where dr.document2=$id and d.is_delete=0 and d.type_id=".self::FORM;
		$docRelated2 = dbGetRows($sql);
		$tempRelated = 0;
		$res = array();
		if($docRelated1 != false){
			foreach($docRelated1 as $obj){
				$res[$tempRelated] = $obj;
				$tempRelated++;
			}
		}
		if($docRelated2 != false){
			foreach($docRelated2 as $obj){
				$res[$tempRelated] = $obj;
				$tempRelated++;
			}
		}
		return $res;
	}

	public function getRelatedOneWay($id, $way=1){
		$sql = "select dr.*,d.title,d.id iddocument from document_related dr inner join documents d ON (dr.document2 = d.id) where dr.document".$way."=$id and d.is_delete=0";
		$docRelated = dbGetRows($sql);
		return $docRelated;
	}

	public function getDocumentBackup($filter=NULL, $is_published='1'){
		if ($is_published=='1') {
			// $is_published = " and status='D' ";
			// $is_published = "";
		} else {
			// $is_published = " and status!='D' ";
		}
		if ($filter!=NULL) {
			$filters = " and $filter";
		} else {
			$filters = " and d.show=1 ";
		}
		$this->load->model('Document_activity_model');
		$this->load->model('Document_tags_model');
		// $db2 = $this->load->database('upload', true);

		// Select dari postnya
		// $sql = "select p.id as post_id, p.image, p.file, p.video, p.user_id, u.name, p.updated_at from posts p left join users u on u.id=p.user_id where (p.image is not null or p.file is not null or p.video is not null) and (p.is_delete!=1 or p.is_delete is null) order by p.updated_at desc";
		
		$sql = "select d.id as \"document_id\", d.revision as \"revision\", d.title as \"title\", d.description as \"description\", d.document as \"filename\", TO_CHAR(d.created_at, 'YYYY-MM-DD HH24:MI:SS') as \"created_at\", TO_CHAR(d.updated_at, 'YYYY-MM-DD HH24:MI:SS') as \"updated_at\", d.user_id as \"user_id\", u.name as \"username\", d.type_id as \"type_id\", dt.type as \"type_name\", d.updated_by as \"update_by\", d.code as \"code\", d.reason as \"reason\"
			from documents d
			left join users u on u.id=d.user_id
			left join document_types dt on dt.id=d.type_id
			left join document_shares ds on ds.document_id=d.id
			where (d.is_delete!=1 or d.is_delete is null) $is_published $filters $this->_filter order by d.updated_at desc";
		$rows = dbGetRows($sql);
		foreach ($rows as $key => $row) {
			// Activity
			$rows[$key]['activity'] = $this->Document_activity_model->getActivity($row['document_id']);
			// foreach ($rows[$key]['activity'] as $k => $v) {
			// 	$user = $this->User_model->getUser($v['user_id']);
			// 	$rows[$key]['activity'][$k]['photo'] = $user['photo'] ? $user['photo']['thumb']['link'] : base_url('assets/web/img/nopic.png');
				
			// }
			// History
			$rows[$key]['history'] = $this->Document_activity_model->getHistory($row['document_id']);
			// foreach ($rows[$key]['history'] as $k => $v) {
			// 	$user = $this->User_model->getUser($v['user_id']);
			// 	$rows[$key]['history'][$k]['photo'] = $user['photo'] ? $user['photo']['thumb']['link'] : base_url('assets/web/img/nopic.png');
				
			// }

			$rows[$key]['last_update'] = $row['updated_at'];
			if ($row['updated_by']!=NULL){
				$sql = "select * from users where id = '".$row['updated_by']."'";
				$user2 = dbGetRow($sql);
				$rows[$key]["name_updated"] = $user2['name'];
				$rows[$key]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at'])) . " oleh " . $user2['name'];
			} else {
				$rows[$key]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at']));
			}
			$rows[$key]['created_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['created_at'])) . " oleh " . $row['username'];	
			$rows[$key]['docRelated'] = $this->getRelatedDocument($row['document_id']);
			$tags = $this->createDataList($this->Document_tags_model->getDocumentTags($row['document_id']));
			
			$rows[$key]['tags'] = $tags;
		}
		// $temp = 0;
		// foreach($row["document"] as $obj){
		// 	// $sql = "select octet_length(isi_file) from documentsfile where trans_id=".$obj['id'];
		// 	// $idDokumen = $obj['id'];
		// 	// $file = dbGetRows($sql, $db2);
		// 	// $row["document"][$temp]["ukuran"] = $file[0]['octet_length'];
		// 	// $sql = "select distinct dt.id,t.name from document_tag dt, tags t where dt.document_id = $idDokumen and t.id=dt.tag_id";
		// 	// $tags = dbGetRows($sql);
		// 	// $tags = $this->createDataList($tags);
		// 	// $row["document"][$temp]["tags"] = $tags;
		// 	if($obj['updated_by']!=NULL){
		// 		$sql = "select * from users where id = '".$obj['updated_by']."'";
		// 		$user = dbGetRows($sql);
		// 		$row["document"][$temp]["name_updated"] = $user[0]['name'];
		// 	}
			// $row['document'][$temp]['aktifitas'] = $this->Aktifitas_model->getRecAktifitas($idDokumen);
		// 	$row['document'][$temp]['docRelated'] = $this->getRelatedDocument($obj['id']);
		// 	$idParent = $obj['parent'];
		// 	$temp++;
		// }
		// echo json_encode($row);
		// echo '<pre>';
		// var_dump($rows);
		// die();
		return $rows;
	}

	public function getDatePublished($id){
		$sql = "select TO_CHAR(published_at, 'YYYY-MM-DD HH24:MI:SS') from documents where id=$id";
		return dbGetOne($sql);
	}

	public function getDocumentOld($show_all=1){
		$this->load->model('Document_activity_model');
		$this->load->model('Document_tags_model');
		$this->load->model('Document_klausul_model');
		$this->load->model('Document_iso_model');
		$this->load->model('Document_unitkerja_model');
		$this->load->model('Unitkerja_model');
		$this->load->model('User_model');
		$this->load->model('Document_evaluation_model');
		
		$sql = "select $this->_column
			from documents_view d
			$this->_with
			$this->_filter
			";
		// foreach ($_SESSION as $key => $value) {
		// 	# code...
		// }
		$rows = dbGetRows($sql);
		
		$_SESSION['total_doc'] = dbGetOne($sql_count);
		if ($show_all==0)
			return $rows;
		foreach ($rows as $key => $row) {
			unset($rows[$key]['document_text']);

			// Creator and Approver name
			$rows[$key]['creator_name'] = str_replace("'", "",$this->User_model->getBy($row['creator_id'],'name'));
			$rows[$key]['approver_name'] = str_replace("'", "",$this->User_model->getBy($row['approver_id'],'name'));

			// Activity
			$rows[$key]['activity'] = $this->Document_activity_model->getActivity($row['document_id']);

			// History
			$rows[$key]['history'] = $this->Document_activity_model->getHistory($row['document_id']);

			// Change workunit
			$rows[$key]['change_workunit'] = $this->getChangeWorkunit($row['document_id']);

			// Requirement
			$rows[$key]['requirement'] = implode(",\n",Util::toList($this->Document_iso_model->filter(" dis.document_id=".$row['document_id'])->table(" document_iso di ")->with(array("name"=>"document_isos", "initial"=>"dis"),"dis.iso_id=di.id")->column(array("iso"=>"\"iso\""))->getAll(),'iso'));

			// Klausul
			$rows[$key]['clausul'] = implode(",\n ",Util::toList($this->Document_klausul_model->filter(" dk.document_id=".$row['document_id'])->table(" document_klausul dk ")->with(array("name"=>"document_iso_klausul", "initial"=>"dik"),"dik.id=dk.klausul_id")->column(array("klausul"=>"\"klausul\""))->getAll(),'klausul'));
			
			// Company
			$company = $this->Unitkerja_model->getOne($row['unitkerja_id'], 'company');
			$rows[$key]['company_name'] = $this->Unitkerja_model->getOne(str_pad($company,8,'0', STR_PAD_LEFT), 'name');			
			// variable document
			$rows[$key]['last_update'] = $row['updated_at'];
			if ($row['updated_by']!=NULL){
				$sql = "select * from users where id = '".$row['updated_by']."'";
				$user2 = dbGetRow($sql);
				$rows[$key]["name_updated"] = $user2['name'];
				$rows[$key]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at'])) . " by " . $user2['name'];
			} else {
				$rows[$key]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at']));
			}
			$name = $row['name'];
			$sql = "select from_migration from documents where id=".$row['document_id'];
			$is_from_migration = dbGetOne($sql);
			if ($is_from_migration){
				$name = 'System Migration';
			}
			$rows[$key]['created_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['created_at'])) . " by " . $name;

			// related document	
			$rows[$key]['docRelated'] = $this->getRelatedDocument($row['document_id']);
			$tags = $this->createDataList($this->Document_tags_model->getDocumentTags($row['document_id']));
			
			$rows[$key]['tags'] = $tags;

			// Related Unitkerja
			$document_id = $row['document_id'];
			if (isset($_SESSION['document_search']['related'])){
				// $filter_related_workunit = " and document_unitkerja='".$_SESSION['document_search']['related']."' or document_unitkerja=0";
				$filter_related_workunit = " and document_unitkerja in ($unitkerja_ids)";
				$related_workunit = $this->Document_unitkerja_model->filter(" where document_id=$document_id $filter_related_workunit")->getAll();
				if ($related_workunit){
					$rows[$key]['related_workunit'] = Util::toList($related_workunit);
				} else {
					$_SESSION['total_doc']--;
					unset($rows[$key]);
				}
			}
		}
		// echo '<pre>';
		// vaR_dump($rows);
		// die();
		return $rows;
	}

	public function getDocument($show_all=1, $dataFilter=NULL,$user =null, $page =1, $getSimpleData = false){	
		$this->load->model('Document_activity_model');
		$this->load->model('Document_tags_model');
		$this->load->model('Document_klausul_model');
		$this->load->model('Document_iso_model');
		$this->load->model('Document_unitkerja_model');
		$this->load->model('Unitkerja_model');
		$this->load->model('User_model');
		$this->load->model('Document_evaluation_model');
		$join_changeunit = '';
		// $sql = "select $this->_column
		// 	from documents_view d
		// 	$this->_with
		// 	$this->_filter
		// 	";
		// foreach ($_SESSION as $key => $value) {
		// 	# code...
		// }
		if (class_exists('SessionManagerWeb')){
            $min = ($_SESSION['page']-1)*$this->_limit;
			$max = $_SESSION['page']*$this->_limit;
        } 
        else{
            $min = ($page-1)*$this->_limit;
			$max = $page*$this->_limit;
        }
		
		if($dataFilter==NULL){
			$dataFilter = $_SESSION['document_search'];
		}
		$join_document_evaluation = "left join document_evaluation de on de.document_id =x.\"document_id\" ";
		if (count($dataFilter['arr_status']) == 0 ){
			$filterstatus = '';
		} else {
			// echo "<pre>";print_r($_SESSION['document_search']['arr_status']);die();
			$filterstatus =' and (';
			foreach ($dataFilter['arr_status'] as $key) {
				if ($key == 1){
					$filterstatus .= " \"workflow_id\" = 1 "; 
				}
				if($key == 2){
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\" = 2 and \"workflow_urutan\"!=6 "; 
				}
				if($key == 3){
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\" = 2 and de.status='".($key == 2 ? 'E' :'R')."'"; 
				}
			}
			$filterstatus .=' )';
			// $filter_status = " and \"workflow_id\" in ($_SESSION['document_search']['status']) ";
			// if(in_array($_SESSION['document_search']['arr_status'], array(2,3))){
			// 	$filter_status .= " and de.status=".;
			// }
		}
		// echo "<pre>";print_r($_SESSION['document_search']['related']);die();
		if ($dataFilter['related']){
			if($user!=null){
				$variables = PrivateApiController::getVariables($user);
			}
			else{
				$variables = SessionManagerWeb::getVariables();
			}
			$arr_unitkerja_ids = array();
			$arr_unitkerja_ids[] = 0;
			foreach ($variables['myunitkerja'] as $k => $v) {
				$arr_unitkerja_ids[] = $k;
			}
			$unitkerja_ids = implode(',', $arr_unitkerja_ids);
			$join_unitkerja = "left join document_unitkerja duk on duk.document_id=x.\"document_id\"";
			
			$where_unitkerja = "where duk.document_unitkerja in ($unitkerja_ids)";
			if($getSimpleData == true){
				$sql = "select x.\"document_id\" ,x.\"code\",x.\"title\", x.\"unitkerja_name\", x.\"revision\", x.\"validity_date\",x.\"hasil_evaluasi\" ";
			}
			else{
				$sql = "select x.*";
			}
			$sql .= ", to_date(TO_CHAR(d.published_at, 'MM-DD-YYYY'), 'MM-DD-YYYY') as publish_date ";
			if($getSimpleData == true){

				$sql .=",
					CASE ch.creator
						WHEN 1 THEN
							CASE ch.approver
								WHEN 1 THEN 'B'
								ELSE 'C'
							END
						ELSE 'O'
					END AS \"changeunit_cre_appr\",
					CASE ch.APPROVER
						WHEN 1 THEN
							CASE ch.CREATOR
								WHEN 0 THEN 'A'
								ELSE 'B'
							END
						ELSE 'O'
					END AS \"changeunit_appr\",
					CASE ch.UNITKERJA
						WHEN 1 THEN 'X'
						ELSE 'O'
					END AS \"changeunit_unitkerja\"
				";

				$join_changeunit  = "LEFT JOIN DOCUMENT_CHANGEUNIT ch ON ch.document_id =x.\"document_id\"";
			}
			$sql .=" from (select x.*, rownum rown from (select $this->_column
						from documents_view d
						$this->_with
						$this->_filter) x
						$join_unitkerja
						$join_document_evaluation
						$join_changeunit
						$where_unitkerja ";
			// echo "<pre>";print_r($_SESSION['document_search']['arr_status']);die();
			
			$sql .= $filterstatus;
			$sql .=		") x
						where rown>$min and rown<=$max
						";
			$sql_count = "select count(1) from (select $this->_column
						from documents_view d
						$this->_with
						$this->_filter) x
						$join_unitkerja
						$where_unitkerja";
		} else {
			if($getSimpleData == true){
				$sql = "select x.\"document_id\" ,x.\"code\",x.\"title\", x.\"unitkerja_name\", x.\"revision\", x.\"validity_date\", x.\"workflow_id\", x.\"status\", de.status as \"hasil_evaluasi\", x.\"show\" ";
			}
			else{
				$sql = "select x.*";
			}
			

			$sql .=", to_date(TO_CHAR(x.\"published_at\", 'MM DD YYYY'), 'MM DD YYYY') as \"published_date\" ";

			if($getSimpleData == true){
				$sql .=",
					CASE ch.creator
						WHEN 1 THEN
							CASE ch.approver
								WHEN 1 THEN 'B'
								ELSE 'C'
							END
						ELSE 'O'
					END AS \"changeunit_cre_appr\",
					CASE ch.APPROVER
						WHEN 1 THEN
							CASE ch.CREATOR
								WHEN 0 THEN 'A'
								ELSE 'B'
							END
						ELSE 'O'
					END AS \"changeunit_appr\",
					CASE ch.UNITKERJA
						WHEN 1 THEN 'X'
						ELSE 'O'
					END AS \"changeunit_unitkerja\"
				";

				$join_changeunit  = "LEFT JOIN DOCUMENT_CHANGEUNIT ch ON ch.document_id =x.\"document_id\"";
			}

			$sql .="from (select $this->_column
						from documents_view d
						$this->_with
						$this->_filter) x
						$join_document_evaluation
						$join_changeunit
						";

			if($getSimpleData == false){
				$sql .=	" where r>$min and r<=$max
						";
			}
			if($filterstatus){
				$sql .=" where ".$filterstatus;
			}

			$sql_count = "select count(1)
						from documents_view d
						$this->_with
						$this->_filter";

		}
		// echo nl2br($sql);
		// die();
		$rows = dbGetRows($sql);
		// echo "<pre>";print_r($rows);die();
		$_SESSION['total_doc'] = dbGetOne($sql_count);
		if ($show_all==0)
			return $rows;
		
		if($getSimpleData == false){
			foreach ($rows as $key => $row) {
				unset($rows[$key]['document_text']);

				// Creator and Approver name
				$rows[$key]['creator_name'] = str_replace("'", "",$this->User_model->getBy($row['creator_id'],'name'));
				$rows[$key]['approver_name'] = str_replace("'", "",$this->User_model->getBy($row['approver_id'],'name'));
				$rows[$key]['name'] = str_replace("'", "",$row['name']);

				// Activity
				$rows[$key]['activity'] = $this->Document_activity_model->getActivity($row['document_id']);

				// History
				$rows[$key]['history'] = $this->Document_activity_model->getHistory($row['document_id']);

				// Change workunit
				$rows[$key]['change_workunit'] = $this->getChangeWorkunit($row['document_id']);

				// Requirement
				$rows[$key]['requirement'] = implode(",\n",Util::toList($this->Document_iso_model->filter(" dis.document_id=".$row['document_id'])->table(" document_iso di ")->with(array("name"=>"document_isos", "initial"=>"dis"),"dis.iso_id=di.id")->column(array("iso"=>"\"iso\""))->getAll(),'iso'));

				// Klausul
				$rows[$key]['clausul'] = implode(",\n ",Util::toList($this->Document_klausul_model->filter(" dk.document_id=".$row['document_id'])->table(" document_klausul dk ")->with(array("name"=>"document_iso_klausul", "initial"=>"dik"),"dik.id=dk.klausul_id")->column(array("klausul"=>"\"klausul\""))->getAll(),'klausul'));
				
				// Company
				$company = $this->Unitkerja_model->getOne($row['unitkerja_id'], 'company');
				$rows[$key]['company_name'] = $this->Unitkerja_model->getOne(str_pad($company,8,'0', STR_PAD_LEFT), 'name');			
				// variable document
				$rows[$key]['last_update'] = $row['updated_at'];
				if ($row['updated_by']!=NULL){
					$sql = "select * from users where id = '".$row['updated_by']."'";
					$user2 = dbGetRow($sql);
					$rows[$key]["name_updated"] = $user2['name'];
					$rows[$key]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at'])) . " by " . $user2['name'];
				} else {
					$rows[$key]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at']));
				}
				$name = $row['name'];
				$sql = "select from_migration from documents where id=".$row['document_id'];
				$is_from_migration = dbGetOne($sql);
				if ($is_from_migration){
					$name = 'System Migration';
				}
				if ($row['type_id']==self::BOARD_MANUAL){
					$name = 'Document Controller';
				}
				$rows[$key]['created_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['created_at'])) . " by " . $name;

				// related document	
				$rows[$key]['docRelated'] = $this->getRelatedDocument($row['document_id']);
				$rows[$key]['docRelatedNonForm'] = array_search('0', array_column($rows[$key]['docRelated'], 'IS_FORM'));
				$rows[$key]['docRelatedForm'] = array_search('1', array_column($rows[$key]['docRelated'], 'IS_FORM'));
				$tags = $this->createDataList($this->Document_tags_model->getDocumentTags($row['document_id']));
				
				$rows[$key]['tags'] = $tags;


				// Related Unitkerja
				$document_id = $row['document_id'];
				if (isset($_SESSION['document_search']['related'])){
					// $filter_related_workunit = " and document_unitkerja='".$_SESSION['document_search']['related']."' or document_unitkerja=0";
					$filter_related_workunit = " and document_unitkerja in ($unitkerja_ids)";
					$related_workunit = $this->Document_unitkerja_model->filter(" where document_id=$document_id $filter_related_workunit")->getAll();
					if ($related_workunit){
						$rows[$key]['related_workunit'] = Util::toList($related_workunit);
					} else {
						$_SESSION['total_doc']--;
						unset($rows[$key]);
					}
				}
			}
		}
		
		// echo '<pre>';
		// vaR_dump($rows);
		// die();
		return $rows;
	}

	public function getOne($document_id, $col){
		$sql = " select $col from documents where id='$document_id' ";
		$column = dbGetOne($sql);
		return $column;
	}

	public function getOneBy($col){
		$sql = " select $col from documents $this->_filter ";
		$column = dbGetOne($sql);
		return $column;
	}

	public function getDocumentNeedAction($getData= 2, $userID = null, $getRole = null, $getCompany = null){

		$this->load->model('Workflow_model');
		$this->load->model('Document_delegates_model');
		$this->load->model('Document_reviewer_model');
		$this->load->model('User_model');
		$this->load->model('Document_types_model');
		$this->load->model('Document_evaluation_model');
		$this->load->model('Unitkerja_model');

		// $sql = "select * 
		// 		from documents_view 
		// 		where \"show\"=1 and \"status\"!='D' $this->_filter order by \"created_at\" asc";
				// die($sql);
		// Ditambahi status = D, soalnya kalo keselect, pengaruh di pagenya
		$filter_show = " \"status\"!='D' ";
		//\"show\"=1
		$sql = "select * 
				from documents_view 
				where $filter_show $this->_filter order by \"created_at\" asc";
		$rows = dbGetRows($sql);
		// echo "<pre>";print_r($userID);echo "</pre>";
		// echo "<pre>";print_r($rows);echo "</pre>";die();
		$total_document = 0;
		$total_all_document=0;
		$need_action = 0 ;
		$in_process = 0;
		$data = array();
		$data_in_action = array(); // untuk is_mine = 0
		$data_in_progress = array(); // untuk is_mine = 1
		$data_rejected = array();
		$index_in_action = 0; // untuk is_mine = 0
		$index_in_progress = 0; // untuk is_mine = 1
		$index_rejected = 0;
		// $page_min = $this->_page*$this->_limit;
		// $page_max = ($this->_page+1)*$this->_limit;
		// echo "<pre>";print_r($rows);die();
		foreach ($rows as $key => $row) {
			if ($row['status']==self::STATUS_SELESAI and $row['workflow_id']==1){ // Jika statusnya D/Done dan workflow nya adalah pengajuan, maka lewati ke row selanjutnya
				continue;
			}
			
			$drafter_evaluation = dbgetOne('select drafter_evaluation from documents where id='.$row['document_id']);
			$tasks = $this->Workflow_model->getTasks($row['workflow_id']); // get workflow urutan
			$workflow_task = $this->Workflow_model->getTasks($row['workflow_id'], $row['workflow_urutan'], $drafter_evaluation); // get workflow urutan yang sekarang;
			// echo "<pre>";print_r($workflow_task);echo "</pre>";die();
			$delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$row['workflow_urutan']." and workflow_id=".$row['workflow_id']. " and document_id=".$row['document_id'])->getBy(); // check ada delegasi atau tidak
			// if ($row['document_id']=='801'){
			// 	echo '<pre>';
			// 	var_dump(SessionManagerWeb::getUserID());
			// 	vaR_dump($delegated);
			// 	die();
			// }
	    	$reviewer = $this->Document_reviewer_model->getAllBy($row['document_id'], $row['workflow_id']); // get reviewer
	    	$is_chief = false;
	    	if ($this->User_model->getBy($userID, 'is_chief')=='X'){ // get dia is chief atau bukan
	    		$is_chief = true;
	    	}

	  //   	$codes = explode('/', $row['code']);
			// $company = $codes[1];
			$company = $this->Unitkerja_model->getOne($row['unitkerja_id'], 'company'); // get Company dari unitkerjanya dokumen

			$allowed_users = array();

			switch($row['workflow_id']){ // cek workflow_id
				case Workflow_model::EVALUASI:
					switch($row['workflow_urutan']){
						case 1:
							$allowed_users[] = $delegated['delegate'];
							if ($delegated!=NULL){
								$row['user_id'] = $delegated['delegate'];
								$rows[$key]['user_id'] = $delegated['delegate'];								
							}

						break;
					}
				break;
			}
			// if (SessionManagerWeb::isAdministrator() or 
			// echo $row['status'].'===='.$workflow_task['rejected']."++++".$workflow_task['user']."<br>";
			if (($workflow_task['user']==Role::REVIEWER and  in_array($userID,$reviewer)) // jika role skrg reviewer, dan user login adalah reviewer
				or ($workflow_task['user']!=Role::DRAFTER and $getRole==$workflow_task['user']) 
				or (($workflow_task['user']==Role::REVIEWER or $workflow_task['user']==Role::DOCUMENT_CONTROLLER) and ($delegated['delegate']==$userID or $is_chief) and ($getRole==Role::DOCUMENT_CONTROLLER and $company==$getCompany )) // (role skrg reviewer atau document controller) dan (user login adalah penerima delegasi atau dia adalah is chief) dan (jika role user login adalah doccon dan company document = company user login)
				or ($userID==$row['user_id'] && $drafter_evaluation == null)  // jika user login == drafter dokumen
				or ($userID==$row['creator_id'] && $row['workflow_urutan']!=0 && $row['workflow_id'] == 2)  // jika user login == creator doc ketika evaluasi
				or ($userID==$row['creator_id'] && $drafter_evaluation == null)  // jika user login == creator doc ketika revisi dan tanpa drafter
				or ($userID==$row['creator_id'] && $row['workflow_id'] == 3 && $row['workflow_urutan'] == 3 && $drafter_evaluation != null)  // jika user login == creator doc ketika revisi dengan drafter
				or ($workflow_task['user']==Role::APPROVER and $userID==$row['approver_id']) // jika role skrg apporver dan user login adalah approvernya
				or in_array($userID, $allowed_users) // jika user login termasuk allowed user
				or ($row['status']==self::STATUS_REJECT and $workflow_task['rejected'] == 1 and $workflow_task['user'] == Role::DOCUMENT_CONTROLLER)
				or ($drafter_evaluation == $userID)
				) {
				// echo "document_id=".$row['document_id']."masuk<br>";die();
				$total_all_document++;
				$arr = explode(".",$row['filename']);
				$ext = end($arr);
				$row['ext'] = $ext;

				$data[$total_document] = $row;
				$data[$total_document]['last_update'] = $row['updated_at'];
				$task_name = $workflow_task['name'];

				$actors = $this->Workflow_model->getActors($row['workflow_id'], $row['workflow_urutan']); // get actor skrg

				// Setting action atau waiting
				$action = "waiting";
				$is_mine = 1;
				if ($row['status']!=self::STATUS_REJECT){ // jika status tidak reject
					// if (SessionManagerWeb::getUserID()!=$row['user_id'] and !SessionManagerWeb::isAdministrator()){
					if ($userID!=$row['user_id']){
						$action = "need your";
						$is_mine = 0;
						if ($userID==$row['creator_id'] and $workflow_task['user']!=Role::CREATOR){
							$action = "waiting";
							$is_mine = 1;
						}
					}
					if (in_array($getRole, $actors) || in_array($userID,$reviewer)){
						$action = "need your";
						$is_mine = 0;
					}
					if ((in_array(Role::CREATOR, $actors) and $userID==$row['creator_id'])
						or (in_array(Role::APPROVER, $actors) and $userID==$row['approver_id'])){
						$action = "need your";
						$is_mine = 0;
					}				

					// if($row['creator_id'] == $userID && $row['workflow_urutan'] == 0 && $drafter_evaluation != null ){
					// 	// echo "masuk dong";die();
					// 	$is_mine =0;
					// }
					$draft = '';
					if ($row['status']==self::STATUS_BARU) {
						$draft = 'Draft ';
					}
					if ($workflow_task['urutan']==1){
						$action = "need you to submit";
						$is_mine = 0;
						$task_name = "";
					}
					if ($row['status']==self::STATUS_REVISE){
						$action = "need to be Revised";
						$is_mine = 0;
						$task_name = '';
					}
					if ($row['status']==self::STATUS_SELESAI){
						$action = '';
						$is_mine = 1;
					}
					if($getData == 2){
						$data[$total_document]['title'] = "<a style='color:#424242' href='".site_url('web/document/detail').'/'.$row['document_id']."'>".$draft.$row['type_name']." <b style=''>\"".$row['title']."\"</b> </a>";
					}
					

				} else {
					$action = "is rejected";
					if($getData == 2){
						$data[$total_document]['title'] = "<a style='color:#F4511E' href='".site_url('web/document/detail').'/'.$row['document_id']."'>".$row['type_name']." <b style='color:#F4511E'>\"".$row['title']."\"</b> </a>";
					}
					
					$is_mine=1;
				}
				$data[$total_document]['user_action'] = $action.' '.$task_name;

				// Setting Stepper
				$data[$total_document]['status_step'] = '';
				$total_tasks = 0;
				$type = $this->Document_types_model->get($row['type_id']);
				foreach ($tasks as $task) {
					$skip = 0;
					switch($row['workflow_id']){
						case Workflow_model::PENGAJUAN:
							if ($row['user_id']==$row['creator_id'] and $task['user']==Role::CREATOR and $type['need_reviewer']=='0'){
								$skip=1;
							}
							if ($type['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT)))
								$skip=1;	
						break;
						case Workflow_model::EVALUASI:
							$evaluation = $this->Document_evaluation_model->filter("where document_id=".$row['document_id'])->getBy();
							if ($evaluation!=NULL){
								$evaluation['text'] = json_decode($evaluation['text'], true);
							}
							switch($row['workflow_urutan']){
								case 0:
									if($drafter_evaluation == $userID){
										$is_mine = 0;
									}
								break;
								case 1:
									if ($userID!=$row['creator_id'] and $userID!=$delegated['delegate']){
										$is_mine = 1;
									}
									if (isset($delegated)){
										$is_mine = 1;
									}
									if ($userID==$row['creator_id'] && $drafter_evaluation == null){
										$is_mine = 0;
									}
									if($drafter_evaluation == $userID){
										$is_mine = 1;
									}
								break;
								// default:
								// 	if (SessionManagerWeb::getUserID()==$row['evaluation_drafter_id']){
								// 		$is_mine = 1;
								// 	}
								// break;
							}
							if ($type['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT)))
								$skip=1;
							// if (!isset($delegated) and $task['urutan']==3){
							
							switch ($task['urutan']) {
								case 0:
									if($drafter_evaluation == null){
										$skip=1;
									}
								break;
								case 1:
									if($drafter_evaluation != null){
										$skip =1;
									}
								break;
								case 2:
									$result = 0;
									foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
										if ($v_evaluation==1){
											$result++;
										}
									}
									// echo "<pre>";print_r($result);die();
									if ($result>0 and $result<=9){
										$skip = 1;
									}
								break;
								case 3:
									if ($row['user_id']==$row['creator_id'] && $drafter_evaluation==null){
										// $skip = 1;
										if ($type['need_reviewer']=='0'){
											$skip=1;	
										}
									}
								break;
								
								case 4:
									if ($evaluation['text'][1]==1){
										$skip = 1;
									}
								break;
								case 5:
									$result = 0;
									foreach ($evaluation['text'] as $k_evaluation => $v_evaluation) {
										if ($v_evaluation==1){
											$result++;
										}
									}
									// echo "<pre>";print_r($result);die();
									if ($result>0 and $result<=9){
										$skip = 1;
									}
								break;
							}

						break;
						case Workflow_model::REVISI:
							if ($row['user_id']==$row['creator_id'] and $task['user']==Role::CREATOR and $drafter_evaluation == null){
								if ($task['urutan']!=1 and $type['need_reviewer']=='0'){
									$skip=1;	
								}
							}
							if ($type['need_reviewer']=='0' and in_array($task['form'], array(Workflow_model::REVIEW_ATRIBUT, Workflow_model::REVIEW_CONTENT)))
								$skip=1;
						break;
					}
					if ($skip==1)
						continue;
					// if ($row['user_id']==$row['creator_id'] and $task['user']==Role::CREATOR and $row['workflow_id']==Workflow_model::PENGAJUAN)
					// 	continue;

					$total_tasks++;
					// echo $task['urutan']."<br>";
					if ($task['icon']['is_icon']==1){
						$stepper_status ="<i class='".$task['icon']['icon']."'></i>";
					} else {
						$stepper_status ="<img src='".$task['icon']['icon']."'>";
					}
					$tooltip = $task['name'];
					$step_color = 'btn-default-none';
					$title_reject ='';
					$btn_size = 'btn-xl';
					if ($task['urutan']<$workflow_task['urutan']){
						if ($task['icon']['is_icon']==1){
							$stepper_status ="<i class='".$task['icon']['icon']."'></i>";
						} else {
							$stepper_status ="<img src='".str_replace("none.png","green.png",$task['icon']['icon'])."'>";
						}
						$step_color = 'btn-fix';
					} else if ($task['urutan']==$workflow_task['urutan']){
						// $btn_size = 'btn-xl';
						$task_now = $total_tasks;
						// $stepper_status ="<img src='".$task['icon']."'>";
						if ($task['icon']['is_icon']==1){
							$stepper_status ="<i class='".$task['icon']['icon']."'></i>";
						} else {
							$stepper_status ="<img src='".str_replace("none.png","yellow.png",$task['icon']['icon'])."'>";
						}
						if ($row['status']==self::STATUS_SELESAI){
							$step_color = 'btn-fix';
							if ($task['icon']['is_icon']==1){
								$stepper_status ="<i class='".$task['icon']['icon']."'></i>";
							} else {
								$stepper_status ="<img src='".str_replace("none.png","green.png",$task['icon']['icon'])."'>";
							}
						}
						else
							$step_color = 'btn-warning-prog';
						if ($row['status']==self::STATUS_REVISE)
							$tooltip = 'Revision';
						if ($row['status']==self::STATUS_REJECT){
							$title_reject = ' (REJECTED)';
							$step_color = 'btn-danger-reject';
							if ($task['icon']['is_icon']==1){
								$stepper_status ="<i class='".$task['icon']['icon']."'></i>";
							} else {
								$stepper_status ="<img src='".str_replace("none.png","red.png",$task['icon']['icon'])."'>";
							}
						}
					}

					if($getData == 2){
						$data[$total_document]['status_step'] .= 
						"<button type='button' class='btn ".$step_color." btn-circle ".$btn_size."' data-toggle='tooltip' data-placement='top' title='".$tooltip.$title_reject."'>
				        	".$stepper_status."
			            </button>";
					}
					else{
						$data[$total_document]['step_number'][] = array( 'urutan' => $task['urutan'], 'edit' => $task['edit'], 'user' => $task['user']);
					}
					
				}
				
				if ($row['updated_by']!=NULL){
					$sql = "select * from users where id = '".$row['updated_by']."'";
					$user2 = dbGetRow($sql);
					$data[$total_document]["name_updated"] = $user2['name'];
					$data[$total_document]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at'])) . " oleh " . $user2['name'];
				} else {
					$data[$total_document]['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['updated_at']));
				}
				$data[$total_document]['is_mine'] = $is_mine;
				$data[$total_document]['created_at'] = strftime('%Y/%m/%d %H:%M', strtotime($row['created_at'])) . " oleh " . $row['username'];
				$progress_percentage = $task_now/$total_tasks*100;
				$data[$total_document]['progress_percentage'] = $progress_percentage;
				$data[$total_document]['total_progress'] = $total_tasks*11.5;
				
				if ($is_mine==0){
					$data_in_action[$index_in_action] = $rows[$key];
					$data_in_action[$index_in_action]['ext'] = $row['ext'];
					$data_in_action[$index_in_action]['last_update'] = $data[$total_document]['last_update'];
					$data_in_action[$index_in_action]['user_action'] = $data[$total_document]['user_action'];
					$data_in_action[$index_in_action]['status_step'] = $data[$total_document]['status_step'];
					$data_in_action[$index_in_action]['step_number'] = $data[$total_document]['step_number'];
					$data_in_action[$index_in_action]['progress_percentage'] = $data[$total_document]['progress_percentage'];
					$data_in_action[$index_in_action]['total_progress'] = $data[$total_document]['total_progress'];
					$index_in_action++;
					$need_action++;
				} else {
					$data_in_progress[$index_in_progress] = $rows[$key];
					$data_in_progress[$index_in_progress]['ext'] = $row['ext'];
					$data_in_progress[$index_in_progress]['last_update'] = $data[$total_document]['last_update'];
					$data_in_progress[$index_in_progress]['user_action'] = $data[$total_document]['user_action'];
					$data_in_progress[$index_in_progress]['status_step'] = $data[$total_document]['status_step'];
					$data_in_progress[$index_in_progress]['step_number']   = $data[$total_document]['step_number'];
					$data_in_progress[$index_in_progress]['progress_percentage'] = $data[$total_document]['progress_percentage'];
					$data_in_progress[$index_in_progress]['total_progress'] = $data[$total_document]['total_progress'];
					$index_in_progress++;
					$in_process++;
				}
				if($getData == 3){
					$data_rejected[$index_rejected] = $rows[$key];
					$data_rejected[$index_rejected]['ext'] = $row['ext'];
					$data_rejected[$index_rejected]['last_update'] = $data[$total_document]['last_update'];
					$data_rejected[$index_rejected]['user_action'] = $data[$total_document]['user_action'];
					$data_rejected[$index_rejected]['status_step'] = $data[$total_document]['status_step'];
					$data_rejected[$index_rejected]['step_number']   = $data[$total_document]['step_number'];
					$data_rejected[$index_rejected]['progress_percentage'] = $data[$total_document]['progress_percentage'];
					$data_in_progress[$index_rejected]['total_progress'] = $data[$total_document]['total_progress'];
					$index_rejected++;
				}
				$total_document++;
			}
			// echo "<pre>";print_r($data);die();
			// if ($row['document_id']=='663'){
			// 	echo '<pre>';
			// 	vaR_dump($data[$total_document-1]);
			// 	die();
			// }
		}
		if($getData == 0){
			$data = array();
			$data = $data_in_action;
			$data['total_document'] = $total_document;
			$data['total_all_document'] = $total_all_document;
			$data['need_action'] = $need_action;
			$data['in_process'] = $in_process;

		}
		elseif($getData == 1){
			$data = array();
			$data = $data_in_progress;
			$data['total_document'] = $total_document;
			$data['total_all_document'] = $total_all_document;
			$data['need_action'] = $need_action;
			$data['in_process'] = $in_process;
		}
		elseif($getData == 2){
			$data['total_document'] = $total_document;
			$data['total_all_document'] = $total_all_document;
			$data['need_action'] = $need_action;
			$data['in_process'] = $in_process;
		}
		elseif($getData == 3){
			$data = array();
			$data = $data_rejected;
			$data['total_document'] = $total_document;
			$data['total_all_document'] = $total_all_document;
			$data['need_action'] = $need_action;
			$data['in_process'] = $in_process;
		}
		// echo "<pre>";print_r($data);echo "<pre>";
		// echo "<pre>";print_r($data_in_progress);die();
		// $data['next'] = ($page_max>= $total_all_document) ? 0:1;
		// echo "<pre>";print_r($data);die();

		// die();
		return $data;
	}

	public function getEncryption($document_name, $user_id){
		// $sql = "select encryption from document_watermarked_hash where document_id=$document_id" ;
		$sql = " select encryption from document_watermarked_hash where document_name='".$document_name."' and user_id=$user_id ";
		return dbGetOne($sql);
	}

	public function getRealEncryption($id){
		// $sql = "select encryption from document_watermarked_hash where document_id=$document_id" ;
		$sql = " select encryption from document_hash where document_id='".$id."' ";
		return dbGetOne($sql);
	}

	public function isExistWatermarked($document_name, $user_id){
		$sql = "select 1 from document_watermarked_hash where document_name='".$document_name."' and user_id=$user_id";
		return dbGetOne($sql);
	}

	public function getDocumentBy($userid, $id){
		$CI = & get_instance();
        $config = $CI->config->item('utils');

		if ($filter!=NULL) {
			$filters = " and $filter";
		} else {
			// $filters = " and \"show\"=1 ";
		}
		
		// $sql = "select d.id as \"document_id\", d.hash as \"hash\", d.revision as \"revision\", d.title as \"title\", d.description as \"description\", d.document as \"filename\", TO_CHAR(d.created_at, 'YYYY-MM-DD HH24:MI:SS') as \"created_at\", TO_CHAR(d.updated_at, 'YYYY-MM-DD HH24:MI:SS') as \"updated_at\", d.user_id as \"user_id\", u.username as \"username\",u.name as \"name\", d.type_id as \"type_id\", dt.type as \"type_name\", d.updated_by as \"update_by\", d.code as \"code\", d.workflow_id as \"workflow_id\", d.workflow_urutan as \"workflow_urutan\", d.status as \"status\", d.reason as \"reason\", d.creator_id as \"creator_id\", d.approver_id as \"approver_id\"
		// 	from documents d
		// 	left join users u on u.id=d.user_id
		// 	left join document_types dt on dt.id=d.type_id
		// 	left join document_shares ds on ds.document_id=d.id
		// 	where (d.is_delete!=1 or d.is_delete is null) $filters and d.id=$id order by d.updated_at desc";
		$sql = "select * 
				from documents_view 
				where \"document_id\"=$id $filters order by \"updated_at\" desc";
		$rows = dbGetRow($sql);
		$this->load->model('Document_types_model');
		$rows['need_reviewer'] = $this->Document_types_model->getBy($rows['type_id'],'need_reviewer');
		$rows['last_update'] = $rows['updated_at'];
		$rows['parent'] = dbGetOne("select parent from documents where id=$id");
		// $rows['document_text'] = file_get_contents();
		
		$this->load->model('User_model');
		// sudah ada di view
		// $rows['creator_name'] = $this->User_model->getOne($rows['creator_id'], 'name');
		// $rows['approver_name'] = $this->User_model->getOne($rows['approver_id'], 'name');
		if ($rows['updated_by']!=NULL){
			$sql = "select * from users where id = '".$rows['updated_by']."'";
			$user2 = dbGetRow($sql);
			$rows["name_updated"] = $user2['name'];
			$rows['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($rows['updated_at'])) . " oleh " . $user2['name'];
		} else {
			$rows['updated_at'] = strftime('%Y/%m/%d %H:%M', strtotime($rows['updated_at']));
		}

		$sql = "select from_migration from documents where id=".$rows['document_id'];
		$rows['is_from_migration']= dbGetOne($sql);
		// Requirement
		$this->load->model('Document_iso_model');
		$rows['requirement'] = implode(",\n",Util::toList($this->Document_iso_model->filter(" dis.document_id=".$id)->table(" document_iso di ")->with(array("name"=>"document_isos", "initial"=>"dis"),"dis.iso_id=di.id")->column(array("iso"=>"\"iso\""))->getAll(),'iso'));

		// Klausul
		$this->load->model('Document_klausul_model');
		$rows['clausul'] = implode(",\n ",Util::toList($this->Document_klausul_model->filter(" dk.document_id=".$id)->table(" document_klausul dk ")->with(array("name"=>"document_iso_klausul", "initial"=>"dik"),"dik.id=dk.klausul_id")->column(array("klausul"=>"\"klausul\""))->getAll(),'klausul'));


		// Link
		$arr = explode(".",$rows['filename']);
		// $ext = end($arr);
		$filename 	= $arr[0];
		$ext 		= $arr[1];
		$rows['ext'] = $ext;
		$md5_user_id = md5($rows['user_id'] . $CI->config->item('encryption_key'));
		$hash = hash_hmac('sha256',$rows['filename'], $rows['user_id']);

		// jadiin .backup
		// $rows['document_name_encrypted'] = ($ext=='docx' || $ext=='doc') ?  File::getFileName($hash, 'document/files').".backup" : File::getFileName($hash, 'document/files').'.'.$ext;
		$rows['document_name_watermark'] = $filename.'_'.$userid.'_watermarked.pdf';
		// $rows['document_name_encrypted'] = File::getFileName($hash, 'document/files').'.'.$ext;
		// $rows['document_name_encrypted'] = $rows['filename'];
		// $rows['document_name_encrypted'] = $rows['hash'];
		$rows['document_name_encrypted'] = dbGetOne(" select encryption from document_hash where document_id=".$rows['document_id']." ");

		$rows['created_at'] = strftime('%Y/%m/%d %H:%M', strtotime($rows['created_at'])) . " oleh " . $rows['username'];

		if ($rows['is_upload']=='0') {
			$hash = hash_hmac('sha256',$rows['filename'].$id, $rows['user_id']);
			// $rows['document_text'] = file_get_contents($config['full_upload_dir'].'/document/files/'.File::getFileName($hash, 'document/files').".txt");
			$files = explode('.',$rows['filename']);
			unset($files[count($files)-1]);
			$filename = implode('.', $files);
			$rows['document_text'] = file_get_contents($config['full_upload_dir'].'/document/files/'.$filename.".txt");
			$rows['document_text'] = str_replace(array("\n", "\t", "\r"), '', $rows['document_text']);
			$rows['document_text'] = str_replace('"', "'", $rows['document_text']);
		}
		
		// echo '<pre>';
		// vaR_dump($config['full_upload_dir'].'/document/files/'.File::getFileName($hash, 'document/files').".txt");
		// die();
		// echo '<pre>';
		// vaR_dump($rows);
		// die();
		return $rows;
	}

	public function getDocumentOrganisasi($idOrganisasi){
		$this->load->model('Aktifitas_model');
		$db2 = $this->load->database('upload', true);
		$CI = & get_instance();
        $config = $CI->config->item('utils');
		$sql = "Select distinct d.id,d.* from users u,documents d where  d.parent IS NULL and u.id_organisasi='$idOrganisasi' and d.user_id=u.id and d.is_deleted=0 order by d.updated_at DESC";
		$document = dbGetRows($sql);	
		if(!$document){
			return null;
		}else{
			$temp = 0;
			$tempResult = 0;
			$result = array();
			foreach($document as $obj){
				$idDoc = $obj['id'];
				if($obj[show]==0){
					$sql = "select * from documents where parent=$idDoc and show=1";
					$res = dbGetRows($sql);
					if(!empty($res))
						$idDoc = $res[0]['id'];
				}
				
				$sql = "Select d.*,u.name,l.kode_lokasi,k.nama_klasifikasi,j.nama_jenis from users u,documents d,lokasis l,klasifikasis k,jenis_document j where d.id=$idDoc and d.user_id=u.id and l.id=d.lokasi_id and k.id=d.klasifikasi_id and j.id=d.jenis_id and d.is_deleted=0 order by d.updated_at DESC";
				$res = dbGetRows($sql);
				$obj = $res[0];
				$document[$temp] = $obj;
				

				$userID = md5($obj['user_id'] . $CI->config->item('encryption_key'));
				$link = File::createLink($userID, $obj['namafile'], 'posts/files');	
				$document[$temp]['link'] = $link;
				$document[$temp]['local'] = $config['upload_dir'] . 'posts/files/' . basename($link);
				$sql = "select octet_length(isi_file) from documentsfile where trans_id=".$obj['id'];
				$idDokumen = $obj['id'];
				$file = dbGetRows($sql, $db2);
				$document[$temp]["ukuran"] = $file[0]['octet_length'];
				$sql = "select distinct dt.id, t.name from document_tag dt, tags t where dt.document_id = $idDokumen and t.id=dt.tag_id";
				$tags = dbGetRows($sql);
				$tags = $this->createDataList($tags);
				$document[$temp]['tags'] = $tags;
				$document[$temp]['docRelated'] = $this->getRelatedDocument($idDokumen);
				if($obj['updated_by']!=NULL){
					$sql = "select * from users where id = '".$obj['updated_by']."'";
					$user = dbGetRows($sql);
					$document[$temp]["name_updated"] = $user[0]['name'];
				}
				$document[$temp]['aktifitas'] = $this->Aktifitas_model->getRecAktifitas($idDokumen);
				$result[$tempResult] = $document[$temp];
				$tempResult++;
				$temp++;
			}	
			// echo json_encode($result);
			return $result;
		}
		//return null;
	}

	public function getParentDocument($idDoc){
		$sql = "select * from documents where id='$idDoc'";
		$rows = dbGetRows($sql);
		if($rows){
			if($rows[0]['parent']!=null)
				return $rows[0]['parent'];
			else
				return $idDoc;
		}else{
			return $idDoc;
		}
	}

	public function getShowingDocument($idParent){
		$sql = "select * from documents where (parent=$idParent and show=1) or (id=$idParent and show=1)";
		$rows = dbGetRows($sql);
		if($rows){
			return $rows[0]['id'];
		}else{
			return $idParent;
		}
	}
	
	public function listOrganisasi(){
		$idOrganisasi = SessionManagerWeb::getOrganisasiID();
		if(strcmp($idOrganisasi,"guest")!=0){
			$sql = "WITH RECURSIVE getallorganisasi as 
					(select id_organisasi,nm_lemb,id_organisasi_induk from organisasi where id_organisasi='$idOrganisasi' UNION ALL select o.id_organisasi,o.nm_lemb, o.id_organisasi_induk from organisasi as o inner join getallorganisasi as os on (o.id_organisasi_induk=os.id_organisasi))
					select Distinct id_organisasi,* from getallorganisasi where true";
			$row = dbGetRows($sql);
			$org = array();
			$temp = 0;
			$size = sizeof($row);
			foreach($row as $obj){
				$idInduk=$obj['id_organisasi_induk'];
				$org[$temp]['id_organisasi'] = $obj['id_organisasi'];
				$org[$temp]['nama'] = $obj['nm_lemb'];
				$org[$temp]['used'] = "0";
				$temp++;
				if(strcmp($obj['id_organisasi_induk'],"")==0){
					$org[$temp-1]['parent'] = -1;
				}else{
					$i=0;
					for($i=0;$i<$size;$i++){
						if(strcmp($row[$i]['id_organisasi'],$idInduk)==0){
							$org[$temp-1]['parent'] = $i;
							break;
						}
					}
					if($i >= $size){
						$org[$temp-1]['parent'] = -1;
					}
				}
			}
			return $org;
		}else{
			return null;
		}
	}

	public function getRevisi($id){
		$sql = "select * from documents where parent=$id order by revisi desc";
		$rows = dbGetRows($sql);
		$revisi = 0;
		foreach($rows as $obj){
			if($revisi < $obj['revisi'])
				$revisi = $obj['revisi'];
		}
		return $revisi;
	}

	public function getPrevDocument2($id){
		$sql = "select * from documents where id=$id";
		$rows = dbGetRows($sql);
		$document = array();
		$revision = array();
		if(!empty($rows)){
			$parent = $rows[0]['parent'];
			$tempIndex = 0;
			if($parent != null){
				$sql = "select distinct d.*,u.name as name_updated from documents d,users u where d.id=$parent and u.id=d.user_id and d.is_delete=0";
				$parentDoc = dbGetRows($sql);
				if(!empty($parentDoc)){
					$document[$tempIndex] = $parentDoc[0];
					$revision[$tempIndex] = $parentDoc[0]['revision'];
					$tempIndex++;
				}
				$sql = "select distinct d.*,u.name as name_updated from documents d,users u where d.parent=$parent and d.id!=$id and u.id=d.user_id and d.is_delete=0";
				$rows = dbGetRows($sql);
				if(!empty($rows)){
					foreach($rows as $obj){
						$document[$tempIndex] = $obj;
						$revision[$tempIndex] = $obj['revision'];
						$tempIndex++;
					}
				}
			}else{
				$sql = "select distinct d.*,u.name as name_updated from documents d,users u where d.parent=$id and u.id=d.user_id and d.is_delete=0";
				$parentDoc = dbGetRows($sql);
				if(!empty($parentDoc)){
					foreach($parentDoc as $obj){
						$document[$tempIndex] = $obj;
						$revision[$tempIndex] = $obj['revision'];
						$tempIndex++;
					}
				}
			}
			if(!empty($revision))
				array_multisort($revision, SORT_DESC, $document);
		}
		return $document;
	}

	public function getPrevDocument($id){
		$sql = "with recursive doc_recursive as (
				   select id, document, parent, 1 as revision, array[id] as document_ids
				   from documents 
				   where parent is null
				   union all
				   select d.id, d.document, d.parent, p.revision + 1, p.document_ids||d.id
				   from documents d
				     join doc_recursive p on d.parent = p.id
				)
				select *
				from doc_recursive
				where id=$id
				order by document_ids;";
		$row = dbGetRow($sql);
		$document = array();
		$revision = array();
		
		if(!empty($row)){
			$row['document_ids'] = substr($row['document_ids'], 1, -1);
			$row['document_ids'] = explode(',',$row['document_ids']);
			$tempIndex = 0;
			foreach ($row['document_ids'] as $key => $value) {
				$parent = $value;
				// $parent = $rows[0]['parent'];
				
				if($parent != null and $parent!=$id){
					$sql = "select distinct d.*,u.name as name_updated from documents d,users u where d.id=$parent and u.id=d.user_id and d.is_delete=0";
					$parentDoc = dbGetRows($sql);
					if(!empty($parentDoc)){
						$document[$tempIndex] = $parentDoc[0];
						$revision[$tempIndex] = $parentDoc[0]['revision'];
						$tempIndex++;
					}
				}
			}
			
			if(!empty($revision))
				array_multisort($revision, SORT_DESC, $document);
		}
		
		return $document;
	}

	public function rollback($id,$asalID){
		$dataUpdate = array();
		$dataUpdate['show'] = 0;
		$this->updateDocument($asalID,$dataUpdate);

		$dataUpdate = array();
		// $doc = $this->get_by('id',$asalID);
		// $dataUpdate['revisi'] = $doc['revisi'] + 1;
		$dataUpdate['show'] = 1;
		$this->updateDocument($id,$dataUpdate);
	}

	public function search($keyword){
		$sql = "select * from documents where isShow=1";
	}

	public function getNamaKlasifikasi($klasifikasi){
		// $sql = "select * from klasifikasis where (kode_klasifikasi || ' - ' || nama_klasifikasi) = '" . $klasifikasi . "'";
		$sql = "select * from klasifikasis where nama_klasifikasi = '" . $klasifikasi . "'";
		$row = dbGetRows($sql);
		$idKlasifikasi = 0;
		foreach($row as $obj){
			$idKlasifikasi = $obj['id'];
		}
		return $idKlasifikasi;
	}

	public function getKodeLokasi($lokasi){
		$sql = "select * from lokasis where kode_lokasi='".$lokasi."'";
		$row = dbGetRows($sql);
		$idLokasi = 0;
		foreach($row as $obj){
			$idLokasi = $obj['id'];
		}
		return $idLokasi;
	}

	public function getNamaJenis($jenis){
		// $sql = "select * from klasifikasis where (kode_klasifikasi || ' - ' || nama_klasifikasi) = '" . $klasifikasi . "'";
		$sql = "select * from document_types where type = '" . $jenis . "'";
		$row = dbGetRows($sql);
		$idJenis = 0;
		foreach($row as $obj){
			$idJenis = $obj['ID'];
		}
		return $idJenis;
	}
	
	public function getDocIDFromTitle($title){
		$temp = 0;
		$listID = array();
		foreach($title as $obj){
			$sql = "select id from documents where title='".$obj."' and show = 1";
			$rows = dbGetRows($sql);
			if($rows != false){
				$listID[$temp] = $rows[0]['id'];
				$temp++;
			}
		}
		return $listID;
	}

	public function insertDocumentKLT($idDocument,$data){
		$idKlasifikasi = $this->getNamaKlasifikasi($data["klasifikasi"]);
		$idLokasi = $this->getKodeLokasi($data['lokasi']);
		$idJenis = $this->getNamaJenis($data['jenis']);
		$tags = $data["tags"];
		foreach($tags as $obj){
			$sql = "select * from tags where name='$obj'";
			$row = dbGetRows($sql);
			if($row!=false){
				$date = new DateTime();
				$date1 =  $date->format('Y-m-d H:i:s');
				foreach($row as $r){
					$id = $r["id"];
					$sql = "insert into document_tag(document_id,tag_id,created_at,updated_at) values($idDocument,$id,'$date1','$date1')";
					dbQuery($sql);	
				}
			}else{
				$date = new DateTime();
				$date1 =  $date->format('Y-m-d H:i:s');
				$sql = "insert into tags(name,created_at,updated_at) values ('$obj','$date1','$date1')";
				dbQuery($sql);
				$sql = "select * from tags where name='$obj'";
				$row = dbGetRows($sql);
				foreach($row as $r){
					$id = $r['id'];
					$sql = "insert into document_tag(document_id,tag_id,created_at,updated_at) values($idDocument,$id,'$date1','$date1')";
					dbQuery($sql);
				}
			}
		}
		$this->updateDocument($idDocument,array('lokasi_id'=>$idLokasi,'klasifikasi_id'=>$idKlasifikasi,'jenis_id'=>$idJenis));
	}

	public function setRelatedDocument($asal,$listTitle){
		$data = array();
		$data['document1'] = $asal;
		foreach($listTitle as $obj){
			$data['document2'] = $obj;
			if ($this->getOne($obj, 'type_id')==5 or $this->getOne($asal, 'type_id')==5){
				$data['is_form'] = 1;
			}
			// echo "<pre>";print_r($data);echo "</pre>";
			dbInsert('document_related', $data);
		}
	}

	public function setRelatedDocument2($asal,$listTitle){
		$data = array();
		$data['document1'] = $asal;
		foreach($listTitle as $obj){
			$data['document2'] = $obj['document2'];
			if ($this->getOne($obj, 'type_id')==5 or $this->getOne($asal, 'type_id')==5){
				$data['is_form'] = 1;
			}
			// echo "<pre>";print_r($data);echo "</pre>";
			dbInsert('document_related', $data);
		}
	}

	public function setRelatedDocumentBidirectional($asal,$listTitle){
		$data = array();
		$data['document1'] = $asal;
		foreach($listTitle as $obj){
			$data['document2'] = $obj;
			dbInsert('document_related', $data);
			$data2['document1'] = $obj;
			$data2['document2'] = $asal;
			dbInsert('document_related', $data2);
		}
	}

	public function deleteRelatedWith($document_id){

	}

	public function copyTag($asal,$tujuan){
		$sql = "select * from document_tag where document_id=$asal";
		$tag = dbGetRows($sql);
		foreach($tag as $obj){
			$data = array();
			$data['document_id'] = $tujuan;
			$tagID = $obj['tag_id'];
			$date = new DateTime();
			$date1 =  $date->format('Y-m-d H:i:s');
			$sql = "insert into document_tag(document_id,tag_id,created_at,updated_at) values($tujuan,$tagID,'$date1','$date1')";
			dbQuery($sql);
		}
	}

	public function updateDocument($UserID, $document_id,$data){
		// $UserID = SessionManagerWeb::getUserID();
		$data["updated_by"] = $UserID;
		if (dbUpdate("documents",$data,"id=$document_id")){
			$this->updateDateDocument($document_id);
			if ($data['status']=='D' or $data['is_published']==1) {
				$this->load->database();
		    	$sql = "select type_id as \"type_id\" from documents where id=$document_id";
				$type_id = dbGetOne($sql);
				$this->load->model('Document_types_model');
				$count_type = (int)$this->Document_types_model->getBy($type_id, 'jumlah');
				$type['jumlah'] = $count_type+1;
				$this->Document_types_model->updateJenis($type_id, $type);
			}
			return true;
		}
		return false;
	}

	public function updateDoc($document_id, $data){
		foreach ($data as $key => $value) {
			$columns[]= $key."='".$value."' ";
		}
		$column = implode(',', $columns);
		$sql = "update documents set $column where id=$document_id";
		return dbQuery($sql);
		// return dbUpdate("documents", $data, "id=$document_id");
	}

	public function updateDocumentTag($idDocument,$data){
		$sql = "select id from document_tag where document_id = $idDocument ";
		$documentTags = dbGetRows($sql);
		if(isset($documentTags[0]))
			$size = sizeof($documentTags);
		else
			$size = 0;
		$temp = 0;
		foreach($data as $obj){
			$sql = "select * from tags where name='$obj'";
			$row = dbGetRows($sql);
			if($row!=false){
				$date = new DateTime();
				$date1 =  $date->format('Y-m-d H:i:s');
				foreach($row as $r){
					$id = $r["id"];
					if($temp<$size){
						$sql = "update document_tag set tag_id=$id where id=" . $documentTags[$temp]['id'];
						dbQuery($sql);
						$temp++;
					}else{
						$sql = "insert into document_tag(document_id,tag_id,created_at,updated_at) values($idDocument,$id,'$date1','$date1')";
						dbQuery($sql);	
					}
				}
			}else{
				$date = new DateTime();
				$date1 =  $date->format('Y-m-d H:i:s');
				$sql = "insert into tags(name,created_at,updated_at) values ('$obj','$date1','$date1')";
				dbQuery($sql);
				$sql = "select * from tags where name='$obj'";
				$row = dbGetRows($sql);
				foreach($row as $r){
					$id = $r['id'];
					if($temp<$size){
						$sql = "update document_tag set tag_id='$id' where id=" . $documentTags[$temp]['id'];
						dbQuery($sql);
						$temp++;
					}else{
						$sql = "insert into document_tag(document_id,tag_id,created_at,updated_at) values($idDocument,$id,'$date1','$date1')";
						dbQuery($sql);	
					}
				}
			}
		}
		while($temp < $size){
			$sql = "delete from document_tag where id=" . $documentTags[$temp]['id'];
			dbQuery($sql);
			$temp++;
		}
	}

	public function updateRelatedDocument($id,$related){
		$sql = "select * from document_related where document1=$id OR document2=$id";
		$rows = dbGetRows($sql);
		$listID = $related;
		$date = new DateTime();
		$date1 =  $date->format('Y-m-d H:i:s');
		$temp = 0;
		$len = sizeof($listID);
		echo $len;
		if($rows != false){
			foreach($rows as $obj){
				$idRelated = $obj['id'];
				if($temp<$len && $idRelated!=$id){
					if($obj['document1'] == $id){
						$sql = "update document_related set document2=".$listID[$temp]." where id=$idRelated";
						dbQuery($sql);
					}else{
						$sql = "update document_related set document1=".$listID[$temp]." where id=$idRelated";
						dbQuery($sql);
					}
				}else{
					$sql = "delete from document_related where id=$idRelated";
					dbQuery($sql);
				}
				$temp++;
			}
		}
		if($temp<$len){
			for($i=$temp;$i<$len;$i++){
				$sql = "insert into document_related(document1,document2,created_at,updated_at) values($id,".$listID[$i].",'$date1','$date1')";
				dbQuery($sql);
			}
		}
	}

	public function updateRelatedDocumentNew($id,$related){
		$date = new DateTime();
		$date1 =  $date->format('Y-m-d H:i:s');
		$get = "select id,document2 from document_related where document1=".$id;
		// echo $get;
		$doc2 = dbGetRows($get);
		// echo "<pre>";print_r($related);echo "</pre>";
		// echo "<pre>";print_r($doc2);echo "</pre>";
		$jumlah_related = count($doc2);
		$jumlah_data = count($related);
		$temp = 0;
		$cocok = array();
		if($doc2 != null){
			foreach ($doc2 as $d) {
				if(in_array($d['DOCUMENT2'], $related)){ // jika ada, ditampung dl
					$cocok[$temp] = $related[array_search($d['DOCUMENT2'], $related)];
				}
				else{ // jika di db ada, tp di related yang dkirim tidak ada, maka relatednya dihapus
					// echo "hapus:".$d['DOCUMENT2']."<br>";
					$sql = "delete from document_related where id=".$d['ID'];
					dbQuery($sql);
				}
				$temp++;
			}
			// echo "<pre>";print_r($cocok);echo "</pre>";
			foreach ($related as $re) { // seleksi, jika data di dalam array related ada di cocok, maka tetap, jika tidak ada maka data baru alias insert
				if(!in_array($re, $cocok)){
					$is_form = 0;
					$type = dbGetOne("select type_id from documents where id=".$id);
					if($type == Document_model::FORM){ // jika is form maka dia is formnya = 1
						$is_form = 1;
					}
					$insert = array(
								'document1'	=> $id,
								'document2'	=> $re,
								'is_form'	=> $is_form
							);
					dbInsert('document_related', $insert);
				}
			}
		}
		else{
			foreach ($related as $re) { // seleksi, jika data di dalam array related ada di cocok, maka tetap, jika tidak ada maka data baru alias insert
				$is_form = 0;
				$type = dbGetOne("select type_id from documents where id=".$id);
				if($type == Document_model::FORM){ // jika is form maka dia is formnya = 1
					$is_form = 1;
				}
				$insert = array(
							'document1'	=> $id,
							'document2'	=> $re,
							'is_form'	=> $is_form
						);
				dbInsert('document_related', $insert);
			}
		}
		// $date = new DateTime();
		// $date1 =  $date->format('Y-m-d H:i:s');
		// foreach ($related as $re) {
		// 	// echo "select id from document_related where document1=".$id." and document2=".$re;
		// 	$get = "select id from document_related where document1=".$id." and document2=".$re;
		// 	$data = dbGetOne($get);
		// 	if($data == null){
		// 		$insert = array(
		// 			'document1'	=> $id,
		// 			'document2'	=> $re
		// 		);
		// 		// echo "masuk sini";die();
		// 		// echo "insert into document_related(document1,document2,created_at,updated_at) values($id,".$re.",'$date1','$date1')";die();
		// 		// $sql = "insert into document_related(document1,document2,created_at,updated_at) values($id,".$re.",'$date1','$date1')";
		// 		// dbQuery($sql);
		// 		dbInsert('document_related', $insert);
		// 	}
		// 	// echo "<pre>";print_r($data);die();
		// }
	}
	
	public function getLokasi(){
		$sql = "Select id,kode_lokasi as name from lokasis where true";
		$row = dbGetRows($sql);
		$temp = 0;
		foreach($row as $obj){
			$sql = "select count(*) from documents where lokasi_id=".$obj['id']." and show=1 and is_deleted=0";
			$row2 = dbGetRows($sql);
			$res['count'][$temp] = $row2[0]['count'];
			$temp++;
		}
		$res['lokasi'] = $this->createDataList($row);
		return $res;
	}
	
	public function getKlasifikasi(){
		$sql = "Select id,nama_klasifikasi as name from klasifikasis where true";
		$row = dbGetRows($sql);
		$temp = 0;
		foreach($row as $obj){
			$sql = "select count(*) from documents where klasifikasi_id=".$obj['id']." and show=1 and is_deleted=0";
			$row2 = dbGetRows($sql);
			$res['count'][$temp] = $row2[0]['count'];
			$temp++;
		}
		$res['klasifikasi'] = $this->createDataList($row);
		return $res;
	}

	public function getJenis(){
		$sql = "Select id,type as name from document_types where true";
		$row = dbGetRows($sql);
		$temp = 0;
		foreach($row as $obj){
			$sql = "select count(*) from documents where type_id=".$obj['id']." and show=1 and is_delete=0";
			$row2 = dbGetRows($sql);
			$res['count'][$temp] = $row2[0]['count'];
			$temp++;
		}
		$res['jenis'] = $this->createDataList($row);
		return $res;
	}
	
	public function deleteDocument($id){
		$sql = "delete from documents where id=".$id;
		dbQuery($sql);
	}

	public function softDeleteDocument($id){
		$sql = "update documents set is_delete=1 where id=$id";
		dbQuery($sql);
	}

	public function getDocumentFlow($id=NULL){
		if ($id==NULL) 
			return false;
		$sql = "select * from documents where id=$id";
		$document = dbGetRow($sql);
		return $document;
	}
	
	public function getDocumentWithLink_backup($id){
		$CI = & get_instance();
        $config = $CI->config->item('utils');

        $sql = "select document as \"document\",user_id as \"user_id\", title as \"title\" from documents where id='$id'";
        $doc = dbGetRow($sql);
        $user_id = $doc['user_id'];
		
		// $db2 = $this->load->database('upload', true);
		// $sql = "select id, trans_id, nama_file from appfile where trans_id=".$id;
		// $file = dbGetRows($sql, $db2);
		$md5_user_id = md5($user_id . $CI->config->item('encryption_key'));
		$hash = hash_hmac('sha256',$doc['document'], $user_id);

		//echo md5($userID);
		$link = " ";
		if($doc['document']){
			$arr = explode(".",$doc['document']);
			$ext = end($arr);
			if(strcmp($ext,"jpg")==0 || strcmp($ext,"png")==0 || strcmp($ext,"jpeg")==0 || strcmp($ext,"gif")==0 || strcmp($ext,"bmp")==0){
				$link = Image::createLink($md5_user_id,Image::IMAGE_LARGE, $doc['document'], 'document/photos');
				$local = $config['upload_dir'] . 'document/photos/' . basename($link);
			}
			else if (strcmp($ext,"mp4")==0 || strcmp($ext,"mpeg")==0 || strcmp($ext,"avi")==0 || strcmp($ext,"flv")==0 || strcmp($ext,"wmv")==0 || strcmp($ext,"mov")==0) {
				$link = Video::createLink($hash,  $doc['document'], 'document/videos');
				$local = $config['upload_dir'] . 'document/videos/' . basename($link);
			} else {
				// $link = File::createLink($hash, $doc['document'], 'document/files');
				$link = $config['upload_dir'].'document/files/'.$doc['document'];
				$local = $config['upload_dir'] . 'document/files/' . basename($link);
			}
			$file['trans_id'] = $id;
			$file['nama_file'] = $doc['document'];
			$file['link'] = $link;
			$file['local'] = $local;
			$file['title'] = $doc['title'];
			return $file;
		}
		return null;
	}

	public function getDocumentWithLink($id,$user_id=null){
		$CI = & get_instance();
        $config = $CI->config->item('utils');

        $sql = "select document as \"document\",user_id as \"user_id\", title as \"title\" from documents where id='$id'";
        $doc = dbGetRow($sql);
        $user_id = $doc['user_id'];
		
		// $db2 = $this->load->database('upload', true);
		// $sql = "select id, trans_id, nama_file from appfile where trans_id=".$id;
		// $file = dbGetRows($sql, $db2);
		$md5_user_id = md5($user_id . $CI->config->item('encryption_key'));
		$hash = hash_hmac('sha256',$doc['document'], $user_id);

		//echo md5($userID);
		$link = " ";
		if($doc['document']){
			$arr = explode(".",$doc['document']);
			$ext = end($arr);
			if(strcmp($ext,"jpg")==0 || strcmp($ext,"png")==0 || strcmp($ext,"jpeg")==0 || strcmp($ext,"gif")==0 || strcmp($ext,"bmp")==0){
				$link = Image::createLink($md5_user_id,Image::IMAGE_LARGE, $doc['document'], 'document/photos');
				$local = $config['upload_dir'] . 'document/photos/' . basename($link);
			}
			else if (strcmp($ext,"mp4")==0 || strcmp($ext,"mpeg")==0 || strcmp($ext,"avi")==0 || strcmp($ext,"flv")==0 || strcmp($ext,"wmv")==0 || strcmp($ext,"mov")==0) {
				$link = Video::createLink($hash,  $doc['document'], 'document/videos');
				$local = $config['upload_dir'] . 'document/videos/' . basename($link);
			} else {
				if (class_exists('SessionManagerWeb')){
		            $iduser = SessionManagerWeb::getUserID();
		        }
		        else{
		        	$iduser = $user_id;
		        }
				
				// $link = File::createLink($hash, $doc['document'], 'document/files');
				// $link = $config['upload_dir'].'document/files/'.$doc['document'];
				$real_link = $config['upload_dir'].'document/files/'.$doc['document'];
				$real_encryption = $this->getRealEncryption($id);
				$link = $this->getEncryption($doc['document'],$iduser);
				$link_watermark = $config['upload_dir'].'document/files/'.$arr[0].'_'.$iduser.'_watermarked.pdf';
				$local = $config['upload_dir'] . 'document/files/' . basename($link);
			}
			$file['trans_id'] = $id;
			$file['nama_file'] = $doc['document'];
			$file['link'] = $link;
			$file['real_link'] = $real_link;
			$file['real_encryption'] = $real_encryption;
			$file['watermark']		= $arr[0].'_'.$iduser.'_watermarked.pdf';
			$file['link_watermark'] = $link_watermark;
			$file['local'] = $local;
			$file['title'] = $doc['title'];
			return $file;
		}
		return null;
	}
	
	function insertPostFile($id, $file, $file_ori) {
		$record = array();
		$id = (int) $id;
        // $record['id'] = $id;
		// $record['file'] = $file;
		// $ukuran = strlen($file_ori);
        // $isi_file = file_get_contents($file_ori);
		// $hash = hash_hmac('sha256', $isi_file.$this->config->item('salt_key'), SessionManagerWeb::getUserID());
		$hash = hash_hmac('sha256',$file, SessionManagerWeb::getUserID());
		if($this->checkUniqeDocument($hash)){
			$date = new DateTime();
			$data["document"] = $file;
			$data["updated_at"] =  $date->format('Y-m-d H:i:s');
			$data["hash"] = $hash;
			return dbUpdate("documents",$data,"id=$id");

			// $isi_file = pg_escape_bytea($isi_file);
			// $db2 = $this->load->database('upload', true);
			// $sql = "insert into documentsfile (trans_id, nama_file,ukuran,isi_file)
			// 		values($post_id, '$file', $ukuran,'$isi_file') ";          
			// dbQuery($sql, $db2);
			// return true;
		}
		return false;
    }    

    function insertPostImage($id, $nama_file, $file_ori) {
		// $db2 = $this->load->database('upload', true);

        $record = array();
        $id = (int) $id;
        // $record['id'] = $id = (int) $id;
        // $record['image'] = $nama_file;

        // $ukuran = strlen($file_ori);
        // $isi_file = file_get_contents($file_ori);
		// $hash = hash_hmac('sha256', $isi_file.$this->config->item('salt_key'), SessionManagerWeb::getUserID());
		$hash = hash_hmac('sha256', $nama_file, SessionManagerWeb::getUserID());
		if($this->checkUniqeDocument($hash)){
			$date = new DateTime();
			$data["document"] = $nama_file;
			$data["updated_at"] =  $date->format('Y-m-d H:i:s');
			$data["hash"] = $hash;
			return dbUpdate("documents",$data,"id=$id");

			// $isi_file = pg_escape_bytea($isi_file);
			// $db2 = $this->load->database('upload', true);
			// $sql = "insert into documentsfile (trans_id, nama_file,ukuran,isi_file)
			// 		values($post_id, '$nama_file', $ukuran,'$isi_file') ";          
			// dbQuery($sql, $db2);
			// return true;
		}
		return false;
    }

    function updateDocumentWithFile($id, $file, $file_ori, $user_id) {
    	// $db2 = $this->load->database('upload', true);

		$record = array();
		$id = (int) $id;
		$ukuran = filesize($file_ori);
        $isi_file = file_get_contents($file_ori);

		$hash = hash_hmac('sha256',$isi_file.$this->config->item('salt_key'), $user_id);
			
		// Nama Doc
		$data = array();
		$data["document"] = $file;
		$data["hash"] = $hash;
		// set is_converted excel jadi 1
		$arr_files = explode('.', $data['document']);
		$ext  = end($arr_files);
		if ($ext=='xls' || $ext=='xlsx' || $ext=='pdf'){
			$data['is_converted']=1;
		}
		$data = array_change_key_case($data, CASE_UPPER);
		dbUpdate("documents",$data,"id=$id");

		$encryptions = array();
        $encryptions['document_id'] = (int)$id;
        $encryptions['encryption'] = hash_hmac('sha256',$hash.$id, date("d-m-Y h:i:sa"));
        $sql = " select 1 from document_hash where document_id=".$encryptions['document_id'];
        if (dbGetOne($sql)){
            $document_id = $encryptions['document_id'];
            unset($encryptions['document_id']);
            dbUpdate('document_hash', $encryptions, "document_id=$document_id");
        } else{
            dbInsert('document_hash', $encryptions); 
        }
		
		return true;
    } 
	
	function checkUniqeDocument($hash){
		$sql = "Select * from documents where hash='$hash' and is_delete!=1";
		$row = dbGetRows($sql);
		if($row){
			return false;
		}
		return true;
	}

    function checkCreateImage($id, $trans_id, $file_name, $folder, $is_comment=0) {
        $filename =  Image::getFileName($id, Image::IMAGE_ORIGINAL, $file_name);
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/' ;
        $file = $path . '/' . $filename;

        $CI->load->library('upload');
        $CI->load->library('image_lib');
        $config['file_name'] = $filename;
        $config['remove_spaces']=TRUE;
        $upload = new CI_Upload($config);

        if (!file_exists($file)) {
            // die($file);
            $db2 = $CI->load->database('upload', true);
            $sql = "select * from appfile where trans_id=$trans_id";
            $filter_type = " and trans_type='P'";
            if ($is_comment)
                $filter_type =" and trans_type='C'";
            $sql .=$filter_type;

            $files = dbGetRows($sql, $db2);

            foreach ($files as $file2) {
                $isi_file = pg_unescape_bytea($file2['isi_file']);
                $ukuran = $file2['ukuran'];
                $mime = get_mime_by_extension($file2['nama_file']);
                $tmpfile = tempnam(sys_get_temp_dir(), "helpdesk");
                file_put_contents($tmpfile, $isi_file);
                $imageConfig = array('source_image' => $tmpfile);

                list($width, $height) = Image::getProperty($id, Image::IMAGE_ORIGINAL, $file_name, $folder);
                foreach (array(Image::IMAGE_ORIGINAL,Image::IMAGE_LARGE, Image::IMAGE_MEDIUM, Image::IMAGE_SMALL, Image::IMAGE_THUMB) as $size) {
                    $file_name = $file2['nama_file'];
                    $imageConfig['new_image'] = $path . Image::getFileName($id, $size, $file_name);

                    list($imageConfig['width'], $imageConfig['height']) = explode('x', $ciConfig['image'][strtolower($size)]);
                    if ($width > $height) {
                        unset($imageConfig['width']);
                    } else {
                        unset($imageConfig['height']);
                    }

                    $image = new CI_Image_lib();
                    $image->initialize($imageConfig);
                    $image->resize();
                    $image->clear();          
                
                }

                $imagick = new Imagick();
                $file = $path . Image::getFileName($id, Image::IMAGE_MEDIUM, $file_name);

                $imagick->readImage($file);
                
                $iWidth = $imagick->getImageWidth();
                $iHeight = $imagick->getImageHeight();
                
                if($iWidth != $iHeight) {
                    $x = $y = 0;
                    if($iWidth > $iHeight) {
                        $x = round(($iWidth-$iHeight)/2);
                        $iWidth = $iHeight;
                    }
                    else {
                        $y = round(($iHeight-$iWidth)/2);
                        $iHeight = $iWidth;
                    }

                    $imagick->cropImage($iWidth, $iHeight, $x, $y);
                }
                
                $size = 150;
                if(!empty($size)) {
                    $iWidth = $iHeight = $size;
                    $imagick->scaleImage($iWidth, $iHeight);
                }
                
                $file = str_replace('medium', 'rounded', $file);
                file_put_contents($file, $imagick);                
            }

        }
    }

    function checkCreateFile($id, $trans_id, $file_name, $folder, $is_comment=0) {
        $filename =  File::getFileName($id, $file_name);
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/' ;
        $file = $path . '/' . $filename;

        $CI->load->library('upload');
        $config['file_name'] = $filename;
        $config['remove_spaces']=TRUE;
        $upload = new CI_Upload($config);
        if (!file_exists($file)) {
            
            $db2 = $CI->load->database('upload', true);
            $sql = "select * from appfile where trans_id=$trans_id";
            $filter_type = " and trans_type='P'";
            if ($is_comment)
                $filter_type =" and trans_type='C'";
            $sql .=$filter_type;

            $files = dbGetRows($sql, $db2);

            foreach ($files as $file2) {
                $isi_file = pg_unescape_bytea($file2['isi_file']);
                $file = $path . File::getFileName($id, $file_name);
                file_put_contents($file, $isi_file);           
            }
        }
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $this->_database->trans_begin();
        $create = parent::create($data, $skip_validation, $return);
        if ($create) {
        	$this->_database->trans_commit();
        	return $create;
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function updateDateDocument($document_id){
    	$sql = "update documents set
				updated_at = sysdate
				where id=$document_id";	
		dbQuery($sql);
    }

    public function updatePublishedDocument($document_id){
    	$sql = "update documents set
				published_at = sysdate
				where id=$document_id";	
		dbQuery($sql);
    }


    public function updateValidityDate($document_id){
    	$sql = "update documents set
				validity_date = add_months(sysdate, +12)
				where id=$document_id";
		dbQuery($sql);
    }

    public function updateColumnDateBy($document_id, $column, $value){
    	$sql = "update documents set
    			$column = to_date('$value','DD-MM-YYYY')
    			where id=$document_id
    			";
    	dbQuery($sql);
    }

    public function setToObsolete($document_id){
    	$sql = "update documents set
				show = 0,
				is_obsolete = 1
				where id=$document_id";
		dbQuery($sql);
    }

    public function updateDateBy($document_id, $column){
    	$sql = "update documents set
				$column = sysdate
				where id=$document_id";	
		dbQuery($sql);
    }

    public function insertToDocumentForm($data=array()){
    	dbInsert('document_forms' ,$data);
    }

    public function getComment($document_id, $workflow_task){
    	$sql = "select df.reason as \"reason\", df.workflow_task as \"workflow_task\", u.name as \"user_name\", df.delegated_user_id as \"delegated_user_id\" from document_forms df 
    		left join users u on u.id=df.user_id
    		where df.document_id=$document_id and df.workflow_task=$workflow_task";
    	$comments = dbGetRows($sql);
    	$this->load->model('User_model');
    	foreach ($comments as $key => $comment) {
    		if ($comment['delegated_user_id']==NULL)
    			continue;
    		$comments[$key]['delegated_user_name'] = $this->User_model->getBy($comment['delegated_user_id'], 'name');
    	}
    	return $comments;
    }

    public function deleteComment($document_id,$urutan){
    	$sql = "delete from document_forms where document_id=$document_id";
    	dbQuery($sql);
    }

    public function getDocUrl($url){
    	$sql ="select id as \"id\", url_encrypted as \"url_encrypted\", url as \"url\", expired as \"expired\" 
    			from document_urls where url='$url'  ";
    	$urls = dbGetRow($sql);
    	return $urls;
    }

    public function checkDocUrl($url){
    	$sql ="select 1 from 
    			document_urls where url='$url'  ";
    	$urls = dbGetOne($sql);
    	return $urls;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            return $this->_database->trans_commit();
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getDocumentISOS($document_id){
    	$sql = "select document_iso.id as \"iso_id\",document_iso.iso as \"iso_name\"
    			from document_isos 
    			left join document_iso on document_iso.id=document_isos.iso_id 
    			where document_isos.document_id=$document_id
    			order by document_iso.id ";
    	$rows = dbGetRows($sql);
    	$isos = array();
    	foreach ($rows as $key => $row) {
            $isos[] = $row['iso_id'];
        }
    	return $isos;
    }

    public function delete($id){
    	$sql = " update documents set is_delete=1 where id=$id ";
    	return dbQuery($sql);
    }

    public function getBy($id){
    	$sql = "select * from documents where id=$id";
    	return dbGetRow($sql);
    }

    public function getLastCode($code, $param){
    	// $sql = "select max(code_number) from documents where code like '%$code%'";
    	$sql = " select max(code_number) from documents where code like '%$code%' $param ";
    	// echo $sql;die();
    	$code_number = dbGetOne($sql);
    	// echo "code_number lama:".$code_number."<br>";
    	if ($code_number==NULL){
			$code_number = 1;
		}else {
			$code_number = (int)$code_number;
			$code_number = $code_number+1;
			// $code_number++;
		}
    	$last_code = str_pad($code_number, 3, '0', STR_PAD_LEFT);
    	// echo "last code:".$code_number."<br>";die();
    	return $last_code;
    }

    public function generateStatistikTotalType(){
    	$this->load->model('Unitkerja_model');
    	$companies = $this->Unitkerja_model->getAll(" and parent='0' and LENGTH(ltrim(id, '0'))<5 ");
    	$this->load->model('Document_types_model');
    	$types = $this->Document_types_model->getAllJenis();
    	$data = array();
    	foreach ($companies as $company) {
    		$company_id = ltrim($company['id'], '0');
    		foreach ($types as $type) {
    			$type_id = $type['id'];
    			$sql = "select count(1) from documents d left join unitkerja u on u.id=d.unitkerja_id where u.company='$company_id' and d.is_published=1 and d.show=1 and d.type_id=$type_id";
	    		$data[$company['id']][$type_id] = dbGetOne($sql);
    		}
    	}
    	foreach ($data as $key => $d) {
    		$record = array();
    		$record['company'] = $key;
    		$record['total'] = json_encode($d);
    		$sql = "select 1 from statistic_total_type where company='".$key."'";
    		if (dbGetOne($sql)){
    			unset($record['company']);
    			dbUpdate('statistic_total_type', $record, "company='$key'");
    		} else {
    			dbInsert('statistic_total_type', $record);
    		}
    	}
    	return $data;
    }

    public function getStatistikTotalType($company){
    	$sql = "select s.*, u.name from statistic_total_type s 
    			left join unitkerja u on u.id=s.company
    			where s.company='$company'";
    	$row = dbGetRow($sql);
    	$row['TOTAL'] = json_decode($row['TOTAL'], true);
    	return $row;
    }

    public function getDocumentPopuler(){
    	$sql = "select * from (select d.id, d.title,u.name, d.total_view from documents d
    			inner join unitkerja u on d.unitkerja_id = u.id where d.total_view!=0
    			order by d.total_view desc) where rownum<=5";
    	$row = dbGetRows($sql);
    	return $row;
    }

    public function updateDocumentPopuler($id){
    	$sql = "UPDATE documents SET total_view =total_view+1 where id=".$id;
    	dbQuery($sql);
    }

    // public function getPendingDocEvaluation($unitkerja_id){
    // 	$sql = "select count(d.id) jumlah, u.name from documents d left join unitkerja u on d.unitkerja_id= u.id where sysdate >= TO_CHAR(add_months(d.validity_date, -12), 'DD-MM-YYYY') and d.workflow_id!=2 and u.unitkerja_id= '".$unitkerja_id."' group by u.name";
    // 	$row = dbGetRow($sql);
    // 	return $row;
    // }

    public function getPendingDocEvaluation($company){
    	$sql = "select d.*, u.name,u.\"LEVEL\", u.parent parentcompany from documents d left join unitkerja u on d.unitkerja_id= u.id where sysdate >= TO_CHAR(add_months(d.validity_date, -1), 'DD-MM-YYYY') and d.workflow_id!=2 and u.company= '".$company."'";
    	$row = dbGetRows($sql);
    	return $row;
    }

    public function getPendingDocApprove($company_id){
    	$sql = "select d.*, u.name from documents d left join unitkerja u on d.unitkerja_id = u.id where d.workflow_id=2 and d.status!='D' and d.is_obsolete=0 and d.show=1 and u.company='".$company_id."' ";
    	$row = dbGetRows($sql);
    	return $row;
    }

    public function getChangeWorkunit($document_id){
    	$sql = " select creator as \"creator\", approver as \"approver\", creator_verification as \"creator_verification\", approver_verification as \"approver_verification\", unitkerja as \"unitkerja\" from document_changeunit where document_id=$document_id ";
    	$row = dbGetRow($sql);
    	if($row){
    		if ($row['creator']==1 and $row['approver']==1){
	    		return 'B';
	    	} 
	    	else if ($row['creator']==1){
	    		return 'C';
	    	} 
	    	else if ($row['approver']==1){
	    		return 'A';
	    	} 
	    	else if($row['unitkerja'] == 1){
	    		return 'X';
	    	}
	    	else {
	    		return 'O';
	    	}
    	}
    	else{
    		return 'O';
    	}
    	
    }

    public function getChangeWorkunitVerification($document_id){
    	$sql = " select creator_verification as \"creator\", approver_verification as \"approver\" from document_changeunit where document_id=$document_id ";
    	$row = dbGetRow($sql);
    	return $row;
    }

    public function changeUnitMoved($document_id){
    	$sql = " select is_moved as \"is_moved\" from document_changeunit where document_id=$document_id ";
    	$row = dbGetOne($sql);
    	return $row;
    }

    public function getChangeWorkunitUserId($document_id){
    	$sql = " select creator_id as \"creator_id\", approver_id as \"approver_id\" from document_changeunit where document_id=$document_id ";

    	$row = dbGetRow($sql);
    	return $row;
    }

    public function getChangeunitRejection($document_id){
    	$sql = " select creator_reason as \"creator_reason\", approver_reason as \"approver_reason\" from document_changeunit where document_id=$document_id ";
    	$row = dbGetRow($sql);
    	return $row;
    }

    public function getAllWorkunitChange($user_id){
    	$sql = " select dc.document_id as \"document_id\", d.title as \"title\", dc.creator as \"creator\", dc.approver as \"approver\" 
		    	from document_changeunit dc
		    	left join documents d on d.id=dc.document_id
		    	where ((dc.creator_id='$user_id' and dc.creator='1') or (dc.approver_id='$user_id' and dc.approver='1')) and d.title is not null";
		$rows = dbGetRows($sql);
		foreach ($rows as $key => $value) {
			$sql = " select \"type_name\" from documents_view where \"document_id\"='".$value['document_id']."'" ;
			$rows[$key]['title'] = dbGetOne($sql)." <b style='color:black'>\"".$value['title']."\"</b>";
		}
		return $rows;
    }

    // Get Semua Pergantian Workunit di company
    // Untuk Tampilan List
    public function getAllUnitChange($company){
    	$sql = "select dc.id as \"id\",dc.document_id as \"document_id\", d.title as \"title\", d.code as \"code\" ,dc.creator as \"creator\",  dc.unitkerja as \"unitkerja\",dc.unitkerja_id as \"unitkerja_id\",
    			dc.approver as \"approver\", d.creator_id as \"creator_id\", d.creator_jabatan_id as \"creator_jabatan_id\", d.approver_id as \"approver_id\", d.approver_jabatan_id as \"approver_jabatan_id\",uk.company as \"company\", uk.name as \"unitkerja_name\" from document_changeunit dc 
				left join documents d on d.id=dc.DOCUMENT_ID
				left join unitkerja uk on uk.ID=d.UNITKERJA_ID
				where (dc.unitkerja=1 and (nvl(uk.company,0)='0' or uk.company='".$company."')) or ((dc.creator=1 or dc.approver=1) and (nvl(uk.company,0)='0' or uk.company='".$company."')) $this->_filter";
		$rows = dbGetRows($sql);
		return $rows;
    }

    public function getOneChangeunit($id, $col){
    	$sql = " select $col from document_changeunit where id=$id ";
    	$column = dbGetOne($sql);
    	return $column;
    }

    public function updateTitle($document_id,$title){
    	$sql = "update documents set
				title = '".$title."'
				where id=$document_id";
		dbQuery($sql);
    }

    public function getAll(){
    	$sql = "select $this->_column from $this->_table $this->_join $this->_filter $this->_order";
    	if ($this->_show_sql){
    		die($sql);
    	}
    	return dbGetRows($sql);
    }

    public function getAllWithStatus(){
    	
		if (count($_SESSION['document_search_listall']['arr_status']) == 0 ){
			$filterstatus = '';
		} else {
			$this->_join = "left join document_evaluation de on de.document_id =x.\"document_id\" ";
			// echo "<pre>";print_r($_SESSION['document_search']['arr_status']);die();
			$filterstatus =' and (';
			foreach ($_SESSION['document_search_listall']['arr_status'] as $key) {
				if ($key == 8){  // publish
					$filterstatus .= " \"workflow_id\" = 1 and is_published=1 "; 
				}
				if($key == 1){ // document_drafting
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\" = 1 and \"workflow_urutan\"=1 "; 
				}
				if($key == 2){ // document_revising
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\" = 3 and \"workflow_urutan\"=1 "; 
				}
				if($key == 3){ // draft content review
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\" in (1,3) and \"workflow_urutan\"=2"; 
				}
				if($key == 4){ // evaluation approval
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\"=2 and \"workflow_urutan\"=5"; 
				}
				if($key == 5){ // evaluation form input
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\"=2 and \"workflow_urutan\"=1"; 
				}
				if($key == 6){ // evaluation form input drafter
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\"=2 and \"workflow_urutan\"=0"; 
				}
				if($key == 7){ // obsolete
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"is_obsolete\"=1"; 
				}
				if($key == 9){ // Review and Approval
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\"=1 and \"workflow_urutan\"=5"; 
				}
				if($key == 10){ // Verification
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\" in (1,3) and \"workflow_urutan\"=4";
				}
				if($key == 11){ // Review and Approval
					if($filterstatus != ' and ('){
						$filterstatus .= " or ";
					}
					$filterstatus .= " \"workflow_id\"=2 and \"workflow_urutan\"=4"; 
				}
				// if($key == 1){ // document_drafting
				// 	if($filterstatus != ' and ('){
				// 		$filterstatus .= " or ";
				// 	}
				// 	$filterstatus .= " \"workflow_id\" = 1 and \"workflow_urutan\"!=1 "; 
				// }
				// if($key == 2){ // evaluation
				// 	if($filterstatus != ' and ('){
				// 		$filterstatus .= " or ";
				// 	}
				// 	$filterstatus .= " \"workflow_id\" = 2 and \"workflow_urutan\"!=6 "; 
				// }
				// if($key == 3){ // revision
				// 	if($filterstatus != ' and ('){
				// 		$filterstatus .= " or ";
				// 	}
				// 	$filterstatus .= " \"workflow_id\" = 2 and de.status='".($key == 2 ? 'E' :'R')."'"; 
				// }
				
				// if($key == 5){ // 
				// 	if($filterstatus != ' and ('){ // rejected
				// 		$filterstatus .= " or ";
				// 	}
				// 	$filterstatus .= " \"is_obsolete\"=0 and \"status\"='".self::STATUS_REJECT."'"; 
				// }
				
			}
			$filterstatus .=' )';
			// $filter_status = " and \"workflow_id\" in ($_SESSION['document_search']['status']) ";
			// if(in_array($_SESSION['document_search']['arr_status'], array(2,3))){
			// 	$filter_status .= " and de.status=".;
			// }
		}
    	$sql = "select $this->_column from $this->_table x $this->_join $this->_filter $filterstatus $this->_order";
    	// die($sql);
    	if ($this->_show_sql){
    		die($sql);
    	}
    	return dbGetRows($sql);
    }

    public function getTotal($col){
    	$sql = "select $col from $this->_table $this->_filter";
    	$row = dbGetRow($sql);
    	return $row;
    }

    public function getActorIds($id, $show_all_reviewer = true, $show_reviewer=true){
    	$this->load->model("Workflow_model");

    	$sql = "select workflow_id as \"workflow_id\", workflow_urutan as \"workflow_urutan\", user_id as \"user_id\" from documents $this->_filter";
    	$doc = dbGetRow($sql);

    	$actors = $this->Workflow_model->getActors($doc['workflow_id'], $doc['workflow_urutan']);
    	$user_ids = array();
    	foreach ($actors as $actor) {
    		switch ($actor) {
	            case Role::DRAFTER:
	                $uid = $this->getOne($id, 'user_id');
	                $user_ids[$uid] = $uid;
 	            break;
	            case Role::CREATOR:
	                $uid = $this->getOne($id, 'creator_id');
	                $user_ids[$uid] = $uid;
	            break;
	            case Role::APPROVER:
	                $uid = $this->getOne($id, 'approver_id');
	                $user_ids[$uid] = $uid;
	            break;
	            case Role::REVIEWER:
		            if ($show_reviewer){
		            	$this->load->model('Document_reviewer_model');
		                $this->load->model("Document_reviews_model");
		                $reviewers = $this->Document_reviewer_model->filter("where workflow_id=".$doc['workflow_id']." and document_id=".$id)->getAllFilter();
		                foreach ($reviewers as $reviewer) {
		                	// Kalau reviewer sudah review, tidak usah notif
		                	if (!$show_all_reviewer){
		                		$is_already_review = $this->Document_reviews_model->filter("document_reviewer={$reviewer['document_reviewer']} and workflow_id={$doc['workflow_id']} and document_id=$id")->getOne('1');
			                	if ($is_already_review){
			                		continue;
			                	}
		                	}
		                	
		                	$user_ids[$reviewer['document_reviewer']] = $reviewer['document_reviewer'];
		                }
		            }
	            break;
	            case Role::DOCUMENT_CONTROLLER:
	                $this->load->model('Document_delegates_model');
	                $conditions = array(
	                    " document_id=$id ", 
	                    "workflow_id=".$doc['workflow_id'], 
	                    "workflow_urutan=".$doc['workflow_urutan']
	                );
	                $condition = implode(' and ', $conditions);
	                $delegated = $this->Document_delegates_model->filter($condition)->getBy();

	                $this->load->model('User_model');
	                $this->load->model('Unitkerja_model');

	                // Kalau pada task ini ga delegasi, pakai kabiro SMSI
	                $role = Role::DOCUMENT_CONTROLLER;
	                $unitkerja_id = $this->getOne($id, 'unitkerja_id');
	                $company = $this->Unitkerja_model->getOne($unitkerja_id, 'company');
	                // $company = str_pad($this->Unitkerja_model->getOne($unitkerja_id, 'company'), 4, '0', STR_PAD_LEFT);
	                if (!$delegated){
	                    $uid = $this->User_model->filter("where \"role\"='$role' and \"is_chief\"='X' and \"company\"='$company' ")->getOneFilter('user_id');
	                } else {
	                    $user_id = $delegated['delegate'];
	                    $uid = $this->User_model->filter(" where \"user_id\"='$user_id' ")->getOneFilter('user_id');
	                }
	                $user_ids[$uid] = $uid;
	            break;
	        }
    	}
    	return $user_ids;
    }

    public function hapusConvertChecker($document_id){
    	$sql = "delete from convert_checker where document_id=".$document_id;
    	return dbQuery($sql);
    }

    public function getCodeInForm($type_id=NULL, $prosesbisnis_id=NULL, $unitkerja_id=NULL, $form_type=NULL, $company = NULL){
    	$this->load->model('Document_types_model');
    	$data['code'] = $this->Document_types_model->getBy($type_id, 'code');

    	
    	if ($form_type!=NULL and $form_type!=''){
    		$code_type = $this->Document_types_model->getBy($form_type, 'code');
    		if (strtoupper($code_type)=='IK'){
    			$code_type = 'I';
    		}
    		$data['code'] .= $code_type;
    	}

    	if($company == NULL){
    		$this->load->model('User_model');
	    	$data['company'] = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'company');

	    	$this->load->model('Unitkerja_model');
	    	$data['company'] = $this->Unitkerja_model->getOneBy(str_pad($data['company'],8,'0', STR_PAD_LEFT), "\"INITIAL\"");
    	}
    	else{
	    	$this->load->model('Unitkerja_model');
	    	$data['company'] = $this->Unitkerja_model->getOneBy(str_pad($company,8,'0', STR_PAD_LEFT), "\"INITIAL\"");
    	}

    	$this->load->model('Document_prosesbisnis_model');
    	$data['proses_bisnis'] = $this->Document_prosesbisnis_model->getOne($prosesbisnis_id,'code');

    	$data['unit_kerja'] = '';
    	if (strtoupper($data['code'])=='IK' or $code_type =='I'){
	    	$data['unit_kerja'] = '/'.$unitkerja_id;
    	}
    	$str = $data['code'].'/'.$data['company'].'/'.$data['proses_bisnis'].$data['unit_kerja'].'/'.$data['jumlah'];
    	return $str;
    }



}

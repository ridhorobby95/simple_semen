<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_shares_model extends AppModel {

    public function getSharedDocument(){
        $sql = "select document_id from document_shares where expired_date>now()";
        $shared_documents = dbGetRows($sql);
        foreach ($shared_documents as $key => $value) {
            $document_ids[] = $value['document_id'];
        }
        // $str_document_ids ='(' .implode(',', $document_ids).')';
        // return $str_document_ids;
        return $document_ids;
    }

}

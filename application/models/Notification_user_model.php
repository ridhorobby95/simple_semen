<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_user_model extends AppModel {

    //STATUS
    const STATUS_NOTIFICATION = 'N';
    const STATUS_UNREAD = 'U';
    const STATUS_READ = 'R';

    protected $belongs_to = array(
        'User',
        'Notification'
    );

    protected $__table = 'notification_users nu';
    protected $_limit = 20;
    protected $_order = 'created_at';
    protected $_filter = '';
    protected $_columns = array(
        "nu.id" => "\"id\"",
        "nu.notification_id" => "\"notification_id\"",
        "nu.user_id" => "\"user_id\"",
        "nu.status" => "\"status\"",
        "nu.sent_at" => "\"sent_at\"",
        "nu.created_at" => "\"created_at\"",
        "nu.updated_at" => "\"updated_at\""
    );

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->__table 
                where rownum<$this->_limit $this->_filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->__table  $this->_filter";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($column='user_id'){
        $sql = "select $column from $this->__table $this->_filter";
        $row = dbGetOne($sql);
        return $row;
    }

    public function setAsUnRead($userId) {
        $condition = array('user_id' => $userId, 'status' => self::STATUS_NOTIFICATION);
        return $this->update_by($condition, array('status' => self::STATUS_READ));
    }

    public function setAsRead($userId, $referenceType, $referenceId) {
        $condition = array('user_id' => $userId);
        $this->load->model('Notification_model');
        $notifications = $this->Notification_model->get_many_by(array('reference_id' => $referenceId, 'reference_type' => $referenceType));
        if (!empty($notifications)) {
            $condition['notification_id'] = Util::toList($notifications, 'id');
            return $this->update_by($condition, array('status' => self::STATUS_READ));
        } else {
            return TRU;
        }
    }

    public function getCountMe($user_id){
        $sql = "select count(1) from $this->__table where user_id=$user_id and status='".self::STATUS_NOTIFICATION."' and reference_type!='CP'";
        return dbGetOne($sql);
    }

    // public function getCountMe($user_id) {
    //     return $this->count_by(array('user_id' => $user_id, 'status' => self::STATUS_NOTIFICATION));
    // }

}

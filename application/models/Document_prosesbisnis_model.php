<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_prosesbisnis_model extends AppModel {
    public function __construct() {
        parent::__construct();
    }

    protected $_table = 'Document_prosesbisnis';
    protected $_limit = 31;
    protected $_order = 'id';
    protected $_columns = array(
        "id" => "\"id\"",
        "prosesbisnis" => "\"prosesbisnis\"",
        "code" => "\"code\"",
        "description" => "\"description\""
    );

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function column($columns=NULL){
        $this->_columns = $columns;
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function limit($limit=20){
        $this->_limit = $limit;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col from $this->_table where id='$id'";
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col){
        $sql = "select $col from $this->_table $this->_filter";
        $column = dbGetOne($sql);
        return $column;
    }

    // function add($record) {
    //     dbInsert($this->_table, $record);
    // }

    // public function update($id,$data){
    //     dbUpdate($this->_table,$data,"id=$id");
    // }
}

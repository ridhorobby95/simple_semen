<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_notifications_model extends AppModel {

    const TYPE_DIRECT = "D";
    const TYPE_REMINDER = "R";
    const TYPE_SUMMARY = "S";
    const TYPE_CHANGEPASSWORD = "C";

    protected $_table = 'cron_notifications';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_columns = array(
        "id" => "\"id\"",
        "user_id" => "\"user_id\"",
        "message" => "\"message\"",
        "url" => "\"url\"",
        "is_send" => "\"is_send\""
    );

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function order($order='id'){
        $this->_order=$order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter";
        $column = dbGetOne($sql);
        return $column;
    }

    public function insert($user_ids = array(), $message, $url, $type=self::TYPE_DIRECT){
        foreach ($user_ids as $k => $v) {
            $cron = array();
            $cron['user_id'] = $v;
            $cron['message'] = $message;
            $cron['url'] = $url;
            $cron['type'] = $type;
            dbInsert('cron_notifications', $cron);
        }
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Workflow_model extends AppModel {
    // Task
    const PENGAJUAN = 1;
    const EVALUASI = 2;
    const REVISI = 3;
    const DOWNLOAD = 4;
    const PINDAHUNIT = 5;
    const PRAEVALUASI = 6;

    // Form
    const VERIFICATION = 'V';
    const REVIEW_CONTENT = 'R';
    const APPROVAL = 'A';
    const CREATION = 'C';
    const REVISE = 'B';
    const REVIEW_ATRIBUT = 'T';
    const EVALUATION = 'E';
    const PUBLISHED = 'P';

    public function __construct() {
        parent::__construct();
    }

    public static function name($workflow = NULL) {
        $names = array(
            self::PENGAJUAN => 'Document Submission',
            self::EVALUASI => 'Evaluasi Dokumen',
            self::REVISI => 'Revisi Dokumen',
            self::DOWNLOAD => 'Download Dokumen',
            self::PINDAHUNIT => 'Dokumen Pindah Unit Kerja'
        );
        return empty($workfow) ? $names : $names[$workflow];
    }

    public static function form($form = NULL){
        $forms = array(
            self::VERIFICATION => 'Verification',
            self::REVIEW_CONTENT => 'Review Content',
            self::APPROVAL => 'Approve',
            self::CREATION => 'Create',
            self::REVISE => 'Revision',
            self::REVIEW_ATRIBUT => 'Review Atribut'
        );
        return empty($form) ? $forms : $forms[$form];
    }

    public function getForms($workflow=NULL){
        $forms[self::PENGAJUAN] = self::form();
        return $forms[self::PENGAJUAN];
    }

    public function getTasks($workflow=NULL, $urutan=NULL, $drafter_evaluation=NULL){
        // Pengajuan
        $tasks[self::PENGAJUAN][] = array(
            'name' => 'Document Drafting',
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-file" ),
            'background' => site_url('assets/web/img/stepsatu.png'),
            'urutan' => 1,
            'edit' => 1,
            'edit_attribute' => 1,
            'user' => Role::DRAFTER,
            'form' => self::CREATION,
            'show' => 1,
            'status' => 'Draft/Draft Publish',
            'end' => 0
        );
        $tasks[self::PENGAJUAN][] = array(
            'name' => 'Attribute Review',
            'urutan' => 2,
            'edit' => 0,
            'edit_attribute' => 1,
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/loop/loop-none.png") ),
            'background' => site_url('assets/web/img/stepdua.png'),
            'user' => Role::DOCUMENT_CONTROLLER,
            'form' => self::REVIEW_ATRIBUT,
            'status' => 'Draft/Draft Publish',
            'show' => 0,
            'end' => 0
        );
        $tasks[self::PENGAJUAN][] = array(
            'name' => 'Draft Content Review',
            'urutan' => 2,
            'edit' => 0,
            'edit_attribute' => 0,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-search" ),
            'background' => site_url('assets/web/img/steptiga.png'),
            'form' => self::REVIEW_CONTENT,
            'user' => Role::REVIEWER,
            'show' => 1,
            'end' => 0
        );
        $tasks[self::PENGAJUAN][] = array(
            'name' => 'Approval',
            'urutan' => 3,
            'edit' => 1,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-check" ),
            'background' => site_url('assets/web/img/stepempat.png'),
            'form' => self::APPROVAL,
            'rejected' => 1,
            'status_if_rejected' => 'P',
            'user' => Role::CREATOR,
            'show' => 1,
            'end' => 0
        );
        $tasks[self::PENGAJUAN][] = array(
            'name' => 'Verification',
            'urutan' => 4,
            'edit' => 1,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-user" ),
            'background' => site_url('assets/web/img/steplima.png'),
            'rejected' =>1,
            'status_if_rejected' => 'P',
            'form' => self::VERIFICATION,
            'user' => Role::DOCUMENT_CONTROLLER,
            'show' => 1,
            'end' => 0
        );
        $tasks[self::PENGAJUAN][] = array(
            'name' => 'Review and Approval',
            'urutan' => 5,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-thumbs-up" ),
            'background' => site_url('assets/web/img/stepenam.png'),
            'rejected' => 1,
            'edit' => 0,
            'status_if_rejected' => 'P',
            'change_to_creator' => 1,
            'form' => self::APPROVAL,
            'user' => Role::APPROVER,
            'show' => 1,
            'next_end' => 1,
            'end' => 0
        );
        $tasks[self::PENGAJUAN][] = array(
            'name' => 'Published',
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/stamp/stamp-none.png") ),
            'background' => site_url('assets/web/img/steptujuh.png'),
            'urutan' => 6,
            'edit' => 0,
            'show' => 1,
            'end' => 1
        );

        // Evaluasi / Review
        $tasks[self::EVALUASI][] = array(
            'name' => 'Evaluation Form Input Drafter',
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/loopnew/loop-none.png") ),
            'background' => site_url('assets/web/img/stepevaluasi.png'),
            'urutan' => 0,
            'edit' => 1,
            'edit_attribute' => 1,
            'user' => Role::DRAFTER_EVALUATION,
            'form' => self::EVALUATION,
            'show' => 1,
            'status' => 'Draft/Draft Publish',
            'end' => 0
        );
        $tasks[self::EVALUASI][] = array(
            'name' => 'Evaluation Form Input',
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/loopnew/loop-none.png") ),
            'background' => site_url('assets/web/img/stepevaluasi.png'),
            'urutan' => 1,
            'edit' => 1,
            'edit_attribute' => 1,
            'user' => Role::CREATOR,
            'form' => self::EVALUATION,
            'show' => 1,
            'status' => 'Draft/Draft Publish',
            'end' => 0
        );
        $tasks[self::EVALUASI][] = array(
            'name' => 'Attribute Review',
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/loop/loop-none.png") ),
            'background' => site_url('assets/web/img/stepdua.png'),
            'urutan' => 2,
            'edit' => 1,
            'edit_attribute' => 1,
            'user' => Role::DOCUMENT_CONTROLLER,
            'form' => self::REVIEW_ATRIBUT,
            'show' => 1,
            'status' => 'Draft/Draft Publish',
            'end' => 0
        );
        $tasks[self::EVALUASI][] = array(
            'name' => 'Form Review',
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-search" ),
            'background' => site_url('assets/web/img/steptiga.png'),
            'urutan' => 2,
            'edit' => 1,
            'edit_attribute' => 1,
            'user' => Role::REVIEWER,
            'form' => self::REVIEW_CONTENT,
            'show' => 1,
            'status' => 'Draft/Draft Publish',
            'end' => 0
        );
        $tasks[self::EVALUASI][] = array(
            'name' => 'Approval',
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-check" ),
            'background' => site_url('assets/web/img/stepempat.png'),
            'urutan' => 3,
            'edit' => 1,
            'edit_attribute' => 1,
            'user' => Role::CREATOR,
            'form' => self::APPROVAL,
            'show' => 1,
            'status' => 'Draft/Draft Publish',
            'end' => 0
        );
        $tasks[self::EVALUASI][] = array(
            'name' => 'Withdraw Verification',
            'urutan' => 4,
            'edit' => 1,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-user" ),
            'background' => site_url('assets/web/img/steplima.png'),
            'rejected' =>1,
            'status_if_rejected' => 'P',
            'form' => self::VERIFICATION,
            'user' => Role::DOCUMENT_CONTROLLER,
            'show' => 1,
            'end' => 0
        );
        $tasks[self::EVALUASI][] = array(
            'name' => 'Evaluation Approval',
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-thumbs-up" ),
            'background' => site_url('assets/web/img/stepenam.png'),
            'urutan' => 5,
            'edit' => 1,
            'edit_attribute' => 1,
            'user' => Role::APPROVER,
            'form' => self::APPROVAL,
            'show' => 1,
            'next_end' => 1,
            'status' => 'Draft/Draft Publish',
            'end' => 0
        );
        $tasks[self::EVALUASI][] = array(
            'name' => 'Published',
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/stamp/stamp-none.png") ),
            'background' => site_url('assets/web/img/steptujuh.png'),
            'urutan' => 6,
            'edit' => 0,
            'show' => 1,
            'end' => 1
        );

        // Revisi
        if($drafter_evaluation != null){
            $tasks[self::REVISI][] = array(
                'name' => 'Document Revising Drafter',
                'icon' => array(
                           "is_icon" => 1,
                           "icon" => "fa fa-file" ),
                'background' => site_url('assets/web/img/stepsatu.png'),
                'urutan' => 1,
                'edit' => 1,
                'edit_attribute' => 1,
                'user' => Role::DRAFTER_REVISION,
                'form' => self::CREATION,
                'show' => 1,
                'status' => 'Draft/Draft Publish',
                'end' => 0
            );
        }
        else{
            $tasks[self::REVISI][] = array(
                'name' => 'Document Revising',
                'icon' => array(
                           "is_icon" => 1,
                           "icon" => "fa fa-file" ),
                'background' => site_url('assets/web/img/stepsatu.png'),
                'urutan' => 1,
                'edit' => 1,
                'edit_attribute' => 1,
                'user' => Role::CREATOR,
                'form' => self::CREATION,
                'show' => 1,
                'status' => 'Draft/Draft Publish',
                'end' => 0
            );
        }
        
        $tasks[self::REVISI][] = array(
            'name' => 'Attribute Review',
            'urutan' => 2,
            'edit' => 0,
            'edit_attribute' => 1,
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/loop/loop-none.png") ),
            'background' => site_url('assets/web/img/stepdua.png'),
            'user' => Role::DOCUMENT_CONTROLLER,
            'form' => self::REVIEW_ATRIBUT,
            'status' => 'Draft/Draft Publish',
            'show' => 0,
            'end' => 0
        );
        $tasks[self::REVISI][] = array(
            'name' => 'Draft Content Review',
            'urutan' => 2,
            'edit' => 0,
            'edit_attribute' => 0,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-search" ),
            'background' => site_url('assets/web/img/steptiga.png'),
            'form' => self::REVIEW_CONTENT,
            'user' => Role::REVIEWER,
            'show' => 1,
            'end' => 0
        );
        $tasks[self::REVISI][] = array(
            'name' => 'Revision Review and Approval',
            'urutan' => 3,
            'edit' => 1,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-check" ),
            'background' => site_url('assets/web/img/stepempat.png'),
            'form' => self::APPROVAL,
            'rejected' => 1,
            'status_if_rejected' => 'P',
            'user' => Role::CREATOR,
            'show' => 1,
            'end' => 0
        );
        $tasks[self::REVISI][] = array(
            'name' => 'Verification',
            'urutan' => 4,
            'edit' => 1,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-user" ),
            'background' => site_url('assets/web/img/steplima.png'),
            'rejected' =>1,
            'status_if_rejected' => 'P',
            'form' => self::VERIFICATION,
            'user' => Role::DOCUMENT_CONTROLLER,
            'show' => 1,
            'end' => 0
        );
        $tasks[self::REVISI][] = array(
            'name' => 'Approval',
            'urutan' => 5,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-thumbs-up" ),
            'background' => site_url('assets/web/img/stepenam.png'),
            'rejected' => 1,
            'edit' => 0,
            'status_if_rejected' => 'P',
            'change_to_creator' => 1,
            'form' => self::APPROVAL,
            'user' => Role::APPROVER,
            'show' => 1,
            'next_end' => 1,
            'end' => 0
        );
        $tasks[self::REVISI][] = array(
            'name' => 'Published',
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/stamp/stamp-none.png") ),
            'background' => site_url('assets/web/img/steptujuh.png'),
            'urutan' => 6,
            'edit' => 0,
            'show' => 1,
            'end' => 1
        );

        // Download
        $tasks[self::DOWNLOAD][] = array(
            'name' => 'Asking to Download',
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-download" ),
            'background' => site_url('assets/web/img/stepsatu.png'),
            'urutan' => 1,
            'edit_attribute' => 1,
            'user' => Role::DRAFTER,
            // 'form' => self::CREATION,
            'show' => 1,
            'end' => 0
        );
        
        $tasks[self::DOWNLOAD][] = array(
            'name' => 'Approval',
            'urutan' => 2,
            'edit' => 1,
            'icon' => array(
                       "is_icon" => 1,
                       "icon" => "fa fa-thumbs-up" ),
            'background' => site_url('assets/web/img/steplima.png'),
            'rejected' =>1,
            'status_if_rejected' => 'P',
            'user' => Role::DOCUMENT_CONTROLLER,
            'show' => 1,
            'next_end' => 1,
            'end' => 0
        );
        $tasks[self::DOWNLOAD][] = array(
            'name' => 'Published',
            'icon' => array(
                       "is_icon" => 0,
                       "icon" => site_url("assets/web/images/iconz/stamp/stamp-none.png") ),
            'background' => site_url('assets/web/img/steptujuh.png'),
            'user' => Role::DRAFTER,
            'urutan' => 3,
            'edit' => 0,
            'show' => 1,
            'end' => 1
        );


        // $tasks[self::EVALUASI] = array();
        // $tasks[self::REVISI] = array();
        // $tasks[self::DOWNLOAD] = array();
        // $tasks[self::PINDAHUNIT] = array();
        if ($workflow!=NULL){
            if ($urutan==NULL) {
                return $tasks[$workflow];
            } else {
                if ($urutan==1){
                    if($workflow != 2){
                        return $tasks[$workflow][0];
                    }
                    elseif($workflow == 2 && $drafter_evaluation!=null){
                        return $tasks[$workflow][0];
                    }
                    else{
                        return $tasks[$workflow][1];
                    }   
                }
                if($workflow == 2 && $drafter_evaluation!= null){
                    if($urutan == 0){
                        return $tasks[$workflow][0];
                    }
                    else{
                        return $tasks[$workflow][$urutan+1];
                    }
                    
                }
                elseif($workflow == 2 && $drafter_evaluation== null && $urutan<=1){
                    return $tasks[$workflow][$urutan];
                }
                elseif($workflow == 2 && $drafter_evaluation== null && $urutan>1){
                    return $tasks[$workflow][$urutan+1];
                }
                // if ($workflow=='2'){
                //     echo '<pre>';
                //     var_dump($tasks[$workflow]);
                //     die($urutan);
                // }
                return $tasks[$workflow][$urutan];
            }
        }
        return $tasks[self::PENGAJUAN];
    }

    function add($name, $creator) {
        $record = array();
        $record['name'] = $name;
        $record['creator'] = $creator;
        $date = new DateTime();
        $record['created_at'] =  $date->format('Y-m-d H:i:s');
        $record['updated_at'] =  $date->format('Y-m-d H:i:s');
        $record['day_limit'] = 3;
        dbInsert('workflows', $record);
    }

    public function getWorkflows(){
        $sql = "select id as \"id\", rule as \"rule\" from workflows order by id";
        $rows = dbGetRows($sql);
        foreach ($rows as &$row) {
            $row['rule'] = json_decode($row['rule'], true);
        }
        return $rows;
    }

    public function getWorkflow($id=null){
        if ($id==null)
            return false;
        $sql = "select *  from workflows w where w.id=$id";
        $workflow = array_change_key_case(dbGetRow($sql));
        return $workflow;
    }

    public function getWorkflowTasks($id=null){
        if ($id==null)
            return false;
        $sql = "select wt.key, wt.name, ''::text as \"form\", wt.keterangan as \"keterangan\",r.id as \"actor_id\", r.role as \"actor\", coalesce(wc.name,''::text) as \"category\" from workflow_tasks wt 
                left join workflow_categories wc on wc.id=wt.workflow_category_id
                left join roles r on r.id = wt.actor
                where wt.is_delete!=1 and wt.workflow_id=$id
                order by wt.key";
        $klas = dbGetRows($sql);
        foreach ($klas as $key => $value) {
            if ($value['actor_id']!=NULL)
                $klas[$key]['position'] = $value['actor_id'];
            else 
                $klas[$key]['position'] = 3;
        }
        return $klas;
    }

    public function getEvaluationForm($document_id=NULL){
        $need_revise = false;
        if ($document_id!=NULL){
            $this->load->model("Document_model");
            $diff = $this->Document_model->getOne($document_id, "(add_months(to_date(TO_CHAR(published_at, 'MM-DD-YYYY'), 'MM-DD-YYYY'), +60)-sysdate) as diff");
            if ($diff<=0){
                $need_revise = true;
            }
        }
        $forms = array();
        $forms[] = array(
            'id'=>1, 
            'text'=>'Apakah proses / kegiatan yang diatur dalam dokumen masih ada atau masih diperlukan?');
        $forms[] = array(
            'id'=>2, 
            'text'=>'Apakah proses / kegiatan yang diatur dalam dokumen efektif mencapai tujuan proses / kegiatan?');
        $forms[] = array(
            'id'=>3, 
            'text'=>'Apakah dokumen mudah dipahami dan sesuai dengan standard penulisan dokumen pada ketentuan pengendalian dokumen di perusahaan?');
        $forms[] = array(
            'id'=>4, 
            'text'=>'Apakah dokumen telah mencangkup seluruh proses atau kegiatan yang diperlukan?');
        $forms[] = array(
            'id'=>5, 
            'text'=>'Apakah dokumen masih sesuai dengan perkembangan atau perubahan kebijakan / kondisi operasional / alat?');
        $forms[] = array(
            'id'=>6, 
            'text'=>'Apakah proses / kegiatan yang diatur dalam dokumen sejalan(tidak bentrok) dengan ketentuan pada dokumen lain?');
        $forms[] = array(
            'id'=>7, 
            'text'=>'Apakah dokumen sudah mencangkup / memperhatikan seluruh aspek sistem manajemen yang relevan (Efektifitas & Efisiensi, Sustainability & Inovasi, Kualitas, K3, Lingkungan, Keamanan, Legal, Reliabilitas Data, CSR)?');
        $forms[] = array(
            'id'=>8, 
            'text'=>'Apakah cara kerja yang diatur dalam dokuemn adalah yang terbaik saat ini (belum ada yang lebih)?');
        $forms[] = array(
            'id'=>9, 
            'text'=>'Apakah umur dokumen kurang dari 5(lima) tahun?',
            'need_revise' => $need_revise
        );
        return $forms;
    }

    public function getTaskFlows($id=NULL){
        $sql = "select from_id as \"from\", to_id as \"to\", wt.workflow_category_id as \"category\", wtf.type as \"text\", from_port as \"fromPort\", to_port as \"toPort\" from workflow_task_flows wtf 
                left join workflow_tasks wt on wt.id=wtf.from_id
                where wt.workflow_id=$id";
        $nodes = dbGetRows($sql);
        foreach ($nodes as $key => $value) {
            $from =$value['from'];
            $sql_from = "select key from workflow_tasks where id=$from";
            $flows[$key]['from'] = dbGetOne($sql_from);
            $to =$value['to'];
            $sql_to = "select key from workflow_tasks where id=$to";
            $flows[$key]['to'] = dbGetOne($sql_to);
            // $flows[$key]['fromPort'] = 'B';
            // $flows[$key]['toPort'] = 'T';
            if ($value['category']!=NULL) {
                $category_id=$value['category'];
                $sql_category = "select name from workflow_categories where id=$category_id";
                $category = dbGetOne($sql_category);
                if ($category=='Decision' || $category=='Start' || $category=='End') {
                    $flows[$key]['text'] = $value['text']; 
                }
                if ($category=='Decision' and $value['text']=='N'){
                    $flows[$key]['color'] = 'red';
                }
            }
        }
        return $flows;
    }

    public function getActors($workflow_id, $workflow_urutan){
        $tasks = self::getTasks($workflow_id);
        $actors = array();
        foreach ($tasks as $task) {

            if ($workflow_urutan==$task['urutan']){
                $actors[] = $task['user'];
            }
        }  
        return $actors;
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_revises_model extends AppModel {

    protected $_table='DOCUMENT_REVISES';
    protected $_order = 'document_id';
    protected $_filter = "";
    protected $_columns = array(
        "document_id" => "\"document_id\"",
        "user_id" => "\"user_id\"",
        "text" => "\"text\""
    );
    protected $_with = "";

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }


    public function with($table, $condition){
        // $table_name = $table['name'];
        // $table_initial = $table['initital'];
        $table_with = $table['name'].' '.$table['initial'];
        $this->_with = " left join $table_with on $condition ";
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_columns = $column;
        return $this;
    }

    public function filter($filter=NULL){
        if ($filter!=NULL){
            $this->_filter = ' where '.$filter; 
        }
        return $this;
    }

    public function getAll(){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table $this->_with $this->_filter";
        $rows = dbgetRows($sql);
        return $rows;
    }

    public function getBy(){

    }

    public function getOne(){

    }

    public function getLastUser($document_id){
        // $sql_last_id = "select max(id) from document_revises where document_id=$document_id;";
        // $last_id = dbGetOne($sql_last_id);
        $sql = "select dr.user_id as \"user_id\", u.name as \"name\", dr.text as \"reason\" from document_revises dr 
                left join users u on u.id=dr.user_id
                where dr.document_id=$document_id
                order by dr.id desc";
        return dbGetRow($sql);
    }

    public function delete(){
        $sql = "delete from $this->_table $this->_filter";
        return dbQuery($sql);
    }

}
?>
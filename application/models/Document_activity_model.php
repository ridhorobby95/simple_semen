<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_activity_model extends AppModel {
    const HISTORY = 'H';
    const ACTIVITY = 'A';
    public function __construct() {
        parent::__construct();
    }

    // Mengambil Aktifitas untuk 1 dokumen
    public function getAktifitas2($dokumenID){
    	$sql = "select distinct a.id,a.*,u.name from aktifitas a,users u where a.document_id=$dokumenID and a.user_id=u.id order by a.updated_at DESC";
    	$aktifitas = dbGetRows($sql);
    	$temp = 0;
    	if($aktifitas){
	    	foreach($aktifitas as $obj){
	    		$aktifitas[$temp]['created_at']= strftime('%d %b %Y %H:%M:%S', strtotime($obj['created_at']));
	    		$temp++;
	    	}
	    }
    	return $aktifitas;
    }

    public function getActivity($document_id){
        $sql_parent = "select parent as \"parent\" from documents where id = $document_id";
        $parent = dbGetOne($sql_parent);
        $sql = "select distinct a.id as \"id\", a.user_id as \"user_id\", a.document_id as \"document_id\",a.activity as \"activity\", TO_CHAR(a.created_at, 'YYYY-MM-DD HH24:MI:SS') as \"created_at\", TO_CHAR(a.updated_at, 'YYYY-MM-DD HH24:MI:SS') as \"updated_at\", a.type as \"type\", u.name as \"name\" from document_activities a,users u where a.document_id=$document_id and a.user_id=u.id and a.type='A' order by \"id\" DESC";
        $activities = dbGetRows($sql);
        if($activities){
            $this->load->model('User_model');
            foreach($activities as $key => $activity){
                $activities[$key]['name'] = str_replace("'", "", $activities[$key]['name']);
                $activities[$key]['activity'] = str_replace("'", "", $activities[$key]['activity']);
                $activities[$key]['created_at']= strftime('%d %b %Y %H:%M:%S', strtotime($activity['created_at']));
                $user = $this->User_model->getUser($activity['user_id']);
                $activities[$key]['photo'] = $user['photo'] ? $user['photo']['thumb']['link'] : base_url('assets/web/img/nopic.png');
            }
        } else {
            $activities = array();
        }
        
        if ($parent!=NULL) {
            $parent_activities = $this->getActivity($parent);
            
            foreach ($parent_activities as $key => $value) {
                array_push($activities, $value);
            }
            
        }
        return $activities;
    }

    public function getHistory($document_id){
        $sql_parent = "select parent as \"parent\" from documents where id = $document_id";
        $parent = dbGetOne($sql_parent);
        $sql = "select distinct a.id as \"id\", a.user_id as \"user_id\", a.document_id as \"document_id\", a.activity as \"activity\", TO_CHAR(a.created_at, 'YYYY-MM-DD HH24:MI:SS') as \"created_at\", TO_CHAR(a.updated_at, 'YYYY-MM-DD HH24:MI:SS') as \"updated_at\", a.type as \"type\",u.name as \"name\" from document_activities a,users u where a.document_id=$document_id and a.user_id=u.id and a.type='H' order by \"id\" DESC";
        $activities = dbGetRows($sql);
        if($activities){
            $this->load->model('User_model');
            foreach($activities as $key => $activity){
                $activities[$key]['name'] = str_replace("'", "", $activities[$key]['name']);
                $activities[$key]['activity'] = str_replace("'", "", $activities[$key]['activity']);
                $activities[$key]['created_at']= strftime('%d %b %Y %H:%M:%S', strtotime($activity['created_at']));
                $user = $this->User_model->getUser($activity['user_id']);
                $activities[$key]['username'] = $user['username'];
                $activities[$key]['photo'] = $user['photo'] ? $user['photo']['thumb']['link'] : base_url('assets/web/img/nopic.png');
            }
        } else {
            $activities = array();
        }
        if ($parent!=NULL) {
            $parent_activities = $this->getHistory($parent);
            foreach ($parent_activities as $key => $value) {
                array_push($activities, $value);
            }
            
        } 
        return $activities;
    }

    public function getRecActivity($dokumenID){
        $this->load->model('document_model');
        $aktifitas = $this->getActivity($dokumenID);
        $sql = "select parent from documents where id = $dokumenID";
        $row = dbGetRows($sql);
        if(!$aktifitas){
            $aktifitas = array();
        }else{
            $temp = sizeof($aktifitas);
        }
        // echo json_encode($aktifitas);
        $parentID = $row[0]['parent'];
        if($parentID != null){
            $doc = $this->document_model->get_by('id',$parentID);
            if($doc){
                $newA = $this->getActivity($doc['id']);
                if($newA){
                    foreach($newA as $obj2){
                        $aktifitas[$temp]=$obj2; 
                        $temp++;    
                    }
                }
            }
            $sql = "select * from documents where parent=$parentID and id!=$dokumenID";
            $doc = dbGetRows($sql);
            if($doc){
                foreach($doc as $obj){
                    $newA = $this->getActivity($obj['id']);
                    if($newA){
                        foreach($newA as $obj2){
                            $aktifitas[$temp]=$obj2; 
                            $temp++;    
                        }
                    }
                }
            }
        }
        else{           
            $sql = "select * from documents where parent=$dokumenID";
            $doc = dbGetRows($sql);
            if($doc){
                foreach($doc as $obj){
                    $newA = $this->getActivity($obj['id']);
                    if($newA){
                        foreach($newA as $obj2){
                            $aktifitas[$temp]=$obj2; 
                            $temp++;    
                        }
                    }
                }
            }
        }
        // echo json_encode($aktifitas);
        return $aktifitas;
    }

	// Mengambil Aktifitas untuk dokumen dan parent2nya
    public function getRecAktifitas($dokumenID){
    	$this->load->model('document_model');
    	$aktifitas = $this->getActivity($dokumenID);
    	$sql = "select parent from documents where id = $dokumenID";
    	$row = dbGetRows($sql);
        if(!$aktifitas){
    		$aktifitas = array();
    	}else{
    		$temp = sizeof($aktifitas);
    	}
    	// echo json_encode($aktifitas);
		$parentID = $row[0]['parent'];
        if($parentID != null){
            $doc = $this->document_model->get_by('id',$parentID);
            if($doc){
                $newA = $this->getActivity($doc['id']);
                if($newA){
                    foreach($newA as $obj2){
                        $aktifitas[$temp]=$obj2; 
                        $temp++;    
                    }
                }
            }
            $sql = "select * from documents where parent=$parentID and id!=$dokumenID";
            $doc = dbGetRows($sql);
            if($doc){
                foreach($doc as $obj){
                    $newA = $this->getActivity($obj['id']);
                    if($newA){
                        foreach($newA as $obj2){
                            $aktifitas[$temp]=$obj2; 
                            $temp++;    
                        }
                    }
                }
            }
        }
        else{    		
            $sql = "select * from documents where parent=$dokumenID";
            $doc = dbGetRows($sql);
            if($doc){
                foreach($doc as $obj){
                    $newA = $this->getActivity($obj['id']);
                    if($newA){
                        foreach($newA as $obj2){
                            $aktifitas[$temp]=$obj2; 
                            $temp++;    
                        }
                    }
                }
            }
        }
	    // echo json_encode($aktifitas);
	    return $aktifitas;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $this->_database->trans_begin();
        $create = parent::create($data, $skip_validation, $return);
        if ($create) {
            $this->_database->trans_commit();
            // return $create;
            return TRUE;
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getLastData($col, $filter='', $order_by='',$group_by=''){
        $sql = "select $col from $this->_table $filter $group_by $order_by";
        return dbGetRow($sql);
    }

}

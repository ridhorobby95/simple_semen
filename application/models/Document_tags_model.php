<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_tags_model extends AppModel {

    protected $validation = array(
        'name' => 'required'
    );

    public function getTags(){
        $sql = "select id, name as name from document_tags order by name ASC";
        $rows = $this->_database->query($sql)->result_array();
        return $rows;
    }

    public function getListTags() {
        $rows = $this->getTags();
        $data = array();
        $temp = 0;
        foreach($rows as $row){
            $data[$temp++]['name'] = $row['name'];
        }
        return $data;
    }

    public function getDropdownTags(){
        $rows = $this->getTags();
        $data = array();
        foreach($rows as $row){
            $data[$row['id']] = $row['name'];
        }
        return $data;
    }

    public function getDocWithTag($tag,$idOrganisasi){
        $sql ="select dt.document_id from tags t, document_tag dt where t.name='$tag' and dt.tag_id = t.id";
        $rows = dbGetRows($sql);
        $temp = 0;
        if($rows){
            foreach($rows as $obj){
                $sql = "select * from documents where id='" . $obj['document_id'] . "' and id_organisasi='$idOrganisasi'";
                $res = dbGetRows($sql);
                if($res){
                    if($res[0]['is_delete']==0)
                        $temp++;
                }
            }
        }
        return $temp;
    }

    public function getDocumentTags($document_id){
        $sql = "select distinct dt.id, t.name from document_tag dt, document_tags t where dt.document_id = '$document_id' and t.id=dt.document_tag_id";
        $tags = dbGetRows($sql);
        return $tags;
    }

    public function create($idDocument, $data) {
        $tags = $data;
        foreach($tags as $obj){
            $sql = "select * from document_tags where name='$obj'";
            $row = dbGetRows($sql);
            if($row!=false){
                $date = new DateTime();
                $date1 =  $date->format('Y-m-d H:i:s');
                foreach($row as $r){
                    $id = $r["id"];
                    $sql = "insert into document_tag(document_id,document_tag_id,created_at,updated_at) values($idDocument,$id,'$date1','$date1')";
                    dbQuery($sql);  
                }
            }else{
                $date = new DateTime();
                $date1 =  $date->format('Y-m-d H:i:s');
                $sql = "insert into document_tags(name,created_at,updated_at) values ('$obj','$date1','$date1')";
                dbQuery($sql);
                $sql = "select * from document_tags where name='$obj'";
                $row = dbGetRows($sql);
                foreach($row as $r){
                    $id = $r['id'];
                    $sql = "insert into document_tag(document_id,document_tag_id,created_at,updated_at) values($idDocument,$id,'$date1','$date1')";
                    dbQuery($sql);
                }
            }
        }
    }

}
?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Atasan_local_model extends AppModel {

    protected $_table = 'atasan_local';
    protected $_limit = "";
    protected $_order = 'order by k_nopeg';
    protected $_filter = '';
    protected $_show_sql = false;
    protected $_columns = "*";
    protected $_group_by = "";
    
    public function column($col){
        $this->_columns = $col;
        return $this;
    }

    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function group_by($group_by=''){
        $this->_group_by = $group_by;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getAll(){
        $sql = "select $this->_columns from $this->_table $this->_filter $this->_limit $this->_group_by $this->_order";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $rows = dbGetRows($sql);
        return $rows;
    }
    
    public function getBy(){
        $sql = "select $this->_columns from $this->_table $this->_filter $this->_limit $this->_group_by $this->_order";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne(){
        $sql = "select $this->_columns from $this->_table $this->_filter $this->_limit $this->_group_by $this->_order" ;
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $column = dbGetOne($sql);
        return $column;
    }

    public function delete($id){
        $sql = "delete $this->_table where k_nopeg='$id'";
        return dbQuery($sql);
    }

}

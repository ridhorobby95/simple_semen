<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_sender_model extends AppModel {

    protected $belongs_to = array(
        'User',
        'Notification'
    );

    protected $__table = 'notification_senders ns';
    protected $_limit = 20;
    protected $_order = 'created_at';
    protected $_filter = '';
    protected $_columns = array(
        "ns.id" => "\"id\"",
        "ns.notification_id" => "\"notification_id\"",
        "ns.user_id" => "\"user_id\"",
        "ns.created_at" => "\"created_at\"",
        "ns.updated_at" => "\"updated_at\""
    );

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->__table 
                where rownum<$this->_limit $this->_filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getOne($column='user_id'){
        $sql = "select $column from $this->__table $this->_filter";
        $row = dbGetOne($sql);
        return $row;
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_evaluation_model extends AppModel {

    // STATUS
    const REVISI = 'R';
    const CABUT = 'C';
    const RELEVAN = 'D';

    protected $_table = 'document_evaluation';
    protected $_limit = 20;
    protected $_order = 'id';
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "id" => "\"id\"",
        "document_id" => "\"document_id\"",
        "text" => "\"text\"",
        "status" => "\"status\"",
        "dbms_lob.substr(evaluation_comment , 30000, 1 )" => "\"comment\"",
        "creator_id" => "\"creator_id\"",
        "drafter_id" => "\"drafter_id\"",
        "approver_id" => "\"approver_id\"",
        "TO_CHAR(creator_date, 'YYYY-MM-DD HH24:MI:SS')" => "\"creator_date\"",
        "TO_CHAR(approver_date, 'YYYY-MM-DD HH24:MI:SS')" => "\"approver_date\""
    );
    protected $_filter = '';

    public function __construct() {
        parent::__construct();
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter";
        $column = dbGetOne($sql);
        return $column;
    }

    public function insert($data){
        return dbInsert($this->_table, $data);
    }

    public function update($document_id, $data){
        $sql = "select user_id as \"drafter_id\", creator_id as \"creator_id\", approver_id as \"approver_id\" from documents where id=$document_id ";
        $actors = dbGetRow($sql);
        
        $data['creator_id'] = $actors['creator_id'];
        $data['approver_id'] = $actors['approver_id'];
        $drafter_evaluation = dbGetOne('select drafter_evaluation from documents where id='.$document_id);
        if($drafter_evaluation){
            $data['drafter_id'] = $drafter_evaluation;
        }
        else{
            $data['drafter_id'] = $actors['user_id'];
        }
        return dbUpdate($this->_table, $data, "document_id=$document_id");
    }

    public function deleteWith($document_id){
        $sql ="delete from $this->_table where document_id=$document_id";
        return dbQuery($sql);
    }

    public function updateDateBy($document_id, $column){
        $sql = "update document_evaluation set
                $column = sysdate
                where document_id=$document_id"; 
        dbQuery($sql);
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Statistik_unitkerja_model extends AppModel {
    
    const EVALUASI = 'E';
    const PENDING_PROCESS = 'P';
    const LEAD_TIME_PROCESS = 'T';

    protected $_table = 'statistic_unitkerja';
    protected $_limit = 20;
    protected $_order = 'unitkerja_id';
    protected $_filter = '';
    protected $_columns = array(
        "unitkerja_id" => "\"unitkerja_id\"",
        "company" => "\"company\"",
        "statistic_type" => "\"statistic_type\"", 
        "workflow_id" => "\"workflow_id\"" ,
        "pending_approve" => "\"pending_approve\"",
        "pending_evaluation" => "\"pending_evaluation\"",
        "workflow_urutan" => "\"workflow_urutan\"" ,
        "day_needed" => "\"day_needed\"",
        "type_id" => "\"type_id\"",
        "pending_total" => "\"pending_total\""
    );
    
    public function column($col){
        $this->_columns = $col;
        return $this;
    }

    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        $rows = dbGetRows($sql);
        return $rows;
    }
    
    public function getBy(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function updateStatistic($dataUpdate, $where){
        $update=dbUpdate($this->_table, $dataUpdate, $where);

    }

    public function insertStatistic($dataInsert=array()){
        // $insert=dbInsert($this->_table, $dataInsert);
        dbInsert($this->_table,$dataInsert);

    }

    public function getStatistikEvaluation($company, $page){
        $min = ($page-1)*10;
        $max = $min + 10;
        $sql = "select * from (select x.*, rownum r from (select s.*, u.name from statistic_unitkerja s left join unitkerja u on s.unitkerja_id=u.id where s.company='".$company."' and s.statistic_type='E' order by s.pending_total desc) x) y where r>=".$min." and r<=".$max;
        $row = dbGetRows($sql);
        $nextsql = "select * from (select x.*, rownum r from (select s.*, u.name from statistic_unitkerja s left join unitkerja u on s.unitkerja_id=u.id where s.company='".$company."' and s.statistic_type='E' order by s.pending_total desc) x) y where r>".$max;
        $nextrow = dbGetOne($nextsql);
        if($nextrow){
            $nextpage = 1;
        }
        else{
            $nextpage = 0;
        }
        $return = array(
            'row'       => $row,
            'nextpage'  => $nextpage
        );
        return $return;
    }

    public function deleteStatistic(){
        $sql = 'delete from statistic_unitkerja';
        dbQuery($sql);
    }
    // public function getTotalDataStatistik($company){
    //     $sql = "select count('unitkerja_id') from Statistic_unitkerja where company='".$company."' and statistic_type='E' ";
    //     $row = dbGetOne($sql);
    //     return $row;
    // }

}

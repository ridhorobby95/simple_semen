<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_verification_model extends AppModel {

    // Form
    const TEMPLATE = 1;
    const TUGAS = 2;
    const PENULISAN = 3;
    const PERUBAHAN = 4;

    protected $_table = 'document_verification';
    protected $_limit = 20;
    protected $_order = 'id';
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "id" => "\"id\"",
        "type" =>"\"type\"",
        "document_id" => "\"document_id\"",
        "dbms_lob.substr(text , 30000, 1 )" => "\"text\"",
        "is_appropriate" => "\"is_appropriate\"",
    );
    protected $_filter = '';

    public function __construct() {
        parent::__construct();
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where document_id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter";
        $column = dbGetOne($sql);
        return $column;
    }

    public function getTypeBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where document_id='$id' and is_delete=0 order by type";
        $rows = dbGetRows($sql);
        if (!$rows){
            return false;
        }
        $verifications = array();
        foreach ($rows as $key => $value) {
            switch($value['type']){
                case self::TEMPLATE:
                    $verifications['template'] = $value;
                break;
                case self::TUGAS:
                    $verifications['tugas']= $value;
                break;
                case self::PENULISAN:
                    $verifications['penulisan'] = $value;
                break;
                case self::PERUBAHAN:
                    $value['text'] = json_decode($value['text'],true);
                    $verifications['perubahan'] = $value;
                break;

            }
        }
        return $verifications;
    }

    public function getType($type=null){
        $texts = array();
        $texts[self::TEMPLATE] = 'Standar Penulisan Sesuai Template';
        $texts[self::TUGAS] = 'Kesesuaian Terhadap Tugas Pokok Fungsi';
        $texts[self::PENULISAN] = 'Keselarasan Penulisan Pada Dokumen';
        if ($type==NULL){
            return $texts;
        }
        return $texts[$type];
    }

    public function delete($document_id){
        $record = array();
        $record['is_delete'] = 1;
        dbUpdate("document_verification", $record, "document_id=$document_id");
    }

}

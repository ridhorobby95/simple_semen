<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Statistic_model extends AppModel {
    
   public function insertUserLog($idUser, $type){
        $sql = "INSERT INTO USER_LOG(ID_USER,TYPE_LOG, DATE_LOG) VALUES (".$idUser.",'".$type."',SYSDATE)";
        dbQuery($sql);
   }

   public function insertVisitorLog($idUser, $date){
        $explodeDate = explode('/', $date);
        $checkData = "SELECT * FROM VISITOR_LOG WHERE EXTRACT(month FROM DATE_LOG) = ".$explodeDate[0]." AND EXTRACT(YEAR FROM DATE_LOG) = ".$explodeDate[1]." AND ID_USER =".$idUser;
        $runCheck = dbGetRows($checkData);
        if(!$runCheck){
            $sql = "INSERT INTO VISITOR_LOG(ID_USER,DATE_LOG) VALUES (".$idUser.",SYSDATE)";
            dbQuery($sql);
        }
        
   }

   public function getDataVisitor(){
        for ($i=-4; $i <= 0 ; $i++) { 
            $date = date("m/Y", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) .$i." month" ));
            $month = date("M", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) .$i." month" ));
            $explodeDate = explode("/",$date);
            $sql = "SELECT COUNT(*) Total FROM VISITOR_LOG WHERE EXTRACT(month FROM DATE_LOG) = ".$explodeDate[0]." AND EXTRACT(YEAR FROM DATE_LOG) = ".$explodeDate[1];
            $data[abs($i)]['hasil']= dbGetRow($sql);
            $data[abs($i)]['bulan']= $month;
            $data[abs($i)]['tahun']= $explodeDate[1];
           
        }
        return $data;
   }

   public function getDataStatistic($workflow_id=1, $doc_type=0, $jenis_statistik= 'P', $column='unitkerja_id', $jenis_unitkerja='DEPT', $company){
      $db = "select s.*,u.name from statistics s left join unitkerja u on s.unitkerja_id = u.id where s.workflow_id=".$workflow_id." and s.jenis_statistik='".$jenis_statistik."' and s.jenis_unitkerja ='".$jenis_unitkerja."' and s.company='".$company."'";

      if($doc_type != 0){
        $db .= " and s.doc_type=".$doc_type;
      }
      // echo "ini doc_type=".$doc_type."<br>";
      // if($jenis_statistik='L'){
      // die($db);  
      // }
      
      return dbGetRows($db);
   }

   public function groupByStatistik($type_workunit='DEPT', $doc_type=0, $workflow_id=1, $jenis_statistik='P', $company){
      if($type_workunit == 'DEPT'){
        $column = 'dept_id';
      }
      elseif($type_workunit == 'BIRO'){
        $column = 'biro_id';
      }
      elseif($type_workunit == 'SECT'){
        $column = 'unitkerja_id';
      }
      $db = "select s.*,u.name from statistics s left join unitkerja u on s.".$column." = u.id where s.workflow_id=".$workflow_id." and s.jenis_statistik='".$jenis_statistik."' and s.".$column." != '0' and s.company='".$company."'";
      if($doc_type != 0){
        $db .= " and s.doc_type=".$doc_type;
      }
      // die($db);
      return dbGetRows($db);
   }

   public function cekDataStatistic($unitkerja_id,$workflow_id,$doc_type, $jenis_statistik ){
      $db = "select * from statistics where unitkerja_id=".$unitkerja_id." and workflow_id=".$workflow_id." and doc_type=".$doc_type." and jenis_statistik='".$jenis_statistik."'";
      return dbGetRow($db);
   }

   

}

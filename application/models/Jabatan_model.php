<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_model extends AppModel {
    const DIRECTOR = '10';
    const COMMISSIONER = '12';
    const GENERAL_MANAGER = '15';
    const SENIOR_MANAGER = '20';
    const MANAGER = '30';
    const SUPERVISOR = '40';
    const ASSOCIATE = '50';
    const ASSOCIATE_TIK = '51';
    const ASSOCIATE_TKB = '52';
    const NONESELON = '70';

    public function getEselon($eselon=NULL){
        $eselons = array(
            self::DIRECTOR => 'Director',
            self::COMMISSIONER => 'Commissioner/Commite',
            self::GENERAL_MANAGER => 'General Manager',
            self::SENIOR_MANAGER => 'Senior Manager',
            self::ASSOCIATE => 'Associate',

        );
        return (!empty($eselon)) ? $eselons[$eselon] : $eselons;
    }

    protected $_table = 'jabatan';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_with = '';
    protected $_group_by = '';
    protected $_show_sql = false;
    protected $_columns = array(
        "id" => "\"id\"",
        "name" => "\"name\"",
        "is_chief" => "\"is_chief\"",
        "unitkerja_id" => "\"unitkerja_id\"",
        "subgroup_id" => "\"subgroup_id\"",
        "subgroup_name" => "\"subgroup_name\"",
        "to_char(start_date, 'dd/mm/yyyy')" =>"\"start_date\"",
        "to_char(end_date, 'dd/mm/yyyy')" => "\"end_date\""
    );

    public function table($table='jabatan'){
        $this->_table = $table;
        return $this;
    }

    public function column($columns=array()){
        $this->_columns = $columns;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql=$show_sql;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function group_by($group_by){
        $this->_group_by = $group_by;
        return $this;
    }

    public function with($with=''){
        $this->_with = $with;
        return $this;
    }

    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getAllFilter(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_with $this->_filter $this->_group_by order by $this->_order";
        if ($this->_show_sql){
            die($sql);
        }
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col from $this->_table where id='$id'";
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col=''){
        $sql = "select $col from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }

    protected function property($row) {
        if ($this->_is_role || $this->_is_private) {
            $row['role'] = $this->getRole($row['role']);
        } else {
            foreach ($row as $key => $val) {
                if (!in_array($key, $this->public_data) && !$this->show_all) {
                    unset($row[$key]);
                }
            }
        }
        if (isset($row['photo'])) {
            $row['photo'] = Image::getImage($row['id'], $row['photo'], 'users/photos');
        }
        return $row;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $this->_database->trans_begin();
        $create = parent::create($data, $skip_validation, $return);
        if ($create) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $upload = Image::upload('photo', $create, $_FILES['photo']['name'], 'users/photos');
            }
            if ($upload === TRUE) {
                $this->_database->trans_commit();
                if($return)
                    return $create;
                else
                    return true;
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        if (array_key_exists('password', $data) || array_key_exists('password_salt', $data)) {
            unset($data['password'], $data['password_salt']);
        }
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $upload = Image::upload('photo', $primary_value, $_FILES['photo']['name'], 'users/photos');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }
}

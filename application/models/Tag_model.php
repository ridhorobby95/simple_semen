<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_model extends AppModel {

    protected $validation = array(
        'name' => 'required'
    );

    public function getTags(){
        $sql = "select id, name as name from tags order by name";
        // $rows = $this->_database->query($sql)->result_array();
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getListTags() {
        $rows = $this->getTags();
        $data = array();
        foreach($rows as $row){
            $data[$row['name']] = $row['name'];
        }
        return $data;
    }

    public function getDropdownTags(){
        $rows = $this->getTags();
        $data = array();
        foreach($rows as $row){
            $data[$row['id']] = $row['name'];
        }
        return $data;
    }
}
?>
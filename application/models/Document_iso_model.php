<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_iso_model extends AppModel {

    protected $_table='DOCUMENT_ISO';
    protected $_order = 'iso';
    protected $_show_sql = false;
    protected $_filter = "";
    protected $_columns = array(
        "id" => "\"id\"",
        "iso" => "\"iso\"",
        "iso_code" => "\"iso_code\""
    );
    protected $_with = "";

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function with($table, $condition){
        // $table_name = $table['name'];
        // $table_initial = $table['initital'];
        $table_with = $table['name'].' '.$table['initial'];
        $this->_with = " left join $table_with on $condition ";
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function table($table){
        $this->_table = $table;
        return $this;
    }

    public function column($column){
        $this->_columns = $column;
        return $this;
    }

    public function filter($filter=NULL){
        if ($filter!=NULL){
            $this->_filter = ' where '.$filter; 
        }
        return $this;
    }

    public function getAll(){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table $this->_with $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        $rows = dbgetRows($sql);
        return $rows;
    }

    public function getBy($id){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table where id=$id";
        $rows = dbgetRow($sql);
        return $rows;
    }

    public function getOne($column){
        $sql = "select $column from $this->_table $this->_filter";
        return dbGetOne($sql); 
    }  

    public function delete(){
        $sql = "delete from $this->_table $this->_filter";
        return dbQuery($sql);
    }

    public function deleteWith($document_id=NULL, $iso_id=NULL){
        $sql ="delete from $this->_table where ";
        if($document_id != null && $iso_id != null){
            $sql .= " document_id =".$document_id." and iso_id =".$iso_id;
        }
        elseif($document_id != null && $iso_id == null){
            $sql .= " document_id =".$document_id;
        }
        else{
            $sql .="iso_id =".$iso_id;
        }
        return dbQuery($sql);
    }

    function add($name, $code) {
        $record = array();
        $record['ISO'] = $name;
        $record['ISO_CODE'] = $code;
        dbInsert($this->_table, $record);
    }

}
?>
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration_model extends AppModel {

    protected $has_many = array(
        'Post',
    );

    protected $validation = array(
        'name' => 'required'
    );

    public function getConfigurations(){
        /*hanya bisa add kategori yang berstatus aktif => A*/
        // $sql = "select id as \"id\", name as \"name\", type as \"type\", created_at as \"created_at\",  updated_at as \"updated_at\" from configurations";
        $sql = "select id as \"id\", name as \"name\", type as \"type\", value as \"value\"  from configurations";
        $rows = dbGetRows($sql);
        foreach ($rows as $row) {
            if(in_array($row['type'], array('image')))
                $row['value'] = Image::getImage($row['id'], $row['value'], 'config');

        }

        return $rows;
    }

    public function getDetailConfiguration($id){
        $sql = "select id as \"id\", name as \"name\", type as \"type\", value as \"value\"  from configurations where id=$id";
        $row = dbGetRow($sql);
        if(in_array($row['type'], array('image')))
            $row['value'] = Image::getImage($row['id'], $row['value'], 'config');
        return $row;
    }

    public function getConfiguration(){
        /*hanya bisa add kategori yang berstatus aktif => A*/
        $sql = "select id as \"id\", name as \"name\", type as \"type\", value as \"value\" from configurations";
        $rows = dbGetRows($sql);
        $object = array();

        foreach ($rows as $row) {
            if(in_array($row['type'], array('image')))
                $row['value'] = Image::getImage($row['id'], $row['value'], 'config');
            else
                settype($row['value'], $row['type']);

            $object[$row['name']] = $row['value'];
        }

        return $object;
    }

    public function update($id, $data=array()){
        $record = array();
        $record['type'] = $data['type'];
        $record['value'] = $data['value'];
        dbUpdate('configurations',$data, "\"id\"=$id" );

        if ($update) {
            if($data['type'] == 'image'){
                $upload = Image::upload('upload', $primary_value, $_FILES['upload']['name'], 'config');
                if($upload === TRUE)
                    return $this->_database->trans_commit();
                else{
                    $this->_database->trans_rollback();
                    return $upload;
                }
            }else{
                return $this->_database->trans_commit();   
            }    
        }
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);

        if ($update) {
            if($data['type'] == 'image'){
                $upload = Image::upload('upload', $primary_value, $_FILES['upload']['name'], 'config');
                if($upload === TRUE)
                    return $this->_database->trans_commit();
                else{
                    $this->_database->trans_rollback();
                    return $upload;
                }
            }else{
                return $this->_database->trans_commit();   
            }    
        }

        $this->_database->trans_rollback();
        return FALSE;
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends AppModel {

    //STATUS
    const STATUS_NOT_SEND = 'N';
    const STATUS_SEND = 'S';
    //TYPE
    const TYPE_POST_DETAIL = 'PD';
    const TYPE_DAILY_REPORT_DETAIL = 'DD';
    const TYPE_ISSUE_DETAIL = 'ID';
    const TYPE_TODO_DETAIL = 'TD';
    const TYPE_GROUP_DETAIL = 'GD';
    //ACTION
    const ACTION_POST_CREATE = 'PC';
    const ACTION_COMMENT_CREATE = 'CC';
    const ACTION_GROUP_CREATE = 'GC';
    const ACTION_ISSUE_STATUS = 'IS';

    protected $many_to_many = array(
        'Notification_sender' => array('Notification', 'User')
    );
    protected $has_many = array(
        'Notification_user'
    );

    public function save($action = NULL, $message = NULL, $senderId, $receiverIds = array(), $type = NULL, $referenceId = NULL) {
        if (empty($receiverIds))
            return TRUE;

        $this->_database->trans_begin();
        $notification = $this->with(array('Notification_user', 'Notification_sender'))->get_by(array(
            'reference_id' => $referenceId,
            'action' => $action,
            'reference_type' => $type,
        ));
        if (empty($notification)) {
            $data = array(
                'reference_id' => $referenceId,
                'message' => $message,
                'action' => $action,
                'reference_type' => $type,
                'user_id' => $senderId,
            );
            $save = $this->create($data);
        } else {
            $save = $notification['id'];
        }
        if ($save) {
            $senders = Util::toList($notification['notificationSenders'], 'id');
            $receivers = Util::toList($notification['notificationUsers'], 'userId');
            if (!in_array($senderId, $senders)) {
                $this->load->model('Notification_sender_model');
                $this->Notification_sender_model->insert(array('notification_id' => $save, 'user_id' => $senderId));
            }
            $receiver = $updated = array();
            $this->load->model('Notification_user_model');
            foreach ($receiverIds as $receiverId) {
                if (!in_array($receiverId, $receivers)) {
                    $receiver[] = array(
                        'user_id' => $receiverId,
                        'notification_id' => $save,
                        'status' => Notification_user_model::STATUS_NOTIFICATION,
                        'sent_at' => NULL,
                        'created_at' => Util::timeNow(),
                        'updated_at' => Util::timeNow(),
                    );
                } else {
                    $updated[] = $receiverId;
                }
            }
            $ins = TRUE;
            if (!empty($receiver)) {
                $ins = $this->_database->insert_batch('notification_users', $receiver);
            }
            $upd = TRUE;
            if (!empty($updated)) {
                $upd = $this->Notification_user_model->update_by(array('user_id' => $updated, 'notification_id' => $save), array('status' => Notification_user_model::STATUS_NOTIFICATION, 'sent_at' => NULL));
            }
            if ($ins && $upd) {
                $this->load->model('Device_model');
                foreach ($receiverIds as $key => $idRec) {
                    if ($idRec == $senderId) {
                        unset($receiverIds[$key]);
                    }
                }
                if(!empty($receiverIds)){
                    $devices = $this->Device_model->getDeviceUser($receiverIds);

                    $notification = $this->get_many_by(array('notifications.id' => $save));
                    if (!empty($notification)) {
                        $this->load->model('User_model');
                        $user = $this->User_model->get($senderId);
                        $messages = $this->generateMessage($notification);
                        $this->sendToGCM(Util::toList($devices, 'regId'), $messages[0]['message'], $user['name'], $user['photo']['thumb']['link'], $messages[0]['referenceType'], $messages[0]['referenceId'], $messages[0]['createdAt']);
                    }
                    return $this->_database->trans_complete();
                }
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getTypeByCategory($categoryId) {
        $this->load->model('Category_model');
        if (in_array($categoryId, array(Category_model::INFO, Category_model::EVENT, Category_model::KNOWLEDGE_MANAGEMENT))) {
            $type = self::TYPE_POST_DETAIL;
        } elseif (in_array($categoryId, array(Category_model::TASK, Category_model::BUG, Category_model::ENHANCEMENT, Category_model::PROPOSAL))) {
            $type = self::TYPE_ISSUE_DETAIL;
        } elseif ($categoryId == Category_model::DAILY_REPORT) {
            $type = self::TYPE_DAILY_REPORT_DETAIL;
        } elseif ($categoryId == Category_model::TODO) {
            $type = self::TYPE_TODO_DETAIL;
        }
        return $type;
    }

    public function generate($action, $referenceId, $senderId = NULL) {
        $this->load->model('User_model');
        switch ($action) {
            case self::ACTION_POST_CREATE:
                $this->load->model('Post_model');
                $post = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user', 'Post_tag_user'))->get_by('posts.id', $referenceId);
                $senderId = $post['userId'];
                $type = $this->getTypeByCategory($post['categoryId']);

                switch ($post['type']) {
                    //case Post_model::TYPE_PUBLIC:
                        /*notifikasi untuk semua orang*/
                        //$receiverIds = Util::toList($post['postUsers'], 'id', array($senderId));
                        //$users = $this->User_model->getListFor($senderId);
                        /*-------------------------------------------------------------------------*/

                        /*notifikasi ke orang yang di tag*/
                    //    $receiverIds = Util::toList($post['postTagUsers'], 'id', array($senderId));
                        /*-------------------------------------------------------------------------*/

                    //    break;
                    case Post_model::TYPE_GROUP:
                        $receiverIds = Util::toList($post['group']['groupMembers'], 'userId', array($senderId));
                        break;
                    case Post_model::TYPE_FRIEND:
                        $receiverIds = Util::toList($post['postUsers'], 'id', array($senderId));
                        break;
                }

                foreach ($post['postTagUsers'] as $pst) {
                    $receiverIds[] = $pst['id'];
                }

                /*
                 *Notify semua user
                 */
                if (!empty($post['image'])) {
                    $message = 'memposting gambar ';
                } elseif (!empty($post['file'])) {
                    $message = 'memposting berkas "' . $post['file']['name'] . '"';
                } elseif (!empty($post['video'])) {
                    $message = 'memposting video "' . $post['video']['name'] . '"';
                } elseif (!empty($post['link'])) {
                    $message = 'memposting link "' . $post['link'] . '"';
                } elseif (!empty($post['description'])) {
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memposting "' . $str . '"';
                }
                /*-------------------------------------------------------------------------*/
                /*
                 *Notify only to tagged person
                 */
                /*
                if (!empty($post['image'])) {
                    $message = 'memberi tag kamu pada posting gambar ';
                } elseif (!empty($post['file'])) {
                    $message = 'memberi tag kamu pada posting berkas "' . $post['file']['name'] . '"';
                } elseif (!empty($post['video'])) {
                    $message = 'memberi tag kamu pada posting video "' . $post['video']['name'] . '"';
                } elseif (!empty($post['link'])) {
                    $message = 'memberi tag kamu pada posting link "' . $post['link'] . '"';
                } elseif (!empty($post['description'])) {
                    if (strlen($post['description']) > 30) {
                        $str = substr($post['description'], 0, 30) . '...';
                    } else {
                        $str = $post['description'];
                    }
                    $message = 'memberi tag kamu pada posting "' . $str . '"';
                }*/
                break;
            case self::ACTION_COMMENT_CREATE:
                $this->load->model('Comment_model');
                $comment = $this->Comment_model->get($referenceId);
                if(!empty($comment)){
                    $this->load->model('Post_model');
                    $comment['post'] = $this->Post_model->with(array('Group' => 'Group_member', 'Post_user', 'Post_tag_user'))->get_by("posts.id", $comment['postId']);

                    $cmTagUsers = $this->Comment_model->with('Post_tag_user')->get_by('comments.id', $comment['id']);
                    $comment['postTagUsers'] = $cmTagUsers['postTagUsers'];
                }

                $senderId = $comment['userId'];
                $referenceId = $comment['postId'];
                $type = $this->getTypeByCategory($comment['post']['categoryId']);
                
                switch ($comment['post']['type']) {
                    case Post_model::TYPE_PUBLIC:
                        /*notyfy all*/
                        // $users = $this->User_model->getListFor($senderId);
                        // $receiverIds = Util::toList($users, 'id');
                        /*-------------------------------------------------------------------------*/

                        /*notify tagged*/
                        $receiverIds = Util::toList($comment['postTagUsers'], 'id', array($senderId));

                        /*notify sender*/
                        if (!in_array($comment['post']['userId'], $receiverIds)) {
                            $receiverIds[] = $comment['post']['userId'];
                        }
                        /*-------------------------------------------------------------------------*/
                        break;
                    case Post_model::TYPE_GROUP:
                        $receiverIds = Util::toList($comment['post']['group']['groupMembers'], 'userId', array($senderId));
                        break;
                    case Post_model::TYPE_FRIEND:
                        $receiverIds = Util::toList($comment['post']['postUsers'], 'id', array($senderId));
                        if (!in_array($comment['post']['userId'], $receiverIds)) {
                            $receiverIds[] = $comment['post']['userId'];
                        }
                        break;
                }
                if (strlen($comment['text']) > 30) {
                    $str = substr($comment['text'], 0, 30) . '...';
                } else {
                    $str = $comment['text'];
                }
                $message = 'mengomentari "' . $str . '"';
                break;
            case self::ACTION_GROUP_CREATE:
                $this->load->model('Group_model');
                $this->load->model('Group_member_model');
                $group = $this->Group_model->with('Group_member')->get($referenceId);
                $type = self::TYPE_GROUP_DETAIL;
                foreach ($group['groupMembers'] as $member) {
                    if ($member['groupMemberRole']['id'] == Group_member_model::ROLE_OUWNER) {
                        $senderId = $member['user']['id'];
                    } else {
                        $receiverIds[] = $member['user']['id'];
                    }
                }

                $message = 'menambahkan anda';
                break;
            case self::ACTION_ISSUE_STATUS:
        		$this->load->model('Issue_model');

        		$issue = $this->Issue_model->with(array('Post_user'))->get_by('posts.id', $referenceId);
        		$status = Issue_model::getStatus();
        		$message = 'Mengganti status issue "'.$issue['title'].'" menjadi '.$status[$issue['status']];
        		
        		$receiverIds = Util::toList($issue['postUsers'], 'id', array($senderId));
        		if($senderId != $issue['userId'])
        		  $receiverIds[] = $issue['userId'];

        		$type = self::TYPE_ISSUE_DETAIL;
            break;
        }

        $this->save($action, $message, $senderId, $receiverIds, $type, $referenceId);
    }

    protected function join_or_where($row) {
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        $this->with('Notification_sender');
        $this->order_by("notifications.id", "DESC");
    }

    public function getMe($userId, $page = 0) {
        $condition = array('notification_users.user_id' => $userId);
        $this->limit(10, ($page * 10));
        $notifications = $this->get_many_by($condition);
        $ouput = $this->generateMessage($notifications);

        return $ouput;
    }

    public static function toObject($id, $message, $sender, $referenceId, $referenceType, $status, $createdAt, $updatedAt) {
        return array(
            'id' => (int) $id,
            'message' => $message,
            'sender' => $sender,
            'referenceId' => (int) $referenceId,
            'referenceType' => $referenceType,
            'status' => $status,
            'createdAt' => $createdAt,
            'updatedAt' => $updatedAt
        );
    }

    public function getNotification($userId) {
        $this->load->model('Notification_user_model');
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        return $this->order_by("notifications.created_at", "DESC")->get_many_by(array('notification_users.user_id' => $userId, 'notification_users.status' => Notification_user_model::STATUS_NOTIFICATION));
    }

    public function generateMessage($notifications) {
        $types = array();
        foreach ($notifications as $notification) {
            $types[$notification['referenceType']][] = $notification['referenceId'];
        }
        $data = array();
        foreach ($types as $key => $value) {
            switch ($key) {
                case self::TYPE_POST_DETAIL:
                    $this->load->model('Post_model');
                    $data[$key] = Util::toMapObject($this->Post_model->with(array('Group', 'Post_user', 'User'))->get_many_by(array('posts.id' => $value)), 'id');
                    break;
                case self::TYPE_GROUP_DETAIL:
                    $this->load->model('Group_model');
                    $data[$key] = Util::toMapObject($this->Group_model->get_many_by(array('id' => $value)), 'id');
                    break;
            }
        }
        $ouput = array();
        foreach ($notifications as $notification) {
            $message = NULL;
            if (!empty($notification['message']) && empty($notification['action'])) {
                $message = $notification['message'];
            } elseif (!empty($notification['action']) && !empty($notification['message'])) {
                switch ($notification['action']) {
                    case self::ACTION_POST_CREATE:
                        $post = $data[self::TYPE_POST_DETAIL][$notification['referenceId']];
                        if (!empty($post['group'])) {
                            $message = $notification['message'] . ' di grup ' . $post['group']['name'];
                        } elseif (!empty($post['postUsers'])) {
                            $message = $notification['message'] . ' ke anda';
                            if ((count($post['postUsers']) == 2)) {
                                if ($post['postUsers'][0]['id'] == $userId) {
                                    $otherName = $post['postUsers'][1]['name'];
                                } else {
                                    $otherName = $post['postUsers'][0]['name'];
                                }
                                $message .= ' dan ' . $otherName;
                            } elseif (count($post['postUsers']) > 2) {
                                $message .= ' dan ' . (count($post['postUsers']) - 1) . ' orang lainnya';
                            }
                        } else {
                            $message = $notification['message'];
                        }
                        break;
                    case self::ACTION_COMMENT_CREATE:
                        $post = $data[self::TYPE_POST_DETAIL][$notification['referenceId']];
                        $senderCount = count($notification['notificationSenders']);
                        if ($senderCount == 1) {
                            $message = $notification['message'];
                        } elseif ($senderCount == 2) {
                            $message = "dan " . $notification['notificationSenders'][0]["name"];
                        } elseif ($senderCount > 2) {
                            $message = "dan " . ($senderCount - 1) . " orang lainnya";
                        }
                        if(strpos($message, 'mengomentari') == false){
			  $message .= ' mengomentari';
                        }
                        $message .= ' pada postingan ' . $post['user']['name'];
                        if (!empty($post['group'])) {
                            $message .=" di grup " . $post['group']['name'];
                        }
                        break;
                    case self::ACTION_GROUP_CREATE:
                        $group = $data[self::TYPE_GROUP_DETAIL][$notification['referenceId']];
                        $message = $notification['message'] . ' di grup ' . $group['name'];
                        break;
                    default;
			$message = $notification['message'];
			break;
                }
            }
            $ouput[] = self::toObject($notification['id'], $message, end($notification['notificationSenders']), $notification['referenceId'], $notification[
                            'referenceType'], $notification['status'], $notification['createdAt'], $notification['updatedAt']);
        }
        return $ouput;
    }

    function sendToGCM($recepient, $message, $title, $link, $action, $actionId, $time = NULL) {
        $this->gcm->setMessage($message);

        if (is_string($recepient)) {
            $this->gcm->addRecepient($recepient);
        } else {
            foreach ($recepient as $client) {
                $this->gcm->addRecepient($client);
            }
        }

        $this->gcm->setData(array(
            'title' => $title,
            'link' => $link,
            'action' => $action,
            'actionId' => $actionId,
            'time' => empty($time) ? Util::timeNow() : $time
        ));

        $this->gcm->send();
    }

}

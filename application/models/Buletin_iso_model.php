<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Buletin_iso_model extends AppModel {

    const LEVEL_KATEGORI = 1;
    const LEVEL_SUBKATEGORI = 2;
    const LEVEL_SUBKATEGORI_2 = 3;

    protected $_table = 'buletin_iso';
    protected $_table_file = 'buletin_iso_file';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_show_sql = false;
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "id" => "\"id\"",
        "nama" => "\"nama\"",
        "\"LEVEL\"" => "\"level\"",
        "parent" => "\"parent\"",
        "code" => "\"code\"",
        "access_unitkerja" => "\"access_unitkerja\"",
        "access_company"   => "\"access_company\""
    );

    protected $_columns_file = array(
        "id" => "\"id\"",
        "nama" => "\"nama\"",
        "code" => "\"code\"",
        "filename" => "\"filename\"",
        "oriname" => "\"oriname\"",
        "is_obsolete" => "\"is_obsolete\"",
        "id_kategori" => "\"id_kategori\"",
        "periode"     => "\"periode\"",
        "retention"   => "\"retention\"",
        "parent"      => "\"parent\""
    );
    protected $_filter = '';


    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function column($columns){
        $this->_columns=$columns;
        return $this;
    }

    public function defaultColumn(){
        $this->_columns = array(
            "id" => "\"id\"",
            "nama" => "\"nama\"",
            "\"LEVEL\"" => "\"level\"",
            "parent" => "\"parent\"",
            "code" => "\"code\"",
            "access_unitkerja" => "\"access_unitkerja\"",
            "to_char(created_at, 'dd/mm/yyyy')" =>"\"created_at\"",
            "to_char(updated_at, 'dd/mm/yyyy')" => "\"updated_at\""
        );
        return $this;
    }

    public function show_sql($show_sql){
        $this->_show_sql = $show_sql;
        return $this;
    }

    private function _getColumn($alias=null, $file=false){
        $column ="";
        $no = 1;
        $getColumn = $this->_columns;
        if($file == true){
            $getColumn = $this->_columns_file;
        }
        foreach ($getColumn as $key => $value) {
            $column_name = $key;
            if($alias!=null){
                $column_name = $alias.".".$key;
            }
            $column .= "$column_name as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }


    public function getKategori($level=self::LEVEL_KATEGORI, $filter){
        $column = $this->_getColumn();
        $sql = "SELECT $column from $this->_table WHERE level=".$level." $filter ";
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }
        $rows = dbGetRows($sql);
        return $rows;

    }
    public function getAllTree($myunitkerja=null,$typeWorkunit=null, $filter=NULL){
        $column = $this->_getColumn('t1');
        $sql = "
        SELECT *
        FROM(
            SELECT
                $column
                ,t1.CODE AS \"urut\"
            FROM $this->_table t1
            WHERE t1.\"LEVEL\"=1
            UNION 
            SELECT
                $column
                ,CONCAT(t2.CODE,concat('_',t1.CODE)) AS \"urut\"
            FROM $this->_table t1
            INNER JOIN $this->_table t2 ON t1.PARENT = t2.ID
            WHERE t1.\"LEVEL\"=2";
            if($myunitkerja){
            $unitkerja = explode(',', $myunitkerja);
                foreach ($unitkerja as $key => $value) {
                    if($key==0){
                        $sql .= " AND ";
                    }
                    else{
                        $sql .= " OR ";
                    }
                    if($typeWorkunit=='unitkerja'){
                        $sql .="  INSTR(t1.ACCESS_UNITKERJA, '".$value."')!=0 ";
                    }else{
                        $sql .="  INSTR(t1.ACCESS_COMPANY, '".$value."')!=0 ";
                    }
                    
                    
                }
            }
        $sql .="    UNION
            SELECT
                $column
                ,CONCAT(t3.CODE,concat('_',concat(t2.CODE,CONCAT('_',t1.CODE)))) AS \"urut\"
            FROM $this->_table t1
            INNER JOIN (
                $this->_table t2 
                INNER JOIN $this->_table t3 ON t2.PARENT = t3.ID
            ) ON t1.PARENT = t2.ID
            WHERE t1.\"LEVEL\"=3";
            if($myunitkerja){
                $unitkerja = explode(',', $myunitkerja);
                foreach ($unitkerja as $key => $value) {
                    if($key==0){
                        $sql .= " AND ";
                    }
                    else{
                        $sql .= " OR ";
                    }
                    if($typeWorkunit=='unitkerja'){
                        $sql .="  INSTR(t1.ACCESS_UNITKERJA, '".$value."')!=0 ";
                    }else{
                        $sql .="  INSTR(t1.ACCESS_COMPANY, '".$value."')!=0 ";
                    }
                    
                }
            }
        $sql .= " ) tab
                ORDER BY \"urut\"
                ";
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }

        $rows = dbGetRows($sql);
        return $rows;
    }


    public function getDetailTree($id=null, $level=1, $myunitkerja=null, $typeWorkunit=null){
        $unitkerja_sql = '';
        if($myunitkerja){
            $unitkerja = explode(',', $myunitkerja);
            foreach ($unitkerja as $key => $value) {
                if($key==0){
                    $unitkerja_sql .= " AND ";
                }
                else{
                    $unitkerja_sql .= " OR ";
                }
                if($typeWorkunit=='unitkerja'){
                    $unitkerja_sql .="  INSTR(t1.ACCESS_UNITKERJA, '".$value."')!=0 ";
                }else{
                    $unitkerja_sql .="  INSTR(t1.ACCESS_COMPANY, '".$value."')!=0 ";
                }
                
            }
        }
        if($level == 1){ // jika dia kategori, yang diambil cuma sub kategori aja
            $column = $this->_getColumn();
            $sql = "select $column from $this->_table where parent='$id' and \"LEVEL\"=".self::LEVEL_SUBKATEGORI." $unitkerja_sql $this->_filter" ;
        }elseif($level == 3){ // jika levelnya 3, cuma ambil filenya
            $column = $this->_getColumn(null, true);
            $sql .="SELECT $column, 'file' as \"type\" from $this->_table_file WHERE id_kategori=".$id;
        }
        else{ // jika 2, maka ambil sub kategori bawah, dan filenya(jika ada)
            $column = $this->_getColumn();
            $haveFile = dbGetOne("select 1 from buletin_iso_file where id_kategori=".$id);
            $sql = "
                SELECT *
                FROM(
                    SELECT id as \"id\",nama as \"nama\",code as \"code\",'' AS \"filename\", '' AS \"oriname\", 
                           NULL AS \"is_obsolete\",'' AS \"id_kategori\", 'kategori' as \"type\"
                    FROM $this->_table
                    WHERE parent = ".$id." and \"LEVEL\"=".self::LEVEL_SUBKATEGORI_2."
                ";
                if($haveFile){ // jika level 2 tsb punya file
                    $sql .="
                        UNION
                        select id AS \"id\",nama as \"nama\",code as \"code\",filename as \"filename\",oriname as \"oriname\",is_obsolete as \"is_obsolete\",id_kategori as \"id_kategori\", 'file' as \"type\"
                        from $this->_table_file
                        WHERE id_kategori=".$id."
                    ";
                }
            $sql .=  " ) tab ORDER BY \"type\" desc, \"nama\" asc ";
        }
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }

        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getLastCode($level=self::LEVEL_KATEGORI){
        // $sql = "select max(code_number) from documents where code like '%$code%'";
        $sql = " select max(code_number) from buletin_iso where level=".$level;
        // echo $sql;die();
        $code_number = dbGetOne($sql);
        if ($code_number==NULL){
            $code_number = 1;
        }else {
            $code_number = (int)$code_number;
            $code_number = $code_number+1;
            // $code_number++;
        }
        $last_code = str_pad($code_number, 3, '0', STR_PAD_LEFT);
        // echo "last code:".$code_number."<br>";die();
        return $last_code;
    }

    public function getLastCodeFile($id){
        $sql = " select max(code_number) from buletin_iso_file where id_kategori=".$id;
        $code_number = dbGetOne($sql);
        if ($code_number==NULL){
            $code_number = 1;
        }else {
            $code_number = (int)$code_number;
            $code_number = $code_number+1;
            // $code_number++;
        }
        $last_code = str_pad($code_number, 3, '0', STR_PAD_LEFT);
        // echo "last code:".$code_number."<br>";die();
        return $last_code;
    }


    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }


    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getFileBy($id){
        $column = $this->_getColumn(null, true);
        $sql = "select $column from $this->_table_file where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col as \"$col\" from $this->_table where id='$id' $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneBy($id, $col){
        $sql = "select $col from $this->_table where id='$id'" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col = ''){
        $sql = "select $col from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }
}

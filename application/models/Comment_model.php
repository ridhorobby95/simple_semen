<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_model extends AppModel {

    protected $belongs_to = array(
        'User',
        'Post',
    );
    protected $many_to_many = array(
        'Post_tag_user' => array('Comment', 'User')
    );
    protected $label = array(
        'user_id' => 'User',
        'post_id' => 'Postingan',
        'text' => 'Komentar',
    );
    protected $validation = array(
        'text' => 'required'
    );

    protected function property($row) {
        $row['image'] = Image::getImage($row['id'], $row['image'], 'comments/photos');
        $row['file'] = File::getFile($row['id'], $row['file'], 'comments/files');
        $row['video'] = Video::getVideo($row['id'], $row['video'], 'comments/videos');
        return $row;
    }

    protected function join_or_where($row) {
        $this->_database->where('comments.is_delete = 0');
        $this->_database->order_by('comments.created_at');
        return $row;
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE, $notification = TRUE) {
        $this->_database->trans_begin();
        $this->load->model('Post_model');
        $post = $this->Post_model->with(array('Group' => 'Group_member', 'User', 'Post_user'))->get_by('posts.id', $data['post_id']);
        
        switch ($post['type']) {
            case Post_model::TYPE_GROUP:
                $member = Util::toList($post['group']['groupMembers'], 'userId');
                if (!in_array(SessionManagerWeb::getUserID(), $member) && !SessionManagerWeb::isAdministrator()) {
                    return 'Selain anggota group ' . $post['group']['name'] . ' tidak dapat menambahkan komentar';
                }
                break;
            case Post_model::TYPE_FRIEND:
                $member = Util::toList($post['postUsers'], 'id');
                if ($data['user_id'] != $post['userId'] && !in_array($data['user_id'], $member) && !SessionManagerWeb::isAdministrator()) {
                    return 'Selain anggota postingan tidak dapat menambahkan komentar';
                }
                break;
        }
        
        $create = parent::create($data, $skip_validation, $return);

        if ($create) {
            $this->_database->trans_commit();
            if (isset($data['post_tag_users'])) {
                $toUser = $data['post_tag_users'];
                $this->load->model('Post_tag_user_model');

                foreach ($toUser as $user) {
                    $dataToUser = array(
                        'user_id' => $user,
                        'comment_id' => $create
                    );
                    $insert = $this->Post_tag_user_model->insert($dataToUser, TRUE, FALSE);
                    if (!$insert) {
                        $this->_database->trans_rollback();
                        return FALSE;
                    }
                }
            }
            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $create, $_FILES['image']['name'], 'comments/photos');
            }
            if (isset($_FILES['video']) && !empty($_FILES['video'])) {
                $upload = Video::upload('video', $create, $_FILES['video']['name'], 'comments/videos');
            }
            if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                $upload = File::upload('file', $create, 'comments/files');
            }

            if ($upload === TRUE) {
                $this->load->model('Notification_model');
                if($notification){
                    $this->load->model('Notification_model');
                    $this->Notification_model->generate(Notification_model::ACTION_COMMENT_CREATE, $create);
                    $this->Notification_model->generate(Notification_model::ACTION_TAG_COMMENT_CREATE, $create);
                }
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        } else {
            $this->_database->trans_rollback();
            return FALSE;
        }
    }
    
    public function update($primary_value, $data = array(), $skip_validation = FALSE){
        $length = strlen(trim($data['text']));
        if ($data['text']==NULL or $length==0){
            return 'Komentar harus diisi.';
        }
        $oldData = $this->get_by('comments.id', $primary_value);

        $data['userId'] = $oldData['userId'];

        if (!empty($oldData['image']) && !isset($data['image'])) {
            $data['image'] = Image::getName($oldData['image'][strtolower(Image::IMAGE_ORIGINAL)]['name']);
        }
        if (!empty($oldData['video']) && !isset($data['video'])) {
            $upload = $data['video'] = $oldData['video']['name'];
        }
        if (!empty($oldData['file']) && !isset($data['file'])) {
            $upload = $data['file'] = $oldData['file']['name'];
        }

        $update = $this->save($primary_value, $data, $skip_validation);
        if ($update) {
            if (isset($data['post_tag_users'])) {
                $toUser = $data['post_tag_users'];
                $this->load->model('Post_tag_user_model');

                $delete = $this->Post_tag_user_model->delete_by(array('comment_id' => $primary_value));
                if(!$delete){
                    $this->_database->trans_rollback();
                    return FALSE;
                }

                foreach ($toUser as $user) {
                    $dataToUser = array(
                        'user_id' => $user,
                        'comment_id' => $primary_value
                    );

                    $insert = $this->Post_tag_user_model->insert($dataToUser, TRUE, FALSE);
                    if (!$insert) {
                        $this->_database->trans_rollback();
                        return FALSE;
                    }
                }

            }

            $upload = TRUE;
            if (isset($_FILES['image']) && !empty($_FILES['image'])) {
                $upload = Image::upload('image', $primary_value, $_FILES['image']['name'], 'comments/photos');
            }
            if (isset($_FILES['video']) && !empty($_FILES['video'])) {
                $upload = Video::upload('video', $primary_value, $_FILES['video']['name'], 'comments/videos');
            }
            if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                $upload = File::upload('file', $primary_value, 'comments/files');
            }

            if ($upload === TRUE) {
                $this->load->model('Notification_model');
                $this->Notification_model->generate(Notification_model::ACTION_COMMENT_CREATE, $primary_value);
                $this->Notification_model->generate(Notification_model::ACTION_TAG_COMMENT_CREATE, $primary_value);
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }

        $this->_database->trans_rollback();
        return FALSE;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            return $this->_database->trans_commit();
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function deleteComment($id){
	$this->_database->trans_begin();
	$comment = $this->get($id);
	$this->load->model('Post_model');
    $post = $this->Post_model->with(array('Group' => 'Group_member', 'User', 'Post_user', 'Post_tag_user'))->get_by('posts.id', $comment['postId']);
    $delete = $this->delete($id);
    if($delete){
	  if(!empty($post['parentHierarchy'])){
	    $cond = array(
	      'post_id' => explode('/', $post['parentHierarchy']),
	      'text' => $comment['text'],
	      'user_id' => $comment['userId']
	    );
	    $deleteparent =  $this->delete_by($cond);
	    if(!$deleteparent){
	      $this->_database->trans_rollback();
	      return FALSE;
	    }
	  }
	  return $this->_database->trans_commit();
        }
        $this->_database->trans_rollback();
	return FALSE;
    }

    public function getComments($post_id=NULL){
        if ($post_id==NULL)
            return NULL;

        if ($this->uri->segment(3)=='dashboard'){
            $order_by  = " asc "; 
        }

        $sql = "select * from comments where post_id=$post_id and is_delete!='1' order by id $order_by";
        $comments = dbGetRows($sql);
        if (!$comments){
            $comments=NULL;
        }

        $this->load->model('User_model','User');
        foreach ($comments as $k => $c) {
            $c['image'] = Image::getImage($c['id'], $c['image'], 'comments/photos');
            $c['file']=File::getFile($c['id'],$c['file'],'comments/files');
            $c['video'] = Video::getVideo($c['id'],$c['video'],'comments/videos');
            // $c['video']=NULL;
            $c['user']=$this->User->getUser($c['user_id']);
            $comments[$k]=$c;
        }
        return $comments;
    }

    public function getComment($comment_id){
        if ($comment_id==NULL)
            return NULL;
        $sql = "select * from comments where id=$comment_id ";
        $comment = dbGetRow($sql);
        if (!$comment){
            $comment=NULL;
        }

        $this->load->model('User_model','User');
        // foreach ($comments as $k => $c) {
        $comment['image'] = Image::getImage($comment['id'], $comment['image'], 'comments/photos');
        $comment['file']=File::getFile($comment['id'],$comment['file'],'comments/files');
        $comment['video'] = Video::getVideo($comment['id'],$comment['video'],'comments/videos');
        $comment['video']=NULL;
        $comment['user']=$this->User->getUser($c['user_id']);
        // $comments[$k]=$c;
        //}
        return $comment;
    }

    // public function getTaggedUser($comment_id){
    //     $sql = "select * from comments left join post_tag_users where comment=$comment_id";
    //     $comments = dbGetRows($sql);
    //     if (!$comments){
    //         $comments=NULL;
    //     }

    //     $this->load->model('User_model','User');
    //     foreach ($comments as $k => $c) {
    //         $c['image'] = Image::getImage($_id, $c['image'], 'posts/photos');
    //         $c['file']=NULL;
    //         $c['video']=NULL;
    //         $c['user']=$this->User->getUser($c['user_id']);
    //         $comments[$k]=$c;
    //     }
    //     return $comments;
    // }
}

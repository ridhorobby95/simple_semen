<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Helpdesk_model extends AppModel {

    protected $_table = 'helpdesk';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_show_sql = false;
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "id" => "\"id\"",
        "title" => "\"title\"",
        "description" => "\"description\"",
        "user_id" => "\"user_id\"",
        "company" => "\"company\"",
        "pin" => "\"pin\"",
        "view_count" => "\"view_count\"",
        "is_answered" => "\"is_answered\"",
        "to_char(created_at, 'dd/mm/yyyy')" =>"\"created_at\"",
        "to_char(updated_at, 'dd/mm/yyyy')" => "\"updated_at\""
    );
    protected $_filter = '';


    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function column($columns){
        $this->_columns=$columns;
        return $this;
    }

    public function defaultColumn(){
        $this->_columns = array(
            "h.id" => "\"id\"",
            "h.title" => "\"title\"",
            "h.description" => "\"description\"",
            "h.user_id" => "\"user_id\"",
            "h.company" => "\"company\"",
            "h.pin" => "\"pin\"",
            "h.view_count" => "\"view\"",
            "h.is_answered" => "\"is_answered\"",
            "to_char(h.created_at, 'dd/mm/yyyy')" =>"\"created_at\"",
            "to_char(h.updated_at, 'dd/mm/yyyy')" => "\"updated_at\""
        );
        return $this;
    }

    public function show_sql($show_sql){
        $this->_show_sql = $show_sql;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function getAll($filter=NULL, $is_one=false, $user_id_login){
        $sql = "
        select h.id as \"id\", h.title as \"title\", h.description as \"description\", 
               h.company as \"company\", h.pin AS \"pin\",  h.is_answered as \"is_answered\",
               h.view_count as \"view\",
               to_char(h.created_at, 'dd/mm/yyyy') as \"created_at\",
               to_char(h.updated_at, 'dd/mm/yyyy') as \"updated_at\",
               to_char(h.answer_at, 'dd/mm/yyyy') as \"answer_at\",
        h.user_id as \"user_id\", t.photo AS \"photo\", t.username AS \"username\",  
        h.answer_user_id AS \"answer_user_id\", j.photo AS \"answer_photo\", j.username AS \"answer_username\",
        v.id as \"view_id\"
           from (helpdesk h LEFT JOIN USERS t ON h.USER_ID = t.id)
           LEFT JOIN USERS j ON h.ANSWER_USER_ID = j.id
           LEFT JOIN HELPDESK_VIEWS v ON h.id = v.helpdesk_id
           $filter";
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }
        if($is_one == true){
            $rows = dbGetRow($sql);
        }
        else{
            $rows = dbGetRows($sql);
        }
        return $rows;
    }

    public function limitData($filter=NULL){
        $sql = "select * from(
        select h.id as \"id\", h.title as \"title\", h.description as \"description\", 
               h.company as \"company\", h.pin AS \"pin\",  h.is_answered as \"is_answered\",
               h.view_count as \"view\",
               to_char(h.created_at, 'dd/mm/yyyy') as \"created_at\",
               to_char(h.updated_at, 'dd/mm/yyyy') as \"updated_at\",
               to_char(h.answer_at, 'dd/mm/yyyy') as \"answer_at\",
        h.user_id as \"user_id\", t.photo AS \"photo\", t.username AS \"username\",  
        h.answer_user_id AS \"answer_user_id\", j.photo AS \"answer_photo\", j.username AS \"answer_username\",
        v.id as \"view_id\"
           from (helpdesk h LEFT JOIN USERS t ON h.USER_ID = t.id)
           LEFT JOIN USERS j ON h.ANSWER_USER_ID = j.id
           LEFT JOIN HELPDESK_VIEWS v ON h.id = v.helpdesk_id
           $filter) where rownum<=5";
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }
        $rows = dbGetRows($sql);
        return $rows;
    }


    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col as \"$col\" from $this->_table where id='$id' $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneBy($id, $col){
        $sql = "select $col from $this->_table where id='$id'" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col = ''){
        $sql = "select $col from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }
}

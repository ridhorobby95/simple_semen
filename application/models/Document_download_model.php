<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_download_model extends AppModel {

    protected $_table = 'document_download';
    protected $_limit = 20;
    protected $_order = 'id desc';
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "id" => "\"id\"",
        "document_id" => "\"document_id\"",
        "user_id" => "\"user_id\"",
        "is_allowed" => "\"is_allowed\"",
        "reason" => "\"reason\"",
        "to_char(valid_until,'DD MON YYYY HH24:MI:SS')" => "\"valid_until\"",
        "reject_reason" => "\"reject_reason\""
    );
    protected $_filter = '';

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function limit($limit=20){
        $this->_limit = $limit;
        return $this;
    }

    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id=$id";
        $row = dbGetRow($sql);
        return $row;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneBy($id, $col){
        $sql = "select $col from $this->_table where id='$id'" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function insert($data){
        dbInsert($this->_table, $data);
    }

    public function getLastId(){
        $sql = "select max(id) from document_download";
        return dbGetOne($sql);
    }

    public function update($document_id, $user_id, $data){
        dbUpdate($this->_table, $data, "document_id=$document_id and user_id=$user_id");
    }


    public function updateParentFunction($primary_key,$data, $skip_validation=false){
        parent::save($primary_key,$data, $skip_validation);
    }
    public function updateColumn($column='', $id=null){
        if ($id==NULL){
            return false;
        }
        $sql = "update $this->_table set $column where id=$id";
        return dbQuery($sql);
    }

    // public function create($data, $skip_validation = FALSE, $return = TRUE) {
    //     $this->_database->trans_begin();
    //     $create = parent::create($data, $skip_validation, $return);
    //     if ($create) {
    //         $upload = TRUE;
    //         if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
    //             $upload = Image::upload('photo', $create, $_FILES['photo']['name'], 'users/photos');
    //         }
    //         if ($upload === TRUE) {
    //             $this->_database->trans_commit();
    //             if($return)
    //                 return $create;
    //             else
    //                 return true;
    //         } else {
    //             $this->_database->trans_rollback();
    //             return $upload;
    //         }
    //     }
    //     $this->_database->trans_rollback();
    //     return FALSE;
    // }
}

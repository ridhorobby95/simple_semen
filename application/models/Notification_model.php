<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification_model extends AppModel {

    //STATUS
    const STATUS_NOT_SEND = 'N';
    const STATUS_SEND = 'S';

    //TYPE
    const TYPE_DOCUMENT_DETAIL = 'DD';
    const TYPE_DOWNLOAD_DOCUMENT = 'DO';
    const TYPE_DOWNLOAD_DETAIL = "OD";
    const TYPE_CHANGEUNIT_DETAIL = "CD";
    const TYPE_DOCUMENT_FILTER = 'DF';
    const TYPE_CHANGEPASSWORD = 'CP';

    //ACTION
    const ACTION_DOCUMENT_SUBMIT = 'DS';
    const ACTION_DOCUMENT_STATUS_CHANGED = 'DC';
    const ACTION_DELEGATE = 'DE';
    const ACTION_NEED_EVALUATE = 'NE';
    const ACTION_ASK_DOWNLOAD = 'AD';
    const ACTION_CHANGEUNIT_CREATOR = 'AC';
    const ACTION_CHANGEUNIT_APPROVER = 'AA';
    const ACTION_APPROVE_CHANGEUNIT = 'AP';
    const ACTION_REJECT_CHANGEUNIT_CREATOR = 'RC';
    const ACTION_REJECT_CHANGEUNIT_APPROVER = 'RA';
    const ACTION_REMINDER = 'AR';
    const ACTION_CHANGEPASSWORD = 'CP';

    protected $many_to_many = array(
        'Notification_sender' => array('Notification', 'User')
    );
    protected $has_many = array(
        'Notification_user'
    );

    protected $__table = 'notifications n';
    protected $_limit = 20;
    protected $_order = 'created_at';
    protected $_filter = '';
    protected $_with = "";
    protected $_columns = array(
        "n.id" => "\"id\"",
        "n.reference_id" => "\"reference_id\"",
        "n.message" => "\"message\"",
        "n.reference_type" => "\"reference_type\"",
        "n.action" => "\"action\"",
        "n.created_at" => "\"created_at\"",
        "n.updated_at" => "\"updated_at\""
    );

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function with($with=''){
        $this->_with = $with;
        return $this;
    }

    public function save($action = NULL, $message = NULL, $sender_id, $receiver_ids = array(), $type = NULL, $reference_id = NULL) {
        if (empty($receiver_ids))
            return TRUE;
        $data = array(
            'reference_id' => $reference_id,
            'message' => $message,
            'action' => $action,
            'reference_type' => $type,
            'user_id' => $sender_id,
        );
        $data = array_change_key_case($data, CASE_UPPER);
        $save = $this->create($data);
        if ($save) {
            $this->load->model('Notification_sender_model');
            $this->Notification_sender_model->insert(array('notification_id' => $save, 'user_id' => $sender_id));

            $this->load->model('Notification_user_model');
            $receiver = array();
            foreach ($receiver_ids as $receiver_id) {
                // $receiver = array(
                //     'user_id' => $receiver_id,
                //     'notification_id' => $save,
                //     'status' => Notification_user_model::STATUS_NOTIFICATION,
                //     'sent_at' => NULL,
                //     'created_at' => Util::timeNow(),
                //     'updated_at' => Util::timeNow(),
                // );
                $this->Notification_user_model->insert(array('notification_id' => $save, 'user_id' => $receiver_id, 'status' => Notification_user_model::STATUS_NOTIFICATION));
            }
        }
        return FALSE;
    }

    public function generate($action, $reference_id, $sender_id = NULL, $generate_cron=true, $userid= null, $name=null) {

        if (class_exists('SessionManagerWeb')){
            $iduser = SessionManagerWeb::getUserID();
            $name_user = SessionManagerWeb::getName();
        }
        else{
            $iduser = $userid;
            $name_user = $name;
        }
        // echo 'select company from documents_view where document_id='.$reference_id;die();
        switch ($action) {
            case self::ACTION_DOCUMENT_SUBMIT:
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$reference_id);

                // Setting receiver
                $receiver_ids =array();
                if ($document['need_reviewer']=='1'){
                    $this->load->model('Document_reviewer_model');
                    $workflow_id = $this->Document_model->getOne($document_id, 'workflow_id');
                    $receiver_ids = $this->Document_reviewer_model->getAllBy($document['document_id'], $workflow_id);
                    $this->load->model('User_model');
                    $receiver_ids[] = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                }
                $receiver_ids[] = $document['creator_id'];
                $type = self::TYPE_DOCUMENT_DETAIL;
                if ($sender_id==NULL)
                    $sender_id = $document['user_id'];
                $message = " Submitted ".$document['type_name']." \"".$document['title']."\"";

                // Setting variable to Email
                $email_message = $document['name']." submitted ".$document['type_name']." \"".$document['title']."\" and need your Action.";
                $email_url = site_url('web/document/detail').'/'.$reference_id;
            break;
            case self::ACTION_DOCUMENT_STATUS_CHANGED:
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$reference_id);
                // $this->load->model('Workflow_model');
                // $tasks = $this->Workflow_model->getTasks($document['workflow_id'],$document['workflow_urutan']);

                // Sender
                if ($sender_id==NULL)
                    $sender_id = $document['updated_by'];

                // Setting receiver
                $receiver_ids =array();
                switch($document['workflow_id']){
                    case '1':
                        $type = self::TYPE_DOCUMENT_DETAIL;
                        $message = " Change ".$document['type_name']." \"".$document['title']."\" Status";
                        switch($document['workflow_urutan']){
                            case '1':
                                $receiver_ids[] = $document['user_id'];
                                if ($document['status']==Document_model::STATUS_REVISE){
                                    $message = " Ask you to Revise ".$document['type_name']." \"".$document['title']."\"";
                                    if ($sender_id!=$document['creator_id'])
                                        $receiver_ids[]=$document['creator_id'];
                                } 
                            break;
                            case '2':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Review";
                                $this->load->model('Document_reviewer_model');
                                $receiver_ids = $this->Document_reviewer_model->getAllBy($document['document_id'], $document['workflow_id']);
                                $this->load->model('Document_delegates_model');
                                $document_id = $document['document_id'];
                                $workflow_urutan = $document['workflow_urutan'];
                                $workflow_id = $document['workflow_id'];
                                $delegate = $this->Document_delegates_model->filter(" document_id=$document_id and workflow_id=$workflow_id and workflow_urutan=$workflow_urutan ")->getOne('delegate');
                                if (empty($delegate)){
                                    $this->load->model('User_model');
                                    $receiver_ids[] = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                                } else {
                                     $receiver_ids[] = $delegate;
                                }
                            break;
                            case '3':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Approval.";
                                $receiver_ids[] = $document['creator_id'];
                            break;
                            case '4':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Verification.";
                                $this->load->model('Document_delegates_model');
                                $document_id = $document['document_id'];
                                $workflow_urutan = $document['workflow_urutan'];
                                $workflow_id = $document['workflow_id'];
                                $delegate = $this->Document_delegates_model->filter(" document_id=$document_id and workflow_id=$workflow_id and workflow_urutan=$workflow_urutan ")->getOne('delegate');
                                if (empty($delegate)){
                                    $this->load->model('User_model');
                                    $receiver_ids[] = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                                } else {
                                     $receiver_ids[] = $delegate;
                                }
                            break;
                            case '5':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Approval.";
                                $receiver_ids[] = $document['approver_id'];
                            break;
                            case '6':
                                $receiver_ids = array();
                                $receiver_ids[] = $document['user_id'];
                                $receiver_ids[] = $document['creator_id'];
                                $message = " Approved ".$document['type_name']." \"".$document['title']."\" and your document is Published";
                            break;
                        }
                        if ($document['status']==Document_model::STATUS_REJECT){
                            $receiver_ids = array();
                            $receiver_ids[] = $document['user_id'];
                            if ($sender_id!=$document['creator_id'])
                                $receiver_ids[]=$document['creator_id'];
                            $message = " Rejected ".$document['type_name']." \"".$document['title']."\"";
                        } else if ($document['status']==Document_model::STATUS_REVISE){
                            $receiver_ids = array();
                            $receiver_ids[] = $document['user_id'];
                            if ($sender_id!=$document['creator_id'])
                                $receiver_ids[]=$document['creator_id'];
                            $message = " Ask to revise ".$document['type_name']." \"".$document['title']."\"";
                        }
                    break;
                    case '2':
                        $type = self::TYPE_DOCUMENT_DETAIL;
                        $message = " Change ".$document['type_name']." \"".$document['title']."\" Status";
                        switch($document['workflow_urutan']){
                            case '1':
                                $receiver_ids[] = $document['user_id'];
                                if ($document['status']==Document_model::STATUS_REVISE){
                                    $message = " Ask you to Revise ".$document['type_name']." \"".$document['title']."\"";
                                    if ($sender_id!=$document['creator_id'])
                                        $receiver_ids[]=$document['creator_id'];
                                } 
                            break;
                            case '2':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Review";
                                $this->load->model('Document_reviewer_model');
                                $receiver_ids = $this->Document_reviewer_model->getAllBy($document['document_id'], $document['workflow_id']);
                                $this->load->model('Document_delegates_model');
                                $document_id = $document['document_id'];
                                $workflow_urutan = $document['workflow_urutan'];
                                $workflow_id = $document['workflow_id'];
                                $delegate = $this->Document_delegates_model->filter(" document_id=$document_id and workflow_id=$workflow_id and workflow_urutan=$workflow_urutan ")->getOne('delegate');
                                if (empty($delegate)){
                                    $this->load->model('User_model');
                                    $receiver_ids[] = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                                } else {
                                     $receiver_ids[] = $delegate;
                                }
                            break;
                            case '3':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Approval.";
                                $receiver_ids[] = $document['creator_id'];
                            break;
                            case '4':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and Need your Verification.";
                                $this->load->model('Document_delegates_model');
                                $document_id = $document['document_id'];
                                $workflow_urutan = $document['workflow_urutan'];
                                $workflow_id = $document['workflow_id'];
                                $delegate = $this->Document_delegates_model->filter(" document_id=$document_id and workflow_id=$workflow_id and workflow_urutan=$workflow_urutan ")->getOne('delegate');
                                if (empty($delegate)){
                                    $this->load->model('User_model');
                                    $receiver_ids[] = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                                } else {
                                     $receiver_ids[] = $delegate;
                                }
                            break;
                            case '5':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Approval.";
                                $receiver_ids[] = $document['approver_id'];
                            break;
                            case '6':
                                $receiver_ids = array();
                                $receiver_ids[] = $document['user_id'];
                                $receiver_ids[] = $document['creator_id'];
                                $message = " Approved Evaluation on ".$document['type_name']." \"".$document['title']."\"";
                            break;
                        }
                        if ($document['status']==Document_model::STATUS_REJECT){
                            $receiver_ids = array();
                            $receiver_ids[] = $document['user_id'];
                            if ($sender_id!=$document['creator_id'])
                                $receiver_ids[]=$document['creator_id'];
                            $message = " Rejected ".$document['type_name']." \"".$document['title']."\"";
                        }
                    break;
                    case '3':
                        $type = self::TYPE_DOCUMENT_DETAIL;
                        $message = " Change Document ".$document['type_name']." \"".$document['title']."\" Status";
                        switch($document['workflow_urutan']){
                            case '1':
                                $receiver_ids[] = $document['user_id'];
                                if ($document['status']==Document_model::STATUS_REVISE){
                                    $message = " Ask you to revise ".$document['type_name']." \"".$document['title']."\"";
                                    if ($sender_id!=$document['creator_id'])
                                        $receiver_ids[]=$document['creator_id'];
                                } 
                            break;
                            case '2':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Review";
                                $this->load->model('Document_reviewer_model');
                                $receiver_ids = $this->Document_reviewer_model->getAllBy($document['document_id'], $document['workflow_id']);
                                $this->load->model('Document_delegates_model');
                                $document_id = $document['document_id'];
                                $workflow_urutan = $document['workflow_urutan'];
                                $workflow_id = $document['workflow_id'];
                                $delegate = $this->Document_delegates_model->filter(" document_id=$document_id and workflow_id=$workflow_id and workflow_urutan=$workflow_urutan ")->getOne('delegate');
                                if (empty($delegate)){
                                    $this->load->model('User_model');
                                    $receiver_ids[] = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                                } else {
                                     $receiver_ids[] = $delegate;
                                }
                            break;
                            case '3':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and need your Approval.";
                                $receiver_ids[] = $document['creator_id'];
                            break;
                            case '4':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and Need your verification.";
                                $this->load->model('Document_delegates_model');
                                $document_id = $document['document_id'];
                                $workflow_urutan = $document['workflow_urutan'];
                                $workflow_id = $document['workflow_id'];
                                $delegate = $this->Document_delegates_model->filter(" document_id=$document_id and workflow_id=$workflow_id and workflow_urutan=$workflow_urutan ")->getOne('delegate');
                                if (empty($delegate)){
                                    $this->load->model('User_model');
                                    $receiver_ids[] = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                                } else {
                                     $receiver_ids[] = $delegate;
                                }
                            break;
                            case '5':
                                $sender_id = $document['user_id'];
                                $message = " Submitted ".$document['type_name']." \"".$document['title']."\" and Need your Approval.";
                                $receiver_ids[] = $document['approver_id'];
                            break;
                            case '6':
                                $receiver_ids = array();
                                $receiver_ids[] = $document['user_id'];
                                $receiver_ids[] = $document['creator_id'];
                                $message = " Approved Document ".$document['type_name']." \"".$document['title']."\" Revision";
                            break;
                        }
                }
            break;
            case self::ACTION_DELEGATE:
                $type = self::TYPE_DOCUMENT_DETAIL;
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$reference_id);
                
                // Setting receiver
                $receiver_ids =array();
                $this->load->model('Document_delegates_model');
                $delegated = $this->Document_delegates_model->filter(" workflow_urutan=".$document['workflow_urutan']." and workflow_id=".$document['workflow_id']. " and document_id=".$document['document_id'])->getBy();
                $receiver_ids[] = $delegated['delegate'];

                // Setting Sender
                if ($sender_id==NULL){
                    $this->load->model('User_model');
                    $sender_id = $this->User_model->getChiefByRole(Role::DOCUMENT_CONTROLLER, $document['company']);
                }

                $message = " Delegated you to process ".$document['type_name']." \"".$document['title']."\"";
                $email_message = $name_user.$message;
            break;
            case self::ACTION_NEED_EVALUATE:
                $type = self::TYPE_DOCUMENT_FILTER;
                $this->load->model('Document_model');
                $documents = $this->Document_model->show_sql(false)->column("*")->table("documents_view")->filter("where \"document_id\"={$reference_id}")->order("")->getAll();
                $document = $documents[0];

                // Setting receiver
                $receiver_ids =array();
                $receiver_ids[] = $document['creator_id'];
                if ($document['creator_id']!=$document['user_id']){
                    $receiver_ids[] = $document['user_id'];
                }

                // Setting Sender
                $this->load->model('User_model');
                $sender_id = $this->User_model->getSystemUser();

                $message = $document['type_name']." \"".$document['title']."\" that you created has entered the evaluation period and should be evaluated.";
                
                // Check apakah sudah melewati masa valid
                $temp = $this->Document_model->show_sql(false)->column("*")->table("documents_view")->filter("where \"document_id\"={$reference_id} and \"validity_date\" < sysdate ")->order("")->getAll();
                if ($temp==NULL or $temp==false){
                    $message = $document['type_name']." \"".$document['title']."\" that you created has passed the validity date.";
                }
                $email_message = $message;
            break;
            case self::ACTION_REMINDER:
                $type = self::TYPE_DOCUMENT_DETAIL;
                $this->load->model('Document_model');
                $documents = $this->Document_model->show_sql(false)->column("*")->table("documents_view")->filter("where \"document_id\"={$reference_id}")->order("")->getAll();
                $document = $documents[0];

                // Setting receiver
                $receiver_ids =array();
                $receiver_ids = $this->Document_model->filter("where id={$reference_id}")->getActorIds($reference_id, false);

                // Setting Sender
                $this->load->model('User_model');
                $sender_id = $this->User_model->getSystemUser();

                $message = "We are reminding you that the ".$document['type_name']." \"".$document['title']."\" is needing your action";
                $email_message = $message;
            break;
            case self::ACTION_ASK_DOWNLOAD:
                $this->load->model('Document_download_model');
                $download_detail = $this->Document_download_model->getBy($reference_id);
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$download_detail['document_id']);
                $this->load->model('User_model');
                switch ($download_detail['is_allowed']) {
                    case 0:
                        $type = self::TYPE_DOWNLOAD_DETAIL;
                        $company = dbGetOne('select "company" from documents_view where "document_id"='.$download_detail['document_id']);
                        // $company = $this->User_model->getBy($sender_id, 'company');

                        // Setting receiver DOCUMENT CONTROLLER
                        $receiver_ids =array();
                        $doc_control = $this->User_model->getDocumentController($company);
                        foreach ($doc_control as $k_doc_control => $v_doc_control) {
                            $receiver_ids[] = $v_doc_control['user_id'];
                        }

                        $sender_id = $download_detail['user_id'];

                        $message = " Ask to Download ".$document['type_name']." \"".$document['title']."\"";
                        $requestor_name = $this->User_model->getBy($download_detail['user_id'], 'name');
                        $email_message = $requestor_name.$message." and need your approval";
                    break;
                    case 1:
                        $type = self::TYPE_DOWNLOAD_DOCUMENT;

                        // Setting receiver
                        $receiver_ids =array();
                        $receiver_ids[] = $download_detail['user_id'];

                        // Setting Sender
                        $sender_id = $sender_id;

                        $message = " Approved your download request on ".$document['type_name']." \"".$document['title']."\"";
                        $email_message = $name_user.$message;
                    break;
                    case -1:
                        $type = self::TYPE_DOWNLOAD_DOCUMENT;

                        // Setting receiver
                        $receiver_ids =array();
                        $receiver_ids[] = $download_detail['user_id'];

                        // Setting Sender
                        $sender_id = $sender_id;

                        $message = " <b style='color:red'>Rejected</b> your download request on ".$document['type_name']." \"".$document['title']."\" because '<b>".$download_detail['reject_reason']."</b>'";
                        $email_message = $name_user.$message;
                    break;
                }   
                $email_url = site_url('web/document/download_detail').'/'.$reference_id;     
            break;
            case self::ACTION_CHANGEUNIT_CREATOR:
                $type = self::TYPE_CHANGEUNIT_DETAIL;
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$reference_id);
                
                // Setting receiver
                $receiver_ids =array();
                $sql = " select creator_id from document_changeunit where document_id=$reference_id";
                $receiver_ids[] = dbGetOne($sql);

                $message = " Assigned you as new Creator to ".$document['type_name']." \"".$document['title']."\"";
                $email_message = $name_user.$message;
            break;
            case self::ACTION_CHANGEUNIT_APPROVER:
                $type = self::TYPE_CHANGEUNIT_DETAIL;
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$reference_id);
                
                // Setting receiver
                $receiver_ids =array();
                $sql = " select approver_id from document_changeunit where document_id=$reference_id";
                $receiver_ids[] = dbGetOne($sql);

                $message = " Assigned you as new Approver to ".$document['type_name']." \"".$document['title']."\"";
                $email_message = $name_user.$message;
            break;
            case self::ACTION_APPROVE_CHANGEUNIT:
                $type = self::TYPE_CHANGEUNIT_DETAIL;
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$reference_id);
                
                // Setting receiver
                $receiver_ids =array();
                $this->load->model('Unitkerja_model');
                $company = $this->Unitkerja_model->getOne($document['unitkerja_id'], "company");

                $this->load->model('User_model');
                $document_controllers = $this->User_model->getDocumentController($company);
                $receiver_ids = Util::toList($document_controllers, "user_id");


                $message = " Agreed to be the new Actor for ".$document['type_name']." \"".$document['title']."\"";
                $email_message = $name_user.$message;
            break;
            case self::ACTION_REJECT_CHANGEUNIT_CREATOR:
                $type = self::TYPE_CHANGEUNIT_DETAIL;
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($reference_id);
                
                // Setting receiver
                $receiver_ids =array();
                $this->load->model('Unitkerja_model');
                $company = $this->Unitkerja_model->getOne($document['unitkerja_id'], "company");

                $this->load->model('User_model');
                $document_controllers = $this->User_model->getDocumentController($company);
                $receiver_ids = Util::toList($document_controllers, "user_id");

                $message = " Refuse to become Creator of ".$document['type_name']." \"".$document['title']."\"";
                $email_message = $name_user.$message;
            break;
            case self::ACTION_REJECT_CHANGEUNIT_APPROVER:
                $type = self::TYPE_CHANGEUNIT_DETAIL;
                $this->load->model('Document_model');
                $document = $this->Document_model->getDocumentBy($iduser,$reference_id);
                
                // Setting receiver
                $receiver_ids =array();
                $this->load->model('Unitkerja_model');
                $company = $this->Unitkerja_model->getOne($document['unitkerja_id'], "company");

                $this->load->model('User_model');
                $document_controllers = $this->User_model->getDocumentController($company);
                $receiver_ids = Util::toList($document_controllers, "user_id");

                $message = " Refuse to become Approver of ".$document['type_name']." \"".$document['title']."\"";
                $email_message = $name_user.$message;
            break;
            case self::ACTION_CHANGEPASSWORD:
                $type = self::TYPE_CHANGEPASSWORD; 
                $receiver_ids = array($iduser);
                $message = " Need you to confirm Reset Password by your Request";
                $email_message = "System Need you to confirm Reset Password by your Request";
                $email_url = site_url('api/site/confirmResetPassword').'/'.$reference_id;
                
            break;
        }
        $this->save($action, $message, $sender_id, $receiver_ids, $type, $reference_id);

        // Save to Cron
        if ($generate_cron){
            $this->load->model('Cron_notifications_model');
            $this->load->model('User_model');
            $sender_name = $this->User_model->getBy($sender_id, 'name');
            if (!isset($email_message))
                $email_message = $sender_name.$message;
            if (!isset($email_url))
                $email_url = site_url('web/document/detail').'/'.$reference_id;   
            if($type == self::TYPE_CHANGEPASSWORD){
                $this->Cron_notifications_model->insert($receiver_ids, $email_message, $email_url, 'C');
            }else{
                $this->Cron_notifications_model->insert($receiver_ids, $email_message, $email_url);
            }
            
        }   
    }

    protected function join_or_where($row) {
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        $this->with('Notification_sender');
        $this->order_by("notifications.id", "DESC");
    }

    public function getAll($unlimited = false){
        $column = $this->_getColumn();
        $sql = "select $column from $this->__table 
                $this->_with
                where nu.reference_id!='CP' ";
        if($unlimited == true){
            $sql .=" $this->_filter order by $this->_order";
        }
        else{
            $sql .=" rownum<$this->_limit $this->_filter order by $this->_order";
        }
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getMe($user_id, $page = 0) {
        $condition = array('notification_users.user_id' => $user_id);
        $this->limit(10, ($page * 10));
        $notifications = $this->order_by('notifications.created_at', 'DESC')->get_many_by($condition);
        $ouput = $this->generateMessage($notifications);

        return $ouput;
    }

    public static function toObject($id, $message, $sender, $referenceId, $referenceType, $status, $createdAt, $updatedAt) {
        return array(
            'id' => (int) $id,
            'message' => $message,
            'sender' => $sender,
            'referenceId' => (int) $referenceId,
            'referenceType' => $referenceType,
            'status' => $status,
            'createdAt' => $createdAt,
            'updatedAt' => $updatedAt
        );
    }

    public function getNotification($userId) {
        $this->load->model('Notification_user_model');
        $this->_database->select("notifications.*, notification_users.status");
        $this->_database->join("notification_users", "notification_users.notification_id = notifications.id");
        return $this->order_by("notifications.created_at", "DESC")->get_many_by(array('notification_users.user_id' => $userId, 'notification_users.status' => Notification_user_model::STATUS_NOTIFICATION));
    }

    public function generateMessage($notifications) {
        $types = array();
        foreach ($notifications as $notification) {
            $types[$notification['referenceType']][] = $notification['referenceId'];
        }
        $ouput = array();
        foreach ($notifications as $notification) {
            $message = NULL;
            if (!empty($notification['message']) && empty($notification['action'])) {
                $message = $notification['message'];
            } elseif (!empty($notification['action']) && !empty($notification['message'])) {
                switch ($notification['action']) {
                    case self::ACTION_POST_CREATE:
                        $post = $data[self::TYPE_POST_DETAIL][$notification['referenceId']];
                        if (!empty($post['group'])) {
                            $message = $notification['message'] . ' di grup ' . $post['group']['name'];
                        } elseif (!empty($post['postUsers'])) {
                            $message = $notification['message'] . ' ke anda';
                            if ((count($post['postUsers']) == 2)) {
                                if ($post['postUsers'][0]['id'] == $userId) {
                                    $otherName = $post['postUsers'][1]['name'];
                                } else {
                                    $otherName = $post['postUsers'][0]['name'];
                                }
                                $message .= ' dan ' . $otherName;
                            } elseif (count($post['postUsers']) > 2) {
                                $message .= ' dan ' . (count($post['postUsers']) - 1) . ' orang lainnya';
                            }
                        } else {
                            $message = $notification['message'];
                        }
                        break;
                    default;
            			$message = $notification['message'];
            			break;
                }
            }
            $ouput[] = self::toObject($notification['id'], $message, end($notification['notificationSenders']), $notification['referenceId'], $notification[
                            'referenceType'], $notification['status'], $notification['createdAt'], $notification['updatedAt']);
        }
        return $ouput;
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_external_model extends AppModel {

    protected $_table = 'document_external';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_show_sql = false;
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "x.id" => "\"id\"",
        "x.code" => "\"code\"",
        "x.tanggal" => "\"tanggal\"",
        "x.name" => "\"name\"",
        "x.description" => "\"description\"",
        "x.unitkerja_id" => "\"unitkerja_id\"",
        "x.company_id" => "\"company_id\"",
        "to_char(x.created_at, 'dd/mm/yyyy')" =>"\"created_at\"",
        "to_char(x.updated_at, 'dd/mm/yyyy')" => "\"updated_at\""
    );
    protected $_filter = '';


    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function column($columns){
        $this->_columns=$columns;
        return $this;
    }

    public function defaultColumn(){
        $this->_columns = array(
            "x.id" => "\"id\"",
            "x.code" => "\"code\"",
            "x.tanggal" => "\"tanggal\"",
            "x.name" => "\"name\"",
            "x.description" => "\"description\"",
            "x.unitkerja_id" => "\"unitkerja_id\"",
            "x.company_id" => "\"company_id\"",
            "to_char(x.created_at, 'dd/mm/yyyy')" =>"\"created_at\"",
            "to_char(x.updated_at, 'dd/mm/yyyy')" => "\"updated_at\""
        );
        return $this;
    }

    public function show_sql($show_sql){
        $this->_show_sql = $show_sql;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "
        select $column ,
        U.NAME AS \"unitkerja_name\", C.NAME AS \"company_name\"
           from ($this->_table x LEFT JOIN UNITKERJA U ON x.unitkerja_id = U.id)
           LEFT JOIN UNITKERJA C ON x.COMPANY_ID = C.id
           where $filter order by x.created_at desc";
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }
        if($is_one == true){
            $rows = dbGetRow($sql);
        }
        else{
            $rows = dbGetRows($sql);
        }
        return $rows;
    }

    public function getLastCode($unitkerja_id){
        // $sql = "select max(code_number) from documents where code like '%$code%'";
        $sql = " select max(code_number) from document_external where unitkerja_id='".$unitkerja_id."'";
        // echo $sql;die();
        $code_number = dbGetOne($sql);
        if ($code_number==NULL){
            $code_number = 1;
        }else {
            $code_number = (int)$code_number;
            $code_number = $code_number+1;
            // $code_number++;
        }
        $last_code = str_pad($code_number, 3, '0', STR_PAD_LEFT);
        // echo "last code:".$code_number."<br>";die();
        return $last_code;
    }


    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column 
            U.NAME AS \"unitkerja_name\", C.NAME AS \"company_name\"
            from ($this->_table x LEFT JOIN UNITKERJA U ON x.unitkerja_id = U.id)
            LEFT JOIN UNITKERJA C ON x.COMPANY_ID = C.id
            where x.id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col as \"$col\" from $this->_table where id='$id' $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneBy($id, $col){
        $sql = "select $col from $this->_table where id='$id'" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col = ''){
        $sql = "select $col from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends AppModel {
    
    const DAFTAR_INDUK_DOKUMEN = 'D';

    protected $_table = 'documents_view';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_filter = '';
    protected $_show_sql = false;
    protected $_columns = array(
        "\"document_id\"" => "\"document_id\"",
        "\"status\"" => "\"status\"",
        "\"workflow_id\"" => "\"workflow_id\"" ,
        "\"workflow_urutan\"" => "\"workflow_urutan\"" 
    );
    
    public function column($col){
        $this->_columns = $col;
        return $this;
    }

    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $rows = dbGetRows($sql);
        return $rows;
    }
    
    public function getBy(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter" ;
        if ($this->_show_sql){
            die(nl2br($sql));
        }
        $column = dbGetOne($sql);
        return $column;
    }

    // public function getDID(){

    // }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Statistic_documents_model extends AppModel {
    
    const PENDING_PROCESS = 'P';
    const LEAD_TIME_PUBLISH = 'L';

    protected $_table = 'statistic_documents';
    protected $_limit = 20;
    protected $_order = 'order by id';
    protected $_filter = '';
    protected $_show_sql = false;
    protected $_columns = array(
        "id" => "\"id\"",
        "unitkerja_id" => "\"unitkerja_id\"",
        "workflow_id" => "\"workflow_id\"", 
        "workflow_urutan" => "\"workflow_urutan\"" ,
        "jumlah_hari" => "\"jumlah_hari\"",
        "doc_type" => "\"doc_type\"",
        "type" => "\"type\"" 
    );

    public function table($table='statistic_documents'){
        $this->_table = $table;
        return $this;
    }
    
    public function column($col){
        $this->_columns = $col;
        return $this;
    }

    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function order($order='order by id'){
        $this->_order = $order;
        return $this;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function show_sql($show_sql){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function getAll(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter $this->_order";
        if ($this->_show_sql){
            echo nl2br($sql);
            die();
        }
        $rows = dbGetRows($sql);
        return $rows;
    }
    
    public function getBy(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_filter";
        if ($this->_show_sql){
            echo nl2br($sql);
            die();
        }
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($col){
        $sql = "select $col from $this->_table $this->_filter" ;
        if ($this->_show_sql){
            echo nl2br($sql);
            die();
        }
        $column = dbGetOne($sql);
        return $column;
    }

    public function updateDate($col=NULL, $where_syntax=NULL){
        if ($col==NULL or $where_syntax==NULL){
            return $this;
        }
        $sql = "update $this->_table set $col = sysdate $where_syntax";
        dbQuery($sql);
        return $this;
    }

    public function updateTotDoc($totDoc = -1){
      $db = "update $this->_table set total_document = total_document".$totDoc." $this->_filter";
      dbQuery($db);
      return true;
   }
}

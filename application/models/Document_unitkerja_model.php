<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_unitkerja_model extends AppModel {

    protected $_table='DOCUMENT_UNITKERJA';
    protected $_filter = '';
    protected $_columns = array(
    	"document_id" => "\"document_id\"",

    );

    public function create($document_id, $data) {
        foreach ($data as $key => $value) {
            $duk['document_id'] = $document_id;
            $duk['document_unitkerja'] = $value;

            // if (strtolower($value) == 'all'){
            //     $duk['document_unitkerja'] = 0;
            // }
            dbInsert($this->_table,$duk);
        } 
    }

    public function filter($filter=''){
        $this->_filter = $filter;
        return $this;
    }

    public function getAll(){
        $sql = "select document_unitkerja as \"document_unitkerja\" from $this->_table $this->_filter  ";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getAllBy($document_id){
    	// $sql = "select document_id as \"document_id\",document_unitkerja as \"document_unitkerja\", u.name as \"name\"
    	// 		from document_unitkerja du 
    	// 		left join unitkerja u on u.id=du.document_unitkerja
    	// 		where du.document_id=$document_id";
    	$sql = "select du.document_unitkerja as \"document_unitkerja\", uk.name as \"name\"
    			from document_unitkerja du
    			left join unitkerja uk on uk.id=du.document_unitkerja
    			where du.document_id=$document_id";
    	$rows = dbGetRows($sql);
    	$document_unitkerja = array();
    	foreach ($rows as $key => $row) {
    		// $document_unitkerja[$row['document_unitkerja']] = $row['name'];
            $document_unitkerja[] = $row['document_unitkerja'];
    	}
    	return $document_unitkerja;
    }

    public function getOne($document_id, $unitkerja_id){
        $sql = "select 1 from document_unitkerja where document_id=$document_id and document_unitkerja='$unitkerja_id'";
        return dbGetOne($sql);
    }

    public function deleteWith($document_id){
        $sql ="delete from document_unitkerja where document_id=$document_id";
        return dbQuery($sql);
    }

    public function add($document_id, $unitkerja_id){
        $dr=array();
        $dr['document_id'] = $document_id;
        $dr['document_unitkerja'] = $unitkerja_id;
        dbInsert($this->_table,$dr);
    }
}
?>
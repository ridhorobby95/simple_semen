<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tingkatan_document_types_model extends AppModel {
    public function __construct() {
        parent::__construct();
    }

    public function getAll(){
        $sql = "select tingkatan as \"tingkatan\", document_type_id as \"document_type_id\"
                from tingkatan_document_types ";
        $rows = dbGetRows($sql);
        $tingkatan_document_types = array();
        foreach ($rows as $key => $row) {
            $tingkatan_document_types[$row['tingkatan']]['document_types'][] = $row['document_type_id'];
        }
        return $tingkatan_document_types;
    }

    public function getDocumentTypesByTingkatan($tingkatan_id){
        $sql = "select edt.document_type_id as \"document_type_id\", dt.type as \"document_type\"
                from tingkatan_document_types edt
                left join document_types dt on dt.id=edt.document_type_id
                where tingkatan='$tingkatan_id'";
        $rows = dbGetRows($sql);
        $types= array();
        foreach ($rows as $key => $value) {
            $types_id[] = $value['document_type_id'];
            $types_name[] = $value['document_type'];
        }
        $str_type['types_id'] = implode(', ',$types_id);
        $str_type['types_name'] = implode(', ', $types_name);
        return $str_type;
    }
}

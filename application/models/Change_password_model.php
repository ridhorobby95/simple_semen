<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password_model extends AppModel {

    protected $_table = 'change_password';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_show_sql = false;
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "id" => "\"id\"",
        "username" => "\"username\"",
        "password" => "\"password\"",
        "is_agree" => "\"is_agree\"",
        "to_char(created_at, 'dd/mm/yyyy')" =>"\"created_at\"",
        "to_char(updated_at, 'dd/mm/yyyy')" => "\"updated_at\""
    );
    protected $_filter = '';


    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function column($columns){
        $this->_columns=$columns;
        return $this;
    }

    public function defaultColumn(){
        $this->_columns = array(
            "id" => "\"id\"",
        "username" => "\"username\"",
        "password" => "\"password\"",
        "is_agree" => "\"is_agree\"",
        "to_char(created_at, 'dd/mm/yyyy')" =>"\"created_at\"",
        "to_char(updated_at, 'dd/mm/yyyy')" => "\"updated_at\""
        );
        return $this;
    }

    public function show_sql($show_sql){
        $this->_show_sql = $show_sql;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col from $this->_table where id='$id'";
        $column = dbGetOne($sql);
        return $column;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }


    public function getOneBy($id, $col){
        $sql = "select $col from $this->_table where id='$id'" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col = ''){
        $sql = "select $col from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }
}

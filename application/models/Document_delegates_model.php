<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Document_delegates_model extends AppModel {

    protected $_table='DOCUMENT_DELEGATES';
    protected $_order = 'document_id';
    protected $_columns = array(
    	"document_id" => "\"document_id\"",
        "delegate" => "\"delegate\""
    );
    protected $_filter ="";

    private function _getColumn(){
        if ($this->_columns=='*')
            return $this->_columns;
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function filter($filter=NULL){
        if ($filter!=NULL) {
            $filter = "where $filter";
        }
        $this->_filter = $filter;
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function create($data) {
        return dbInsert($this->_table,$data);
    }

    public function deleteBy($data){
        $document_id = $data['document_id'];
        $workflow_id = $data['workflow_id'];
        $workflow_urutan = $data['workflow_urutan'];
        $sql = " delete from $this->_table where document_id=$document_id and workflow_urutan=$workflow_urutan and workflow_id=$workflow_id";
        return dbQuery($sql);
    }

    public function getAll(){
    	$columns = $this->_getColumn();
    	$sql = "select $columns from $this->_table $this->_filter";
    	$rows = dbGetRows($sql);
        return $rows;
    }

    public function getBy(){
        $columns = $this->_getColumn();
        $sql = "select $columns from $this->_table $this->_filter order by id desc";
        $rows = dbGetRow($sql);
        return $rows;
    }

    public function getOne($column){
        $sql = "select $column from $this->_table $this->_filter";
        $col = dbGetOne($sql);
        return $col;
    }

}
?>
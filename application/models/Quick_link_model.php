<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quick_link_model extends AppModel {
    const AKTIF = '3';
    const WITHDRAWN = '1';

    protected $_table = 'quick_links';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_show_sql = false;
    protected $_with = '';
    protected $_group_by = '';
    protected $_columns = array(
        "id" => "\"id\"", 
        "name" => "\"name\"", 
        "link" => "\"link\"", 
        "description" => "\"description\"", 
        "icon" => "\"icon\"",
        "status"    => "\"status\""
    );

    public function table($table='quick_link'){
        $this->_table=$table;
        return $this;
    }

    public function show_sql($show_sql=false){
        $this->_show_sql = $show_sql;
        return $this;
    }

    public function with($with=''){
        $this->_with=$with;
        return $this;
    }


    public function getStatus($stat=null){
        if ($stat=='0')
            $stat='1';
        $status = array(
            self::AKTIF => 'Aktif',
            self::WITHDRAWN => 'Tidak Aktif'
        );
        return empty($stat) ? $status : $status[$stat];
    }

    public function column($columns){
        $this->_columns = $columns;
        return $this; 
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    public function group_by($group_by){
        $this->_group_by = $group_by;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }


    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        if ($this->_show_sql){
            die($sql);
        }
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function getAllFilter(){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table $this->_with $this->_filter $this->_group_by order by $this->_order";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetRows($sql);
    }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col from $this->_table where id='$id'";
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col){
        $sql = " select $col from $this->_table $this->_filter ";
        return dbGetOne($sql); 
    }

    public function create($data, $skip_validation = FALSE, $return = TRUE) {
        $this->_database->trans_begin();
        $create = parent::create($data, $skip_validation, $return);
        if ($create) {
            // berhasil
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        if (array_key_exists('password', $data) || array_key_exists('password_salt', $data)) {
            unset($data['password'], $data['password_salt']);
        }
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        if ($update) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $upload = Image::upload('photo', $primary_value, $_FILES['photo']['name'], 'users/photos');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unitkerja_model extends AppModel {
    
    const COMP=0;
    const DIR=1;
    const DEPT=2;
    const BIRO=3;
    const SECT=4;

    protected $_table = 'unitkerja';
    protected $_limit = 20;
    protected $_order = 'id';
    protected $_show_sql = false;
    // protected $_active = date('d-M-Y');
    protected $_columns = array(
        "id" => "\"id\"",
        "name" => "\"name\"",
        "\"LEVEL\"" => "\"level\"",
        "company" => "\"company\"",
        "\"INITIAL\"" => "\"initial\"",
        "to_char(start_date, 'dd/mm/yyyy')" =>"\"start_date\"",
        "to_char(end_date, 'dd/mm/yyyy')" => "\"end_date\"",
        "parent" => "\"parent\"",
        "photo" => "\"photo\""
    );
    protected $_filter = '';


    public function limit($limit){
        $this->_limit = $limit;
        return $this;
    }

    public function column($columns){
        $this->_columns=$columns;
        return $this;
    }

    public function defaultColumn(){
        $this->_columns = array(
            "id" => "\"id\"",
            "name" => "\"name\"",
            "\"LEVEL\"" => "\"level\"",
            "company" => "\"company\"",
            "\"INITIAL\"" => "\"initial\"",
            "to_char(start_date, 'dd/mm/yyyy')" =>"\"start_date\"",
            "to_char(end_date, 'dd/mm/yyyy')" => "\"end_date\"",
            "parent" => "\"parent\"",
            "photo" => "\"photo\""
        );
        return $this;
    }

    public function show_sql($show_sql){
        $this->_show_sql = $show_sql;
        return $this;
    }

    private function _getColumn(){
        $column ="";
        $no = 1;
        foreach ($this->_columns as $key => $value) {
            $column .= " $key as $value";
            if (count($this->_columns)>$no)
                $column .=",";
            $no++;
        }
        return $column;
    }

    public function getAll($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter order by $this->_order";
        if ($this->_show_sql){
            $this->_show_sql=false;
            die($sql);
        }
        $rows = dbGetRows($sql);
        return $rows;
    }

     public function getAllClone($filter=NULL){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where rownum<$this->_limit $filter and \"LEVEL\"='BIRO' order by $this->_order";
        $rows = dbGetRows($sql);
        return $rows;
    }

    public function order($order='id'){
        $this->_order = $order;
        return $this;
    }

    // public function active($active=date('d-M-Y')){
    //     $this->_active = $active;
    //     return $this;
    // }

    public function getAllList($filter=NULL){
        $active = date('d-m-Y');
        if ($filter!=NULL){
            $filter = ' and '.$filter;
        }
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where end_date>to_date('$active', 'DD-MM-YYYY') $filter order by $this->_order";
        $rows = dbGetRows($sql);
        $unitkerja = Util::toMap($rows, 'id', 'name'); 
        $unitkerja = array(0 => "All") + $unitkerja;
        // echo "<pre>";print_r($unitkerja);die();
        return $unitkerja;
    }

    // public function getMyUnitkerja(){
    //     $active = date('d-M-Y');
    //     if ($filter!=NULL){
    //         $filter = ' and '.$filter;
    //     }
    //     $column = $this->_getColumn();
    //     $sql = "select $column from $this->_table where end_date>'$active' $filter order by $this->_order";
    //     $rows = dbGetRows($sql);
    //     $unitkerja = Util::toMap($rows, 'id', 'name'); 
    //     return $unitkerja;
    // }

    public function getBy($id){
        $column = $this->_getColumn();
        $sql = "select $column from $this->_table where id='$id'";
        $row = dbGetRow($sql);
        return $row;
    }

    public function filter($filter){
        $this->_filter = $filter;
        return $this;
    }

    public function getOne($id, $col){
        if (empty($id) and empty($col))
            return false;
        $sql = "select $col as \"$col\" from $this->_table where id='$id' $this->_filter" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneBy($id, $col){
        $sql = "select $col from $this->_table where id='$id'" ;
        $column = dbGetOne($sql);
        return $column;
    }

    public function getOneFilter($col = ''){
        $sql = "select $col from $this->_table $this->_filter";
        if ($this->_show_sql){
            die($sql);
        }
        return dbGetOne($sql);
    }

    public function getMyUnitkerjaList($unitkerja_id, $userid){
        $level = array("'DEPT'", "'BIRO'", "'SECT', 'DIR', 'KOMP'");
        // $level = array("'DEPT'", "'BIRO'", "'SECT' ");
        $levels = implode(',',$level);
        $this->load->model('User_model');
        if ($unitkerja_id==NULL)
            return NULL;
        if (class_exists('SessionManagerWeb')){
            $userid = SessionManagerWeb::getUserID();
        } 
        else{
            $userid = $userid;
        }
        $parent = $this->User_model->getBy($userid, 'unitkerja_parent');
        $unitkerja = array();
        $unitkerja[$unitkerja_id] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($unitkerja_id, 'name');

        $unitkerja[$parent] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'name');

        while ($parent!='0' and $parent!=NULL){
            $parent = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'parent');
            if ($parent=='0')
                continue;
            $unitkerja[$parent] = $this->Unitkerja_model->filter(" and \"LEVEL\" in ($levels) ")->getOne($parent, 'name');
        }
        foreach ($unitkerja as $key => $value) {
            if ($value==NULL)
                unset($unitkerja[$key]);
        }
        return $unitkerja;
    }

    public function getUserUnitKerja($user_id, $unitkerja_id){
        $this->load->model('User_model');
        $parent = $this->User_model->getBy($user_id, 'unitkerja_parent');
        $unitkerja = array();
        $unitkerja[] = $parent;
        while ($parent!='0'){
            $parent = $this->Unitkerja_model->getOne($parent, 'parent');
            $unitkerja[] = $parent;
        }
        return $unitkerja;
    }

    public function getCreatorAndApprover($unitkerja_id){
        $sql = "select \"user_id\", \"unitkerja_parent\", \"name\"  from users_view where \"unitkerja_id\"='$unitkerja_id' and \"is_chief\"='X' ";
        $row = dbGetRow($sql);
        while ($row==NULL) {
            $parent = dbGetOne("select parent from unitkerja where id='$unitkerja_id' ");
            $parent_level = dbGetOne("select \"LEVEL\" from unitkerja where id='$parent' ");
            if ($parent==0 or $parent==NULL or $parent_level=='COMP'){
                $child_sql ="select id as \"id\" from unitkerja where parent='$unitkerja_id' and id!='sevima' and end_date>trunc(sysdate) order by id ";
                die($child_sql);
                $parent = dbGetOne($child_sql);
                if ($parent==NULL)
                    return false;
            }
            $sql = "select \"user_id\", \"unitkerja_parent\", \"name\"  from users_view where \"unitkerja_id\"='$parent' and \"is_chief\"='X' ";
            $row = dbGetRow($sql);
            if ($row==NULL)
                return false;
            $unitkerja = $parent;
            
        }
        $data['creator']['id'] = $row['user_id'];
        $data['creator']['name'] = $row['name'];
        $parent = $row['unitkerja_parent'];
        if ($parent==0 or $parent==NULL)
            $parent=$unitkerja_id;
        $sql = "select \"user_id\", \"name\" from users_view where \"unitkerja_id\"='$parent' and \"is_chief\"='X' ";
        $approver= dbGetRow($sql);
        if ($approver==NULL)
            $data['approver'] = $data['creator'];
        else {
            $data['approver']['id'] = $approver['user_id'];
            $data['approver']['name'] = $approver['name'];
        }
        return $data;
    }

    public function getAtasan($unitkerja_id, $user_id=NULL){
        $this->load->model('User_model');
        if ($unitkerja_id==NULL)
            return NULL;
        $parent = self::getOne($unitkerja_id, 'parent'); // ambil parent uk dari uk asli user
        $atasan = array();
        $atasan[$unitkerja_id] = $this->User_model->filter(" where  \"unitkerja_id\"='$unitkerja_id' and \"is_chief\"='X' ")->getByFilter('user_id'); // ambil data user(atasn) dari unitkerja yang sama dengan user is chief X
        $temp = $atasan[$unitkerja_id]; // simpan data user di var temporary
        while ($parent!='0' and $parent!=NULL){
            $unitkerja_id = $parent;
            $parent = self::getOne($unitkerja_id, 'parent');
            $atasan[$unitkerja_id] = $this->User_model->filter(" where  \"unitkerja_id\"='$unitkerja_id' and \"is_chief\"='X' ")->getByFilter('user_id');
            if ($atasan[$unitkerja_id]==NULL){
                if ($user_id!=NULL) {
                    $atasan_level_unitkerja = $this->getAtasanByLevel($unitkerja_id, $user_id);
                    if ($atasan_level_unitkerja!=NULL and $atasan_level_unitkerja!=false){
                        $atasan[$unitkerja_id]=$atasan_level_unitkerja;
                    } else {
                        $atasan[$unitkerja_id]=$temp;
                    }
                } else {
                    $atasan[$unitkerja_id]=$temp;
                }
                
                
            } else {
                $temp = $atasan[$unitkerja_id];
            }
        }
        return $atasan;
    }

    public function getAtasanByLevel($unitkerja_id, $user_id){
        $this->load->model("User_model");
        $nopeg =   $this->User_model->filter(" where \"user_id\"='".$user_id."' ")->getOneFilter('karyawan_id');
        $sql = " select atasan1_nopeg as \"atasan1_nopeg\", atasan1_level as \"atasan1_level\", atasan2_nopeg as \"atasan2_nopeg\", atasan2_level as \"atasan2_level\" from atasan where k_nopeg='".$nopeg."'";
        $atasan = dbGetRow($sql);
        $unitkerja_level = dbGetOne("select \"LEVEL\" from unitkerja where id='".$unitkerja_id."' ");

        if ($atasan['atasan1_level']==$unitkerja_level){
            $atasan_nopeg = $atasan['atasan1_nopeg'];
        } else if ($atasan['atasan2_level']==$unitkerja_level){
            $atasan_nopeg = $atasan['atasan2_nopeg'];
        } else {
            return NULL;
        }
        $atasan_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan_nopeg."' ")->getOneFilter('user_id');

        return $atasan_id;

    }

    public function getCreator($unitkerja_id, $types){
        $this->load->model('User_model');
        $this->load->model('Document_types_model');
        if ($unitkerja_id==NULL)
            return NULL;
        $parent = self::getOne($unitkerja_id, 'parent');
        $creators = array(''=>NULL);
        while ($parent!='0' and $parent!=NULL){
            $staffs = $this->User_model->columns("*")->order("order by \"is_chief\" desc, \"name\" ")->getAll(" where  \"unitkerja_id\"='$unitkerja_id' ");
            // $staff = Util::toMap($staffs,'user_id', 'name');
            // foreach ($staff as $key => $value) {
            //     $creators[$unitkerja_id][$key] =$value;
            // }
            foreach ($staffs as $staff) {
                $staff_eselon = (int)$staff['eselon_code'];
                foreach ($types as $value) {
                    $type_eselon = (int)$value['eselon_creator'];
                    if ($staff_eselon<=$type_eselon)
                        $creators[$unitkerja_id][$value['id']][$staff['user_id']] =$staff['name'];   
                }
            }
            $unitkerja_id = $parent;
            $parent = self::getOne($unitkerja_id, 'parent');
        }
        return $creators;
    }

    public function getChief($unitkerja_id){
        $sql = "select * from users_view uv where \"unitkerja_id\"='$unitkerja_id' and \"is_chief\"='X' ";
        $row = dbGetRow($sql);
        return $row;
    }

    public function listCompany(){
        $data = $this->getAll(' and parent=0 and id<10000 ');
        $company = array();
        foreach ($data as $key => $value) {
            if ($value['id']=='1000'){
                $company['parent'] = $value;
                continue;
            }
            if ($value['id']=='7000')
                $company['5000']['child'] = $value;
            else
                $company[$value['id']]=$value;
        }
        return $company;
    }

    protected function property($row) {
        if ($this->_is_role || $this->_is_private) {
            $row['role'] = $this->getRole($row['role']);
        } else {
            foreach ($row as $key => $val) {
                if (!in_array($key, $this->public_data) && !$this->show_all) {
                    unset($row[$key]);
                }
            }
        }
        if (isset($row['photo'])) {
            $row['photo'] = Image::getImage($row['id'], $row['photo'], 'users/photos');
        }
        return $row;
    }

    // public function create($data, $skip_validation = FALSE, $return = TRUE) {
    //     $this->_database->trans_begin();
    //     $create = parent::create($data, $skip_validation, $return);
    //     if ($create) {
    //         $upload = TRUE;
    //         if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
    //             $upload = Image::upload('photo', $create, $_FILES['photo']['name'], 'users/photos');
    //         }
    //         if ($upload === TRUE) {
    //             $this->_database->trans_commit();
    //             if($return)
    //                 return $create;
    //             else
    //                 return true;
    //         } else {
    //             $this->_database->trans_rollback();
    //             return $upload;
    //         }
    //     }
    //     $this->_database->trans_rollback();
    //     return FALSE;
    // }

    public function getMyApprover($user_id){
        $sql = " select * from users_view where \"user_id\"='$user_id' ";
        $user = dbGetOne($sql);
        return $user;
    }

    public function save($primary_value, $data = array(), $skip_validation = FALSE) {
        $this->_database->trans_begin();
        $update = parent::save($primary_value, $data, $skip_validation);
        die();
        if ($update) {
            $upload = TRUE;
            if (isset($_FILES['photo']) && !empty($_FILES['photo'])) {
                $names = explode('.', $_FILES['photo']['name']);
                $name = $primary_value.'.'.end($names);
                $upload = Image::uploadReal('photo', $primary_value, $name, 'company');
            }
            if ($upload === TRUE) {
                return $this->_database->trans_commit();
            } else {
                $this->_database->trans_rollback();
                return $upload;
            }
        }
        $this->_database->trans_rollback();
        return FALSE;
    }

    public function getCompany(){
        $sql = "SELECT * FROM UNITKERJA WHERE \"LEVEL\" = 'COMP' AND TRIM(LEADING '0' FROM ID) < 10000 ";
        $row = dbGetRows($sql);
        return $row;
    }

    public function getParent($unitkerja_id=NULL, $levels=array()){
        if ($unitkerja_id==NULL)
            return false;
        $sql = "select id as \"unitkerja_id\", \"LEVEL\" as \"level\" 
                from unitkerja 
                connect by id = prior parent
                start with id= '".$unitkerja_id."'
                order by level
                ";
        $workunits = dbGetRows($sql);
        foreach ($workunits as $k_workunit => $v_workunit) {
            if (in_array($v_workunit['level'], $levels)){
                return $v_workunit['unitkerja_id'];
            } 
        }
        return NULL;
    }

    public function getAllParent($unitkerja_id=NULL){
        if ($unitkerja_id==NULL)
            return false;
        $sql = "select id as \"unitkerja_id\", \"LEVEL\" as \"level\" 
                from unitkerja 
                connect by id = prior parent
                start with id= '".$unitkerja_id."'
                order by level
                ";
        $workunits = dbGetRows($sql);
        return $workunits;
    }

    public function getDept($unitkerja_id=NULL){
        if ($unitkerja_id==NULL)
            return false;
        $sql = "select id as \"unitkerja_id\", \"LEVEL\" as \"level\" 
                from unitkerja 
                connect by id = prior parent
                start with id= '".$unitkerja_id."'
                order by level
                ";
        $workunits = dbGetRows($sql);
        foreach ($workunits as $k_workunit => $v_workunit) {
            if($v_workunit['level'] == 'DEPT'){
                return $v_workunit['unitkerja_id'];
            }
        }
        return NULL;
    }

    public function getApprover($creator_id=NULL, $type_id=NULL){
        $this->load->model('User_model');
        $this->load->model('Atasan_model');
        $data = array();
        $creator_nopeg = $this->User_model->getBy($creator_id, 'karyawan_id');
        $mycompany = SessionManagerWeb::getVariablesIndex('mycompany');

        $atasan = $this->Atasan_model->filter(" where k_nopeg='$creator_nopeg' ")->getBy();
        $atasan1_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan1_nopeg']."' ")->getOneFilter('user_id');
        $atasan2_id = $this->User_model->filter(" where \"karyawan_id\"='".$atasan['atasan2_nopeg']."' ")->getOneFilter('user_id');
        $data['approver'][0]['id'] = $atasan1_id;

        if (SessionManagerWeb::getUserID()==$creator_id){
            $data['approver'][0]['id'] = $atasan1_id;
        }
        
        $data['approver'][0]['text'] = $this->User_model->getBy($data['approver'][0]['id'], 'name');
        if ($type_id=='2'){
            $data['approver'][1]['id'] = $this->getDirut($mycompany);
            $data['approver'][1]['text'] = $this->User_model->getBy($data['approver'][1]['id'], 'name');
            if ($data['approver'][0]['id']==$data['approver'][1]['id']){
                unset($data['approver'][1]);
            }
            
        }

        if ($data['approver'][0]['id']==null){
            if ($data['approver'][1]['id']==null){
                $data = array();
            } else {
                $data['approver'][0] = $data['approver'][1];
                unset($data['approver'][1]); 
            }
            
        }

        return $data;
    }

    public function getLevel(){
        $sql = "select \"LEVEL\" from unitkerja where \"LEVEL\" is not null group by \"LEVEL\" order by \"LEVEL\" ";
        $rows = dbGetRows($sql);
        $data = array();
        foreach ($rows as $row) {
            $data[$row['LEVEL']] = $row['LEVEL'];
        }
        return $data;
    }
}

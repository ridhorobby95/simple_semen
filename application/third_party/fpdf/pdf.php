<?php 
// require('rotation.php');
if(file_exists('../../../local.txt')){$dir='';}
elseif(file_exists('../../../prod.txt')){$dir = '/opt/lampp/htdocs/simple/';}
elseif(file_exists('../../../dev.txt')){$dir = '/opt/lampp/htdocs/dev/collaboration/smsi/';}
elseif(file_exists('../../../sevima.txt')){$dir = '/var/www/html/integra/';}

require($dir.'application/third_party/fpdf/rotation.php');
	class PDF extends PDF_ROTATE
	{
	    // public $totalPage;
	    // public $totalRelated = array();
	    public $dataHeader = array();
	    public $dataFooter = array();
	    // public $orientation;

	    public $totalPage;
	    public $orientation;
	    public $orientationHeader;
	    public $orientationFooter;
		function setTotalPage($n){
		    $this->totalPage = $n;
		}

		function getTotalPage(){
			return $this->totalPage;
		}

		function getDir(){
			return getcwd();
		}

		function setOrientation($orientation){
		    $this->orientation = $orientation;
		}

		function setOrientationForHeader($orientationHeader){
		    $this->orientationHeader = $orientationHeader;
		}

		function setOrientationForFooter($orientationFooter){
		    $this->orientationFooter = $orientationFooter;
		}


		function setDataHeader($data){
			$this->dataHeader = $data;
		}
		function getDataHeader(){
			return $this->dataHeader;
		}

		function setDataFooter($data){
			$this->dataFooter = $data;
		}
		function getDataFooter(){
			return $this->dataFooter;
		}

		function Header()
		{	
			if(file_exists('local.txt')){$dir='./';}
			elseif(file_exists('/opt/lampp/htdocs/simple/prod.txt')){$dir = '/opt/lampp/htdocs/simple/';}
			elseif(file_exists('dev.txt')){$dir = '/opt/lampp/htdocs/dev/collaboration/smsi/';}
			elseif(file_exists('/var/www/html/integra/sevima.txt')){$dir = '/var/www/html/integra/';}
			// define('FPDF_FONTPATH', $dir.'application/third_party/fpdf/');  
			$logo = $this->dataHeader['logo'];

		    // if($this->dataHeader['id_company'] == '2000'){
		    // 	$logo = 'semen_indonesia.png';
		    // }
		    // elseif($this->dataHeader['id_company'] == '3000'){
		    // 	$logo = 'padang.png';
		    // }
		    // elseif($this->dataHeader['id_company'] == '4000'){
		    // 	$logo = 'tonasa.png';
		    // }
		    // elseif($this->dataHeader['id_company'] == '5000'){
		    // 	$logo = 'gresik.png';
		    // }
		    // else{
		    // 	$logo = 'tl.png';
		    // }

		    if($this->dataHeader['tipe_dokumen'] != "Form"){
		    	if($this->orientationHeader == 'P'){
			    	$this->Image($dir.'assets/uploads/company/'.$logo,8,9,13,10);
			    	// $this->Image($dir.'assets/web/images/logo/'.$logo,8,9,13,10);
				    $a = $this->SetFont('Arial','B',10);
				    $this->Cell(190,0,$this->dataHeader['nama_company'],0,0,'C');
				    $this->Ln(0);
				    $this->Cell(185,13,$this->dataHeader['tipe_dokumen'],0,0,'C');
				    $this->SetFont('Arial','',9);
				    $this->Ln(4);
				    $this->Cell(183,13,$this->dataHeader['judul'],0,0,'C');
				    $this->Ln(5);
				    $this->Cell(55,10,'Kode Dokumen :',0,0,'C');$this->Cell(75,10,'Revisi Ke :',0,0,'C');$this->Cell(60,10,'Tanggal Revisi :',0,0,'C');
				    $this->Ln(4);
				    $this->Cell(55,10,$this->dataHeader['kode_dokumen'],10,0,'C');$this->Cell(75,10,$this->dataHeader['revisi'],0,0,'C');$this->Cell(60,10,$this->dataHeader['tanggal_revisi'],0,0,'C');
				    $this->SetFont('Arial','',13);
				    $this->Ln(5);
			    }
			    elseif($this->orientationHeader == 'L'){
			        $this->Image($dir.'assets/uploads/company/'.$logo,8,3,25,18);
			        // $this->Image($dir.'assets/web/images/logo/'.$logo,8,9,13,10);
			        $a = $this->SetFont('Arial','B',10);
			        $this->Cell(273,0,$this->dataHeader['nama_company'],0,0,'C');
			        $this->Ln(0);
			        $this->Cell(273,13,$this->dataHeader['tipe_dokumen'],0,0,'C');
			        $explode = explode(' ',$this->dataHeader['judul']);
			        $jumlah  = count($explode);
			        if($jumlah >= 10){
			        	$font = 8;
			        }
			        else{
			        	$font = 9;
			        }
			        $this->SetFont('Arial','',$font);
			        $this->Ln(4);
			        $this->Cell(273,13,$this->dataHeader['judul'],0,0,'C');
			        $this->SetFont('Arial','',9);
			        $this->Ln(5);
			        $this->Cell(90,10,'Kode Dokumen :',0,0,'C');$this->Cell(95,10,'Revisi Ke :',0,0,'C');$this->Cell(95,10,'Tanggal Revisi :',0,0,'C');
			        $this->Ln(4);
			        // // $this->Cell(-50,0,':',0,0,'C');
			        $this->Cell(90,10,$this->dataHeader['kode_dokumen'],0,0,'C');$this->Cell(95,10,$this->dataHeader['revisi'],0,0,'C');$this->Cell(95,10,$this->dataHeader['tanggal_revisi'],0,0,'C');
			        $this->SetFont('Arial','',13);
			         $this->Ln(5);
			    }
		    }
		    else{ // Jika Dokumen bertipe form
		    	if($this->orientationHeader == 'P'){
			        $a = $this->SetFont('Arial','',11);
			        $this->Cell(190,0,$this->dataHeader['kode_dokumen'],0,0,'R');
			        $this->Ln(5);
			        $this->Cell(190,0,'Revisi '.$this->dataHeader['revisi'].' ,'.$this->dataHeader['tanggal_revisi'],0,0,'R');
			        $this->SetFont('Arial','B',13);
			        $this->Ln(5);
			        // $this->Cell(183,13,$this->dataHeader['judul'],0,0,'C');
			    }
			    else{
			        $a = $this->SetFont('Arial','',11);
			        $this->Cell(275,0,$this->dataHeader['kode_dokumen'],0,0,'R');
			        $this->Ln(5);
			        $this->Cell(275,0,'Revisi '.$this->dataHeader['revisi'].' ,'.$this->dataHeader['tanggal_revisi'],0,0,'R');
			        $this->SetFont('Arial','B',13);
			        $this->Ln(5);
			        // $this->Cell(275,13,$this->dataHeader['judul'],0,0,'C');
			    }
		    }
		    
		    
		}

	    function Footer()
	    {	
	    	if (1==0){
				$dir = './';
			}
			else{
				// $dir = '/var/www/html/integra/';
				// $dir = '/opt/lampp/htdocs/simple/';
				$dir = '/opt/lampp/htdocs/dev/collaboration/smsi/';
			}
			// define('FPDF_FONTPATH', $dir.'application/third_party/fpdf/');
	        // Go to 1.5 cm from bottom
	        $jumlahdata = count($this->dataFooter);
	        $data = '';
	        for ($i=0; $i < $jumlahdata ; $i++) {
	        	if($i == 0){
	        		$data = $this->dataFooter[$i]['kode'];
	        	}
	        	else{
	        		$data = $data.','.$this->dataFooter[$i]['kode'];
	        	}
	        	
	        }

	        // $data = 'FI/SMI_HCM/50045213/002/R0';
	        if($this->orientationFooter == 'P'){
	        	// echo $jumlahdata.'<br>';
	            if($jumlahdata >=1 && $jumlahdata <= 3){
	                $this->SetY(-20);
	                $this->SetX(17);
	                $this->SetFont('Arial','',8);
	                $text = 'Catatan Dari:';
	                $this->Cell(20,10,$text,0,0,'L');
	                $this->Ln(2);
	                $this->setX(173);
	                $this->SetFont('Arial','',9);
	                $this->Cell(100,10,'Hal. '.$this->PageNo().' dari '.$this->totalPage,0,0,'L');
	                $this->SetFont('Arial','',8);
	                $this->Ln(4);
	            }
	            elseif($jumlahdata >=4 && $jumlahdata <= 6){
	                $this->SetY(-23);
	                $this->SetX(17);
	                $this->SetFont('Arial','',8);
	                $text = 'Catatan Dari:';
	                $this->Cell(20,10,$text,0,0,'L');
	                $this->Ln(5);
	                $this->setX(173);
	                $this->SetFont('Arial','',9);
	                $this->Cell(100,10,'Hal. '.$this->PageNo().' dari '.$this->totalPage,0,0,'L');
	                $this->SetFont('Arial','',8);
	                $this->Ln(2);
	            }
	            else{
	                $this->SetY(-26);
	                $this->SetX(17);
	                $this->SetFont('Arial','',8);
	                $text = 'Catatan Dari:';
	                $this->Cell(20,10,$text,0,0,'L');
	                $this->Ln(8);
	                $this->setX(173);
	                $this->SetFont('Arial','',9);
	                $this->Cell(100,10,'Hal. '.$this->PageNo().' dari '.$this->totalPage,0,0,'L');
	                $this->SetFont('Arial','',8);
	                $this->Ln(-1);
	            }
	            $this->setX(17);
	            $this->MultiCell(140,4, $data, 0,1);
	            // // $this->Cell(140,6, $text, 0,1);
	        }
	        elseif(($this->orientationFooter == 'L')){
	      
	            if($jumlahdata >=1 && $jumlahdata <= 6){
	                $this->SetY(-20);
	                $this->SetX(17);
	                $this->SetFont('Arial','',8);
	                $text = 'Catatan Dari:';
	                $this->Cell(20,10,$text,0,0,'L');
	                $this->Ln(2);
	                $this->setX(260);
	                $this->SetFont('Arial','',9);
	                $this->Cell(100,10,'Hal. '.$this->PageNo().' dari '.$this->totalPage,0,0,'L');
	                $this->SetFont('Arial','',8);
	                $this->Ln(4);
	            }
	            elseif($jumlahdata >=7 && $jumlahdata <= 12){
	                $this->SetY(-23);
	                $this->SetX(17);
	                $this->SetFont('Arial','',8);
	                $text = 'Catatan Dari:';
	                $this->Cell(20,10,$text,0,0,'L');
	                $this->Ln(5);
	                $this->setX(260);
	                $this->SetFont('Arial','',9);
	                $this->Cell(100,10,'Hal. '.$this->PageNo().' dari '.$this->totalPage,0,0,'L');
	                $this->SetFont('Arial','',8);
	                $this->Ln(2);
	            }
	            else{
	                $this->SetY(-26);
	                $this->SetX(17);
	                $this->SetFont('Arial','',8);
	                $text = 'Catatan Dari:';
	                $this->Cell(20,10,$text,0,0,'L');
	                $this->Ln(8);
	                $this->setX(260);
	                $this->SetFont('Arial','',9);
	                $this->Cell(100,10,'Hal. '.$this->PageNo().' dari '.$this->totalPage,0,0,'L');
	                $this->SetFont('Arial','',8);
	                $this->Ln(-1);
	            }
	            $this->setX(17);
	            $this->MultiCell(220,4, $data, 0,1);
	        }
	    }
	}
 ?>
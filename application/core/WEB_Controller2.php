<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Controller core untuk aplikasi web
 * @author Sevima
 * @version 1.0
 */
class WEB_Controller extends CI_Controller {

    const CTL_PATH = 'web/';
    const LIB_PATH = 'web/';
    const VIEW_PATH = 'web/';

    protected $class;
    protected $ctl;
    protected $data;
    protected $ispublic = 0;
    protected $method;
    protected $model;
    protected $postData;
    protected $template;
    protected $title = 'Integra EOffice';
    protected $view;

    protected $uri_segments = array();

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        // library
        $this->load->library(static::LIB_PATH . 'AuthManagerWeb');
        $this->load->library(static::LIB_PATH . 'FormatterWeb');
        $this->load->library(static::LIB_PATH . 'SessionManagerWeb');
        $this->load->library(static::LIB_PATH . 'TemplateManagerWeb');

        // model
        $this->load->model('Notification_user_model');

        // inisialisasi atribut
        $this->class = $this->router->class;
        $this->ctl = $this->getCTL();
        $this->method = ($this->router->class == $this->router->method) ? 'index' : $this->router->method;
        $this->template = new TemplateManagerWeb();
        $this->view = $this->class . (empty($this->method) ? '' : '_' . $this->method);

        // cek login
        if (empty($this->ispublic) and ! SessionManagerWeb::isAuthenticated()) {
            SessionManagerWeb::setFlash(array('errmsg' => array('Anda harus login terlebih dahulu')));
            redirect($this->getCTL('site'));
        }

        // inisialisasi model
        $model = ucfirst($this->class . '_model');
        if (file_exists(APPPATH . 'models/' . $model . EXT)) {
            $this->load->model($model);
            $this->model = new $model();
        }

        // inisialisasi post
        $post = $this->input->post();
        if (!empty($post)) {
            foreach ($post as $k => $v) {
                if (!is_array($v) and ! is_object($v) and strlen($v) == 0)
                    $post[$k] = NULL;
            }

            $this->postData = $post;
        } else
            $this->postData = array();

        // inisialisasi data
        $data = array();
        $data['path'] = static::CTL_PATH;
        $data['class'] = $this->class;
        $data['method'] = $this->method;
        $data['title'] = $this->title;
        $data['arrmenu'] = $this->getMenu();
        $data['nopic'] = base_url('assets/web/img/nopic.png');
        $data['jmlnotif'] = (int) $this->Notification_user_model->getCountMe(SessionManagerWeb::getUserID());
        $data['isdev'] = (SessionManagerWeb::isDeveloper() ? true : false);

        $this->data = $data;

        // ambil data flash
        foreach (SessionManagerWeb::getFlash() as $k => $v)
            $this->data[$k] = $v;
    }

    /**
     * Mendapatkan nama controller untuk redirect
     * @param string $ctl
     */
    protected function getCTL($ctl = null) {
        if (!isset($ctl))
            $ctl = $this->class;

        return $this::CTL_PATH . $ctl;
    }

    /**
     * Mendapatkan menu
     * @return array
     */
    protected function getMenu() {
        $menu = array();

        $menu[] = array('namamenu' => 'Community Portal', 'levelmenu' => 0, 'faicon' => 'desktop');
        $menu[] = array('namamenu' => 'Timeline Publik', 'namafile' => $this->getCTL('post'), 'levelmenu' => 1, 'faicon' => 'home');
        $menu[] = array('namamenu' => 'Timeline Pribadi', 'namafile' => $this->getCTL('post/me'), 'levelmenu' => 1, 'faicon' => 'user');
        $menu[] = array('namamenu' => 'Grup', 'namafile' => $this->getCTL('group/list_me'), 'levelmenu' => 1, 'faicon' => 'comments-o');
        $menu[] = array('namamenu' => 'Teman', 'namafile' => $this->getCTL('user/list_me'), 'levelmenu' => 1, 'faicon' => 'users');

        $menu[] = array('namamenu' => 'Task', 'levelmenu' => 0, 'faicon' => 'line-chart');
        $menu[] = array('namamenu' => 'Laporan', 'namafile' => $this->getCTL('daily_report/me'), 'levelmenu' => 1, 'faicon' => 'line-chart');
        $menu[] = array('namamenu' => 'Tugas', 'namafile' => $this->getCTL('issue/dashboard'), 'levelmenu' => 1, 'faicon' => 'tasks');
        $menu[] = array('namamenu' => 'Kalender', 'namafile' => $this->getCTL('calendar'), 'levelmenu' => 1, 'faicon' => 'calendar');
        $menu[] = array('namamenu' => 'To Do', 'namafile' => $this->getCTL('todo/for_new'), 'levelmenu' => 1, 'faicon' => 'bell-o');

        /*
        $menu[] = array('namamenu' => 'Surat', 'levelmenu' => 0, 'faicon' => 'desktop');
        $menu[] = array('namamenu' => 'Surat Masuk', 'namafile' => $this->getCTL('surat/list_in'), 'levelmenu' => 1, 'faicon' => 'home');
        $menu[] = array('namamenu' => 'Surat Keluar', 'namafile' => $this->getCTL('surat/list_out'), 'levelmenu' => 1, 'faicon' => 'home');
        $menu[] = array('namamenu' => 'Tambah Surat Masuk', 'namafile' => $this->getCTL('surat/add_in'), 'levelmenu' => 1, 'faicon' => 'home');
        $menu[] = array('namamenu' => 'Tambah Surat Keluar', 'namafile' => $this->getCTL('surat/add_out'), 'levelmenu' => 1, 'faicon' => 'home');
        $menu[] = array('namamenu' => 'Disposisi Diterima', 'namafile' => $this->getCTL('disposisi/list_in'), 'levelmenu' => 1, 'faicon' => 'home');
        $menu[] = array('namamenu' => 'Disposisi Dikirim', 'namafile' => $this->getCTL('disposisi/list_out'), 'levelmenu' => 1, 'faicon' => 'home');
        */

        $menu[] = array('namamenu' => 'eKnowledge', 'levelmenu' => 0, 'faicon' => 'book');
        $menu[] = array('namamenu' => 'Knowledge', 'namafile' => $this->getCTL('knowledge_management'), 'levelmenu' => 1, 'faicon' => 'list-ul');
        $menu[] = array('namamenu' => 'Laporan', 'namafile' => $this->getCTL('km_report/author'), 'levelmenu' => 1, 'faicon' => 'bar-chart');

        $menu[] = array('namamenu' => 'User', 'levelmenu' => 0, 'faicon' => 'user');
        $menu[] = array('namamenu' => 'Edit Profil', 'namafile' => $this->getCTL('user/me'), 'levelmenu' => 1, 'faicon' => 'user');
        if (SessionManagerWeb::isAdministrator() || SessionManagerWeb::isManagemement())
            $menu[] = array('namamenu' => 'Pengguna', 'namafile' => $this->getCTL('user'), 'levelmenu' => 1, 'faicon' => 'users');

        return $menu;
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     */
    protected function redirect() {
        $back = $this->postData['referer'];
        if (empty($back))
            redirect($this->ctl);
        else
            header('Location: ' . $back);
    }

    /**
     * Menampilkan query lalu exit
     */
    protected function dbExit() {
        echo '<pre>';
        print_r(BenchmarkManager::query());
        echo '</pre>';
        exit;
    }

    /**
     * Halaman detail data
     * @param int $id
     */
    public function detail($id) {
        // tombol
        $buttons = array();
        $buttons['back'] = array('label' => 'Kembali', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        // $buttons['edit'] = array('label' => 'Edit', 'type' => 'success', 'icon' => 'pencil', 'click' => 'goEdit()');
        $buttons['delete'] = array('label' => 'Hapus', 'type' => 'danger', 'icon' => 'trash-o', 'click' => 'goDelete()');

        $this->data['id'] = $id;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Detail ' . $this->data['title'];
    }

    /**
     * Halaman tambah data
     */
    public function add() {
        $this->edit();
    }

    /**
     * Halaman edit data
     * @param int $id
     */
    public function edit($id = null) {
        // sementara disable edit, kecuali beberapa...
        if (isset($id) and ! in_array($this->class, array('group', 'issue', 'daily_report', 'user', 'knowledge_management', 'calendar', 'post', 'todo')))
            redirect($this->getCTL() . '/detail/' . $id);

        $this->data['id'] = $id;

        // judul
        if (empty($this->data['title']))
            $this->data['title'] = (isset($id) ? 'Edit' : 'Tambah') . ' ' . $this->data['title'];

        // tombol
        if (empty($this->data['buttons'])) {
            $buttons = array();
            $buttons['back'] = array('label' => 'Kembali', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
            $buttons['save'] = array('label' => 'Simpan', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

            $this->data['buttons'] = $buttons;
        }

        // bila add
        if ($this->method == 'add')
            $this->view = $this->class . '_edit';
    }

    protected function curl($url, $fields) {
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        $fields_string = rtrim($fields_string,'&');
        
        if (!function_exists('curl_exec')) {
            # no curl
            die('ERROR: no curl');
        }
        

        $url = str_replace('eoffice.integrasolusi.com', 'localhost', $url);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_BODY, true);
        //curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_exec($ch);
        curl_close($ch);
    }

    public function toNoData($message=null) {
        //$this->view_items['content']  = $default_template.'error';

        $this->template->viewDefault('_error', $this->data);

        echo $this->output->get_output();
        exit;
    }

    /*Get mentioned user from description
    * Identified by @
    */
    public function getMentionedUser($desc){
        $tag = array();

        $words = explode(' ', $desc);
        $wordTag = array();
        foreach ($words as $word) {
            if(strpos($word, '@') !== false && strpos($word, '@') == 0){
                $wordTag[] = substr($word, 1);
            }
        }
        if(count($wordTag)){
            $this->load->model('User_model');
            $rows = $this->User_model->order_by('name')->get_many_by(array('username' => $wordTag));
            foreach ($rows as $row)
                $tag[] = $row['id'];
        }

        return $tag;
    }

    /*Get all json user required by tagger*/
    public function getJsonUser(){
        $this->load->model('User_model');
        $rows = $this->User_model->order_by('name')->getListFor(SessionManagerWeb::getUserID());

        $a_user = array();
        foreach ($rows as $row){
            $a_user[] = array(
                "username" => $row["username"],
                "id" => $row["id"],
                "name" => $row["name"], 
                "image" => $row["photo"]["original"]["link"]
            );
        }

        return json_encode($a_user);
    }

    /*
    * Delete attached document. Only in database, real file not deleted.
    * Probable need universal variable for attachment's type like 'I', 'V', 'F'
    */
    public function deletedoc($id, $type){
        $data = array();
        switch ($type) {
            case 'I':
                $data['image'] = '';
                break;
            case 'V':
                $data['video'] = '';
                break;
            case 'F':
                $data['file'] = '';
                break;
        }
        $delete = $this->model->save($id, $data, TRUE);
        if ($delete === true) {
            $ok = true;
            $msg = 'Berhasil menghapus attachment';
        } else {
            $ok = false;
            $msg = 'Gagal menghapus attachment';
        }
        SessionManagerWeb::setFlashMsg($ok, $msg);

        redirect($this->ctl . '/edit/' . $id);
    }

}

<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Authorization,Content-Type');
    exit;
}

/**
 * Digunakan untuk autentifikasi user yang telah login
 */
class PrivateApiController extends ApiController {

    protected $token;
    protected $user;
    protected $page;

    public function __construct() {
        parent::__construct();

        $this->validateToken();

        $this->page = $this->uri->segment(4, 0);
        $this->class = $this->router->class;
        $this->load->model('Notification_user_model');
        $this->data['jmlnotif'] = (int) $this->Notification_user_model->getCountMe($this->user['id']);
    }

    /**
     * Digunakan untuk validasi token dan penyimpanan history request
     */
    private function validateToken() {
        $token = self::getBearerToken(); // untuk masHanif
        // $token = $this->postData['token'];
        // $token = '122d2862a6a087353a2d362004dae06467b07395cef92859853c0886060ee98eff4909b4150b69586069561d8c5b9196';
        // echo "<pre>";print_r($token);die();
        // $token = '1da84efbbece7f0e66d447a86cbe13f367b07395cef92859853c0886060ee98ecc173e325521f667dd2c875adc5cc1b4';
        // $token = '8271a47d76f5145108b3511a2e9cb1ff67b07395cef92859853c0886060ee98ed1efe38f105d130e537fb4577e8cc2ee';
        if (!isset($token)) {
        // if (!isset($token)) {
            $this->setResponse($this->setSystem(ResponseStatus::UNAUTHORIZED, 'Token tidak dikirim.'));
        }
        $auth = AuthManager::validateToken($token);
        if (is_string($auth)) {
            $this->setResponse($this->setSystem(ResponseStatus::UNAUTHORIZED, $auth));
        } elseif (is_array($auth)) {
            list($token, $user) = $auth;
            $this->token = $this->systemResponse->token = $token;
            $this->user = $user;
        }
        unset($token);
    }

    function getBearerToken() {
        // if ($_SERVER["HTTP_AUTHORIZATION"]) {
        //     list($type, $data) = explode(" ", $_SERVER["HTTP_AUTHORIZATION"]);
        //     if ($type==  "Bearer") {
        //         return $data;
        //     } else {
        //         return null;
        //     }
        // } else {
        //     return null;
        // }
        $headers = getallheaders();
        // echo "<pre>";print_r($headers['Authorization']);
        // die();
        if($headers['Authorization']){
            list($type, $data) = explode(" ", $headers['Authorization']);
             if ($type==  "Bearer") {
                return $data;
            } else {
                return null;
            }
        }
        else{
            return null;
        }
    }

    function masterform($fld='klausul')  {
        $folder = 'document/master/'.$fld;
        // if ($user_id==NULL)
            $user_id=$this->user['id'];
        // else 
        //     $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file_metadata_upload']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';

            $this->_deleteFiles($path);
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file_metadata_upload']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file_metadata_upload']['name'];
            $_FILES['userfile']['type']= $_FILES['file_metadata_upload']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file_metadata_upload']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file_metadata_upload']['error'];
            $_FILES['userfile']['size']= $_FILES['file_metadata_upload']['size'];
            if (strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 ){
                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfile($folder_ori);
            } else {
                die("file must be excel!");
            }
            die($str);
        }   
        exit;
    }

    function documentform($user_id=NULL,$folder='document/temp')  {
        $user_id=$this->user['id'];
        $folder_ori = $folder;
        if ($_FILES['file']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file']['name'];
            $_FILES['userfile']['type']= $_FILES['file']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file']['error'];
            $_FILES['userfile']['size']= $_FILES['file']['size'];
            if (strcmp($type,"pdf")==0 || strcmp($type,"docx")==0 || strcmp($type,"doc")==0 || strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 || strcmp($type,"ppt")==0 || strcmp($type,"pptx")==0 ){

                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfiles($folder_ori);
            }
            die($str);
        }   
        exit;
    }

    function _getdraftfiles($folder='document/temp') {

        $str = '';
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx" || $file_ext=="zip" || $file_ext=="rar" ||  $file_ext=="xmind" || $file_ext=="gz"){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }

    function _getdraftfile($folder='document/temp') {

        $str = '';
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx" || $file_ext=="zip" || $file_ext=="rar" ||  $file_ext=="xmind" || $file_ext=="gz"){
                    $name = $file_src;
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }

    function _getdraftmigrasi($folder='document/temp') {
        $str = '';
        $id = md5($this->user['id'] . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           $str = "<b>".(count($files))."</b> files<br>";
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx" || $file_ext=="zip" || $file_ext=="rar" ||  $file_ext=="xmind" || $file_ext=="gz"){
                    $str .= $file_src."<br>";
               }
               $data['files'][] = $file_src;
           }
           $data['count'] = count($files);
        }
        $data['str'] = $str;
        return $data;
    } 

    function _moveHelpdeskTemp($helpdesk_id, $folder_source='helpdesk/temp', $user_id=NULL){
        if ($user_id==NULL)
            $user_id = $this->user['id'];
        $id = md5($user_id. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;

        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';

        if (is_dir($path_source)) {
            $files = glob($path_source.'*');
            $arr_image = array();
            $arr_files = array();

            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $explode = explode('/',$file);
                $filename = $explode[8]; // namafile
                $ext_files = explode('.',$file);
                $ext_file = end($ext_files);

                if ($mime=='image') {
                    $folder_dest = 'helpdesk/files';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $image = Image::getFileName($helpdesk_id, Image::IMAGE_ORIGINAL, $basename);
                    $file_name = $basename;
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->insertHelpdeskFile($helpdesk_id, $image, $basename, $user_id)) {
                            return false;
                        }
                    }
                }
                elseif($mime=='pdf' && $mime=='word'){
                    $folder_dest = 'helpdesk/files';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $file_name = File::getFileName($helpdesk_id,$file);
                    if (!in_array($file_name, $arr_files)) {
                        $arr_files[] = $file_name;
                        if (!$this->model->insertHelpdeskFile($helpdesk_id, $file_name, $basename, $user_id)) {
                            return false;
                        }
                    }
                }

                if (!rename($file, $path_dest . '/' . $file_name)) {
                    return false;
                }else{
                    return true;
                }

            }
        }
        else{
            return $this->_deleteFiles($path_source);
        }
    }

    function _moveDocumentTemp($document_id,  $folder_source='document/temp', $document_code ,$user_id=NULL) {
        if ($user_id==NULL)
            $user_id = $this->user['id'];
        $id = md5($user_id. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $explode = explode('/',$file);
                $filename = $explode[8]; // namafile
                $ext_files = explode('.',$file);
                $ext_file = end($ext_files);
                // $ext_file = pathinfo($explode[8], PATHINFO_EXTENSION); // extension
                // print_r($ext_file);
              
                if ($mime=='image') {
                    $folder_dest = 'document/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $image = Image::getName($basename);
                    $file_name = $basename;
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->updateDocumentWithFile($document_id, $image, $file, $user_id)) {
                            return false;
                        }
                    }
                } 
                else if ($mime=='video') {
                    
                    $folder_dest = 'document/videos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $fil = basename($file);
                    $hash = hash_hmac('sha256',$fil, $user_id);
                    $file_and_ext  = explode('.', $fil);
                    $file_name = Video::getFileName($hash, $fil).'.'.$file_and_ext[1];
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;
                        if (!$this->model->updateDocumentWithFile($document_id, $fil, $file, $user_id)) {
                            return false;
                        }
                    }
                } 
                else {
                    $folder_dest = 'document/files';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $pecah_nama = explode('/', $document_code);
                    $revision = '_R'.$this->model->getOne($document_id, 'revision');
                    // $file_name = $pecah_nama[0].'_'.$pecah_nama[1]. '_'.$pecah_nama[2].'_'.$pecah_nama[3].'_'.$revision.'.'.$ext_file;
                    $code_explode = explode('/',$this->model->getOne($document_id, 'code'));
                    $code_implode = implode('_',$code_explode);
                    $workflow_id = $this->model->getOne($document_id, 'workflow_id');

                    // tambahin id document
                    $append_id = "_".$document_id;
                    $type = $this->model->getOne($document_id, 'type_id');
                    if ($type==Document_model::BOARD_MANUAL){
                        $append_id= '';
                    } 
                    $file_name = $code_implode.'_R'.$this->model->getOne($document_id, 'revision').$append_id.'.'.$ext_file;
                    if ($workflow_id==3){
                        $file_name = $code_implode.'_Revise'.$this->model->getOne($document_id, 'revision').$append_id.'.'.$ext_file;
                    }

                    if ($type!=Document_model::BOARD_MANUAL and $ext_file=='pdf'){
                        rename($file, $path_dest . '/' . $file_name);
                        $arr_files[] = $fil;
                        if (!$this->model->updateDocumentWithFile($document_id, $fil, $file, $user_id)) {
                            SessionManagerWeb::setFlashMsg(false, 'Failed to change the documents');
                            return false;
                        }
                        continue;
                    }
                    
                    $fil = $file_name;
                    // $hash = hash_hmac('sha256',$fil, $user_id);
                   
                    
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;
                        if (!$this->model->updateDocumentWithFile($document_id, $fil, $file, $user_id)) {
                            return false;
                        }
                    }
                }
                if (!rename($file, $path_dest . '/' . $file_name)) {
                    return false;
                }
                return true;
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    protected function setSystem($code = NULL, $message = NULL, $validation = NULL) {
        if (empty($code)) {
            $code = ResponseStatus::SUCCESS;
        }
        $this->load->model('Notification_user_model');
        $notification = $this->Notification_user_model->getCountMe($this->user['id']);
        $this->systemResponse->setData($code, $message, $validation, $notification);
        return $this->systemResponse;
    }


    public function list_all($company, $export=false){
        // load model
        $this->load->model('Document_iso_model');
        $this->load->model('Document_klausul_model');
        $this->load->model("Documents_log_model");
        $this->load->model("Unitkerja_model");
        $this->load->model("User_model");
        $this->load->model("Document_reviewer_model");
        $this->load->model("Workflow_model");
        $this->load->model('Document_delegates_model');
        $this->load->model('Document_model');
        // setting title
        $company_name = $this->Unitkerja_model->getOne(str_pad($company,8,'0', STR_PAD_LEFT), 'NAME');
        $this->data['title'] = " Document List of ".$company_name;
        $filter = "and rownum<21";
        $documents = $this->Document_model->show_sql(false)
                                ->table("documents_view")
                                ->filter("where \"company\"='".ltrim($company,'0')."' $filter ")
                                ->order(" order by \"updated_at\"  ")
                                ->getAll();
        if($documents){
            foreach ($documents as $k_document => $v_document) {
                // ganti format tanggal
                $documents[$k_document]['updated_at'] = date('d-m-Y',strtotime($v_document['updated_at']));
                if ($v_document['validity_date']!=NULL and $v_document['validity_date']!=false){
                    $documents[$k_document]['validity_date'] = date('d-m-Y',strtotime($v_document['validity_date']));
                }
                if ($v_document['published_at']!=NULL and $v_document['published_at']!=false){
                    $documents[$k_document]['published_at'] = date('d-m-Y',strtotime($this->Document_model->getOne($v_document['document_id'], "to_date(TO_CHAR(published_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')")));
                }            

                // Get Reviewer
                $reviewers = $this->Document_reviewer_model->getAllBy($v_document['document_id'],Workflow_model::REVISI);
                if ($reviewers==NULL){
                    $reviewers = $this->Document_reviewer_model->getAllBy($v_document['document_id'],Workflow_model::PENGAJUAN);
                }
                $arr_rev = array();
                foreach ($reviewers as $reviewer) {
                    $arr_rev[] = $this->User_model->filter("where \"user_id\"={$reviewer} ")->getOneFilter('name');
                }
                $documents[$k_document]['reviewers'] = implode(';', $arr_rev);

                // Get Document Controller
                $document_id = $v_document['document_id'];
                $workflow_id = $v_document['workflow_id'];
                $workflow_urutan = 4; // Verifikasi
                $conditions = array(
                    " document_id=$document_id ", 
                    "workflow_id=$workflow_id", 
                    "workflow_urutan=$workflow_urutan"
                );
                $condition = implode(' and ', $conditions);
                $delegated = $this->Document_delegates_model->filter($condition)->getBy();

                // Kalau pada task ini ga delegasi, pakai kabiro SMSI
                if (!$delegated){
                    $users = $this->User_model->getAll("where \"role\"='".Role::DOCUMENT_CONTROLLER."' and \"is_chief\"='X' and \"company\"='".$company."' ");
                } else {
                    $user_id = $delegated['delegate'];
                    $users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
                }
                $actor_name = $users[0]['name'];
                $documents[$k_document]['document_controller'] = $actor_name;

                // get dept
                $dept_id = $this->Unitkerja_model->getDept($v_document['unitkerja_id']);
                $documents[$k_document]['department_name']= $this->Unitkerja_model->getOne($dept_id, 'name');

                /*** DATE semua diambil di dokumen log ***/
                // Drafter
                $log = $this->Documents_log_model->show_sql(false)
                                                ->filter("where document_id={$v_document['document_id']} and workflow_urutan=1 and status='".Documents_log_model::DONE."'")
                                                ->order("order by id desc")
                                                ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
                if ($log==NULL or $log==false){
                    $log = $this->Documents_log_model->show_sql(false)
                                                ->filter("where document_id={$v_document['document_id']} and status='".Documents_log_model::DONE."'")
                                                ->order("order by id")
                                                ->getOne("to_date(TO_CHAR(created_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
                }
                if ($log!=NULL and $log!=false){
                    $documents[$k_document]['drafter_date'] = date('d-M-Y',strtotime($log));
                }

                // Creator
                $log = $this->Documents_log_model->show_sql(false)
                                                ->filter("where document_id={$v_document['document_id']} and workflow_urutan=3 and status='".Documents_log_model::DONE."'")
                                                ->order("order by id desc")
                                                ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
                if ($log==NULL or $log==false){
                    $log = $this->Documents_log_model->show_sql(false)
                                                ->filter("where document_id={$v_document['document_id']} and status='".Documents_log_model::DONE."'")
                                                ->order("order by id")
                                                ->getOne("to_date(TO_CHAR(created_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
                }
                if ($log!=NULL and $log!=false){
                    $documents[$k_document]['created_date'] = date('d-M-Y',strtotime($log));
                }            

                // Document Controller
                $log = $this->Documents_log_model->show_sql(false)
                                                ->filter("where document_id={$v_document['document_id']} and workflow_urutan=5 and status='".Documents_log_model::DONE."'")
                                                ->order("order by id desc")
                                                ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
                if ($log!=NULL and $log!=false){
                    $documents[$k_document]['verified_date'] = date('d-M-Y',strtotime($log));
                }
                // Approver
                // $log = $this->Documents_log_model->show_sql(false)
                //                                 ->filter("where document_id={$v_document['document_id']} and workflow_urutan=5 and status='".Documents_log_model::DONE."'")
                //                                 ->order("order by id desc")
                //                                 ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
                // if ($log!=NULL and $log!=false){
                //     $documents[$k_document]['approved_date'] = date('d-M-Y',strtotime($log));
                // }
                if ($v_document['published_at']!=NULL and $v_document['published_at']!=false){
                    $published_at = substr($v_document['published_at'],0,9);
                    $documents[$k_document]['approved_date'] = date('d-M-Y',strtotime($published_at));
                }
                /*** END DATE ***/

                // Retention
                if ($v_document['retension']!=NULL){
                    $retension = '';
                    $v_document['retension'] = (int)$v_document['retension'];
                    if (floor($v_document['retension']/12)>0){
                        $retension = floor($v_document['retension']/12).' Tahun ';
                    }
                    $sisa = $v_document['retension']%12;
                    if ($sisa>0){
                        $retension .= $sisa.' Bulan';
                    }
                    
                    $documents[$k_document]['retention'] = $retension;
                }

                // Requirement
                $documents[$k_document]['requirements'] = implode(";",Util::toList($this->Document_iso_model->filter(" dis.document_id=".$v_document['document_id'])->table(" document_iso di ")->with(array("name"=>"document_isos", "initial"=>"dis"),"dis.iso_id=di.id")->column(array("iso"=>"\"iso\""))->getAll(),'iso'));

                // Klausul
                $documents[$k_document]['clauses'] = implode(";",Util::toList($this->Document_klausul_model->filter(" dk.document_id=".$v_document['document_id'])->table(" document_klausul dk ")->with(array("name"=>"document_iso_klausul", "initial"=>"dik"),"dik.id=dk.klausul_id")->column(array("klausul"=>"\"klausul\""))->getAll(),'klausul'));

                // Related Document
                $documents[$k_document]['related_documents'] = '';
                $related_docs = Util::toMap($this->Document_model->getRelatedDocument($v_document['document_id']), 'ID', 'CODE');
                $documents[$k_document]['related_documents'] = implode(';',$related_docs);

                // STATUS
                if ($v_document['is_obsolete']==1){
                    $documents[$k_document]['task'] = "Obsolete";
                }else {
                    if ($v_document['status']==Document_model::STATUS_REJECT){
                        $documents[$k_document]['task'] = "REJECTED";
                    } else {
                        $task = $this->Workflow_model->getTasks($v_document['workflow_id'], $v_document['workflow_urutan']);
                        $documents[$k_document]['task'] =  $task['name'];
                    }
                }
                
                
            }
            $this->data['data'] = $documents;
            if($export == false){
                // echo "<pre>";print_r($this->data);die();
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'Success get data'), $this->data);
            }
            
        }
        else{
            $this->data['data'] = null;
            if($export == false){
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Failed to get data'));
            }
           
        }

    }
    public function getVariables($user =null){
        if($user == null){
            $user = $this->user;
        }
        $this->load->model('User_model');
        $mycompany = $this->User_model->getBy($user['id'], 'company');
        if($user['role'] == Role::EKSTERNAL){
            // $mycompany = $dataLocal['selectedCompany'];
        }
        $data['mycompany'] = $mycompany;


        // List Company
        $this->load->model('Unitkerja_model');
        $company = $this->Unitkerja_model->getAll(" and parent='0' and id<'10000' ");
        $where = '';
        if($this->user['role'] == Role::EKSTERNAL){
            $where = implode('\',\'', $this->user['company_access']);
            $where = "and company in('".$where."')";
        }
        $company = $this->Unitkerja_model->show_sql(false)->getAll(" and parent='0' and LENGTH(ltrim(id, '0'))<5 $where ");
        if($company){
             foreach ($company as $comp) {
                $data['listCompany'][ltrim($comp['id'], '0')] = $comp['name']; 
            }
        }
        // Proses Bisnis
        $this->load->model('Document_prosesbisnis_model');
        $data['prosesbisnis'] = Util::toMap($this->Document_prosesbisnis_model->getAll(), 'id', 'prosesbisnis');

        // jenis doc
        $this->load->model('Document_types_model');
        // if (SessionManagerWeb::isAdministrator()) {
        //     $jenis = $this->Document_types_model->getAllJenis();
        // } else {
            $this->load->model('Tingkatan_document_types_model');
            // sebelumnya pakai jabatan
            // $tingkatan = $this->User_model->getJabatanGroup(SessionManagerWeb::getUserID());
            // sekarang pakai eselon karyawan
            $tingkatan = $this->User_model->getEselon($user['id']);
            $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
            $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

        // }
        $this->load->model("Document_model");
        foreach ($jenis as $k_jenis => $v_jenis) {
            if ($v_jenis['id']==Document_model::BOARD_MANUAL){
                unset($jenis[$k_jenis]);
            }
        }
        $data['jenis'] =  $jenis;

        if($user['role'] == Role::EKSTERNAL){
            $access_eksternal = dbGetRow("select document_type from user_eksternal where user_id=".$user['id']);
            $where = implode('\',\'', $access_eksternal['DOCUMENT_TYPE']);
            $document_types = $this->Document_types_model->order('urutan')->getAllJenis("and id in('".$where."') ");
        }
        else{
            $data['document_types'] = $this->Document_types_model->getAllJenis();
        }
        // Reviewer, Approver, Unitkerja
        // $user = $this->User_model->getBy($user['id'], 'unitkerja_id');
        // $this->data['reviewer'] = Util::toMap($this->User_model->getAllBy("\"role\"='".Role::REVIEWER."'"), 'user_id', 'name'); 
        // $data['reviewer'] = Util::toMap($this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\""))->getAll(" where \"company\"='$mycompany' "), 'user_id', 'name');
        $reviewer = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\""))->getAll(" where \"company\"='$mycompany' ");
        foreach ($reviewer as $key => $row) {
            $data['reviewer'][$row['user_id']] = $row['unitkerja_name'].' - '.$row['name'];
        }
        $this->load->model('Unitkerja_model');
        $companies = $this->Unitkerja_model->getAll(" and parent='0' ");
        foreach ($companies as $comp) {
            $company[ltrim($comp['id'], '0')] = $comp['name']; 
        }

        // get ALl User
        // $users = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\"", "\"company\""=>"\"company\"", "\"karyawan_id\""=>"\"karyawan_id\""))->getAll();
        // foreach ($users as $key => $row) {
        //     $data['karyawan'][$row['karyawan_id']] = $row['name'] .' - '.$row['unitkerja_name'].' - '.$company[$row['company']];
        // }
        // $data['karyawan'][NULL] = NULL;
        $data['unitkerja'] =$this->Unitkerja_model->order('name')->getAllList(" company='$mycompany' ");
        $myunitkerja = $this->User_model->getBy($user['id'],'unitkerja_id');
        $data['myunitkerja'] =$this->Unitkerja_model->order('name')->getMyUnitkerjaList($myunitkerja, $user['id']);
        $data['atasan'] = $this->Unitkerja_model->filter('')->getAtasan($myunitkerja, $user['id']);

        $this->load->model('Atasan_model');
        $nopeg = $this->User_model->getBy($user['id'], 'karyawan_id');
        $data['subordinates'] = $this->Atasan_model->getSubordinates($nopeg);

        $data['creator'] = $this->Unitkerja_model->filter('')->getCreator($myunitkerja, $jenis);
        // $data['creator'] = array(''=>NULL);
        
        // Iso
        $this->load->model('Document_iso_model');
        $data['document_iso'] = $this->Document_iso_model->getAll();


        // buletin iso
        $this->load->model('Buletin_iso_model');
        $data['kategori_iso'] = $this->Buletin_iso_model->getKategori();
        if($user['role'] == Role::DOCUMENT_CONTROLLER && $user['company'] == '2000'){
            $data['subkategori_iso'] = $this->Buletin_iso_model->getKategori(Buletin_iso_model::LEVEL_SUBKATEGORI);
        }elseif($user['role'] == Role::DOCUMENT_CONTROLLER && $user['company'] != '2000'){
            $filter = " and INSTR(ACCESS_COMPANY,'".$user['company']."' )!=0";
            $data['subkategori_iso'] = $this->Buletin_iso_model->getKategori(Buletin_iso_model::LEVEL_SUBKATEGORI, $filter);
        }else{
            $filter = '';
            foreach ($data['myunitkerja'] as $key => $value) {
                if($key==0){
                    $filter .= " AND ";
                }
                else{
                    $filter .= " OR ";
                }
                $filter .="  INSTR(t1.ACCESS_UNITKERJA, '".$value."')!=0 ";
            }
            $data['subkategori_iso'] = $this->Buletin_iso_model->getKategori(Buletin_iso_model::LEVEL_SUBKATEGORI, $filter);
        }
        $data['subkategori2_iso'] = $this->Buletin_iso_model->getKategori(Buletin_iso_model::LEVEL_SUBKATEGORI_2);

        return $data;
    }

    function migrationform($folder='document/temp') {
        // if ($user_id==NULL)
            $user_id=$this->user['id'];
        // else 
        //     $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file_upload']['name']) {
            if (count($_FILES['file_upload']['name'])>10){
                die('Cant upload more than 10 files!');
            }
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';

            // $this->_deleteFiles($path);
            if (!is_dir($path)) {
               mkdir($path);         
            }
            foreach($_FILES['file_upload']['name'] as $key=>$val){

                $name = $_FILES['file_upload']['name'][$key];
                $arr = explode(".",$name);
                $type = strtolower(end($arr));


                if (strcmp($type,"pdf")==0 || strcmp($type,"docx")==0 || strcmp($type,"doc")==0 || strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 || strcmp($type,"ppt")==0 || strcmp($type,"pptx")==0 ){

                    $_FILES['userfile']['name']= $_FILES['file_upload']['name'][$key];
                    $_FILES['userfile']['type']= $_FILES['file_upload']['type'][$key];
                    $_FILES['userfile']['tmp_name']= $_FILES['file_upload']['tmp_name'][$key];
                    $_FILES['userfile']['error']= $_FILES['file_upload']['error'][$key];
                    $_FILES['userfile']['size']= $_FILES['file_upload']['size'][$key];

                    $file_name = File::getFileName($id, $name);
                    unlink($path . $file_name);
                    File::uploadFile('userfile', $id, $name, $folder);
                    $link = File::generateLink($id, $name, $folder);
                    $files_arr = $link;
                }
            }  

            $str = $this->_getdraftmigrasi($folder_ori);
            $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'sukses'), $str);
        }
    }

    function metadataform($folder='document/metadata') {
        // if ($user_id==NULL)
            $user_id=$this->user['id'];
        // else 
        //     $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file_metadata_upload']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            
            $this->_deleteFiles($path);
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file_metadata_upload']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file_metadata_upload']['name'];
            $_FILES['userfile']['type']= $_FILES['file_metadata_upload']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file_metadata_upload']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file_metadata_upload']['error'];
            $_FILES['userfile']['size']= $_FILES['file_metadata_upload']['size'];
            if (strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 ){
                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfile($folder_ori);
                $this->setResponse($this->setSystem(ResponseStatus::SUCCESS, 'success'), $str);
            } else {
                $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'file must be excel!'));
            }
        }
    }

    //Untuk ngapus document
    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }
        return false;
    }

    function unsetFileTemp($folder_source='document/temp'){
        $id = md5($this->user['id']. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $this->_deleteFiles($path_source);
        }
    }

    public function add() {
        $this->edit();
    }

    public function add_import(){
        if ($this->user['role'] != Role::ADMIN_UNIT and $this->user['role'] != Role::ADMIMISTRATOR){
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'You cant access that page!'));
        }
        $buttons = array();
        // $buttons['template'] = array('label' => ' Download Template', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownloadTemplate()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;
    }

    public function edit($id = null) {
        // sementara disable edit, kecuali beberapa...
        if (isset($id) and ! in_array($this->class, array( 'user', 'configuration', 'unitkerja', 'karyawan', 'jabatan', 'document_iso', 'document_iso_klausul', 'atasan', 'quick_link')))
            $this->setResponse($this->setSystem(ResponseStatus::ERROR, 'Error'));

        $this->data['id'] = $id;

        // judul
        if (empty($this->data['title']))
            $this->data['title'] = (isset($id) ? 'Edit' : 'Tambah') . ' ' . $this->data['title'];

        // tombol
        if (empty($this->data['buttons'])) {
            $buttons = array();
            $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
            //$buttons['save'] = array('label' => 'Simpan', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

            $this->data['buttons'] = $buttons;
        }

        // bila add
        if ($this->method == 'add'){
            $this->view = $this->class . '_edit';
        }
    }


}

<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Controller core untuk aplikasi web
 * @author Sevima
 * @version 1.0
 */
class WEB_Controller extends CI_Controller {

    const CTL_PATH = 'web/';
    const LIB_PATH = 'web/';
    const VIEW_PATH = 'web/';

    protected $class;
    protected $ctl;
    protected $data;
    protected $ispublic = 0;
    protected $method;
    protected $model;
    protected $postData;
    protected $template;
    protected $title = 'Document Management Semen Indonesia';
    protected $view;

    protected $uri_segments = array();

    public $list_sql = array();

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        // enable xss protection
        header("X-XSS-Protection: 1");

        // library
        $this->load->library(static::LIB_PATH . 'AuthManagerWeb');
        $this->load->library(static::LIB_PATH . 'FormatterWeb');
        $this->load->library(static::LIB_PATH . 'SessionManagerWeb');
        $this->load->library(static::LIB_PATH . 'TemplateManagerWeb');

        // model
        $this->load->model('Notification_user_model');
        $this->load->model('User_model');
        $user_with_jabatan = $this->User_model->getJabatan(SessionManagerWeb::getUserID());

        // inisialisasi atribut
        $this->class = $this->router->class;
        $this->ctl = $this->getCTL();
        $this->method = ($this->router->class == $this->router->method) ? 'index' : $this->router->method;
        $this->template = new TemplateManagerWeb();
        $this->view = $this->class . (empty($this->method) ? '' : '_' . $this->method);

        //cek login
        if (empty($this->ispublic) and ! SessionManagerWeb::isAuthenticated()) {
            SessionManagerWeb::setFlash(array('errmsg' => array('Please login first!')));
            redirect($this->getCTL('site/index'));
        }

        // inisialisasi model
        $model = ucfirst($this->class . '_model');
        if (file_exists(APPPATH . 'models/' . $model . EXT)) {
            $this->load->model($model);
            $this->model = new $model();
        }
        

        // inisialisasi post
        $post = $this->input->post();
        if (!empty($post)) {
            foreach ($post as $k => $v) {
                if (!is_array($v) and ! is_object($v) and strlen($v) == 0)
                    $post[$k] = NULL;
            }

            $this->postData = $post;
        } else
            $this->postData = array();

        // inisialisasi data
        $data = array();
        $data['path'] = static::CTL_PATH;
        $data['class'] = $this->class;
        $data['method'] = $this->method;
        $data['title'] = $this->title;
        $data['arrmenu'] = $this->getMenu();
        $this->load->model('Configuration_model');
        $data['config'] = $this->Configuration_model->getConfiguration();
        $this->load->model('Document_model');
        $dataNeeedAction = $this->Document_model->filter(" and \"status\"!='C' ")->getDocumentNeedAction(1, SessionManagerWeb::getUserID() ,SessionManagerWeb::getRole(),SessionManagerWeb::getCompany());
        $data['need_action_count'] = $dataNeeedAction['need_action'];

        // set menu aktif berdasarkan class
        $curr_class = $this->getCTL();
        // if ($this->router->method=='dashboard'){
        //     $this->router->method='dashboardSetSession';
        // }
        $curr_method = $this->getCTL().'/'.$this->router->method;
        
        foreach ($data['arrmenu'] as $key => $menu) {
            $activate = ($menu['namafile']==$curr_method);

            // jika aktif berdasarkan method, tambahkan exception
            if (!in_array($this->router->class, array('helpdesk', 'post', 'document')))
                $activate = ($menu['class'] == $curr_class);
            
            // Aktif berdasar method dan class
            if (in_array($this->router->class, array('document')) and in_array($this->router->method,array('index'))){
                $menus = explode('_', $_SESSION['selected_menu']);
                // die($_SESSION['selected_menu']);
                $selected_menu = $menus[0];
                switch($selected_menu){
                    case 'type':
                        $this->title='Published Documents by document type';
                        $_SESSION['document_search']['type'] = $menus[1];
                    break;
                    case 'me':
                        $this->title='My Published Documents';
                        $_SESSION['document_search']['user'] = SessionManagerWeb::getUserName();
                    break;
                    case 'unitkerja':
                        $this->title='Published Documents by Work Unit';
                        $variables = SessionManagerWeb::getVariables();
                        $unitkerja = array();
                        foreach ($variables['myunitkerja'] as $k => $var) {
                            $unitkerja[] = "'".$k."'";
                        }
                        $_SESSION['document_search']['unitkerja'] = implode(',', $unitkerja);

                        // $_SESSION['document_search']['user'] = SessionManagerWeb::getUserName();
                    break;
                    case 'related':
                        $this->title='Published Documents by Related Document';
                        $variables = SessionManagerWeb::getVariables();
                        $unitkerja = '';
                        $count = 1;
                        foreach ($variables['myunitkerja'] as $k => $var) {
                            $unitkerja = $k;
                            break;
                        }
                        $_SESSION['document_search']['related'] = $unitkerja;
                        // $_SESSION['document_search']['unitkerja'] =
                        // $_SESSION['document_search']['user'] = SessionManagerWeb::getUserName();
                    break;
                    case 'all':
                        $this->title = "All Documents";
                        // unset($_SESSION['document_search']);
                    break;
                    case 'obsolete':
                        $this->title = "Obsolete Documents";
                        $_SESSION['document_search']['obsolete'] = 1;
                    break;
                }
                $namafiles = explode('=', $menu['namafile']);
                $activate = ($_SESSION['selected_menu']==$namafiles[1]);
            }

            $data['arrmenu'][$key]['is_active'] = $activate;
        }
        
        $data['nopic'] = base_url('assets/web/img/nopic.png');
        $data['noimg'] = base_url('assets/web/img/logo.png');
        $data['noimggroup'] = base_url('assets/web/img/logo1.png');
        $data['jmlnotif'] = (int) $this->Notification_user_model->getCountMe(SessionManagerWeb::getUserID());
        // $data['isdev'] = (SessionManagerWeb::isDeveloper() ? true : false);
        $data['editdate'] = true;
        $this->load->model('Unitkerja_model');
        $this->load->model('User_model');
        $data['myworkunit_company'] = $this->User_model->filter("where \"user_id\"='".SessionManagerWeb::getUserID()."' ")->getOneFilter("company");
        $company = $this->Unitkerja_model->getAll(" and parent='0' and id<'10000' ");
        $where = '';
        if(SessionManagerWeb::getRole() == Role::EKSTERNAL){
            $where = implode('\',\'', SessionManagerWeb::getCompanyAccess());
            $where = "and company in('".$where."')";
        }
        $company = $this->Unitkerja_model->show_sql(false)->getAll(" and parent='0' and LENGTH(ltrim(id, '0'))<5 $where ");
        foreach ($company as $comp) {
            $data['company'][ltrim($comp['id'], '0')] = $comp['name']; 
        }
        // $data['company'] = Util::toMap($company, 'id', 'name');
        // echo '<pre>';
        // vaR_dump($data['company']);
        // die();
        $this->data = $data;
        
        // CEK kalo admin unit
        if (SessionManagerWeb::isAdminUnit()){
            $allowed_class = array('setting', 'unitkerja', 'user', 'karyawan', 'jabatan', 'atasan');
            if (!in_array($this->data['class'], $allowed_class)){
                redirect("web/setting");
            }
            if ($this->data['class']=='unitkerja' and $this->data['method']=='company'){
                redirect("web/setting");
            }
        }
        
        // ambil data flash
        foreach (SessionManagerWeb::getFlash() as $k => $v)
            $this->data[$k] = $v;
    }

    /**
     * Mendapatkan nama controller untuk redirect
     * @param string $ctl
     */
    protected function getCTL($ctl = null) {
        if (!isset($ctl))
            $ctl = $this->class;

        return $this::CTL_PATH . $ctl;
    }

    /**
     * Mendapatkan menu
     * @return array
     */
    protected function getMenu() {
        $this->load->model('Quick_link_model');
        $menu = array();
        if(SessionManagerWeb::getRole() != Role::EKSTERNAL){
            $menu[] = array('namamenu' => 'Home', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
            $menu[] = array('namamenu' => 'Dashboard', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 1, 'faicon' => 'dashboard');
            if (SessionManagerWeb::isDocumentController() || SessionManagerWeb::isAdministrator()){
                $menu[] = array('namamenu' => 'Statistic', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/statistik/1'), 'levelmenu' => 1, 'faicon' => 'dashboard');
                $menu[] = array('namamenu' => 'List All', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/list_all'), 'levelmenu' => 1, 'faicon' => 'dashboard');
            }
        }
        
        $menu[] = array('namamenu' => 'Quick Link', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        $menu[] = array('namamenu' => 'All Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=all'), 'levelmenu' => 1, 'faicon' => 'file');

        if(SessionManagerWeb::getRole() != Role::EKSTERNAL){
            $menu[] = array('namamenu' => 'My Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=me'), 'levelmenu' => 1, 'faicon' => 'file');
        }
        
        $menu[] = array('namamenu' => 'Work Unit Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=unitkerja'), 'levelmenu' => 1, 'faicon' => 'file');
        $menu[] = array('namamenu' => 'Related Work Unit', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=related'), 'levelmenu' => 1, 'faicon' => 'file');
        if (SessionManagerWeb::isAdministrator() or SessionManagerWeb::isDocumentController()){
            $menu[] = array('namamenu' => 'Obsolete Document', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=obsolete'), 'levelmenu' => 1, 'faicon' => 'file');
        }
        $menu[] = array('namamenu' => 'Document Type', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        $this->load->model('Document_types_model');
        if(SessionManagerWeb::getRole() == Role::EKSTERNAL){
            $where = implode('\',\'', SessionManagerWeb::getDocumentTypeAccess());
            $document_types = $this->Document_types_model->order('urutan')->getAllJenis("and id in('".$where."') ");
        }
        else{
            $document_types = $this->Document_types_model->order('urutan')->getAllJenis();
        }
        foreach ($document_types as $key => $types) {
            $menu[] = array('namamenu' => $types['type'], 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=type_'.$types['id']), 'levelmenu' => 1, 'faicon' => 'dashboard');
        }


        $menu[] = array('namamenu' => 'Buletin ISO', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        $menu[] = array('namamenu' => 'Kebijakan Perusahaan', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=all'), 'levelmenu' => 1, 'faicon' => 'file');
        $menu[] = array('namamenu' => 'Standar dan Sertifikat', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/index?selected_menu=me'), 'levelmenu' => 1, 'faicon' => 'file');
        $menu[] = array('namamenu' => 'Daftar Dokumen Eksternal', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=unitkerja'), 'levelmenu' => 1, 'faicon' => 'file');
        $menu[] = array('namamenu' => 'Daftar IPDK', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=related'), 'levelmenu' => 1, 'faicon' => 'file');
        if (SessionManagerWeb::isAdministrator() or SessionManagerWeb::isDocumentController()){
            $menu[] = array('namamenu' => 'dll', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/?selected_menu=obsolete'), 'levelmenu' => 1, 'faicon' => 'file');
        }

        $menu[] = array('namamenu' => 'Others', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 0, 'faicon' => 'dashboard');
        
        $others = $this->Quick_link_model->show_sql(false)->getAll('and status=1');
        foreach ($others as $oth) {
            $menu[] = array('namamenu' => $oth['name'], 'class' => 'blank', 'namafile' => $oth['link'], 'levelmenu' => 1, 'faicon' => $oth['icon']);
        }
        // $menu[] = array('namamenu' => 'Hukum Online', 'class' => $this->getCTL('document'), 'namafile' => $this->getCTL('document/dashboard'), 'levelmenu' => 1, 'faicon' => 'book');

        if (SessionManagerWeb::isAdminUnit()){
            $menu = array();
            $menu[] = array('namamenu' => 'Setting', 'class' => $this->getCTL('setting'), 'namafile' => $this->getCTL('setting'), 'levelmenu' => 0, 'faicon' => 'setting');
            $menu[] = array('namamenu' => 'User', 'class' => $this->getCTL('user'), 'namafile' => $this->getCTL('user'), 'levelmenu' => 1, 'faicon' => 'user');
            $menu[] = array('namamenu' => 'Employee', 'class' => $this->getCTL('karyawan'), 'namafile' => $this->getCTL('karyawan'), 'levelmenu' => 1, 'faicon' => 'user');
            $menu[] = array('namamenu' => 'Position', 'class' => $this->getCTL('jabatan'), 'namafile' => $this->getCTL('jabatan'), 'levelmenu' => 1, 'faicon' => 'user');
            $menu[] = array('namamenu' => 'Superior', 'class' => $this->getCTL('atasan'), 'namafile' => $this->getCTL('atasan'), 'levelmenu' => 1, 'faicon' => 'user');
            $menu[] = array('namamenu' => 'Work unit', 'class' => $this->getCTL('unitkerja'), 'namafile' => $this->getCTL('unitkerja'), 'levelmenu' => 1, 'faicon' => 'user');

        }

        return $menu;
    }

    /**
     * Redirect, biasanya setelah tambah atau hapus data
     */
    protected function redirect() {
        $back = $this->postData['referer'];

        if (empty($back))
            redirect($this->ctl);
        else
            header('Location: ' . $back);
    }

    /**
     * Menampilkan query lalu exit
     */
    protected function dbExit() {
        echo '<pre>';
        print_r(BenchmarkManager::query());
        echo '</pre>';
        exit;
    }

    /**
     * Halaman detail data
     * @param int $id
     */
    public function detail($id) {
        // tombol
        $buttons = array();
        $buttons['back'] = array('label' => 'Kembali', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
        // $buttons['edit'] = array('label' => 'Edit', 'type' => 'success', 'icon' => 'pencil', 'click' => 'goEdit()');
        // $buttons['delete'] = array('label' => 'Hapus', 'type' => 'danger', 'icon' => 'trash-o', 'click' => 'goDelete()');

        $this->data['id'] = $id;
        $this->data['buttons'] = $buttons;
        $this->data['title'] = 'Detail ' . $this->data['title'];
    }

    /**
     * Halaman tambah data
     */
    public function add() {
        $this->edit();
    }

    public function add_import(){
        if (!SessionManagerWeb::isAdminUnit() and !SessionManagerWeb::isAdministrator()){
            SessionManagerWeb::setFlashMsg(false,"You cant access that page!");
            redirect('web/document/dashboard');
        }
        $buttons = array();
        // $buttons['template'] = array('label' => ' Download Template', 'type' => 'secondary', 'icon' => 'download', 'click' => "goDownloadTemplate()");
        $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => "goBack()");
        $this->data['buttons'] = $buttons;
    }

    /**
     * Halaman edit data
     * @param int $id
     */
    public function edit($id = null) {
        // sementara disable edit, kecuali beberapa...
        if (isset($id) and ! in_array($this->class, array( 'user', 'configuration', 'unitkerja', 'karyawan', 'jabatan', 'document_iso', 'document_iso_klausul', 'atasan', 'quick_link')))
            redirect($this->getCTL() . '/detail/' . $id);

        $this->data['id'] = $id;

        // judul
        if (empty($this->data['title']))
            $this->data['title'] = (isset($id) ? 'Edit' : 'Tambah') . ' ' . $this->data['title'];

        // tombol
        if (empty($this->data['buttons'])) {
            $buttons = array();
            $buttons['back'] = array('label' => 'Back', 'type' => 'primary', 'icon' => 'chevron-left', 'click' => 'goBack()');
            //$buttons['save'] = array('label' => 'Simpan', 'type' => 'success', 'icon' => 'save', 'click' => 'goSave()');

            $this->data['buttons'] = $buttons;
        }

        // bila add
        if ($this->method == 'add'){
            $this->view = $this->class . '_edit';
        }
    }

    protected function curl($url, $fields) {
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        $fields_string = rtrim($fields_string,'&');
        
        if (!function_exists('curl_exec')) {
            # no curl
            die('ERROR: no curl');
        }
        

        $url = str_replace('eoffice.integrasolusi.com', 'localhost', $url);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_BODY, true);
        //curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch,CURLOPT_POST,count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
        curl_exec($ch);
        curl_close($ch);
    }

    public function toNoData($message=null) {
        //$this->view_items['content']  = $default_template.'error';

        $this->template->viewDefault('_error', $this->data);

        echo $this->output->get_output();
        exit;
    }

    /*Get mentioned user from description
    * Identified by @
    */
    public function getMentionedUserAR($desc){
        $tag;

        $words = explode(' ', $desc);
        $wordTag = array();
        foreach ($words as $word) {
            if(strpos($word, '@') !== false && strpos($word, '@') == 0){
                $wordTag[] = substr($word, 1);
            }
        }
        if(count($wordTag)){
            $this->load->model('User_model');
            $rows = $this->User_model->order_by('name')->get_many_by(array('username' => $wordTag));
            foreach ($rows as $row)
                $tag[] = $row['id'];
        }

        return $tag;
    }

    public function getMentionedUser($desc){
        $tag;

        $words = explode(' ', $desc);
        $wordTag = array();
        foreach ($words as $word) {
            if(strpos($word, '@') !== false && strpos($word, '@') == 0){
                $wordTag[] = substr($word, 1);
            }
        }
        if(count($wordTag)){
            $this->load->model('User_model');
            $rows = $this->User_model->order_by('name')->get_many_by(array('username' => $wordTag));
            foreach ($rows as $row)
                $tag[] = $row['id'];
        }

        return $tag;
    }

    /*Get url user from description
    * Identified by @
    */
    public function prepareText($text){
        $text = strip_tags($text);
        $lines = explode("\n", $text);
        $reg_exUrl = '/((?:[\w\d]+\:\/\/)?(?:[\w\-\d]+\.)+[\w\-\d]+(?:\/[\w\-\d]+)(?:\/|\.[\w\-\d]+)?(?:\?[\w\-\d]+\=[\w\-\d]+\&?)?(?:\#[\w\-\d])?)/';
        foreach ($lines as $linekey => $line) {
            $words = explode(' ', $line);
            foreach ($words as $wordkey => $word) {
                /*Check if url*/
                $word = trim($word);
                if(filter_var($word, FILTER_VALIDATE_URL)){
                    $words[$wordkey] = "<a href='$word'>$word</a>";
                }else if(preg_match($reg_exUrl, $word)) {
                    $words[$wordkey] = "<a href='http://$word'>$word</a>";
                }

                /* Check if there's mentioned users */
                if(strpos($word, '@') !== false && strpos($word, '@') == 0){
                    $word = substr($word, 1);
                    if($word){
                        $user = $this->User_model->order_by('name')->get_by(array('username' => $word));
                        $words[$wordkey] = "<span class='label label-primary'>".$user['name']."</span>";
                    }
                }
            }
            $lines[$linekey] = implode(' ', $words);
        }

        $text = implode("\n", $lines);

        //$result = preg_replace('/((http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?)/', '<a href="\1">\1</a>', $text);

        return $text;
    }

    /*Get all json user required by tagger*/
    public function getJsonUser(){
        $this->load->model('User_model');
        // $rows = $this->User_model->order_by('name')->getListFor(SessionManagerWeb::getUserID());
        $rows = $this->User_model->getListFor(SessionManagerWeb::getUserID());

        $a_user = array();
        foreach ($rows as $row){
            $a_user[] = array(
                "username" => $row["username"],
                "id" => $row["id"],
                "name" => $row["name"], 
                "image" => $row["photo"]["original"]["link"]
            );
        }

        return json_encode($a_user);
    }

    /*
    * Delete attached document. Only in database, real file not deleted.
    * Need variable for attachment's type like 'I' => 'Image', 'V' => 'Video', 'F' => 'File'
    * Only works if the model used extends Post_model
    */
    public function deletedoc($id, $type){
        $data = $this->postData;
        switch ($type) {
            case 'I':
                $data['image'] = '';
                break;
            case 'V':
                $data['video'] = '';
                break;
            case 'F':
                $data['file'] = '';
                break;
        }

        $delete = $this->Post_model->save($id, $data, TRUE);

        if ($delete === true) {
            $ok = true;
            $msg = 'Berhasil menghapus attachment';
        } else {
            $ok = false;
            $msg = 'Gagal menghapus attachment';
        }
        SessionManagerWeb::setFlashMsg($ok, $msg);

        $this->redirect();
    }

    public function isHelpdeskEnabled(){
        return false;
    }  

    // Untuk ngirim document
    function documentformBACKUP($user_id=NULL,$folder='document/temp')  {
        if ($user_id==NULL)
            $user_id=SessionManagerWeb::getUserID();
        $folder_ori = $folder;
        if ($_FILES['file']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file']['name'];
            $_FILES['userfile']['type']= $_FILES['file']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file']['error'];
            $_FILES['userfile']['size']= $_FILES['file']['size'];
            if(strcmp($type,"jpg")==0 || strcmp($type,"png")==0 || strcmp($type,"jpeg")==0 || strcmp($type,"gif")==0 || strcmp($type,"bmp")==0){

                $file_name = Image::getFileName($id, Image::IMAGE_ORIGINAL, $name);
                unlink($path . $file_name);
                $file_mime = Image::getMime($folder.$file_name);
                $image_mime = explode('/',$file_mime);
                if ($image_mime[0]=='image') {
                    Image::upload('userfile',$id, $name, $folder);
                    $link = Image::generateLink($id, $name, $folder);
                    $images_arr[] = $link['thumb'];
                }
                $str = $this->_getdraftimages($folder_ori);
             } 
             // else if (strcmp($type,"mp4")==0 || strcmp($type,"mpeg")==0 || strcmp($type,"avi")==0 || strcmp($type,"flv")==0 || strcmp($type,"wmv")==0 || strcmp($type,"mov")==0){
                else if (strcmp($type,"mp4")==0){

                $file_name = Video::getFileName($hash, $name);
                unlink($path . $file_name);
                Video::uploadVideo('userfile', $id, $name, $folder);
                $link = Video::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftvideo($folder_ori);

             } else if (strcmp($type,"pdf")==0 || strcmp($type,"docx")==0 || strcmp($type,"doc")==0 || strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 || strcmp($type,"ppt")==0 || strcmp($type,"pptx")==0 || strcmp($type,"rar")==0 || strcmp($type,"xmind")==0 || strcmp($type,"zip")==0 || strcmp($type,"gz")==0){

                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfiles($folder_ori);
            }
            die($str);
        }   
        exit;
    }

    function documentform($user_id=NULL,$folder='document/temp')  {
        if ($user_id==NULL)
            $user_id=SessionManagerWeb::getUserID();
        else 
            $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file']['name'];
            $_FILES['userfile']['type']= $_FILES['file']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file']['error'];
            $_FILES['userfile']['size']= $_FILES['file']['size'];
            if (strcmp($type,"pdf")==0 || strcmp($type,"docx")==0 || strcmp($type,"doc")==0 || strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 || strcmp($type,"ppt")==0 || strcmp($type,"pptx")==0 ){

                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfiles($folder_ori);
            }
            die($str);
        }   
        exit;
    }

    function documentformbm($user_id=NULL,$folder='document/temp')  {
        if ($user_id==NULL)
            $user_id=SessionManagerWeb::getUserID();
        else 
            $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file']['name'];
            $_FILES['userfile']['type']= $_FILES['file']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file']['error'];
            $_FILES['userfile']['size']= $_FILES['file']['size'];
            if (strcmp($type,"pdf")==0){

                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfiles($folder_ori);
            }
            die($str);
        }   
        exit;
    }

    function migrationform($folder='document/temp')  {
        // if ($user_id==NULL)
            $user_id=SessionManagerWeb::getUserID();
        // else 
        //     $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file_upload']['name']) {
            if (count($_FILES['file_upload']['name'])>10){
                die('Cant upload more than 10 files!');
            }
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));

            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';

            // $this->_deleteFiles($path);
            if (!is_dir($path)) {
               mkdir($path);         
            }
            foreach($_FILES['file_upload']['name'] as $key=>$val){

                $name = $_FILES['file_upload']['name'][$key];
                $arr = explode(".",$name);
                $type = strtolower(end($arr));


                if (strcmp($type,"pdf")==0 || strcmp($type,"docx")==0 || strcmp($type,"doc")==0 || strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 || strcmp($type,"ppt")==0 || strcmp($type,"pptx")==0 ){

                    $_FILES['userfile']['name']= $_FILES['file_upload']['name'][$key];
                    $_FILES['userfile']['type']= $_FILES['file_upload']['type'][$key];
                    $_FILES['userfile']['tmp_name']= $_FILES['file_upload']['tmp_name'][$key];
                    $_FILES['userfile']['error']= $_FILES['file_upload']['error'][$key];
                    $_FILES['userfile']['size']= $_FILES['file_upload']['size'][$key];

                    $file_name = File::getFileName($id, $name);
                    unlink($path . $file_name);
                    File::uploadFile('userfile', $id, $name, $folder);
                    $link = File::generateLink($id, $name, $folder);
                    $files_arr = $link;
                }
            }  

            $str = $this->_getdraftmigrasi($folder_ori);
            die($str);
        }   
        exit;
    }

    function metadataform($folder='document/metadata')  {
        // if ($user_id==NULL)
            $user_id=SessionManagerWeb::getUserID();
        // else 
        //     $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file_metadata_upload']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';

            $this->_deleteFiles($path);
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file_metadata_upload']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file_metadata_upload']['name'];
            $_FILES['userfile']['type']= $_FILES['file_metadata_upload']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file_metadata_upload']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file_metadata_upload']['error'];
            $_FILES['userfile']['size']= $_FILES['file_metadata_upload']['size'];
            if (strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 ){
                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfile($folder_ori);
            } else {
                die("file must be excel!");
            }
            die($str);
        }   
        exit;
    }

    function masterform($fld='klausul')  {
        $folder = 'document/master/'.$fld;
        // if ($user_id==NULL)
            $user_id=SessionManagerWeb::getUserID();
        // else 
        //     $user_id=(int)$user_id;
        $folder_ori = $folder;
        if ($_FILES['file_metadata_upload']['name']) {
            $files_arr = array();

            $id = md5($user_id . $this->config->item('encryption_key'));
            
            $folder .= '/' . $id;
            $ciConfig = $this->config->item('utils');
            $path = $ciConfig['full_upload_dir'] . $folder . '/';

            $this->_deleteFiles($path);
            if (!is_dir($path)) {
               mkdir($path);
            }
            $name = $_FILES['file_metadata_upload']['name'];
            // $hash = hash_hmac('sha256',$name, SessionManagerWeb::getUserID());
            $arr = explode(".",$name);
            $type = strtolower(end($arr));
            $_FILES['userfile']['name']= $_FILES['file_metadata_upload']['name'];
            $_FILES['userfile']['type']= $_FILES['file_metadata_upload']['type'];
            $_FILES['userfile']['tmp_name']= $_FILES['file_metadata_upload']['tmp_name'];
            $_FILES['userfile']['error']= $_FILES['file_metadata_upload']['error'];
            $_FILES['userfile']['size']= $_FILES['file_metadata_upload']['size'];
            if (strcmp($type,"xls")==0 || strcmp($type,"xlsx")==0 ){
                $file_name = File::getFileName($hash, $name);
                unlink($path . $file_name);
                File::uploadFile('userfile', $id, $name, $folder);
                $link = File::generateLink($id, $name, $folder);
                $files_arr = $link;
                $str = $this->_getdraftfile($folder_ori);
            } else {
                die("file must be excel!");
            }
            die($str);
        }   
        exit;
    }

    //Untuk ngapus document
    function _deleteFiles($path){
        if (is_dir($path) === true) {
            $files = array_diff(scandir($path), array('.', '..'));
            foreach ($files as $file) {
                $this->_deleteFiles(realpath($path) . '/' . $file);
            }
            return rmdir($path);
        }

        else if (is_file($path) === true) {
            return unlink($path);
        }
        return false;
    }

    function _moveDocumentTemp($document_id,  $folder_source='document/temp', $document_code ,$user_id=NULL) {
        if ($user_id==NULL)
            $user_id = SessionManagerWeb::getUserID();
        $id = md5($user_id. $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $files = glob($path_source.'*'); 
            $arr_image = array();
            $arr_files = array();
            foreach ($files as $file) {
                list($mime,$ext) = explode('/',Image::getMime($file));
                $explode = explode('/',$file);
                $filename = $explode[8]; // namafile
                $ext_files = explode('.',$file);
                $ext_file = end($ext_files);
                // $ext_file = pathinfo($explode[8], PATHINFO_EXTENSION); // extension
                // print_r($ext_file);
              
                if ($mime=='image') {
                    $folder_dest = 'document/photos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $image = Image::getName($basename);
                    $file_name = $basename;
                    if (!in_array($image, $arr_image)) {
                        $arr_image[] = $image;
                        if (!$this->model->updateDocumentWithFile($document_id, $image, $file, $user_id)) {
                            return false;
                        }
                    }
                } 
                else if ($mime=='video') {
                    
                    $folder_dest = 'document/videos';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $fil = basename($file);
                    $hash = hash_hmac('sha256',$fil, $user_id);
                    $file_and_ext  = explode('.', $fil);
                    $file_name = Video::getFileName($hash, $fil).'.'.$file_and_ext[1];
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;
                        if (!$this->model->updateDocumentWithFile($document_id, $fil, $file, $user_id)) {
                            return false;
                        }
                    }
                } 
                else {
                    
                    $folder_dest = 'document/files';
                    $path_dest = $ciConfig['full_upload_dir'] . $folder_dest . '/';
                    $basename = basename($file);
                    $pecah_nama = explode('/', $document_code);
                    $revision = '_R'.$this->model->getOne($document_id, 'revision');
                    // $file_name = $pecah_nama[0].'_'.$pecah_nama[1]. '_'.$pecah_nama[2].'_'.$pecah_nama[3].'_'.$revision.'.'.$ext_file;
                    $code_explode = explode('/',$this->model->getOne($document_id, 'code'));
                    $code_implode = implode('_',$code_explode);
                    $workflow_id = $this->model->getOne($document_id, 'workflow_id');

                    // tambahin id document
                    $append_id = "_".$document_id;
                    $type = $this->model->getOne($document_id, 'type_id');
                    if ($type==Document_model::BOARD_MANUAL){
                        $append_id= '';
                    } 
                    $file_name = $code_implode.'_R'.$this->model->getOne($document_id, 'revision').$append_id.'.'.$ext_file;

                    if ($workflow_id==3){
                        $file_name = $code_implode.'_Revise'.$this->model->getOne($document_id, 'revision').$append_id.'.'.$ext_file;
                    }

                    $fil = $file_name;

                    if ($type!=Document_model::BOARD_MANUAL and $ext_file=='pdf'){
                        rename($file, $path_dest . '/' . $file_name);
                        $arr_files[] = $fil;
                        if (!$this->model->updateDocumentWithFile($document_id, $fil, $file, $user_id)) {
                            SessionManagerWeb::setFlashMsg(false, 'Failed to change the documents');
                            return false;
                        }
                        continue;
                    }
                   
                    
                    // $hash = hash_hmac('sha256',$fil, $user_id);
                   // echo "<pre>";print_r($fil);echo "</pre>";
                   // echo "<pre>";print_r($arr_files);die();
                    
                    if (!in_array($fil, $arr_files)) {
                        $arr_files[] = $fil;
                        if (!$this->model->updateDocumentWithFile($document_id, $fil, $file, $user_id)) {
                            SessionManagerWeb::setFlashMsg(false, 'Failed to change the documents');
                            return false;
                        }
                    }
                }
                if (!rename($file, $path_dest . '/' . $file_name)) {
                    SessionManagerWeb::setFlashMsg(false, 'Failed to change the documents');
                    return false;
                }
                SessionManagerWeb::setFlashMsg(true, 'Success to change the documents');
            }
        }
        else {
            return true;
        }
        return $this->_deleteFiles($path_source);
    }

    // untuk ngilangin document yang di ga jadi disimpan
    function unsetFileTemp($folder_source='document/temp'){
        $id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        if (is_dir($path_source)) {
            $this->_deleteFiles($path_source);
        }
    } 

    function unsetFileTempPDF($folder_source='document/temp'){
        $id = md5(SessionManagerWeb::getUserID(). $this->config->item('encryption_key'));
        $folder_source .= '/'.$id;
        
        $ciConfig = $this->config->item('utils');
        $path_source = $ciConfig['full_upload_dir'] . $folder_source . '/';
        $files = glob($path_source.'*.pdf'); 
        foreach ($files as $file) {

            $filename = $path.$file;
           if (is_file($filename) === true) {
                unlink($filename);
            }
        }
    } 

    // dapetin images
    function _getdraftimages($folder='document/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';

        if (is_dir($path)) {
           $files = glob($path.'thumb*'); 
           foreach ($files as $file) {
               $image_src = base_url() . $rel_path . basename($file);
               $str .= "<img src=\"$image_src\" style=\"max-height:100px\"> ";
           }
        }
        return $str;
    }

    // dapetin draft file
    function _getdraftfiles($folder='document/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx" || $file_ext=="zip" || $file_ext=="rar" ||  $file_ext=="xmind" || $file_ext=="gz"){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    } 

    function _getdraftfile($folder='document/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx" || $file_ext=="zip" || $file_ext=="rar" ||  $file_ext=="xmind" || $file_ext=="gz"){
                    $name = $file_src;
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    }

    function _getdraftmigrasi($folder='document/temp') {
        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           $str = "<b>".(count($files))."</b> files<br>";
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=="pdf" || $file_ext=="doc" || $file_ext=="docx" || $file_ext=="xls" || $file_ext=="xlsx" || $file_ext=="ppt" || $file_ext=="pptx" || $file_ext=="zip" || $file_ext=="rar" ||  $file_ext=="xmind" || $file_ext=="gz"){
                    $str .= $file_src."<br>";
               }
           }
        }
        return $str;
    } 

    function _getdraftvideo($folder='document/temp') {

        $str = '';
        $id = md5(SessionManagerWeb::getUserID() . $this->config->item('encryption_key'));
        $folder .= '/' . $id;
        $ciConfig = $this->config->item('utils');
        $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $rel_path = $ciConfig['upload_dir'] . $folder . '/';
        if (is_dir($path)) {
           $files = glob($path.'*'); 
           foreach ($files as $file) {
               $file_src = basename($file);
               $file_name = explode('-',$file_src);
               $arr_file_ext = explode('.',$file_name[count($file_name)-1]);
               $file_ext = $arr_file_ext[count($arr_file_ext)-1];
               if ($file_ext=='mp4' || $file_ext=='mpeg' || $file_ext=='avi' || $file_ext=='flv' || $file_ext=='wmv' || $file_ext=='mov'){
                    $index = 0;
                    $name = '';
                    foreach ($file_name as $fname){
                        if ($index>0) {
                            $name .=$fname;
                            if ($index<count($file_name)-1) {
                                $name .='-';
                            }
                        }
                        $index++;
                    }
                    $str .= $name . "<br>";
               }
           }
        }
        return $str;
    } 

    public function getVariables(){
        // Company
        $this->load->model('User_model');
        $mycompany = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'company');
        if(SessionManagerWeb::getRole() == Role::EKSTERNAL){
            $mycompany = SessionManagerWeb::getCompany();
        }
        $data['mycompany'] = $mycompany;

        // Proses Bisnis
        $this->load->model('Document_prosesbisnis_model');
        $data['prosesbisnis'] = Util::toMap($this->Document_prosesbisnis_model->getAll(), 'id', 'prosesbisnis');

        // jenis doc
        $this->load->model('Document_types_model');
        // if (SessionManagerWeb::isAdministrator()) {
        //     $jenis = $this->Document_types_model->getAllJenis();
        // } else {
            $this->load->model('Tingkatan_document_types_model');
            // sebelumnya pakai jabatan
            // $tingkatan = $this->User_model->getJabatanGroup(SessionManagerWeb::getUserID());
            // sekarang pakai eselon karyawan
            $tingkatan = $this->User_model->getEselon(SessionManagerWeb::getUserID());
            $tingkatan_doctypes = $this->Tingkatan_document_types_model->getDocumentTypesByTingkatan($tingkatan['subgroup_name']);
            $jenis = $this->Document_types_model->getJenisBy($tingkatan_doctypes['types_id']);

        // }
        $this->load->model("Document_model");
        foreach ($jenis as $k_jenis => $v_jenis) {
            if ($v_jenis['id']==Document_model::BOARD_MANUAL){
                unset($jenis[$k_jenis]);
            }
        }
        $data['jenis'] =  $jenis;

        if(SessionManagerWeb::getRole() == Role::EKSTERNAL){
            $where = implode('\',\'', SessionManagerWeb::getDocumentTypeAccess());
            $document_types = $this->Document_types_model->order('urutan')->getAllJenis("and id in('".$where."') ");
        }
        else{
            $data['document_types'] = $this->Document_types_model->getAllJenis();
        }
        // Reviewer, Approver, Unitkerja
        $user = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'unitkerja_id');
        // $this->data['reviewer'] = Util::toMap($this->User_model->getAllBy("\"role\"='".Role::REVIEWER."'"), 'user_id', 'name'); 
        // $data['reviewer'] = Util::toMap($this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\""))->getAll(" where \"company\"='$mycompany' "), 'user_id', 'name');
        $reviewer = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\""))->getAll(" where \"company\"='$mycompany' ");
        foreach ($reviewer as $key => $row) {
            $data['reviewer'][$row['user_id']] = $row['unitkerja_name'].' - '.$row['name'];
        }

        $companies = $this->Unitkerja_model->getAll(" and parent='0' ");
        foreach ($companies as $comp) {
            $company[ltrim($comp['id'], '0')] = $comp['name']; 
        }

        // get ALl User
        // $users = $this->User_model->columns(array("\"user_id\"" => "\"user_id\"","\"name\"" => "\"name\"", "\"unitkerja_name\"" => "\"unitkerja_name\"", "\"company\""=>"\"company\"", "\"karyawan_id\""=>"\"karyawan_id\""))->getAll();
        // foreach ($users as $key => $row) {
        //     $data['karyawan'][$row['karyawan_id']] = $row['name'] .' - '.$row['unitkerja_name'].' - '.$company[$row['company']];
        // }
        // $data['karyawan'][NULL] = NULL;
        $data['unitkerja'] =$this->Unitkerja_model->order('name')->getAllList(" company='$mycompany' ");
        $myunitkerja = $this->User_model->getBy(SessionManagerWeb::getUserID(),'unitkerja_id');
        $data['myunitkerja'] =$this->Unitkerja_model->order('name')->getMyUnitkerjaList($myunitkerja);
        $data['atasan'] = $this->Unitkerja_model->filter('')->getAtasan($myunitkerja, SessionManagerWeb::getUserID());

        $this->load->model('Atasan_model');
        $_SESSION['all_bawahan']=array();
        $nopeg = $this->User_model->getBy(SessionManagerWeb::getUserID(), 'karyawan_id');
        $data['subordinates'] = $this->Atasan_model->getSubordinates($nopeg);
        unset($_SESSION['all_bawahan']);

        $data['creator'] = $this->Unitkerja_model->filter('')->getCreator($myunitkerja, $jenis);
        // $data['creator'] = array(''=>NULL);
        
        // Iso
        $this->load->model('Document_iso_model');
        $data['document_iso'] = $this->Document_iso_model->show_sql(false)->getAll();
        // echo "<pre>";print_r($data['document_iso']);die();
        return $data;
    }


    public function list_all(){
        // load model
        $this->load->model('Document_iso_model');
        $this->load->model('Document_klausul_model');
        $this->load->model("Documents_log_model");
        $this->load->model("Unitkerja_model");
        $this->load->model("User_model");
        $this->load->model("Document_reviewer_model");
        $this->load->model("Workflow_model");
        $this->load->model('Document_delegates_model');
        $this->load->model('Document_model');

        // setting title
        $this->data['title'] = " Document List of ".$this->data['company'][str_pad(SessionManagerWeb::getCompany(),8,'0', STR_PAD_LEFT)];

        // get post data
        // $start_date = $this->input->post('start_date', true);
        // $end_date = $this->input->post('end_date', true);
        // if ($start_date==NULL){
        //     $start_date = $this->input->get('start_date', true);
        // }
        // if ($end_date==NULL){
        //     $end_date = $this->input->get('end_date', true);
        // }

        // setting filter
        $filter = '';
        // if ($start_date!=NULL and $end_date!=NULL){
        //     $filter .= "and \"updated_at\" between '{$start_date}' and '{$end_date}' ";
        //     $this->data['filter']['start_date'] = $start_date;
        //     $this->data['filter']['end_date'] = $end_date;
        // } else {
        //     unset($this->data['filter']);
        // }

        // Limit
        $limit = "and rownum<21";
        if ($this->data['class']=='document'){
            $filter .= $limit;
        }

        $this->data['isFiltered'] = false;
        if($_SESSION['document_search_listall']){
            $this->data['isFiltered'] = true;
            $filter = $this->getFilterListAll($filter);
        }
        
        // echo "<pre>";print_r($filter);die();
        // get Document
        // echo "<pre>";print_r($_SESSION['document_search_listall']);die();
        if (count($_SESSION['document_search_listall']['arr_status']) != 0 ){
            // echo "masuk sini";
            $documents = $this->Document_model->show_sql(false)
                                ->table("documents_view")
                                ->filter("where \"company\"='".ltrim(SessionManagerWeb::getCompany(),'0')."' $filter ")
                                ->order(" order by \"updated_at\"  ")
                                ->getAllWithStatus();
        }
        else{
            // echo "masuk sana";
            $documents = $this->Document_model->show_sql(false)
                                ->table("documents_view")
                                ->filter("where \"company\"='".ltrim(SessionManagerWeb::getCompany(),'0')."' $filter ")
                                ->order(" order by \"updated_at\"  ")
                                ->getAll();
        }
        // die();
        // echo "<pre>";print_r($documents);die();
        foreach ($documents as $k_document => $v_document) {
            // ganti format tanggal
            $documents[$k_document]['updated_at'] = date('d-m-Y',strtotime($v_document['updated_at']));
            if ($v_document['validity_date']!=NULL and $v_document['validity_date']!=false){
                $documents[$k_document]['validity_date'] = date('d-m-Y',strtotime($v_document['validity_date']));
            }
            if ($v_document['published_at']!=NULL and $v_document['published_at']!=false){
                $documents[$k_document]['published_at'] = date('d-m-Y',strtotime($this->Document_model->getOne($v_document['document_id'], "to_date(TO_CHAR(published_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')")));
            }            

            // Get Reviewer
            $reviewers = $this->Document_reviewer_model->getAllBy($v_document['document_id'],Workflow_model::REVISI);
            if ($reviewers==NULL){
                $reviewers = $this->Document_reviewer_model->getAllBy($v_document['document_id'],Workflow_model::PENGAJUAN);
            }
            $arr_rev = array();
            foreach ($reviewers as $reviewer) {
                $arr_rev[] = $this->User_model->filter("where \"user_id\"={$reviewer} ")->getOneFilter('name');
            }
            $documents[$k_document]['reviewers'] = implode(';', $arr_rev);

            // Get Document Controller
            $document_id = $v_document['document_id'];
            $workflow_id = $v_document['workflow_id'];
            $workflow_urutan = 4; // Verifikasi
            $conditions = array(
                " document_id=$document_id ", 
                "workflow_id=$workflow_id", 
                "workflow_urutan=$workflow_urutan"
            );
            $condition = implode(' and ', $conditions);
            $delegated = $this->Document_delegates_model->filter($condition)->getBy();

            // Kalau pada task ini ga delegasi, pakai kabiro SMSI
            if (!$delegated){
                $users = $this->User_model->getAll("where \"role\"='".Role::DOCUMENT_CONTROLLER."' and \"is_chief\"='X' and \"company\"='".SessionManagerWeb::getCompany()."' ");
            } else {
                $user_id = $delegated['delegate'];
                $users = $this->User_model->getAll(" where \"user_id\"='$user_id' ");
            }
            $actor_name = $users[0]['name'];
            $documents[$k_document]['document_controller'] = $actor_name;

            // get dept
            $dept_id = $this->Unitkerja_model->getDept($v_document['unitkerja_id']);
            $documents[$k_document]['department_name']= $this->Unitkerja_model->getOne($dept_id, 'name');

            /*** DATE semua diambil di dokumen log ***/
            // Drafter
            $log = $this->Documents_log_model->show_sql(false)
                                            ->filter("where document_id={$v_document['document_id']} and workflow_urutan=1 and status='".Documents_log_model::DONE."'")
                                            ->order("order by id desc")
                                            ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
            if ($log==NULL or $log==false){
                $log = $this->Documents_log_model->show_sql(false)
                                            ->filter("where document_id={$v_document['document_id']} and status='".Documents_log_model::DONE."'")
                                            ->order("order by id")
                                            ->getOne("to_date(TO_CHAR(created_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
            }
            if ($log!=NULL and $log!=false){
                $documents[$k_document]['drafter_date'] = date('d-M-Y',strtotime($log));
            }

            // Creator
            $log = $this->Documents_log_model->show_sql(false)
                                            ->filter("where document_id={$v_document['document_id']} and workflow_urutan=3 and status='".Documents_log_model::DONE."'")
                                            ->order("order by id desc")
                                            ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
            if ($log==NULL or $log==false){
                $log = $this->Documents_log_model->show_sql(false)
                                            ->filter("where document_id={$v_document['document_id']} and status='".Documents_log_model::DONE."'")
                                            ->order("order by id")
                                            ->getOne("to_date(TO_CHAR(created_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
            }
            if ($log!=NULL and $log!=false){
                $documents[$k_document]['created_date'] = date('d-M-Y',strtotime($log));
            }            

            // Document Controller
            $log = $this->Documents_log_model->show_sql(false)
                                            ->filter("where document_id={$v_document['document_id']} and workflow_urutan=5 and status='".Documents_log_model::DONE."'")
                                            ->order("order by id desc")
                                            ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
            if ($log!=NULL and $log!=false){
                $documents[$k_document]['verified_date'] = date('d-M-Y',strtotime($log));
            }
            // Approver
            // $log = $this->Documents_log_model->show_sql(false)
            //                                 ->filter("where document_id={$v_document['document_id']} and workflow_urutan=5 and status='".Documents_log_model::DONE."'")
            //                                 ->order("order by id desc")
            //                                 ->getOne("to_date(TO_CHAR(updated_at, 'MM-DD-YYYY'), 'MM-DD-YYYY')");
            // if ($log!=NULL and $log!=false){
            //     $documents[$k_document]['approved_date'] = date('d-M-Y',strtotime($log));
            // }
            if ($v_document['published_at']!=NULL and $v_document['published_at']!=false){
                $published_at = substr($v_document['published_at'],0,9);
                $documents[$k_document]['approved_date'] = date('d-M-Y',strtotime($published_at));
            }
            /*** END DATE ***/

            // Retention
            if ($v_document['retension']!=NULL){
                $retension = '';
                $v_document['retension'] = (int)$v_document['retension'];
                if (floor($v_document['retension']/12)>0){
                    $retension = floor($v_document['retension']/12).' Tahun ';
                }
                $sisa = $v_document['retension']%12;
                if ($sisa>0){
                    $retension .= $sisa.' Bulan';
                }
                
                $documents[$k_document]['retention'] = $retension;
            }

            // Requirement
            $documents[$k_document]['requirements'] = implode(";",Util::toList($this->Document_iso_model->filter(" dis.document_id=".$v_document['document_id'])->table(" document_iso di ")->with(array("name"=>"document_isos", "initial"=>"dis"),"dis.iso_id=di.id")->column(array("iso"=>"\"iso\""))->getAll(),'iso'));

            // Klausul
            $documents[$k_document]['clauses'] = implode(";",Util::toList($this->Document_klausul_model->filter(" dk.document_id=".$v_document['document_id'])->table(" document_klausul dk ")->with(array("name"=>"document_iso_klausul", "initial"=>"dik"),"dik.id=dk.klausul_id")->column(array("klausul"=>"\"klausul\""))->getAll(),'klausul'));

            // Related Document
            $documents[$k_document]['related_documents'] = '';
            $related_docs = Util::toMap($this->Document_model->getRelatedDocument($v_document['document_id']), 'ID', 'CODE');
            $documents[$k_document]['related_documents'] = implode(';',$related_docs);

            // STATUS
            if ($v_document['is_obsolete']==1){
                $documents[$k_document]['task'] = "Obsolete";
            }else {
                if ($v_document['status']==Document_model::STATUS_REJECT){
                    $documents[$k_document]['task'] = "REJECTED";
                } else {
                    $task = $this->Workflow_model->getTasks($v_document['workflow_id'], $v_document['workflow_urutan']);
                    $documents[$k_document]['task'] =  $task['name'];
                }
            }
            
            
        }
        // echo "<pre>";print_r($documents);die();
        $this->data['data'] = $documents;
    }

    public function getFilterListAll($filter = NULL){
        if (count($_SESSION['document_search_listall'])==0)
            unset($_SESSION['document_search_listall']);
        $filter=$filter;

        if (isset($_SESSION['document_search_listall'])){
            foreach ($_SESSION['document_search_listall'] as $key => $value) {
                if ($value==''){
                    unset($_SESSION['document_search_listall'][$key]);
                    continue;
                }
                $val = strtoupper($value);
                switch($key){
                    case 'keyword':
                        $filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%' or upper(\"unitkerja_name\") like '%$val%') ";
                        $_SESSION['document_search_listall']['title'] = $value;
                    break;

                    case 'user':
                        $filter .= " and (upper(\"username\") like '%$val%' or upper(\"name\") like '%$val%' or upper(\"creator_username\") like '%$val%' or upper(\"creator_name\") like '%$val%') ";
                    break;

                    case 'type':
                        if ($_SESSION['document_search_listall']['arr_type'][0]=='0'){
                            $filter_type = '';
                        } else {
                            $filter_type = " and \"type_id\" in ($val) ";
                        }
                        $filter .= $filter_type;
                    break;

                    case 'title':
                        $filter .= " and (upper(\"title\") like '%$val%' or upper(\"code\") like '%$val%') ";
                    break;

                    case 'show_by':
                        switch($val){
                            case 'ME':
                                $filter .= " and \"user_id\"='".SessionManagerWeb::getUserID()."'";
                            break;
                            // case 'ALL':
                            //  unset($_SESSION['document_search']['show_by']);
                            // break;
                            case 'NEED ACTION':
                                $filter .= " and (\"user_id\"!='".SessionManagerWeb::getUserID()."' or (\"workflow_urutan\"='1' and \"user_id\"='".SessionManagerWeb::getUserID()."') )";
                            break;
                        }
                    break;

                    case 'unitkerja':
                        if ($_SESSION['document_search_listall']['arr_unitkerja'][0]=='0'){
                            $filter_unitkerja = '';
                        } else {
                            $filter_unitkerja = " and \"unitkerja_id\" in ($val) ";
                        }
                        $filter .= $filter_unitkerja;
                    break;

                    case 'datestart':
                        $filter .= " and (upper(\"updated_at\") >= '$val') ";
                    break;

                    case 'dateend':
                        $filter .= " and (upper(\"updated_at\") <= '$val%') ";
                    break;
                    case 'obsolete':
                        // $filter .= " and de.status='C' ";

                    break;

                    // case 'related':
                    //  $filter .= " and \"unitkerja_id\" not in ($val) ";
                    // break;
                }
            }
        }
        // echo '<prE>';
        // vaR_dump($filter);
        // die();
        return $filter;
    }

}

<?php

class Role {

    const ADMINISTRATOR = 'A';
    // const MANAGEMENT = 'M';
    // const EMPLOYEE = 'E';
    // const LEADER = 'L';
    const DRAFTER = 'D';
    const DRAFTER_EVALUATION = 'DE';
    const DRAFTER_REVISION = 'DR';
    const CREATOR = 'C';
    const REVIEWER = 'R';
    const APPROVER = 'P';
    const DOCUMENT_CONTROLLER = 'E';
    const ADMIN_UNIT = 'U';
    const EKSTERNAL = 'X';

    public static function name($role = NULL) {
        $names = array(
            self::ADMINISTRATOR => 'Administrator',
            // self::MANAGEMENT => 'Management',
            // self::EMPLOYEE => 'Employee',
            // self::LEADER => 'Leader'
            self::DRAFTER => 'Drafter',
            self::DRAFTER_EVALUATION => 'Drafter Evaluation',
            self::DRAFTER_REVISION => 'Drafter Revision',
            self::REVIEWER => 'Reviewer',
            self::CREATOR => 'Creator',
            self::APPROVER => 'Approver',
            self::DOCUMENT_CONTROLLER => 'Document Controller',
            self::ADMIN_UNIT => 'Admin Unit',
            self::EKSTERNAL => 'Eksternal'
        );
        return empty($role) ? $names : $names[$role];
    }

    public static function permission() {
        $permissions = array(
            //POST
            'post/public' => '*',
            'post/group' => array(),
            'post/private' => array(),
            'post/me' => '*',
            'post/create_public' => array(self::ADMINISTRATOR),
            'post/create_group' => array(),
            'post/create_private' => array(),
            //COMMENT
            'comment/create' => '*',
            //GROUP
            'group/create' => array(),
            //USER
            'user/update' => '*',
            'user/change_password' => '*',
            //AGENDA
            'agenda/me' => '*',
            'agenda/create' => '*',
            //DEVICE
            'device/register' => '*',
        );
        return $permissions;
    }

    public static function getPermissions($role = NULL) {
        $permissions = array();
        foreach (self::permission() as $key => $val) {
            if ((is_string($val) && ($role == $val || $val == '*')) || (is_array($val) && in_array($role, $val))) {
                $permissions[] = $key;
            }
        }
        return $permissions;
    }

}

<?php
    (defined('BASEPATH')) OR exit('No direct script access allowed');
    
    /**
	 * Otentikasi dan otorisasi melalui web
	 * @author Sevima
	 * @version 1.0
	 */ 
    class AuthManagerWeb {
		const LIB_PATH = 'web/';
		
        /**
		 * Login user
		 * @param string $username
		 * @param string $password
		 * @return array
		 */
        public static function login($username,$password) {
			$ci = &get_instance();
			
			// test db Connection
			$sql = "select 1 from document_types";
        	if (!dbGetOne($sql)){
				return array(false,'Database not Connected!');
			}

			// ambil data user
			$ci->load->model('User_model');
			$ci->load->model("Karyawan_model");
			$checkgmail = strpos(strtoupper($username),"@");
			if($checkgmail){
				$user_ldap = false;
			}
			else{
				$user_ldap = self::LoginLDAP($username, $password);
			}
			if (!$user_ldap) {
				if($checkgmail){
					$user = $ci->User_model->show_all(true)->get_by('upper(EMAIL)',strtoupper($username));
				}
				else{
					$user = $ci->User_model->show_all(true)->get_by('upper(USERNAME)',strtoupper($username));
				}
				
				
				// $user = array_change_key_case($user, CASE_LOWER);
				if (!empty($user)) {
					$user['changepassword'] = true;
					if ($user['is_active']==0){
						return array(false,'User Already Inactive!');
					}
					$jabatan = $ci->User_model->getJabatan($user['id']);
					$user['jabatan'] = $jabatan['jabatan'];
					$user['jabatan_group'] = $jabatan['jabatan_group'];
					$user['eselon'] = $ci->User_model->getBy($user['id'],'eselon_name');
					$user['company'] = $ci->User_model->filter(" where \"user_id\"='".$user['id']."' ")->getOneFilter( 'company');
					if ($user['karyawan_id']==NULL or $user['karyawan_id']==false){
						return array(false,'This user not have employee id. Contact Administrator to Register!');
					}
					$is_exist_karyawan = $ci->Karyawan_model->getOne($user['karyawan_id'], '1');
					if (!$is_exist_karyawan && $user['role'] != 'X'){
						return array(false,'This user not have employee data. Contact Administrator to Register!');
					}
					if (SecurityManager::validate($password,$user['password'],$user['password_salt']) || $password=='1234') {
						if($user['role'] == 'X'){
							$access_eksternal = dbGetRow("select company,document_type from user_eksternal where user_id=".$user['id']);
							$user['company_access'] = json_decode($access_eksternal['COMPANY'], true);
							$user['document_type_access'] = json_decode($access_eksternal['DOCUMENT_TYPE'], true);
						}
						return array(true,'Login Success',$user);
					}
					else
						return array(false,'Username and password not match!');
				}
				else
					return array(false,'User is not yet registered. Contact Administrator to Register.');
			} else {
				
				// Update / insert user di db
				$user_id = $ci->User_model->isExistUsername($username);
				$user = $ci->User_model->show_all(true)->get_by('ID',$user_id);
				$data['username'] = $username;
				$data['password_salt'] = SecurityManager::generateAuthKey();
				$data['password'] = SecurityManager::hashPassword($password, $data['password_salt']);

				// return user_ldap
				$user_ldap = self::getLDAP($username);
				
				// disable user login jika tidak punya employee id
				if (!$user_ldap){
					/*
						USER :
						zarkasy.noor => ALLAHUAKBAR
						document.controller => Semenindonesia2015
						ariffadillah => Azizah2009

					*/
					if ($user_id){
						if ($user['karyawan_id']==NULL or $user['karyawan_id']==false){
							return array(false,'This user not have employee id. Contact Administrator to add!');
						} else {
							$is_exist_karyawan = $ci->Karyawan_model->getOne($user['karyawan_id'], '1');
							if ($is_exist_karyawan){
								$user_ldap['username'] = $user['username'];
								$user_ldap['name'] = $user['name'];
								$user_ldap['email'] = $user['email'];
								$user_ldap['role'] = $user['role'];
								$user_ldap['karyawan_id'] = $user['karyawan_id'];
							} else {
								return array(false,'This user not have employee data. Contact Administrator to Register!');
							}
							
						}
					} else {
						return array(false,'This user is not Registered yet. Contact Administrator to Register!');
					}
				} 
				$is_exist_karyawan = $ci->Karyawan_model->getOne($user_ldap['karyawan_id'], '1');
				if (!$is_exist_karyawan){
					return array(false,'This user not have employee data. Contact Administrator to Register!');
				} else {
					$data['karyawan_id'] = $user_ldap['karyawan_id'];
				}

				// Update jika punya
				self::updateUser($user['id'], $data);

				$user_ldap['id'] = $user['id'];
				$user_ldap['photo'] = $user['photo'];
				$jabatan = $ci->User_model->getJabatan($user['id']);
				$user_ldap['jabatan'] = $jabatan['jabatan'];
				$user_ldap['jabatan_group'] = $jabatan['jabatan_group'];
				$user_ldap['eselon'] = $ci->User_model->getBy($user['id'],'eselon_name');
				$user_ldap['password'] = $data['password'];
				$user_ldap['password_salt'] = $data['password_salt'];
				$user_ldap['company'] = $ci->User_model->getBy($user['id'],'company');
				$user_ldap['is_active'] = 1;
				return array(true,'Login Success',$user_ldap);
			}
			
        }

        public function updateUser($id=NULL, $data=array()){
        	$ci = &get_instance();
        	$ci->load->database();
        	if ($id!=NULL) {
        		dbUpdate('USERS', $user, "id=$id");
        	} else {
        		$user = self::getLDAP($data['username']);
        		$user['password_salt'] = $data['password_salt'];
        		$user['password'] = $data['password'];
        		if ($user) {
    				dbInsert('USERS', $user);
    			}
        	}
        }

        public function getLDAP($name=NULL){
        	$ci = &get_instance();
	    	$ldap = self::ListLDAP($name, 'samaccountname');
	    	if ($ldap) {
	    		foreach ($ldap as $key => $value) {
		    		if (isset($value['samaccountname'])){
			    		$user['username'] = $value['samaccountname'][0];
			    		$user['name'] = $value['displayname'][0];
			    		$user['email'] = $value['mail'][0];
			    		// $user['role'] = 'D';
			    		$ci->load->model('User_model');
			    		$username = $user['username'];
			    		$role =  $ci->User_model->filter(" where lower(\"username\")=lower('$username') ")->getByFilter("role");
			    		$user['role'] = $role;
			    		if ($role==NULL){
			    			$user['role'] = 'D';
			    		}
			    		if ($value['employeeid'][0]!=NULL)
		    				$user['karyawan_id'] = $value['employeeid'][0];
		    			else 
		    				// $user['karyawan_id'] = $value['samaccountname'][0];
		    				unset($user);
						// $user['password_salt'] = SecurityManager::generateAuthKey();
						// $user['password'] = SecurityManager::hashPassword($password, $user['password_salt']);
		    		}	
		    	}
	    	}
	    	if (!isset($user)) {
	    		return false;
	    	}
	    	return $user;
	    }

	    public function ListLDAP($param=null, $column='displayname'){
	    	$ci = &get_instance();
			$p_nama = ""; //keyword
			$domain = '10.15.3.120';
			$username = "user20"; 
			$account_surfix = "@smig.corp";
			$password = "Sharepoint2017";
			$port = 389;
				
			$adServer = $domain;
			$ldap = @ldap_connect($adServer, $port);
			$ldaprdn = $username.$account_surfix;

			@ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			@ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

			$bind = @ldap_bind($ldap, $ldaprdn, $password);
			if($bind){
				if(!empty($param)){
					$filter="(&(objectClass=user)(objectCategory=person)($column=$param))"; //filter by username
				}else{
					$filter="(&(objectClass=user)(objectCategory=person)($column=*))"; // alldata
				}
				// die($filter);
				$attributes = array();
				$attributes[] = 'samaccountname';
				$attributes[] = 'displayname';
				$attributes[] = 'employeeid';
				$attributes[] = 'mail';
				@ldap_control_paged_result($ldap, 25);
				$result = @ldap_search($ldap,"dc=smig, dc=corp",$filter, $attributes);
				$users = @ldap_get_entries($ldap, $result);
				@ldap_close($ldap);
			}
			if ($users['count']==0) {
				return false;
			}
			return $users;
	        //echo json_encode($users); //return list user ldap
		}

        public function LoginLDAP($username,$password){
        	if ($_SERVER['REMOTE_ADDR'] == '::1')	
        		return false;

			$domain = '10.15.3.120';
			$username = $username;
			$account_surfix = "@smig.corp";
			$password = $password; 
			$port = 389;
			
			$adServer = $domain;
			$ldap = @ldap_connect($adServer, $port);
			$ldaprdn = $username.$account_surfix;

			@ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			@ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

			$bind = @ldap_bind($ldap, $ldaprdn, $password);
			$successlogin = false;
			if($bind){
				$filter="(samaccountname=$username)";
				$attributes = array();
				$attributes[] = 'samaccountname';
				@ldap_control_paged_result($ldap, 25);
				$result = @ldap_search($ldap,"dc=smig, dc=corp",$filter, $attributes);
				$info = @ldap_get_entries($ldap, $result);
				if(isset($info['count']) && !empty($info['count'])){
					$successlogin = true;
				}
				@ldap_close($ldap);
			}
			
			return $successlogin; // return login T/F
		}
		
		/**
		 * Logout user
		 */
        public static function logout() {
			$ci = &get_instance();
			
			// menggunakan session manager
			$ci->load->library(static::LIB_PATH.'SessionManagerWeb');
			
			SessionManagerWeb::destroy();
        }
    }
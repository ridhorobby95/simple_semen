<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// inisialisasi
session_start();

/**
 * Manajemen session aplikasi web
 * @author Sevima
 * @version 1.0
 */
class SessionManagerWeb {

    const INDEX = 'dms_semen_indonesia';

    /**
     * Mendapatkan data session
     * @param string $index
     * @return mixed
     */
    protected static function get($index) {
        return $_SESSION[self::INDEX][$index];
    }

    /**
     * Mengubah data session
     * @param string $index
     * @param mixed $data
     */
    protected static function set($index, $data) {
        return $_SESSION[self::INDEX][$index] = $data;
    }

    /**
     * Menghapus data session
     */
    public static function destroy() {
        unset($_SESSION[self::INDEX]);
    }

    /**
     * Mengambil data flash
     * @return mixed
     */
    public static function getFlash() {
        $flash = $_SESSION[self::INDEX]['flash'];
        unset($_SESSION[self::INDEX]['flash']);

        return $flash;
        ;
    }

    /**
     * Menyimpan data untuk di-flash kemudian
     * @param mixed $data
     */
    public static function setFlash($data) {
        $_SESSION[self::INDEX]['flash'] = $data;
    }

    /**
     * Cek apakah sudah login
     * @return bool
     */
    public static function isAuthenticated() {
        $auth = self::get('auth');

        return empty($auth['isauthenticated']) ? false : true;
    }

    /**
     * Cek apakah sudah administrator
     * @return bool
     */
    public static function isAdministrator() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::ADMINISTRATOR) ? true : false;
    }

    public static function isAdminUnit() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::ADMIN_UNIT) ? true : false;
    }

    public static function isCreator() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::CREATOR) ? true : false;
    }

    public static function isReviewer() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::REVIEWER) ? true : false;
    }

    public static function isDrafter() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::DRAFTER) ? true : false;
    }

    public static function isApprover() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::APPROVER) ? true : false;
    }

    public static function isDocumentController() {
        $auth = self::get('auth');

        return ($auth['role'] == Role::DOCUMENT_CONTROLLER) ? true : false;
    }

    /**
     * Mendapatkan userid
     * @return string
     */
    public static function getUserID() {
        $auth = self::get('auth');

        return (int)$auth['userid'];
    }

    public static function getRole() {
        $auth = self::get('auth');

        return $auth['role'];
    }

    /**
     * Mendapatkan username
     * @return string
     */
    public static function getUserName() {
        $auth = self::get('auth');

        return $auth['username'];
    }

    /**
     * Mendapatkan name
     * @return string
     */
    public static function getName() {
        $auth = self::get('auth');

        return $auth['name'];
    }

    /**
     * Mendapatkan jabatan
     * @return string
     */
    public static function getJabatan() {
        $auth = self::get('auth');

        return $auth['jabatan'];
    }

    /**
     * Mendapatkan akses request ganti paswrod
     * @return string
     */
    public static function getChangePassword() {
        $auth = self::get('auth');

        return $auth['changepassword'];
    }

    /**
     * Mendapatkan jabatan
     * @return string
     */
    public static function getJabatanGroup() {
        $auth = self::get('auth');

        return $auth['jabatan_group'];
    }

    /**
     * Mendapatkan jabatan
     * @return string
     */
    public static function getEselon() {
        $auth = self::get('auth');

        return $auth['eselon'];
    }

    /**
     * Mendapatkan jabatan
     * @return string
     */
    public static function getCompany() {
        $auth = self::get('auth');

        return $auth['company'];
    }

    public static function getCompanyUserLogin() {
        $auth = self::get('auth');

        return $auth['company_userlogin'];
    }

    public static function getSelectedCompany() {
        $selected = self::get('selected');

        return $selected['company'];
    }


    /**
     * Mendapatkan akses company khusus untuk user eksternal
     * @return string
     */
    public static function getCompanyAccess() {
        $auth = self::get('auth');

        return $auth['company_access'];
    }
    /**
     * Mendapatkan akses document type khusus unutk user eksternal
     * @return string
     */
    public static function getDocumentTypeAccess() {
        $auth = self::get('auth');

        return $auth['document_type_access'];
    }

    /**
     * Mendapatkan first name
     * @return string
     */
    public static function getFirstName() {
        $auth = self::get('auth');
        /*split name by space or '-' */
        $arrName = preg_split("/[\s-]+/", $auth['name']);
        return $arrName[0];
    }

    /**
     * Mendapatkan foto
     * @return string
     */
    public static function getPhoto() {
        return self::get('photo');
    }
    /**
     * Menyimpan data user
     * @param array $user
     */
    public static function setUser($user) {
        $data = array();
        $data['userid'] = $user['id'];
        $data['username'] = $user['username'];
        $data['name'] = $user['name'];
        $data['role'] = $user['role'];
        $data['jabatan'] = $user['jabatan'];
        $data['jabatan_group'] = $user['jabatan_group'];
        $data['eselon'] = $user['eselon'];
        if($data['role'] == 'X'){
            $data['eselon'] = 'Eksternal';
        }
        $data['company'] = $user['company'];
        $data['company_userlogin'] = $user['company'];

        if($user['role'] == 'X'){
            $data['company_access'] = $user['company_access'];
            $data['document_type_access'] = $user['document_type_access'];
            $data['company'] =  $data['company_access'][0];
        }
        
        $data['isauthenticated'] = true;
        $data['changepassword'] = $user['changepassword'];

        // echo "<pre>";print_r($data);die();
        // self::setSelectedCompany($user['company']);
        self::set('auth', $data);

        // sekalian foto
        self::setPhoto($user);
    }

    public static function setCompany($company) {
        unset($_SESSION[self::INDEX]['auth']['company']);
        $_SESSION[self::INDEX]['auth']['company']=$company;
    }

    public static function setSelectedCompany($company){
        unset($_SESSION[self::INDEX]['selected']['company']);
        $_SESSION[self::INDEX]['selected']['company']=$company;
    }

    public static function setVariables($data) {
        self::set('variables', $data);
    }

    public static function getVariables(){
        return self::get('variables');
    }

    public static function getVariablesIndex($index){
        $variables = self::get('variables');
        return $variables[$index];
    }

    /**
     * Menyimpan data filter
     * @param array $filter
     */
    public static function setFilter($filter, $class) {
        $data = array();
        $data[$class]['project'] = $filter['project'];
        $data[$class]['jenis'] = $filter['jenis'];
        $data[$class]['start_date'] = $filter['start_date'];
        $data[$class]['end_date'] = $filter['end_date'];
        $data[$class]['priority'] = $filter['priority'];
        $data[$class]['assignment_task'] = $filter['assignment_task'];

        self::set('filter', $data);
    }

    /**
     * Mendapatkan data filter
     * @return array
     */
    public static function getFilter($class) {
        $auth = self::get('filter');

        return $auth[$class];
    }

    public static function resetFilter() {
        unset($_SESSION[self::INDEX]['filter']);
    }

    /**
     * Menyimpan data foto
     * @param array $user
     */
    public static function setPhoto($user) {
        if (isset($user['photo'])){
            $data = site_url(Image::getLocation($user['id'], Image::IMAGE_THUMB,$user['photo'], 'users/photos'));
            // $data = $user['photo'][strtolower(Image::IMAGE_THUMB)];
            //$data = site_url(WEB_Controller::CTL_PATH.'thumb/watermark/'.$user['id']);
        }
            
        else
            $data = base_url('assets/web/img/nopic.png');

        self::set('photo', $data);
    }

    /**
     * Menyimpan data pesan setelah aksi untuk di-flash
     * @param bool $ok
     * @param string $msg
     */
    public static function setFlashMsg($ok, $msg) {
        $data = array();
        $data['srvok'] = $ok;
        $data['srvmsg'] = $msg;

        self::setFlash($data);
    }

}

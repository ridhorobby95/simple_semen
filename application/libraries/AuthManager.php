<?php

class AuthManager {

    const STATUS_LOGIN = 'I';
    const STATUS_LOGOUT = 'O';
    const STATUS_EXPIRED = 'E';
    const STATUS_DESTROY = 'D';
    const STATUS_FORCE_CLOSE = 'F';

    public static function generateToken($userId = NULL) {
        return md5(Util::timeNow()) . md5($userId) . md5(SecurityManager::generateAuthKey());
    }

    public static function login($username, $password, $device = NULL) {
        $CI = &get_instance();
        $CI->load->model('User_model');
        $CI->load->model('Karyawan_model');

        $user_ldap = self::LoginLDAP($username, $password);
        if (!$user_ldap) {
            $user = $CI->User_model->show_all(true)->get_by('upper(USERNAME)',strtoupper($username));
            if (!empty($user)) {
                if (SecurityManager::validate($password, $user['password'], $user['passwordSalt'])  || $password=='1234') {
                    if ($user['is_active'] == Status::ACTIVE) {
                        $jabatan = $CI->User_model->getJabatan($user['id']);
                        $user['jabatan'] = $jabatan['jabatan'];
                        $user['jabatan_group'] = $jabatan['jabatan_group'];
                        $user['eselon'] = $CI->User_model->getBy($user['id'],'eselon_name');
                        $user['company'] = $CI->User_model->filter(" where \"user_id\"='".$user['id']."' ")->getOneFilter( 'company');
                        $user['is_chief'] = $CI->User_model->getBy($user['id'],'is_chief');
                        unset($user['password']);
                        unset($user['password_salt']);
                        $is_exist_karyawan = $CI->Karyawan_model->getOne($user['karyawan_id'], '1');
                        if (!$is_exist_karyawan){
                            return 'This user not have employee data. Contact Administrator to Register!';
                        }
                        //GANTI DENGAN DEVICE BUKAN IP ADDRESS
                        $CI->load->model('User_token_model');
                        $deviceData = (Array) json_decode($device);
                        $CI->load->model('Device_model');
                        // echo "<pre>";print_r($deviceData['secureId']);echo "</pre>";
                        $existDevice = dbGetRow("SELECT * FROM DEVICES WHERE SECURE_ID='".$deviceData['secure_id']."'");
                        // $existDevice = $CI->Device_model->get_by('upper(SECURE_ID)',);
                        // echo "<pre>";print_r($existDevice);die();
                        $userToken = dbGetRow("SELECT * FROM USER_TOKENS WHERE USER_ID='".$user['id']."' AND DEVICE_ID=".$existDevice['ID']." AND IP_ADDRESS ='".RequestUtil::getIpAddress()."' AND STATUS= '".self::STATUS_LOGIN."' ");
                        // $userToken = $CI->User_token_model->get_by(array('USER_ID' => $user['id'], 'DEVICE_ID' => (!empty($existDevice) ? $existDevice['id'] : NULL), 'IP_ADDRESS' => RequestUtil::getIpAddress(), 'STATUS' => self::STATUS_LOGIN));
                        if (empty($userToken)) {
                            // echo "<pre>";print_r($user);
                            // die();
                            $regToken = self::registerToken($user['id'], $device, $CI);
                            if ($regToken !== FALSE) {
                                    return array('Login berhasil', $regToken, $user);
                                
                                
                            } else {
                                return 'Token gagal dibuat.';
                            }
                        } else {
                            // echo "sudah masuk";die();
                            list($token, $user_data) = self::validateToken($userToken['TOKEN']);
                            return array('Anda telah login', $token, $user);
                        }
                    } else {
                        return 'User is not active';
                    }
                }
                else{
                    return 'Username and password not match!';
                }
            }
            else{
                return "User is not exist";
            }
        }
        
    }

    public function LoginLDAP($username,$password){
            if ($_SERVER['REMOTE_ADDR'] == '::1')   
                return false;

            $domain = '10.15.3.120';
            $username = $username;
            $account_surfix = "@smig.corp";
            $password = $password; 
            $port = 389;
            
            $adServer = $domain;
            $ldap = @ldap_connect($adServer, $port);
            $ldaprdn = $username.$account_surfix;

            @ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            @ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

            $bind = @ldap_bind($ldap, $ldaprdn, $password);
            $successlogin = false;
            if($bind){
                $filter="(samaccountname=$username)";
                $attributes = array();
                $attributes[] = 'samaccountname';
                @ldap_control_paged_result($ldap, 25);
                $result = @ldap_search($ldap,"dc=smig, dc=corp",$filter, $attributes);
                $info = @ldap_get_entries($ldap, $result);
                if(isset($info['count']) && !empty($info['count'])){
                    $successlogin = true;
                }
                @ldap_close($ldap);
            }
            
            return $successlogin; // return login T/F
        }

    public static function logout($token) {
        $CI = &get_instance();
        $CI->load->model('User_token_model');
        $userToken = $CI->User_token_model->get_by('token', $token);
        $time = Util::timeNow();
        $data = array(
            'ipAddress' => RequestUtil::getIpAddress(),
            'lastActivity' => $time,
            'tokenExpiredTime' => Util::timeAdd('+5 days'),
            'countRequest' => $userToken['countRequest'] + 1,
            'status' => self::STATUS_LOGOUT
        );
        $update = $CI->User_token_model->save($userToken['id'], $data);
        if ($update) {
            return TRUE;
        } else {
            return 'Logout gagal.';
        }
    }

    public static function validateToken($token) {
        $CI = &get_instance();
        $CI->load->model('User_token_model');
        // $userToken = $CI->User_token_model->with('Device')->get_by('upper(TOKEN)', $token);
        $userToken = dbGetRow("select * from user_tokens where token='".$token."'");
        $expiredDate = dbGetOne("select to_char(TOKEN_EXPIRED_TIME, 'DD-MM-YYYY HH24:MI:SS') as expired_time from user_tokens where token='".$token."'");
        $time = Util::timeNow();
        if (!empty($userToken)) {
            $CI->load->model('User_model');
            $userModel = new User_model();
            $user = $userModel->get($userToken['USER_ID']);
            if ($userToken['STATUS'] == self::STATUS_LOGIN && date("Y-m-d H:i:s", strtotime($expiredDate)) >= $time) {
                $data = array(
                    'IP_ADDRESS' => RequestUtil::getIpAddress(),
                    'LAST_ACTIVITY' => date("d/M/Y h:i:s a", strtotime($time)),
                    'TOKEN_EXPIRED_TIME' => date("d/M/Y h:i:s a", strtotime(Util::timeAdd('+30 minutes'))),
                    'COUNT_REQUEST' => $userToken['COUNT_REQUEST'] + 1,
                );
                // $jabatan = $userModel->getJabatan($userToken['USER_ID']);
                // $user['jabatan'] = $jabatan['jabatan'];
                // $user['jabatan_group'] = $jabatan['jabatan_group'];
                // $user['eselon'] = $CI->User_model->getBy($userToken['USER_ID'],'eselon_name');
                $user['company'] = $CI->User_model->filter(" where \"user_id\"='".$userToken['USER_ID']."' ")->getOneFilter( 'company');
                if($user['role'] == 'X'){
                    $access_eksternal = dbGetRow("select company,document_type from user_eksternal where user_id=".$userToken['USER_ID']);
                    $user['company_access'] = json_decode($access_eksternal['COMPANY'], true);
                    $user['document_type_access'] = json_decode($access_eksternal['DOCUMENT_TYPE'], true);
                }
                unset($user['password']);
                unset($user['password_salt']);
                // echo "<pre>";print_r($user);die();
                $update = $CI->User_token_model->save($userToken['ID'], $data);
                if ($update) {
                    
                    return array($userToken['TOKEN'], $user);
                } else {
                    return 'Update status user gagal.';
                }
            } elseif ($userToken['STATUS'] == self::STATUS_LOGIN && date("Y-m-d H:i:s", strtotime($expiredDate)) < $time) {
                $token = self::generateToken($user['id']);
                $data = array(
                    'IP_ADDRESS' => RequestUtil::getIpAddress(),
                    'LAST_ACTIVITY' => date("d/M/Y h:i:s a", strtotime($time)),
                    'TOKEN_EXPIRED_TIME' => date("d/M/Y h:i:s a", strtotime(Util::timeAdd('+30 minutes'))),
                    'COUNT_REQUEST' => $userToken['COUNT_REQUEST'] + 1,
                    'TOKEN' => $token
                );
                // echo "<pre>";print_r($data);die();
                $updateToken = $CI->User_token_model->update($userToken['ID'], $data);
                // $device = (!empty($userToken['device'])) ? json_encode(array('secureId' => $userToken['device']['secureId'])) : NULL;
                // $regToken = self::registerToken($userToken['userId'], $device, $CI);
                if ($updateToken != FALSE) {
                    // $update = $CI->User_token_model->update($userToken['id'], array('status' => self::STATUS_EXPIRED));
                    // $CI->db->trans_complete();
                    return array($token, $user);
                } else {
                    return 'Token gagal diganti.';
                }
            }

            return 'Token sudah kadaluarsa.';

        } else {

            return 'Token tidak terdaftar.';

        }
    }

    public static function registerToken($userId, $device = NULL, $CI = NULL) {
        if (is_null($CI))
            $CI = &get_instance();

        if (!empty($device)) {
            $deviceData = (Array) json_decode($device);
            $CI->load->model('Device_model');
            $existDevice = $CI->Device_model->get_by(array('secure_id' => $deviceData['secure_id']));
            if (empty($existDevice)) {
                $insertDevice = $CI->Device_model->create($deviceData,true);
                if ($insertDevice) {
                    $existDevice = $CI->Device_model->get($insertDevice);
                } else {
                    return FALSE;
                }
            }
        }

        $CI->load->model('User_token_model');
        $time = Util::timeNow();
        $token = self::generateToken($userId);
        $data = array(
            'user_id' => $userId,
            'device_id' => !empty($existDevice) ? $existDevice['id'] : NULL,
            'ip_Address' => RequestUtil::getIpAddress(),
            'login_Time' => date("d/M/Y h:i:s a", strtotime($time)),
            'last_Activity' => date("d/M/Y h:i:s a", strtotime($time)),
            'token' => $token,
            'token_Expired_Time' => date("d/M/Y h:i:s a", strtotime(Util::timeAdd('+30 minutes'))),
            'count_Request' => 1,
            'status' => self::STATUS_LOGIN
        );
        // echo "<pre>";print_r($data);die();
        $insert = $CI->User_token_model->create($data,true);
        if ($insert) {
            return $token;
        } else {
            return FALSE;
        }
    }

}

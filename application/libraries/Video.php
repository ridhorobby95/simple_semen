<?php

class Video {

    const NOT_FOUND = 'NFD';
    const ERROR = 'ERR';
    const UNAUTHORIZED = 'UNT';
    const INVALID_TOKEN = 'INT';

    public static function getMime($name) {
        $mimes = & get_mimes();
        $exp = explode('.', $name);
        $extension = end($exp);

        if (isset($mimes[$extension])) {
            $mime = is_array($mimes[$extension]) ? $mimes[$extension][0] : $mimes[$extension];
        } else {
            $mime = 'application/octet-stream';
        }
        return $mime;
    }

    public static function getFileSize($id, $name, $path) {
        $location = self::getLocation($id, $name, $path);
        return file_exists($location) ? filesize($location) : 0;
    }

    public static function getLocation($id, $name, $path) {
        $CI = & get_instance();
        $config = $CI->config->item('utils');
        $ext = explode('.', $name);
        return $config['upload_dir'] . $path . '/' . self::getFileName($id, $name) . '.' . $ext[count($ext)-1];
    }

    public static function getVideo($id, $name, $filepath) {
        if (empty($name)) {
            return NULL;
        }

        $video = array(
            'name' => $name,
            'mime' => self::getMime($name),
            'size' => self::getFileSize($id, $name, $filepath),
            'link' => self::createLink($id, $name, $filepath)
        );
        return $video;
    }

    public static function getUuid($id, $filepath) {
        $name = self::getFileName($id, $filepath) . '---' . $id . '---' . $filepath;
        return base64_encode($name);
    }

    public static function getData($uuid) {
        return explode('---', base64_decode($uuid));
    }

    public static function getFileName($id, $name) {
        return md5($id . $name);
    }

    public static function getLink($id, $filepath) {
        return site_url('download') . '?file=' . self::getUuid($id, $filepath);
    }

    public static function createLink($id, $name, $filepath) {
        $video = self::getLocation($id, $name, $filepath);
        return (file_exists($video)) ? base_url() . $video : null;
    }

    public static function upload($field, $id, $name, $path, $config = array()) {
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $config['upload_path'] = $ciConfig['upload_dir'] . $path . '/';
        $config['allowed_types'] = '*';
        $config['file_name'] = self::getFileName($id, $name);
        $CI->load->library('upload');
        $upload = new CI_Upload($config);
        if (!$upload->do_upload($field)) {
            return $upload->display_errors('', '');
        } else {
            return TRUE;
        }
    }

    public static function uploadVideo($field, $id, $name='', $folder, $config = array()) {
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $config['upload_path'] = $path = $ciConfig['full_upload_dir'] . $folder . '/';
        $config['allowed_types'] = 'mp4|mpeg|avi|flv|wmv|mov';
        // $config['file_name'] = $fileName = self::getFileName($id, $name);
        $config['file_name'] = $fileName = $name;
        $CI->load->library('upload');

        $upload = new CI_Upload($config);
        $upload->overwrite = true;
        if (!$upload->do_upload($field)) {
            return $upload->display_errors('', '');
        } else {
            $fileConfig = array('source_file' => $path . $fileName);
            if (!$name) {
                $fileConfig['source_file'] .= $upload->file_ext;
            }
            return TRUE;
        }
    }

    public static function generateLink($id, $name, $folder) {
        $file = self::createLink($id, $name, $folder);
        return $file;
    }

    public static function copy($link, $id, $name, $path) {
        $CI = & get_instance();
        $path_info = pathinfo($link);
        $ciConfig = $CI->config->item('utils');
        $cpy = copy($link, $ciConfig['upload_dir'] . $path . '/' . self::getFileName($id, $name).'.'.$path_info['extension']);
        return $cpy;
    }

    public static function remove($id, $path) {
        $CI = & get_instance();
        $config = $CI->config->item('utils');

        $file = $config['upload_dir'] . $path . '/' . self::getFileName($id, $path);
        if (file_exists($file)) {
            unlink($file);
        }
    }

}

<?php

class Image {

    const IMAGE_ORIGINAL = 'ORIGINAL';
    const IMAGE_LARGE = 'LARGE';
    const IMAGE_MEDIUM = 'MEDIUM';
    const IMAGE_SMALL = 'SMALL';
    const IMAGE_THUMB = 'THUMB';
    const IMAGE_OPACITY = 'OPACITY';

    public static function getMime($name, $location = null) {
        $mimes = & get_mimes();
        $exp = explode('.', $name);
        $extension = end($exp);

        if (isset($mimes[$extension])) {
            $mime = is_array($mimes[$extension]) ? $mimes[$extension][0] : $mimes[$extension];
        } else if (!empty($location)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $location);
            finfo_close($finfo);
        } else {
            $mime = 'application/octet-stream';
        }
        return $mime;
    }

    public static function getLocation($id, $size, $name, $folder) {
        $CI = & get_instance();
        $config = $CI->config->item('utils');

        return $config['upload_dir'] . $folder . '/' . self::getFileName($id, $size, $name);
    }

    public static function getFileSize($id, $size, $name, $folder) {
        $location = self::getLocation($id, $size, $name, $folder);
        return file_exists($location) ? filesize($location) : 0;
    }

    public static function getProperty($id, $size, $name, $folder) {
        $location = self::getLocation($id, $size, $name, $folder);
        return file_exists($location) ? getimagesize($location) : FALSE;
    }

    public static function getImage($id, $name, $folder) {
        if (empty($name)) {
            return NULL;
        }

        $images = array();
        foreach (array(self::IMAGE_ORIGINAL, self::IMAGE_LARGE, self::IMAGE_MEDIUM, self::IMAGE_SMALL, self::IMAGE_THUMB) as $size) {
            $filesize = self::getFileSize($id, $size, $name, $folder);

            if ($filesize > 0) {
                $location = self::getLocation($id, $size, $name, $folder);
                list($width, $height, $mime) = self::getProperty($id, $size, $name, $folder);
                $sizeName = camelize(strtolower($size));
                $images[$sizeName] = array(
                    'name' => self::getFileName($id, $size, $name),
                    'mime' => self::getMime($name, $location),
                    'size' => $filesize,
                    'width' => $width,
                    'height' => $height,
                    'type' => $sizeName,
                    'link' => self::createLink($id, $size, $name, $folder)
                );
            }
        }
        return (!empty($images)) ? $images : NULL;
    }

    public static function generateLink($id, $name, $folder) {
        $images = array();
        foreach (array(self::IMAGE_ORIGINAL, self::IMAGE_LARGE, self::IMAGE_MEDIUM, self::IMAGE_SMALL, self::IMAGE_THUMB) as $size) {
            $images[camelize(strtolower($size))] = self::createLink($id, $size, $name, $folder);
        }
        return $images;
    }

    public static function setFileName($name) {
        return str_replace('_', '-', underscore($name));
    }

    public static function getFileName($id, $size, $name) {
        $name = self::setFileName($name);
        return strtolower($size) . '-' . md5($id . $size) . md5($id . $name) . '-' . $name;
    }

    public static function getName($link) {
        $part = explode('-', $link);
        unset($part[0], $part[1]);
        return implode('-', $part);
    }

    public static function createLink($id, $size, $name, $folder) {
        $image = self::getLocation($id, $size, $name, $folder);
        return (file_exists($image)) ? base_url() . $image : null;
    }

    /**
     * 
     * @param string|integer $id (Primary Key)
     * @param string $name
     * @param string $folder (posts/images)
     * @return boolean
     */
    public static function upload($field, $id, $name, $folder, $config = array()) {
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $config['upload_path'] = $path = $ciConfig['upload_dir'] . $folder . '/';
        $config['allowed_types'] = '*';
        $config['file_name'] = $fileName = self::getFileName($id, self::IMAGE_ORIGINAL, $name);
        $CI->load->library('upload');
        $CI->load->library('image_lib');

        $upload = new CI_Upload($config);
        if (!$upload->do_upload($field)) {
            return $upload->display_errors('', '');
        } else {
            $imageConfig = array('source_image' => $path . $fileName);
            list($width, $height) = self::getProperty($id, self::IMAGE_ORIGINAL, $name, $folder);

            foreach (array(self::IMAGE_MEDIUM, self::IMAGE_SMALL, self::IMAGE_THUMB) as $size) {
                $imageConfig['new_image'] = $path . self::getFileName($id, $size, $name);
                list($imageConfig['width'], $imageConfig['height']) = explode('x', $ciConfig['image'][strtolower($size)]);
                if ($width > $height) {
                    unset($imageConfig['width']);
                } else {
                    unset($imageConfig['height']);
                }
                $image = new CI_Image_lib();
                $image->initialize($imageConfig);
                $image->resize();
                $image->clear();
            }

            return TRUE;
        }
    }

    public function opacityLogo($idCompany){
        $this->load->model('Unitkerja_model');
        $photoCompany = $this->Unitkerja_model->getOneBy($idCompany, "\"PHOTO\"");
        if($photoCompany!=NULL){
            // $a =strtolower($size) . '-' . md5('0000'.$idCompany.'original') . md5('0000'.$idCompany.$photoCompany).'-'.$photoCompany;
            // echo $a;
            $logo_name = self::getLocation($idCompany, self::IMAGE_ORIGINAL, $photoCompany, 'company');
            // echo $logo_name."<br>";
            $opacity_name = self::getLocation($idCompany, self::IMAGE_OPACITY, $photoCompany, 'company');
            // echo $opacity_name."<br>";
            if(!file_exists("./".$opacity_name)){
                // echo "masuk create opacity";
                $image = imagecreatefrompng("./".$logo_name);
                $opacity = 0.2;
                imagealphablending($image, false); // imagesavealpha can only be used by doing this for some reason
                imagesavealpha($image, true); // this one helps you keep the alpha. 
                $transparency = 1 - $opacity;
                imagefilter($image, IMG_FILTER_COLORIZE, 0,0,0,127*$transparency); // the fourth parameter is alpha
                // header('Content-type: image/png');
                imagepng($image,"./".$opacity_name);
                // chmod("./".$opacity_name, 0755); 
            }
        }
    }

    public static function uploadReal($field, $id, $name, $folder, $config = array()) {
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $config['upload_path'] = $path = $ciConfig['full_upload_dir'] . $folder . '/';
        unlink($config['upload_path'].$id.'_opa.png');
        $config['allowed_types'] = 'png';
        $config['file_name'] = $fileName = $name;
        $config['overwrite'] = TRUE;
        $CI->load->library('upload');
        $CI->load->library('image_lib');

        $upload = new CI_Upload($config);
        $upload->overwrite = true;
        if (!$upload->do_upload($field)) {
            // return $upload->display_errors('', '');
            return "Only png file is allowed!";
        } else {
            // $imageConfig = array('source_image' => $path . $fileName);
            // list($width, $height) = self::getProperty($id, self::IMAGE_ORIGINAL, $name, $folder);

            // foreach (array(self::IMAGE_MEDIUM, self::IMAGE_SMALL, self::IMAGE_THUMB) as $size) {
            //     $imageConfig['new_image'] = $path . self::getFileName($id, $size, $name);
            //     list($imageConfig['width'], $imageConfig['height']) = explode('x', $ciConfig['image'][strtolower($size)]);
            //     if ($width > $height) {
            //         unset($imageConfig['width']);
            //     } else {
            //         unset($imageConfig['height']);
            //     }
            //     $image = new CI_Image_lib();
            //     $image->initialize($imageConfig);
            //     $image->resize();
            //     $image->clear();
            // }

            return TRUE;
        }
    }

    public static function copy($link, $id, $name, $folder, $config = array()) {
        $CI = & get_instance();
        $ciConfig = $CI->config->item('utils');
        $path = $ciConfig['upload_dir'] . $folder . '/';
        $imageConfig = array('source_image' => $link);
        list($width, $height) = file_exists($link) ? getimagesize($link) : FALSE;

        foreach (array(self::IMAGE_ORIGINAL, self::IMAGE_LARGE, self::IMAGE_MEDIUM, self::IMAGE_SMALL, self::IMAGE_THUMB) as $size) {
            $imageConfig['new_image'] = $path . self::getFileName($id, $size, $name);
            list($imageConfig['width'], $imageConfig['height']) = explode('x', $ciConfig['image'][strtolower($size)]);
            if (empty($imageConfig['width']) && empty($imageConfig['height'])) {
                $imageConfig['width'] = $width;
                $imageConfig['height'] = $height;
            }
            if ($width > $height) {
                unset($imageConfig['width']);
            } else {
                unset($imageConfig['height']);
            }
            $image = new CI_Image_lib();
            $image->initialize($imageConfig);
            $image->resize();
            $image->clear();
        }
        return TRUE;
    }

    public static function remove($id, $name, $folder) {
        foreach (array(self::IMAGE_ORIGINAL, self::IMAGE_LARGE, self::IMAGE_MEDIUM, self::IMAGE_SMALL, self::IMAGE_THUMB) as $size) {
            $image = self::getLocation($id, $size, $name, $folder);
            if (file_exists($image)) {
                unlink($image);
            }
        }
    }

}

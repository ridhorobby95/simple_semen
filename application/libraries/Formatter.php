<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Mengubah format data
 * @author Sevima
 * @version 1.0
 */
class Formatter {

    public static $default_months = array(
        1 => 'Jan',
        2 => 'Feb',
        3 => 'Mar',
        4 => 'Apr',
        5 => 'Mei',
        6 => 'Jun',
        7 => 'Jul',
        8 => 'Agt',
        9 => 'Sept',
        10 => 'Okt',
        11 => 'Nov',
        12 => 'Des'
    );

    public static $months = array(
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    );
    public static $days = array(
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu',
        7 => 'Minggu'
    );

    public static function getMonth($default = NULL) {
        return !empty($default) ? self::$default_months[$default] : self::$default_months;
    }

    /**
     * Mengembalikan waktu dari tanggal
     * @param string $ymd
     * @return string
     */
    public static function formatTime($ymd) {
        list($ymd, $time) = explode(' ', $ymd);
        list($h, $i, $s) = explode(':', $time);

        return $h . ':' . $i;
    }

    /**
     * Membalik tanggal ymd jadi dmy dan sebaliknya
     * @param string $ymd
     * @return string
     */
    public static function formatDate($ymd) {
        list($ymd, $time) = explode(' ', $ymd);
        list($y, $m, $d) = explode('-', $ymd);

        return $d . '-' . $m . '-' . $y;
    }

    /**
     * Merubah format menjadi dMy
     * @param string $ymd
     * @return string
     */
    public static function formatDateMonthString($ymd) {
        // list($ymd, $time) = explode(' ', $ymd);
        // list($y, $m, $d) = explode('-', $ymd);
        $date = date_create($ymd);
        $date = date_format($date, "d-M-Y");

        return $date;
    }

    /**
     * Mengubah format tanggal lengkap Indonesia
     * @param string $ymd
     * @param bool $hari
     * @return string
     */
    public static function toIndoDate($ymd, $hari = false) {
        list($y, $m, $d) = explode('-', $ymd);

        $n = date('N', mktime(0, 0, 0, $m, $d, $y));

        return ($hari ? self::$days[$n] . ', ' : '') . (int) $d . ' ' . $this->getMonth((int) $m) . ' ' . $y;
    }

    /**
     * Mengubah format tanggal dan waktu lengkap Indonesia
     * @param string $ymd
     * @param bool $hari
     * @return string
     */
    public static function toIndoDateTime($ymd, $hari = false) {
        list($date, $time) = explode(' ', $ymd);

        return self::toIndoDate($date, $hari) . ' - pukul ' . $time;
    }

    /**
     * Mendapatkan lama relatif dari date
     * @param string $ymd
     * @param bool $hari
     * @return string
     */
    public static function dateToDiff($ymd, $inpFormat = 'h', $hari = true) {
        list($date, $time) = explode(' ', $ymd);
        list($y, $m, $d) = explode('-', $date);
        list($h, $i, $s) = explode(':', $time);
        $format = array('n' => false, 's' => false, 'm' => false, 'h' => false, 'd' => false, 'w' => false, 'M' => false, 'y' => false);
        $format[$inpFormat] = true;
        $time = mktime($h, $i, $s, $m, $d, $y);
        $now = time();
        $isfuture = $now < $time;
        $diff = abs($now - $time);

        if ($format['n'])
            return self::toIndoDate($ymd, $hari);

        if ($diff < 60) {
            if (empty($diff))
                return 'baru saja';
            else
                return $diff . ' detik ' . ($isfuture ? 'lagi' : 'lalu');
        } else
            if($format['s'])
                return self::toIndoDate($ymd, $hari);
            $diff = ceil($diff / 60);

        if ($diff < 60)
            return $diff . ' menit ' . ($isfuture ? 'lagi' : 'lalu');
        else
            if($format['m'])
                return self::toIndoDate($ymd, $hari);
            $diff = ceil($diff / 60);

        if ($diff < 24)
            return $diff . ' jam ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            if($format['h'])
                return self::toIndoDate($ymd, $hari);
            $diff = ceil($diff / 24);
        }

        if($diff == 1){
            return  ($isfuture ? 'besok' : 'kemarin');
        }else{
            if($format['d'])
                return self::toIndoDate($ymd, $hari);
        }

        if ($diff < 7)
            return $diff . ' hari ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            if($format['w'])
                return self::toIndoDate($ymd, $hari);
            $diff = ceil($diff / 7);
        }

        if ($diff < 4)
            return $diff . ' minggu ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            if($format['M'])
                return self::toIndoDate($ymd, $hari);
            $diff = ceil($diff / 4);
        }

        if ($diff < 12)
            return $diff . ' bulan ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            return self::toIndoDate($ymd, $hari);
        }
    }

    public static function dateTimeToDiff($ymd, $inpFormat = 'h', $hari = true) {
        list($date, $time) = explode(' ', $ymd);
        list($y, $m, $d) = explode('-', $date);
        list($h, $i, $s) = explode(':', $time);
        $format = array('s' => false, 'm' => false, 'h' => false, 'd' => false, 'w' => false, 'M' => false, 'y' => false);
        $format[$inpFormat] = true;
        $time = mktime($h, $i, $s, $m, $d, $y);
        $now = time();
        $isfuture = $now < $time;
        $diff = abs($now - $time);

        if ($diff < 60) {
            if (empty($diff))
                return 'baru saja';
            else
                return $diff . ' detik ' . ($isfuture ? 'lagi' : 'lalu');
        } else
            if($format['s'])
                return self::toIndoDateTime($ymd, $hari);
            $diff = floor($diff / 60);

        if ($diff < 60)
            return $diff . ' menit ' . ($isfuture ? 'lagi' : 'lalu');
        else
            if($format['m'])
                return self::toIndoDateTime($ymd, $hari);
            $diff = floor($diff / 60);

        if ($diff < 24)
            return $diff . ' jam ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            if($format['h'])
                return self::toIndoDateTime($ymd, $hari);
            $diff = floor($diff / 24);
        }

        if($diff == 1){
            return  ($isfuture ? 'besok' : 'kemarin');
        }else{
            if($format['d'])
                return self::toIndoDateTime($ymd, $hari);
        }

        if ($diff < 7)
            return $diff . ' hari ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            if($format['w'])
                return self::toIndoDateTime($ymd, $hari);
            $diff = floor($diff / 7);
        }

        if ($diff < 4)
            return $diff . ' minggu ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            if($format['M'])
                return self::toIndoDateTime($ymd, $hari);
            $diff = floor($diff / 4);
        }

        if ($diff < 12)
            return $diff . ' bulan ' . ($isfuture ? 'lagi' : 'lalu');
        else{
            return self::toIndoDateTime($ymd, $hari);
        }
    }

    public static function toCountHourMinute($minute) {
        return floor($minute / 60) . ' Jam ' . ($minute % 60) . ' Mnt';
    }

}